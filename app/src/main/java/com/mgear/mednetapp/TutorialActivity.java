package com.mgear.mednetapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.mgear.mednetapp.adapter.TutorialPagerAdapter;
import com.mgear.mednetapp.fragments.organization.TutorialStatusFragment;
import com.mgear.mednetapp.fragments.organization.TutorialTestsFragment;
import com.mgear.mednetapp.views.CustomViewPager;

/**
 * Created by Jye on 2017/6/8.
 * class: TutorialActivity
 */
public class TutorialActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private TutorialPagerAdapter mTutorialPagerAdapter = null;
    private CustomViewPager mViewPager = null;
    private int mIndex = 0;
    private String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        Intent intent = getIntent();
        result = intent.getStringExtra("result");

        // Create the adapter that will return a fragment for each of the
        // primary sections of the activity.
        this.mTutorialPagerAdapter = new TutorialPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        this.mViewPager = (CustomViewPager) findViewById(R.id.vpPager);
        this.mViewPager.addOnPageChangeListener(this);
        this.mViewPager.setAdapter(mTutorialPagerAdapter);
        this.mViewPager.setPagingEnabled(true);

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    public void onPreviousClick(View view) {
        mIndex = mViewPager.getCurrentItem();
        mViewPager.setCurrentItem(--mIndex);
    }

    public void onNextClick(View view) {
        mIndex = mViewPager.getCurrentItem();
        mViewPager.setCurrentItem(++mIndex);
    }

    public void onStartClick(View view) {
        if (result != null && result.equals("main")) {
            setResult(RESULT_OK);
            finish();
        }
        else {
            startActivity(new Intent(this, OrgMainActivity.class));
            this.finish();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 3:
                ((TutorialTestsFragment) mTutorialPagerAdapter.getItem(position)).startPagerAnimation();
                break;
            case 4:
                ((TutorialStatusFragment) mTutorialPagerAdapter.getItem(position)).startPagerAnimation();
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}