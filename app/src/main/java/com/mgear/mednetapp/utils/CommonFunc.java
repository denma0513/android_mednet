package com.mgear.mednetapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jye on 2017/6/9.
 * class: CommonFunc
 */
public class CommonFunc {

    private static MediaPlayer mp = null;
    public static String orgMap = "";
    public static String ratingUrl = "";
    public static String pushToken = "";
    public static String organization = "";
    public static String organizationEn = "";
    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 24 * HOUR;

    /**
     */
    public static String getHour(Double mhour) {
        if (mhour == 0)
            return "0小時0分";
        if (mhour < 0)
            mhour = 0 - mhour;
        int hour = 0;
        int min = 0;
        hour = (int) (mhour / 1);
        min = (int) (mhour * MINUTE) % 60;
        return hour + "小時" + min + "分";
    }


    /**
     * 判斷字串是否有效
     *
     * @param obj
     */
    public static boolean isBlank(Object obj) {
        if (obj == null || "".equals(obj.toString()) || "null".equals(obj.toString()) || "NULL".equals(obj.toString()) || "Null".equals(obj.toString())) {
            return true;
        }
        return false;
    }

    /**
     * 回傳字串
     *
     * @param obj
     */
    public static String getString(Object obj) {
        if (obj == null || "".equals(obj.toString()) || "null".equals(obj.toString()) || "NULL".equals(obj.toString()) || "Null".equals(obj.toString())) {
            return "";
        }
        return getString(obj.toString(), null);
    }

    /**
     * 回傳字串
     *
     * @param obj
     * @param rtnStr
     */
    public static String getString(Object obj, String rtnStr) {
        rtnStr = rtnStr == null ? "" : rtnStr;
        if (obj == null || "".equals(obj.toString())) {
            return rtnStr;
        }
        return getString(obj.toString(), rtnStr);
    }

    /**
     * 回傳字串
     *
     * @param obj
     * @param rtnStr
     */
    public static String getString(String obj, String rtnStr) {
        rtnStr = rtnStr == null ? "" : rtnStr;
        if (obj == null || "".equals(obj)) {
            return rtnStr;
        }
        return obj;
    }

    /**
     * 回傳字串
     *
     * @param jsonStr
     */
    public static JSONObject parseJSON(String jsonStr) {
        try {
            if (null == jsonStr)
                return new JSONObject();
            return new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    /**
     * 撥放聲音檔
     *
     * @param context    Context
     * @param resourceID Resource
     */
    public static void playSound(Context context, int resourceID) {
        try {
            if (mp != null) {
                mp.release();
                mp = null;
            }
            if (resourceID > 0) {
                mp = MediaPlayer.create(context, resourceID);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 檢查網路的狀態
     *
     * @param context Context
     * @return T/F
     */
    public static boolean checkNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //取得WIFI連線狀態
        return cm.getActiveNetworkInfo() != null;
    }

    /**
     * 載入圖片檔
     *
     * @param context Context
     * @param resId   Resource
     * @return Bitmap
     */
    @SuppressWarnings("deprecation")
    public static Bitmap readBitmap(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    /**
     * 載入圖片檔 壓縮1/2
     *
     * @param context Context
     * @param resId   Resource
     * @return Bitmap
     */
    @SuppressWarnings("deprecation")
    public static Bitmap readBitmapHalf(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        opt.inSampleSize = 2;
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    /**
     * 從URL連結載入圖片
     *
     * @param url URL
     * @return Drawable
     */
    public static Bitmap loadImageFromUrl(String url) {
        try {
            return BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 載入HTML內容
     *
     * @param html Html
     * @return Spanned
     */
    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    /**
     * 產生條碼
     *
     * @param contents      Content
     * @param format        Format
     * @param desiredWidth  Width
     * @param desiredHeight Height
     * @return Bitmap
     * @throws WriterException Exception
     */
    public static Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int desiredWidth, int desiredHeight) throws WriterException {
        if (contents.length() == 0) return null;
        final int WHITE = 0xFFFFFFFF;
        final int BLACK = 0xFF000000;
        HashMap<EncodeHintType, String> hints = null;
        String encoding = null;
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                encoding = "UTF-8";
                break;
            }
        }
        if (encoding != null) {
            hints = new HashMap<>(2);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }

        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result = writer.encode(contents, format, desiredWidth, desiredHeight, hints);
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    /**
     * 設定清單的高度
     *
     * @param listView ListView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    /**
     * 設定清單的高度
     *
     * @param listView ListView
     * @param group    Group
     */
    public static void setListViewHeightBasedOnChildren(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null, listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    /**
     * 轉換成整數陣列
     *
     * @param text  String
     * @param regex Regex
     * @return Integer array
     */
    public static int[] convertToIntArray(String text, String regex) {
        try {
            String[] ary = text.split(regex);
            if (ary.length > 0) {
                int[] result = new int[ary.length];
                for (int i = 0; i < ary.length; i++)
                    result[i] = Integer.valueOf(ary[i]);
                return result;
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    /**
     * 字串轉整數
     *
     * @param value Value
     * @return Integer
     */
    public static int tryParseInteger(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }

    /**
     * 取得現在的秒數
     *
     * @return Seconds
     */
    public static int getNowSeconds() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.HOUR) * 3600 + cal.get(Calendar.MINUTE) * 60 + cal.get(Calendar.SECOND);
    }

    /**
     * 尋找陣列的索引
     *
     * @param array Array
     * @param value Value
     * @return Index
     */
    public static int findIndex(int[] array, int value) {
        for (int i = 0; i < array.length; i++)
            if (array[i] == value)
                return i;
        return -1;
    }

    /**
     * 判斷是否為mail格式
     *
     * @param email CharSequence
     * @return boolean
     */
    public static boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * 判斷是否為手機號碼格式
     *
     * @param mobile String
     * @return boolean
     */
    public static boolean isMobileValid(String mobile) {
        Log.i("SIGN", mobile);
        Pattern telRegex = Pattern.compile("(09)+[\\d]{8}");
        Matcher matcher = telRegex.matcher(mobile);

        return !TextUtils.isEmpty(mobile) && matcher.find();
    }


    /**
     * jsonobject 轉成map
     *
     * @param object JSONObject
     * @return boolean
     */
    public static HashMap jsonToMap(JSONObject object) throws JSONException {
        HashMap map = new HashMap();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = jsonToMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }


    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = jsonToMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    /**
     * {@link layout/transient_notification.xml}
     *
     * @param content content to show
     * @param context context
     */
    public static void showToast(@NonNull Context context, String content) {
        boolean longTime = true;
        @ColorInt int textColor = context.getResources().getColor(R.color.write_bg);
        @ColorInt int toastBackgroundColor = context.getResources().getColor(R.color.home_title_black);

        int type = longTime ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, content, type);
        ViewGroup toastView = (ViewGroup) LayoutInflater.from(context).inflate(R.layout
                .custom_toast_background, null, false);
        if (toastBackgroundColor != 0) {
            toastView.setBackgroundDrawable(getToastBackground(context, toastBackgroundColor));
        }
        TextView textView = (TextView) toastView.findViewById(R.id.customToastText);
        // 内部已经作非空判断了
        if (textColor != 0) {
            textView.setTextColor(textColor);
        }
        textView.setText(content);
        Typeface typeface = Typeface.create("sans-serif-condensed", Typeface.NORMAL);
        textView.setTypeface(typeface);
        toast.setView(toastView);
        //toast.setText(content);
        toast.show();
    }

    private static Drawable getToastBackground(@NonNull Context context, @ColorInt int color) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadius(24);
        gradientDrawable.setColor(color);
        return gradientDrawable;
    }

    public static Date UTC2Date(String mTime) {
        Date retDate = null;
        try {
            Date now = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.TAIWAN);
            if (mTime.length() == 20) {
                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.TAIWAN);
            } else if (mTime.length() == 22) {
                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'", Locale.TAIWAN);
            } else if (mTime.length() == 23) {
                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'", Locale.TAIWAN);
            } else if (mTime.length() == 24) {
                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.TAIWAN);
            }

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            //SimpleDateFormat sdf3 = new SimpleDateFormat("HH:mm");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
            retDate = sdf.parse(mTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return retDate;
    }

    public static Date UTC2DateE04(String mTime) {
        Date retDate = null;
        try {
            Date now = new Date();
            Log.i("debug", "mTime.length() = " + mTime.length() + " " + mTime);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'", Locale.TAIWAN);
            if (mTime.length() == 20) {
                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.TAIWAN);
            } else if (mTime.length() == 22) {
                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'", Locale.TAIWAN);
            } else if (mTime.length() == 23) {
                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'", Locale.TAIWAN);
            } else if (mTime.length() == 24) {
                sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.TAIWAN);
            }

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            //SimpleDateFormat sdf3 = new SimpleDateFormat("HH:mm");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
            retDate = sdf.parse(mTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return retDate;
    }

    public static String getUTCDateString(String date) {
        String retDate = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.TAIWAN);
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
            Date mDdate = sdf2.parse(date);
            retDate = sdf.format(mDdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return retDate;
    }

    public static String getNowUTCDateString() {
        String retDate = "";
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.TAIWAN);
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
        retDate = sdf.format(now);
        return retDate;
    }

    public static String getNowUTCDateStringOnlyDate() {
        String retDate = "";
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.TAIWAN);
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
        retDate = sdf.format(now);
        retDate+="T00:00:00.000";
        return retDate;
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 != null && cal2 != null) {
            return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)
                    && cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH);
        } else {
            throw new IllegalArgumentException("The date must not be null");
        }
    }

    public static boolean isSameMonth(Calendar cal1, Calendar cal2) {
        if (cal1 != null && cal2 != null) {
            return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
        } else {
            throw new IllegalArgumentException("The date must not be null");
        }
    }

    public static void getHelloWord() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        Random random = new Random();
        int index = 0;
        String helloTitle, helloContent;
        JSONObject retObj = new JSONObject();

        if (hour >= 6 && hour < 10) {
            index = random.nextInt(MegaApplication.morningHello.length);
            helloContent = MegaApplication.morningHello[index];
            helloTitle = "早安";
        } else if (hour >= 10 && hour < 12) {
            index = random.nextInt(MegaApplication.morningHello2.length);
            helloContent = MegaApplication.morningHello2[index];
            helloTitle = "早安";
        } else if (hour >= 12 && hour < 14) {
            index = random.nextInt(MegaApplication.afternoonHello.length);
            helloContent = MegaApplication.afternoonHello[index];
            helloTitle = "午安";
        } else if (hour >= 14 && hour < 18) {
            index = random.nextInt(MegaApplication.afternoonHello2.length);
            helloContent = MegaApplication.afternoonHello2[index];
            helloTitle = "午安";
        } else if (hour >= 18 && hour < 20) {
            index = random.nextInt(MegaApplication.nightHello.length);
            helloContent = MegaApplication.nightHello[index];
            helloTitle = "晚安";
        } else if (hour >= 20 && hour < 22) {
            index = random.nextInt(MegaApplication.nightHello2.length);
            helloContent = MegaApplication.nightHello2[index];
            helloTitle = "晚安";
        } else {
            index = random.nextInt(MegaApplication.nightHello3.length);
            helloContent = MegaApplication.nightHello3[index];
            helloTitle = "晚安";
        }

        MegaApplication.helloTitle = helloTitle;
        MegaApplication.helloContent = helloContent;
    }

    public static String switchHealthContent(String type) {
        return switchHealthContent(type, "");
    }

    public static String switchHealthContent(String type, String status) {
        Log.i("debug", "switchHealthContent: " + type + "   " + status);
        switch (type) {
            case "1": {
                switch (status) {
                    case "0": {
                        return "其實正常人的血糖調控是很穩定的，只要不刻意禁食太久，都不太會有低血糖的問題，低血糖多半發生在糖尿病患者上。當出現低血糖症狀時，即時的處理是非常重要的，常見的急救方法有吃方糖、喝果汁、喝可樂等，簡而言之就是快速從口補充高含糖物質。";
                    }
                    case "1": {
                        return "血糖是指血液中所含的葡萄糖，它是人體能量供應的主要來源。穩定的血糖是生命活動及生長發育所必需的，能為生命活動提供能量、維持大腦功能、維持心臟及骨骼肌功能、保護肝臟功能、參與組織細胞的構成、參與脂肪代謝。";
                    }
                    case "2": {
                        return "不要以為還沒被宣判為糖尿病，只是血糖偏高，其實有很多研究發現，血液中過多的葡萄糖就是很強的氧化劑，在人體內會產生自由基引起各部位的氧化破壞，尤其是血脂肪的氧化，血糖過高易造成脂肪代謝異常，使血管硬化，不但可能引起高血壓，還容易造成血栓，增加血管栓塞等風險。控制血糖要注意。";
                    }
                    default: {
                        return "血氧，是指血液中的氧氣，人體正常含氧量為95%以上。血液中含氧量越高，人的新陳代謝就越好。當然血氧含量高並不是一個好的現象，人體內的血氧都是有一定的飽和度，過低會造成機體供氧不足，過高會導致體內細胞老化。";
                    }
                }
            }
            case "2":
                switch (status) {
                    case "0": {
                        return "血壓偏低時可能會有頭暈、全身無力、疲倦、視力模糊等症狀。但有些人都沒有上述症狀或疾病，血壓長期就是比正常值偏低一些，例如收縮壓在80到90毫米汞柱之間（甚至極少數人收縮壓血壓只有60、70毫米汞柱），只要沒有合併其他症狀，就不用過度擔心。";
                    }
                    case "1": {
                        return "血壓控制在標準之內，可以降低35~40%中風的發生率，20~25%心肌梗塞的發生率及降低50%心臟衰竭的發生率，由此可見控制血壓對高血壓患者健康及生活品質提昇的重要性。";
                    }
                    case "2": {
                        return "您的血壓正常但是略偏高，處於臨界位置，所以要注意監測血壓的變化。因為即使血壓只是略有點高，時間久了，也可能會有相關疾病的產生包括：心臟病、痴呆症、腎臟疾病、視力問題、血壓越來越高、性功能障礙等。";
                    }
                    case "3": {
                        return "血壓偏高，疑似有高血壓，或有可能是緊張、壓力、睡眠不足引起，請多量測及定期檢測血壓，長期的血壓偏高容易造成動脈硬化，對全身的血管都有傷害，而可能造成多重器官的病變，請減少攝取油脂、高澱粉類及高糖份類食物並多運動，以避免罹患心臟血管等疾病。";
                    }
                    default: {
                        return "血壓是指心臟將血液經過血管運送到身體各部分，這血液對血管壁所產生的壓力稱為血壓。血壓是指動脈血壓，當血管擴張時，血壓下降；血管收縮時，血壓升高。";
                    }
                }
            case "3":
                switch (status) {
                    case "0": {
                        return "血氧不足，會讓身體內的細胞變得遲鈍。全身的氧氣和營養素是透過血液來搬運，因此一旦血液循環不良，就會造成氧氣和營養無法順利輸送，結果就造成了能源不足，產生倦怠和疲勞，並對大腦和各種器官產生危害。適當運動改善血液循環，提高體內含氧量。";
                    }
                    case "1": {
                        return "平時養成均衡飲食的習慣，適度補充維他命C和鐵質，也能讓身體更有「氧」。維他命C是體內水溶性抗氧化營養素，可幫助體內抗氧化能力，減少自由基產生，同時也能幫助鐵質的吸收，維他命C含量高食物有番石榴、木瓜、草莓、菠菜、椰菜花等。";
                    }
                    default: {
                        return "血氧，是指血液中的氧氣，人體正常含氧量為95%以上。血液中含氧量越高，人的新陳代謝就越好。當然血氧含量高並不是一個好的現象，人體內的血氧都是有一定的飽和度，過低會造成機體供氧不足，過高會導致體內細胞老化。";
                    }
                }
            case "4":
                switch (status) {
                    case "0": {
                        return "人體的水分只要失掉15%的水，生命就有危險。水也是全身關節的潤滑劑，更是神經系統的緩衝劑。當身體內缺乏水分時，人體就會犧牲一些部位的正常功能，以保護另一些組織和器官的正常工作，這樣就會產生組織損傷、疼痛等各種健康問題，嚴重時還會引發許多疾病與症狀。記得多喝水！";
                    }
                    case "1": {
                        return "人類可以10天不吃飯，但只要3天不喝水，恐怕就要有見上帝的準備了！\n" +
                                "水分是維持人體運作的重要條件。若是長期水份攝取不足，連帶而來的很可能就是結石、便秘、痛風等相關問題。為了避免疾病纏身，讓身體機能與代謝正常運作，每天正確喝水是很重要的喔！";
                    }
                    default: {
                        return "飲水的重要性：人體有70﹪是水，90﹪的血液是水組成的。 而且，水份還可以協助養份的運輸、促進新陳代謝、調節體溫、潤滑關節、保持皮膚的彈性，並將體內的廢物排出。 由此可知，必須隨時補充足夠的水分，才能維持人體的正常運作！ 再忙也要喝杯水多數人是因為口渴才想到要喝水！";
                    }
                }
            case "5":
                switch (status) {
                    case "0": {
                        return "睡眠不足會影響記憶與大腦認知功能，還會影響工作與日常生活。失眠會降低注意力、警覺性、集中力、思考合理性、以及解決問題的能力。睡眠不足會導致體重增加、皮膚老化，也會增加心臟病、高血壓、心律不整、中風…等危害健康的問題。養成良好的睡眠習慣很重要喔！";
                    }
                    case "1": {
                        return "好的睡眠品質應該由平常生活中就開始維持，平常的生活習慣越好，在晚上睡的也越好。建立良好的睡眠習慣：固定時間就寢及起床、睡前讓自己放鬆、半夜起來不看時間、睡前避免不愉快的談話等。養成良好的生活習慣：定期規律的運動、減少飲用含咖啡因的飲料、避免夜間頻繁上廁所、睡前不要喝太多水等。";
                    }
                    case "2": {
                        return "睡眠對人體健康很重要，由於忙碌的生活，很多人覺得每天難有足夠睡眠，所以很少思考睡太久對身體也有影響。 大多數成年人只需7~9小時的睡眠，就可以在身體機能上達到最佳狀態。如果成年人經常性睡覺超過9小時，反而會影響身體健康，帶來某些風險，如：損害大腦、增加患糖尿病的風險、導致體重增加。適當的睡眠時間很重要。";
                    }
                    default: {
                        return "好的睡眠品質應該由平常生活中就開始維持，平常的生活習慣越好，在晚上睡的也越好。建立良好的睡眠習慣：固定時間就寢及起床、睡前讓自己放鬆、半夜起來不看時間、睡前避免不愉快的談話等。養成良好的生活習慣：定期規律的運動、減少飲用含咖啡因的飲料、避免夜間頻繁上廁所、睡前不要喝太多水等。";
                    }
                }
            case "6":
                switch (status) {
                    case "0": {
                        return "多走路可以帶來許多好處，除了有助瘦身，還可以提升心血管和心智的健康。每天步行的時間長達30分鐘，可以得到許多好處：增加你的心智健康、有助減肥、降低某些癌症的罹患機率、增強免疫系統、避免糖尿病、走路有助戒糖、大大提升心血管健康、維持高齡者的活動力。記得多走路！";
                    }
                    case "1": {
                        return "走路對健康有益，這是無庸置疑的，而且因為它不受時間、空間的限制，行走的速度也可快可慢，進而達到不同的健身效果。只要每天規律的走路，就可以降低疾病的發生率，增強免疫力！不過也要注意走路的姿勢才不會傷到身體，要抬頭挺胸、邁大步、腳跟先著地、雙臂自然的擺動。";
                    }
                    default: {
                        return "世衛組織認定，走路是「世界上最好的運動」。數據統計，每走一步，可推動人體50%的血液流動起來、可擠壓人體50%的血管，是最簡單的「血管體操」，至少可運動50%的肌肉，有助於保持肌肉總量。";
                    }
                }
            case "7":
                switch (status) {
                    case "0": {
                        return "體重偏輕，請採均衡飲食並多運動。事先規劃每週的飲食，循序漸進。切忌以垃圾食物或高熱量、高糖、高鹽、高油脂食物增加熱量。補充足量的水果和蔬菜、五穀雜糧食物及瘦肉（例如去皮雞肉、里肌肉、魚肉），均衡飲食。";
                    }
                    case "1": {
                        return "標準體重，其計算公式：\n男性： （身高cm－80）×70﹪\n女性： （身高cm－70）×60﹪\n" +
                                "身體質量指數（BMI）\n" +
                                "其計算公式： BMI = 體重 (kg) / 身高 (m2)";
                    }
                    case "2": {
                        return "體重偏重，請設定實際可達的健康體重目標。多運動和健康吃：每天減少攝取500大卡熱量、減少靜態活動、每天測量體重、規律生活。並且要注意控制體重時，每日攝取熱量不可低於1200大卡。";
                    }
                    case "3": {
                        return "體重過重，請減輕體重，減少高油脂、高澱粉類及高糖份類食物，並且秉持「聰明吃，快樂動、天天量體重」原則。「聰明吃」，秉持「三多三少」原則，「三多」多喝開水、多吃蔬果與多以全穀雜糧類食物取代精製澱粉食物；「三少」則是少油、少鹽與少糖。快樂動，是指每次運動至少持續10分鐘。";
                    }
                    default: {
                        return "標準體重，其計算公式：\n男性： （身高cm－80）×70﹪\n女性： （身高cm－70）×60﹪\n" +
                                "身體質量指數（BMI）\n" +
                                "其計算公式： BMI = 體重 (kg) / 身高 (m2)";
                    }
                }
        }
        return "";
    }


    public static int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将cookie同步到WebView
     *
     * @param url    WebView要加载的url
     * @param cookie 要同步的cookie
     * @return true 同步cookie成功，false同步cookie失败
     * @Author JPH
     */
    public static boolean syncCookie(String url, String cookie) {
        Log.i("debug", "syncCookie: ");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(MegaApplication.context);
        }
        Log.i("debug", "url: "+url+" cookie = "+cookie);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setCookie(url, cookie);//如果没有特殊需求，这里只需要将session id以"key=value"形式作为cookie即可
        String newCookie = cookieManager.getCookie(url);
        Log.i("debug", "newCookie: "+newCookie);
        return TextUtils.isEmpty(newCookie) ? false : true;
    }

}