package com.mgear.mednetapp.utils;

import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;

/**
 * Created by Jye on 2017/8/30.
 * class: AnimationTools
 */
public class AnimationTools {

    public static void startScaleUpAnimation(View v, float upScale, float downScale, float anchorX, float anchorY) {
        if (v != null) {
            AnimationSet animSet = new AnimationSet(true);
            animSet.setInterpolator(new LinearInterpolator());
            ScaleAnimation animScaleUp = new ScaleAnimation(1.0f, upScale, 1.0f, upScale, 1, anchorX, 1, anchorY);
            animScaleUp.setDuration(500);
            animSet.addAnimation(animScaleUp);
            ScaleAnimation animScaleDown = new ScaleAnimation(1.0f, downScale, 1.0f, downScale, 1, anchorX, 1, anchorY);
            animScaleDown.setDuration(500);
            animScaleDown.setStartOffset(150);
            animSet.addAnimation(animScaleDown);
            v.startAnimation(animSet);
        }
    }

}