package com.mgear.mednetapp.utils;

import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;

/**
 * Created by mundane on 2018/8/26 上午12:50
 */

public class WebViewUtil {
    public static void configWebView(WebView webView) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setGeolocationEnabled(true);

        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        webSettings.setAllowFileAccess(true);

        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setTextSize(WebSettings.TextSize.NORMAL);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);


        //设置在WebView内部是否允许访问文件
        webSettings.setAllowFileAccess(true);

        //设置脚本是否允许自动打开弹窗
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        // 加快HTML网页加载完成速度
        if (Build.VERSION.SDK_INT >= 19) {
            webSettings.setLoadsImagesAutomatically(true);
        } else {
            webSettings.setLoadsImagesAutomatically(false);
        }


        webSettings.setUserAgentString("[medicrowdApp]");

//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
//            webSettings.setMixedContentMode(android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
//        }
    }


}
