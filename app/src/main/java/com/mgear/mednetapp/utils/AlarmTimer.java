package com.mgear.mednetapp.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.provider.SyncStateContract;
import android.util.Log;

import com.mgear.mednetapp.services.AlarmReceiver;

import java.util.Calendar;
import java.util.Date;

/**
 * 鬧鐘定時工具類
 *
 * @author dennis
 */
public class AlarmTimer {
    /**
     * 設定週期性鬧鐘
     *
     * @param context
     * @param firstTime
     * @param cycTime
     * @param action
     * @param AlarmManagerType 鬧鐘的型別，常用的有5個值：AlarmManager.ELAPSED_REALTIME、
     *                         AlarmManager.ELAPSED_REALTIME_WAKEUP、AlarmManager.RTC、
     *                         AlarmManager.RTC_WAKEUP、AlarmManager.POWER_OFF_WAKEUP
     */
    public static void setRepeatingAlarmTimer(Context context, long firstTime, long cycTime, String action, int AlarmManagerType) {
        Intent myIntent = new Intent(context, AlarmReceiver.class);
        myIntent.setAction(action);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, myIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManagerType, firstTime, cycTime, sender);
        //param1：鬧鐘型別，param1：鬧鐘首次執行時間，param1：鬧鐘兩次執行的間隔時間，param1：鬧鐘響應動作。
    }

    /**
     * 設定定時鬧鐘
     *
     * @param context
     * @param cycTime
     * @param action
     * @param AlarmManagerType 鬧鐘的型別，常用的有5個值：AlarmManager.ELAPSED_REALTIME、
     *                         AlarmManager.ELAPSED_REALTIME_WAKEUP、AlarmManager.RTC、
     *                         AlarmManager.RTC_WAKEUP、AlarmManager.POWER_OFF_WAKEUP
     */
    public static void setAlarmTimer(Context context, long cycTime, String action, long AlarmManagerType, Calendar date, int alarmId) {
        Intent myIntent = new Intent(context, AlarmReceiver.class);
        //cycTime = SystemClock.elapsedRealtime()+1000*60;
        //傳遞定時日期
        myIntent.putExtra("date", date.getTimeInMillis());
        myIntent.putExtra("alarmId", alarmId);
        myIntent.setAction(action);

        //給每個鬧鐘設定不同ID防止覆蓋
        //settings.edit().putInt("alarm_id", alarmId);
        PendingIntent sender = PendingIntent.getBroadcast(context, alarmId, myIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //alarm.set(AlarmManagerType, cycTime, sender);
        //alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, cycTime, AlarmManagerType, sender);
        Log.i("debug", "setAlarmTimer: " + cycTime);
        //alarm.setRepeating(AlarmManager.RTC_WAKEUP, cycTime, 1000 * 60 * 60 * 24, sender);
        alarm.setExact(AlarmManager.RTC_WAKEUP, cycTime, sender);
    }

    /**
     * 取消鬧鐘
     *
     * @param context
     * @param action
     */
    public static void cancelAlarmTimer(Context context, String action, int alarmId) {
        Intent myIntent = new Intent();
        myIntent.setAction(action);
        PendingIntent sender = PendingIntent.getBroadcast(context, alarmId, myIntent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(sender);
    }
}
