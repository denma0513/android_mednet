package com.mgear.mednetapp.utils;


import android.content.Context;
import android.util.Log;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.*;
import com.mgear.mednetapp.enums.CultureEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CustomerApiTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

public class HttpConnectFunc {
    private static String TAG = "HttpConnectFunc";

    /**
     * 連線的網址
     */

    //機構
    public static String CONNECT_URL = "http://survey.med-net.com:82/";

    //java api
    //public final static String CONNECT_API_URL = "http://34.80.52.49:8080/";
    //public final static String CONNECT_API_URL = "https://survey.med-net.com:8080/";
    public final static String CONNECT_API_URL = "http://34.80.52.49:8080/";

    //總站
    public final static String CONNECT_HQ_API_URL = "http://104.199.143.179/";

    public final static String CONNECT_MEDNET_API_URL = "https://med-net.com/";


    /**
     * 機構編號
     */
    public static UUID organizationID = UUID.fromString("828F5FAC-C7F3-471B-AEBB-F82139781086");
//    private final static UUID organizationID = UUID.fromString("edcf0770-bb56-440d-945e-ede6dc3e59aa");

    /**
     * 取得機構列表
     *
     * @return Message
     */
    public static JSONObject executeOrganizationList(String appVersion) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();
            params.put("version", appVersion);
            params.put("device", "2");

            //建立HTTP連線
            //URL mURL = new URL(String.format(Locale.getDefault(), "%s%s", CONNECT_API_URL, "MedicalApi/OrganizationListV2"));
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s", CONNECT_API_URL, "MedicalApi/OrganizationListTest"));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(true);

            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(params.toString());
            os.flush();
            os.close();

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JSONObject state = new JSONObject(response.toString());
                return state;
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return null;
    }


    /**
     * 取得訊息通知
     *
     * @param deviceID Device
     * @param contact  Contact
     * @param ticks    Ticks
     * @return Message
     */
    public static String[] executeMessageNotice(String deviceID, String contact, String ticks) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("ticks", ticks);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/MobileMessage", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.split("\\|");
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    /**
     * 取得機構資料
     *
     * @param deviceID DeviceId
     * @return obj
     */
    public static Organization executeOrganization(String deviceID) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/Organization", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();

            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return Organization.parse(state);
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return new Organization();
    }

    /**
     * 取得應用程式更新資訊
     *
     * @param deviceID DeviceID
     * @return obj
     */
    public static AppVersion executeAppVersion(String deviceID) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("appName", "醫聯網");
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/AppVersion", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);
            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return AppVersion.parse(state);
            }
        } catch (Exception ignored) {
        }
        return new AppVersion();
    }

    /**
     * 取得應用程式更新資訊
     *
     * @param version version
     * @return obj
     */
    public static JSONObject executeAppVersion2(String version) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();
            params.put("device", "2");
            params.put("version", version);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s", CONNECT_API_URL, "MedicalApi/AppVersion"));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(true);
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JSONObject state = new JSONObject(response.toString());
                return state;
            }
        } catch (Exception ignored) {
        }
        return new JSONObject();
    }

    /**
     * 取得診間清單
     *
     * @param deviceID DeviceID
     * @param culture  Culture
     * @return Map
     */
    public static RoomSet executeRoom(String deviceID, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/Room", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return Room.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new RoomSet(status);
    }

    /**
     * 取得檢查項目清單
     *
     * @param deviceID DeviceID
     * @param culture  Culture
     * @return Map
     */
    public static ExamineItemSet executeExamineItem(String deviceID, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/ExamItems", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return ExamineItem.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new ExamineItemSet(status);
    }

    /**
     * 取得信標推撥的資訊
     *
     * @param deviceID DeviceID
     * @param sex      Sex
     * @param age      Age
     * @param examines Examines
     * @param culture  Culture
     * @return List
     */
    public static BeaconPushSet executeBeaconPush(String deviceID, String sex, String age, String examines, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("sex", sex);
            params.put("age", age);
            params.put("examItems", examines);
            params.put("deviceID", deviceID);
            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/Beacon2Push", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return BeaconPushSet.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new BeaconPushSet(status);
    }

    /**
     * 取得藍芽發送裝置清單
     *
     * @param deviceID DeviceID
     * @return obj
     */
    public static BeaconInfoSet executeBeaconInfo(String deviceID) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("neighbor", "true");
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/BeaconInfo", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return BeaconInfo.parse(state, status);
            }
        } catch (Exception ignored) {
        }
        return new BeaconInfoSet(status);
    }

    /**
     * 取得圖標資訊清單
     *
     * @param deviceID DeviceID
     * @param culture  Culture
     * @return obj
     */
    public static SpotInfoSet executeSpotInfo(String deviceID, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/SpotInfo", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return SpotInfo.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new SpotInfoSet(status);
    }

    /**
     * 驗證並取得今日健檢人員
     *
     * @param deviceID DeviceID
     * @param identity Identity
     * @return Customer
     */
    public static Customer executeCustomerContactCheck(String deviceID, String identity, CultureEnum culture) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", "0");
            params.put("identity", identity);
            params.put("reload", "true");
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/TodayExamine", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return Customer.parse(state, culture);
            }
        } catch (Exception ignored) {
        }
        return new Customer();
    }

    /**
     * 今日健檢人員登出
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @return T/F
     */
    public static boolean executeCustomerLogout(String deviceID, String contact) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CustomerAuth", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("DELETE");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 更新今日健檢人員的資料
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @param name     Name
     * @param email    Email
     * @param birthday Birthday
     * @param tel      TEL
     * @param mobile   Mobile
     * @return T/F
     */
    public static boolean executeCustomerContactUpdate(String deviceID, String contact, String name, String email, String birthday, String tel, String mobile) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("name", name);
            params.put("email", email);
            params.put("birthday", birthday);
            params.put("tel", tel);
            params.put("mobile", mobile);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/TodayExamine", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 取得今日健檢人員檢查項目更新
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @param culture  Culture
     * @return List
     */
    public static CustomerReportExamineSet executeCustomerExamineUpdate(String deviceID, String contact, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);
            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/TodayExamine", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("PATCH");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return CustomerReportExamine.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new CustomerReportExamineSet(status);
    }

    /**
     * 客戶從行動裝置登入
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @return bool
     */
    public static boolean executeCustomerLogon(String deviceID, String contact) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CustomerAuth", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 取得客戶的贈品
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @return Gifts
     */
    public static String executeCustomerGifts(String deviceID, String contact) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CustomerAuth", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("PUT");
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return Customer.parseGifts(state);
            }
        } catch (Exception ignored) {
        }
        return "";
    }

    /**
     * 客戶目前位置變更通知
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @param beaconId BeaconID
     * @param posX     PositionX
     * @param posY     PositionY
     * @return T/F
     */
    public static boolean executeCustomerInPosition(String deviceID, String contact, String beaconId, String posX, String posY) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("beaconID", beaconId);
            params.put("posX", posX);
            params.put("posY", posY);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CustomerPosition", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 取得今日的檢查結果
     *
     * @param deviceID  DeviceID
     * @param contact   Contact
     * @param systemSet Healthy system
     * @return List
     */
    public static CheckupResultSet executeCustomerCheckupResult(String deviceID, String contact, HealthySystemSet systemSet) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CheckupResult", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return CheckupResult.parse(state, status, systemSet);
            }
        } catch (Exception ignored) {
        }
        return new CheckupResultSet(status);
    }

    /**
     * 紀錄客戶的歷程軌跡
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @param content  Content
     * @return T/F
     */
    public static boolean executeCustomerTrack(String deviceID, String contact, String content) {
        //參數驗證
        if (content == null || content.length() == 0)
            return false;
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("contents", content);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CustomerLog", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 客戶換衣完成開始排程
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @return bool
     */
    public static boolean executeStartSchedule(String deviceID, String contact) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/Schedule", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            return status == HttpsURLConnection.HTTP_OK;
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 等待停留時間完成繼續排程
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @return T/F
     */
    public static boolean executeRemainTimesFinish(String deviceID, String contact) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/Schedule", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("PATCH");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            return status == HttpsURLConnection.HTTP_OK;
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 取得加選項目清單
     *
     * @param deviceID DeviceID
     * @param culture  Culture
     * @return obj
     */
    public static AdditionItemSet executeAdditionItem(String deviceID, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/AdditionItems", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return AdditionItem.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new AdditionItemSet(status);
    }

    /**
     * 取得已加選的檢查項目
     *
     * @param deviceID      DeviceID
     * @param contact       Contact
     * @param additionItems AdditionItems
     * @return CheckableItemSet
     */
    public static CheckableItemSet executeCustomerAdditionData(String deviceID, String contact, AdditionItemSet additionItems) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CustomerAddition", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                return CheckableItem.parse(getStringFromInputStream(is), additionItems);
            }
        } catch (Exception ignored) {
        }
        return new CheckableItemSet();
    }

    /**
     * 新增已選擇的加選項目
     *
     * @param deviceID  DeviceID
     * @param contact   Contact
     * @param additions Addition
     * @return T/F
     */
    public static boolean executeCustomerAdditionSelected(String deviceID, String contact, String additions) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("ids", additions);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CustomerAddition", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 客戶滿意度調查評分回覆
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @param content  Content
     * @param people   People
     * @param suggest  Suggest
     * @return T/F
     */
    public static boolean executeCustomerSurveyScore(String deviceID, String contact, String content, String people, String suggest) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("content", content);
            params.put("people", people);
            params.put("suggest", suggest);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/Survey", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 取得滿意度調查項目清單
     *
     * @param deviceID DeviceID
     * @param culture  Culture
     * @return List
     */
    public static SurveyItemSet executeSurveyItem(String deviceID, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/Survey", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return SurveyItem.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new SurveyItemSet(status);
    }

    /**
     * 取得健檢系統清單
     *
     * @param deviceID DeviceID
     * @param culture  Culture
     * @return List
     */
    public static HealthySystemSet executeHealthySystem(String deviceID, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/HealthySystemType", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return HealthySystem.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new HealthySystemSet(status);
    }

    /**
     * 取得健康小百科資訊
     *
     * @param deviceID DeviceID
     * @param culture  Culture
     * @param gender   Gender
     * @return List
     */
    public static EncyclopediaSet executeEncyclopedia(String deviceID, CultureEnum culture, char gender) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("sex", String.valueOf(gender));
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/HealthyEncyclopediaData", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);
            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return Encyclopedia.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new EncyclopediaSet(status);
    }

    /**
     * 請求提供協助
     *
     * @param deviceID Device
     * @param contact  Contact
     * @return T/F
     */
    public static boolean executeHelpProvider(String deviceID, String contact) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/CustomerHelp", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 傳送執行長信箱
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @param subject  Subject
     * @param content  Content
     * @return T/F
     */
    public static boolean executeSendMailbox(String deviceID, String contact, String subject, String content) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("subject", subject);
            params.put("content", content);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/Feedback", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 取得特殊需求清單資料
     *
     * @param deviceID DeviceID
     * @param culture  Culture
     * @return List
     */
    public static SpecialDemandSet executeSpecialDemandData(String deviceID, CultureEnum culture) {
        int status = 0;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/SpecialNeeds", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return SpecialDemand.parse(state, status, culture);
            }
        } catch (Exception ignored) {
        }
        return new SpecialDemandSet(status);
    }

    /**
     * 取得已選取的特殊需求清單
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @return Selected
     */
    public static ArrayList<String> executeSpecialDemandSelected(String deviceID, String contact) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/SpecialNeeds", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("PATCH");
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return SpecialDemand.parse(state);
            }
        } catch (Exception ignored) {
        }
        return new ArrayList<>();
    }

    /**
     * 更新特殊需求清單
     *
     * @param deviceID DeviceID
     * @param contact  Contact
     * @param names    Name
     * @param confirm  Confirm
     * @param cancel   Cancel
     * @return T/F
     */
    public static boolean executeSpecialDemandUpdate(String deviceID, String contact, String names, String confirm, String cancel) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("contact", contact);
            params.put("names", names);
            params.put("confirm", confirm);
            params.put("cancel", cancel);
            params.put("deviceID", deviceID);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/SpecialNeeds", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * 註冊token
     *
     * @return Message
     */
    public static boolean registerDevice(String contactCode, String englishVersion) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("organizationID", organizationID.toString());
            params.put("pushToken", CommonFunc.pushToken);
            params.put("contactCode", contactCode);
            params.put("englishVersion", englishVersion);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s?%s", CONNECT_URL, "api/PushNotification/RegisterDevice", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return state.equals("success");
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return false;
    }

    /**
     * 記錄使用者推播基本資料
     *
     * @return Message
     */
    public static JSONObject RegisterPushNotificationToken(String name, String age, String gender, String phoneNumber, String englishVersion) {
        int status;
        HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();
            params.put("organization", CommonFunc.organization);
            params.put("name", name);
            params.put("age", age);
            params.put("gender", gender);
            params.put("phoneNumber", phoneNumber);
            params.put("englishVersion", englishVersion); //0.中文 1.英文
            params.put("pushToken", CommonFunc.pushToken);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s", CONNECT_API_URL, "MedicalApi/RegisterPushNotificationToken"));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(true);
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JSONObject state = new JSONObject(response.toString());
                return state;
            }

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return new JSONObject();
    }

    /**
     * yahoo oauth get token
     *
     * @return obj
     */
    public static JSONObject oauth2GetToken(String code) {
        int status;
        HttpURLConnection conn;
        try {
            String url = "https://api.login.yahoo.com/";

            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("grant_type", "authorization_code");
            params.put("redirect_uri", "https://www.med-net.com");
            params.put("code", code);

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s", url, "oauth2/get_token"));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic ZGoweUptazlOVWhVTjNGMVRIZEllVmsxSm1ROVdWZHJPVnBFVm5oaGJGcFdUWHBaYldOSGJ6bE5RUzB0Sm5NOVkyOXVjM1Z0WlhKelpXTnlaWFFtZUQxbU53LS06MmZjYTMxNjVjYzhhOGYwMzNjNmY5NWIyNDFlZDhlYTEzMzYwZTFkZg==");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);

            //帶參數 參數是用form格式傳送 但要帶在body raw
            conn.setDoInput(true);
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
            os.writeBytes(getQuery(params));
            os.flush();
            os.close();

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return new JSONObject(state);
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return new JSONObject();
    }

    /**
     * yahoo oauth get profile
     *
     * @return obj
     */
    public static JSONObject oauth2GetProfile(String token) {
        int status;
        HttpURLConnection conn;
        try {
            String url = "https://social.yahooapis.com/";

            //建立查詢參數
            HashMap<String, String> params = new HashMap<>();
            params.put("format", "json");

            //建立HTTP連線
            URL mURL = new URL(String.format(Locale.getDefault(), "%s%s%s", url, "v1/user/me/profile?", getQuery(params)));
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(false);
            Log.d("debug", "oauth2GetProfile: " + mURL.toString());
            //Log.i(TAG, "oauth2GetProfile: " + mURL.toString());

            status = conn.getResponseCode();
            if (status == HttpsURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                return new JSONObject(state);
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return new JSONObject();
    }

    private static String getQuery(JSONObject params) throws Exception {
        return getQuery(CommonFunc.jsonToMap(params));
    }

    /**
     * 取得查詢字串
     *
     * @param params Params
     * @return String
     * @throws UnsupportedEncodingException Exception
     */
    public static String getQuery(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, String> pair : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    /**
     * 取得查詢字串
     *
     * @param params Params
     * @return String
     * @throws UnsupportedEncodingException Exception
     */
    public static String getQueryJson(JSONObject params) throws Exception {

        Iterator<String> keys = params.keys();
        StringBuilder result = new StringBuilder();
        boolean first = true;

        while (keys.hasNext()) {
            String key = keys.next();
            if (params.get(key) instanceof JSONObject) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(params.optString(key), "UTF-8"));
            }
        }

        return result.toString();
    }

    /**
     * 取得Http回傳的字串
     *
     * @param inputStream Input stream
     * @return String
     * @throws IOException Exception
     */
    private static String getStringFromInputStream(InputStream inputStream) throws IOException {
        String tempStr;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder(1024);
        while ((tempStr = bufferedReader.readLine()) != null) {
            stringBuilder.append(tempStr);
        }
        bufferedReader.close();
        inputStream.close();
        return stringBuilder.toString();
        /*
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = is.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }
        is.close();

        String state = os.toString();
        os.close();
        return state;
        */
    }

    /**
     * 非同步httpclient post
     *
     * @param apiUrl    String
     * @param jsonParam JSONObject
     * @return JSONObject
     */
    public static JSONObject sendPost(String apiUrl, JSONObject jsonParam) {
        HttpURLConnection conn = null;
        try {
            Log.d("debug", "apiUrl = " + apiUrl);
            URL url = new URL(apiUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.write(jsonParam.toString().getBytes());
            os.flush();
            os.close();
            int status = conn.getResponseCode();
            Log.d("debug", "sendPost: "+jsonParam);
            //Log.i("debug", "status = " + status);

            JSONObject result = null;
            try {
                BufferedReader in;
                if (200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
                    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } else {
                    in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                }
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                result = new JSONObject(response.toString());

            } catch (Exception e) {
                Log.w("debug", "call api error e = " + e.getStackTrace());
                e.printStackTrace();
            }

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return null;
    }

    /**
     * 非同步httpclient get
     *
     * @param apiUrl    String
     * @param jsonParam JSONObject
     * @return JSONObject
     */
    public static JSONObject sendGet(String apiUrl, JSONObject jsonParam) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(String.format("%s?%s", apiUrl, getQuery(jsonParam)));
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(50000);
            conn.setConnectTimeout(50000);
            conn.setDoOutput(false);

            int status = conn.getResponseCode();
            JSONObject result = null;
            try {
                BufferedReader in;
                if (200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
                    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } else {
                    in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                }
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                result = new JSONObject(response.toString());

            } catch (Exception e) {
                Log.w("debug", "call api error e = " + e.getStackTrace());
                e.printStackTrace();
            }

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return null;
    }

    /**
     * 非同步httpclient post
     * 會員用需登入，所以會判斷access token
     *
     * @param apiUrl    String
     * @param jsonParam JSONObject
     * @return JSONObject
     */
    public static JSONObject sendCustomerPost(String apiUrl, JSONObject jsonParam) {
        HttpURLConnection conn = null;
        try {

            String Authorization = String.format("bearer %s", MegaApplication.getInstance().getMember().getAccessToken());
            URL url = new URL(apiUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization", Authorization);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            //os.writeBytes(jsonParam.toString());
            os.write(jsonParam.toString().getBytes());
            os.flush();
            os.close();

            Log.d("debug", "sendCustomerPost: " + jsonParam.toString());

            int status = conn.getResponseCode();
            JSONObject result = new JSONObject();
            try {
                BufferedReader in;
                if (200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
                    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } else if (401 == status) {
                    //not auth error
                    result.put("code", "401");
                    return result;
                } else {
                    in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                }
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                result = new JSONObject(response.toString());

            } catch (Exception e) {
                Log.w("debug", "call api error e = " + e.getStackTrace());
                e.printStackTrace();
            }

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return null;
    }

    /**
     * 非同步httpclient get
     * 會員用需登入，所以會判斷access token
     *
     * @param apiUrl    String
     * @param jsonParam JSONObject
     * @return JSONObject
     */
    public static JSONObject sendCustomerGet(String apiUrl, JSONObject jsonParam) {
        HttpURLConnection conn = null;
        try {
            String Authorization = String.format("bearer %s", MegaApplication.getInstance().getMember().getAccessToken());

            URL url = new URL(String.format("%s?%s", apiUrl, getQuery(jsonParam)));
            Log.i("debug","apiUrl = "+url.toString());
            //Log.i("debug","Authorization = "+Authorization);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", Authorization);
            conn.setReadTimeout(50000);
            conn.setConnectTimeout(50000);
            conn.setDoOutput(false);

            int status = conn.getResponseCode();
            JSONObject result = new JSONObject();
            try {
                BufferedReader in;
                if (200 <= status && status <= 299) {
                    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } else if (401 == status) {
                    //not auth error
                    result.put("code", "401");
                    return result;
                } else {
                    in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                }

                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                result = new JSONObject(response.toString());
                //Log.i("debug", "sendCustomerGet result = "+result);
            } catch (Exception e) {
                Log.w("debug", "call api error e = " + e.getStackTrace());
                e.printStackTrace();
            }

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return null;
    }

//    public static void getHealth(JSONObject result, Context context,) {
//        JSONArray healthList = new JSONArray();
//        int i = 0;
//        for (String healthType : MegaApplication.healthSortList) {
//            JSONObject item = new JSONObject();
//            JSONObject healthMap = MegaApplication.healthMap.get(healthType);
//            String typeName = healthMap.optString("healthTypeName");
//            if (result != null) {
//                if (result != null && !result.isNull("source") && !result.optJSONObject("source").isNull(typeName)) {
//                    item = result.optJSONObject("source").optJSONObject(typeName);
//                } else {
//                    item = new JSONObject();
//                }
//
//
//                //特別需要給初始資料，或是透過google fit新增資料的
//                String mTime = item.optString("m_time");
//                if (!"".equals(mTime)) {
//                    Calendar cal = Calendar.getInstance();
//                    cal.setTime(CommonFunc.UTC2Date(mTime));
//                    Calendar now = Calendar.getInstance();
//                    if ("4".equals(healthType)) {
//                        if (now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
//                            //取今天是今天的飲水量
//                            MegaApplication.TodayWaterCapacity = item.optInt("drinking_water_today");
//                            Log.i("debug", "取今天是今天的飲水量 = " + MegaApplication.TodayWaterCapacity);
//                        }
//                    }
//
//                    if ("1".equals(healthType)) {
//                        Log.i("debug", "item.optString(\"ac\") = " + item.optString("ac"));
//                        Log.i("debug", "item.optString(\"pc\") = " + item.optString("pc"));
//                        if (item.optDouble("ac") > 0 && item.optDouble("pc") > 0) {
//                            //飯前飯後都有職 要去抓最後一筆
//                            Calendar cal2 = Calendar.getInstance();
//                            cal2.setTime(new Date());
//                            int maxDay = cal2.getActualMaximum(Calendar.DAY_OF_MONTH);
//                            String startTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + cal2.get(Calendar.DATE) + "T00:00:00Z";
//                            String endTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + maxDay + "T23:59:59Z";
//                            Log.i("debug", "e04");
//                            new CustomerApiTask(context, (TaskPost2ExecuteImpl) context, false, context.getString(R.string.msg_data_loading)
//                                    , TaskEnum.MednetQueryMeasureValuesType1, true).execute(startTime, endTime);
//                        }
//                    } else if ("5".equals(healthType)) {
//                        if (item.optDouble("sleeping_hours") > 0 && now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
//                        } else {
//                            //Log.i("debug", "直接新增");
//                            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
//                                    , TaskEnum.MednetSleepingCreateOrUpdate, true).execute(MegaApplication.TodaySleepTime,
//                                    MegaApplication.TodayWakeUpTime, Double.toString(MegaApplication.TodaySleepHour), CommonFunc.getNowUTCDateString());
//                        }
//                    } else if ("6".equals(healthType)) {
//                        Log.i("debug", "healthType 步行");
//                        Calendar cal2 = Calendar.getInstance();
//                        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//
//                        cal2.setTime(new Date());
//                        if (item.optInt("step_count") > 0 && now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
//                            Log.i("debug", "今天的資料");
//                            //今天的資料
//                            if (item.optInt("step_count") != MegaApplication.TodayStepCount && MegaApplication.TodayStepCount != 0) {
//                                Log.i("debug", "更新");
//
//                                //先撈今天資料
//                                String startTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + "01T00:00:00Z";
//                                String endTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + maxDay + "T23:59:59Z";
//                                new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
//                                        , TaskEnum.MednetQueryMeasureValuesType6, true).execute(startTime, endTime);
//
//                            }
//                        } else {
//                            //Log.i("debug", "直接新增");
//                            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
//                                    , TaskEnum.MednetCreateStepCreateOrUpdate, true)
//                                    .execute(Integer.toString(MegaApplication.TodayStepCount), Double.toString(MegaApplication.TodayDistance),
//                                            Double.toString(MegaApplication.TodayCalories),
//                                            CommonFunc.getNowUTCDateString());
//                        }
//                    }
//                } else {
//                    //沒有資料
//                    if ("5".equals(healthType)) {
//                        if (!"".equals(MegaApplication.TodaySleepTime)) {
//                            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
//                                    , TaskEnum.MednetSleepingCreateOrUpdate, true).execute(MegaApplication.TodaySleepTime,
//                                    MegaApplication.TodayWakeUpTime, Double.toString(MegaApplication.TodaySleepHour), CommonFunc.getNowUTCDateString());
//                        }
//
//                    } else if ("6".equals(healthType)) {
//                        Log.i("debug", "healthType 步行");
//                        if (MegaApplication.TodayStepCount > 0) {
//                            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
//                                    , TaskEnum.MednetCreateStepCreateOrUpdate, true)
//                                    .execute(Integer.toString(MegaApplication.TodayStepCount), Double.toString(MegaApplication.TodayDistance),
//                                            Double.toString(MegaApplication.TodayCalories),
//                                            CommonFunc.getNowUTCDateString());
//                        }
//                    }
//                }
//            }
//
//            if (!"".equals(item.optString("bh"))) {
//                MegaApplication.getInstance().BodyHeight = item.optString("bh");
//            }
//
//            item.put("healthTypeName", healthMap.optString("healthName"));
//            item.put("healthType", healthType);
//            item.put("healthTypeKey", typeName);
//            item.put("healthImg", healthMap.optString("healthImg"));
//            item.put("healthIcon", healthMap.optString("healthRecordIcon"));
//            item.put("index", i);
//            //Log.i("debug", "item  = " + item);
//            healthList.put(item);
//            i++;
//        }
//
//        MegaApplication.healthList = healthList;
//
//    }


}