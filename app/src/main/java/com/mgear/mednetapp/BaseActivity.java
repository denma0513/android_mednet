package com.mgear.mednetapp;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.service.Common;
import com.mgear.mednetapp.entity.MemberSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.WebView2Fragment;
import com.mgear.mednetapp.fragments.WebViewFragment;
import com.mgear.mednetapp.fragments.WebViewUploadFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class BaseActivity extends AppCompatActivity {
    private Fragment fragmentMain;
    protected WebViewFragment webFragment;
    protected WebViewUploadFragment webUploadFragment;
    protected WebView2Fragment webFragment2;
    protected DisplayMetrics displayMetrics = new DisplayMetrics();
    protected int screenHeight;
    protected int screenWidth;
    private ProgressDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;
        mDialog = new ProgressDialog(this);

        SharedPreferences firstSettings = getSharedPreferences("static", 0);
        boolean firstTime = firstSettings.getBoolean("firstTime", true);
        MegaApplication.IsFirstTime = firstTime;


        SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
        String accessToken = settings.getString("accessToken", "");
        String refreshToken = settings.getString("refreshToken", "");
        String name = settings.getString("name", "");
        String firstName = settings.getString("firstName", "");
        String email = settings.getString("email", "");
        String profileImage = settings.getString("profileImage", "");

        String authType = settings.getString("authType", "");
        String drID = settings.getString("drID", "");
        String doctorArticleCount = settings.getString("doctorArticleCount", "");
        String unpromotedArticleCount = settings.getString("unpromotedArticleCount", "");


        //有儲存access token
        if (accessToken != null && !"".equals(accessToken)) {
            //當MegaApplication裡面沒有資料或是與儲存的token不同 則同步線上資料下來
            if (!MegaApplication.getInstance().getMember().getAccessToken().equals(accessToken)) {
                MegaApplication.getInstance().getMember().setAccessToken(accessToken);
                //MegaApplication.getInstance().getMember().setRole("1");//測試用
            }
        }

        //有儲存refresh token
        if (refreshToken != null && !"".equals(refreshToken)) {
            if (!MegaApplication.getInstance().getMember().getRefreshToken().equals(refreshToken)) {
                MegaApplication.getInstance().getMember().setRefreshToken(refreshToken);
            }
        }

        if (name != null && !"".equals(name)) {
            MegaApplication.getInstance().getMember().setName(name);
        }
        if (email != null && !"".equals(email)) {
            MegaApplication.getInstance().getMember().setEmail(email);
        }
        if (profileImage != null && !"".equals(profileImage)) {
            MegaApplication.getInstance().getMember().setProfileImage(profileImage);
        }
        if (firstName != null && !"".equals(firstName)) {
            MegaApplication.getInstance().getMember().setFirstName(firstName);
        }

        if (authType != null && !"".equals(authType)) {
            MegaApplication.getInstance().getMember().setAuthType(authType);
        }
        if (drID != null && !"".equals(drID)) {
            MegaApplication.getInstance().getMember().setDrID(drID);
        }
        if (doctorArticleCount != null && !"".equals(doctorArticleCount)) {
            MegaApplication.getInstance().getMember().setDoctorArticleCount(doctorArticleCount);
        }
        if (unpromotedArticleCount != null && !"".equals(unpromotedArticleCount)) {
            MegaApplication.getInstance().getMember().setUnpromotedArticleCount(unpromotedArticleCount);
        }

        settings = getSharedPreferences("HealthSettingNotice", Activity.MODE_PRIVATE);
        MegaApplication.BodyHeight = settings.getString("BodyHeight", "");
        MegaApplication.TargetStep = settings.getInt("TargetStep", MegaApplication.TargetStep);
        MegaApplication.WaterCapacity = settings.getInt("WaterCapacity", MegaApplication.WaterCapacity);

        String[] list = settings.getString("healthTypeList", "").split(",");//健康排序
        if (list != null && list.length == 7) {
            MegaApplication.healthSortList = list;
        } else {
            MegaApplication.healthSortList = MegaApplication.healthTypeList;
        }
        //MegaApplication.healthRecord = CommonFunc.parseJSON(settings.getString("healthRecord", "{}"));

    }

    private GoogleSignInOptions googleSignInOptions;
    private GoogleSignInClient mGoogleSignInClient;

    public void clearUserData() {
        SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
        settings.edit().clear().apply();
        MegaApplication.clearMember();
        MegaApplication.WebLogin = false;
        MegaApplication.shoppingCart = "0";
        MegaApplication.bell = "0";
        MegaApplication.reservation = "0";

        String server_client_id = getString(R.string.google_client_id);

        googleSignInOptions = new GoogleSignInOptions.Builder()
                .requestIdToken(server_client_id)
                .requestServerAuthCode(server_client_id)
                .requestPhatIdToken(server_client_id)
                .requestEmail().build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        mGoogleSignInClient.signOut();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            //Log.d("debug", "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            Log.d("debug", "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(this);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }

    }

    public void triggerAction(Context context, String action, String target, int view) {
        FragmentManager fragmentManager = getFragmentManager();
        Bundle bundle = new Bundle();
        Intent intent = new Intent();
        switch (action) {
            case MegaApplication.ActionTypeWebView:
                try {
                    for (String url : MegaApplication.needLoginUrlArray) {
                        //Log.i("debug","url = "+url+" target = "+target);
                        if (target.contains(url) && ("".equals(MegaApplication.getInstance().getMember().getAccessToken()))) {
                            //該登入沒登入
                            JSONObject loginTarget = new JSONObject();
                            try {
                                loginTarget.put("action", action);
                                loginTarget.put("target", target);
                                triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return;
                        }

                    }
                    if (target.contains("aidoctor.med-net.com/AIDoctor")) {
                        webUploadFragment = new WebViewUploadFragment();
                        webUploadFragment.setUrl(target);
                        webUploadFragment.setContext(this);
                        fragmentMain = webUploadFragment;
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        ft.add(view, fragmentMain, target);
                        ft.addToBackStack(target);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                    }else if (target.contains("AIDoctor")) {
                        webFragment = new WebViewFragment();
                        webFragment.setUrl("webview", target);
                        webFragment.setContext(this);
                        fragmentMain = webFragment;
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        ft.add(view, fragmentMain, target);
                        ft.addToBackStack(target);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                    } else if (target.contains("Member")) {
                        webUploadFragment = new WebViewUploadFragment();
                        webUploadFragment.setUrl(target);
                        webUploadFragment.setContext(this);
                        fragmentMain = webUploadFragment;
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        ft.add(view, fragmentMain, target);
                        ft.addToBackStack(target);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                    } else {
                        webUploadFragment = new WebViewUploadFragment();
                        webUploadFragment.setUrl(target);
                        webUploadFragment.setContext(this);
                        fragmentMain = webUploadFragment;
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        ft.add(view, fragmentMain, target);
                        ft.addToBackStack(target);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
            case MegaApplication.ActionTypeOutWebView:
                intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(target));
                startActivity(intent);
                break;
            case MegaApplication.ActionTypeMainPage:

                intent = new Intent(this, MainActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
            case MegaApplication.ActionTypeHealthPush:
                if (MegaApplication.getInstance().getMember().isNullAccessToken()) {
                    JSONObject loginTarget = new JSONObject();
                    try {
                        loginTarget.put("action", action);
                        loginTarget.put("target", target);
                        triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                intent = new Intent(context, MainActivity.class);
                bundle = new Bundle();
                bundle.putString("page", "healthList");
                intent.putExtras(bundle);
                startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeHealthPush));
                break;
            case MegaApplication.ActionTypeHealthPushCreate:
                if (MegaApplication.getInstance().getMember().isNullAccessToken()) {
                    JSONObject loginTarget = new JSONObject();
                    try {
                        loginTarget.put("action", action);
                        loginTarget.put("target", target);
                        triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                JSONObject obj = CommonFunc.parseJSON(target);
                if ("6".equals(obj.optString("healthType"))) {
                    triggerAction(context, MegaApplication.ActionTypeHealthPush, "", view);
                } else {
                    intent = new Intent(context, HealthPushCreateActivity.class);
                    bundle = new Bundle();
                    //Log.i("debug", "obj = " + obj);
                    bundle.putInt("index", obj.optInt("index"));
                    bundle.putString("healthType", obj.optString("healthType"));
                    bundle.putString("data", obj.toString());
                    intent.putExtras(bundle);
                    this.startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeHealthPushCreate));
                }

                break;
            case MegaApplication.ActionTypeHealthPushItemSort:
                intent = new Intent(context, HealthPushSettingActivity.class);
                bundle = new Bundle();
                bundle.putString("settingType", "sort");
                intent.putExtras(bundle);
                startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeHealthPushItemSort));
                break;
            case MegaApplication.ActionTypeStepCompetition:
                intent = new Intent(this, StepCompetitionActivity.class);
                bundle = new Bundle();

//                String guid = extras.getString("guid");
//                isAllowFriend = extras.getString("IsAllowFriend");
//                AddFriendsId = extras.getString("AddFriendsId");

                intent.putExtras(bundle);
                startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeStepCompetition));
                break;
            case MegaApplication.ActionTypeLogin:
                intent = new Intent(context, UserLoginActivity.class);
                bundle = new Bundle();
                JSONObject callback = CommonFunc.parseJSON(target);
                Log.d("debug", "導登入 前一個acrion 是:" + callback.optString("action") + " target是: " + callback.optString("target"));
                bundle.putString("action", callback.optString("action"));
                bundle.putString("target", callback.optString("target"));
                intent.putExtras(bundle);
                startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));
                break;
            case MegaApplication.ActionTypeLogout:
                MegaApplication.IsFitFirstTime = true;
                clearUserData();
                intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public void showLoading(String message) {
        if (CommonFunc.isBlank(message))
            message = "loading...";
        mDialog.setMessage(message);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
    }

    public void hideLoading() {
        if (mDialog != null)
            mDialog.dismiss();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
