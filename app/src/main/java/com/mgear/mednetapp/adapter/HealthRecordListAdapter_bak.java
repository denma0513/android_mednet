package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.WebViewBaseFragment;
import com.mgear.mednetapp.interfaces.INativeActionImpl;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Jye on 2017/6/15.
 * class: HomePagerAdapter
 */
public class HealthRecordListAdapter_bak extends BaseExpandableListAdapter {
    private final Context mContext;
    private final INativeActionImpl mINativeAction;

    private ArrayList<WebViewBaseFragment> menuFragmantList;
    private JSONArray healthList;
    private ArrayList<String> monthTitle;


    public HealthRecordListAdapter_bak(Context context) {
        mContext = context;
        mINativeAction = (INativeActionImpl) context;
    }

    public void setAdapterList(JSONArray list, ArrayList<String> monthList) {
        this.healthList = list;
        this.monthTitle = monthList;
    }

    @Override
    public int getGroupCount() {
        if (healthList != null) {
            return healthList.length();
        } else {
            return 0;
        }

    }

    @Override
    public int getChildrenCount(int i) {
        Log.i("debug", "healthList = " + healthList.optJSONArray(i));
        if (healthList != null && healthList.optJSONArray(i) != null) {
            Log.i("debug", "healthList = " + healthList.optJSONArray(i));
            return healthList.optJSONArray(i).length();
        } else {
            return 0;
        }

    }

    @Override
    public Object getGroup(int i) {
        return healthList.optJSONArray(i);
    }

    @Override
    public Object getChild(int i, int j) {
        return healthList.optJSONArray(i).optJSONObject(j);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int j) {
        return j;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.cell_group, null);

        TextView title = convertView.findViewById(R.id.group_title);

        if (monthTitle != null && monthTitle.size() >= (i + 1))
            title.setText(monthTitle.get(i));
        return convertView;
    }

    @Override
    public View getChildView(int i, int j, boolean b, View convertView, ViewGroup viewGroup) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.health_record_cell_child, null);


//        MenuHandle item = (MenuHandle) menuList.get(i).get(j);
//        childTitle.setText(item.getTitle());
//
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //Log.i("DrawerMenuAdapter", "i = " + i + ", j = " + j + " item = " + item.getTitle());
//                mINativeAction.triggerAction(item.action, item.target);
//            }
//        });
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}