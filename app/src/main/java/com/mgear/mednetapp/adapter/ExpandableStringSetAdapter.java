package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;


import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.StringStringSet;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Jye on 2017/8/23.
 * class: ExpandableStringSetAdapter
 */
public class ExpandableStringSetAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<StringStringSet>> expandableListDetail;

    public ExpandableStringSetAdapter(Context context, List<String> expandableListTitle, HashMap<String, List<StringStringSet>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final StringStringSet expandedListText = (StringStringSet) getChild(listPosition, expandedListPosition);
        TextView hold;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.expandable_list_item, null);
            hold = (TextView) convertView.findViewById(R.id.expandedListItem);
            convertView.setTag(hold);
        }
        else hold = (TextView) convertView.getTag();
        hold.setText(expandedListText.getMajor());
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        TextView hold;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.expandable_list_group, null);
            hold = (TextView) convertView.findViewById(R.id.listTitle);
            convertView.setTag(hold);
        }
        else hold = (TextView) convertView.getTag();
        hold.setTypeface(null, Typeface.BOLD);
        hold.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}