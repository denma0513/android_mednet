package com.mgear.mednetapp.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mgear.mednetapp.HealthPushSettingActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.HomePageViewSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.HomePageFragment;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.IRefreshHeader;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.NewDownloadImageTask;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//HomePageViewAdapter.RefreshViewHolder
public class HomePageViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener,
        TaskPost2ExecuteImpl {
    public static final int TYPE_HEADER = 0;  //有Header
    public static final int TYPE_SEARCH = -2;  //有Header
    public static final int TYPE_FOOTER = -1;  //有Footer的

    private Context mContext;
    private IRefreshHeader mRefreshHeader;
    private INativeActionImpl mINativeActionIpml;
    private LayoutInflater mInflater;
    private JSONArray mHomeList = new JSONArray();
    //private JSONArray mHealthList = new JSONArray();
    private HashMap<Integer, HomePageViewSet> viewData = new HashMap<Integer, HomePageViewSet>();
    private int viewWidth;

    private int[] mLayoutList;
    private int[][] mViewIdList;

    private HomePageFragment homePageFragment;


    public HomePageViewAdapter(Context context, int[] layoutList, int[][] viewIdList) {
        this.mContext = context;
        this.mINativeActionIpml = (INativeActionImpl) context;
        this.mInflater = LayoutInflater.from(context);
        this.mLayoutList = layoutList;
        this.mViewIdList = viewIdList;
    }

    public void setHomePageFragmentContext(HomePageFragment context) {
        homePageFragment = context;
    }

    public void setAdapterList(JSONArray list, JSONArray healthList) {
        this.mHomeList = list;
        //this.mHealthList = healthList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.main_cell, parent, false);
        viewWidth = parent.getWidth();
        if (viewType == TYPE_HEADER) {
            return new HeaderViewHolder(mRefreshHeader.getHeaderView());
        } else if (viewType == TYPE_SEARCH) {
            view = mInflater.inflate(R.layout.home_viewtype_search, parent, false);
            int width = (viewWidth / 1) - 10;
            SearchViewHolder searchViewHolder = new SearchViewHolder(view);
            //searchViewHolder.searchBar.getLayoutParams().width = width;
            searchViewHolder.wathchMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mINativeActionIpml.triggerAction(MegaApplication.ActionTypeHealthPush, "");
                }
            });

            searchViewHolder.drWathchMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = String.format("https://expert.med-net.com/doctorsdetail/%s/3", MegaApplication.getInstance().getMember().getDrID());
                    mINativeActionIpml.triggerAction(MegaApplication.ActionTypeWebView, url);
                }
            });
            searchViewHolder.search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    Log.i("debug", "searchViewHolder.search = " + searchViewHolder.search.getText());
                    if (!"".equals(searchViewHolder.search.getText().toString())) {

                        if (i == EditorInfo.IME_ACTION_SEND || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                            switch (keyEvent.getAction()) {
                                case KeyEvent.ACTION_UP: {
                                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                                    mINativeActionIpml.triggerAction(MegaApplication.ActionTypeWebView,
                                            String.format("https://med-net.com/Search?keyword=%s", searchViewHolder.search.getText().toString()));
                                    new CommonApiTask(mContext, HomePageViewAdapter.this, false, "", TaskEnum.MednetAppLogEven)
                                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "search", searchViewHolder.search.getText().toString());
                                    return true;
                                }
                                default:
                                    return true;
                            }
                        }


                    }
                    return false;
                }
            });

            return searchViewHolder;
        } else {
            RecyclerView.ViewHolder viewHolder;
            int index = viewType - 1;
            //JSONArray viewList = mHomeList.optJSONObject(index).optJSONArray("viewList");
            JSONArray viewList = getViewDataListByType(Integer.toString(viewType));
            //String type = mHomeList.optJSONObject(index).optString("type");
            switch (Integer.toString(viewType)) {
                case "1":
                    view = mInflater.inflate(R.layout.home_viewtype1, parent, false);
                    //viewHolder = new ViewType1Holder(view);
                    int width = (viewWidth / 3);
                    int height = (int) ((width / 211.0 * 169.0) + 20);
                    ViewType1Holder viewHolder1 = new ViewType1Holder(view);
                    for (int i = 0; i < viewList.length(); i++) {
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                width, height);
                        viewHolder1.imgList.get(i).setLayoutParams(layoutParams);
                        //viewHolder1.imgList.get(i).getLayoutParams().width = width;
                        viewHolder1.viewList.get(i).setOnClickListener(this::onClick);
                    }
                    return viewHolder1;
                case "2":
                    view = mInflater.inflate(R.layout.home_viewtype2, parent, false);
                    width = (viewWidth / 2);
                    height = (int) ((width / 320.0 * 120.0) + 20);
                    ViewType2Holder viewHolder2 = new ViewType2Holder(view);
                    for (int i = 0; i < viewList.length(); i++) {
                        //設定圖片寬高
                        RelativeLayout.LayoutParams linLayoutParams = new RelativeLayout.LayoutParams(
                                width, height);
                        viewHolder2.imgList.get(i).setLayoutParams(linLayoutParams);
                        viewHolder2.viewList.get(i).setOnClickListener(this::onClick);
                    }
                    return viewHolder2;
                case "3":
                    view = mInflater.inflate(R.layout.home_viewtype3, parent, false);
                    //width = (viewWidth / 2) - 30;
                    ViewType3Holder viewHolder3 = new ViewType3Holder(view);
                    for (int i = 0; i < viewList.length(); i++) {
                        viewHolder3.viewList.get(i).setOnClickListener(this::onClick);
                        //viewHolder3.viewList.get(i).getLayoutParams().width = width;
                    }
                    return viewHolder3;
                case "4":
                    view = mInflater.inflate(R.layout.home_viewtype4_list, parent, false);
                    ViewType4Holder viewHolder4 = new ViewType4Holder(view);
                    return viewHolder4;
                case "5":
                    view = mInflater.inflate(R.layout.home_viewtype_banner, parent, false);
                    ViewTypeBannerHolder viewHolder5 = new ViewTypeBannerHolder(view);
                    return viewHolder5;
                case "6":
                    view = mInflater.inflate(R.layout.home_viewtype_banner, parent, false);
                    ViewTypeBannerHolder viewHolder6 = new ViewTypeBannerHolder(view);
                    return viewHolder6;
            }
        }
        return new HeaderViewHolder(view);
    }

    public JSONArray getViewDataListByType(String type) {
        JSONArray dataList = null;
        for (int i = 0; i < mHomeList.length(); i++) {
            if (type.equals(mHomeList.optJSONObject(i).optString("type"))) {
                dataList = mHomeList.optJSONObject(i).optJSONArray("viewList");
            }
        }
        return dataList;
    }

    public JSONObject getViewDataByType(String type) {
        JSONObject data = null;
        for (int i = 0; i < mHomeList.length(); i++) {
            if (type.equals(mHomeList.optJSONObject(i).optString("type"))) {
                data = mHomeList.optJSONObject(i);
            }
        }
        return data;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Integer index = position - 2;
        if (getItemViewType(position) == TYPE_HEADER) {
            return;
        } else if (getItemViewType(position) == TYPE_SEARCH) {

            SearchViewHolder viewHolder = (SearchViewHolder) holder;
            HomeHealthGridAdapter madAdapter = new HomeHealthGridAdapter(mContext, MegaApplication.healthList);
            viewHolder.healthl.setAdapter(madAdapter);
            //JSONObject hello = CommonFunc.getHelloWord();
            Log.i("debug", "MegaApplication.getInstance().getMember().getFirstName() " + MegaApplication.getInstance().getMember().getFirstName());
            if (!CommonFunc.isBlank(MegaApplication.getInstance().getMember().getFirstName())) {
                viewHolder.helloTitle.setText(MegaApplication.getInstance().getMember().getFirstName() + " " + MegaApplication.helloTitle);
            } else if (!CommonFunc.isBlank(MegaApplication.getInstance().getMember().getName())) {
                viewHolder.helloTitle.setText(MegaApplication.getInstance().getMember().getName() + " " + MegaApplication.helloTitle);
            } else {
                viewHolder.helloTitle.setText("健康新手 " + MegaApplication.helloTitle);
            }

            Log.d("debug", "MegaApplication.getInstance().getMember().getRole(): " + MegaApplication.getInstance().getMember().getAuthType());
            if ("1".equals(MegaApplication.getInstance().getMember().getAuthType())) {
                viewHolder.doctorTitle.setVisibility(View.VISIBLE);
                viewHolder.doctorLayout.setVisibility(View.VISIBLE);
                viewHolder.doctorArticleCount.setText(MegaApplication.getInstance().getMember().getDoctorArticleCount());
                viewHolder.unpromotedArticleCount.setText(MegaApplication.getInstance().getMember().getUnpromotedArticleCount());
            } else {
                viewHolder.doctorTitle.setVisibility(View.GONE);
                viewHolder.doctorLayout.setVisibility(View.GONE);
            }

            viewHolder.helloContent.setText(MegaApplication.helloContent);
            viewHolder.healthReorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mINativeActionIpml.triggerAction(MegaApplication.ActionTypeHealthPushItemSort, "");
                }
            });
            return;
        } else {
            //JSONArray viewList = mHomeList.optJSONObject(index).optJSONArray("viewList");
            //String type = mHomeList.optJSONObject(index).optString("type");
            JSONArray viewList = null;
            if (holder instanceof ViewType1Holder) {
                viewList = getViewDataListByType("1");
                for (int i = 0; i < ((ViewType1Holder) holder).viewList.size(); i++) {
                    JSONObject data = viewList.optJSONObject(i);
                    new NewDownloadImageTask(((ViewType1Holder) holder).imgList.get(i)).executeOnExecutor(MegaApplication.threadPoolExecutor, data.optString("viewUrl"));
                }
                return;
            } else if (holder instanceof ViewType2Holder) {
                viewList = getViewDataListByType("2");
                for (int i = 0; i < ((ViewType2Holder) holder).viewList.size(); i++) {
                    JSONObject data = viewList.optJSONObject(i);
                    new NewDownloadImageTask(((ViewType2Holder) holder).imgList.get(i)).executeOnExecutor(MegaApplication.threadPoolExecutor, data.optString("viewUrl"));
//                    ((ViewType2Holder) holder).titleList.get(i).setText(data.optString("title"));
                }
            } else if (holder instanceof ViewType3Holder) {
                viewList = getViewDataListByType("3");
                for (int i = 0; i < ((ViewType3Holder) holder).viewList.size(); i++) {
                    JSONObject data = viewList.optJSONObject(i);
                    new NewDownloadImageTask(((ViewType3Holder) holder).imgList.get(i)).executeOnExecutor(MegaApplication.threadPoolExecutor, data.optString("viewUrl"));
                    ((ViewType3Holder) holder).titleList.get(i).setText(data.optString("title"));
                }
            } else if (holder instanceof ViewType4Holder) {
                viewList = getViewDataListByType("4");
                String type = mHomeList.optJSONObject(index).optString("type");
                JSONObject viewData = getViewDataByType(type);

                ListAdapter adapter = new ListAdapter(viewList);
                ((ViewType4Holder) holder).title.setText(viewData.optString("headerTitle"));
                ((ViewType4Holder) holder).watchMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mINativeActionIpml.triggerAction(Integer.toString(viewData.optJSONObject("headerActionData").optInt("actionType")),
                                viewData.optJSONObject("headerActionData").optString("actionTarget"));
                    }
                });

                ((ViewType4Holder) holder).listView.setAdapter(adapter);
                JSONArray finalViewList = viewList;
                ((ViewType4Holder) holder).listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        JSONObject obj = finalViewList.optJSONObject(i);
                        mINativeActionIpml.triggerAction(Integer.toString(obj.optJSONObject("actionData").optInt("actionType")),
                                obj.optJSONObject("actionData").optString("actionTarget"));
                    }
                });


                if (adapter != null) {
                    int totalHeight = 0;
                    for (int i = 0; i < adapter.getCount(); i++) {
                        View listItem = adapter.getView(i, null, ((ViewType4Holder) holder).listView);
                        listItem.measure(0, 0);
                        totalHeight += listItem.getMeasuredHeight();
                    }
                    ViewGroup.LayoutParams params = ((ViewType4Holder) holder).listView.getLayoutParams();
                    params.height = totalHeight + (((ViewType4Holder) holder).listView.getDividerHeight() * (((ViewType4Holder) holder).listView.getCount() - 1));
                    ((ViewType4Holder) holder).listView.setLayoutParams(params);
                    ((ViewType4Holder) holder).listView.requestLayout();
                }


//                for (int i = 0; i < ((ViewType3Holder) holder).viewList.size(); i++) {
//                    JSONObject data = viewList.optJSONObject(i);
//                    new NewDownloadImageTask(((ViewType3Holder) holder).imgList.get(i)).executeOnExecutor(MegaApplication.threadPoolExecutor, data.optString("viewUrl"));
//                    ((ViewType3Holder) holder).titleList.get(i).setText(data.optString("title"));
//                }


            } else if (holder instanceof ViewTypeBannerHolder) {
                LinearLayoutManager mlinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                String type = mHomeList.optJSONObject(index).optString("type");
                BannerScrollViewAdapter adapter = null;
                JSONObject viewData = getViewDataByType(type);
                if (viewData != null && viewData.has("headerTitle")) {
                    ((ViewTypeBannerHolder) holder).title.setText(viewData.optString("headerTitle"));
                    if (viewData.has("headerActionData") && viewData.optJSONObject("headerActionData").optInt("actionType") > 0) {
                        ((ViewTypeBannerHolder) holder).watchMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mINativeActionIpml.triggerAction(Integer.toString(viewData.optJSONObject("headerActionData").optInt("actionType")),
                                        viewData.optJSONObject("headerActionData").optString("actionTarget"));
                            }
                        });

                    } else {
                        ((ViewTypeBannerHolder) holder).watchMore.setVisibility(View.GONE);
                    }
                }


                switch (type) {
                    case "4":
                        viewList = getViewDataListByType("4");
                        adapter = new BannerScrollViewAdapter(mContext, viewList, this.mINativeActionIpml, viewWidth, "5", this.mLayoutList, this.mViewIdList);
                        //((ViewTypeBannerHolder) holder).title.setText(R.string.menu_article);
                        break;
                    case "5":
                        viewList = getViewDataListByType("5");
                        adapter = new BannerScrollViewAdapter(mContext, viewList, this.mINativeActionIpml, viewWidth, "6", this.mLayoutList, this.mViewIdList);

                        //((ViewTypeBannerHolder) holder).title.setText("最新問答");
                        break;
                    case "6":
                        viewList = getViewDataListByType("6");
                        adapter = new BannerScrollViewAdapter(mContext, viewList, this.mINativeActionIpml, viewWidth, "7", this.mLayoutList, this.mViewIdList);
                        //((ViewTypeBannerHolder) holder).title.setText("熱門話題");
                        break;
                }
                ((ViewTypeBannerHolder) holder).bannerView.setLayoutManager(mlinearLayoutManager);//設定為橫向
                ((ViewTypeBannerHolder) holder).bannerView.setAdapter(adapter);
            }

        }
    }


    @Override
    public int getItemCount() {

        return mHomeList != null ? mHomeList.length() + 2 : 0;
    }

    @Override
    public int getItemViewType(int position) {
        int index = position - 2;
        if (position == 0) {
            return TYPE_HEADER;
        } else if (position == -1) {
            //最后一个,应该加载Footer
            return TYPE_FOOTER;
        } else if (position == 1) {
            return TYPE_SEARCH;
        } else {
            try {
                String type = mHomeList.optJSONObject(index).optString("type");
                int viewType = Integer.parseInt(type);
                return viewType;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return position;
        }
    }

    public void setRefreshHeader(IRefreshHeader header) {
        if (header != null) {
            mRefreshHeader = header;
        }
    }

    @Override
    public void onClick(View view) {
        if (viewData.get(view.getId()) != null) {
            HomePageViewSet homePageSet = viewData.get(view.getId());
            new CommonApiTask(mContext, this, false, "", TaskEnum.MednetAppLogEven)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "homepage_button", homePageSet.getActionTarget());
            if ("1".equals(homePageSet.getActionType()) && homePageSet.getActionTarget() != null && homePageSet.getActionTarget().contains("AIDoctor")) {
                if (homePageFragment != null) {
                    homePageFragment.triggerAidoctor();

                }
                return;
            }

            mINativeActionIpml.triggerAction(homePageSet.getActionType(), homePageSet.getActionTarget());
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {

    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    class SearchViewHolder extends RecyclerView.ViewHolder {
        LinearLayout searchBar, healthReorder, doctorLayout, doctorTitle;
        EditText search;
        GridView healthl;
        TextView helloTitle, helloContent, wathchMore, doctorArticleCount, unpromotedArticleCount, drWathchMore;

        public SearchViewHolder(View itemView) {
            super(itemView);
            searchBar = itemView.findViewById(R.id.searchBar);
            helloContent = itemView.findViewById(R.id.hello_content);
            doctorTitle = itemView.findViewById(R.id.doctor_title);
            doctorLayout = itemView.findViewById(R.id.doctor_layout);


            search = itemView.findViewById(R.id.search);
            healthl = itemView.findViewById(R.id.healthBar);
            helloTitle = itemView.findViewById(R.id.hello_title);
            doctorArticleCount = itemView.findViewById(R.id.doctor_article_count);
            unpromotedArticleCount = itemView.findViewById(R.id.unpromoted_article_count);
            healthReorder = itemView.findViewById(R.id.health_reorder);
            wathchMore = itemView.findViewById(R.id.wathch_more);
            drWathchMore = itemView.findViewById(R.id.dr_wathch_more);
        }
    }

    class ViewType1Holder extends RecyclerView.ViewHolder {
        public List<RelativeLayout> viewList = new ArrayList<RelativeLayout>();
        public List<ImageView> imgList = new ArrayList<ImageView>();

        public ViewType1Holder(View itemView) {
            super(itemView);
            JSONArray dataList = null;
            dataList = getViewDataListByType("1");
            int index = 0;
            for (int i : mViewIdList[0]) {
                HomePageViewSet homePageViewSet = new HomePageViewSet(dataList.optJSONObject(index));
                viewData.put(i, homePageViewSet);
                viewList.add(itemView.findViewById(i));
                index++;
            }
            imgList.add(itemView.findViewById(R.id.img1));
            imgList.add(itemView.findViewById(R.id.img2));
            imgList.add(itemView.findViewById(R.id.img3));
        }
    }

    class ViewType2Holder extends RecyclerView.ViewHolder {
        public List<RelativeLayout> viewList = new ArrayList<RelativeLayout>();
        //        public List<TextView> titleList = new ArrayList<TextView>();
        public List<ImageView> imgList = new ArrayList<ImageView>();
        public TextView title;

        public ViewType2Holder(View itemView) {
            super(itemView);
            JSONArray dataList = null;
            dataList = getViewDataListByType("2");
            int index = 0;
            for (int i : mViewIdList[1]) {
                HomePageViewSet homePageViewSet = new HomePageViewSet(dataList.optJSONObject(index));
                viewData.put(i, homePageViewSet);
                viewList.add(itemView.findViewById(i));
                index++;
            }
            imgList.add(itemView.findViewById(R.id.img1));
            imgList.add(itemView.findViewById(R.id.img2));
            title = itemView.findViewById(R.id.title);
//            titleList.add(itemView.findViewById(R.id.title1));
//            titleList.add(itemView.findViewById(R.id.title2));
        }
    }

    class ViewType3Holder extends RecyclerView.ViewHolder {
        public List<RelativeLayout> viewList = new ArrayList<RelativeLayout>();
        public List<TextView> titleList = new ArrayList<TextView>();
        public List<ImageView> imgList = new ArrayList<ImageView>();

        public ViewType3Holder(View itemView) {
            super(itemView);
            JSONArray dataList = null;
            dataList = getViewDataListByType("3");
            int index = 0;
            for (int i : mViewIdList[2]) {
                HomePageViewSet homePageViewSet = new HomePageViewSet(dataList.optJSONObject(index));
                viewData.put(i, homePageViewSet);
                viewList.add(itemView.findViewById(i));
                index++;
            }
            imgList.add(itemView.findViewById(R.id.img1));
            imgList.add(itemView.findViewById(R.id.img2));
            imgList.add(itemView.findViewById(R.id.img3));
            imgList.add(itemView.findViewById(R.id.img4));
            titleList.add(itemView.findViewById(R.id.title1));
            titleList.add(itemView.findViewById(R.id.title2));
            titleList.add(itemView.findViewById(R.id.title3));
            titleList.add(itemView.findViewById(R.id.title4));
        }
    }

    class ViewType4Holder extends RecyclerView.ViewHolder {
        public ListView listView;
        public TextView title;
        public TextView watchMore;

        public ViewType4Holder(View itemView) {
            super(itemView);
            listView = itemView.findViewById(R.id.list);
            title = itemView.findViewById(R.id.title);
            watchMore = itemView.findViewById(R.id.watch_more);
        }
    }

    class ViewTypeBannerHolder extends RecyclerView.ViewHolder {
        public RecyclerView bannerView;
        public TextView title;
        public TextView watchMore;

        public ViewTypeBannerHolder(View itemView) {
            super(itemView);
            bannerView = itemView.findViewById(R.id.bannerView);
            title = itemView.findViewById(R.id.title);
            watchMore = itemView.findViewById(R.id.watch_more);
        }
    }


    class ListAdapter extends BaseAdapter {
        private JSONArray mList;

        ListAdapter(JSONArray list) {
            mList = list;
        }

        @Override
        public int getCount() {
            if (mList.length() >= 6)
                return 6;
            else
                return mList.length();
        }

        @Override
        public Object getItem(int i) {
            return mList.length();
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {

            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.home_viewtype4_cell, null);
            }
            ImageView imgView = convertView.findViewById(R.id.img);
            imgView.getLayoutParams().width = (int) (viewWidth / 3.5);
            imgView.getLayoutParams().height = (int) ((viewWidth / 3) / 100.0 * 56);
            TextView title = convertView.findViewById(R.id.title);
            TextView date = convertView.findViewById(R.id.date);
            JSONObject itme = mList.optJSONObject(i);

            //Log.i("debug", "getView: "+imgView.getDrawable().getCurrent().getConstantState());

            try {

                Glide.with(imgView.getContext())
                        .load(itme.optString("viewUrl"))
                        //.placeholder()//loading時候的Drawable
                        //.animate()//設置load完的動畫
                        //.centerCrop()//中心切圖, 會填滿
                        //.fitCenter()//中心fit, 以原本圖片的長寬為主
                        .into(imgView);

//                Bitmap imageBitmap = null;
//                URL imageURL = new URL(itme.optString("viewUrl"));
//                imageBitmap = BitmapFactory.decodeStream(imageURL.openStream());
//                imgView.setImageBitmap(imageBitmap);
//                new NewDownloadImageTask(imgView).executeOnExecutor(MegaApplication.threadPoolExecutor, itme.optString("viewUrl"));

            } catch (Exception e) {
                e.printStackTrace();
            }


            title.setText(itme.optString("title"));
            date.setText(itme.optString("date"));
            //title.setText(settingList.get(i).optString("healthName"));
            return convertView;
        }
    }
}
