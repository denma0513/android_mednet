package com.mgear.mednetapp.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;


import com.mgear.mednetapp.fragments.organization.TutorialStartAppFragment;

import java.util.ArrayList;

/**
 * Created by Dennis.
 * class: TutorialPagerStartAppAdapter
 */
public class TutorialPagerStartAppAdapter extends FragmentPagerAdapter {

    //private TutorialStartAppFragment fragment;
    private ArrayList<TutorialStartAppFragment> listFragment;

    public TutorialPagerStartAppAdapter(FragmentManager fm) {
        super(fm);
        listFragment = new ArrayList<TutorialStartAppFragment>();
        for (int i = 0; i < 2; i++) {
            TutorialStartAppFragment fragment = new TutorialStartAppFragment();
            listFragment.add(fragment);
        }

    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        TutorialStartAppFragment fragment = listFragment.get(position);
        fragment.setPage(position);
        return fragment;
//        switch (position) {
//            case 0:
//                return startFragment;
//            case 1:
//                return havoFragment;
//            //case 2:
//            //    return careFragment;
//            case 2:
//                return homeFragment;
//            //case 3:
//                //return mapFragment;
//            case 3:
//                return testsFragment;
//            case 4:
//                return statusFragment;
//            case 5:
//                return endFragment;
//            default:
//                return null;
//        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}