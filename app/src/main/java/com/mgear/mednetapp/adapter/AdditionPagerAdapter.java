package com.mgear.mednetapp.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.mgear.mednetapp.fragments.organization.AdditionSetFragment;
import com.mgear.mednetapp.fragments.organization.AdditionStatusFragment;


/**
 * Created by Jye on 2017/6/14.
 * class: AdditionPagerAdapter
 */
public class AdditionPagerAdapter extends FragmentPagerAdapter {

    private AdditionSetFragment setFragment;
    private AdditionStatusFragment statusFragment;

    public AdditionPagerAdapter(FragmentManager fm) {
        super(fm);
        setFragment = new AdditionSetFragment();
        statusFragment = new AdditionStatusFragment();
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position) {
            case 0:
                return setFragment;
            case 1:
                return statusFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}