package com.mgear.mednetapp.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.mgear.mednetapp.fragments.organization.HomeJoinFacebookFragment;
import com.mgear.mednetapp.fragments.organization.HomeJoinLineFragment;
import com.mgear.mednetapp.fragments.organization.HomeJoinOfficialFragment;


/**
 * Created by Jye on 2017/6/15.
 * class: HomeJoinPagerAdapter
 */
public class HomeJoinPagerAdapter extends FragmentPagerAdapter {

    private HomeJoinOfficialFragment officialFragment;
    private HomeJoinFacebookFragment facebookFragment;
    private HomeJoinLineFragment lineFragment;

    public HomeJoinPagerAdapter(FragmentManager fm) {
        super(fm);
        officialFragment = new HomeJoinOfficialFragment();
        facebookFragment = new HomeJoinFacebookFragment();
        lineFragment = new HomeJoinLineFragment();
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position) {
            case 0:
                return officialFragment;
            case 1:
                return facebookFragment;
            case 2:
                return lineFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}