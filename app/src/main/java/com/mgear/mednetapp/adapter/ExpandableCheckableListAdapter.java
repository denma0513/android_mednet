package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.CheckableItem;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Jye on 2017/7/20.
 * class: ExpandableCheckableListAdapter
 */
public class ExpandableCheckableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<CheckableItem>> expandableListDetail;

    private class ViewHold {
        ImageView imgCheck;
        TextView txtName;
        TextView txtSubName;
    }

    public ExpandableCheckableListAdapter(Context context, List<String> expandableListTitle, HashMap<String, List<CheckableItem>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final CheckableItem checkableItem = (CheckableItem) getChild(listPosition, expandedListPosition);
        ViewHold hold;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_sub_checkable_list, null);
            hold = new ViewHold();
            hold.imgCheck = (ImageView) convertView.findViewById(R.id.imgCheckableSelect);
            hold.txtName = (TextView) convertView.findViewById(R.id.txtCheckableName);
            hold.txtSubName = (TextView) convertView.findViewById(R.id.txtCheckableSubName);
            convertView.setTag(hold);
        }
        else hold = (ViewHold) convertView.getTag();
        hold.txtName.setText(checkableItem.getName());
        hold.txtSubName.setText(checkableItem.getSubName());
        hold.imgCheck.setImageResource(checkableItem.isCheck() ? R.drawable.check_box_on : R.drawable.check_box_off);
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.expandable_list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}