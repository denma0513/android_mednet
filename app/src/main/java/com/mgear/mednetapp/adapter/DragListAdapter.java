package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mgear.mednetapp.R;

import java.util.ArrayList;


/**
 * Created by Dennis
 */
public class DragListAdapter extends BaseAdapter {
    private ArrayList<String> mDatas;

    private Context context;

    public DragListAdapter(Context context, ArrayList<String> arrayTitles) {
        this.context = context;
        this.mDatas = arrayTitles;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        /***
         * 在這裏盡可能每次都進行實例化新的，這樣在拖拽ListView的時候不會出現錯亂.
         * 具體原因不明，不過這樣經過測試，目前沒有發現錯亂。雖說效率不高，但是做拖拽LisView足夠了。
         */
        view = LayoutInflater.from(context).inflate(R.layout.drag_list_item, null);

        //TextView textView = (TextView) view.findViewById(R.id.tv_name);
        //textView.setText(mDatas.get(position));
        return view;
    }

    /***
     * 動態修改ListVIiw的方位.（數據移位）
     *
     * @param start 點擊移動的position
     * @param end   松開時候的position
     */
    public void update(int start, int end) {
        String data = mDatas.get(start);
        mDatas.remove(start);// 刪除該項
        mDatas.add(end, data);// 添加刪除項
        notifyDataSetChanged();// 刷新ListView
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
