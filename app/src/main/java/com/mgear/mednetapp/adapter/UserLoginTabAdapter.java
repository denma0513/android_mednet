package com.mgear.mednetapp.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.SignInFragment;
import com.mgear.mednetapp.fragments.SignUpFragment;

public class UserLoginTabAdapter extends FragmentPagerAdapter {

    private Context mContext;
    //分頁數量
    private int NUM_ITEMS = 2;

    public UserLoginTabAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    //要切換的Fragment
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new SignInFragment();
            case 1:
                return new SignUpFragment();
        }
        return new SignInFragment();
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.button_SignIn);
            case 1:
                return mContext.getString(R.string.button_SignUp);
            default:
                return null;
        }
    }
}
