package com.mgear.mednetapp.adapter;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.mgear.mednetapp.fragments.HealthRecordFragment;
import com.mgear.mednetapp.fragments.HealthRecordListFragment;

public class HealthDetailTabAdapter extends FragmentPagerAdapter {

    private final HealthRecordFragment mHealthRecordFragment;
    private final HealthRecordListFragment mHealthRecordListFragment;
    private Context mContext;
    //分頁數量
    private int NUM_ITEMS = 2;

    public HealthDetailTabAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        mHealthRecordFragment = null;
        mHealthRecordListFragment = null;
    }

    public HealthDetailTabAdapter(Context context, FragmentManager fm, HealthRecordFragment healthRecordFragment, HealthRecordListFragment healthRecordListFragment) {
        super(fm);
        mContext = context;
        mHealthRecordFragment = healthRecordFragment;
        mHealthRecordListFragment = healthRecordListFragment;
    }

    //要切換的Fragment
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return mHealthRecordFragment;
            case 1:
                return mHealthRecordListFragment;
        }
        return new HealthRecordFragment();
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "紀錄";
            case 1:
                return "紀錄列表";
            default:
                return null;
        }
    }
}
