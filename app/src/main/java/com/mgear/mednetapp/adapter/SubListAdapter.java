package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.StringStringSet;


/**
 * Created by Jye on 2017/8/8.
 * class: SubListAdapter
 */
public class SubListAdapter extends ArrayAdapter<StringStringSet> {

    private final Context context;
    private final int layout;

    private class ViewHold {
        TextView txtMajor;
        TextView txtMinor;
    }

    public SubListAdapter(Context context, StringStringSet[] set) {
        super(context, R.layout.item_sub_list, set);
        this.layout = R.layout.item_sub_list;
        this.context = context;
    }

    public SubListAdapter(Context context, int layout, StringStringSet[] set) {
        super(context, layout, set);
        this.layout = layout;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHold hold;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(layout, null);
            hold = new ViewHold();
            hold.txtMajor = (TextView) convertView.findViewById(R.id.txtSubListMajorName);
            hold.txtMinor = (TextView) convertView.findViewById(R.id.txtSubListMinorName);
            convertView.setTag(hold);
        }
        else hold = (ViewHold) convertView.getTag();
        StringStringSet value = getItem(position);
        if (value != null) {
            hold.txtMajor.setText(value.getMajor());
            hold.txtMinor.setText(value.getMinor());
            //判斷是否顯示紅字
            if (value.getResult()) {
                convertView.setBackgroundColor(Color.TRANSPARENT);
                hold.txtMajor.setTextColor(Color.BLACK);
            }
            else {
                convertView.setBackgroundColor(ContextCompat.getColor(context, R.color.ui_rich_pink));
                hold.txtMajor.setTextColor(Color.WHITE);
            }
        }
        return convertView;
    }

}