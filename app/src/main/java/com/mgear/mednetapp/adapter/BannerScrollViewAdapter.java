package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.HomePageViewSet;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.task.common.NewDownloadImageTask;
import com.mgear.mednetapp.views.CircleImageView;

import org.json.JSONArray;
import org.json.JSONObject;


public class BannerScrollViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private LayoutInflater mInflater;
    private INativeActionImpl mINativeActionImpl;
    private JSONArray list;
    private int mViewWidth;
    private String mViewType;
    private int[] mLayoutList;
    private int[][] mViewIdList;


    public BannerScrollViewAdapter(Context context, JSONArray list, INativeActionImpl INativeActionIpml, int viewWidth, String viewType, int[] layoutList, int[][] viewIdList) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mINativeActionImpl = INativeActionIpml;
        this.list = list;
        this.mViewWidth = viewWidth;
        this.mViewType = viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.home_viewtype4, parent, false);
        switch (mViewType) {
            case "5":
                view = mInflater.inflate(R.layout.home_viewtype4, parent, false);
                ViewType4Holder holder4 = new ViewType4Holder(view);
                int height = (int) (((mViewWidth - 100) / 500.0 * 300.0) + 20);
                holder4.banner.getLayoutParams().width = mViewWidth;
                holder4.banner.getLayoutParams().height = height;
                holder4.title.getLayoutParams().width = mViewWidth - 100;
                return holder4;
            case "6":
                view = mInflater.inflate(R.layout.home_viewtype5, parent, false);
                ViewType5Holder holder5 = new ViewType5Holder(view);
                holder5.cardView.getLayoutParams().width = (int) ((mViewWidth / 6.0) * 5);
                return holder5;
            case "7":
                view = mInflater.inflate(R.layout.home_viewtype6, parent, false);
                ViewType6Holder holder6 = new ViewType6Holder(view);
                holder6.cardView.getLayoutParams().width = (int) ((mViewWidth / 6.0) * 5);
                return holder6;
        }

        //holder.banner.getLayoutParams().height = 300;
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        JSONObject item = list.optJSONObject(position);
        switch (mViewType) {
            case "5":
                new NewDownloadImageTask(((ViewType4Holder) holder).banner).executeOnExecutor(MegaApplication.threadPoolExecutor, item.optString("viewUrl"));
                ((ViewType4Holder) holder).title.setText(item.optString("title"));
                ((ViewType4Holder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onItemClick(position);
                    }
                });
                break;
            case "6":
                String ansDate = "%s已於%s回答";
                //Glide.with(((ViewType5Holder) holder).picture.getContext()).load(item.optString("viewUrl")).into(((ViewType5Holder) holder).picture);
                new NewDownloadImageTask(((ViewType5Holder) holder).picture).executeOnExecutor(MegaApplication.threadPoolExecutor, item.optString("viewUrl"));
                String title = item.optString("title");
                String content = item.optString("content");
                content = content.replaceAll("\n", "");

                if (title.length() > 15)
                    title = title.substring(0, 15) + "...";
                if (content.length() > 50)
                    content = content.substring(0, 50) + "...";

                ((ViewType5Holder) holder).title.setText(title);
                ((ViewType5Holder) holder).content.setText(content);
                ((ViewType5Holder) holder).doct.setText(String.format(ansDate, item.optString("docName"), item.optString("ansDate")));
                ((ViewType5Holder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onItemClick(position);
                    }
                });
                break;
            case "7":
                //Glide.with(((ViewType6Holder) holder).picture.getContext()).load(item.optString("viewUrl")).into(((ViewType6Holder) holder).picture);
                new NewDownloadImageTask(((ViewType6Holder) holder).picture).executeOnExecutor(MegaApplication.threadPoolExecutor, item.optString("viewUrl"));
                content = item.optString("content");
                content = content.replaceAll("\n", "");
                if (content.length() > 50)
                    content = content.substring(0, 50) + "...";

                ((ViewType6Holder) holder).title.setText(item.optString("title"));
                ((ViewType6Holder) holder).content.setText(content);
                JSONArray divisionList = item.optJSONArray("division");
                ((ViewType6Holder) holder).divisionView.removeAllViews();
                if (divisionList.length() > 0)
                    for (int i = 0; i < divisionList.length(); i++) {
                        if (divisionList.optString(i) == null || "null".equals(divisionList.optString(i)))
                            continue;

                        TextView textView = new TextView(context);
                        textView.setText(divisionList.optString(i));
                        textView.setTextColor(ContextCompat.getColor(context, R.color.home_title_blue));
                        textView.setTextSize(14);
                        textView.setBackgroundResource(R.drawable.edittext_frame);
                        textView.setPadding(20, 8, 20, 8);
                        ((ViewType6Holder) holder).divisionView.addView(textView);
                    }
                ((ViewType6Holder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onItemClick(position);
                    }
                });


                break;
        }

        return;
    }

    //點擊事件
    private void onItemClick(int position) {
        JSONObject data = list.optJSONObject(position);
        HomePageViewSet homePageViewSet = new HomePageViewSet(data);
        //統一處理原生點擊事件
        mINativeActionImpl.triggerAction(homePageViewSet.getActionType(), homePageViewSet.getActionTarget());
    }

    @Override
    public int getItemCount() {
        return list.length();
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ViewType4Holder extends RecyclerView.ViewHolder {
        public ImageView banner;
        public TextView title;
        LinearLayout cardView;

        public ViewType4Holder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            banner = itemView.findViewById(R.id.banner);
            title = itemView.findViewById(R.id.title);
        }
    }

    class ViewType5Holder extends RecyclerView.ViewHolder {
        public CircleImageView picture;
        public TextView title, content, doct;
        public RelativeLayout cardView;

        public ViewType5Holder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            picture = itemView.findViewById(R.id.picture);
            title = itemView.findViewById(R.id.title);
            content = itemView.findViewById(R.id.content);
            doct = itemView.findViewById(R.id.doct);
        }
    }

    class ViewType6Holder extends RecyclerView.ViewHolder {
        public CircleImageView picture;
        public TextView title, content;
        public RelativeLayout cardView;
        public LinearLayout divisionView;

        public ViewType6Holder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            divisionView = itemView.findViewById(R.id.divisionView);
            picture = itemView.findViewById(R.id.picture);
            title = itemView.findViewById(R.id.title);
            content = itemView.findViewById(R.id.content);
        }
    }
}
