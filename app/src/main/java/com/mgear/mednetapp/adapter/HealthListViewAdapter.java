package com.mgear.mednetapp.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.health.HealthBaseSet;
import com.mgear.mednetapp.entity.health.HealthBloodOxygenSet;
import com.mgear.mednetapp.entity.health.HealthBloodPressureSet;
import com.mgear.mednetapp.entity.health.HealthBloodSugarSet;
import com.mgear.mednetapp.entity.health.HealthBodySet;
import com.mgear.mednetapp.entity.health.HealthDrinkingSet;
import com.mgear.mednetapp.entity.health.HealthSleepingSet;
import com.mgear.mednetapp.entity.health.HealthStepSet;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.IRefreshHeader;
import com.mgear.mednetapp.interfaces.OnItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class HealthListViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE_HEADER = 0;  //有Header
    public static final int TYPE_FOOTER = -1;  //有Footer的

    private Context mContext;
    private IRefreshHeader mRefreshHeader;
    private OnItemClickListener mItemClickListener;
    private LayoutInflater mInflater;
    //private JSONArray healthList = new JSONArray();


    public HealthListViewAdapter(Context context) {
        this.mContext = context;
        this.mItemClickListener = (OnItemClickListener) context;
        this.mInflater = LayoutInflater.from(context);
        //healthList = new JSONArray();
    }

    public void setAdapterList(JSONArray list) {
        //this.healthList = list;
    }

    private void hasRecode(ItemHolder itemHolder) {
        itemHolder.nonValueView.setVisibility(View.GONE);
        itemHolder.valueView.setVisibility(View.VISIBLE);
    }

    private void hasNoRecode(ItemHolder itemHolder) {
        itemHolder.nonValueView.setVisibility(View.VISIBLE);
        itemHolder.valueView.setVisibility(View.GONE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.health_list_cell, parent, false);
        //Log.i("debug","onCreateViewHolder"+" "+viewType);
        if (viewType == TYPE_HEADER) {
            return new HeaderViewHolder(mRefreshHeader.getHeaderView());
        } else {
            view = mInflater.inflate(R.layout.health_list_cell, parent, false);
            ItemHolder viewHolder = new ItemHolder(view);
            viewHolder.cell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(view, viewType - 1, MegaApplication.healthList.optJSONObject(viewType - 1));
                }
            });
            return viewHolder;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (getItemViewType(position) == TYPE_HEADER) {
            return;
        } else {
            int index = position - 1;
            ItemHolder itemHolder = (ItemHolder) holder;
            JSONObject viewData = MegaApplication.healthList.optJSONObject(index);
            //Log.i("debug", "index = " + index + " viewData = " + viewData);
            //Log.i("debug", "viewData= "+viewData );
            itemHolder.icon.setImageResource(viewData.optInt("healthIcon"));
            switch (viewData.optString("healthType")) {
                case "1":
                    HealthBloodSugarSet bloodSugarSet = new HealthBloodSugarSet(mContext, viewData);
                    HealthBaseSet baseSet = bloodSugarSet;
                    itemHolder.title.setText(baseSet.getName());
                    if (baseSet.getCreatedAt().length() > 0) {
                        hasRecode(itemHolder);
                        itemHolder.unit.setText(baseSet.getUnit());
                        itemHolder.recode.setText(parseDate(baseSet.getmTime()));
                        itemHolder.value.setText(baseSet.getValue());
                        itemHolder.value2.setText(baseSet.getValue2());
                        itemHolder.status.setTextColor(baseSet.getColor());
                        itemHolder.status.setText(baseSet.getStatusUseSpannableString());
                    } else {
                        hasNoRecode(itemHolder);
                        itemHolder.recode.setText(mContext.getString(R.string.title_non_recode));
                    }

                    break;
                case "2":
                    HealthBloodPressureSet bloodPressureSet = new HealthBloodPressureSet(mContext, viewData);
                    baseSet = bloodPressureSet;
                    itemHolder.title.setText(baseSet.getName());
                    if (baseSet.getCreatedAt().length() > 0) {
                        hasRecode(itemHolder);
                        itemHolder.unit.setText(baseSet.getUnit());
                        itemHolder.recode.setText(parseDate(baseSet.getmTime()));
                        itemHolder.value.setText(baseSet.getValue());
                        itemHolder.value2.setText(baseSet.getValue2());
                        itemHolder.status.setTextColor(baseSet.getColor());
                        itemHolder.status.setText(baseSet.getStatusUseSpannableString());
                    } else {
                        hasNoRecode(itemHolder);
                        itemHolder.recode.setText(mContext.getString(R.string.title_non_recode));
                    }
                    break;
                case "3":
                    HealthBloodOxygenSet bloodOxygenSet = new HealthBloodOxygenSet(mContext, viewData);
                    baseSet = bloodOxygenSet;
                    itemHolder.title.setText(baseSet.getName());
                    if (baseSet.getCreatedAt().length() > 0) {
                        hasRecode(itemHolder);
                        itemHolder.unit.setText(baseSet.getUnit());
                        itemHolder.recode.setText(parseDate(baseSet.getmTime()));
                        itemHolder.value.setText(baseSet.getValue());
                        itemHolder.value2.setText(baseSet.getValue2());
                        itemHolder.status.setTextColor(baseSet.getColor());
                        itemHolder.status.setText(baseSet.getStatusUseSpannableString());
                    } else {
                        hasNoRecode(itemHolder);
                        itemHolder.recode.setText(mContext.getString(R.string.title_non_recode));
                    }
                    break;
                case "4":
                    HealthDrinkingSet drinkingSet = new HealthDrinkingSet(mContext, viewData);
                    baseSet = drinkingSet;
                    itemHolder.title.setText(drinkingSet.getName());
                    if (baseSet.getCreatedAt().length() > 0) {
                        hasRecode(itemHolder);
                        itemHolder.unit.setText(baseSet.getUnit());
                        itemHolder.recode.setText(parseDate(baseSet.getmTime()));
                        itemHolder.value.setText(baseSet.getValue());
                        itemHolder.value2.setText(baseSet.getValue2());
                        itemHolder.status.setTextColor(mContext.getResources().getColor(R.color.home_title_black));
                        //itemHolder.status.setTextColor(baseSet.getColor());
                        itemHolder.status.setText("ml");
                        itemHolder.value2.setText("目標 " + MegaApplication.WaterCapacity);
                    } else {
                        hasNoRecode(itemHolder);
                        itemHolder.recode.setText(mContext.getString(R.string.title_non_recode));
                    }
                    break;
                case "5":
                    HealthSleepingSet sleepingSet = new HealthSleepingSet(mContext, viewData);
                    baseSet = sleepingSet;
                    itemHolder.title.setText(baseSet.getName());
                    if (baseSet.getCreatedAt().length() > 0) {
                        hasRecode(itemHolder);
                        itemHolder.unit.setText(baseSet.getUnit());
                        itemHolder.recode.setText(parseDate(baseSet.getmTime()));
                        itemHolder.value.setText(baseSet.getValue());
                        itemHolder.value2.setText(baseSet.getValue2());
                        itemHolder.status.setTextColor(baseSet.getColor());
                        itemHolder.status.setText(baseSet.getStatusUseSpannableString());
                    } else {
                        hasNoRecode(itemHolder);
                        itemHolder.recode.setText(mContext.getString(R.string.title_non_recode));
                    }
                    break;
                case "6":
                    HealthStepSet stepSet = new HealthStepSet(mContext, viewData);
                    baseSet = stepSet;
                    itemHolder.title.setText(baseSet.getName());
                    if (baseSet.getCreatedAt().length() > 0) {
                        hasRecode(itemHolder);
                        itemHolder.unit.setText(baseSet.getUnit());
                        itemHolder.recode.setText(parseDate(baseSet.getmTime()));
                        itemHolder.value.setText(baseSet.getValue());
                        itemHolder.value2.setText("目標 " + baseSet.getValue2());
                        itemHolder.status.setTextColor(baseSet.getColor());
                        itemHolder.status.setText(baseSet.getStatusUseSpannableString());
                    } else {
                        hasNoRecode(itemHolder);
                        itemHolder.recode.setText(mContext.getString(R.string.title_non_recode));
                    }
                    break;
                case "7":
                    HealthBodySet bodySet = new HealthBodySet(mContext, viewData);
                    baseSet = bodySet;

                    itemHolder.title.setText(baseSet.getName());
                    if (baseSet.getCreatedAt().length() > 0) {
                        hasRecode(itemHolder);
                        itemHolder.unit.setText(baseSet.getUnit());
                        itemHolder.recode.setText(parseDate(baseSet.getmTime()));
                        itemHolder.value.setText(baseSet.getValue());
                        //itemHolder.value2.setText(baseSet.getValue2());
                        itemHolder.status.setTextColor(baseSet.getColor());
                        itemHolder.status.setText(baseSet.getStatusUseSpannableString());
                    } else {
                        hasNoRecode(itemHolder);
                        itemHolder.recode.setText(mContext.getString(R.string.title_non_recode));
                    }
                    break;
            }

        }
    }


    @Override
    public int getItemCount() {
        //Log.i("debug","mHomeList.length() = "+healthList.length());
        if (MegaApplication.healthList != null)
            return MegaApplication.healthList.length() + 1;
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        int index = position - 1;
        if (position == 0) {
            //Log.i("debug","viewType= "+TYPE_HEADER);
            return TYPE_HEADER;
        } else if (position == -1) {
            //最后一个,应该加载Footer
            return TYPE_FOOTER;
        } else {
            try {
                //Log.i("debug","position = "+position);
                int viewType = index;
                //Log.i("debug","index = "+index);
                return position;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return position;
        }
    }

    public void setRefreshHeader(IRefreshHeader header) {
        if (header != null) {
            mRefreshHeader = header;
        }
    }


    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ItemHolder extends RecyclerView.ViewHolder {
        public RelativeLayout cell;
        public LinearLayout valueView;
        public RelativeLayout nonValueView;
        public TextView title;
        public TextView unit;
        public TextView value;
        public TextView value2;
        public TextView recode;
        public TextView status;
        public ImageView icon;

        public ItemHolder(View itemView) {
            super(itemView);
            cell = itemView.findViewById(R.id.health_cell);
            valueView = itemView.findViewById(R.id.health_already_recode);
            nonValueView = itemView.findViewById(R.id.health_never_recode);
            title = itemView.findViewById(R.id.health_title);
            unit = itemView.findViewById(R.id.health_unit);
            value = itemView.findViewById(R.id.health_value);
            value2 = itemView.findViewById(R.id.health_value2);
            recode = itemView.findViewById(R.id.health_recode);
            status = itemView.findViewById(R.id.health_status);
            icon = itemView.findViewById(R.id.health_img);
        }
    }

    public String parseDate(String fullDate) {
        String retDate = "";
        if (fullDate != null && fullDate.indexOf("T") >= 0) {
            retDate = fullDate.substring(0, fullDate.indexOf("T"));
        }
        return retDate;
    }

}
