package com.mgear.mednetapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.internal.service.Common;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.health.HealthBaseSet;
import com.mgear.mednetapp.entity.health.HealthBloodOxygenSet;
import com.mgear.mednetapp.entity.health.HealthBloodPressureSet;
import com.mgear.mednetapp.entity.health.HealthBloodSugarSet;
import com.mgear.mednetapp.entity.health.HealthBodySet;
import com.mgear.mednetapp.entity.health.HealthDrinkingSet;
import com.mgear.mednetapp.entity.health.HealthSleepingSet;
import com.mgear.mednetapp.entity.health.HealthStepSet;
import com.mgear.mednetapp.entity.organization.StringIntegerSet;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.organization.HomePageImpl;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.CustomViewPager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Stack;

/**
 * Created by Jye on 2017/6/14.
 * class: HomeGridAdapter
 */
public class HomeHealthGridAdapter extends BaseAdapter {

    private Context mContext;
    private INativeActionImpl mINativeActionIpml;
    private LayoutInflater inflater;
    private JSONArray mHealthList;

    public HomeHealthGridAdapter(Context context, JSONArray healthList) {
        this.mContext = context;
        this.mINativeActionIpml = (INativeActionImpl) context;
        this.mHealthList = healthList;
        this.inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public String parseDate(String fullDate) {
        String retDate = "";
        if (fullDate != null && fullDate.indexOf("T") >= 0) {
            retDate = fullDate.substring(0, fullDate.indexOf("T"));
        }
        return retDate;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ResourceType")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.home_health_grid_item, null);
        ImageView gridImg = convertView.findViewById(R.id.grid_img);
        TextView gridType = convertView.findViewById(R.id.grid_type);
        TextView gridValue = convertView.findViewById(R.id.grid_value);
        TextView gridDate = convertView.findViewById(R.id.grid_date);
        JSONObject item = mHealthList.optJSONObject(position);
        //Log.i("debug", "item = " + item);
        if (item != null) {
            HealthBaseSet baseSet = null;
            switch (item.optString("healthType")) {
                case "1":
                    HealthBloodSugarSet bloodSugarSet = new HealthBloodSugarSet(mContext, item);
                    baseSet = bloodSugarSet;
                    break;
                case "2":
                    HealthBloodPressureSet bloodPressureSet = new HealthBloodPressureSet(mContext, item);
                    baseSet = bloodPressureSet;
                    break;
                case "3":
                    HealthBloodOxygenSet bloodOxygenSet = new HealthBloodOxygenSet(mContext, item);
                    baseSet = bloodOxygenSet;
                    break;
                case "4":
                    HealthDrinkingSet drinkingSet = new HealthDrinkingSet(mContext, item);
                    baseSet = drinkingSet;
                    break;
                case "5":
                    HealthSleepingSet sleepingSet = new HealthSleepingSet(mContext, item);
                    baseSet = sleepingSet;
                    break;
                case "6":
                    HealthStepSet stepSet = new HealthStepSet(mContext, item);
                    baseSet = stepSet;
                    break;
                case "7":
                    HealthBodySet bodySet = new HealthBodySet(mContext, item);
                    baseSet = bodySet;
                    break;
            }

            gridImg.setImageResource(baseSet.getHealthIcon());
            gridType.setText(baseSet.getName());
            if ("".equals(baseSet.getmTime())) {
                gridValue.setText("- " + baseSet.getUnit());
            } else {
                gridValue.setText(baseSet.getValue() + " " + baseSet.getUnit());
            }

            gridDate.setText(parseDate(baseSet.getmTime()));

        } else {

            item = MegaApplication.healthMap.get(MegaApplication.healthTypeList[position]);
            gridImg.setImageResource(item.optInt("healthRecordIcon"));
            gridType.setText(item.optString("healthName"));
            gridValue.setText("-");
            gridDate.setText("");


        }
        JSONObject finalItem = item;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.i("debug", "finalItem = " + finalItem);
                if ("4".equals(finalItem.optString("healthType"))) {
                    JSONObject send = new JSONObject();
                    try {
                        send.put("healthType", "4");
                        mINativeActionIpml.triggerAction(MegaApplication.ActionTypeHealthPushCreate, send.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    JSONObject send = new JSONObject();
                    try {
                        send.put("healthType", finalItem.optString("healthType"));
                        mINativeActionIpml.triggerAction(MegaApplication.ActionTypeHealthPushCreate, send.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        return convertView;
    }
}