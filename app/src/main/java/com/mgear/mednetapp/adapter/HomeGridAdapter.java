package com.mgear.mednetapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.StringIntegerSet;
import com.mgear.mednetapp.interfaces.organization.HomePageImpl;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.CustomViewPager;

import java.util.Stack;

/**
 * Created by Jye on 2017/6/14.
 * class: HomeGridAdapter
 */
public class HomeGridAdapter extends BaseAdapter {

    private class HomeGridItem {
        LinearLayout layout;
        ImageView image;
        ImageView image2;
        //TextView text;
    }
    private HomePageImpl homePageImpl;
    private String strTitle;
    private CustomViewPager mPager;
    private int pagePosition;
    private Stack<StringIntegerSet> previousPage;

    private final int[] aryImages;
    private final String[] aryItems;
    private final int[] aryColors;
    private LayoutInflater inflater;
    private Context mContext;
    private int itemHeight;

    public HomeGridAdapter(Context context, String[] items, int[] images, int[] colors, int height, HomePageImpl homePageImpl) {
        this.homePageImpl = homePageImpl;
        this.mContext = context;
        this.inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.aryItems = items;
        this.aryImages = images;
        this.aryColors = colors;
        this.itemHeight = height;

        //新增欄位 為了可以在這裡開啟新畫面
        this.strTitle = strTitle;
        this.mPager = mPager;
        this.pagePosition = pagePosition;
        this.previousPage = previousPage;
    }

    @Override
    public int getCount() {
        return aryItems.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ResourceType")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HomeGridItem item = null;
        String itemsName = aryItems[position];
        String itemsName2 = "";

        Log.i("debug","itemsName = "+itemsName);


        if (itemsName.contains(mContext.getResources().getString(R.string.menu_join)) || itemsName.contains(mContext.getResources().getString(R.string.menu_ilink))) {
            Log.i("debug","93");
            //int newitemHeight = itemHeight / 3;
            //導向社群網站 or 導向社群網站
//            if (convertView == null) {
                convertView = this.inflater.inflate(R.layout.home_grid_item2, null);
                convertView.setLayoutParams(new AbsListView.LayoutParams(itemHeight, itemHeight / 2));
                item = new HomeGridItem();
                item.layout = (LinearLayout) convertView.findViewById(R.id.rootHomeGridItem2);
                item.image = (ImageView) convertView.findViewById(R.id.imgHomeGridItem1);
                item.image2 = (ImageView) convertView.findViewById(R.id.imgHomeGridItem2);
                convertView.setTag(item);
//            }
//            else item = (HomeGridItem) convertView.getTag();
            Log.i("Homeitem","item.image = "+item.image+" item.image2 = "+item.image2);

            item.image.setOnClickListener((View.OnClickListener) mContext);
            item.image2.setOnClickListener((View.OnClickListener) mContext);
            item.image.getLayoutParams().height = itemHeight/2;
            item.image.getLayoutParams().width = itemHeight/2;
            item.image2.getLayoutParams().height = itemHeight/2;
            item.image2.getLayoutParams().width = itemHeight/2;
            if (itemsName.contains(mContext.getResources().getString(R.string.menu_join))) {
                item.image.setId(R.string.menu_facebook);
                item.image.setImageDrawable(new BitmapDrawable(mContext.getResources(), CommonFunc.readBitmap(mContext, R.drawable.menu_fb)));
                item.image2.setId(R.string.menu_line);
                item.image2.setImageDrawable(new BitmapDrawable(mContext.getResources(), CommonFunc.readBitmap(mContext, R.drawable.menu_line)));
            }else if (itemsName.contains(mContext.getResources().getString(R.string.menu_ilink))) {
                item.image.setId(R.string.menu_ilink);
                item.image.setImageDrawable(new BitmapDrawable(mContext.getResources(), CommonFunc.readBitmap(mContext, R.drawable.menu_firstnet)));
                item.image2.setId(R.string.menu_org_rating);
                item.image2.setImageDrawable(new BitmapDrawable(mContext.getResources(), CommonFunc.readBitmap(mContext, R.drawable.menu_org_rating)));

            }


            item.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("Homeitem","itemsName = "+itemsName+" view.getId() = "+view.getId());
                    int linkPosition = 0;
                    int homePosition = 0;
                    if (view.getId() == R.string.menu_facebook) {
                        linkPosition = 0;
                        homePosition = 14;
                        strTitle = "FB";
                    } else if (view.getId() == R.string.menu_ilink){
                        linkPosition = 2;
                        homePosition = 8;
                        strTitle =  "醫聯網";
                    }
                    Log.i("Homeitem","linkPosition = "+linkPosition);
                    homePageImpl.onClickItem(linkPosition, homePosition);


//                    //紀錄目前的頁面
//                    previousPage.push(new StringIntegerSet(strTitle, pagePosition));
//                    //顯示到新頁面
//                    pagePosition = position + 1;
//                    mPager.setCurrentItem(pagePosition, false);
//                    ((MainActivity) mContext).setPreviousPage((PreviousPageImpl) mContext);
                }
            });

            item.image2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("Homeitem","itemsName = "+itemsName+" view.getId() = "+view.getId());
                    int linkPosition = 1;
                    int homePosition = 0;
                    if (view.getId() == R.string.menu_line) {
                        linkPosition = 1;
                        homePosition = 15;
                        strTitle =  "Line";
                    } else if (view.getId() == R.string.menu_org_rating){
                        linkPosition = 3;
                        homePosition = 13;
                        strTitle = "機構";
                    }
                    Log.i("Homeitem","linkPosition = "+linkPosition);
                    homePageImpl.onClickItem(linkPosition, homePosition);

//                    //紀錄目前的頁面
//                    previousPage.push(new StringIntegerSet(strTitle, pagePosition));
//                    //顯示到新頁面
//                    pagePosition = position + 1;
//                    mPager.setCurrentItem(pagePosition, false);
//                    ((MainActivity) mContext).setPreviousPage((PreviousPageImpl) mContext);
                }
            });



        } else {
            //一般格式的圖，一個gridview 放一張圖
            Log.i("debug","187");
//            if (convertView == null) {
//
//            }
//            else item = (HomeGridItem) convertView.getTag();

            convertView = this.inflater.inflate(R.layout.home_grid_item, null);
            //noinspection SuspiciousNameCombination
            convertView.setLayoutParams(new AbsListView.LayoutParams(itemHeight, itemHeight));
            item = new HomeGridItem();
            item.layout = (LinearLayout) convertView.findViewById(R.id.rootHomeGridItem);
            item.image = (ImageView) convertView.findViewById(R.id.imgHomeGridItem);
            //item.text = (TextView) convertView.findViewById(R.id.txtHomeGridItem);
            convertView.setTag(item);


            //setup
            //item.image.setBackgroundColor(aryColors[position]);
            item.image.setImageDrawable(new BitmapDrawable(mContext.getResources(), CommonFunc.readBitmap(mContext, aryImages[position])));
            //item.text.setText(aryItems[position]);
        }
        return convertView;
    }
}