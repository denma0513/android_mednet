package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.CheckableItem;

/**
 * Created by Jye on 2017/8/18.
 * class: IconCheckableListAdapter
 */
public class IconCheckableListAdapter extends ArrayAdapter<CheckableItem> {

    private final Context context;
    private final int layout;

    private class ViewHold {
        ImageView imgIcon;
        ImageView imgCheck;
        TextView txtName;
    }

    public IconCheckableListAdapter(Context context, CheckableItem[] set) {
        super(context, R.layout.item_icon_checkable_list, set);
        this.layout = R.layout.item_icon_checkable_list;
        this.context = context;
    }

    public IconCheckableListAdapter(Context context, int layout, CheckableItem[] set) {
        super(context, layout, set);
        this.layout = layout;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHold hold;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(layout, null);
            hold = new ViewHold();
            hold.imgIcon = (ImageView) convertView.findViewById(R.id.imgIconCheckable);
            hold.imgCheck = (ImageView) convertView.findViewById(R.id.imgIconCheckableSelect);
            hold.txtName = (TextView) convertView.findViewById(R.id.txtIconCheckableName);
            convertView.setTag(hold);
        }
        else hold = (ViewHold) convertView.getTag();
        final CheckableItem checkableItem = getItem(position);
        if (checkableItem != null) {
            hold.txtName.setText(checkableItem.getName());
            //判斷是否已選取
            hold.imgCheck.setImageResource(checkableItem.isCheck() ? R.drawable.check_box_on : R.drawable.check_box_off);
            //顯示圖示
            if (checkableItem.getNumber() > 0) {
                hold.imgIcon.setImageResource(checkableItem.getNumber());
            }
        }
        return convertView;
    }

}