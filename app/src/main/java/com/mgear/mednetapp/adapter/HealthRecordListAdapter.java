package com.mgear.mednetapp.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.internal.service.Common;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.health.HealthBaseSet;
import com.mgear.mednetapp.entity.health.HealthBloodOxygenSet;
import com.mgear.mednetapp.entity.health.HealthBloodPressureSet;
import com.mgear.mednetapp.entity.health.HealthBloodSugarSet;
import com.mgear.mednetapp.entity.health.HealthBodySet;
import com.mgear.mednetapp.entity.health.HealthDrinkingSet;
import com.mgear.mednetapp.entity.health.HealthSleepingSet;
import com.mgear.mednetapp.entity.health.HealthStepSet;
import com.mgear.mednetapp.interfaces.IRefreshHeader;
import com.mgear.mednetapp.interfaces.OnItemClickListener;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class HealthRecordListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE_HEADER = 0;  //有Header
    public static final int TYPE_FOOTER = -1;  //有Footer的

    private Context mContext;
    private IRefreshHeader mRefreshHeader;
    private OnItemClickListener mItemClickListener;
    private LayoutInflater mInflater;
    private JSONArray healthList = new JSONArray();
    private String mHealthType;


    public HealthRecordListAdapter(Context context, String healthType) {
        this.mContext = context;
        this.mItemClickListener = (OnItemClickListener) context;
        this.mInflater = LayoutInflater.from(context);
        this.mHealthType = healthType;
        healthList = new JSONArray();

    }

    public void setAdapterList(JSONArray list) {
        this.healthList = list;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.health_list_cell, parent, false);
        //Log.i("debug","onCreateViewHolder"+" "+viewType);
        if (viewType == TYPE_HEADER) {
            return new HeaderViewHolder(mRefreshHeader.getHeaderView());
        } else {
            JSONObject healthData = healthList.optJSONObject(viewType - 1);
            if (!"".equals(healthData.optString("title"))) {
                view = mInflater.inflate(R.layout.health_record_cell_group, parent, false);
                GroupHolder groupHolder = new GroupHolder(view);
                return groupHolder;
            } else {
                view = mInflater.inflate(R.layout.health_record_cell_child, parent, false);
                ItemHolder viewHolder = new ItemHolder(view);
                return viewHolder;
            }

        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (getItemViewType(position) == TYPE_HEADER) {
            return;
        } else {
            int i = position - 1;
            JSONObject healthData = healthList.optJSONObject(i);
            if (holder instanceof ItemHolder) {
                Calendar cal = Calendar.getInstance();
                ItemHolder itemHolder = (ItemHolder) holder;
                HealthBaseSet baseSet = null;
                //Log.i("debug", "mHealthType = " + mHealthType);
                switch (mHealthType) {
                    case "1":
                        HealthBloodSugarSet bloodSugarSet = new HealthBloodSugarSet(mContext, healthData);
                        baseSet = bloodSugarSet;
                        itemHolder.value1.setText(bloodSugarSet.getValue());
                        itemHolder.value2.setText(bloodSugarSet.getValue2());
                        itemHolder.unit1.setText("mg/dl");

                        itemHolder.valueLayout3.setVisibility(View.GONE);
                        itemHolder.value3.setVisibility(View.GONE);
                        itemHolder.unit2.setVisibility(View.GONE);
                        itemHolder.unit3.setVisibility(View.GONE);
                        break;
                    case "2":
                        HealthBloodPressureSet bloodPressureSet = new HealthBloodPressureSet(mContext, healthData);
                        baseSet = bloodPressureSet;
                        itemHolder.value1.setText(Integer.toString(bloodPressureSet.getSbp()));
                        itemHolder.value2.setText(Integer.toString(bloodPressureSet.getDbp()));
                        itemHolder.value3.setText(Integer.toString(bloodPressureSet.getHr()));
                        itemHolder.unit1.setText("收縮");
                        itemHolder.unit2.setText("舒張");
                        itemHolder.unit3.setText("脈搏");
                        break;
                    case "3":
                        HealthBloodOxygenSet bloodOxygenSet = new HealthBloodOxygenSet(mContext, healthData);
                        baseSet = bloodOxygenSet;
                        itemHolder.value1.setText(baseSet.getValue());
                        itemHolder.unit1.setText("%");
                        itemHolder.valueLayout2.setVisibility(View.GONE);
                        itemHolder.valueLayout3.setVisibility(View.GONE);
                        itemHolder.value2.setVisibility(View.GONE);
                        itemHolder.value3.setVisibility(View.GONE);
                        itemHolder.unit2.setVisibility(View.GONE);
                        itemHolder.unit3.setVisibility(View.GONE);
                        break;
                    case "4":
                        HealthDrinkingSet drinkingSet = new HealthDrinkingSet(mContext, healthData);
                        baseSet = drinkingSet;
                        itemHolder.value1.setText(Integer.toString(drinkingSet.getToday()));
                        itemHolder.unit1.setText("ml");
                        itemHolder.value2.setText(Integer.toString(drinkingSet.getCupCount()));
                        itemHolder.unit2.setText("杯");
                        itemHolder.valueLayout3.setVisibility(View.GONE);
                        itemHolder.value3.setVisibility(View.GONE);
                        itemHolder.unit3.setVisibility(View.GONE);
                        itemHolder.time.setVisibility(View.GONE);
                        break;
                    case "5":
                        HealthSleepingSet sleepingSet = new HealthSleepingSet(mContext, healthData);
                        baseSet = sleepingSet;

                        CommonFunc.UTC2Date(baseSet.getmTime());
                        cal.setTime(CommonFunc.UTC2Date(sleepingSet.getSleepTime()));
                        String hour = cal.get(Calendar.HOUR_OF_DAY) > 9 ? cal.get(Calendar.HOUR_OF_DAY) + "" : "0" + cal.get(Calendar.HOUR_OF_DAY);
                        String minute = cal.get(Calendar.MINUTE) > 9 ? cal.get(Calendar.MINUTE) + "" : "0" + cal.get(Calendar.MINUTE);
                        itemHolder.value1.setText(hour + ":" + minute);

                        CommonFunc.UTC2Date(baseSet.getmTime());
                        cal.setTime(CommonFunc.UTC2Date(sleepingSet.getGetupTime()));
                        hour = cal.get(Calendar.HOUR_OF_DAY) > 9 ? cal.get(Calendar.HOUR_OF_DAY) + "" : "0" + cal.get(Calendar.HOUR_OF_DAY);
                        minute = cal.get(Calendar.MINUTE) > 9 ? cal.get(Calendar.MINUTE) + "" : "0" + cal.get(Calendar.MINUTE);
                        itemHolder.value2.setText(hour + ":" + minute);
                        itemHolder.value3.setText(sleepingSet.getSleepingHours().toString());
                        itemHolder.unit1.setText("就寢");
                        itemHolder.unit2.setText("起床");
                        itemHolder.unit3.setText("小時");
                        itemHolder.time.setVisibility(View.GONE);
                        break;
                    case "6":
                        HealthStepSet stepSet = new HealthStepSet(mContext, healthData);
                        baseSet = stepSet;
                        itemHolder.value1.setText(Integer.toString(stepSet.getStepCount()));
                        itemHolder.value2.setText(Double.toString(stepSet.getDistance()));
                        itemHolder.unit1.setText("步");
                        itemHolder.unit2.setText("km");
                        itemHolder.valueLayout3.setVisibility(View.GONE);
                        itemHolder.value3.setVisibility(View.GONE);
                        itemHolder.unit3.setVisibility(View.GONE);
                        itemHolder.time.setVisibility(View.GONE);

                        if (stepSet.getStepCount() >= MegaApplication.TargetStep) {
                            itemHolder.statusImg.setImageResource(R.drawable.ic_status_green);
                        } else {
                            itemHolder.statusImg.setImageResource(R.drawable.ic_status_blue);
                        }

                        break;
                    case "7":
                        HealthBodySet bodySet = new HealthBodySet(mContext, healthData);
                        baseSet = bodySet;
                        itemHolder.value1.setText(bodySet.getBh() + "");
                        itemHolder.value2.setText(bodySet.getBw() + "");
                        itemHolder.value3.setText(bodySet.getBmi() + "");
                        itemHolder.unit1.setText("cm");
                        itemHolder.unit2.setText("kg");
                        itemHolder.unit3.setText("BMI");
                        break;
                }


                Date mtime = CommonFunc.UTC2Date(baseSet.getmTime());
                if (mtime != null) {
                    cal.setTime(mtime);
                    int day = cal.get(Calendar.DATE);
                    int hour = cal.get(Calendar.HOUR_OF_DAY);
                    int minute = cal.get(Calendar.MINUTE);
                    itemHolder.date.setText((day > 9 ? day + "" : "0" + day) + "日");
                    itemHolder.time.setText((hour > 9 ? hour + "" : "0" + hour) + ":" + (minute > 9 ? minute + "" : "0" + minute));
                } else {

                }
                itemHolder.status.setText(baseSet.getStatusTitle().replaceAll("●  ", ""));
                itemHolder.status.setTextColor(baseSet.getColor());
                //Log.i("debug", "itemHolder.status " + itemHolder.status.getText());
                switch (baseSet.getColorID()) {
                    case R.color.status_normal:
                        //Log.i("debug", "status_normal");
                        itemHolder.statusImg.setImageResource(R.drawable.ic_status_green);
                        break;
                    case R.color.status_non_enough:
                        //Log.i("debug", "status_non_enough");
                        itemHolder.statusImg.setImageResource(R.drawable.ic_status_blue);
                        break;
                    case R.color.status_warning:
                        //Log.i("debug", "status_warning");
                        itemHolder.statusImg.setImageResource(R.drawable.ic_status_yellow);
                        break;
                    case R.color.status_danger:
                        //Log.i("debug", "status_danger");
                        itemHolder.statusImg.setImageResource(R.drawable.ic_status_red);
                        break;
                }

                itemHolder.cell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Log.i("debug", "i = " + i + ", healthData = " + healthData);
                        if (!"6".equals(mHealthType))
                            mItemClickListener.onItemClick(view, i, healthData);
                    }
                });

            } else if (holder instanceof GroupHolder) {
                GroupHolder groupHolder = (GroupHolder) holder;
                groupHolder.title.setText(healthData.optString("title"));
            }
        }
    }


    @Override
    public int getItemCount() {
        //Log.i("debug","mHomeList.length() = "+healthList.length());
        return healthList != null ? healthList.length() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {
        int index = position - 1;
        if (position == 0) {
            //Log.i("debug","viewType= "+TYPE_HEADER);
            return TYPE_HEADER;
        } else if (position == -1) {
            //最后一个,应该加载Footer
            return TYPE_FOOTER;
        } else {
            try {
                //Log.i("debug","position = "+position);
                int viewType = index;
                //Log.i("debug","index = "+index);
                return position;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return position;
        }
    }

    public void setRefreshHeader(IRefreshHeader header) {
        if (header != null) {
            mRefreshHeader = header;
        }
    }


    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ItemHolder extends RecyclerView.ViewHolder {
        public LinearLayout cell;
        public LinearLayout valueLayout1, valueLayout2, valueLayout3;
        public TextView date;
        public TextView time;
        public TextView value1, value2, value3;
        public TextView unit1, unit2, unit3;
        public TextView status;
        public ImageView statusImg;

        public ItemHolder(View itemView) {
            super(itemView);
            cell = itemView.findViewById(R.id.record_cell);
            date = itemView.findViewById(R.id.record_cell_date);
            time = itemView.findViewById(R.id.record_cell_time);
            value1 = itemView.findViewById(R.id.record_cell_value1);
            value2 = itemView.findViewById(R.id.record_cell_value2);
            value3 = itemView.findViewById(R.id.record_cell_value3);
            unit1 = itemView.findViewById(R.id.record_cell_unit1);
            unit2 = itemView.findViewById(R.id.record_cell_unit2);
            unit3 = itemView.findViewById(R.id.record_cell_unit3);
            status = itemView.findViewById(R.id.record_cell_status);
            statusImg = itemView.findViewById(R.id.record_cell_status_img);
            valueLayout1 = itemView.findViewById(R.id.value_layout1);
            valueLayout2 = itemView.findViewById(R.id.value_layout2);
            valueLayout3 = itemView.findViewById(R.id.value_layout3);
        }
    }

    class GroupHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public GroupHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
        }
    }

    public String parseDate(String fullDate) {
        String retDate = "";
        if (fullDate != null && fullDate.indexOf("T") >= 0) {
            retDate = fullDate.substring(0, fullDate.indexOf("T"));
        }
        return retDate;
    }

}
