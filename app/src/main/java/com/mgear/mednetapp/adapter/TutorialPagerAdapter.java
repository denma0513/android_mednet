package com.mgear.mednetapp.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.mgear.mednetapp.fragments.organization.TutorialEndFragment;
import com.mgear.mednetapp.fragments.organization.TutorialHavoFragment;
import com.mgear.mednetapp.fragments.organization.TutorialHomeFragment;
import com.mgear.mednetapp.fragments.organization.TutorialStartFragment;
import com.mgear.mednetapp.fragments.organization.TutorialStatusFragment;
import com.mgear.mednetapp.fragments.organization.TutorialTestsFragment;

/**
 * Created by Jye on 2017/6/8.
 * class: TutorialPagerAdapter
 */
public class TutorialPagerAdapter extends FragmentPagerAdapter {

    private TutorialStartFragment startFragment;
    private TutorialHavoFragment havoFragment;
    //private TutorialCareFragment careFragment;
    private TutorialHomeFragment homeFragment;
   // private TutorialMapFragment mapFragment;
    private TutorialTestsFragment testsFragment;
    private TutorialStatusFragment statusFragment;
    private TutorialEndFragment endFragment;

    public TutorialPagerAdapter(FragmentManager fm) {
        super(fm);
        startFragment = new TutorialStartFragment();
        havoFragment = new TutorialHavoFragment();
        //careFragment = new TutorialCareFragment();
        homeFragment = new TutorialHomeFragment();
        //mapFragment = new TutorialMapFragment();
        testsFragment = new TutorialTestsFragment();
        statusFragment = new TutorialStatusFragment();
        endFragment = new TutorialEndFragment();
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position) {
            case 0:
                return startFragment;
            case 1:
                return havoFragment;
            //case 2:
            //    return careFragment;
            case 2:
                return homeFragment;
            //case 3:
                //return mapFragment;
            case 3:
                return testsFragment;
            case 4:
                return statusFragment;
            case 5:
                return endFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}