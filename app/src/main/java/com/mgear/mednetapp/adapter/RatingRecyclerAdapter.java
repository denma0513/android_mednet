package com.mgear.mednetapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.SurveyItem;
import com.mgear.mednetapp.entity.organization.SurveyItemSet;

/**
 * Created by Jye on 2017/8/1.
 * class: RatingRecyclerAdapter
 */
public class RatingRecyclerAdapter extends RecyclerView.Adapter<RatingRecyclerAdapter.ViewHolder> {

    private boolean isFinish;
    private SurveyItemSet dataSet;

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text;
        RatingBar rating;

        ViewHolder(View v) {
            super(v);
            text = (TextView) v.findViewById(R.id.txtSurveyItem);
            rating = (RatingBar) v.findViewById(R.id.rbSurveyItem);
        }
    }

    public RatingRecyclerAdapter(SurveyItemSet data, boolean finish) {
        dataSet = data;
        isFinish = finish;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RatingRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_survey, parent, false);
        // set the view's size, margins, padding and layout parameters
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your data set at this position
        // - replace the contents of the view with that element
        holder.rating.setNumStars(5);
        final SurveyItem item = dataSet.get(position);
        holder.text.setText(item.getName());
        holder.rating.setTag(item.getId());
        //取得評分
        float score = item.getRating();
        if (score > 0f) {
            holder.rating.setRating(score);
            if (isFinish)
                holder.rating.setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}