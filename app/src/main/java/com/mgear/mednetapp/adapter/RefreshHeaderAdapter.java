package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.interfaces.IRefreshHeader;
import com.mgear.mednetapp.views.ArrowRefreshHeader;
import com.mgear.mednetapp.views.RefreshHeaderRecyclerView;

public class RefreshHeaderAdapter extends RecyclerView.Adapter<RefreshHeaderAdapter.RefreshViewHolder> {

    public static final int TYPE_HEADER = 0;  //有Header
    public static final int TYPE_FOOTER = 1;  //有Footer的
    public static final int TYPE_NORMAL = 2;  //header和footer皆無

    private Context mContext;
    private View mHeaderView;
    private View mFooterView;
    private IRefreshHeader mRefreshHeader;
    private LayoutInflater mInflater;


    public RefreshHeaderAdapter(Context context) {
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public RefreshViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return new RefreshViewHolder(mRefreshHeader.getHeaderView());
        }

        View view = mInflater.inflate(R.layout.main_cell, parent, false);
        TextView testText = view.findViewById(R.id.testText);
        RefreshViewHolder holder = new RefreshViewHolder(view);
        holder.testText = testText;

        return new RefreshViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RefreshViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_NORMAL) {
            if (holder instanceof RefreshViewHolder) {
                ImageView img = holder.image;
                TextView testText = holder.testText;
                return;
            }
            return;
        } else if (getItemViewType(position) == TYPE_HEADER) {
            return;
        } else {
            return;
        }
    }


    @Override
    public int getItemCount() {
        return 7;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else if (position == getItemCount() - 1) {
            //最后一个,应该加载Footer
            return TYPE_FOOTER;
        } else {
            return TYPE_NORMAL;
        }
    }

    public void setRefreshHeader(IRefreshHeader header) {
        if (header != null) {
            mRefreshHeader = header;
        }
    }

    class RefreshViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView testText;

        public RefreshViewHolder(View itemView) {
            super(itemView);
        }
    }
}
