package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;


/**
 * Created by Jye on 2017/8/19.
 * class: HtmlListAdapter
 */
public class HtmlListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final int layout;

    public HtmlListAdapter(Context context, String[] set) {
        super(context, R.layout.item_message_list, set);
        this.layout = R.layout.item_message_list;
        this.context = context;
    }

    public HtmlListAdapter(Context context, int layout, String[] set) {
        super(context, layout, set);
        this.layout = layout;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView txt;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(layout, null);
            txt = (TextView) convertView.findViewById(R.id.txtMessageListContent);
            convertView.setTag(txt);
        }
        else txt = (TextView) convertView.getTag();
        txt.setText(CommonFunc.fromHtml(getItem(position)));
        return convertView;
    }

}