package com.mgear.mednetapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mgear.mednetapp.R;


/**
 * Created by Jye on 2017/8/25.
 * class: ListAdapter
 */
public class ListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final int layout;

    public ListAdapter(Context context, String[] content) {
        super(context, R.layout.expandable_list_item, content);
        this.layout = R.layout.expandable_list_item;
        this.context = context;
    }

    public ListAdapter(Context context, int layout, String[] content) {
        super(context, layout, content);
        this.layout = layout;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView hold;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(layout, null);
            hold = (TextView) convertView.findViewById(R.id.expandedListItem);
            convertView.setTag(hold);
        }
        else hold = (TextView) convertView.getTag();
        hold.setText(getItem(position));
        return convertView;
    }

}