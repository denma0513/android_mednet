package com.mgear.mednetapp.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.mgear.mednetapp.fragments.organization.WelcomeStartAppFragment;

import java.util.ArrayList;

/**
 * Created by Dennis.
 * class: TutorialPagerStartAppAdapter
 */
public class WelcomePagerStartAppAdapter extends FragmentPagerAdapter {

    //private TutorialStartAppFragment fragment;
    private ArrayList<WelcomeStartAppFragment> listFragment;

    public WelcomePagerStartAppAdapter(FragmentManager fm) {
        super(fm);
        listFragment = new ArrayList<WelcomeStartAppFragment>();
        for (int i = 0; i < 7; i++) {
            WelcomeStartAppFragment fragment = new WelcomeStartAppFragment();
            listFragment.add(fragment);
        }

    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        WelcomeStartAppFragment fragment = listFragment.get(position);
        fragment.setPage(position);
        return fragment;
    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}