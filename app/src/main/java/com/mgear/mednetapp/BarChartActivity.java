package com.mgear.mednetapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.android.gms.common.internal.service.Common;
import com.google.android.youtube.player.internal.u;
import com.mgear.mednetapp.adapter.HealthRecordListAdapter;
import com.mgear.mednetapp.entity.health.HealthBaseSet;
import com.mgear.mednetapp.entity.health.HealthBloodOxygenSet;
import com.mgear.mednetapp.entity.health.HealthBloodPressureSet;
import com.mgear.mednetapp.entity.health.HealthBloodSugarSet;
import com.mgear.mednetapp.entity.health.HealthBodySet;
import com.mgear.mednetapp.entity.health.HealthDrinkingSet;
import com.mgear.mednetapp.entity.health.HealthSleepingSet;
import com.mgear.mednetapp.entity.health.HealthStepSet;
import com.mgear.mednetapp.entity.organization.BeaconInfoSet;
import com.mgear.mednetapp.enums.RefreshViewEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.OnItemClickListener;
import com.mgear.mednetapp.interfaces.OnRefreshListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.DayAxisValueFormatter;
import com.mgear.mednetapp.utils.MyValueFormatter;
import com.mgear.mednetapp.views.RefreshHeaderRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BarChartActivity extends BarBase implements OnChartValueSelectedListener, TaskPost2ExecuteImpl, View.OnClickListener, OnItemClickListener {
    private static String TAG = "debug";
    private static final int FINISH = 1;
    private final static int R_WEEK = 7;
    private final static int R_MONTH = 30;
    private final static int R_HALF = 180;
    private int searchRange = 7;
    private int xCount = 7;
    private int rangeType = R_WEEK;

    private BarChart chart;
    private CandleStickChart mCandleStickChart;
    private LineChart mLineChart;
    private XAxis xAxis;
    private YAxis leftAxis, rightAxis;
    private LimitLine ll1, ll2;
    private DayAxisValueFormatter xAxisFormatter;

    protected int screenHeight;
    protected int screenWidth;
    protected DisplayMetrics displayMetrics = new DisplayMetrics();
    private TextView mChangeTitle, mTopTitle, mHealthContent, mSelectDate;
    private ImageView mChangeBack, mChangeNext, mTopBack;
    private TextView mChangeWeek, mChangeMomth, mChangeHalfYear;

    private RefreshHeaderRecyclerView mRecordList;
    private HealthRecordListAdapter mAdapter;

    private JSONArray healthAdapterList;
    private JSONArray healthDetailList;
    private String mHealthType;
    private HealthBaseSet baseSet;
    private Date searchDate;
    private int maxDay;
    private HashMap<String, JSONObject> mDateMap;
    private HashMap<String, JSONArray> healthDetailMap;
    private int dataCount;
    private String unit, type;
    private float emtpty;
    private float lim1 = 0, lim2 = 0;

    @SuppressLint("HandlerLeak")
    private Handler sHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == FINISH) {
                mRecordList.refreshComplete();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_barchart);

        mDateMap = new HashMap<String, JSONObject>();
        healthDetailMap = new HashMap<String, JSONArray>();

        mChangeTitle = findViewById(R.id.change_title);
        mChangeBack = findViewById(R.id.change_back);
        mChangeNext = findViewById(R.id.change_next);

        mRecordList = findViewById(R.id.record_list);
        mSelectDate = findViewById(R.id.select_date);

        mTopBack = findViewById(R.id.top_back);
        mTopTitle = findViewById(R.id.top_title);
        mHealthContent = findViewById(R.id.health_content);

        mChangeWeek = findViewById(R.id.change_week);
        mChangeMomth = findViewById(R.id.change_month);
        mChangeHalfYear = findViewById(R.id.change_half_year);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mHealthType = extras.getString("healthType");
        }

        mChangeTitle.setOnClickListener(this);
        mChangeBack.setOnClickListener(this);
        mChangeNext.setOnClickListener(this);
        mTopBack.setOnClickListener(this);
        mChangeWeek.setOnClickListener(this);
        mChangeMomth.setOnClickListener(this);
        mChangeHalfYear.setOnClickListener(this);


        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        healthAdapterList = new JSONArray();
        healthDetailList = new JSONArray();

        //Log.i("debug", "onCreate: screenHeight = " + screenHeight);
        mCandleStickChart = findViewById(R.id.candle_stick_chart);
        chart = findViewById(R.id.chart1);
        mLineChart = findViewById(R.id.line_chart);

        if ("1".equals(mHealthType) || "2".equals(mHealthType) || "3".equals(mHealthType) || "7".equals(mHealthType)) {
            mCandleStickChart.setVisibility(View.GONE);
            chart.setVisibility(View.GONE);
            mLineChart.setVisibility(View.VISIBLE);
            mLineChart.getLayoutParams().height = (int) (screenHeight / 2.5);
            mLineChart.setDrawBorders(false);
            mLineChart.getDescription().setEnabled(false);
            mLineChart.setScaleXEnabled(false);
            mLineChart.setScaleYEnabled(false);
            mLineChart.setOnChartValueSelectedListener(this);
            mLineChart.setDragEnabled(false);
            //mLineChart.setMaxVisibleValueCount(250);
            mLineChart.setPinchZoom(false);
            mLineChart.setDrawGridBackground(false);
            mLineChart.setTouchscreenBlocksFocus(false);
            //禁止向XY軸拉伸
            mLineChart.setScaleXEnabled(false);
            mLineChart.setScaleYEnabled(false);
        } else if ("0".equals(mHealthType)) {
            mCandleStickChart.setVisibility(View.VISIBLE);
            chart.setVisibility(View.GONE);
            mLineChart.setVisibility(View.GONE);
            mCandleStickChart.getLayoutParams().height = (int) (screenHeight / 2.5);
            mCandleStickChart.setDrawBorders(false);
            mCandleStickChart.getDescription().setEnabled(false);
            mCandleStickChart.setScaleXEnabled(false);
            mCandleStickChart.setScaleYEnabled(false);
            mCandleStickChart.setOnChartValueSelectedListener(this);
            mCandleStickChart.setDragEnabled(false);
            mCandleStickChart.setMaxVisibleValueCount(250);
            mCandleStickChart.setPinchZoom(false);
            mCandleStickChart.setDrawGridBackground(false);
            mCandleStickChart.setTouchscreenBlocksFocus(false);
            //禁止向XY軸拉伸
            mCandleStickChart.setScaleXEnabled(false);
            mCandleStickChart.setScaleYEnabled(false);
        } else {
            mCandleStickChart.setVisibility(View.GONE);
            chart.setVisibility(View.VISIBLE);
            mLineChart.setVisibility(View.GONE);
            chart.getLayoutParams().height = (int) (screenHeight / 2.5);
            chart.setDrawBorders(false);
            chart.getDescription().setEnabled(false);
            chart.setScaleXEnabled(false);
            chart.setScaleYEnabled(false);
            chart.setOnChartValueSelectedListener(this);
            chart.setDragEnabled(false);
            chart.setMaxVisibleValueCount(60);
            chart.setPinchZoom(false);
            chart.setDrawGridBackground(false);
            chart.setTouchscreenBlocksFocus(false);
            //禁止向XY軸拉伸
            chart.setScaleXEnabled(false);
            chart.setScaleYEnabled(false);
            chart.invalidate();
        }

        mHealthContent.setText(CommonFunc.switchHealthContent(mHealthType));

        //取得健康資料
        switchHealth();
        getHealthListData();


        mAdapter = new HealthRecordListAdapter(this, mHealthType);
        mAdapter.setAdapterList(healthDetailList);
        mRecordList.setLayoutManager(new LinearLayoutManager(this));
        mRecordList.setAdapter(mAdapter, RefreshViewEnum.healthRecordListRefreshView);
        mRecordList.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                Message message = Message.obtain();
                message.what = FINISH;
                sHandler.sendMessage(message);

            }
        });

    }


    private void switchHealth() {
        switch (mHealthType) {
            case "1":
                unit = "mg/dl";
                type = "INT";
                mTopTitle.setText("血糖");
                if (leftAxis != null) {
                    if (dataCount <= 0) leftAxis.setAxisMaximum(250);
                    else leftAxis.resetAxisMaximum();
                }
                lim1 = 120f;
                lim2 = 80f;
                break;
            case "2":
                unit = "mmHg";
                type = "INT";
                mTopTitle.setText("血壓");
                if (leftAxis != null) {
                    if (dataCount <= 0) leftAxis.setAxisMaximum(200);
                    else leftAxis.resetAxisMaximum();
                }
                lim1 = 140f;
                lim2 = 80f;
                break;
            case "3":
                unit = "%";
                type = "INT";
                mTopTitle.setText("血氧");
                if (leftAxis != null) {
                    if (dataCount <= 0) leftAxis.setAxisMaximum(100);
                    else leftAxis.resetAxisMaximum();
                }
                break;
            case "4":
                unit = "ml";
                type = "INT";
                mTopTitle.setText("飲水量");
                if (leftAxis != null) {
                    if (dataCount <= 0)
                        leftAxis.setAxisMaximum(MegaApplication.WaterCapacity + 1000);
                    else leftAxis.resetAxisMaximum();
                }
                lim1 = MegaApplication.WaterCapacity;
                break;
            case "5":
                unit = "hr";
                type = "FLOAT";
                mTopTitle.setText("睡眠時間");
                if (leftAxis != null) {
                    if (dataCount <= 0) leftAxis.setAxisMaximum(10);
                    else leftAxis.resetAxisMaximum();
                }
                lim1 = 9f;
                lim2 = 6f;
                break;
            case "6":
                unit = "步";
                type = "INT";
                mTopTitle.setText("步數統計");
                if (leftAxis != null) {
                    if (dataCount <= 0) leftAxis.setAxisMaximum(MegaApplication.TargetStep + 2000);
                    else leftAxis.resetAxisMaximum();
                }
                lim1 = MegaApplication.TargetStep;
                break;
            case "7":
                unit = "KG";
                type = "FLOAT";
                mTopTitle.setText("體位");
                if (leftAxis != null) {
                    if (dataCount <= 0) leftAxis.setAxisMaximum(100);
                    else leftAxis.resetAxisMaximum();
                }
                break;
        }

        if (leftAxis != null) {
            ll1 = new LimitLine(lim1, "");
            ll2 = new LimitLine(lim2, "");
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setTypeface(tfRegular);
            ll1.setLineColor(getResources().getColor(R.color.status_normal));
            ll2.enableDashedLine(10f, 10f, 0f);
            ll2.setTypeface(tfRegular);
            ll2.setLineColor(getResources().getColor(R.color.home_title_blue));
            leftAxis.setDrawLimitLinesBehindData(true);
            if (lim1 > 0) {
                //ll1.setLabel(lim1 + "");
                //ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
                leftAxis.addLimitLine(ll1);
            }
            if (lim2 > 0)
                leftAxis.addLimitLine(ll2);
        }


    }

    private void setView() {
        xAxisFormatter = new DayAxisValueFormatter(chart);
        ((DayAxisValueFormatter) xAxisFormatter).setMaxDate(xCount);
        xAxisFormatter.setMaxDate(maxDay);
        xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(tfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(searchRange);
        xAxis.setValueFormatter(xAxisFormatter);

        unit = "";
        type = "";

        leftAxis = chart.getAxisLeft();

        //leftAxis.setMinWidth(5000);


        ValueFormatter custom = new MyValueFormatter(unit, type);
        leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(custom);
        leftAxis.setAxisMinimum(0);
        leftAxis.setDrawGridLines(true);
        //leftAxis.setValueFormatter();

        rightAxis = chart.getAxisRight();
        rightAxis.setDrawLabels(false);
        rightAxis.setDrawGridLines(false);

        switchHealth();

        Legend l = chart.getLegend();
        l.setEnabled(false);

        chart.invalidate();
    }

    private void setLineChartView() {
        xAxisFormatter = new DayAxisValueFormatter(mLineChart);
        ((DayAxisValueFormatter) xAxisFormatter).setMaxDate(xCount);
        xAxisFormatter.setMaxDate(maxDay);
        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(tfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(searchRange);
        xAxis.setValueFormatter(xAxisFormatter);

        unit = "";
        type = "";

        leftAxis = mLineChart.getAxisLeft();
        switchHealth();

        ValueFormatter custom = new MyValueFormatter(unit, type);
        leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(custom);

        if ("1".equals(mHealthType)) {
            leftAxis.setAxisMinimum(0);
            leftAxis.setAxisMaximum(230);
        } else if ("2".equals(mHealthType)) {
            leftAxis.setAxisMinimum(40);
            leftAxis.setAxisMaximum(200);
        } else if ("3".equals(mHealthType)) {
            leftAxis.setAxisMinimum(80);
            leftAxis.setAxisMaximum(110);
        } else if ("7".equals(mHealthType)) {
            float max = 0;
            float min = 0;
            if (healthAdapterList.length() == 0) {
                emtpty = 60;
                max = 60;
                min = 60;
            } else {
                for (int i = 0; i < healthAdapterList.length(); i++) {
                    HealthBodySet bodySet = new HealthBodySet(this, healthAdapterList.optJSONObject(i));
                    emtpty = Float.parseFloat(bodySet.getBw().toString());
                    //Log.i(TAG, "bodySet.getBw() = " + bodySet.getBw() + " min = " + min);

                    if (max == 0.0) max = Float.parseFloat(bodySet.getBw().toString());
                    if (min == 0.0) min = Float.parseFloat(bodySet.getBw().toString());

                    if (bodySet.getBw() > max)
                        max = Float.parseFloat(bodySet.getBw().toString());
                    if (bodySet.getBw() < min && bodySet.getBw() > 0.0)
                        min = Float.parseFloat(bodySet.getBw().toString());
                }
            }
            //Log.i(TAG, "setLineChartView: max = " + max + " min = " + min);
            leftAxis.resetAxisMaximum();
            leftAxis.setAxisMinimum(min - 20);
            leftAxis.setAxisMaximum(max + 20);

        }

        leftAxis.setDrawGridLines(true);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        rightAxis = mLineChart.getAxisRight();
        rightAxis.setDrawLabels(false);
        rightAxis.setDrawGridLines(false);
//        rightAxis.enableGridDashedLine(10f, 10f, 0f);


        Legend l = mLineChart.getLegend();
        //l.setEnabled(false);
        l.setForm(Legend.LegendForm.LINE);
        l.setTypeface(tfLight);
        l.setTextSize(11f);
        l.setTextColor(getResources().getColor(R.color.home_title_black));
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);

        mLineChart.invalidate();
    }

    private void setCandleStickView() {
        xAxisFormatter = new DayAxisValueFormatter(mCandleStickChart);
        ((DayAxisValueFormatter) xAxisFormatter).setMaxDate(xCount);
        xAxisFormatter.setMaxDate(maxDay);
        XAxis xAxis = mCandleStickChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(tfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(searchRange);
        xAxis.setValueFormatter(xAxisFormatter);

        unit = "";
        type = "";

        leftAxis = mCandleStickChart.getAxisLeft();
        switchHealth();

        ValueFormatter custom = new MyValueFormatter(unit, type);
        leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(custom);
        leftAxis.setAxisMinimum(0);
        leftAxis.setDrawGridLines(true);

        rightAxis = mCandleStickChart.getAxisRight();
        rightAxis.setDrawLabels(false);
        rightAxis.setDrawGridLines(false);

        Legend l = mCandleStickChart.getLegend();
        l.setEnabled(false);

        mCandleStickChart.invalidate();
    }

    private void getHealthListData() {
        getHealthListData(0);
    }

    private void getHealthListData(int add) {
        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        Calendar calToday = Calendar.getInstance();
        calToday.setTime(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String startTime = "";
        String endTime = "";
        maxDay = 0;
        int month = 0;
        String title = "";
        try {

            mSelectDate.setVisibility(View.GONE);
            healthDetailList = new JSONArray();
            if (mAdapter != null) {
                mAdapter.setAdapterList(healthDetailList);
                mAdapter.notifyDataSetChanged();
            }

            if (searchDate == null) {
                searchDate = new Date();
                cal.setTime(searchDate);

                switch (rangeType) {
                    case R_WEEK: {
                        maxDay = cal.get(Calendar.DATE);
                        month = cal.get(Calendar.MONTH) + 1;
                        endTime = cal.get(Calendar.YEAR) + "/" + (month <= 9 ? "0" + month : "" + month) + "/" + (maxDay <= 9 ? "0" + maxDay : "" + maxDay) + "T23:59:59Z";
                        break;
                    }
                    case R_MONTH: {
                        maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                        month = cal.get(Calendar.MONTH) + 1;
                        endTime = cal.get(Calendar.YEAR) + "/" + (month <= 9 ? "0" + month : "" + month) + "/" + (maxDay <= 9 ? "0" + maxDay : "" + maxDay) + "T23:59:59Z";
                        break;
                    }
                    case R_HALF: {
                        maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                        month = cal.get(Calendar.MONTH) + 1;
                        endTime = cal.get(Calendar.YEAR) + "/" + (month <= 9 ? "0" + month : "" + month) + "/" + (maxDay <= 9 ? "0" + maxDay : "" + maxDay) + "T23:59:59Z";
                        break;
                    }
                }

                title = (month <= 9 ? "0" + month : "" + month) + "/" + (maxDay <= 9 ? "0" + maxDay : "" + maxDay);
            } else {
                cal.setTime(searchDate);
                if (CommonFunc.isSameDay(cal, calToday) && add > 0) {
                    Log.i(TAG, "已經到最後一天了 ");
                    CommonFunc.showToast(this, "已經到最後一天囉!");
                    return;
                }

                mSelectDate.setText("");
                healthDetailMap = new HashMap<String, JSONArray>();
                mAdapter.notifyDataSetChanged();

                Log.i(TAG, "getHealthListData: rangeType = " + rangeType + " addDays = " + add);

                switch (rangeType) {
                    case R_WEEK: {
                        cal.add(Calendar.DATE, add);
                        searchDate = cal.getTime();
                        maxDay = cal.get(Calendar.DATE);
                        break;
                    }
                    case R_MONTH: {
                        cal.add(Calendar.MONTH, add);
                        searchDate = cal.getTime();
                        maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                        break;
                    }
                    case R_HALF: {
                        cal.add(Calendar.MONTH, add);
                        searchDate = cal.getTime();
                        maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                        break;
                    }
                }

                month = cal.get(Calendar.MONTH) + 1;
                endTime = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + maxDay + "T23:59:59Z";
                title = (month <= 9 ? "0" + month : "" + month) + "/" + (maxDay <= 9 ? "0" + maxDay : "" + maxDay);
            }

            cal.setTime(sdf.parse(endTime));
            switch (rangeType) {
                case R_WEEK: {
                    cal.add(Calendar.DATE, -6);
                    int startDay = cal.get(Calendar.DATE);
                    month = cal.get(Calendar.MONTH) + 1;
                    title = (month <= 9 ? "0" + month : "" + month) + "/" + (startDay <= 9 ? "0" + startDay : "" + startDay) + "~" + title;
                    mChangeTitle.setText(title);
                    startTime = cal.get(Calendar.YEAR) + "/" + (month <= 9 ? "0" + month : "" + month) + "/" + (startDay <= 9 ? "0" + startDay : "" + startDay) + "T00:00:00Z";
                    break;
                }
                case R_MONTH: {
                    int startDay = 1;
                    month = cal.get(Calendar.MONTH) + 1;
                    title = (month <= 9 ? "0" + month : "" + month) + "/" + (startDay <= 9 ? "0" + startDay : "" + startDay) + "~" + title;
                    mChangeTitle.setText(title);
                    startTime = cal.get(Calendar.YEAR) + "/" + (month <= 9 ? "0" + month : "" + month) + "/" + (startDay <= 9 ? "0" + startDay : "" + startDay) + "T00:00:00Z";
                    break;
                }
                case R_HALF: {
                    cal.add(Calendar.MONTH, -5);
                    int startDay = 1;
                    month = cal.get(Calendar.MONTH) + 1;
                    title = (month <= 9 ? "0" + month : "" + month) + "/" + (startDay <= 9 ? "0" + startDay : "" + startDay) + "~" + title;
                    mChangeTitle.setText(title);
                    startTime = cal.get(Calendar.YEAR) + "/" + (month <= 9 ? "0" + month : "" + month) + "/" + (startDay <= 9 ? "0" + startDay : "" + startDay) + "T00:00:00Z";
                    break;
                }
            }

            cal.setTime(sdf.parse(startTime));
            cal2.setTime(sdf.parse(endTime));


            long diff = cal2.getTimeInMillis() - cal.getTimeInMillis();
            //Log.i(TAG, "diff = " + diff + "   (diff / (1000 * 60 * 60 * 24)): " + (diff / (1000 * 60 * 60 * 24)));
            xCount = (int) (diff / (1000 * 60 * 60 * 24)) + 1;


            Log.d("debug", "mHealthType = " + mHealthType + "  : " + startTime + " ~ " + endTime + " xCount = " + xCount);
            if ("4".equals(mHealthType) || "6".equals(mHealthType)) {
                new CustomerApiTask(this, this, true, getString(R.string.msg_data_loading)
                        , TaskEnum.MednetQueryMeasureValues, true).execute(mHealthType, startTime, endTime, "true");
            } else {
                new CustomerApiTask(this, this, true, getString(R.string.msg_data_loading)
                        , TaskEnum.MednetQueryMeasureValues, true).execute(mHealthType, startTime, endTime, "false");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setData(JSONArray list) {
        int count = list.length();
        //Log.i("debug", "setData: count=  " + count);
        float start = 1f;
        int blue = ColorTemplate.rgb("#1F81C4");
        int gray = ColorTemplate.rgb("#ff999999");
        int[] colorArray = new int[list.length()];

        ArrayList<BarEntry> values = new ArrayList<>();
        float maxValue = 0;
        for (int i = 0; i < list.length(); i++) {
            float compareValue = 0;
            colorArray[i] = gray;
            JSONObject healthData = list.optJSONObject(i);
            Log.i(TAG, "setData: healthData = " + healthData);
            switch (mHealthType) {
                case "1":
                    HealthBloodSugarSet bloodSugarSet = new HealthBloodSugarSet(this, healthData);
                    baseSet = bloodSugarSet;
                    break;
                case "2":
                    HealthBloodPressureSet bloodPressureSet = new HealthBloodPressureSet(this, healthData);
                    baseSet = bloodPressureSet;
                    break;
                case "3":
                    HealthBloodOxygenSet bloodOxygenSet = new HealthBloodOxygenSet(this, healthData);
                    baseSet = bloodOxygenSet;
                    break;
                case "4":
                    HealthDrinkingSet drinkingSet = new HealthDrinkingSet(this, healthData);
                    baseSet = drinkingSet;
                    compareValue = MegaApplication.WaterCapacity;

                    if (maxValue < MegaApplication.WaterCapacity)
                        maxValue = MegaApplication.WaterCapacity + 500;

                    if (Integer.parseInt(baseSet.getValue()) > maxValue)
                        maxValue = Integer.parseInt(baseSet.getValue());

                    break;
                case "5":
                    HealthSleepingSet sleepingSet = new HealthSleepingSet(this, healthData);
                    baseSet = sleepingSet;
                    if (maxValue < 9)
                        maxValue = 9;
                    if (Double.parseDouble(baseSet.getValue()) > maxValue)
                        maxValue = (float) Double.parseDouble(baseSet.getValue());
                    colorArray[i] = blue;
                    break;
                case "6":
                    HealthStepSet stepSet = new HealthStepSet(this, healthData);
                    baseSet = stepSet;
                    compareValue = MegaApplication.TargetStep;

                    if (maxValue < MegaApplication.TargetStep)
                        maxValue = MegaApplication.TargetStep + 1000;

                    if (Integer.parseInt(baseSet.getValue()) > maxValue)
                        maxValue = Integer.parseInt(baseSet.getValue());

                    break;
                case "7":
                    HealthBodySet bodySet = new HealthBodySet(this, healthData);
                    baseSet = bodySet;
                    break;
            }

            float val;
            //Log.i(TAG, "setData: baseSet.getHealthType() = " + mHealthType);

            val = (float) Double.parseDouble(baseSet.getValue());
            //Log.i(TAG, " val = " + val + " compareValue = " + compareValue);
            if (compareValue != 0 && val >= compareValue) {
                colorArray[i] = blue;
            }
            Log.i(TAG, "setData: i = " + i + "   val = " + val);
            values.add(new BarEntry(i, val));
        }

        rightAxis.resetAxisMaximum();
        rightAxis.setAxisMaximum(maxValue);
        leftAxis.resetAxisMaximum();
        leftAxis.setAxisMaximum(maxValue);


        BarDataSet set1;

        if (chart.getData() != null && chart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.setColors(colorArray);
            //set2 = (BarDataSet) chart.getData().getDataSetByIndex(1);
            //set2.setValues(values);

            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();

        } else {
            set1 = new BarDataSet(values, "");
            set1.setDrawIcons(false);
            set1.setHighLightAlpha(0);
            set1.setDrawValues(false);
            set1.setColors(ColorTemplate.MATERIAL_COLORS);
            set1.setColors(colorArray);
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(12f);
            data.setValueTextColor(getResources().getColor(R.color.home_title_black));
            data.setValueTypeface(tfLight);
            data.setBarWidth(0.25f);
            chart.setData(data);
        }

        if (rightAxis != null && ("4".equals(mHealthType) || "6".equals(mHealthType))) {
            LimitLine ll = new LimitLine(lim1, "");
            ll.setLineColor(Color.parseColor("#00000000"));
            ll.setLineWidth(0);
            //ll.enableDashedLine(10f, 10f, 0f);
            ll.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            ll.setTypeface(tfRegular);
            ll.setLabel(lim1 + "");
            rightAxis.addLimitLine(ll);
            Log.i(TAG, "setData: ll.getYOffset() = " + ll.getYOffset());
        }
    }

    private void setmLineChartData(JSONArray list) {
        ArrayList<Entry> values1 = new ArrayList<>();
        ArrayList<Entry> values2 = new ArrayList<>();
        ArrayList<Entry> entries1 = new ArrayList<>();
        ArrayList<Entry> entries2 = new ArrayList<>();
        String label = "", label2 = "";

        mLineChart.resetTracking();

        for (int i = 0; i < searchRange; i++) {
            try {
                Log.i(TAG, "i = : " + i);
                Calendar cal = Calendar.getInstance();
                cal.setTime(searchDate);
                Calendar tempCel = Calendar.getInstance();
                tempCel.setTime(cal.getTime());
                tempCel.add(Calendar.DATE, (i - 6));
                int month = tempCel.get(Calendar.MONTH) + 1;
                int date = tempCel.get(Calendar.DATE);
                String yyyymmdd = tempCel.get(Calendar.YEAR) + (month > 9 ? "" + month : "0" + month) + (date > 9 ? "" + date : "0" + date);

                Log.i(TAG, "healthDetailMap.get(yyyymmdd) = : " + healthDetailMap.get(yyyymmdd));

                if (healthDetailMap.get(yyyymmdd) != null) {
                    //這一天有資料
                    JSONArray detailList = healthDetailMap.get(yyyymmdd);
                    float val = 0, val2 = 0;
                    float valEntries = 0, val2Entries = 0;
                    for (int j = 0; j < detailList.length(); j++) {
                        JSONObject obj = detailList.optJSONObject(j);

                        if ("1".equals(mHealthType)) {
                            label = "飯後";
                            label2 = "飯前";

                            HealthBloodSugarSet bloodPressureSet = new HealthBloodSugarSet(this, obj);
                            baseSet = bloodPressureSet;
                            if ("飯後".equals(baseSet.getValue2()) && val < Integer.parseInt(baseSet.getValue())) {
                                val = Integer.parseInt(baseSet.getValue());
                            } else if ("飯前".equals(baseSet.getValue2()) && val2 < Integer.parseInt(baseSet.getValue())) {
                                val2 = Integer.parseInt(baseSet.getValue());
                            }
                        } else if ("2".equals(mHealthType)) {
                            label = "收縮壓";
                            label2 = "舒張壓";


                            JSONObject healthData = list.optJSONObject(i);
                            Log.i(TAG, "setData: healthData = " + healthData);
                            HealthBloodPressureSet bloodPressureSet = new HealthBloodPressureSet(this, healthData);
                            baseSet = bloodPressureSet;

                            if (baseSet.getValue() != null && baseSet.getValue().contains("/")) {
                                String[] varArr = baseSet.getValue().split("/");
                                val = (float) Double.parseDouble(varArr[0]);
                                val2 = (float) Double.parseDouble(varArr[1]);
                            }

                        } else if ("3".equals(mHealthType)) {
                            label = "血氧";
                            //label2 = "飯前";

                            HealthBloodOxygenSet bloodOxygenSet = new HealthBloodOxygenSet(this, obj);
                            baseSet = bloodOxygenSet;
                            if (val < Float.parseFloat(baseSet.getValue()))
                                val = Integer.parseInt(baseSet.getValue());
                        } else if ("7".equals(mHealthType)) {
                            label = "體重";
                            HealthBodySet bloodBodySet = new HealthBodySet(this, obj);
                            baseSet = bloodBodySet;
                            Log.i(TAG, "baseSet.getValue() = " + Float.parseFloat(baseSet.getValue()));
                            if (val < Float.parseFloat(baseSet.getValue())) {
                                val = Float.parseFloat(baseSet.getValue());
                            }
                        }
                    }

                    Log.i(TAG, " val = " + val + " val2 = " + val2);
                    values1.add(new Entry(i, val));
                    values2.add(new Entry(i, val2));

                    if (val != 0) {
                        entries1.add(new Entry(i, val));
                    }
                    if (val2 != 0) {
                        entries2.add(new Entry(i, val2));
                    }

                } else {
                    values1.add(new Entry(i, emtpty));
                    values2.add(new Entry(i, emtpty));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Log.i(TAG, "setmLineChartData: values  " + values1.size());
        //Log.i(TAG, "setmLineChartData: entries1  " + entries1.size());
        Log.i(TAG, "label " + label);
        Log.i(TAG, "label2 " + label2);


        LineDataSet set1 = new LineDataSet(values1, "");
        LineDataSet set2 = new LineDataSet(values2, "");
        LineDataSet set1Entries = new LineDataSet(entries1, label);
        LineDataSet set2Entries = new LineDataSet(entries2, label2);

        //Log.i(TAG, "setmLineChartData: values  " + set1.getValues().size());

        set1.setAxisDependency(YAxis.AxisDependency.RIGHT);
        set1.setColor(Color.parseColor("#00000000"));
        set1.setDrawCircles(false);
        set1.setCircleColor(Color.parseColor("#00000000"));
        set1.setFillColor(Color.parseColor("#00000000"));
        set1.setHighLightColor(Color.parseColor("#00000000"));

        set1Entries.setAxisDependency(YAxis.AxisDependency.LEFT);
        set1Entries.setColor(getResources().getColor(R.color.home_title_blue));
        set1Entries.setCircleColor(getResources().getColor(R.color.home_title_blue));
        set1Entries.setLineWidth(2f);
        set1Entries.setCircleRadius(3f);
        set1Entries.setFillAlpha(65);
        set1Entries.setFillColor(getResources().getColor(R.color.home_title_blue));
        set1Entries.setHighLightColor(Color.parseColor("#00000000"));
        set1Entries.setDrawCircleHole(false);


        Log.i(TAG, "setmLineChartData: mHealthType = " + mHealthType);

        if ("1".equals(mHealthType) || "2".equals(mHealthType)) {
            set2.setAxisDependency(YAxis.AxisDependency.RIGHT);
            set2.setColor(Color.parseColor("#00000000"));
            set2.setDrawCircles(false);
            set2.setCircleColor(Color.parseColor("#00000000"));
            set2.setFillColor(Color.parseColor("#00000000"));
            set2.setHighLightColor(Color.parseColor("#00000000"));

            set2Entries.setAxisDependency(YAxis.AxisDependency.LEFT);
            set2Entries.setColor(getResources().getColor(R.color.home_title_orange));
            set2Entries.setCircleColor(getResources().getColor(R.color.home_title_orange));
            set2Entries.setLineWidth(2f);
            set2Entries.setCircleRadius(3f);
            set2Entries.setFillAlpha(65);
            set2Entries.setFillColor(getResources().getColor(R.color.home_title_orange));
            set2Entries.setHighLightColor(Color.parseColor("#00000000"));
            set2Entries.setDrawCircleHole(false);
        }


        //隱藏數值
        set1.setDrawValues(false);
        set2.setDrawValues(false);
        set1Entries.setDrawValues(false);
        set2Entries.setDrawValues(false);

        if ("1".equals(mHealthType)) {
            LineData data = new LineData(set1, set2, set1Entries, set2Entries);
            mLineChart.setVisibleXRangeMaximum(250);
            mLineChart.setData(data);
            mLineChart.invalidate();
        } else if ("2".equals(mHealthType)) {
            LineData data = new LineData(set1, set2, set1Entries, set2Entries);
            mLineChart.setVisibleXRangeMaximum(200);
            mLineChart.setData(data);
            mLineChart.invalidate();
        } else if ("3".equals(mHealthType)) {
            LineData data = new LineData(set1, set1Entries);
            mLineChart.setVisibleXRangeMaximum(110);
            mLineChart.setData(data);
            mLineChart.invalidate();
        } else if ("7".equals(mHealthType)) {
            LineData data = new LineData(set1, set1Entries);
            mLineChart.setVisibleXRangeMaximum(200);
            mLineChart.setData(data);
            mLineChart.invalidate();
        }


    }

    private void setCandleStickData(JSONArray list) {

        ArrayList<CandleEntry> values = new ArrayList<>();
        mCandleStickChart.resetTracking();


        for (int i = 0; i < list.length(); i++) {

            JSONObject healthData = list.optJSONObject(i);
            Log.i(TAG, "setData: healthData = " + healthData);
            HealthBloodPressureSet bloodPressureSet = new HealthBloodPressureSet(this, healthData);
            baseSet = bloodPressureSet;

            float val, val2 = 0;
            Log.i(TAG, "setData: baseSet.getHealthType() = " + mHealthType);
            if (baseSet.getValue() != null && baseSet.getValue().contains("/")) {
                String[] varArr = baseSet.getValue().split("/");
                val = (float) Double.parseDouble(varArr[0]);
                val2 = (float) Double.parseDouble(varArr[1]);
                values.add(new CandleEntry(i, val2 + 80, val, val2, val, getResources().getDrawable(R.drawable.ic_game_mission)));
            } else {
                values.add(new CandleEntry(i, 0, 0, 0, 0, getResources().getDrawable(R.drawable.ic_game_mission)));
            }

        }

        Log.i(TAG, "setCandleStickData: values  " + values.size());

        CandleDataSet set1 = new CandleDataSet(values, "Data Set");

        set1.setDrawIcons(false);
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
//        set1.setColor(Color.rgb(80, 80, 80));
        set1.setShadowColor(Color.parseColor("#00000000"));
        set1.setHighLightColor(Color.parseColor("#00000000"));
        set1.setShadowWidth(0.5f);
        set1.setDecreasingColor(getResources().getColor(R.color.home_title_blue));
        set1.setDecreasingPaintStyle(Paint.Style.FILL);
        set1.setIncreasingColor(getResources().getColor(R.color.home_title_blue));
        set1.setIncreasingPaintStyle(Paint.Style.FILL);
//        set1.setIncreasingColor(Color.rgb(122, 242, 84));
//        set1.setIncreasingPaintStyle(Paint.Style.STROKE);
        set1.setNeutralColor(Color.BLUE);

        //set1.setHighlightLineWidth(1f);

        //隱藏數值
        set1.setDrawValues(false);

        CandleData data = new CandleData(set1);
        mCandleStickChart.setVisibleXRangeMaximum(250);
        mCandleStickChart.setData(data);
        mCandleStickChart.invalidate();
    }

    private void clickIndex(int index) {
        JSONObject healthData = healthAdapterList.optJSONObject(index);
        //Log.i(TAG, "setData: healthData = " + healthData);
        //Log.i(TAG, "setData: healthData = " + healthDetailMap);
        //Log.i(TAG, "setData: healthData = " + healthDetailMap.get(healthData.optString("yyyymmdd")));

        if (healthData.isNull("yyyymmdd"))
            return;

        String yyyymmdd = healthData.optString("yyyymmdd");
        healthDetailList = healthDetailMap.get(yyyymmdd);
        //Log.i(TAG, "onValueSelected: yyyymmdd = " + yyyymmdd);
        //Log.i(TAG, "onValueSelected: healthDetailList = " + healthDetailList);

        int status = 0;

        if (healthDetailList.length() > 0) {
            for (int i = 0; i < healthDetailList.length(); i++) {
                JSONObject obj = healthDetailList.optJSONObject(i);
                switch (mHealthType) {
                    case "1":
                        HealthBloodSugarSet bloodSugarSet = new HealthBloodSugarSet(this, obj);
                        baseSet = bloodSugarSet;
                        break;
                    case "2":
                        HealthBloodPressureSet bloodPressureSet = new HealthBloodPressureSet(this, obj);
                        baseSet = bloodPressureSet;
                        break;
                    case "3":
                        HealthBloodOxygenSet bloodOxygenSet = new HealthBloodOxygenSet(this, obj);
                        baseSet = bloodOxygenSet;
                        break;
                    case "4":
                        HealthDrinkingSet drinkingSet = new HealthDrinkingSet(this, obj);
                        baseSet = drinkingSet;
                        break;
                    case "5":
                        HealthSleepingSet sleepingSet = new HealthSleepingSet(this, obj);
                        baseSet = sleepingSet;
                        break;
                    case "6":
                        HealthStepSet stepSet = new HealthStepSet(this, obj);
                        baseSet = stepSet;
                        break;
                    case "7":
                        HealthBodySet bodySet = new HealthBodySet(this, obj);
                        baseSet = bodySet;
                        break;
                }

                try {
                    Log.i(TAG, "switchHealthContent: " + baseSet.getStatus() + "  " + status);
                    if (baseSet.getStatus() != status && baseSet.getStatus() > status) {
                        if (baseSet.getStatus() == 1) {
                            status = baseSet.getStatus();
                        }
                        if (baseSet.getStatus() == 0) {
                            status = baseSet.getStatus();
                        }
                        if (baseSet.getStatus() == 2) {
                            status = baseSet.getStatus();
                        }
                        if (baseSet.getStatus() == 3) {
                            status = baseSet.getStatus();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        mHealthContent.setText(CommonFunc.switchHealthContent(mHealthType, status + ""));

        mAdapter.setAdapterList(healthDetailList);
        mAdapter.notifyDataSetChanged();

        mSelectDate.setText(yyyymmdd.substring(0, 4) + "年" + yyyymmdd.substring(4, 6) + "月" + yyyymmdd.substring(6, 8) + "日");
        mSelectDate.setVisibility(View.VISIBLE);

        //MPPointF position = chart.getPosition(e, YAxis.AxisDependency.LEFT);
        //MPPointF.recycleInstance(position);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_back: {
                Log.i(TAG, "onClick: change_back");
                getHealthListData(-searchRange);
                break;
            }
            case R.id.change_next: {
                Log.i(TAG, "onClick: change_next");
                getHealthListData(searchRange);
                break;
            }
            case R.id.top_back: {
                this.finish();
                break;
            }
            case R.id.change_week: {
                rangeType = R_WEEK;
                searchDate = null;
                searchRange = 7;
                xCount = 7;
                mChangeWeek.setTextColor(getResources().getColor(R.color.home_title_blue));
                mChangeMomth.setTextColor(getResources().getColor(R.color.home_title_black));
                mChangeHalfYear.setTextColor(getResources().getColor(R.color.home_title_black));
                getHealthListData();
                break;
            }
            case R.id.change_month: {
                rangeType = R_MONTH;
                searchDate = null;
                searchRange = 1;
                xCount = 30;
                mChangeWeek.setTextColor(getResources().getColor(R.color.home_title_black));
                mChangeMomth.setTextColor(getResources().getColor(R.color.home_title_blue));
                mChangeHalfYear.setTextColor(getResources().getColor(R.color.home_title_black));
                getHealthListData();
                break;
            }
            case R.id.change_half_year: {
                rangeType = R_HALF;
                searchDate = null;
                searchRange = 6;
                xCount = 180;
                mChangeWeek.setTextColor(getResources().getColor(R.color.home_title_black));
                mChangeMomth.setTextColor(getResources().getColor(R.color.home_title_black));
                mChangeHalfYear.setTextColor(getResources().getColor(R.color.home_title_blue));
                getHealthListData();
                break;
            }
        }

    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetQueryMeasureValues: {
                healthAdapterList = new JSONArray();
                if (result == null || result.optInt("state_code") != 200) {
                    return;
                }

                JSONArray list = result.optJSONArray("source");
                Calendar cal = Calendar.getInstance();

                dataCount = list.length();

                Log.i(TAG, "onPostExecute: dataCount  = " + dataCount + " xCount = " + xCount);

                for (int i = 0; i < dataCount; i++) {
                    JSONObject obj = list.optJSONObject(i);
                    try {
                        Date mTime = CommonFunc.UTC2Date(obj.optString("m_time"));
                        Date updatedAt = CommonFunc.UTC2Date(obj.optString("updated_at"));
                        cal.setTime(mTime);

                        int month = cal.get(Calendar.MONTH) + 1;
                        int date = cal.get(Calendar.DATE);
                        String yyyymmdd = cal.get(Calendar.YEAR) + (month > 9 ? "" + month : "0" + month) + (date > 9 ? "" + date : "0" + date);
                        //Log.i(TAG, "onPostExecute: yyyymmdd = " + yyyymmdd);
                        //cal.setTime(updatedAt);
                        if (mDateMap.get(yyyymmdd) != null) {
                            mDateMap.put(yyyymmdd, obj);
                        } else {
                            mDateMap.put(yyyymmdd, obj);
                        }

                        if (healthDetailMap.get(yyyymmdd) != null) {
                            JSONArray detailList = healthDetailMap.get(yyyymmdd);
                            detailList.put(obj);
                            healthDetailMap.put(yyyymmdd, detailList);
                        } else {
                            JSONArray detailList = new JSONArray();
                            detailList.put(obj);
                            healthDetailMap.put(yyyymmdd, detailList);
                        }

                        //healthAdapterList.put(obj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Log.i(TAG, "onPostExecute: healthDetailMap = " + healthDetailMap);
                Log.i(TAG, "onPostExecute: mDateMap = " + mDateMap);

                for (int i = 0; i < xCount; i++) {
                    try {
                        cal.setTime(searchDate);
                        Calendar tempCel = Calendar.getInstance();
                        tempCel.setTime(cal.getTime());
                        tempCel.add(Calendar.DATE, (i - (xCount - 1)));
                        int month = tempCel.get(Calendar.MONTH) + 1;
                        int date = tempCel.get(Calendar.DATE);
                        String yyyymmdd = tempCel.get(Calendar.YEAR) + (month > 9 ? "" + month : "0" + month) + (date > 9 ? "" + date : "0" + date);

                        Log.i(TAG, "onPostExecute: yyyymmdd = " + yyyymmdd);
                        if (mDateMap.get(yyyymmdd) != null) {
                            JSONObject obj = mDateMap.get(yyyymmdd);
                            obj.put("yyyymmdd", yyyymmdd);
                            healthAdapterList.put(obj);
                        } else {
                            healthAdapterList.put(new JSONObject());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                //顯示最後一筆紀錄的資料
                if (healthAdapterList != null) {
                    for (int i = healthAdapterList.length() - 1; i >= 0; i--) {
                        try {
                            JSONObject obj = healthAdapterList.getJSONObject(i);
                            if (obj.has("m_time")) {
                                clickIndex(i);
                                break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }


                if ("1".equals(mHealthType) || "2".equals(mHealthType) || "3".equals(mHealthType) || "7".equals(mHealthType)) {
                    setLineChartView();
                    setmLineChartData(healthAdapterList);
                } else if ("0".equals(mHealthType)) {
                    setCandleStickView();
                    setCandleStickData(healthAdapterList);
                } else {
                    setView();
                    setData(healthAdapterList);
                }


                Log.i(TAG, "MednetQueryMeasureValues: healthAdapterList =  " + healthAdapterList);
                break;
            }
        }
    }


    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "BarChartActivity");
    }

    private final RectF onValueSelectedRectF = new RectF();

    @SuppressLint("SetTextI18n")
    @Override
    public void onValueSelected(Entry e, Highlight h) {
        float x = e.getX();
        int index = (int) x;

        //JSONObject item = healthAdapterList.optJSONObject(index);
        clickIndex(index);
    }

    @Override
    public void onNothingSelected() {
    }

    @Override
    public void onItemClick(View view, int position, JSONObject data) {

    }
}
