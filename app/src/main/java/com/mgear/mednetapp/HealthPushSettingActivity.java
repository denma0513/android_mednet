package com.mgear.mednetapp;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.fragments.HealthSettingDetailFragment;
import com.mgear.mednetapp.fragments.HealthSettingFragment;
import com.mgear.mednetapp.fragments.HealthSettingSortFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;

import org.json.JSONObject;

import java.util.ArrayList;

public class HealthPushSettingActivity extends BaseActivity implements View.OnClickListener, IBackHandleIpml, AdapterView.OnItemClickListener {

    private static final String TAG = "HealthSetting";

    //畫面元件
    private ImageView mSettingTopBack;
    private TextView mSettingTopTitle, mSettingTopRightBtn;
    private FrameLayout mSettingContent, mSettingTimer;
    private RelativeLayout mSettingGoogleFit;
    private ListView mSettingList;

    //邏輯元件
    private MegaBaseFragment selectedFragment;
    private ArrayList<JSONObject> settingList = new ArrayList<JSONObject>();
    FragmentManager fragmentManager = getFragmentManager();
    private HealthSettingDetailFragment settingDetailFragment;
    private HealthSettingFragment healthSettingFragment;
    private HealthSettingSortFragment healthSettingSortFragment;

    public void exit() {
        if (selectedFragment == null || !selectedFragment.onBackPressed()) {
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                FragmentManager.BackStackEntry fragment = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1);
                if ("HealthSettingMain".equals(fragment.getName())) {
                    this.finish();
                } else if ("HealthSettingSort".equals(fragment.getName())) {
                    this.finish();
                } else if ("HealthSettingDetail".equals(fragment.getName())) {
                    mSettingTimer.setVisibility(View.GONE);
                    mSettingTopRightBtn.setVisibility(View.GONE);
                    mSettingTopTitle.setText("健康記錄設定");
                }
                getFragmentManager().popBackStack();
            }
        }
    }

    public void goDetail(String title, String type) {
        mSettingTimer.setVisibility(View.VISIBLE);
        mSettingTopTitle.setText(title + "提醒");
        mSettingTopRightBtn.setVisibility(View.VISIBLE);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        settingDetailFragment = new HealthSettingDetailFragment();
        settingDetailFragment.setHealthType(type);
        //Log.i("debug", "Log.i(\"debug\",\"goDetail\"+fragmentMain.isAdded());" + settingDetailFragment.isAdded());
        if (settingDetailFragment.isAdded()) { // 如果 home fragment 已經被 add 過，

            ft.hide(healthSettingFragment).show(settingDetailFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.setting_timer, settingDetailFragment, "HealthSettingDetail"); // 使用 add 方法。
            ft.addToBackStack("HealthSettingDetail");
        }
        ft.commit();

    }

    private void setMainView() {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        healthSettingFragment = new HealthSettingFragment();
        if (healthSettingFragment.isAdded()) { // 如果 home fragment 已經被 add 過，
            ft.show(healthSettingFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.setting_content, healthSettingFragment, "HealthSettingMain"); // 使用 add 方法。
            ft.addToBackStack("HealthSettingMain");
        }
        ft.commit();

        mSettingTopRightBtn.setVisibility(View.GONE);
        mSettingTopTitle.setText("健康記錄設定");
    }

    private void setOrderView() {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        healthSettingSortFragment = new HealthSettingSortFragment();
        if (healthSettingSortFragment.isAdded()) { // 如果 home fragment 已經被 add 過，
            ft.show(healthSettingSortFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.setting_content, healthSettingSortFragment, "HealthSettingSort"); // 使用 add 方法。
            ft.addToBackStack("HealthSettingSort");
        }
        ft.commit();

        mSettingTopRightBtn.setVisibility(View.GONE);
        mSettingTopTitle.setText("變更健康記錄順序");
    }

    private void newSetting() {
        settingDetailFragment.newSetting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.health_setting_main);
        Bundle extras = null;
        String settingType = "";
        try {
            extras = getIntent().getExtras();
            if (extras != null)
                settingType = extras.getString("settingType");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //settingTop = findViewById(R.id.setting_top);
        mSettingTopBack = findViewById(R.id.setting_top_back);
        mSettingTopTitle = findViewById(R.id.setting_top_title);
        mSettingTopRightBtn = findViewById(R.id.setting_top_right_btn);
        mSettingTimer = findViewById(R.id.setting_timer);

        if ("sort".equals(settingType)) {
            setOrderView();
        } else {
            setMainView();
        }

        mSettingTopBack.setOnClickListener(this::onClick);
        mSettingTopRightBtn.setOnClickListener(this::onClick);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_top_back:
                exit();
                break;
            case R.id.setting_top_right_btn:
                newSetting();
                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        mSettingTopTitle.setText(settingList.get(i).optString("healthName") + "提醒");
        mSettingTopRightBtn.setVisibility(View.VISIBLE);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        settingDetailFragment = new HealthSettingDetailFragment();
        settingDetailFragment.setHealthType(settingList.get(i).optString("healthType"));
        //fragmentMain = healthListFragment;
        if (settingDetailFragment.isAdded()) { // 如果 home fragment 已經被 add 過，
            ft.show(settingDetailFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.setting_content, settingDetailFragment, TAG); // 使用 add 方法。
            ft.addToBackStack(TAG);
        }
        ft.commit();

    }

    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {
        this.selectedFragment = selectedFrangment;
    }


    @Override
    public void onBackPressed() {
        exit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
    }

}
