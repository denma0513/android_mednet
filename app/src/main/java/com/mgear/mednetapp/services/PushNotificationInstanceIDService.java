package com.mgear.mednetapp.services;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class PushNotificationInstanceIDService extends FirebaseInstanceIdService {
    private static String TAG = "PushNotification";

    @Override
    public void onTokenRefresh(){
        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG,"refreshToken = "+refreshToken);
        sendRegistrationToServer(refreshToken);
    }

    private void sendRegistrationToServer(String Token) {
    }
}
