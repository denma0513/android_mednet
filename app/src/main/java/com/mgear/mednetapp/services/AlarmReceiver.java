package com.mgear.mednetapp.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.AlarmTimer;
import com.mgear.mednetapp.utils.CommonFunc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AlarmReceiver extends BroadcastReceiver {

    private NotificationManager m_notificationMgr = null;
    private static final int NOTIFICATION_FLAG = 3;
    private int alarmId = 0;
    private Long date;


    @Override
    public void onReceive(Context context, Intent intent) {
        String title = "";
        String content = "";
        String channelID = context.getString(R.string.cnannel_ID);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //CommonFunc.showToast(context, "e04");
        m_notificationMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        alarmId = intent.getIntExtra("alarmId", 0);
        date = intent.getLongExtra("date", 0);
        Log.i("debug", "alarmId = " + alarmId);
        Log.i("debug", "date = " + date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date datetime = new Date(date);
        Log.i("debug", "date  format = " + datetime);

        if (intent.getAction().equals(MegaApplication.TIMER_ACTION_REPEATING)) {
            Log.e("debug", "週期鬧鐘");
        } else if (intent.getAction().equals(MegaApplication.TIMER_ACTION)) {
            Log.e("debug", "定時鬧鐘");

            String type = "";
            if (Integer.toString(alarmId).length() > 1)
                type = Integer.toString(alarmId).substring(0, 1);

            switch (type) {
                case "1":
                    title = "血糖值紀錄";
                    content = "Hi ! 該紀錄血糖了！每日記錄血糖，了解血糖波動！";
                    break;
                case "2":
                    title = "血壓值紀錄";
                    content = "Hi ! 別忘了紀錄你的血壓值喔！";
                    break;
                case "3":
                    title = "血氧值紀錄";
                    content = "Hi ! 別忘了紀錄你的血氧值喔！";
                    break;
                case "4":
                    title = "飲水量值紀錄";
                    content = "Hi ! 你的身體已經乾旱缺水ing，在忙也別忘了喝水喔！";
                    break;
                case "5":
                    title = "睡眠時間";
                    content = "Hi ! 該睡覺囉！";
                    break;
                case "6":
                    return;
//                    break;
                case "7":
                    title = "體重紀錄";
                    content = "Hi ! 該紀錄體重囉！";
                    break;
            }
            //Log.e("debug", "type = " + type + " content= " + content);
            CommonFunc.showToast(context, content);


            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            cal.setTime(datetime);
            cal.add(Calendar.DATE, 1);

            Log.d("debug", "notificationID  = " + alarmId);
            Log.d("debug", "預計提醒時間  = " + sdf.format(cal.getTime()));
            AlarmTimer.setAlarmTimer(context, cal.getTimeInMillis(), MegaApplication.TIMER_ACTION, AlarmManager.INTERVAL_DAY, cal, alarmId);


            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification);
            Intent intent1 = new Intent(context, MainActivity.class);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent1, 0);

            Notification notify =
                    new NotificationCompat.Builder(context, channelID)
                            .setContentIntent(pendingIntent)
                            .setContentTitle(title)
                            .setContentText(content)
                            .setWhen(System.currentTimeMillis())
                            .setSmallIcon(R.drawable.ic_notification)
                            .setAutoCancel(true)
                            .setSound(soundUri)
                            .setPriority(NotificationManager.IMPORTANCE_HIGH)
                            .setDefaults(Notification.DEFAULT_VIBRATE).build();//震動

            notify.flags |= Notification.FLAG_AUTO_CANCEL;
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(NOTIFICATION_FLAG, notify);
            bitmap.recycle(); //回收bitmap
        }
    }


}
