package com.mgear.mednetapp.services;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.gms.common.internal.service.Common;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mgear.mednetapp.BuildConfig;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

import java.util.List;
import java.util.Map;

import static android.app.NotificationManager.*;

public class PushNotificationMessagingService extends FirebaseMessagingService {
    private static String TAG = "PushNotificationService";
    private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            if (isAppForeground()) {
                //sendNotificationＦ(remoteMessage.getData());
                String title = remoteMessage.getData().get("title");
                String content = remoteMessage.getData().get("content");
                String url = remoteMessage.getData().get("url");

                Log.i(TAG, "onMessageReceived: title = " + title);
                Log.i(TAG, "onMessageReceived: content = " + content);
                Log.i(TAG, "onMessageReceived: url = " + url);

                if (url != null && url.contains("mednetapp")) {
                    if (url.contains("addfriend")) {

                        //CommonFunc.showToast(this,"您收到一則好友邀請");
//                        Looper.prepare();
//                        AlertDialog dialog = new AlertDialog.Builder(getApplicationContext()).setCancelable(false)
//                                .setTitle(content)
//                                .setMessage("是否跳轉到交友頁面？")
//                                .setPositiveButton("否", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                                    }
//                                }).setPositiveButton("是", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                                    }
//                                }).create();
//
//                        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//                        dialog.show();
//                        Looper.loop();

                    }
                }

                Intent intent = new Intent(SignalService.PUSH_NOTIFICATION_NOTICE);

                Bundle bundle = new Bundle();
                bundle.putString("notification", "1");
                bundle.putString("title", title);
                bundle.putString("message", content);
                bundle.putString("url", url);
                bundle.putString("foreground", "1");
                intent.putExtras(bundle);

                intent.putExtra("title", title);
                intent.putExtra("content", content);
                intent.putExtra("url", url);
                broadcaster.sendBroadcast(intent);
                //Log.i("debug", "e04e04e04");
                sendBroadcast(intent);
                return;
            }
            sendNotification(remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.i(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.i(TAG, "Message Notification Body: " + remoteMessage.getNotification().getTitle());
            sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

    }


    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.i(TAG, "token " + s);
    }

    private void sendNotification(String title, String content) {

    }

    @SuppressLint("WrongConstant")
    private void sendNotification(Map messageData) {
        String title = CommonFunc.getString(messageData.get("title"), "您有一則新訊息");
        String content = CommonFunc.getString(messageData.get("content"), "診間呼叫");
        String sound = CommonFunc.getString(messageData.get("sound") == null ? "" : messageData.get("sound"));
        String url = CommonFunc.getString(messageData.get("url") == null ? "" : messageData.get("url"));
        String channelID = getString(R.string.cnannel_ID);
        String cnannelName = getString(R.string.cnannel_name);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        int pushId = 1;
        //Log.i(TAG, "sendNotification: soundUri1 = "+soundUri);
        if ("event_notice".equals(sound)) {
            soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.raw.event_notice);
        }
        //Log.i(TAG, "sendNotification: soundUri2 = "+soundUri);
        //Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.raw.telegraph);

        //設定intnet
        Intent intent = new Intent(this, MainActivity.class);

        Bundle bundle = new Bundle();
        //Log.i(TAG, url.contains("mednetapp")+"");

        if (url != null && url.contains("mednetapp")) {
            //mednetapp://shareapp?go=addfriend
            if (url.contains("addfriend")) {
                Log.i(TAG, "加好友囉");
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                bundle.putString("notification", "1");
                bundle.putString("title", title);
                bundle.putString("message", content);
                bundle.putString("url", url);
                bundle.putString("action", MegaApplication.ActionTypeStepCompetition);
            }
        } else {

            //先暫時用聲音檔案判斷是不是機構
            if ("event_notice".equals(sound)) {
                intent = new Intent(this, OrgMainActivity.class);
            } else {
                intent = new Intent(this, MainActivity.class);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            bundle.putString("notification", "1");
            bundle.putString("title", title);
            bundle.putString("message", content);
            if (!CommonFunc.isBlank(url)) {
                bundle.putString("action", MegaApplication.ActionTypeWebView);
                bundle.putString("actionTarget", url);
            }
            bundle.putString("foreground", "1");
        }

        intent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);


        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder;


        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build();

        //判断是否是8.0Android.O
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.e(TAG, "sendNotification: Android.O");
            NotificationChannel chan1 = new NotificationChannel(channelID, cnannelName, NotificationManager.IMPORTANCE_MAX);
            chan1.setLightColor(Color.GREEN);
            chan1.setVibrationPattern(new long[]{300, 300, 300});
            chan1.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            chan1.enableVibration(true);
            chan1.enableLights(true);
            chan1.setImportance(IMPORTANCE_MAX);
            chan1.setSound(soundUri, attributes);

            mNotificationManager.createNotificationChannel(chan1);
            mBuilder = new NotificationCompat.Builder(this, channelID);
        } else {
            mBuilder = new NotificationCompat.Builder(this);
        }


        mBuilder.setContentTitle(title)//设置通知栏标题
                .setContentText(content)//设置通知栏内容
                .setContentIntent(pendingIntent) //设置通知栏点击意图
                .setChannelId(channelID)
//                .setNumber(++pushNum) //设置通知集合的数量
                .setSound(soundUri)
                .setVibrate(new long[]{300, 300, 300})
                .setTicker(content + ":" + content) //通知首次出现在通知栏，带上升动画效果的
                .setWhen(System.currentTimeMillis())//通知产生的时间，会在通知信息里显示，一般是系统获取到的时间
                .setSmallIcon(R.drawable.ic_notification);//设置通知小ICON
        Notification notify = mBuilder.build();
        notify.flags |= Notification.FLAG_AUTO_CANCEL;

        mNotificationManager.notify((int) System.currentTimeMillis(), notify);


        try {
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), soundUri);
            //r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private boolean isAppForeground() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcessInfos = manager.getRunningAppProcesses();
        if (appProcessInfos == null || appProcessInfos.isEmpty()) {
            return false;
        }
        for (ActivityManager.RunningAppProcessInfo info : appProcessInfos) {
            //当前应用处于运行中，并且在前台
            if (info.processName.equals(getPackageName()) && info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }


}
