package com.mgear.mednetapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.MessageNoticeTask;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/07/09.
 * class: SignalService
 */
public class SignalService extends Service implements Runnable, TaskPostExecuteImpl {
    
    public static final String MOVE_ROOM_NOTICE = "MOVE_ROOM_NOTICE";
    public static final String WAIT_CONFIRM_NOTICE = "WAIT_CONFIRM_NOTICE";
    public static final String CALL_ENTER_ROOM_NOTICE = "CALL_ENTER_ROOM_NOTICE";
    public static final String ADDITION_SUCCESS_NOTICE = "ADDITION_SUCCESS_NOTICE";
    public static final String ROOM_CHECKIN_NOTICE = "ROOM_CHECKIN_NOTICE";
    public static final String PROCESS_END_NOTICE = "PROCESS_END_NOTICE";
    public static final String PROCESS_START_NOTICE = "PROCESS_START_NOTICE";
    public static final String PROCESS_PAUSE_NOTICE = "PROCESS_PAUSE_NOTICE";
    public static final String WAIT_TIMES_FINISH_NOTICE = "WAIT_TIMES_FINISH_NOTICE";
    public static final String SIMULATION_PATH_NOTICE = "SIMULATION_PATH_NOTICE";
    public static final String PUSH_NOTIFICATION_NOTICE = "PUSH_NOTIFICATION_NOTICE";

    private enum NoticeEnum {
        MOVE_ROOM_NOTICE,
        WAIT_CONFIRM_NOTICE,
        CALL_ENTER_ROOM_NOTICE,
        ADDITION_SUCCESS_NOTICE,
        ROOM_CHECKIN_NOTICE,
        PROCESS_END_NOTICE,
        PROCESS_START_NOTICE,
        PROCESS_PAUSE_NOTICE,
        WAIT_TIMES_FINISH_NOTICE,
        SIMULATION_PATH_NOTICE,
        PUSH_NOTIFICATION_NOTICE
    }

    private Handler mHandler;
    // Binder given to clients
    private final IBinder mBinder = new SignalLocalBinder();
    private String mDeviceId;
    private String mContact;
    private long mTicks = 0;
    private int mFailCount = 0;

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class SignalLocalBinder extends Binder {
        public SignalService getService() {
            // Return this instance of SignalRService so clients can call public methods
            return SignalService.this;
        }
    }

    public SignalService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the communication channel to the service.
        return mBinder;
    }

    @Override
    public void run() {
        //檢查目前的網路狀態
        if (CommonFunc.checkNetworkInfo(this)) {
            //以目前的時間戳取得訊息
            new MessageNoticeTask
                    (this, this, false, null).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mDeviceId, mContact, String.valueOf(mTicks));
        }
        else mHandler.postDelayed(this, 2000);
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        if (type == TaskEnum.MessageNoticeTask) {
            //判斷接收訊息是否成功
            if (result == null) {
                //沒有訊息等候兩秒再接收
                mHandler.postDelayed(this, 2000);
            }
            else {
                //取得推撥的訊息
                String[] value = (String[]) result;
                //判斷訊息陣列的基本大小
                int len = value.length;
                if (len >= 2) {
                    //取得目前訊息的時間戳
                    long nowTicks = Long.parseLong(value[1]);
                    //判斷訊息是否已推撥
                    if (mTicks != nowTicks) {
                        //紀錄是否成功發送訊息
                        boolean isSuccess = false;
                        //廣播訊息
                        Intent intent;
                        NoticeEnum notice = NoticeEnum.valueOf(value[0]);

                        switch (notice) {
                            case MOVE_ROOM_NOTICE:
                                //驗證訊息的長度
                                if (len == 8) {
                                    intent = new Intent(MOVE_ROOM_NOTICE);
                                    intent.putExtra("room", value[2]);
                                    intent.putExtra("examines", value[3]);
                                    intent.putExtra("seconds", Integer.valueOf(value[4]));
                                    intent.putExtra("waits", Integer.valueOf(value[5]));
                                    intent.putExtra("doubles", Boolean.valueOf(value[6]));
                                    intent.putExtra("showRoomWait", value[7]);
                                    sendBroadcast(intent);
                                    isSuccess = true;
                                }
                                break;
                            case ADDITION_SUCCESS_NOTICE:
                                //驗證訊息的長度
                                if (len == 4 || len == 5) {
                                    intent = new Intent(ADDITION_SUCCESS_NOTICE);
                                    intent.putExtra("addition", value[2]);
                                    intent.putExtra("result", Integer.valueOf(value[3]));
                                    if (len == 5) {
                                        intent.putExtra("showRoomWait", value[4]);
                                    }
                                    sendBroadcast(intent);
                                    isSuccess = true;
                                }
                                break;
                            case SIMULATION_PATH_NOTICE:
                                //驗證訊息的長度
                                if (len == 4 || len == 5) {
                                    intent = new Intent(SIMULATION_PATH_NOTICE);
                                    intent.putExtra("source", Integer.valueOf(value[2]));
                                    intent.putExtra("target", Integer.valueOf(value[3]));
                                    if (len == 5) {
                                        intent.putExtra("showRoomWait", value[4]);
                                    }
                                    sendBroadcast(intent);
                                    isSuccess = true;
                                }
                                break;
                            case WAIT_CONFIRM_NOTICE:
                                sendBroadcast(new Intent(WAIT_CONFIRM_NOTICE));
                                isSuccess = true;
                                break;
                            case CALL_ENTER_ROOM_NOTICE:
                                //驗證訊息的長度
                                if (len == 4 || len == 5) {
                                    intent = new Intent(CALL_ENTER_ROOM_NOTICE);
                                    intent.putExtra("room", value[2]);
                                    intent.putExtra("examines", value[3]);
                                    intent.putExtra("examines", value[3]);
                                    if (len == 5) {
                                        intent.putExtra("showRoomWait", value[4]);
                                    }
                                    sendBroadcast(intent);
                                    isSuccess = true;
                                }
                                break;
                            case ROOM_CHECKIN_NOTICE:
                                //驗證訊息的長度
                                if (len == 4 || len == 5) {
                                    intent = new Intent(ROOM_CHECKIN_NOTICE);
                                    intent.putExtra("room", value[2]);
                                    intent.putExtra("examines", value[3]);
                                    if (len == 5) {
                                        intent.putExtra("showRoomWait", value[4]);
                                    }
                                    sendBroadcast(intent);
                                    isSuccess = true;
                                }
                                break;
                            case PROCESS_END_NOTICE:
                                sendBroadcast(new Intent(PROCESS_END_NOTICE));
                                isSuccess = true;
                                break;
                            case PROCESS_START_NOTICE:
                                sendBroadcast(new Intent(PROCESS_START_NOTICE));
                                isSuccess = true;
                                break;
                            case PROCESS_PAUSE_NOTICE:
                                sendBroadcast(new Intent(PROCESS_PAUSE_NOTICE));
                                isSuccess = true;
                                break;
                            case WAIT_TIMES_FINISH_NOTICE:
                                //驗證訊息的長度
                                if (len == 3 || len == 4) {
                                    intent = new Intent(WAIT_TIMES_FINISH_NOTICE);
                                    intent.putExtra("status", Integer.valueOf(value[2]));
                                    if (len == 4) {
                                        intent.putExtra("showRoomWait", value[3]);
                                    }
                                    sendBroadcast(intent);
                                    isSuccess = true;
                                }
                                break;
                        }

                        //判斷是否成功發送訊息
                        if (isSuccess) {
                            //記錄目前訊息的時間戳
                            mTicks = nowTicks;
                            //清空失敗紀錄
                            mFailCount = 0;
                        }
                        else {
                            //紀錄發送訊息失敗的次數
                            mFailCount++;
                            //失敗次數超過6次則放棄該筆訊息
                            if (mFailCount >= 6) {
                                //記錄目前訊息的時間戳
                                mTicks = nowTicks;
                                mFailCount = 0;
                            }
                        }
                    }
                }

                //等候1秒再重新接收訊息
                mHandler.postDelayed(this, 1000);
            }
        }
    }

    /**
     * 開始接收訊號
     */
    public void login(String identity, String deviceId) {
        mContact = String.valueOf(identity);
        mDeviceId = deviceId;
        mHandler = new Handler(Looper.getMainLooper());
        mHandler.post(this);
    }

    /**
     * 結束接收訊號
     */
    public void logout() {
        mHandler.removeCallbacks(this);
        mContact = "0";
        mDeviceId = null;
    }

}