package com.mgear.mednetapp.algorithms.dijkstra;

/**
 * Created by Jye on 2017/7/9.
 * class: Vertex
 */
public class Vertex {

    final private int id;
    final private String name;

    public Vertex(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vertex other = (Vertex) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return name;
    }

}