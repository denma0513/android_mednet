package com.mgear.mednetapp.algorithms.dijkstra;

import java.util.List;

/**
 * Created by Jye on 2017/7/9.
 * class: Graph
 */
public class Graph {

    private final List<Vertex> vertexes;
    private final List<Edge> edges;

    public Graph(List<Vertex> vertexes, List<Edge> edges) {
        this.vertexes = vertexes;
        this.edges = edges;
    }

    List<Vertex> getVertexes() {
        return vertexes;
    }

    List<Edge> getEdges() {
        return edges;
    }

}