package com.mgear.mednetapp.algorithms.dijkstra;

/**
 * Created by Jye on 2017/7/9.
 * class: Edge
 */
public class Edge {

    private final String id;
    private final Vertex source;
    private final Vertex destination;
    private final int weight;

    public Edge(String id, Vertex source, Vertex destination, int weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    String getId() {
        return id;
    }

    Vertex getDestination() {
        return destination;
    }

    Vertex getSource() { return source; }

    int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return source + " " + destination;
    }

}