package com.mgear.mednetapp.managers;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Build;
import android.util.SparseArray;

import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.interfaces.organization.BeaconManagerImpl;
import com.mgear.mednetapp.interfaces.organization.BeaconReceiveImpl;
import com.mgear.mednetapp.entity.organization.BeaconInfo;
import com.mgear.mednetapp.entity.organization.BeaconInfoSet;
import com.mgear.mednetapp.entity.organization.BeaconNeighbor;
import com.mgear.mednetapp.entity.organization.BeaconReceiveValue;

import java.util.UUID;

import static com.mgear.mednetapp.entity.organization.BeaconInfo.INCREMENTAL;

/**
 * Created by Jye on 2017/6/19.
 * class: BeaconManager
 */
public class BeaconManager implements BeaconManagerImpl {

    //instance
    public static BeaconManager getInstance() {
        return manager;
    }
    private static BeaconManager manager;
    static {
        if (manager == null) {
            manager = new BeaconManager();
        }
    }

    private int nDeadLockKey;
    private BeaconInfo myPosition;
    private SparseArray<BeaconReceiveValue> mapValue;
    private BluetoothAdapter mBluetoothAdapter;
    private ScanCallback mScanCallback;
    private BluetoothAdapter.LeScanCallback mLeScanCallback;

    private BeaconManager() {
        mapValue = new SparseArray<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mScanCallback = new ScanCallback() {
                @Override
                @TargetApi(21)
                public void onScanResult(int callbackType, ScanResult result) {
                    ScanRecord scanRecord = result.getScanRecord();
                    if (scanRecord == null) {
                        return;
                    }
                    onLeScan(result.getDevice(), result.getRssi(), scanRecord.getBytes());
                }
            };
        }
        else {
            mLeScanCallback = this::onLeScan;
        }
    }

    private void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
        BeaconInfoSet beaconList = CustomerActivity.getBeaconList();
        if (beaconList == null || beaconList.size() == 0) return;
        //尋找開始的長度
        int startByte = 2;
        boolean patternFound = false;
        while (startByte <= 5) {
            if (((int) scanRecord[startByte + 2] & 0xff) == 0x02 && ((int) scanRecord[startByte + 3] & 0xff) == 0x15) {
                patternFound = true;
                break;
            }
            startByte++;
        }

        if (patternFound) {
            int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);
            int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);
            int key = major * INCREMENTAL + minor;
            BeaconInfo bi = beaconList.get(key);
            if (bi != null) {
                //int txPower = (scanRecord[startByte + 24]);
                //計算距離
                //double ca = calculateAccuracy(txPower, rssi);
                //強度增減
                int increase = bi.getIncrease();
                if (increase != 0) {
                    rssi += increase;
                }

                //將距離值加入清單
                BeaconReceiveValue brv = mapValue.get(key);
                if (brv == null)
                    mapValue.put(key, new BeaconReceiveValue(key, rssi));
                else
                    brv.add(rssi);
            }
        }
    }

    private boolean enableBluetooth(boolean enable) {
        if (enable) {
            if (!mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.enable();
            }
            return true;
        }
        else {
            if (mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.disable();
            }
            return false;
        }
    }

    /**
     * 開始接收資料
     * @param context Context
     * @param receiver Receiver
     * @param beaconList Beacons
     * @param scanMillisecond Scan
     * @param betweenMillisecond Between
     * @param parameter Parameter
     * @return T/F
     */
    @Override
    public boolean startScanBeacon(Context context, BeaconReceiveImpl receiver, final BeaconInfoSet beaconList, long scanMillisecond, long betweenMillisecond, long parameter) {
        if (mBluetoothAdapter == null) {
            BluetoothManager mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = mBluetoothManager.getAdapter();
            enableBluetooth(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBluetoothAdapter.getBluetoothLeScanner().startScan(mScanCallback);
        }
        else {
            //noinspection deprecation
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        }
        return true;
    }

    /**
     * 結束接收資料
     */
    @Override
    public void stopScanBeacon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(mScanCallback);
        }
        else {
            //noinspection deprecation
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    /**
     * 重新載入信標清單
     * @param beaconList BeaconList
     */
    public void resetBeaconList(final BeaconInfoSet beaconList) { }

    /**
     * 取得目前所在的信標
     * @param guideTarget Target
     * @return obj
     */
    @Override
    public BeaconInfo getPositionInBeacon(UUID guideTarget) {
        //判斷前次定位點是否存在
        BeaconReceiveValue preValue = null;
        if (myPosition != null && (preValue = mapValue.get(myPosition.getKey())) == null) {
            return null;
        }

        //取得信標清單
        BeaconInfoSet beaconList = CustomerActivity.getBeaconList();

        //判斷是否有接收到信標
        if (mapValue.size() > 0) {
            //取得目前最接近的五點位置
            int[] keys = new int[] { 0, 0, 0, 0, 0 };
            double[] vals = new double[] { 10000, 10000, 10000, 10000, 10000 };
            for(int i = 0; i < mapValue.size(); i++) {
                BeaconReceiveValue brv = mapValue.get(mapValue.keyAt(i));
                double tmp = brv.getResult();
                if (tmp < vals[0]) {
                    //移動後續的陣列
                    keys[4] = keys[3];
                    vals[4] = vals[3];
                    keys[3] = keys[2];
                    vals[3] = vals[2];
                    keys[2] = keys[1];
                    vals[2] = vals[1];
                    keys[1] = keys[0];
                    vals[1] = vals[0];
                    //設定最小值
                    keys[0] = brv.getKey();
                    vals[0] = tmp;
                }
                else if (tmp < vals[1]) {
                    //移動後續的陣列
                    keys[4] = keys[3];
                    vals[4] = vals[3];
                    keys[3] = keys[2];
                    vals[3] = vals[2];
                    keys[2] = keys[1];
                    vals[2] = vals[1];
                    //設定最小值
                    keys[1] = brv.getKey();
                    vals[1] = tmp;
                }
                else if (tmp < vals[2]) {
                    //移動後續的陣列
                    keys[4] = keys[3];
                    vals[4] = vals[3];
                    keys[3] = keys[2];
                    vals[3] = vals[2];
                    //設定最小值
                    keys[2] = brv.getKey();
                    vals[2] = tmp;
                }
                else if (tmp < vals[3]) {
                    //移動後續的陣列
                    keys[4] = keys[3];
                    vals[4] = vals[3];
                    //設定最小值
                    keys[3] = brv.getKey();
                    vals[3] = tmp;
                }
                else if (tmp < vals[4]) {
                    //設定最小值
                    keys[4] = brv.getKey();
                    vals[4] = tmp;
                }
            }

            if (myPosition == null) {
                //取得最近的位置
                myPosition = beaconList.get(keys[0]);
                mapValue.clear();
                return myPosition;
            }
            else if (myPosition.getKey() == keys[0]) {
                //位置沒有變更
                mapValue.clear();
                return myPosition;
            }
            else {
                //取得本次清單距離最近的值
                BeaconReceiveValue nearValue = mapValue.get(keys[0]);
                //判斷前次定位點離這次最近的定位點是否超過50公分
                assert preValue != null;
                if (Math.abs(preValue.getResult() - nearValue.getResult()) > 0.5d) {
                    //取得目前可移動的位置
                    SparseArray<BeaconNeighbor> neighbor = myPosition.getNeighborList();
                    if (neighbor.size() == 0) {
                        //無設定可移動清單，可隨意移動
                        myPosition = beaconList.get(keys[0]);
                        mapValue.clear();
                        return myPosition;
                    }
                    else {
                        //判斷是否可移動到最近的前三個點
                        for (int i = 0; i < 3; i++) {
                            BeaconNeighbor bn = neighbor.get(keys[i]);
                            if (bn != null) {
                                myPosition = beaconList.get(keys[i]);
                                mapValue.clear();
                                return myPosition;
                            }
                        }
                    }
                }
                else {
                    //判斷是否與前次無法移動的位置一致
                    if (nDeadLockKey == nearValue.getKey()) {
                        //強制移動
                        myPosition = beaconList.get(nDeadLockKey);
                        mapValue.clear();
                        nDeadLockKey = 0;
                        return myPosition;
                    }

                    //紀錄無法移動的位置
                    nDeadLockKey = nearValue.getKey();
                }

                //不可移動，回傳目前的位置
                mapValue.clear();
                return myPosition;
            }
        }
        return null;
    }

    /**
     * 計算精密度公式
     * @param txPower txPower
     * @param rssi rssi
     * @return accuracy
     */
    private double calculateAccuracy(int txPower, double rssi) {
        if (rssi == 0)
            return -1.0;
        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0)
            return Math.pow(ratio, 10);
        else
            return (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
    }

}