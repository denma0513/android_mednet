package com.mgear.mednetapp.managers;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.SparseArray;

import com.mgear.mednetapp.interfaces.organization.BeaconManagerImpl;
import com.mgear.mednetapp.interfaces.organization.BeaconReceiveImpl;
import com.mgear.mednetapp.entity.organization.BeaconInfo;
import com.mgear.mednetapp.entity.organization.BeaconInfoSet;
import com.mgear.mednetapp.entity.organization.BeaconNeighbor;
import com.mgear.mednetapp.entity.organization.BeaconReceiveValue;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Region;

import java.util.UUID;

import static com.mgear.mednetapp.entity.organization.BeaconInfo.INCREMENTAL;

/**
 * Created by Jye on 2017/7/18.
 * class: AltBeaconManager
 */
public class AltBeaconManager implements BeaconManagerImpl, BeaconConsumer {

    //IBEACON "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"
    private static final String IBEACON_FORMAT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";
    private static final String FILTER_UUID = "23bf5443-0a6f-4452-8bb3-f6e55df4e9ef";

    private int parameter;
    private BeaconInfo myPosition;
    private BeaconReceiveImpl receiveListener;
    private SparseArray<BeaconReceiveValue> mapValue;

    private BeaconInfoSet beaconSet;

    //instance
    public static AltBeaconManager getInstance() {
        return manager;
    }
    private static AltBeaconManager manager;
    static {
        if (manager == null) {
            manager = new AltBeaconManager();
        }
    }

    private Context mContext;
    private BeaconManager beaconManager;

    private AltBeaconManager() { mapValue = new SparseArray<>(); }

    /**
     * 是否可移動到下一個點
     * @param receives Receiver
     * @param keys Keys
     * @param vals Values
     * @param target Target
     * @return T/F
     */
    private boolean canMovePosition(SparseArray<BeaconReceiveValue> receives, int[] keys, int[] vals, UUID target) {
        BeaconInfo beaconInfo;
        //依最近的位置進行處理
        for (int i = 0; i < vals.length; i++) {
            //判斷是否有索引
            if (keys[i] == 0)
                break;
            //根據索引取得信標資料
            beaconInfo = beaconSet.get(keys[i]);

            //判斷目前的點是否為診間
            if (beaconInfo.isRoom()) {
                //若為診間，則判斷必須是目標診間才可移動
                if (beaconInfo.getId().equals(target)) {
                    myPosition = beaconInfo;
                    return true;
                }
            }
            else {
                //判斷是否可進入該點位置
                if (vals[i] >= beaconInfo.getRssi()) {
                    //依據參數處理
                    switch (parameter) {
                        case 1:
                            //取得移動的鄰居清單，判斷是否合理
                            SparseArray<BeaconNeighbor> neighborsThis = beaconInfo.getNeighborList();
                            if (neighborsThis.size() == 0) {
                                myPosition = beaconInfo;
                                return true;
                            }
                            //判斷接收到的鄰居訊號量是否合理
                            for (int j = 0; j < neighborsThis.size(); j++) {
                                int key = neighborsThis.keyAt(j);
                                BeaconReceiveValue brv = receives.get(key);
                                if (brv != null && brv.getResult() > neighborsThis.get(key).getRssi()) {
                                    myPosition = beaconInfo;
                                    return true;
                                }
                            }
                            break;
                        default:
                            myPosition = beaconInfo;
                            return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 開始接收資料
     * @param context Context
     * @param receiver Receiver
     * @param beaconList Beacons
     * @param scanMillisecond Scan
     * @param betweenMillisecond Between
     * @param parameter Parameter
     * @return T/F
     */
    @Override
    public boolean startScanBeacon(Context context, BeaconReceiveImpl receiver, final BeaconInfoSet beaconList, long scanMillisecond, long betweenMillisecond, long parameter) {
        myPosition = null;
        mContext = context;
        receiveListener = receiver;
        beaconSet = beaconList;
        this.parameter = (int) parameter;
        try {
            if (beaconManager == null) {
                beaconManager = BeaconManager.getInstanceForApplication(context);
            }
            //set the duration of the scan to be 1.1 seconds
            beaconManager.setBackgroundScanPeriod(scanMillisecond);
            //set the time between each scan to be 3.1 seconds
            beaconManager.setBackgroundBetweenScanPeriod(betweenMillisecond);
            beaconManager.setBackgroundMode(true);
            //parser
            beaconManager.getBeaconParsers().clear();
            beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(IBEACON_FORMAT));
            beaconManager.bind(this);
            return true;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * 結束接收資料
     */
    @Override
    public void stopScanBeacon() {
        try {
            if (beaconManager != null) {
                beaconManager.unbind(this);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        mContext = null;
    }

    /**
     * 重新載入信標清單
     * @param beaconList BeaconList
     */
    public void resetBeaconList(final BeaconInfoSet beaconList) {
        synchronized (AltBeaconManager.class) {
            beaconSet = beaconList;
        }
    }

    /**
     * 取得目前所在的信標
     * @param guideTarget Target
     * @return obj
     */
    @Override
    public BeaconInfo getPositionInBeacon(UUID guideTarget) {
        //判斷是否有接收到信標
        if (mapValue.size() > 0) {
            SparseArray<BeaconReceiveValue> lstReceive;
            synchronized (AltBeaconManager.class) {
                lstReceive = mapValue.clone();
                if (mapValue.size() > 10)
                    mapValue.clear();
            }
            //取得目前最接近的八點位置
            int[] keys = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            int[] vals = new int[] { -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000 };
            for(int i = 0; i < lstReceive.size(); i++) {
                BeaconReceiveValue brv = lstReceive.get(lstReceive.keyAt(i));
                int key = brv.getKey();
                int val = brv.getResult();
                if (val > vals[0]) {
                    //移動後續的陣列
                    keys[7] = keys[6];
                    vals[7] = vals[6];
                    keys[6] = keys[5];
                    vals[6] = vals[5];
                    keys[5] = keys[4];
                    vals[5] = vals[4];
                    keys[4] = keys[3];
                    vals[4] = vals[3];
                    keys[3] = keys[2];
                    vals[3] = vals[2];
                    keys[2] = keys[1];
                    vals[2] = vals[1];
                    keys[1] = keys[0];
                    vals[1] = vals[0];
                    //設定最小值
                    keys[0] = key;
                    vals[0] = val;
                }
                else if (val > vals[1]) {
                    //移動後續的陣列
                    keys[7] = keys[6];
                    vals[7] = vals[6];
                    keys[6] = keys[5];
                    vals[6] = vals[5];
                    keys[5] = keys[4];
                    vals[5] = vals[4];
                    keys[4] = keys[3];
                    vals[4] = vals[3];
                    keys[3] = keys[2];
                    vals[3] = vals[2];
                    keys[2] = keys[1];
                    vals[2] = vals[1];
                    //設定最小值
                    keys[1] = key;
                    vals[1] = val;
                }
                else if (val > vals[2]) {
                    //移動後續的陣列
                    keys[7] = keys[6];
                    vals[7] = vals[6];
                    keys[6] = keys[5];
                    vals[6] = vals[5];
                    keys[5] = keys[4];
                    vals[5] = vals[4];
                    keys[4] = keys[3];
                    vals[4] = vals[3];
                    keys[3] = keys[2];
                    vals[3] = vals[2];
                    //設定最小值
                    keys[2] = key;
                    vals[2] = val;
                }
                else if (val > vals[3]) {
                    //移動後續的陣列
                    keys[7] = keys[6];
                    vals[7] = vals[6];
                    keys[6] = keys[5];
                    vals[6] = vals[5];
                    keys[5] = keys[4];
                    vals[5] = vals[4];
                    keys[4] = keys[3];
                    vals[4] = vals[3];
                    //設定最小值
                    keys[3] = key;
                    vals[3] = val;
                }
                else if (val > vals[4]) {
                    //移動後續的陣列
                    keys[7] = keys[6];
                    vals[7] = vals[6];
                    keys[6] = keys[5];
                    vals[6] = vals[5];
                    keys[5] = keys[4];
                    vals[5] = vals[4];
                    //設定最小值
                    keys[4] = key;
                    vals[4] = val;
                }
                else if (val > vals[5]) {
                    keys[7] = keys[6];
                    vals[7] = vals[6];
                    keys[6] = keys[5];
                    vals[6] = vals[5];
                    //設定最小值
                    keys[5] = key;
                    vals[5] = val;
                }
                else if (val > vals[6]) {
                    keys[7] = keys[6];
                    vals[7] = vals[6];
                    //設定最小值
                    keys[6] = key;
                    vals[6] = val;
                }
                else if (val > vals[7]) {
                    //設定最小值
                    keys[7] = key;
                    vals[7] = val;
                }
            }

            //第一次取得定位點
            if (myPosition == null) {
                return canMovePosition(lstReceive, keys, vals, guideTarget) ? myPosition : null;
            }
            //位置沒有變更
            else if (myPosition.getKey() == keys[0]) {
                return myPosition;
            }
            else {
                //取得本次清單距離最近的值
                BeaconReceiveValue nearValue = lstReceive.get(keys[0]);
                //判斷前次的位置在本次是否有接收到
                BeaconReceiveValue preValue = lstReceive.get(myPosition.getKey());

                //前次的定位點在本次中無搜尋到，直接判斷是否可直接移動
                if (preValue == null ||
                    //判斷前次定位點離這次最近的定位點的訊號量是否超過標準
                    Math.abs(preValue.getResult() - nearValue.getResult()) > 2) {
                    if (canMovePosition(lstReceive, keys, vals, guideTarget))
                        return myPosition;
                }
            }
        }

        //回傳目前的位置
        return myPosition;
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier((beacons, region) -> {
            int size = beacons.size();
            if (size > 0) {
                synchronized (AltBeaconManager.class) {
                    for (Beacon beacon : beacons) {
                        //取得索引
                        int key = beacon.getId2().toInt() * INCREMENTAL + beacon.getId3().toInt();
                        BeaconInfo bi = beaconSet.get(key);
                        if (bi != null) {
                            //取得距離
                            int rssi = beacon.getRssi();
                            //強度增減
                            int increase = bi.getIncrease();
                            if (increase != 0) {
                                rssi += increase;
                            }

                            //將距離值加入清單
                            BeaconReceiveValue value = mapValue.get(key);
                            if (value == null)
                                mapValue.put(key, new BeaconReceiveValue(key, rssi));
                            else
                                value.add(rssi);
                        }
                    }
                }
                receiveListener.onBeaconReceiver(beacons.size());
            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region(FILTER_UUID, null, null, null));
        }
        catch (RemoteException ignored) { }
    }

    @Override
    public Context getApplicationContext() {
        return mContext.getApplicationContext();
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {
        mContext.unbindService(serviceConnection);
    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        return mContext.bindService(intent, serviceConnection, i);
    }

}