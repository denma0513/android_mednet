package com.mgear.mednetapp;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.linecorp.linesdk.auth.LineLoginApi;
import com.linecorp.linesdk.auth.LineLoginResult;
import com.mgear.mednetapp.adapter.TutorialPagerStartAppAdapter;
import com.mgear.mednetapp.entity.OauthSet;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.CustomViewPager;


/**
 * Created by Dennis
 * class: TutorialStartAppActivity
 */
public class TutorialStartAppActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private TutorialPagerStartAppAdapter mTutorialPagerAdapter = null;
    private CustomViewPager mViewPager = null;
    private int mIndex = 0;
    private String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        Intent intent = getIntent();
        result = intent.getStringExtra("result");

        // Create the adapter that will return a fragment for each of the
        // primary sections of the activity.
        this.mTutorialPagerAdapter = new TutorialPagerStartAppAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        this.mViewPager = (CustomViewPager) findViewById(R.id.vpPager);
        this.mViewPager.addOnPageChangeListener(this);
        this.mViewPager.setAdapter(mTutorialPagerAdapter);
        this.mViewPager.setPagingEnabled(true);

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void onPreviousClick(View view) {
        mIndex = mViewPager.getCurrentItem();
        mViewPager.setCurrentItem(--mIndex);
    }

    public void onNextClick(View view) {
        mIndex = mViewPager.getCurrentItem();
        mViewPager.setCurrentItem(++mIndex);
    }

    public void onStartClick(View view) {
        //如果不是mega版本要跳註冊
        SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
        String contact = settings.getString("contact", "");
        String refreshToken = MegaApplication.getInstance().getMember().getRefreshToken();
        if (refreshToken == null || refreshToken.length() <= 0) {
            Intent intent = new Intent(this, UserLoginActivity.class);
            startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));
            return;
        }

        Intent intent = new Intent(this, CultureSelectActivity.class);
        startActivity(intent);
        this.finish();
    }

    //回接activity的結果
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        Log.d("debug", "requestCode = " + requestCode);
        if (requestCode == Integer.parseInt(MegaApplication.ActionTypeLogin)) {
            try {
                //完成註冊
                //Log.i("debug", "bundle.getString(\"status\") = " + data.getStringExtra("status"));
                if (resultCode == RESULT_OK && "success".equals(data.getStringExtra("status"))) {
                    Intent intent = new Intent(this, CultureSelectActivity.class);
                    startActivity(intent);
                } else {
                    this.finish();

                }
            } catch (Exception e) {
                Log.w("debug", "signInResult:failed code=" + e.getMessage());
                this.finish();
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
//        switch (position) {
//            case 3:
//                ((TutorialTestsFragment) mTutorialPagerAdapter.getItem(position)).startPagerAnimation();
//                break;
//            case 4:
//                ((TutorialStatusFragment) mTutorialPagerAdapter.getItem(position)).startPagerAnimation();
//                break;
//        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}