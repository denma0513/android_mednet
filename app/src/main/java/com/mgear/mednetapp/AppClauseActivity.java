package com.mgear.mednetapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.mgear.mednetapp.enums.CultureEnum;
import com.mgear.mednetapp.utils.CommonFunc;

import java.net.HttpURLConnection;


/**
 * Created by Jye on 2017/6/16.
 * class: ClauseActivity
 */
public class AppClauseActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_clause);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbAppClause);
        toolbar.setTitle(R.string.title_read_app_clause);
        setSupportActionBar(toolbar);

        TextView txtClause = (TextView) findViewById(R.id.txtClauseContent);

        String englishVersion = CultureSelectActivity.SelectCulture.equals(CultureEnum.English) ? "1" : "0";

        if ("1".equals(englishVersion)) {
            txtClause.setText(CommonFunc.fromHtml(String.format(getString(R.string.content_app_agreement), CommonFunc.organizationEn)+"\n\n\n\n"+getString(R.string.content_personal_agreement)));
        }else {
            txtClause.setText(CommonFunc.fromHtml(String.format(getString(R.string.content_app_agreement), CommonFunc.organization)+"\n\n\n\n"+getString(R.string.content_personal_agreement)));
        }

        Button btnAgree = (Button) findViewById(R.id.btnClauseAgree);
        btnAgree.setOnClickListener(this);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(this);

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClauseAgree:
//                new IntentIntegrator(this).setCaptureActivity(LogonActivity.class).setBeepEnabled(true).initiateScan();
//                OrgMainActivity.addCustomerTrack("clause_agree", CommonFunc.getNowSeconds());
                startActivity(new Intent(this, LogonActivity.class));
                this.finish();
                break;
            default:
                //startActivity(new Intent(this, CultureSelectActivity.class));
                this.finish();
                break;
        }
    }

}