package com.mgear.mednetapp;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.SyncStateContract;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.mgear.mednetapp.adapter.UserLoginTabAdapter;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.SignUpChkSMSFragment;
import com.mgear.mednetapp.fragments.SignUpProfileFragment;
import com.mgear.mednetapp.fragments.WebViewFragment;
import com.mgear.mednetapp.fragments.WebViewLoginFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.helpers.DataHelper;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.WebViewUtil;

import org.json.JSONObject;

public class UserLoginActivity extends BaseActivity implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener, IBackHandleIpml, TaskPost2ExecuteImpl {
    //常數
    private static String TAG = "SIGNIN";

    private RelativeLayout mUserLoginView;
    private TabLayout mTebView;
    private ViewPager mViewPager;
    private UserLoginTabAdapter myAdapter;
    private MegaBaseFragment selectedFragment;
    private WebView webView, webView2;
    private boolean finish1 = false, finish2 = false;
    private ProgressDialog mDialog;
    private Bundle retBundle;

    private DataHelper mDataHelper;


    private String customerId; //暫存用

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_user_login);
        Bundle extras = getIntent().getExtras();
        retBundle = extras;
        mDataHelper = new DataHelper();

        mUserLoginView = findViewById(R.id.user_login_view);
        mTebView = findViewById(R.id.tabView);
        mViewPager = findViewById(R.id.viewPager);

        webView = findViewById(R.id.web_login_view);
        webView2 = findViewById(R.id.web_login_view2);

        myAdapter = new UserLoginTabAdapter(this, getFragmentManager());

        if (!MegaApplication.getInstance().getMember().isNullAccessToken()) {
            new CustomerApiTask(this, this, true, "", TaskEnum.MednetGetProfile).execute();
        }

        mViewPager.setAdapter(myAdapter);

        //記錄應用程式事件
        mViewPager.addOnPageChangeListener(this);
        mTebView.addOnTabSelectedListener(this);
        mTebView.setupWithViewPager(mViewPager);
    }

    public void openWebView(String url) {
        try {
            //mViewPager.setVisibility(View.GONE);
            FragmentManager fragmentManager = getFragmentManager();
            WebViewFragment webFragment = new WebViewFragment();
            webFragment.setTextZoom(1.2f);
            webFragment.setUrl("", url);

            FragmentTransaction ft = fragmentManager.beginTransaction();
            Fragment fragmentMain = webFragment;

            if (fragmentMain.isAdded()) { // 如果 home fragment 已經被 add 過，
                ft.show(fragmentMain); // 顯示它。
            } else { // 反之，
                ft.add(R.id.user_login_view, fragmentMain, "SIGN"); // 使用 add 方法。
                ft.addToBackStack("HOME");
            }

            ft.commit();
        } catch (Exception e) {
            Log.w(TAG, "SignUpChkSMSFragment  e = " + e.getStackTrace());
            e.printStackTrace();
        }
    }

    public void goChkSMS() {
        try {
            mTebView.clearFocus();
            mViewPager.clearFocus();
            mViewPager.setVisibility(View.GONE);
            FragmentManager fragmentManager = getFragmentManager();
            SignUpChkSMSFragment signUpChkSMSFragment = new SignUpChkSMSFragment();
            signUpChkSMSFragment.setType(0);
            FragmentTransaction ft = fragmentManager.beginTransaction();
            Fragment fragmentMain = signUpChkSMSFragment;
            ft.replace(R.id.user_login_view, fragmentMain);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        } catch (Exception e) {
            Log.w(TAG, "SignUpChkSMSFragment  e = " + e.getStackTrace());
            e.printStackTrace();
        }
    }

    public void goUpProfile() {
        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }

            FragmentManager fragmentManager = getFragmentManager();
            SignUpProfileFragment signUpProfile = new SignUpProfileFragment();
            signUpProfile.setType(0);
            FragmentTransaction ft = fragmentManager.beginTransaction();
            Fragment fragmentMain = signUpProfile;
            ft.replace(R.id.user_login_view, fragmentMain);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        } catch (Exception e) {
            Log.w(TAG, "goUpProfile  e = " + e.getStackTrace());
            e.printStackTrace();
        }
    }

    //登入後紀錄token
    public void loginForever() {
        Log.i(TAG, "loginForever");
        try {
            //登入完成需要先取得用戶資料，並且讓網頁也登入
            String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
            SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
            String refreshToken = MegaApplication.getInstance().getMember().getRefreshToken();
            settings.edit().putString("accessToken", accessToken).putString("refreshToken", refreshToken).apply();
            new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetProfile).execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //登入後紀錄token
    public void cancel() {
        Log.i(TAG, "cancel");
        try {
            //清空使用者資料
            MegaApplication.getInstance().clearMember();
            SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
            settings.edit().clear().commit();
            Intent intent = new Intent();
            intent.putExtra("status", "fail");
            this.setResult(RESULT_CANCELED, intent);
            this.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openMednetRule() {
        //Log.i("debug", "openMednetRule");
    }

    public void openPrivacy() {
        //Log.i("debug", "openPrivacy");
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //Log.i("debug", " onPageScrolled position" + position);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!MegaApplication.getInstance().getMember().isNullAccessToken()) {
//            new AlertDialog.Builder(this).setCancelable(false)
//                    .setTitle(R.string.title_warning)
//                    .setMessage("已經登入").setPositiveButton(R.string.button_close, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    //UserLoginActivity.this.finish();
//                }
//            }).show();
        }
    }

    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {
        this.selectedFragment = selectedFrangment;
    }

    @Override
    public void onBackPressed() {
        if (selectedFragment == null || !selectedFragment.onBackPressed()) {
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                FragmentManager.BackStackEntry fragment = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1);
                if ("SIGN".equals(fragment.getName())) {
                    Intent intent = new Intent();
                    intent.putExtra("status", "fail");
                    this.setResult(RESULT_OK, intent);
                    this.finish();
                }
                getFragmentManager().popBackStack();
            }
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetGetWebToken:
                if (result != null && !result.isNull("source")) {
                    String token = result.optJSONObject("source").optString("token");
                    String webLoginUrl = String.format("https://med-net.com/CMSContent?user_id=%s&token=%s", customerId, token);
                    WebSettings webSettings = webView.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    Log.i("debug", "onPostExecute: " + webLoginUrl);
                    webView.setWebViewClient(new MednetWebViewClient());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl(webLoginUrl);
                        }
                    }, 0);
                }

                break;
            case MednetGetProfile:
                Log.d("debug", "MednetGetProfile result = " + result);
                if (result != null && !result.isNull("source")) {
                    //已經登入
                    JSONObject source = result.optJSONObject("source");

                    String webToken = source.optString("web_token");
                    customerId = source.optString("customer_id");
                    String webLoginUrl = String.format("https://med-net.com/CMSContent?user_id=%s&token=%s", customerId, webToken);

                    mDialog = new ProgressDialog(this);
                    mDialog.setMessage("登入中，請稍等...");
                    mDialog.setCancelable(false);
                    mDialog.setCanceledOnTouchOutside(false);
                    mDialog.show();

                    JSONObject sendObj = new JSONObject();
                    try {
                        sendObj.put("id", source.optString("customer_id"));
                        sendObj.put("email", source.optString("email"));
                        sendObj.put("name", source.optString("name"));
                        sendObj.put("first_name", source.optString("first_name"));
                        sendObj.put("last_name", source.optString("last_name"));
                        sendObj.put("id_number", source.optString("id_number"));
                        sendObj.put("cellphone", source.optString("cellphone"));
                        sendObj.put("gender", source.optInt("gender"));
                        sendObj.put("birthday", source.optString("birthday"));
                        sendObj.put("view_count", 0);
                        sendObj.put("self_introduction", source.optString("self_introduction"));
                        sendObj.put("active", 1);
                        sendObj.put("create_by", "null");
                        sendObj.put("create_from", "null");
                        sendObj.put("profile_image", source.optString("profile_image"));
                        sendObj.put("cover_image", source.optString("cover_image"));

                        MegaApplication.getInstance().getMember().setName(source.optString("name"));
                        MegaApplication.getInstance().getMember().setEmail(source.optString("email"));
                        MegaApplication.getInstance().getMember().setProfileImage(source.optString("profile_image"));
                        MegaApplication.getInstance().getMember().setCustomerId(source.optString("customer_id"));

                        SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
                        settings.edit().putString("name", source.optString("name"))
                                .putString("firstName", source.optString("first_name"))
                                .putString("profile_image", source.optString("profile_image"))
                                .putString("profileImage", source.optString("profile_image"))
                                .putString("email", source.optString("email"))
                                .apply();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String webLoginUrl2 = String.format("https://expert.med-net.com/login/GetOAuthLogInfo?data=%s", sendObj.toString().replaceAll("null",""));


                    WebSettings webSettings = webView.getSettings();
                    WebSettings webSettings2 = webView2.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webSettings2.setJavaScriptEnabled(true);
                    WebViewUtil.configWebView(webView);
                    WebViewUtil.configWebView(webView2);
                    webView.setWebViewClient(new MednetWebViewClient());
                    webView2.setWebViewClient(new MednetWebViewClient());
                    mViewPager.setVisibility(View.GONE);

                    //Log.i("debug", "webToken: " + webToken);

                    if (webToken == null || "".equals(webToken) || "null".equals(webToken)) {
                        new CustomerApiTask(this, this, true, "", TaskEnum.MednetGetWebToken).execute();
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.i("debug", "主站: " + webLoginUrl);
                                webView.loadUrl(webLoginUrl);
                            }
                        }, 0);
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("debug", "醫師諮詢: " + webLoginUrl2);
                            webView2.loadUrl(webLoginUrl2);
                        }
                    }, 0);
                    //MegaApplication.WebLogin = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mDialog.dismiss();
                            Intent intent = new Intent();
                            intent.putExtra("status", "success");
                            if (retBundle != null) {
                                retBundle.putString("status", "success");
                                intent.putExtras(retBundle);
                            }
                            setResult(RESULT_OK, intent);
                            //CommonFunc.showToast(this, "登入成功！");
                            finish();
                        }
                    }, 3000);


                    //Log.i("debug", "webLoginUrl = " + webLoginUrl);

                }
                break;
        }
    }

    private void logFinish() {
        if (!MegaApplication.IsMegaAppVersion) {
            finish();
            return;
        }

        if (finish1 && finish2) {
//        if (finish1) {
//        if (finish2) {
            mDialog.dismiss();
            Intent intent = new Intent();
            intent.putExtra("status", "success");
            if (retBundle != null) {
                retBundle.putString("status", "success");
                intent.putExtras(retBundle);
            }
            this.setResult(RESULT_OK, intent);
            //CommonFunc.showToast(this, "登入成功！");
            this.finish();
        }
    }

    private class MednetWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //Log.i("debug", "shouldOverrideUrlLoading = " + url);

            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            //Log.i("debug", "shouldOverrideUrlLoading = " + request.getUrl().toString());
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

            try {
                String url = request.getUrl().toString();
                WebResourceResponse response = mDataHelper.getReplacedWebResourceResponse(
                        getApplicationContext(), url);

                if (response != null) {
                    return response;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            return super.shouldInterceptRequest(view, request);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.e("debug", "onPageFinished = " + url);
            String cookies = CookieManager.getInstance().getCookie(url);
            CommonFunc.syncCookie(url,cookies);
            if (url.contains("med-net.com/CMSContent")) {
                Log.d("debug", "主站登入  ");
                finish1 = true;
                logFinish();
            } else if (url.contains("expert.med-net.com/index")) {
                Log.d("debug", "醫師諮詢登入  ");
                finish2 = true;
                logFinish();
            }
            view.setVisibility(View.VISIBLE);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
        }

//        @Override
//        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            //全部擋掉
//            handler.cancel();
//        }

    }

}
