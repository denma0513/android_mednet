package com.mgear.mednetapp.enums;

/**
 * Created by Jye on 2017/7/10.
 * enum: CultureEnum
 */
public enum CultureEnum {
    English,
    TraditionalChinese
}