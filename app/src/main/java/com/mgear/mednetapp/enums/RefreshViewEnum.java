package com.mgear.mednetapp.enums;

public enum  RefreshViewEnum {
    homePageRefreshView,
    healthListRefreshView,
    healthRecordListRefreshView
}
