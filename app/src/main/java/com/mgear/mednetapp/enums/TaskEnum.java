package com.mgear.mednetapp.enums;

public enum TaskEnum {
    //fix
    AndroidKeyHashApi,
    FixEmail,

    //log
    MednetAppLogEven,  //紀錄點擊事件log
    MednetAppLogNotice,  //紀錄提示訊息log
    MednetAppLogHealthOrder,  //紀錄健康紀錄排序log

    //主站
    NewDownloadImageTask, //下載圖片
    HomePageTask, //首頁ＡＰＩ
    MednetLoginTask, //主站登入
    MednetRefreshTokenTask, //更新token
    MednetCreateAccount, //新增使用者
    MednetGetProfile, //取得使用者資料
    MednetSetFCMToken, //取得使用者資料
    MednetGetAuthType, //取得身份資訊
    MednetGetBadge, //取得badge
    MednetGetWebToken, //取得使用者WebToekn
    MednetUpdateProfile, //更新使用者資料
    MednetSendSMS, //寄送簡訊驗證
    MednetCheckSMS, //確認簡訊驗證碼
    MednetForgetPassword, //忘記密碼
    MednetCheckForgetPassword, //忘記密碼確認簡訊驗證碼
    MednetSetPassword, //設定密碼
    MednetRegisterPushTokenSave, //記錄推播token
    MednetSendMessageWithCustomerId, //發送交友推播

    //健康資料
    MednetQueryMeasureValueByID, //撈健康資料
    MednetQueryMeasureValues, //撈健康資料
    MednetQueryLastMeasureValue, //撈最新健康資料
    MednetQueryMeasureValuesType1,
    MednetQueryMeasureValuesType4,
    MednetQueryMeasureValuesType6,
    MednetCreateBloodSugar,
    MednetCreateBloodPressure,
    MednetCreateBloodOxygen,
    MednetCreateDrinkingCreateOrUpdate,
    MednetCreateBodyMassWeight,
    MednetCreateStepCreateOrUpdate,
    MednetSleepingCreateOrUpdate,
    MednetUpdateBloodSugar,
    MednetUpdateBloodPressure,
    MednetUpdateBloodOxygen,
    MednetUpdateBodyMassWeight,

    //好友相關
    MednetGetStepCompetition,
    MednetGetRelationships,
    MednetAddRelationship,
    MednetIsAcceptRelationship,
    MednetRejectAddFriend,

    Oauth2GetTokenTask,
    Oauth2GetProfileTask,

    //機構端
    OrganizationListTask,
    OrganizationCheckTask,
    AppCheckUpgradeTask,
    CustomerContactCheckTask,
    CustomerContactUpdateTask,
    CustomerExamineUpdateTask,
    CustomerHelpProviderTask,
    CustomerLogonTask,
    CustomerLogoutTask,
    CustomerGiftsDataTask,
    CustomerSurveyScoreTask,
    CustomerInPositionTask,
    CustomerCheckupResultTask,
    CustomerTrackRecordTask,
    CustomerAdditionSelectedTask,
    CustomerAdditionDataTask,
    StartScheduleTask,
    WaitRemainTimeFinishTask,
    BeaconDataTask,
    BeaconPushDataTask,
    SpotDataTask,
    RoomDataTask,
    ExamineItemDataTask,
    AdditionItemDataTask,
    DownloadImageTask,
    DownloadFileTask,
    SurveyItemDataTask,
    MessageNoticeTask,
    SendCeoMailboxTask,
    HealthySystemDataTask,
    EncyclopediaDataTask,
    SpecialDemandDataTask,
    SpecialDemandSelectedTask,
    SpecialDemandUpdateTask,
    RegisterPushNotificationTokenTask,
    RegisterDevice
}