package com.mgear.mednetapp.enums;

/**
 * Created by Jye on 2017/7/11.
 * enum: StatusEnum
 */
public enum StatusEnum {
    //EMS通知已報到
    Initial(0),
    //手機已登入
    Logon(1),
    //等候進入診間
    WaitCheckin(2),
    //已進入診間
    Checkin(3),
    //已放行
    Released(4),
    //暫放行
    Temporarily(5),
    //待判讀
    ToBeJudged(7),
    //補作
    Supplement(8),
    //拒作
    Reject(9),
    //呼叫進入診間
    CallEnterRoom(10),
    //等待回診間檢查
    WaitDoubleInRoom(11),
    //等待時間中
    WaitingTime(12),
    //異常
    Alarm(99),
    //暫停
    Pause(100),
    //等待總確認
    WaitConfirm(101),
    //排程結束
    End(102);

    public static StatusEnum valueOf(int value) {
        switch (value) {
            case 0:
                return StatusEnum.Initial;
            case 1:
                return StatusEnum.Logon;
            case 2:
                return StatusEnum.WaitCheckin;
            case 3:
                return StatusEnum.Checkin;
            case 4:
                return StatusEnum.Released;
            case 5:
                return StatusEnum.Temporarily;
            case 7:
                return StatusEnum.ToBeJudged;
            case 8:
                return StatusEnum.Supplement;
            case 9:
                return StatusEnum.Reject;
            case 10:
                return StatusEnum.CallEnterRoom;
            case 11:
                return StatusEnum.WaitDoubleInRoom;
            case 12:
                return StatusEnum.WaitingTime;
            case 99:
                return StatusEnum.Alarm;
            case 100:
                return StatusEnum.Pause;
            case 101:
                return StatusEnum.WaitConfirm;
            case 102:
                return StatusEnum.End;
            default:
                return null;
        }
    }

    private int code;
    StatusEnum(int code) { this.code = code; }
    public int getCode() { return code; }
}