package com.mgear.mednetapp.enums;

/**
 * Created by Jye on 2017/7/27.
 * enum: RemainEnum
 */
public enum RemainEnum {
    None,
    WaitingChangeClothes,
    WaitingScheduledTime,
    HelpProvider
}