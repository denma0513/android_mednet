package com.mgear.mednetapp;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgear.mednetapp.enums.CultureEnum;
import com.mgear.mednetapp.utils.CommonFunc;


/**
 * Created by Jye on 2017/9/6.
 * class: PersonalClauseActivity
 */
public class PersonalClauseActivity extends AppCompatActivity implements View.OnClickListener {

    public static boolean Agree = true;

    private boolean agree = true;
    private ImageView imgCheckable;
    private Drawable checkOn, checkOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_clause);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbPersonalClause);
        toolbar.setTitle(R.string.title_read_personal_clause);
        setSupportActionBar(toolbar);

        imgCheckable = (ImageView) findViewById(R.id.imgPersonalSelect);
        imgCheckable.setOnClickListener(this);
        TextView txtAgree = (TextView) findViewById(R.id.txtPersonalAgreeText);
        txtAgree.setOnClickListener(this);
        TextView txtClause = (TextView) findViewById(R.id.txtPersonalClauseContent);

        String englishVersion = CultureSelectActivity.SelectCulture.equals(CultureEnum.English) ? "1" : "0";

        if ("1".equals(englishVersion)) {
            txtClause.setText(CommonFunc.fromHtml(String.format(getString(R.string.content_app_agreement), CommonFunc.organizationEn)+"\n\n\n\n"+getString(R.string.content_personal_agreement)));
        }else {
            txtClause.setText(CommonFunc.fromHtml(String.format(getString(R.string.content_app_agreement), CommonFunc.organization)+"\n\n\n\n"+getString(R.string.content_personal_agreement)));
        }


        Button btnAgree = (Button) findViewById(R.id.btnPersonalClauseAgree);
        btnAgree.setOnClickListener(this);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(this);

        //取得核取方塊的圖示
        checkOn = new BitmapDrawable(getResources(), CommonFunc.readBitmap(this, R.drawable.check_box_on));
        checkOff = new BitmapDrawable(getResources(), CommonFunc.readBitmap(this, R.drawable.check_box_off));
        agree = Agree;
        imgCheckable.setImageDrawable(agree ? checkOn : checkOff);

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPersonalClauseAgree:
                Agree = agree;
                if (agree)
                    OrgMainActivity.addCustomerTrack("personal_agree", CommonFunc.getNowSeconds());
                else
                    OrgMainActivity.addCustomerTrack("personal_disagree", CommonFunc.getNowSeconds());
                startActivity(new Intent(this, CustomerActivity.class));
                this.finish();
                break;
            case R.id.txtPersonalAgreeText:
            case R.id.imgPersonalSelect:
                agree = !agree;
                imgCheckable.setImageDrawable(agree ? checkOn : checkOff);
                break;
            default:
                startActivity(new Intent(this, LogonActivity.class));
                this.finish();
                break;
        }
    }

}