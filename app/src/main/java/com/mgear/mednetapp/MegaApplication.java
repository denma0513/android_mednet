package com.mgear.mednetapp;

import android.app.Application;
import android.util.Log;

import com.mgear.mednetapp.entity.MemberSet;
import com.mgear.mednetapp.entity.OauthSet;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.PublicKey;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MegaApplication extends Application {

    public static MegaApplication context;

    public static MegaApplication getInstance() {
        if (context == null) {
            return context = new MegaApplication();
        } else {
            return context;
        }
    }

    public static String AIDOCTOR_URL = "https://aidoctor.med-net.com/AIDoctor/Index?customerID=%s";

    // 週期性的鬧鐘
    public final static String TIMER_ACTION_REPEATING = "TIMER_ACTION_REPEATING";
    // 定時鬧鐘
    public final static String TIMER_ACTION = "TIMER_ACTION";
    final public static boolean IsMegaAppVersion = true;
    public static boolean IsFirstTime = true;
    public static boolean IsFitFirstTime = true;
    public static boolean WebLogin = false;
    public static String pushToken = "";
    public static String deviceID = "";
    public static String UUID = "";
    public static String IPAddress = "";
    public static boolean scrollLog = false;


    private static int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    private static int CORE_POOL_SIZE = Math.max(2, Math.min(CPU_COUNT, 8));
    private static int MAXIMUM_POOL_SIZE = CPU_COUNT * 2 + 1;
    private static int KEEP_ALIVE_SECONDS = 30;
    public static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
            CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_SECONDS, TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(128));

    public static boolean ORG_ISOPEN = false;

    //anction type
    final public static String ActionTypeWebView = "1";
    final public static String ActionTypeOutWebView = "2";
    final public static String ActionTypeMainPage = "3";
    final public static String ActionTypeHealthPush = "4";
    final public static String ActionTypeHealthPushCreate = "5";
    final public static String ActionTypeHealthPushItemSort = "6";
    final public static String ActionTypeStepCompetition = "7";
    final public static String ActionTypeLogin = "1001";
    final public static String ActionTypeLogout = "1002";

    //badge
    public static String shoppingCart = "0";
    public static String bell = "0";
    public static String reservation = "0";

    //健康資料
    public static String BodyHeight = "";//設定身高
    public static int TargetStep = 10000;//目標步數
    public static int WaterCapacity = 2000;//目標飲水量
    public static int TodayWaterCapacity = 0;//今天飲水量
    public static int TodayStepCount = 0;//今天步數
    public static Double TodayCalories = 0.0;//今天卡路里
    public static Double TodayDistance = 0.0;//今天距離
    public static String TodaySleepTime = "";//今天就寢時間
    public static String TodayWakeUpTime = "";//今天起床時間
    public static Double TodaySleepHour = 0.0;//今天睡眠時間
    public static int ActivityTypeGoogleFit = 98;
    public static JSONObject FitStepObj = new JSONObject();


    //member
    public static MemberSet member;

    public MemberSet getMember() {
        if (member == null)
            member = new MemberSet();
        return this.member;
    }

    //oauth
    public static OauthSet oauth;

    public OauthSet getOauth() {
        if (oauth == null)
            oauth = new OauthSet();
        return this.oauth;
    }

    public OauthSet setOauth(OauthSet oauthSet) {
        this.oauth = oauthSet;
        return this.oauth;
    }

    public static void clearMember() {
        member = new MemberSet();
    }

    public static JSONArray friendList = new JSONArray();
    public static JSONArray friendNonAllowList = new JSONArray();

    //健康紀錄
    public static JSONArray homeList;
    public static JSONArray healthList;
    public static String[] healthSortList;
    public static HashMap<String, JSONObject> healthMap = new HashMap<String, JSONObject>();

    //預載健康歷史記錄列表
    public static JSONObject healthRecord = new JSONObject();

    public static String[] healthTypeList = {
            "1", "2", "3", "4", "5", "6", "7"
    };

    public static int[] healthRecordIconList = {
            R.drawable.ic_record_blood_suger,
            R.drawable.ic_record_blood_pressure,
            R.drawable.ic_record_blood_o2,
            R.drawable.ic_record_drink,
            R.drawable.ic_record_sleep,
            R.drawable.ic_record_step,
            R.drawable.ic_record_weight
    };

    public static int[] healthImgList = {
            R.drawable.ic_blood_sugar,
            R.drawable.ic_blood_pleasure,
            R.drawable.ic_o2,
            R.drawable.ic_drink_water,
            R.drawable.ic_sleep,
            R.drawable.ic_walk,
            R.drawable.ic_weight
    };
    public static String[] healthNameList = {
            "血糖", "血壓", "血氧", "飲水量", "睡眠時間", "每日步數", "體位"
    };

    public static String[] healthTypeNameList = {
            "blood_sugar", "blood_pressure", "blood_oxygen", "drinking", "sleeping", "step", "body_mass_weight"
    };

    {
        //Log.e("debug", "CPU_COUNT = " + CPU_COUNT);
        //Log.e("debug", "CORE_POOL_SIZE = " + CORE_POOL_SIZE);
        //Log.e("debug", "MAXIMUM_POOL_SIZE = " + MAXIMUM_POOL_SIZE);
        setHealth();
    }

    public static void setHealth() {
        healthMap = new HashMap<String, JSONObject>();
        for (int i = 0; i < healthTypeList.length; i++) {
            JSONObject item = new JSONObject();
            try {
                item.put("healthImg", healthImgList[i]);
                item.put("healthRecordIcon", healthRecordIconList[i]);
                item.put("healthType", healthTypeList[i]);
                item.put("healthName", healthNameList[i]);
                item.put("healthTypeName", healthTypeNameList[i]);
                healthMap.put(healthTypeList[i], item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static JSONObject setHealthBasData(JSONObject obj, String type) {
        try {
            JSONObject healthMap = MegaApplication.healthMap.get(type);
            String typeName = healthMap.optString("healthTypeName");
            obj.put("healthTypeName", healthMap.optString("healthName"));
            obj.put("healthType", type);
            obj.put("healthTypeKey", typeName);
            obj.put("healthImg", healthMap.optString("healthImg"));
            obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static JSONArray replaceHealthItem(JSONArray array, JSONObject obj) {
        try {
            Date mtime = CommonFunc.UTC2Date(obj.optString("m_time"));
            Calendar cal = Calendar.getInstance();
            cal.setTime(mtime);
            for (int i = 0; i < array.length(); i++) {
                Calendar cal2 = Calendar.getInstance();
                JSONObject item = array.optJSONObject(i);
                if ("".equals(item.optString("m_time")))
                    continue;
                mtime = CommonFunc.UTC2Date(item.optString("m_time"));
                cal2.setTime(mtime);
                if (CommonFunc.isSameDay(cal, cal2)) {
                    array.put(i, obj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;
    }

    public static JSONArray replaceHealthItemById(JSONArray array, JSONObject obj) {
        try {
            int id = obj.optInt("id");
            for (int i = 0; i < array.length(); i++) {
                JSONObject item = array.optJSONObject(i);
                if (item.optInt("id") == id)
                    array.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;
    }

    //問候語
    public static String helloTitle = "早安";
    public static String helloContent = "早安，你有睡足8小時嗎?";
    public static String[] morningHello = new String[]{
            "早安，你有睡足8小時嗎?",
            "先喝一杯溫開水，喚醒頭腦！",
            "早餐記得要吃得好，充分攝取足夠蛋白質喔！"
    };
    public static String[] morningHello2 = new String[]{
            "小口小口地喝足量的溫水，增強消化力、增加代謝！",
            "坐太久囉，該起來走動，舒展一下筋骨吧！",
            "盯著電腦太久嗎? 記得30分鐘休息至少5分鐘喔！"
    };

    public static String[] afternoonHello = new String[]{
            "午餐時間到了，糙米飯取代白米飯，並搭配低油脂、高蛋白儲備工作能量喔！",
            "午餐可食用的優質蛋白質來源食物：奶酪、堅果、豆腐、魚和雞蛋",
            "蔬果中含有人體必需的多種維生素和微量元素以及大量的纖維素，是午餐全面營養必不可少的部分",
            "用從容、享受的心情對待午餐，吃飯的時候儘可能慢一點，不要吃得太匆忙，以免對腸胃造成負擔"
    };
    public static String[] afternoonHello2 = new String[]{
            "下午茶時光，可補充堅果、無糖豆漿、烤地瓜、茶葉蛋喔！",
            "坐太久囉，該起來伸伸懶腰，動一動囉！",
            "工作太忙了，忘記喝水嗎? 要養成喝水的好習慣喔！",
            "盯著電腦太久嗎? 記得30分鐘休息至少5分鐘喔！",
            "下午茶選擇「麻煩吃」及需要「咀嚼」的食物，例如堅果、芭樂等可以慢慢吃，增加飽足感及滿足感喔!",
            "需耗費大量腦力、創意力時，可選擇原味堅果，幫助補充腦力、精神集中!"
    };

    public static String[] nightHello = new String[]{
            "準備要吃晚餐囉，避免高油、高糖、高鹽與加工食品！",
            "晚餐建議選擇蔬菜、有飽足感的根莖類 (如地瓜、芋頭)、低GI食物，會更健康喔！",
            "晚餐時間不要過晚，睡前3～4小時不要進食，才不會影響睡眠喔！",
            "晚餐不宜過度攝取，容易囤積在體內，轉化為體脂肪！"
    };
    public static String[] nightHello2 = new String[]{
            "飯後散步有助於平穩血糖，晚餐後散步效果更佳！",
            "食物攝取後的72分鐘血液中的葡萄糖會達到高峰，趕快開始運動吧！",
            "散步可以調節血液循環系統和呼吸系統的功能，防止肌肉萎縮，保持關節靈活性！",
            "輕快的步行可以緩和神經肌肉緊張，也是治療情緒緊張的「解毒劑」喔！"
    };
    public static String[] nightHello3 = new String[]{
            "睡前四小時不要進食，避免消化不良喔！",
            "晚安，準備要上床睡覺囉！不要再滑手機囉！",
            "適度的伸展運動幫助舒展肌肉、消除緊張，放鬆身心，準備睡覺囉！",
            "以感謝為今日畫下句點，在睡前感謝今日與自己相遇的所有人，放下各種情緒，有助睡眠！",
            "光線會抑制褪黑激素的分泌，妨礙睡眠品質，記得關燈，讓褪黑激素分泌，幫助大腦進入深沉睡眠！",
            "夜裡不需要光線，但明日一早卻需要陽光喚醒大腦，拉窗簾時留一點縫，為下一個美好的清晨做準備！"
    };


    public static String[] needLoginUrlArray = {
            "med-net.com/Member",
            "med-net.com/Member/PasswordEdit",
            "med-net.com/Member/questionnaire",
            "med-net.com/DailyAnalysis",
            "med-net.com/PhysicalExamination",
            "med-net.com/CustomerDiet",
            "med-net.com/CustomerExercise",
            "med-net.com/Member/OrderList",
            "med-net.com/MindRecord",
            "med-net.com/MemberFavorite",
            "med-net.com/Member/Friendsettings",
            "med-net.com/ChannelCommentRecord",

            "med-net.com/BellCenter",

            "expert.med-net.com/askform/symptom",
            "expert.med-net.com/askform/division",
            "expert.med-net.com/askform",
            "sso2.med-net.com/Account/Login",

            "aidoctor.med-net.com/AIDoctor"
    };

}
