package com.mgear.mednetapp;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mgear.mednetapp.entity.MemberSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.HealthSettingDetailFragment;
import com.mgear.mednetapp.fragments.HealthSettingFragment;
import com.mgear.mednetapp.fragments.HealthSettingSortFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.task.common.NewDownloadImageTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.CircleImageView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class StepCompetitionActivity extends BaseActivity implements View.OnClickListener, TaskPost2ExecuteImpl {
    private static final String TAG = "debug";

    public enum NS_ENUM {
        stepViewDirectionLeftToRight,
        stepViewDirectionRightToLeft,
        stepViewDirectionTopToDown,
        stepViewDirectionDownToTop,
        stepViewImageMission
    }

    //畫面元件
    private HorizontalScrollView mGameScrollView;
    private RelativeLayout mGameLayout;
    private ImageView mGameImg, mMenu, mMenuClose;
    private TextView mTotalStepCount, mRanking, mPlatformRanking, mMenuTitle;
    private ImageView mTopBack;
    private TextView mTopTitle;
    private ProgressbarView mGamePath;
    private ExpandableListView mMenuList;
    private ListAdapter mListAdapter;

    private ProgressbarView progressbarView, progressbarView2, progressbarView3;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle acrionActionBarDrawerToggle; //左側選單控制元件
    //private Toolbar toolBar;


    //邏輯元件
    private int stepCount = 0;
    private JSONObject competitionData = new JSONObject();
    private JSONObject competitionMap = new JSONObject();
    private String AddFriendsId, isAllowFriend, guid;

    private ArrayList<Bitmap> picList = new ArrayList<Bitmap>();
    private ArrayList<Integer> picStepList = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_competition);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            guid = extras.getString("guid");
            isAllowFriend = extras.getString("IsAllowFriend");
            AddFriendsId = extras.getString("AddFriendsId");
            if (guid != null && AddFriendsId != null) {
                new CustomerApiTask(this, this, false, "", TaskEnum.MednetAddRelationship).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AddFriendsId);
            }
        }


        mGameScrollView = findViewById(R.id.game_view);
        mGameLayout = findViewById(R.id.game_layout);
        drawerLayout = findViewById(R.id.drawer_layout);
        //toolBar = findViewById(R.id.toolbar);
        mMenu = findViewById(R.id.menu);
        mMenuList = findViewById(R.id.menu_list_view);
        mMenuTitle = findViewById(R.id.menu_title);
        mMenuClose = findViewById(R.id.menu_close);
        mTopBack = findViewById(R.id.top_back);
        mTopTitle = findViewById(R.id.top_title);

        mTotalStepCount = findViewById(R.id.total_step_count);
        mRanking = findViewById(R.id.ranking);
        mPlatformRanking = findViewById(R.id.platform_ranking);

        mMenuTitle.setOnClickListener(this);
        mMenuClose.setOnClickListener(this);
        mTopBack.setOnClickListener(this);
        mTopTitle.setOnClickListener(this);

        mMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.i(TAG, "mMenu onClick: ");
                if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(Gravity.END);
                }
            }
        });
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        initLayout();
    }

    private void initLayout() {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int month = cal.get(Calendar.MONTH) + 1;
        mTopTitle.setText((month > 9 ? "" + month : "0" + month) + "月競賽");

        progressbarView = new ProgressbarView(this);
        progressbarView.setLayoutParams(mGameScrollView.getLayoutParams().width * 2, mGameScrollView.getLayoutParams().height);

        mGameLayout.addView(progressbarView);
        View v = new ImageView(getBaseContext());
        mGameImg = new ImageView(v.getContext());

        BitmapDrawable bgImg = new BitmapDrawable(getResources(), readBitmapHalf(this, R.drawable.bg_game_fall));
        mGameImg.setImageDrawable(bgImg);
        mGameScrollView.getLayoutParams().width = screenWidth;
        mGameScrollView.getLayoutParams().height = (int) (screenWidth / 750.0 * 850);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(new LinearLayout.LayoutParams(mGameScrollView.getLayoutParams().width * 2, mGameScrollView.getLayoutParams().height));
        mGameImg.setLayoutParams(lp);


        //mGameImg.setScaleType(ImageView.ScaleType.MATRIX);
        mGameImg.requestLayout();
        mGameLayout.addView(mGameImg);

        progressbarView2 = new ProgressbarView(this, "value");
        progressbarView2.setLayoutParams(mGameScrollView.getLayoutParams().width * 2, mGameScrollView.getLayoutParams().height);
        mGameLayout.addView(progressbarView2);

        progressbarView3 = new ProgressbarView(this, "person");
        progressbarView2.setLayoutParams(mGameScrollView.getLayoutParams().width * 2, mGameScrollView.getLayoutParams().height);
        mGameLayout.addView(progressbarView3);

        mListAdapter = new ListAdapter();
        mMenuList.setAdapter(mListAdapter);

        for (int i = 0; i < mListAdapter.getGroupCount(); i++) {
            mMenuList.expandGroup(i);
        }
        mMenuList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return true;
            }
        });
        //mListAdapter.notifyDataSetChanged();

    }


    /**
     * 載入圖片檔 壓縮1/2
     *
     * @param context Context
     * @param resId   Resource
     * @return Bitmap
     */
    @SuppressWarnings("deprecation")
    public static Bitmap readBitmapHalf(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        opt.inSampleSize = 2;
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu_title:
                MemberSet member = MegaApplication.getInstance().getMember();
                String shareText = String.format("http://34.80.52.49:8080/MedicalApi/Link.do?AddFriendsId=%s&guid=%s",
                        member.getCustomerAppId(), member.getCustomerId());

                Log.i(TAG, "onClick: shareText = " + shareText);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "請加我好友，\n" + shareText);
                shareIntent.setType("text/plain");
                startActivity(shareIntent);
                break;
            case R.id.menu_close:
                drawerLayout.closeDrawers();
                break;
            case R.id.top_back:
                exit();
                break;
            case R.id.top_title:
                drawerLayout.closeDrawers();
                exit();
                break;
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetGetRelationships:
                if (result != null && !result.isNull("source")) {
                    JSONArray source = result.optJSONArray("source");
                    try {
                        //MegaApplication.friendList = new JSONArray();
                        MegaApplication.friendNonAllowList = new JSONArray();
                        boolean addMy = false;

                        for (int i = 0; i < source.length(); i++) {
                            JSONObject obj = new JSONObject();
                            JSONObject sourceObj = source.optJSONObject(i);

                            obj.put("appId", sourceObj.optString("app_id"));
                            obj.put("customerAppId", sourceObj.optString("customer_app_id"));
                            obj.put("customerId", sourceObj.optString("customer_id"));
                            obj.put("friendId", sourceObj.optString("friend_id"));
                            obj.put("friendAppId", sourceObj.optString("friend_app_id"));
                            obj.put("createdAt", sourceObj.optString("created_at"));
                            obj.put("updatedAt", sourceObj.optString("updated_at"));
                            obj.put("active", sourceObj.optString("active"));
                            obj.put("name", sourceObj.optString("friend_name"));
                            obj.put("isSync", sourceObj.optString("is_sync"));
                            obj.put("syncState", sourceObj.optString("sync_state"));
                            obj.put("syncedAt", sourceObj.optString("synced_at"));
                            obj.put("isAllow", sourceObj.optString("is_allow"));
                            obj.put("profileImage", sourceObj.optString("profile_image"));

                            //Log.i(TAG, "onPostExecute: " + obj);
                            String friendAppId = obj.optString("friendAppId");

                            if ("0".equals(obj.optString("active")) && "true".equals(obj.optString("isAllow"))) {
                                MegaApplication.friendNonAllowList.put(obj);
                            }
                        }
                        //mMenuList.setAdapter(mListAdapter);
                        //mListAdapter.updateList(MegaApplication.friendList,MegaApplication.friendNonAllowList);
                        //mListAdapter.notifyDataSetChanged();
                        mListAdapter = new ListAdapter();
                        mMenuList.setAdapter(mListAdapter);

                        for (int i = 0; i < mListAdapter.getGroupCount(); i++) {
                            mMenuList.expandGroup(i);
                        }
                        mMenuList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                            @Override
                            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                                return true;
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case MednetGetStepCompetition:
                if (result != null && !result.isNull("source")) {
                    JSONObject source = result.optJSONObject("source");
                    try {
                        MegaApplication.friendList = new JSONArray();
                        competitionData.put("stepCount", source.optString("step_count"));
                        competitionData.put("platformRanking", source.optString("platform_ranking"));
                        competitionData.put("profileImage", source.optString("profile_image"));
                        JSONArray list = source.optJSONArray("data");

                        for (int i = 0; i < list.length(); i++) {
                            JSONObject obj = list.optJSONObject(i);
                            obj.put("stepCount", obj.optInt("step_count", 0) + "");
                            obj.put("profileImage", obj.optString("profile_image", ""));
                            obj.put("platformRanking", obj.optString("platform_ranking", ""));
                            obj.put("customerId", obj.optString("customer_id", ""));
                            obj.put("customerAppId", obj.optString("customer_app_id", ""));
                            String customerAppId = obj.optString("customerAppId");
                            if (MegaApplication.getInstance().getMember().getCustomerAppId().equals(obj.optString("customer_app_id"))) {
                                competitionData.put("ranking", obj.optString("ranking"));
                                competitionData.put("customerId", obj.optString("customer_id"));
                                competitionData.put("customerAppId", obj.optString("customer_app_id"));
                            }
                            MegaApplication.friendList.put(obj);
                            competitionMap.put(customerAppId, obj);
                        }

                        //Log.i(TAG, "onPostExecute: competitionData - " + competitionData);
                        //Log.i(TAG, "onPostExecute: MegaApplication.friendList - " + MegaApplication.friendList);

                        mTotalStepCount.setText(competitionData.optString("stepCount"));
                        mRanking.setText(competitionData.optString("ranking"));
                        mPlatformRanking.setText(competitionData.optString("platformRanking"));
                        stepCount = Integer.parseInt(competitionData.optString("stepCount", "0"));

                        //取得好友列表
                        new CustomerApiTask(this, this, false, "資料讀取中...", TaskEnum.MednetGetRelationships).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        //更新畫布
                        progressbarView.invalidate();
                        progressbarView2.invalidate();
                        progressbarView3.invalidate();

                        //右側選單更新
                        mListAdapter = new ListAdapter();
                        mMenuList.setAdapter(mListAdapter);

                        for (int i = 0; i < mListAdapter.getGroupCount(); i++) {
                            mMenuList.expandGroup(i);
                        }
                        mMenuList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                            @Override
                            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                                return true;
                            }
                        });
                        if ("1".equals(isAllowFriend)) {
                            drawerLayout.openDrawer(GravityCompat.END);

                        }

                        //下載大頭貼
                        setPic();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case MednetAddRelationship:
                if (result == null) {
                    CommonFunc.showToast(this, "加入好友失敗，請檢察網路是否有連接...");
                } else if (!"200".equals(result.optString("state_code"))) {
                    CommonFunc.showToast(this, result.optString("message"));
                } else {
                    CommonFunc.showToast(this, "已送出好友邀請");
                    //發送推播
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetSendMessageWithCustomerId)
                            .executeOnExecutor(MegaApplication.threadPoolExecutor, guid);
                }

                break;
            case MednetIsAcceptRelationship:
                drawerLayout.closeDrawers();
                if (result == null) {
                    CommonFunc.showToast(this, "加入好友失敗");
                } else {
                    CommonFunc.showToast(this, "成功加為好友");
                    new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetStepCompetition).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                break;
            case MednetRejectAddFriend:
                drawerLayout.closeDrawers();
                break;
        }
    }

    private void setPic() {
        for (int i = 0; i < MegaApplication.friendList.length(); i++) {
            JSONObject obj = MegaApplication.friendList.optJSONObject(i);
            Log.i(TAG, "setPic: " + obj.has("profileImage") + " ");

            if (obj.has("profileImage") && obj.optString("profileImage").length() > 0) {
                try {
                    URL url = new URL(obj.optString("profileImage"));
                    new DownloadFilesTask(obj.optString("stepCount")).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, obj.optString("profileImage"));
                } catch (Exception e) {
                    Bitmap nullBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_member);
                    picList.add(nullBitmap);
                    picStepList.add(-1);
                    //e.printStackTrace();
                }

            } else {
                Bitmap nullBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_member);
                picList.add(nullBitmap);
                picStepList.add(-1);

            }

        }
    }

    //下載圖片用
    private class DownloadFilesTask extends AsyncTask<String, Integer, Bitmap> {
        private Bitmap myBitmap;
        private String stepCount;

        DownloadFilesTask(String stepCount) {
            this.stepCount = stepCount;
        }

        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                myBitmap = BitmapFactory.decodeStream(input);
                Log.i(TAG, "run: myBitmap = " + myBitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return myBitmap;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Bitmap result) {
            Log.i(TAG, "onPostExecute: myBitmap = " + myBitmap);
            picList.add(result);
            picStepList.add(Integer.parseInt(stepCount));

            //Looper.prepare();
            Handler mHandler = new Handler(Looper.myLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    progressbarView3.invalidate();
//                    if (MegaApplication.friendList.length() == picList.size()) {
//                        progressbarView3.invalidate();
//                    } 
                }
            };
            Message msg = Message.obtain();
            msg.what = picStepList.size() - 1;
            mHandler.handleMessage(msg);
            //Looper.loop();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        drawerLayout.closeDrawers();
        new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetStepCompetition).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    class ProgressbarView extends View {

        private Context mContext;
        private Paint paint;
        private Paint mPaint = new Paint(); //画笔
        private Paint textPaint = new Paint();
        private RectF squareRectf;
        private Rect dst;
        private int width, height;
        private String type;
        private int picIndex;

        public ProgressbarView(Context context) {
            super(context);
            type = "path";
            mContext = context;
            initView(context, null, 0);
        }

        public ProgressbarView(Context context, String type) {
            super(context);
            this.type = type;
            mContext = context;
            initView(context, null, 0);
        }

        public ProgressbarView(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
            mContext = context;
        }

        public ProgressbarView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            mContext = context;
            initView(context, attrs, defStyleAttr);
        }

        public ProgressbarView(StepCompetitionActivity context, String type, int picIndex) {
            super(context);
            this.type = type;
            this.picIndex = picIndex;
            mContext = context;
            initView(context, null, 0);
        }

        private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
            paint = new Paint();
            squareRectf = new RectF();

            paint.setAntiAlias(true);
            paint.setFlags(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(getResources().getColor(R.color.step_gmae_path));
            paint.setStrokeWidth(10);
            paint.setStyle(Paint.Style.FILL);

            Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
            textPaint.setAntiAlias(true);
            textPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
            textPaint.setStrokeWidth(10);
            textPaint.setTextSize(CommonFunc.dip2px(mContext, 18));
            textPaint.setTypeface(font);
            textPaint.setColor(getResources().getColor(R.color.write_bg));
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(mGameScrollView.getLayoutParams().width * 2, mGameScrollView.getLayoutParams().height);
        }

        @SuppressLint("DrawAllocation")
        @Override
        protected void onDraw(final Canvas canvas) {
            super.onDraw(canvas);
            Bitmap mBitmap = null;
            Boolean finish = false;
            int startX = 0;
            int startY = 0;
            int width = 0;
            int high = 0;
            NS_ENUM nsEnum = null;

            //Log.i("debug", " stepCount: " + stepCount);
            int mission = R.drawable.ic_game_mission_achievement;

            int finishIndex = 0;
            for (int i = 1; i <= 30; i++) {
                float percentage = (float) stepCount / (i * 20000);

                if (percentage >= 1) {
                    mission = R.drawable.ic_game_mission_achievement;
                    percentage = 1;
                } else {
                    percentage = (float) (stepCount - (i - 1) * 20000) / 20000;
                    finish = true;
                    if (finishIndex == 0)
                        finishIndex = i;

                    if (percentage > 0.5) mission = R.drawable.ic_game_mission_achievement;
                    else mission = R.drawable.ic_game_mission;
                }
                //Log.i("debug", "percentage== " + i + " ==== : " + percentage);
                RectF retSquareRectf = new RectF();
                switch (i) {
                    case 1:
                        startX = 0;
                        startY = 87;
                        width = 170;
                        high = 15;

                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        break;
                    case 2:
                        startX = 155;
                        startY = 103;
                        width = 15;
                        high = 76;
                        nsEnum = NS_ENUM.stepViewDirectionTopToDown;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(148, 118, 180, 150);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);

                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);
                        break;
                    case 3:
                        startX = 40;
                        startY = 165;
                        width = 130;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionRightToLeft;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;
                    case 4:
                        startX = 40;
                        startY = 180;
                        width = 15;
                        high = 86;
                        nsEnum = NS_ENUM.stepViewDirectionDownToTop;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(33, 200, 65, 232);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);

                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);
                        break;
                    case 5:
                        startX = 60;
                        startY = 249;
                        width = 125;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;
                    case 6:
                        startX = 164;
                        startY = 194;
                        width = 15;
                        high = 60;
                        nsEnum = NS_ENUM.stepViewDirectionDownToTop;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;
                    case 7:
                        startX = 183;
                        startY = 194;
                        width = 100;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(203, 187, 235, 219);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);
                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);
                        break;
                    case 8:
                        startX = 263;
                        startY = 209;
                        width = 15;
                        high = 122;
                        nsEnum = NS_ENUM.stepViewDirectionTopToDown;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;
                    case 9:
                        startX = 110;
                        startY = 315;
                        width = 168;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionRightToLeft;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;
                    case 10:
                        startX = 110;
                        startY = 330;
                        width = 16;
                        high = 70;
                        nsEnum = NS_ENUM.stepViewDirectionTopToDown;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 11:
                        startX = 120;
                        startY = 385;
                        width = 236;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(170, 378, 202, 410);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);
                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);
                        break;
                    case 12:
                        startX = 334;
                        startY = 122;
                        width = 15;
                        high = 272;
                        nsEnum = NS_ENUM.stepViewDirectionDownToTop;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(327, 252, 359, 284);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);
                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);

                        break;
                    case 13:
                        startX = 210;
                        startY = 122;
                        width = 140;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionRightToLeft;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 14:
                        startX = 210;
                        startY = 37;
                        width = 16;
                        high = 90;
                        nsEnum = NS_ENUM.stepViewDirectionDownToTop;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 15:
                        startX = 227;
                        startY = 37;
                        width = 205;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(327, 30, 359, 62);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);
                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);

                        break;
                    case 16:
                        startX = 411;
                        startY = 55;
                        width = 15;
                        high = 64;
                        nsEnum = NS_ENUM.stepViewDirectionTopToDown;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;
                    case 17:
                        startX = 427;
                        startY = 100;
                        width = 141;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(467, 93, 499, 125);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);
                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);
                        break;
                    case 18:
                        startX = 552;
                        startY = 120;
                        width = 15;
                        high = 85;
                        nsEnum = NS_ENUM.stepViewDirectionTopToDown;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 19:
                        startX = 425;
                        startY = 188;
                        width = 125;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionRightToLeft;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;
                    case 20:
                        startX = 425;
                        startY = 205;
                        width = 15;
                        high = 125;
                        nsEnum = NS_ENUM.stepViewDirectionTopToDown;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(418, 257, 450, 289);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);
                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);
                        break;
                    case 21:
                        startX = 443;
                        startY = 314;
                        width = 75;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        break;
                    case 22:
                        startX = 498;
                        startY = 332;
                        width = 15;
                        high = 73;
                        nsEnum = NS_ENUM.stepViewDirectionTopToDown;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 23:
                        startX = 515;
                        startY = 380;
                        width = 203;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(600, 373, 632, 405);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);
                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);
                        break;
                    case 24:
                        startX = 700;
                        startY = 305;
                        width = 15;
                        high = 91;
                        nsEnum = NS_ENUM.stepViewDirectionDownToTop;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 25:
                        startX = 575;
                        startY = 302;
                        width = 125;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionRightToLeft;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 26:
                        startX = 575;
                        startY = 219;
                        width = 15;
                        high = 81;
                        nsEnum = NS_ENUM.stepViewDirectionDownToTop;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);

                        mBitmap = BitmapFactory.decodeResource(getResources(), mission);
                        dst = new Rect(568, 230, 600, 262);
                        dst = createImgWithOrginalFrame(dst, percentage, NS_ENUM.stepViewImageMission);
                        canvas.drawText((i * 20000 - 10000) + "", dst.left - 15, dst.bottom + 30, textPaint);
                        break;

                    case 27:
                        startX = 588;
                        startY = 219;
                        width = 87;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 28:
                        startX = 658;
                        startY = 31;
                        width = 15;
                        high = 190;
                        nsEnum = NS_ENUM.stepViewDirectionDownToTop;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                    case 29:
                        startX = 670;
                        startY = 31;
                        width = 80;
                        high = 15;
                        nsEnum = NS_ENUM.stepViewDirectionLeftToRight;
                        squareRectf.set(startX, startY, startX + width, startY + high);
                        retSquareRectf = createViewWithOrginalFrame(squareRectf, percentage, nsEnum);
                        break;

                }


                if ("value".equals(type)) {
                    //地圖上的點點
                    if (mBitmap != null && dst != null) {
                        canvas.drawBitmap(mBitmap, null, dst, null);
                    }
                } else if ("person".equals(type)) {
                    //地圖上的人的頭像
                    for (int j = 0; j < MegaApplication.friendList.length(); j++) {
                        JSONObject obj = MegaApplication.friendList.optJSONObject(j);
                        //Log.i(TAG, "onDraw: obj = "+obj);
                        int stepCount = Integer.parseInt(obj.optString("stepCount", "0"));
                        //int stepCount = obj.optInt("stepCount");
                        String profileImage = obj.optString("profileImage");
                        int section = (stepCount / 20000) + 1;
                        percentage = (float) (stepCount - (section - 1) * 20000) / 20000;

                        if (section == i) {
                            //Log.i(TAG, "onDraw: stepCount = " + stepCount);
                            //Log.i(TAG, "percentage: " + percentage + " section = " + section);
                            Bitmap myStep = BitmapFactory.decodeResource(getResources(), R.drawable.ic_game_friend_step);
                            retSquareRectf = createImgWithOrginalFrame(squareRectf, percentage, nsEnum);
                            //Log.i(TAG, "onDraw: " + retSquareRectf.left + " " + retSquareRectf.right);
                            canvas.drawBitmap(myStep, null, retSquareRectf, null);

                            for (int k = 0; k < picList.size(); k++) {
                                Log.i(TAG, "onDraw: " + picStepList.get(k) + "   " + stepCount);
                                if (picStepList.get(k) == stepCount) {
                                    Bitmap pic = getCirleBitmap(picList.get(k));
                                    retSquareRectf = createPicWithOrginalFrame(squareRectf, percentage, nsEnum);
                                    //int radius = (int) retSquareRectf.width() / 2;
                                    canvas.drawBitmap(pic, null, retSquareRectf, null);
                                }
                            }
                        }
                    }

                    if (competitionData != null) {
                        int stepCount = Integer.parseInt(competitionData.optString("stepCount", "0"));
                        String profileImage = competitionData.optString("profileImage");
                        int section = (stepCount / 20000) + 1;
                        //Log.i(TAG, "percentage1 = " + percentage);
                        percentage = (float) (stepCount - (section - 1) * 20000) / 20000;
                        //Log.i(TAG, "percentage2 = " + percentage);
                        if (finishIndex == i) {
                            //Log.i(TAG, "finishIndex = " + finishIndex + " i: " + i);
                            //Log.i(TAG, "stepCount = " + stepCount + " percentage: " + percentage);
                            Bitmap myStep = BitmapFactory.decodeResource(getResources(), R.drawable.ic_game_my_step);
                            retSquareRectf = createImgWithOrginalFrame(squareRectf, percentage, nsEnum);
                            //Log.i(TAG, "onDraw: " + retSquareRectf.left + " " + retSquareRectf.right);
                            canvas.drawBitmap(myStep, null, retSquareRectf, null);
                            //new BitmapWorker(canvas, retSquareRectf, paint).execute(profileImage);
                            Log.i(TAG, "run: profileImage = " + profileImage);

                            for (int k = 0; k < picList.size(); k++) {
                                Log.i(TAG, "onDraw: " + picStepList.get(k) + "   " + stepCount);
                                if (picStepList.get(k) == stepCount) {
                                    Bitmap pic = getCirleBitmap(picList.get(k));
                                    retSquareRectf = createPicWithOrginalFrame(squareRectf, percentage, nsEnum);
                                    //int radius = (int) retSquareRectf.width() / 2;
                                    canvas.drawBitmap(pic, null, retSquareRectf, null);
                                }
                            }

                        }
                    }
                } else if ("pic".equals(type)) {
//                    for (int j = 0; j < picList.size(); j++) {
//                        int picStep = picStepList.get(picIndex);
//                        int section = (picStep / 20000) + 1;
//                        percentage = (float) (picStep - (section - 1) * 20000) / 20000;
//
//                        if (section == i) {
//                            Log.i(TAG, "onDraw:picIndex = " + picIndex + " picStep = " + picStep + "pic " + section + "   " + i);
//                            Bitmap pic = getCirleBitmap(picList.get(picIndex));
//
//                            retSquareRectf = createPicWithOrginalFrame(squareRectf, percentage, nsEnum);
//                            //int radius = (int) retSquareRectf.width() / 2;
//
//                            canvas.drawBitmap(pic, null, retSquareRectf, null);
//                        }
//
//
//                    }
                } else {
                    //畫路線
                    if (!finish || finishIndex == i) {
                        canvas.drawRoundRect(retSquareRectf, 0, 0, paint);
                    }
                }

            }
        }

        //將圖片化成圓形
        public Bitmap getCirleBitmap(Bitmap bmp) {
            int w = bmp.getWidth();
            int h = bmp.getHeight();
            int r = Math.min(w, h);

            //creat paint
            Paint paint = new Paint();
            paint.setAntiAlias(true);

            //新创建一个Bitmap对象newBitmap 宽高都是r
            Bitmap newBitmap = Bitmap.createBitmap(r, r, Bitmap.Config.ARGB_8888);

            //创建一个使用newBitmap的Canvas对象
            Canvas canvas = new Canvas(newBitmap);

            //创建一个Path对象，path添加一个圆 圆心半径均是r / 2， Path.Direction.CW顺时针方向
            Path path = new Path();
            path.addCircle(r / 2, r / 2, r / 2, Path.Direction.CW);
            //canvas绘制裁剪区域
            canvas.clipPath(path);
            //canvas将图画到留下的圆形区域上
            canvas.drawBitmap(bmp, 0, 0, paint);

            return newBitmap;
        }

        //設定路線的細節
        private RectF createViewWithOrginalFrame(RectF squareRectf, float percentage, NS_ENUM direction) {
            float percent = (float) (mGameImg.getLayoutParams().width / 750.0);
            squareRectf.left *= percent;
            squareRectf.top *= percent;
            squareRectf.right *= percent;
            squareRectf.bottom *= percent;

            if (direction == NS_ENUM.stepViewDirectionLeftToRight || direction == NS_ENUM.stepViewDirectionRightToLeft) {
                squareRectf.top -= 7.5;
                squareRectf.bottom += 15;
            } else {
                squareRectf.left -= 7.5;
                squareRectf.right += 15;
            }

            RectF retSquareRectf = new RectF();

            if (percentage > 0.99) {
                return squareRectf;
            } else {
                switch (direction) {
                    case stepViewDirectionLeftToRight: {
                        float viewWidth = (squareRectf.width() - 15 * percent) * percentage;
                        retSquareRectf.set(squareRectf.left, squareRectf.top, squareRectf.left + viewWidth, squareRectf.bottom);
                        break;
                    }
                    case stepViewDirectionRightToLeft: {
                        float viewWidth = squareRectf.width() - ((squareRectf.width() - 15 * percent) * percentage);
                        retSquareRectf.set(squareRectf.left + viewWidth, squareRectf.top, squareRectf.right, squareRectf.bottom);
                        break;
                    }
                    case stepViewDirectionTopToDown: {
                        float viewHeight = (squareRectf.height() - 15 * percent) * percentage;
                        retSquareRectf.set(squareRectf.left, squareRectf.top, squareRectf.right, squareRectf.top + viewHeight);
                        break;
                    }
                    case stepViewDirectionDownToTop: {
                        float viewHeight = squareRectf.height() - ((squareRectf.height() - 15 * percent) * percentage);
                        retSquareRectf.set(squareRectf.left, (squareRectf.top + viewHeight), squareRectf.right, squareRectf.bottom);
                        break;
                    }
                    default:
                        break;
                }
            }
            //Log.i(TAG, " left " + retSquareRectf.left + " right " + retSquareRectf.right + " top " + retSquareRectf.top + " bottom " + retSquareRectf.bottom);
            return retSquareRectf;
        }

        //設定點位的細節
        private Rect createImgWithOrginalFrame(Rect rect, float percentage, NS_ENUM direction) {
            float percent = (float) (mGameImg.getLayoutParams().width / 750.0);
            rect.left *= percent;
            rect.top *= percent;
            rect.right *= percent;
            rect.bottom *= percent;
            return rect;
        }

        //設定個人像匡的細節
        private RectF createImgWithOrginalFrame(RectF rect, float percentage, NS_ENUM direction) {
            float percent = (float) (mGameImg.getLayoutParams().width / 750.0);
            //Log.i(TAG, "createImgWithOrginalFrame: percentage = " + rect.left + " " + rect.right);

            RectF retRect = new RectF();
            float imgWidth = (float) ((15 * percent) * 3);
            float imgHeight = (float) (imgWidth / 168.0 * 212);

            switch (direction) {
                case stepViewDirectionLeftToRight: {
                    float target = (rect.width() - 15 * percent) * percentage;
                    float startX = (rect.left + target) - imgWidth / 2;
                    float startY = (rect.top + rect.height() / 2) - imgHeight;
                    retRect.set(startX, startY, startX + imgWidth, startY + imgHeight);
                    break;
                }
                case stepViewDirectionRightToLeft: {
                    float target = rect.width() - ((rect.width() - 15 * percent) * percentage);
                    float startX = (float) (rect.left + target - imgWidth / 2.0);
                    float startY = (rect.top + rect.height() / 2) - imgHeight;
                    retRect.set(startX, startY, startX + imgWidth, startY + imgHeight);
                    break;
                }
                case stepViewDirectionTopToDown: {
                    float target = rect.top + (rect.height() - 15 * percent) * percentage;
                    float startX = (rect.left + rect.width() / 2) - imgWidth / 2;
                    //float startY = squareRectf.bottom
                    retRect.set(startX, target - imgHeight, startX + imgWidth, target);
                    break;
                }
                case stepViewDirectionDownToTop: {
                    float target = rect.top + (rect.height() - ((rect.height() - 15 * percent) * percentage));
                    float startX = (rect.left + rect.width() / 2) - imgWidth / 2;
                    //float startY = squareRectf.bottom
                    retRect.set(startX, target - imgHeight, startX + imgWidth, target);
                    break;
                }
                default:
                    break;
            }

            return retRect;
        }

        //設定個人照片的細節
        private RectF createPicWithOrginalFrame(RectF rect, float percentage, NS_ENUM direction) {
            float percent = (float) (mGameImg.getLayoutParams().width / 750.0);
            RectF retRect = new RectF();
            float imgWidth = (float) ((15 * percent) * 2.6);
            float imgY = (float) ((imgWidth / 168.0 * 212) + (5.5 * percent));
            float imgHeight = imgWidth;

            switch (direction) {
                case stepViewDirectionLeftToRight: {
                    float target = (rect.width() - 15 * percent) * percentage;
                    float startX = (rect.left + target) - imgWidth / 2;
                    float startY = (rect.top + rect.height() / 2) - imgY;
                    retRect.set(startX, startY, startX + imgWidth, startY + imgHeight);
                    break;
                }
                case stepViewDirectionRightToLeft: {
                    float target = rect.width() - ((rect.width() - 15 * percent) * percentage);
                    float startX = (float) (rect.left + target - imgWidth / 2.0);
                    float startY = (rect.top + rect.height() / 2) - imgY;
                    retRect.set(startX, startY, startX + imgWidth, startY + imgHeight);
                    break;
                }
                case stepViewDirectionTopToDown: {
                    float target = rect.top + (rect.height() - 15 * percent) * percentage;
                    float startX = (rect.left + rect.width() / 2) - imgWidth / 2;
                    retRect.set(startX, target - imgY, startX + imgWidth, (target - imgY) + imgHeight);
                    break;
                }
                case stepViewDirectionDownToTop: {
                    float target = rect.top + (rect.height() - ((rect.height() - 15 * percent) * percentage));
                    float startX = (rect.left + rect.width() / 2) - imgWidth / 2;
                    retRect.set(startX, target - imgY, startX + imgWidth, (target - imgY) + imgHeight);
                    break;
                }
                default:
                    break;
            }
            return retRect;
        }

        public void setLayoutParams(int w, int h) {
            width = w;
            height = h;

        }
    }

    class ListAdapter extends BaseExpandableListAdapter {

        @Override
        public int getGroupCount() {
            if (MegaApplication.friendList != null && MegaApplication.friendList.length() > 0 && MegaApplication.friendNonAllowList != null && MegaApplication.friendNonAllowList.length() > 0) {
                return 2;
            } else if (MegaApplication.friendList != null && MegaApplication.friendList.length() > 0) {
                return 1;
            }
            return 0;
        }

        @Override
        public int getChildrenCount(int i) {
            if (i == 0) {
                return MegaApplication.friendList.length();
            } else {
                return MegaApplication.friendNonAllowList.length();
            }
        }

        @Override
        public Object getGroup(int i) {
            if (i == 0) {
                return "好友";
            } else {
                return "待確認";
            }
        }

        @Override
        public Object getChild(int i, int j) {
            if (i == 0) {
                return MegaApplication.friendList.optJSONObject(j);
            } else {
                return MegaApplication.friendNonAllowList.optJSONObject(j);
            }

        }

        @Override
        public long getGroupId(int i) {
            return 0;
        }

        @Override
        public long getChildId(int i, int i1) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
            convertView = LayoutInflater.from(StepCompetitionActivity.this).inflate(R.layout.cell_group, null);
            TextView groupTitle = convertView.findViewById(R.id.group_title);
            groupTitle.setText(getGroup(i).toString());
            return convertView;
        }

        @Override
        public View getChildView(int i, int j, boolean b, View convertView, ViewGroup viewGroup) {
            convertView = LayoutInflater.from(StepCompetitionActivity.this).inflate(R.layout.friend_cell, null);
            CircleImageView img = convertView.findViewById(R.id.cell_icon);
            TextView title = convertView.findViewById(R.id.cell_title);
            TextView value = convertView.findViewById(R.id.cell_value);
            TextView agree = convertView.findViewById(R.id.agree);
            TextView notAgree = convertView.findViewById(R.id.not_agree);
            JSONObject item = (JSONObject) getChild(i, j);
            try {
                if (!"".equals(item.optString("profileImage")) && !"null".equals(item.optString("profileImage"))) {
                    Log.i(TAG, "img: " + img.getContext());
                    Glide.with(img.getContext()).load(item.optString("profileImage")).into(img);
                } else {
                    img.setImageResource(R.drawable.ic_member);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            title.setText(item.optString("name"));
            value.setText(item.optString("stepCount") + " 步");
            if (i == 0) {
                value.setVisibility(View.VISIBLE);
                agree.setVisibility(View.GONE);
                notAgree.setVisibility(View.GONE);
            } else {
                value.setVisibility(View.GONE);
                agree.setVisibility(View.VISIBLE);
                notAgree.setVisibility(View.GONE);
            }

            agree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new CustomerApiTask(StepCompetitionActivity.this, StepCompetitionActivity.this,
                            false, "", TaskEnum.MednetIsAcceptRelationship)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, item.optString("appId"), "true");
                }
            });
            notAgree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new CommonApiTask(StepCompetitionActivity.this, StepCompetitionActivity.this,
                            false, "", TaskEnum.MednetRejectAddFriend)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, item.optString("appId"), MegaApplication.getInstance().getMember().getCustomerId(), "false");
                }
            });
            return convertView;

        }

        @Override
        public boolean isChildSelectable(int i, int i1) {
            return false;
        }

        public void updateList(JSONArray friendList, JSONArray friendNonAllowList) {
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        exit();
    }

    private void exit() {
        this.finish();
    }
}
