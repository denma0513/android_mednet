package com.mgear.mednetapp;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.DrawerMenuAdapter;
import com.mgear.mednetapp.fragments.HealthDetailFragment;
import com.mgear.mednetapp.fragments.HealthListFragment;
import com.mgear.mednetapp.fragments.HomePageFragment;
import com.mgear.mednetapp.fragments.WebViewUploadFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.helpers.BadgeActionProvider;
import com.mgear.mednetapp.helpers.BottomNavigationViewHelper;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.OnItemClickListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.task.common.NewDownloadImageTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.HttpConnectFunc;
import com.mgear.mednetapp.utils.WebViewUtil;
import com.mgear.mednetapp.views.CircleImageView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class MainActivity extends BaseActivity implements View.OnClickListener, TaskPost2ExecuteImpl, INativeActionImpl, IBackHandleIpml,
        NavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemSelectedListener,
        OnItemClickListener {

    private static final String TAG = "MainActivity debug";
    private static final String actionType = MegaApplication.ActionTypeMainPage;
    private static int DETAIL = 99;
    private static int GoogleFit = 98;
    private static final int REQUEST = 112;
    private static boolean backBtn = false;

    //畫面元件
    private DrawerLayout drawerLayout;
    private ExpandableListView mMenuListView;
    private BottomNavigationView mBottomNavigation, mBottomNavigationDr;
    private NavigationView menuLayout;
    private AppBarLayout mTopBar;
    private Toolbar toolBar;
    //private Fragment fragmentMain;
    private FrameLayout mainContent;
    private ImageView mMemberPic, mTopBack, mMainBack;
    private RelativeLayout mSpecialTop, mTop;
    private LinearLayout mSpecialBottomLayout;
    private TextView mTopTitle, mTopRightBtn;
    private Button mBottomButton;

    //fragment
    private HomePageFragment homePageFragment;
    private HealthListFragment healthListFragment;
    private HealthDetailFragment healthDetailFragment;
    private CircleImageView mDrowerHeaderUserPic;
    private TextView mDrowerHeaderUserName, mDrowerHeaderMail;
    private ActionBarDrawerToggle acrionActionBarDrawerToggle; //左側選單控制元件
    //private FrameLayout webView, webView2;
    private WebView webView, webView2;

    //邏輯元件
    private MegaBaseFragment selectedFragment;
    private String topType = "default";
    private String page = "home";
    private DrawerMenuAdapter drawerMenuAdapter;
    private ArrayList<JSONObject> mWebViewList;
    private MenuItem notice, shoppingCar;
    private BadgeActionProvider mNoticeProvider, mShoppingCarProvider;
    private String webLoginUrl, webLoginUrl2;
    private boolean reInsertEmail = false;

    private String action = null, guid = null, AddFriendsId = null, actionTarget = null, notification = null;

    /****** set fragment ****/
    /**
     * 首頁fragment
     */
    private void setHomeFragment() {
        //Log.d("debug", "setHomeFragment");
        cannotBack();
        CommonFunc.getHelloWord();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        homePageFragment = new HomePageFragment();
        ft = fragmentManager.beginTransaction();

        if (homePageFragment.isAdded()) { // 如果 home fragment 已經被 add 過，
            ft.show(homePageFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.main_content, homePageFragment, "HOME"); // 使用 add 方法。
            ft.addToBackStack("HOME");
        }
        ft.commit();
    }


    /**
     * 健康列表fragment
     */
    private void setHealthListFragment() {
        //Log.i("debug", "setHealthListFragment");
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        healthListFragment = new HealthListFragment();
        //fragmentMain = healthListFragment;
        if (healthListFragment.isAdded()) { // 如果 home fragment 已經被 add 過，
            ft.show(healthListFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.main_content, healthListFragment, "HEALTH"); // 使用 add 方法。
            ft.addToBackStack("HEALTH");
        }
        ft.commit();
    }

    //開啟健康記錄設定
    public void goHealthDetail(int index, JSONObject data) {
        //Log.i("debug", "index = " + index + " data" + data);
        Intent intent = new Intent(this, HealthPushDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index", index);
        bundle.putString("data", data.toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, DETAIL);
    }

    /**
     * 健康資料fragment
     */
    private void setHealthDetailFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        healthDetailFragment = new HealthDetailFragment();
        //fragmentMain = healthListFragment;

        //fragmentMain.setHealthData(healthData);
        if (healthDetailFragment.isAdded()) { // 如果 home fragment 已經被 add 過，
            ft.show(healthDetailFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.main_content, healthDetailFragment, "HEALTHDETAIL"); // 使用 add 方法。
            ft.addToBackStack("HEALTHDETAIL");
        }
        ft.commit();
    }


    public void showTopBar() {
        Log.i(TAG, "showTopBar: ");
        TypedValue tv = new TypedValue();
        int actionBarHeight = CommonFunc.dip2px(this, 60);
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
//        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        lp.setMargins(0, 0, 0, 0);
//        mainContent.setLayoutParams(lp);
        mTopBar.setVisibility(View.VISIBLE);
        mTop.setVisibility(View.VISIBLE);
    }

    public void hideTopBar() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, 0);
        //imageView.setLayoutParams(lp);
        mainContent.setLayoutParams(lp);
        mTopBar.setVisibility(View.GONE);
        mSpecialTop.setVisibility(View.GONE);
        mTop.setVisibility(View.GONE);
    }

    private void initView() {

        mTopBar.setVisibility(View.VISIBLE);
        mSpecialTop.setVisibility(View.GONE);
        mSpecialBottomLayout.setVisibility(View.GONE);

        if ("2".equals(MegaApplication.getInstance().getMember().getAuthType())) {
            mBottomNavigation.setVisibility(View.GONE);
            mBottomNavigationDr.setVisibility(View.VISIBLE);
        } else {
            mBottomNavigation.setVisibility(View.VISIBLE);
            mBottomNavigationDr.setVisibility(View.GONE);
        }

        //讓bottom的icon固定
        BottomNavigationViewHelper.disableShiftMode(mBottomNavigation);
        mBottomNavigation.setItemTextAppearanceActive(R.style.NavigationDrawer);
        mBottomNavigation.setItemTextAppearanceInactive(R.style.NavigationDrawer);
        mBottomNavigation.setOnNavigationItemSelectedListener(this);

        BottomNavigationViewHelper.disableShiftMode(mBottomNavigationDr);
        mBottomNavigationDr.setItemTextAppearanceActive(R.style.NavigationDrawer);
        mBottomNavigationDr.setItemTextAppearanceInactive(R.style.NavigationDrawer);
        mBottomNavigationDr.setOnNavigationItemSelectedListener(this);


        //預設bottom選項
        mBottomNavigation.setSelectedItemId(R.id.mainPage);

        if (MegaApplication.ActionTypeMainPage.equals(actionType) && MegaApplication.ActionTypeHealthPush.equals(actionType)) {
            //mBottomNavigation.setSelectedItemId(R.id.mainPage);
        }
        setMenu();

        //將toolbar 設定到頂端的action bar
        ((AppCompatActivity) this).setSupportActionBar(toolBar);

        //隱藏title
        ((AppCompatActivity) this).getSupportActionBar().setDisplayShowTitleEnabled(false);

        //左側選單漢堡圖示設定
        acrionActionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolBar, R.string.msg_drawer_open, R.string.msg_drawer_close) {
            @Override
            public void onDrawerClosed(View draweView) {
                super.onDrawerClosed(draweView);
                Log.d(TAG, "onDrawerClosed");
            }

            @Override
            public void onDrawerOpened(View draweView) {
                super.onDrawerOpened(draweView);
                new CommonApiTask(MainActivity.this, MainActivity.this, false, "", TaskEnum.MednetAppLogEven)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "top_button", "menu");
                Log.d(TAG, "onDrawerOpened");
            }
        };
        drawerLayout.addDrawerListener(acrionActionBarDrawerToggle);
        acrionActionBarDrawerToggle.syncState();

        //取消功能表圖示的色調
        mBottomNavigation.setItemIconTintList(null);
        //mSpecialBottom.setItemIconTintList(null);
        getProfile();
        //new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetProfile).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void setMenu() {
        //設定左側選單的adapter 並展開所有選項 且設定group無法點擊
        drawerMenuAdapter = new DrawerMenuAdapter(this);
        if (mMenuListView != null) {
            mMenuListView.setAdapter(drawerMenuAdapter);
        }
        for (int i = 0; i < drawerMenuAdapter.getGroupCount(); i++) {
            mMenuListView.expandGroup(i);
        }
        mMenuListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return true;
            }
        });
    }

    public void exit() {
        //Log.i("debug", "exit");
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void canBack() {
        acrionActionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mMainBack.setVisibility(View.VISIBLE);
    }

    public void cannotBack() {
        if (acrionActionBarDrawerToggle != null) {
            acrionActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mMainBack.setVisibility(View.GONE);
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void getProfile() {
        new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetProfile).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    //背景登入網頁
    private void webLogin() {
        //登入網頁
        WebSettings webSettings = webView.getSettings();
        webSettings = webView2.getSettings();
        webSettings.setJavaScriptEnabled(true);
        WebViewUtil.configWebView(webView);
        WebViewUtil.configWebView(webView2);

        Log.i(TAG, "webLogin: getAuthType() = " + MegaApplication.getInstance().getMember().getAuthType() + " MegaApplication.WebLogin = " + MegaApplication.WebLogin);
        if (!MegaApplication.WebLogin || action != null) {
            webView.setWebViewClient(new MednetWebViewClient());
            webView2.setWebViewClient(new MednetWebViewClient());

            //導頁部分
            if (action != null) {
                showLoading("確認登入中，請稍候...");
            }

            if ("1".equals(MegaApplication.getInstance().getMember().getAuthType()))
                showLoading("醫師登入中，請稍候...");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (reInsertEmail) {
                        new CustomerApiTask(MainActivity.this, MainActivity.this, true, "", TaskEnum.MednetGetWebToken).execute();
                        reInsertEmail = false;
                        return;
                    }
                    webView.loadUrl(webLoginUrl);
                }
            }, 0);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.i("debug", "醫師諮詢: " + webLoginUrl2);
                    webView2.loadUrl(webLoginUrl2);
                }
            }, 0);
        }
        MegaApplication.WebLogin = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());

        setContentView(R.layout.activity_main_frame);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            page = extras.getString("page") == null ? "home" : extras.getString("page");
            notification = extras.getString("notification");

            action = extras.getString("action");
            guid = extras.getString("guid");
            AddFriendsId = extras.getString("AddFriendsId");
            actionTarget = extras.getString("actionTarget");
            if (CommonFunc.isBlank(actionTarget)) {
                actionTarget = extras.getString("url");
            }


            Log.i(TAG, "onCreate: notification = " + notification + " action = " + action + " actionTarget = " + actionTarget);
        }
        drawerLayout = findViewById(R.id.drawer_layout);
        mMenuListView = findViewById(R.id.menu_list_view);

        //一般top
        mTopBar = findViewById(R.id.topBar);
        toolBar = findViewById(R.id.toolbar);
        mainContent = findViewById(R.id.main_content);
        mMemberPic = findViewById(R.id.main_pic);
        mBottomNavigation = findViewById(R.id.main_bottom);
        mBottomNavigationDr = findViewById(R.id.main_bottom_dr);
        mMainBack = findViewById(R.id.main_back);

        //Special top
        mSpecialTop = findViewById(R.id.special_top);
        mTop = findViewById(R.id.top);
        mTopBack = findViewById(R.id.top_back);
        mTopTitle = findViewById(R.id.top_title);
        mTopRightBtn = findViewById(R.id.top_right_btn);
        mSpecialBottomLayout = findViewById(R.id.special_bottom_layout);
        //mSpecialBottom = findViewById(R.id.special_bottom);
        mBottomButton = findViewById(R.id.bottom_button);

        mBottomButton.setOnClickListener((View.OnClickListener) this);
        mTopBack.setOnClickListener((View.OnClickListener) this);
        mTopRightBtn.setOnClickListener((View.OnClickListener) this);
        mMemberPic.setOnClickListener(this::onClick);
        mMainBack.setOnClickListener(this::onClick);

        //左側選單
        mDrowerHeaderUserPic = findViewById(R.id.drower_header_user_pic);
        mDrowerHeaderUserName = findViewById(R.id.drower_header_user_name);
        mDrowerHeaderMail = findViewById(R.id.drower_header_mail);
        mWebViewList = new ArrayList<JSONObject>();
        mDrowerHeaderUserPic.setOnClickListener(this::onClick);
        setDrowerData();
        initView();

        webView = findViewById(R.id.web_login_view);
        webView2 = findViewById(R.id.web_login_view2);


        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST);
            } else {
                //do here
            }
        } else {
            //do here
        }

        if (guid != null && AddFriendsId != null) {
            Intent intent = new Intent(this, StepCompetitionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("guid", guid);
            bundle.putString("AddFriendsId", AddFriendsId);
            intent.putExtras(bundle);
            startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeStepCompetition));
            //new CustomerApiTask(this, this, false, "", TaskEnum.MednetAddRelationship).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AddFriendsId);
        }


    }

    /**
     * 設定左側選單資料
     */
    public void setDrowerData() {
        //Log.i("debug", "更新照片 " + MegaApplication.getInstance().getMember().getProfileImage());
        if (!"".equals(MegaApplication.getInstance().getMember().getProfileImage())) {
            new NewDownloadImageTask(mDrowerHeaderUserPic).executeOnExecutor(MegaApplication.threadPoolExecutor, MegaApplication.getInstance().getMember().getProfileImage());
            new NewDownloadImageTask(mMemberPic).executeOnExecutor(MegaApplication.threadPoolExecutor, MegaApplication.getInstance().getMember().getProfileImage());
            mDrowerHeaderUserPic.setWidth(CommonFunc.dip2px(this, 60));
        }
        mDrowerHeaderUserName.setText(MegaApplication.getInstance().getMember().getName());
        mDrowerHeaderMail.setText(MegaApplication.getInstance().getMember().getEmail());
    }

    private Handler sHandler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.i(TAG, "onPageFinished: msg.what = " + msg.what);
            Log.i(TAG, "onPageFinished: action = " + action + " actionTarget = " + actionTarget);
            hideLoading();
            if (action != null) {
                switch (action) {
                    case MegaApplication.ActionTypeWebView: {
                        triggerAction(MegaApplication.ActionTypeWebView, actionTarget);
                    }
                    break;
                    case MegaApplication.ActionTypeStepCompetition: {
                        Intent intent = new Intent(MainActivity.this, StepCompetitionActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("isAllowFriend", "1");
                        intent.putExtras(bundle);
                        startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeStepCompetition));
                    }
                    break;
                }
            }


        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_pic:
                //Log.i("debug", "MegaApplication.getInstance().getMember().getAccessToken() = " + MegaApplication.getInstance().getMember().getAccessToken());
                if (!"".equals(MegaApplication.getInstance().getMember().getAccessToken())) {
                    triggerAction("1", "https://med-net.com/Member");
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "top_button", "https://med-net.com/Member");
                } else {
                    Bundle bundle = new Bundle();
                    Intent intent = new Intent(this, UserLoginActivity.class);
                    startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));
                }
                break;
            case R.id.drower_header_user_pic:
                //Log.i("debug", "MegaApplication.getInstance().getMember().getAccessToken() = " + MegaApplication.getInstance().getMember().getAccessToken());
                if (!"".equals(MegaApplication.getInstance().getMember().getAccessToken())) {
                    triggerAction("1", "https://med-net.com/Member");
                } else {
                    Bundle bundle = new Bundle();
                    Intent integer = new Intent(this, UserLoginActivity.class);
                    bundle.putString("action", MegaApplication.ActionTypeWebView);
                    bundle.putString("target", "https://med-net.com/Member");
                    integer.putExtras(bundle);
                    startActivityForResult(integer, Integer.parseInt(MegaApplication.ActionTypeLogin));
                }
                break;
            case R.id.main_back:
                //Log.i("debug", "webFragment.canGoBack() = " + webFragment.canGoBack());
                if (webFragment != null) {
                    if (webFragment.canGoBack()) {
                        webFragment.goBack();
                    } else {
                        onBackPressed();
                    }
                } else {
                    onBackPressed();
                }
                break;
        }
    }

    @Override
    public void onItemClick(View view, int position, JSONObject data) {
        goHealthDetail(position, data);
    }

    /**
     * Api reponse handle
     */
    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case AppCheckUpgradeTask:
                JSONObject av = (JSONObject) result;
                //判斷應用程式的狀態
                //Log.i("debug", "av + " + av);

                if (!av.isNull("data") && av.optJSONObject("data").optBoolean("result")) {

                } else {
                    String newVersion = av.optJSONObject("data").optString("newVersion");
                    String appPath = av.optJSONObject("data").optString("url");
                    if (!"".equals(newVersion)) {
                        new AlertDialog.Builder(this)
                                .setTitle(R.string.msg_new_version)
                                .setMessage(String.format(Locale.getDefault(), "應用程式版號 %s", newVersion))
                                .setPositiveButton("安裝", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        //判斷是否下載應用程式
                                        Intent intent = new Intent();
                                        intent.setAction(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.mgear.mednetapp"));
                                        if (appPath == null) {
                                            //取得應用程式更新訊息
                                            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.mgear.mednetapp"));
                                        } else {
                                            intent.setData(Uri.parse(appPath));
                                        }
                                        startActivity(intent);
                                    }
                                }).setPositiveButton("關閉", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).show();
                    }
                }
                break;
            case MednetGetProfile:
                //Log.i("debug", result + "result = " + result);
                if (result != null && !result.isNull("source")) {
                    JSONObject source;
                    source = result.optJSONObject("source");
                    MegaApplication.getInstance().getMember().setName(source.optString("name"));
                    MegaApplication.getInstance().getMember().setCustomerAppId(source.optString("customer_app_id"));
                    MegaApplication.getInstance().getMember().setCustomerId(source.optString("customer_id"));
                    MegaApplication.getInstance().getMember().setEmail(source.optString("email"));
                    MegaApplication.getInstance().getMember().setFirstName(source.optString("first_name"));
                    MegaApplication.getInstance().getMember().setLastName(source.optString("last_name"));
                    MegaApplication.getInstance().getMember().setWebToken(source.optString("web_token"));
                    MegaApplication.getInstance().getMember().setCellphone(source.optString("cellphone"));
                    MegaApplication.getInstance().getMember().setGender(source.optString("gender"));
                    MegaApplication.getInstance().getMember().setBirthday(source.optString("birthday"));
                    MegaApplication.getInstance().getMember().setProfileImage(source.optString("profile_image"));
                    MegaApplication.getInstance().getMember().setCoverImage(source.optString("cover_image"));

                    if (CommonFunc.isBlank(MegaApplication.getInstance().getMember().getEmail())) {
                        reInputEmail();
                        return;
                    }

                    SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
                    settings.edit().putString("name", MegaApplication.getInstance().getMember().getName())
                            .putString("firstName", MegaApplication.getInstance().getMember().getFirstName())
                            .putString("profileImage", MegaApplication.getInstance().getMember().getProfileImage())
                            .putString("email", MegaApplication.getInstance().getMember().getEmail())
                            .apply();
                    setDrowerData();
                    setMenu();

                    JSONObject sendObj = new JSONObject();
                    try {
                        sendObj.put("id", source.optString("customer_id"));
                        sendObj.put("email", source.optString("email"));
                        sendObj.put("name", source.optString("name"));
                        sendObj.put("first_name", source.optString("first_name"));
                        sendObj.put("last_name", source.optString("last_name"));
                        sendObj.put("id_number", source.optString("id_number"));
                        sendObj.put("cellphone", source.optString("cellphone"));
                        sendObj.put("gender", source.optInt("gender"));
                        sendObj.put("birthday", source.optString("birthday"));
                        sendObj.put("view_count", 0);
                        sendObj.put("self_introduction", source.optString("self_introduction"));
                        sendObj.put("active", 1);
                        sendObj.put("create_by", "null");
                        sendObj.put("create_from", "null");
                        sendObj.put("profile_image", source.optString("profile_image"));
                        sendObj.put("cover_image", source.optString("cover_image"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String webToken = source.optString("web_token");
                    String customerId = source.optString("customer_id");

                    webLoginUrl = String.format("https://med-net.com/CMSContent?user_id=%s&token=%s", customerId, webToken);
                    webLoginUrl2 = String.format("https://expert.med-net.com/login/GetOAuthLogInfo?data=%s", sendObj.toString());

                    new CustomerApiTask(this, this, false, "", TaskEnum.MednetSetFCMToken).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    //註冊推播token
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetRegisterPushTokenSave).executeOnExecutor(MegaApplication.threadPoolExecutor);
                    //登入身份
                    new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetAuthType).executeOnExecutor(MegaApplication.threadPoolExecutor);
                    //new CommonApiTask(this, this, false, "", TaskEnum.HomeGetAuthTypeApi).executeOnExecutor(MegaApplication.threadPoolExecutor);
                    //撈出badge
                    new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetBadge).executeOnExecutor(MegaApplication.threadPoolExecutor);
                }
                break;
            case MednetGetAuthType:
                JSONObject authType = new JSONObject();
                //Log.i("debug", "authType: " + result);
                if (result != null && "200".equals(result.optString("code"))) {
                    authType = result.optJSONObject("data");
                    //Log.i("debug", "authType: " + authType);

                    MegaApplication.getInstance().getMember().setAuthType(authType.optString("isExpert"));
                    MegaApplication.getInstance().getMember().setDrID(authType.optString("drID"));
                    MegaApplication.getInstance().getMember().setDoctorArticleCount(authType.optString("Release"));
                    MegaApplication.getInstance().getMember().setUnpromotedArticleCount(authType.optString("NotRelease"));

                    //將身份資訊寫入本地端
                    SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
                    settings.edit().putString("authType", MegaApplication.getInstance().getMember().getAuthType())
                            .putString("drID", MegaApplication.getInstance().getMember().getDrID())
                            .putString("doctorArticleCount", MegaApplication.getInstance().getMember().getDoctorArticleCount())
                            .putString("unpromotedArticleCount", MegaApplication.getInstance().getMember().getUnpromotedArticleCount())
                            .apply();

                    if ("1".equals(MegaApplication.getInstance().getMember().getAuthType())) {
                        //醫師
                        mBottomNavigation.setVisibility(View.GONE);
                        mBottomNavigationDr.setVisibility(View.VISIBLE);
                        //showLoading("醫師登入中，請稍候...");
                    } else {
                        //一般用戶
                        mBottomNavigation.setVisibility(View.VISIBLE);
                        mBottomNavigationDr.setVisibility(View.GONE);
                    }
                    if (homePageFragment != null) {
                        homePageFragment.refreshData();
                        //setHomeFragment();
                    }
                    webLogin();
                    setMenu();
                }
                break;
            case MednetGetBadge: {
                if (result != null && "200".equals(result.optString("code"))) {
                    JSONObject data = result.optJSONObject("data");
                    if (data == null)
                        return;
                    MegaApplication.shoppingCart = data.optString("shoppingCart", "0");
                    MegaApplication.bell = data.optString("bell", "0");
                    MegaApplication.reservation = data.optString("reservation", "0");

                    int bell = 0, shopping = 0;
                    try {
                        bell = Integer.parseInt(MegaApplication.bell);
                    } catch (Exception e) {
                        bell = 0;
                    }
                    try {
                        shopping = Integer.parseInt(MegaApplication.shoppingCart);
                    } catch (Exception e) {
                        shopping = 0;
                    }
                    mNoticeProvider.setBadge(bell);
                    mShoppingCarProvider.setBadge(shopping);

                }
                break;
            }
            case MednetAddRelationship: {
                if (result == null) {
                    CommonFunc.showToast(this, "加入好友失敗，請檢察網路是否有連接...");
                } else if (!"200".equals(result.optString("state_code"))) {
                    CommonFunc.showToast(this, result.optString("message"));
                } else {
                    CommonFunc.showToast(this, "已送出好友邀請");
                }
                break;
            }
            case FixEmail: {
                if (result == null || !"200".equals(result.optString("code"))) {
                    CommonFunc.showToast(this, "更新資料失敗...");
                    reInputEmail();
                } else {
                    //補填完email之後重新登入
                    getProfile();
                    reInsertEmail = true;
                    //new CustomerApiTask(this, this, true, "", TaskEnum.MednetGetWebToken).execute();
                }
                break;
            }
            case MednetGetWebToken: {
                Log.i(TAG, "onPostExecute: MednetGetWebToken " + result);
                if (result != null && !result.isNull("source")) {
                    String token = result.optJSONObject("source").optString("token");
                    String webLoginUrl = String.format("https://med-net.com/CMSContent?user_id=%s&token=%s",
                            MegaApplication.getInstance().getMember().getCustomerId(), token);
                    WebSettings webSettings = webView.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    Log.i("debug", "onPostExecute: " + webLoginUrl);
                    webView.setWebViewClient(new MednetWebViewClient());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl(webLoginUrl);
                        }
                    }, 0);
                }
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
//                        startActivity(intent);
//                        finish();
//                    }
//                }, 5000);
//                break;
            }
        }
    }

    /**
     * 補填email
     * 不填寫無法繼續使用
     */
    private void reInputEmail() {
        final EditText editText = (EditText) LayoutInflater.from(this).inflate(R.layout.dialog_insert_email, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.dialog_soft_input).setView(editText);
        builder.setCancelable(false);
        builder.setTitle("系統偵測Email資料異常，請補填寫您的Email")
                .setNegativeButton("登出", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //登出
                        triggerAction(MegaApplication.ActionTypeLogout, "");
                    }
                })
                .setPositiveButton("送出", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String email = editText.getText().toString();
                        if (CommonFunc.isEmailValid(email)) {
                            showLoading("更新資料中...");
                            new CommonApiTask(MainActivity.this, MainActivity.this, false, "更新資料中...", TaskEnum.FixEmail)
                                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, email);
                        } else {
                            CommonFunc.showToast(MainActivity.this, "請輸入正確email格式");
                            reInputEmail();
                        }
                    }
                });
        builder.show();
    }

    /**
     * 建置top bar 畫面
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_top_menu, menu);
        notice = menu.findItem(R.id.notice);
        mNoticeProvider = (BadgeActionProvider) MenuItemCompat.getActionProvider(notice);
        mNoticeProvider.setOnClickListener(0, new BadgeActionProvider.OnClickListener() {
            @Override
            public void onClick(int what) {
                triggerAction(MegaApplication.ActionTypeWebView, "https://med-net.com/BellCenter");
                canBack();
                new CommonApiTask(MainActivity.this, MainActivity.this, false, "", TaskEnum.MednetAppLogEven)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "top_button", "https://med-net.com/BellCenter");
            }
        });

        shoppingCar = menu.findItem(R.id.shopping_car);
        mShoppingCarProvider = (BadgeActionProvider) MenuItemCompat.getActionProvider(shoppingCar);
        mShoppingCarProvider.setOnClickListener(0, new BadgeActionProvider.OnClickListener() {
            @Override
            public void onClick(int what) {
                triggerAction(MegaApplication.ActionTypeWebView, "https://med-net.com/Cart#cart1");
                new CommonApiTask(MainActivity.this, MainActivity.this, false, "", TaskEnum.MednetAppLogEven)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "top_button", "https://med-net.com/Cart#cart1");
                canBack();
            }
        });

        return true;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (mNoticeProvider != null)
            mNoticeProvider.setIcon(R.drawable.ic_main_notice);
        if (mShoppingCarProvider != null)
            mShoppingCarProvider.setIcon(R.drawable.ic_main_shopping);
    }


    /**
     * 建置top bar 點擊事件
     */
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.notice:
//                triggerAction(MegaApplication.ActionTypeWebView, "https://med-net.com/BellCenter");
//                canBack();
//                //triggerAction(MegaApplication.ActionTypeWebView, "https://stage.med-net.com/CMSContent/Content/544");
//                new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
//                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "top_button", "https://med-net.com/BellCenter");
//
//                return true;
//            case R.id.shopping_car:
//                //Log.d("debug", "shoppingCar");
//                triggerAction(MegaApplication.ActionTypeWebView, "https://med-net.com/Cart#cart1");
//                new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
//                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "top_button", "https://med-net.com/Cart#cart1");
//                canBack();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    /**
     * 監控地的選項點擊觸發
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();
        Intent intent;
        JSONObject webObj = new JSONObject();
        showTopBar();
        switch (id) {
            case R.id.mainPage:
                //Log.d("debug", "mainPage" + " page = " + page);
                switch (page) {
                    case "home":

                        setHomeFragment();

                        if (backBtn) {
                            backBtn = false;
                        } else {
                            //Log.i(TAG, "mainPage: home");
                            new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", "home_page");
                        }

                        break;
                    case "healthList":
                        setHealthListFragment();
                        page = "home";
                        break;
                    case "healthDetail":
                        break;
                }
                //setHomeFragment();
                break;
            case R.id.item2:
                try {
                    canBack();
                    String url = "https://med-net.com/PhysicalExamination";
                    webObj.put("web", "item2");
                    triggerAction(MegaApplication.ActionTypeWebView, url);
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.item3:
                try {
                    canBack();
                    //String url = "https://expert.med-net.com/login/GetOAuthLogInfo?data=%7B%22id%22:%2221358%22,%22email%22:%22denma0513@gmail.com%22,%22name%22:%22%E9%BB%83%E6%9F%8F%E8%AB%BA%22,%22first_name%22:%22%E6%9F%8F%E8%AB%BA%22,%22last_name%22:%22%E9%BB%83%22,%22id_number%22:%22string%22,%22cellphone%22:%220921303200%22,%22gender%22:0,%22birthday%22:%22null%22,%22view_count%22:0,%22self_introduction%22:%22string%22,%22active%22:1,%22create_by%22:%22null%22,%22create_from%22:%22null%22,%22profile_image%22:%22null%22,%22cover_image%22:%22null%22%7D";
                    String url = "https://med-net.com/Member";
                    webObj.put("web", "item3");
                    triggerAction(MegaApplication.ActionTypeWebView, url);
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.item4:
                try {
                    canBack();
                    String url = "https://med-net.com/MemberFavorite";
                    webObj.put("web", "item4");
                    triggerAction(MegaApplication.ActionTypeWebView, url);
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.orgApp:
                //開啟機構端檢中功能

                try {
                    webObj.put("web", "item5");
                    mWebViewList.add(webObj);
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", "test_page");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //判斷系統是否已經登入
                if (MegaApplication.ORG_ISOPEN) {
                    intent = new Intent(this, OrgMainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    this.startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));
                } else {
                    intent = new Intent(this, TutorialStartAppActivity.class);
                    this.startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));
                }

//                if (!"".equals(MegaApplication.getInstance().getMember().getAccessToken())) {
//
//                } else {
//                    Bundle bundle = new Bundle();
//                    Intent integer = new Intent(this, UserLoginActivity.class);
//                    bundle.putString("action", MegaApplication.ActionTypeMainPage);
//                    bundle.putString("target", "");
//                    integer.putExtras(bundle);
//                    startActivityForResult(integer, Integer.parseInt(MegaApplication.ActionTypeLogin));
//                }

                break;
            case R.id.my_ask:
                try {
                    canBack();
                    String url = String.format("https://expert.med-net.com/doctorsdetail/%s/1", MegaApplication.getInstance().getMember().getDrID());
                    webObj.put("web", "my_ask");
                    triggerAction(MegaApplication.ActionTypeWebView, url);
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.my_data:
                try {
                    canBack();
                    String url = String.format("https://expert.med-net.com/doctorsdetail/%s/0", MegaApplication.getInstance().getMember().getDrID());
                    webObj.put("web", "my_data");
                    triggerAction(MegaApplication.ActionTypeWebView, url);
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.my_donate:
                try {
                    canBack();
                    String url = String.format("https://expert.med-net.com/doctorsdetail/%s/2", MegaApplication.getInstance().getMember().getDrID());
                    webObj.put("web", "my_donate");
                    triggerAction(MegaApplication.ActionTypeWebView, url);
                    Log.i(TAG, "onNavigationItemSelected: url = " + url);
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.message:
                try {
                    canBack();
                    String url = String.format("https://expert.med-net.com/doctorsdetail/%s/5", MegaApplication.getInstance().getMember().getDrID());
                    webObj.put("web", "message");
                    triggerAction(MegaApplication.ActionTypeWebView, url);
                    new CommonApiTask(this, this, false, "", TaskEnum.MednetAppLogEven)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "tabbar_button", url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    /**
     * 首頁點擊事件監控
     */
    @Override
    public void triggerAction(String action, String target) {
        if ("1".equals(action)) {
            canBack();
        }

        //增加AIDoctor專用判斷
        if (target.contains("aidoctor.med-net.com/AIDoctor")) {
            hideTopBar();
        }

        Log.i(TAG, "triggerAction: " + action + " " + target);
        triggerAction(this, action, target, R.id.main_content);
        drawerLayout.closeDrawers();
    }

    /**
     * 管理首頁中開啟的fragmaent
     * selectedFragment 為目前顯示的視窗層
     * 當觸發onBackPressed時會依照個畫面有不同動作
     * ＥＸ 當name = HOME 時，表示當時在首頁畫面
     * 則直接關閉ＡＰＰ
     */
    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {
        this.selectedFragment = selectedFrangment;
    }

    //回接activity的結果
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        //Log.i("debug", "requestCode = " + requestCode + " resultCode = " + resultCode + "data = " + data);
        if (requestCode == Integer.parseInt(MegaApplication.ActionTypeLogin)) {
            try {
                String action = "";
                String target = "";
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    action = bundle.getString("action", "");
                    target = bundle.getString("target", "");

                }
                cannotBack();
                //完成登入
                if (resultCode == Activity.RESULT_OK) {
                    getProfile();
                    //new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetProfile).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    //執行登入前執行的動作
                    if (!"".equals(action)) {

                        //增加AIDoctor專用判斷
                        if (target.contains("AIDoctor")) {
                            target = String.format(MegaApplication.AIDOCTOR_URL, MegaApplication.getInstance().getMember().getCustomerId());
                            Log.i(TAG, "onActivityResult: target = "+target);
                            hideTopBar();
                        }
                        triggerAction(action, target);
                        return;
                    }

                    switch (page) {
                        case "home":
                            setHomeFragment();
                            break;
                        case "healthList":
                            setHealthListFragment();
                            break;
                    }
                } else {
                    exit();
                }
            } catch (Exception e) {
                Log.w("debug", "signInResult:failed code=" + e.getMessage());
                exit();
                e.printStackTrace();
            }

        } else if (requestCode == Integer.parseInt(MegaApplication.ActionTypeHealthPush)) {
            homePageFragment.refreshHealth();
        } else if (requestCode == DETAIL) {
            healthListFragment.refreshData();
        } else if (requestCode == Integer.parseInt(MegaApplication.ActionTypeHealthPushCreate)) {
            homePageFragment.refreshHealth();
        } else if (requestCode == Integer.parseInt(MegaApplication.ActionTypeHealthPushItemSort)) {
            homePageFragment.refreshHealth();
        }

    }

    @Override
    public void onBackPressed() {

        backBtn = true;
        if (selectedFragment == null || !selectedFragment.onBackPressed()) {
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                FragmentManager.BackStackEntry fragment = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1);
                if ("HOME".equals(fragment.getName())) {
                    MegaApplication.IsFitFirstTime = true;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        this.finishAffinity();
                    } else {
                        this.finish();
                        System.exit(0);
                    }
                } else if ("HEALTH".equals(fragment.getName())) {
                    Intent intent = new Intent();
                    this.setResult(RESULT_OK, intent);
                    this.finish();
                } else if (fragment.getName().contains("aidoctor.med-net.com/AIDoctor")) {
                    return ;
                }
                getFragmentManager().popBackStack();

                try {
                    //判斷前一頁是不是首頁，如果是重新loading
                    fragment = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 2);
                    Log.d("debug", "onBackPressed fragment.getName() = " + fragment.getName());
                    CommonFunc.getHelloWord();
                    if ("HOME".equals(fragment.getName())) {
                        page = "home";
                        mBottomNavigation.setSelectedItemId(R.id.mainPage);
                        mBottomNavigationDr.setSelectedItemId(R.id.mainPage);
                        homePageFragment.refreshData();
                        homePageFragment.refreshHealth();
                        cannotBack();
                    } else if (fragment.getName().contains("aidoctor.med-net.com/AIDoctor")) {
                        hideTopBar();
                        return;
                    } else if ("https://med-net.com/PhysicalExamination".equals(fragment.getName())) {
                        mBottomNavigation.setSelectedItemId(R.id.mainPage);
                    } else if ("https://med-net.com/Member".equals(fragment.getName())) {
                        mBottomNavigation.setSelectedItemId(R.id.mainPage);
                    } else if ("https://med-net.com/MemberFavorite".equals(fragment.getName())) {
                        mBottomNavigation.setSelectedItemId(R.id.mainPage);
                    }
                    showTopBar();
                } catch (Exception e) {
                    Log.e("debug", "err = " + e.getStackTrace());
                }

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        drawerLayout.closeDrawers();
        //Log.i("debug", "mainActivity, on Start");
        String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
        String email = MegaApplication.getInstance().getMember().getEmail();
        //Log.i("debug", "accessToken = " + accessToken + ", \nMegaApplication.getInstance().getMember().isNullEmail() " + MegaApplication.getInstance().getMember().isNullEmail());
        //有token 但是email是空的 表示有登入 但是資料尚未同步回ＡＰＰ
//        if (!MegaApplication.getInstance().getMember().isNullAccessToken()) {
//            //Log.i("debug", "ＡＰＩ");
//            new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetProfile).execute();
//        }
    }

    private class MednetWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //Log.i("debug", "shouldOverrideUrlLoading = " + url);
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            //Log.i("debug", "shouldOverrideUrlLoading = " + request.getUrl().toString());
            view.loadUrl(request.getUrl().toString());
            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            hideLoading();
            String cookies = CookieManager.getInstance().getCookie(url);
            CommonFunc.syncCookie(url, cookies);
            Log.d("debug", "onPageFinished = " + url);
            if (url.contains("med-net.com/CMSContent")) {
                Log.d("debug", "主站登入  cookies = " + cookies);
            } else if (url.contains("https://expert.med-net.com")) {
                //CommonFunc.syncCookie(url,cookies);
                Log.d("debug", "醫師諮詢登入  cookies = " + cookies);
                hideLoading();
                Message message = Message.obtain();
                message.what = 1;
                sHandler.sendMessage(message);
            }

            view.setVisibility(View.VISIBLE);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
        }


        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("SSL 憑證錯誤");
            builder.setMessage("無法驗證伺服器SSL憑證。\n仍要繼續嗎?");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

}
