package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jye on 2017/7/16.
 * class: ExamineItem
 */
public class ExamineItem {

    //編號
    private UUID id;
    //代碼
    private String code;
    //名稱
    private String name;
    //描述
    private String description;
    //檢查秒數
    private int spendSeconds;
    //時間規則秒數
    private int timeRuleSeconds;
    //時間規則前後
    private boolean timeRuleBefore;
    //回診秒數
    private int doubleRuleSeconds;
    //優先權規則
    private int priorityRule;

    /**
     * 轉換函數
     * @param json Json
     * @param status Status
     * @param culture Culture
     * @return Map
     */
    public static ExamineItemSet parse(String json, int status, CultureEnum culture) {
        ExamineItemSet result = new ExamineItemSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String nameField, descriptionField;
            switch (culture) {
                case English:
                    nameField = "NameEn";
                    descriptionField = "DescriptionEn";
                    break;
                case TraditionalChinese:
                    nameField = "Name";
                    descriptionField = "Description";
                    break;
                default:
                    nameField = "Name";
                    descriptionField = "Description";
                    break;
            }

            for (int i = 0; i < len; i++) {
                ExamineItem ei = new ExamineItem();
                JSONObject obj = jDataAry.getJSONObject(i);
                String code = obj.getString("Code");
                ei.setId(UUID.fromString(obj.getString("ID")));
                ei.setCode(code);
                ei.setName(obj.getString(nameField));
                ei.setSpendSeconds(obj.getInt("SpendSeconds"));
                ei.setTimeRuleSeconds(obj.getInt("TimeRuleSeconds"));
                ei.setTimeRuleBefore(obj.getBoolean("TimeRuleBefore"));
                ei.setDoubleRuleSeconds(obj.getInt("DoubleRuleSeconds"));
                ei.setPriorityRule(obj.getInt("PriorityRule"));
                ei.setDescription(obj.getString(descriptionField));
                result.put(code, ei);
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getSpendSeconds() {
        return spendSeconds;
    }

    public int getTimeRuleSeconds() {
        return timeRuleSeconds;
    }

    public boolean isTimeRuleBefore() {
        return timeRuleBefore;
    }

    public int getDoubleRuleSeconds() {
        return doubleRuleSeconds;
    }

    public int getPriorityRule() {
        return priorityRule;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    private void setCode(String code) {
        this.code = code;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setSpendSeconds(int spendSeconds) {
        this.spendSeconds = spendSeconds;
    }

    private void setTimeRuleSeconds(int timeRuleSeconds) {
        this.timeRuleSeconds = timeRuleSeconds;
    }

    private void setTimeRuleBefore(boolean timeRuleBefore) {
        this.timeRuleBefore = timeRuleBefore;
    }

    private void setDoubleRuleSeconds(int doubleRuleSeconds) { this.doubleRuleSeconds = doubleRuleSeconds; }

    private void setPriorityRule(int priorityRule) {
        this.priorityRule = priorityRule;
    }

}