package com.mgear.mednetapp.entity.organization;

import android.graphics.Point;
import android.util.SparseArray;

import com.mgear.mednetapp.algorithms.dijkstra.Edge;
import com.mgear.mednetapp.algorithms.dijkstra.Vertex;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Jye on 2017/6/19.
 * class: BeaconInfo
 */
public class BeaconInfo {

    public static final int INCREMENTAL = 10000;

    //編號
    private UUID id;
    //主要編號
    private int major;
    //次要編號
    private int minor;
    //座標位置
    private int posLeft;
    //座標位置
    private int posTop;
    //訊號量標準
    private int rssi;
    //訊號增量
    private int increase;
    //是否在診間
    private boolean isRoom;
    //移動停留點
    private UUID moveStayId;
    //鄰居清單
    private SparseArray<BeaconNeighbor> neighborList;
    //暫存清單
    private ArrayList<BeaconNeighbor> tmpList;

    /**
     * 轉換函數
     * @param json JsonString
     * @param status Status
     * @return map list
     */
    public static BeaconInfoSet parse(String json, int status) {
        BeaconInfoSet result = new BeaconInfoSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //初始化UUID與索引的轉換清單
            HashMap<UUID, Integer> mapConvert = new HashMap<>();
            for (int i = 0; i < len; i++) {
                JSONObject obj = jDataAry.getJSONObject(i);
                //判斷是否為實體裝置
                if (!obj.getBoolean("IsInstance")) continue;

                //裝置資訊
                UUID id = UUID.fromString(obj.getString("ID"));
                int major = obj.getInt("Major");
                int minor = obj.getInt("Minor");
                int key = major * INCREMENTAL + minor;
                //加入對應清單
                mapConvert.put(id, key);
                //加入頂點
                result.addVertex(new Vertex(key, String.valueOf(key)));

                //初始化物件
                BeaconInfo bi = new BeaconInfo();
                bi.setId(id);
                bi.setMajor(major);
                bi.setMinor(minor);
                bi.setPosLeft(obj.getInt("PosLeft"));
                bi.setPosTop(obj.getInt("PosTop"));
                bi.setRssi(obj.getInt("RSSI"));
                bi.setIncrease(obj.getInt("Increase"));
                bi.setRoom(obj.getBoolean("IsRoom"));
                String moveStay = obj.getString("MoveStayID");
                if (!moveStay.equals("null"))
                    bi.setMoveStayId(UUID.fromString(moveStay));

                //鄰居清單 NeighborList
                bi.tmpList = new ArrayList<>();
                JSONArray jNeighborAry = obj.getJSONArray("NeighborList");
                for (int j = 0; j < jNeighborAry.length(); j++) {
                    JSONObject objNeighbor = jNeighborAry.getJSONObject(j);
                    BeaconNeighbor bn = new BeaconNeighbor();
                    bn.setBeaconID(UUID.fromString(objNeighbor.getString("BeaconID")));
                    bn.setNeighborID(UUID.fromString(objNeighbor.getString("NeighborID")));
                    bn.setDistanceCm(objNeighbor.getInt("DistanceCm"));
                    bn.setMoveRatio(objNeighbor.getInt("MoveRatio"));
                    bn.setRssi(objNeighbor.getInt("Reference"));

                    //中繼清單
                    JSONArray jRelayAry = objNeighbor.getJSONArray("Relays");
                    for (int k = 0; k < jRelayAry.length(); k++) {
                        JSONObject objRelay = jRelayAry.getJSONObject(k);
                        Point pnt = new Point();
                        pnt.set(objRelay.getInt("PosLeft"), objRelay.getInt("PosTop"));
                        bn.addRelay(pnt);
                    }
                    bi.tmpList.add(bn);
                }
                result.put(key, bi);
            }
            //設定對應清單
            result.setKeyMap(mapConvert);

            //轉換鄰居的索引
            for (int i = 0; i < result.size(); i++) {
                //get the object by the key.
                int keyMajor = result.keyAt(i);
                BeaconInfo bi = result.get(keyMajor);
                Vertex vertexMajor = result.getVertex(keyMajor);

                //將陣列清單轉換成字典清單
                //noinspection Convert2streamapi
                for (BeaconNeighbor bn : bi.tmpList) {
                    if (mapConvert.containsKey(bn.getNeighborID())) {
                        int keyMinor = mapConvert.get(bn.getNeighborID());
                        bn.setNeighborKey(keyMinor);
                        bi.addNeighbor(keyMinor, bn);
                        //加入圖形移動的邊
                        result.addEdge(new Edge(String.format(Locale.getDefault(), "%s_%s", keyMajor, keyMinor),
                                vertexMajor, result.getVertex(keyMinor), bn.getDistanceCm()));
                    }
                }
                bi.tmpList = null;
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    /**
     * 建構函數
     */
    private BeaconInfo() { neighborList = new SparseArray<>(); }

    public UUID getId() {
        return id;
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

    public int getKey() { return major * INCREMENTAL + minor; }

    public int getPosLeft() {
        return posLeft;
    }

    public int getPosTop() {
        return posTop;
    }

    public int getRssi() { return rssi; }

    public int getIncrease() {
        return increase;
    }

    public boolean isRoom() { return isRoom; }

    public UUID getMoveStayId() { return moveStayId; }

    public SparseArray<BeaconNeighbor> getNeighborList() {
        return neighborList;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    private void setMajor(int major) {
        this.major = major;
    }

    private void setMinor(int minor) {
        this.minor = minor;
    }

    private void setPosLeft(int posLeft) {
        this.posLeft = posLeft;
    }

    private void setPosTop(int posTop) {
        this.posTop = posTop;
    }

    private void setRssi(int rssi) { this.rssi = rssi; }

    private void setIncrease(int increase) {
        this.increase = increase;
    }

    private void setRoom(boolean room) { isRoom = room; }

    private void setMoveStayId(UUID moveStayId) { this.moveStayId = moveStayId; }

    private void addNeighbor(int key, BeaconNeighbor neighbor) { this.neighborList.put(key, neighbor); }

}