package com.mgear.mednetapp.entity.health;

import android.content.Context;

import com.mgear.mednetapp.R;

import org.json.JSONObject;

/**
 * 血氧
 */
public class HealthBloodOxygenSet extends HealthBaseSet {
    private int bs;
    private Context mContext;

    public HealthBloodOxygenSet(Context context, JSONObject data) {
        mContext = context;
        name = data.optString("healthTypeName");
        healthType = data.optString("healthType");
        bs = data.optInt("bs");
        id = data.optInt("id");
        measureID = data.optString("measure_id");
        customerID = data.optInt("customer_id");
        customerAppID = data.optString("customer_app_id");
        createdAt = data.optString("created_at");
        updatedAt = data.optString("updated_at");
        mTime = data.optString("m_time");
        type = data.optInt("type");

        unit = "%";
        healthImg = data.optInt("healthImg");
        healthIcon = data.optInt("healthIcon");
        statusArr = new String[]{"不足", "標準"};
        rangeArr = new double[]{94, 130};
        colorArr = new int[]{R.color.status_danger, R.color.status_normal};
        max = rangeArr[rangeArr.length - 1];
        computeValue();

    }

    //判斷血氧
    private void computeValue() {
        if (bs >= 90 && bs <= 94) {
            status = 0;
        } else if (bs >= 95 ) {
            status = 1;
        }

        statusTitle = "●  " + statusArr[status];
        color = mContext.getResources().getColor(colorArr[status]);
        colorID = colorArr[status];

        value = bs + "";
        value2 = "";
        //unit2 = "血氧量 ("+unit+")";
    }

    public int getBs() {
        return bs;
    }

    public void setBs(int bs) {
        this.bs = bs;
    }
}
