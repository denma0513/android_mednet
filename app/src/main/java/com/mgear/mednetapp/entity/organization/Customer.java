package com.mgear.mednetapp.entity.organization;

import android.util.SparseArray;

import com.mgear.mednetapp.enums.CultureEnum;
import com.mgear.mednetapp.enums.StatusEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/5.
 * class: Customer
 */
public class Customer {

    //編號
    private UUID id;
    private String contactCode;
    private String contactEmsCode;
    private String name;
    private String identityID;
    private char gender;
    private Date birthday;
    private String passportID;
    private String email;
    private String email2;
    private String phoneArea;
    private String phoneNumber;
    private String phoneExt;
    private String mobile;
    private String questionLink;
    private String level;
    private String extNotes;
    private String address;
    private String lockerID;
    private String lockerType;
    private String productName;
    private String reportCode;
    private int status;
    private CustomerReportSet reportList;
    private CustomerReportExamineSet examineList;

    /**
     * 轉換函數
     * @param json Json
     * @return Gifts
     */
    public static String parseGifts(String json) {
        StringBuilder result = new StringBuilder();
        if (json.equals("")) return result.toString();
        try {
            JSONArray ary = new JSONObject(json).getJSONArray("GiftList");
            if (ary == null) return "";
            int len = ary.length();
            if (len == 0) return "";
            for (int i = 0; i < len; i++) {
                JSONObject obj = ary.getJSONObject(i);
                result.append(obj.getString("GiftName")).append(",");
            }
            //清除最後的分隔符號
            result.deleteCharAt(result.length() - 1);
        }
        catch (Exception ignored) { }
        return result.toString();
    }

    /**
     * 轉換函數
     * @param json JsonString
     * @param culture Culture
     * @return Customer
     */
    public static Customer parse(String json, CultureEnum culture) {
        Customer result = new Customer();
        if (json.equals("")) return result;
        //判斷語系
        String examineField;
        switch (culture) {
            case English:
                examineField = "ExamNameEn";
                break;
            case TraditionalChinese:
                examineField = "ExamName";
                break;
            default:
                examineField = "ExamName";
                break;
        }

        try {
            JSONObject obj = new JSONObject(json);
            result.setId(UUID.fromString(obj.getString("ID")));
            result.setContactCode(obj.optString("ContactCode"));
            result.setContactEmsCode(obj.getString("ContactEmsCode"));
            result.setName(obj.getString("Name"));
            result.setIdentityID(obj.getString("IdentityID"));
            result.setGender(obj.getString("Gender").charAt(0));
            result.setPassportID(obj.getString("PassportID"));
            result.setEmail(obj.getString("Email"));
            result.setEmail2(obj.getString("Email2"));
            result.setPhoneArea(obj.getString("PhoneArea"));
            result.setPhoneNumber(obj.getString("PhoneNumber"));
            result.setPhoneExt(obj.getString("PhoneExt"));
            result.setMobile(obj.getString("Mobile"));
            result.setQuestionLink(obj.getString("QuestionLink"));
            result.setLevel(obj.getString("Level"));
            result.setExtNotes(obj.getString("ExtNotes"));
            result.setAddress(obj.getString("Address"));
            result.setLockerID(obj.getString("LockerID"));
            result.setLockerType(obj.getString("LockerType"));
            result.setProductName(obj.getString("ProductName"));
            result.setReportCode(obj.getString("ReportCode"));
            result.setStatus(obj.getInt("Status"));
            //取得日期格式
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            result.setBirthday(sdf.parse(obj.getString("Birthday")));

            //判斷身分證是否為空，若為空則為護照號碼
            if (result.getIdentityID().equals(""))
                result.setIdentityID(result.getPassportID());

            //取得報告清單 ReportList
            JSONArray jReportAry = obj.getJSONArray("ReportList");
            for (int i = 0; i < jReportAry.length(); i++) {
                JSONObject objReport = jReportAry.getJSONObject(i);
                long documentCode = objReport.getLong("DocumentCode");
                //判斷是否有報告
                if (documentCode == 0)
                    continue;
                CustomerReport report = new CustomerReport();
                report.setId(UUID.fromString(objReport.getString("ID")));
                report.setCustomerId(UUID.fromString(objReport.getString("CustomerID")));
                report.setContactCode(objReport.getInt("ContactCode"));
                report.setReportCode(objReport.getString("ReportCode"));
                report.setDocumentCode(documentCode);
                report.setProductName(objReport.getString("ProductName"));
                report.setEventDate(sdf.parse(objReport.getString("EventDate")));
                result.addReport(report);
            }

            //取得檢查項目清單 ExamineList
            int statusCode;
            boolean showSupplementIcon = false;
            JSONArray jExamineAry = obj.getJSONArray("ExamineList");
            for (int j = 0; j < jExamineAry.length(); j++) {
                JSONObject objExamine = jExamineAry.getJSONObject(j);
                CustomerReportExamine examine = new CustomerReportExamine();
                examine.setId(UUID.fromString(objExamine.getString("ID")));
                examine.setReportId(UUID.fromString(objExamine.getString("ReportID")));
                examine.setExamineId(UUID.fromString(objExamine.getString("ExamID")));
                examine.setExamineCode(objExamine.getString("ExamCode"));
                examine.setRoomId(UUID.fromString(objExamine.getString("RoomID")));
                examine.setExamineName(objExamine.getString(examineField));
                statusCode = objExamine.getInt("Status");
                examine.setStatus(statusCode);

                //判斷是否顯示補做的圖示
                if (!showSupplementIcon && statusCode == StatusEnum.Supplement.getCode())
                    showSupplementIcon = true;

                //取得時間格式
                String dateStr = objExamine.getString("CheckInTime");
                if (!dateStr.equals("null"))
                    examine.setCheckin(sdf.parse(dateStr));
                dateStr = objExamine.getString("CheckOutTime");
                if (!dateStr.equals("null"))
                    examine.setCheckout(sdf.parse(dateStr));
                result.addExamine(examine);
            }
            result.examineList.setSupplementIcon(showSupplementIcon);
        }
        catch (Exception ignored) { ignored.printStackTrace(); }
        return result;
    }

    /**
     * 建構函數
     */
    public Customer() {
        reportList = new CustomerReportSet();
        examineList = new CustomerReportExamineSet(0);
    }

    public UUID getId() {
        return id;
    }

    public String getContactCode() {
        return contactCode;
    }

    public String getContactEmsCode() { return contactEmsCode; }

    public String getName() {
        if (name != null && name.indexOf("_") >= 0) {
            name = name.split("_")[0];
        }
        return name;
    }

    public String getIdentityID() { return identityID; }

    public char getGender() {
        return gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getBirthdayString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return formatter.format(birthday);
    }

    public int getAge() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(birthday);
        return Calendar.getInstance().get(Calendar.YEAR) -  cal.get(Calendar.YEAR);
    }

    public String getPassportID() {
        return passportID;
    }

    public String getEmail() {
        return email;
    }

    public String getEmail2() {
        return email2;
    }

    public String getPhoneArea() {
        return phoneArea;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhoneExt() {
        return phoneExt;
    }

    public String getMobile() {
        return mobile;
    }

    public String getQuestionLink() {
        return questionLink;
    }

    public String getLevel() {
        return level;
    }

    public String getExtNotes() {
        return extNotes;
    }

    public String getAddress() { return address; }

    public String getLockerID() { return lockerID; }

    public String getLockerType() { return lockerType; }

    public String getProductName() {
        return productName;
    }

    public String getReportCode() {
        return reportCode;
    }

    public int getStatus() { return status; }

    public CustomerReportSet getReportList() { return reportList; }

    public CustomerReportExamineSet getExamineList() { return examineList; }

    public String getExamineCodes() {
        StringBuilder sb = new StringBuilder();
        for (CustomerReportExamine examine : examineList)
            sb.append(examine.getExamineCode()).append(',');
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
            return sb.toString();
        }
        else return "";
    }

    public HashMap<String, Integer> getExamineCodeStatusList() {
        HashMap<String, Integer> result = new HashMap<>();
        for (CustomerReportExamine examine : examineList)
            result.put(examine.getExamineCode(), examine.getStatus());
        return result;
    }

    public ArrayList<String> getExamineNameList() {
        ArrayList<String> result = new ArrayList<>();
        //noinspection Convert2streamapi
        for (CustomerReportExamine examine : examineList)
            result.add(examine.getExamineName());
        return result;
    }

    public HashMap<String, List<StringStringSet>> getExamineListByStatus(SparseArray<String> map) {
        HashMap<String, List<StringStringSet>> result = new HashMap<>();
        for (CustomerReportExamine examine : examineList) {
            String status = map.get(examine.getStatus());
            List<StringStringSet> items = result.get(status);
            if (items == null) {
                items = new ArrayList<>();
                items.add(new StringStringSet(examine.getExamineName(), examine.getExamineCode()));
                result.put(status, items);
            }
            else items.add(new StringStringSet(examine.getExamineName(), examine.getExamineCode()));
        }
        return result;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setContactCode(String contactCode) {
        this.contactCode = contactCode;
    }

    private void setContactEmsCode(String contactEmsCode) { this.contactEmsCode = contactEmsCode; }

    public void setName(String name) {
        this.name = name;
    }

    private void setIdentityID(String identityID) {
        this.identityID = identityID;
    }

    private void setGender(char gender) {
        this.gender = gender;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    private void setPassportID(String passportID) {
        this.passportID = passportID;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private void setEmail2(String email2) {
        this.email2 = email2;
    }

    private void setPhoneArea(String phoneArea) {
        this.phoneArea = phoneArea;
    }

    private void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private void setQuestionLink(String questionLink) {
        this.questionLink = questionLink;
    }

    private void setLevel(String level) {
        this.level = level;
    }

    private void setExtNotes(String extNotes) {
        this.extNotes = extNotes;
    }

    private void setAddress(String address) { this.address = address; }

    private void setLockerID(String lockerID) { this.lockerID = lockerID; }

    private void setLockerType(String lockerTyper) { this.lockerType = lockerTyper; }

    private void setProductName(String productName) {
        this.productName = productName;
    }

    private void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public void setStatus(int status) { this.status = status; }

    private void addReport(CustomerReport report) { this.reportList.add(report); }

    private void addExamine(CustomerReportExamine examine) { this.examineList.add(examine); }

    public void setExamine(CustomerReportExamineSet examine) { this.examineList = examine; }

}