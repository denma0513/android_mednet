package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Jye on 2017/8/20.
 * class: BeaconPushSet
 */
public class BeaconPushSet extends HashMap<UUID, List<String>> {

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public BeaconPushSet(int status) { this.status = status; }

    /**
     * 轉換函數
     * @param json Json
     * @param status Status
     * @param culture Culture
     * @return List
     */
    public static BeaconPushSet parse(String json, int status, CultureEnum culture) {
        BeaconPushSet result = new BeaconPushSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String contentField;
            switch (culture) {
                case English:
                    contentField = "ContentsEn";
                    break;
                case TraditionalChinese:
                    contentField = "Contents";
                    break;
                default:
                    contentField = "Contents";
                    break;
            }

            for (int i = 0; i < len; i++) {
                JSONObject obj = jDataAry.getJSONObject(i);
                UUID key = UUID.fromString(obj.getString("BeaconID"));
                List<String> values = result.get(key);
                if (values == null) {
                    values = new ArrayList<>();
                    values.add(obj.getString(contentField));
                    result.put(key, values);
                }
                else values.add(obj.getString(contentField));
            }
        }
        catch (Exception ignored) { }
        return result;
    }

}