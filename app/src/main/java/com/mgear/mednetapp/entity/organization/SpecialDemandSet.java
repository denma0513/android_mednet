package com.mgear.mednetapp.entity.organization;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Jye on 2017/8/18.
 * class: SpecialDemandSet
 */
public class SpecialDemandSet extends ArrayList<SpecialDemand> {

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public SpecialDemandSet(int status) { this.status = status; }

    /**
     * 根據性別取得特殊需求的項目
     * @param gender Gender
     * @param selectedItems  Selected list
     * @param iconMap Icon map
     * @return CheckableItem list
     */
    public CheckableItem[] getByGender(char gender, ArrayList<String> selectedItems, Map<String, Integer> iconMap) {
        if (this.size() == 0)
            return new CheckableItem[0];
        ArrayList<CheckableItem> result = new ArrayList<>();
        if (this.size() > 0) {
            for (SpecialDemand item : this) {
                //判斷需求項目的顯示代碼是否為03
                if (item.getDisplayCategory().equals("03")) {
                    //只有女性才有03的需求
                    if (gender == 'F') {
                        //判斷是否已選取
                        boolean checked = selectedItems.contains(item.getCode());
                        //取得圖示編號
                        Integer img = iconMap.get(item.getFieldName());
                        result.add(new CheckableItem(item.getId(), item.getDisplayCategory(), item.getCode(), item.getName(), item.getCode(), item.getRemark(), img == null ? 0 : img, checked));
                    }
                }
                else {
                    //判斷是否已選取
                    boolean checked = selectedItems.contains(item.getCode());
                    //取得圖示編號
                    Integer img = iconMap.get(item.getFieldName());
                    result.add(new CheckableItem(item.getId(), item.getDisplayCategory(), item.getCode(), item.getName(), item.getCode(), item.getRemark(), img == null ? 0 : img, checked));
                }
            }
        }
        CheckableItem[] array = new CheckableItem[result.size()];
        return result.toArray(array);
    }

}