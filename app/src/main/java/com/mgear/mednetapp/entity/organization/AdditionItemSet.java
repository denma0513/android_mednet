package com.mgear.mednetapp.entity.organization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/20.
 * class: AdditionItemSet
 */
public class AdditionItemSet extends HashMap<UUID, AdditionItem> {

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public AdditionItemSet(int status) { this.status = status; }

    /**
     * 取得加選的摺疊清單
     * @param items Items
     * @param adds Additions
     * @param gender Gender
     * @return List
     */
    public HashMap<String, List<CheckableItem>> getExpandableList(HashMap<String, Integer> items, ArrayList<UUID> adds, char gender) {
        //檢查項目有ED013005 , 就不要顯示ED013004和ED01
        boolean hasAllEd = items.containsKey("ED013005");
        //初始化回傳項目
        HashMap<String, List<CheckableItem>> result = new HashMap<>();
        for (AdditionItem item : this.values()) {
            //判斷加選的項目性別是否符合
            if (item.isEnabled() && (item.getGender() == 'N' || item.getGender() == gender)) {
                String code = item.getCode();
                //判斷是否顯示ED子項目
                if (hasAllEd && (code.equals("ED013004") || code.equals("ED01"))) continue;
                //判斷該項目是否已在檢查清單
                if (!items.containsKey(code) && !adds.contains(item.getId())) {
                    String type = item.getTypeName();
                    CheckableItem ci = new CheckableItem(item.getId(), type, code, item.getName(),
                            String.format(Locale.getDefault(), "NT$%s", String.valueOf(item.getProductPrice())),
                            item.getDescription(), item.getProductPrice());
                    List<CheckableItem> lst = result.get(type);
                    if (lst == null) {
                        lst = new ArrayList<>();
                        lst.add(ci);
                        result.put(type, lst);
                    }
                    else lst.add(ci);
                }
            }
        }
        return result;
    }

}