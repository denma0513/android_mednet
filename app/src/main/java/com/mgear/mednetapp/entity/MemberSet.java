package com.mgear.mednetapp.entity;

public class MemberSet {
    private String name;
    private String accessToken;
    private String refreshToken;
    private String mobile;
    private String email;
    private String customerAppId;
    private String customerId;
    private String firstName;
    private String lastName;
    private String webToken;
    private String cellphone;
    private String gender; //女:1 男：0
    private String birthday;
    private String profileImage;
    private String coverImage;
    private String authType;//身份 0:一般 1:醫師
    private String drID;//醫師諮詢ID
    private String doctorArticleCount;
    private String unpromotedArticleCount;

    public MemberSet() {
        name = "";
        accessToken = "";
        refreshToken = "";
        mobile = "";
        email = "";
        customerAppId = "";
        customerId = "";
        firstName = "";
        lastName = "";
        webToken = "";
        cellphone = "";
        gender = "";
        birthday = "";
        profileImage = "";
        coverImage = "";
        authType = "";
    }


    public boolean isNullAccessToken() {
        return accessToken == null || "".equals(accessToken);
    }

    public boolean isNullRefreshToken() {
        return refreshToken == null || "".equals(refreshToken);
    }

    public boolean isNullEmail() {
        return email == null || "".equals(email);
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerAppId() {
        return customerAppId;
    }

    public void setCustomerAppId(String customerAppId) {
        this.customerAppId = customerAppId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWebToken() {
        return webToken;
    }

    public void setWebToken(String webToken) {
        this.webToken = webToken;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getDrID() {
        return drID;
    }

    public void setDrID(String drID) {
        this.drID = drID;
    }

    public String getDoctorArticleCount() {
        return doctorArticleCount;
    }

    public void setDoctorArticleCount(String doctorArticleCount) {
        this.doctorArticleCount = doctorArticleCount;
    }

    public String getUnpromotedArticleCount() {
        return unpromotedArticleCount;
    }

    public void setUnpromotedArticleCount(String unpromotedArticleCount) {
        this.unpromotedArticleCount = unpromotedArticleCount;
    }
}
