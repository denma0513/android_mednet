package com.mgear.mednetapp.entity.organization;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Jye on 2017/6/19.
 * class: BeaconNeighbor
 */
public class BeaconNeighbor {

    //編號
    private UUID beaconID;
    //鄰居編號
    private UUID neighborID;
    //鄰居索引
    private int neighborKey;
    //距離
    private int distanceCm;
    //移動比率
    private int moveRatio;
    //訊號量
    private int rssi;
    //中繼清單
    private ArrayList<Point> relayList;

    /**
     * 建構函數
     */
    BeaconNeighbor() {
        relayList = new ArrayList<>();
    }

    public UUID getBeaconID() {
        return beaconID;
    }

    public UUID getNeighborID() {
        return neighborID;
    }

    public int getNeighborKey() { return neighborKey; }

    public int getDistanceCm() {
        return distanceCm;
    }

    public int getMoveRatio() {
        return moveRatio;
    }

    public int getRssi() { return rssi; }

    public ArrayList<Point> getRelayList() {
        return relayList;
    }

    void setBeaconID(UUID beaconID) {
        this.beaconID = beaconID;
    }

    void setNeighborID(UUID neighborID) {
        this.neighborID = neighborID;
    }

    void setNeighborKey(int neighborKey) { this.neighborKey = neighborKey; }

    void setDistanceCm(int distanceCm) {
        this.distanceCm = distanceCm;
    }

    void setMoveRatio(int moveRatio) {
        this.moveRatio = moveRatio;
    }

    void setRssi(int rssi) { this.rssi = rssi; }

    void addRelay(Point relay) {
        this.relayList.add(relay);
    }

}