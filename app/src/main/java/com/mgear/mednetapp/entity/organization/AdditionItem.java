package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jye on 2017/6/19.
 * class: AdditionItem
 */
public class AdditionItem {

    //編號
    private UUID id;
    //類型編號
    private UUID typeId;
    //類型名稱
    private String typeName;
    //診間編號
    private UUID roomId;
    //加選代碼
    private String code;
    //加選名稱
    private String name;
    //描述
    private String description;
    //價格
    private int productPrice;
    //加選性別
    private char gender;
    //加選空腹狀態
    private char hungry;
    //是否啟用
    private boolean enabled;

    /**
     * 轉換函數
     * @param json JsonString
     * @param culture Culture
     * @param status Status
     * @return obj
     */
    public static AdditionItemSet parse(String json, int status, CultureEnum culture) {
        AdditionItemSet result = new AdditionItemSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String nameField, typeField, descriptionField;
            switch (culture) {
                case English:
                    nameField = "NameEn";
                    typeField = "TypeNameEn";
                    descriptionField = "DescriptionEn";
                    break;
                case TraditionalChinese:
                    nameField = "Name";
                    typeField = "TypeName";
                    descriptionField = "Description";
                    break;
                default:
                    nameField = "Name";
                    typeField = "TypeName";
                    descriptionField = "Description";
                    break;
            }

            for (int i = 0; i < len; i++) {
                AdditionItem ai = new AdditionItem();
                JSONObject obj = jDataAry.getJSONObject(i);
                UUID key = UUID.fromString(obj.getString("ID"));
                ai.setId(key);
                ai.setTypeId(UUID.fromString(obj.getString("TypeID")));
                ai.setTypeName(obj.getString(typeField));
                ai.setName(obj.getString(nameField));
                ai.setRoomId(UUID.fromString(obj.getString("RoomID")));
                ai.setCode(obj.getString("Code"));
                ai.setProductPrice(obj.getInt("ProductPrice"));
                ai.setGender(obj.getString("Gender").charAt(0));
                ai.setHungry(obj.getString("Hungry").charAt(0));
                ai.setEnabled(obj.getBoolean("Enabled"));
                ai.setDescription(obj.getString(descriptionField));
                result.put(key, ai);
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public UUID getTypeId() {
        return typeId;
    }

    public String getTypeName() { return typeName; }

    public UUID getRoomId() {
        return roomId;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() { return description; }

    public int getProductPrice() {
        return productPrice;
    }

    public char getGender() {
        return gender;
    }

    public char getHungry() {
        return hungry;
    }

    public boolean isEnabled() {
        return enabled;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setTypeId(UUID typeId) {
        this.typeId = typeId;
    }

    private void setTypeName(String typeName) { this.typeName = typeName; }

    private void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    private void setCode(String code) {
        this.code = code;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    private void setGender(char gender) {
        this.gender = gender;
    }

    private void setHungry(char hungry) {
        this.hungry = hungry;
    }

    private void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}