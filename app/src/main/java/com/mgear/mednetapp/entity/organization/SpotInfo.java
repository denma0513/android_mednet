package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jye on 2017/6/19.
 * class: SpotInfo
 */
public class SpotInfo {

    //編號
    private UUID id;
    //名稱
    private String name;
    //簡介
    private String summary;
    //影像路徑
    private String imagePath;
    //影音路徑
    private String voicePath;
    //圖標位置(左)
    private int markerLeft;
    //圖標位置(上)
    private int markerTop;
    //是否啟用
    private boolean isEnable;

    /**
     * 轉換函數
     * @param json JsonString
     * @param status Status
     * @param culture Culture
     * @return obj
     */
    public static SpotInfoSet parse(String json, int status, CultureEnum culture) {
        SpotInfoSet result = new SpotInfoSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String nameField, summaryField;
            switch (culture) {
                case English:
                    nameField = "NameEn";
                    summaryField = "SummaryEn";
                    break;
                case TraditionalChinese:
                    nameField = "Name";
                    summaryField = "Summary";
                    break;
                default:
                    nameField = "Name";
                    summaryField = "Summary";
                    break;
            }

            for (int i = 0; i < len; i++) {
                SpotInfo si = new SpotInfo();
                JSONObject obj = jDataAry.getJSONObject(i);
                UUID key = UUID.fromString(obj.getString("ID"));
                si.setId(key);
                si.setName(obj.getString(nameField));
                si.setSummary(obj.getString(summaryField));
                si.setImagePath(obj.getString("ImagePath"));
                si.setVoicePath(obj.getString("VoicePath"));
                si.setMarkerLeft(obj.getInt("MarkerLeft"));
                si.setMarkerTop(obj.getInt("MarkerTop"));
                si.setEnable(obj.getBoolean("IsEnable"));
                result.put(key, si);
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSummary() {
        return summary;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getVoicePath() {
        return voicePath;
    }

    public int getMarkerLeft() {
        return markerLeft;
    }

    public int getMarkerTop() {
        return markerTop;
    }

    public boolean isEnable() { return isEnable; }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setSummary(String summary) {
        this.summary = summary;
    }

    private void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    private void setVoicePath(String voicePath) {
        this.voicePath = voicePath;
    }

    private void setMarkerLeft(int markerLeft) {
        this.markerLeft = markerLeft;
    }

    private void setMarkerTop(int markerTop) {
        this.markerTop = markerTop;
    }

    private void setEnable(boolean enable) { isEnable = enable; }
}