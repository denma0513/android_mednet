package com.mgear.mednetapp.entity.organization;

import java.util.ArrayList;

/**
 * Created by Jye on 2017/8/23.
 * class: CustomerReportExamineSet
 */
public class CustomerReportExamineSet extends ArrayList<CustomerReportExamine> {

    //狀態
    private int status;
    //是否顯示補做的圖示
    private boolean showSupplementIcon;

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public boolean showSupplementIcon() { return showSupplementIcon; }
    public void setSupplementIcon(boolean supplementIcon) { showSupplementIcon = supplementIcon; }

    public CustomerReportExamineSet(int status) { this.status = status; }

}