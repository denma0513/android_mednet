package com.mgear.mednetapp.entity.organization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jye on 2017/8/21.
 * class: CheckupResultSet
 */
public class CheckupResultSet extends HashMap<UUID, ArrayList<CheckupResult>> {

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public CheckupResultSet(int status) { this.status = status; }

    private HashMap<UUID, Boolean> mapCheckupResult = new HashMap<>();

    void setCheckupResult(UUID key, Boolean value) {
        if (!mapCheckupResult.containsKey(key))
            mapCheckupResult.put(key, value);
    }

    public Boolean getCheckupResult(UUID key) {
        return mapCheckupResult.get(key);
    }

}