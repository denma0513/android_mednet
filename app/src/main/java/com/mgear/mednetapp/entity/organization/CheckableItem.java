package com.mgear.mednetapp.entity.organization;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/20.
 * class: CheckableItem
 */
public class CheckableItem {

    //編號
    private UUID id;
    //類型
    private String type;
    //代碼
    private String code;
    //名稱
    private String name;
    //子名稱
    private String subName;
    //備註
    private String memo;
    //數字編號
    private int number;
    //狀態
    private byte status;
    //是否勾選
    private boolean isCheck;

    /**
     * 轉換函數
     * @param json Json
     * @param additionItems Addition item set
     * @return CheckableSet
     */
    public static CheckableItemSet parse(String json, final AdditionItemSet additionItems) {
        CheckableItemSet result = new CheckableItemSet();
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            for (int i = 0; i < len; i++) {
                JSONObject obj = jDataAry.getJSONObject(i);
                //取得加選的項目
                AdditionItem item = additionItems.get(UUID.fromString(obj.getString("AdditionID")));
                if (item == null) continue;
                result.put(item.getId(), new CheckableItem(item.getId(), item.getTypeName(), item.getCode(), item.getName(),
                        String.format(Locale.getDefault(), "NT$%s", String.valueOf(item.getProductPrice())),
                        item.getDescription(), item.getProductPrice(), (byte) obj.getInt("ResultCode")));
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    public CheckableItem(String name) {
        this.id = UUID.fromString("00000000-0000-0000-0000-000000000000");
        this.type = "";
        this.code = "";
        this.name = name;
        this.subName = "";
        this.memo = "";
        this.number = 0;
        this.status = 0;
        this.isCheck = false;
    }

    CheckableItem(UUID id, String type, String code, String name, String sub, String memo, int number) {
        this.id = id;
        this.type = type;
        this.code = code;
        this.name = name;
        this.subName = sub;
        this.memo = memo;
        this.number = number;
        this.status = 0;
        this.isCheck = false;
    }

    CheckableItem(UUID id, String type, String code, String name, String sub, String memo, int number, boolean checked) {
        this(id, type, code, name, sub, memo, number);
        this.isCheck = checked;
    }

    private CheckableItem(UUID id, String type, String code, String name, String sub, String memo, int number, byte status) {
        this(id, type, code, name, sub, memo, number);
        this.status = status;
    }

    public void setStatus(byte status) { this.status = status; }

    public void setCheck(boolean check) { isCheck = check; }

    public UUID getId() { return id; }

    public String getType() { return type; }

    public String getCode() { return code; }

    public String getName() { return name; }

    public String getSubName() {
        return subName;
    }

    public String getMemo() { return memo; }

    public int getNumber() { return number; }

    public byte getStatus() { return status; }

    public boolean isCheck() { return isCheck; }

}