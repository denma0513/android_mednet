package com.mgear.mednetapp.entity.health;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.mgear.mednetapp.R;

import org.json.JSONObject;


/**
 * 血糖
 */
public class HealthBloodSugarSet extends HealthBaseSet {
    private Context mContext;
    private int ac;
    private int pc;

    public HealthBloodSugarSet(Context context, JSONObject data) {
        mContext = context;
        ac = data.optInt("ac");
        pc = data.optInt("pc");

        name = data.optString("healthTypeName");
        healthType = data.optString("healthType");

        id = data.optInt("id");
        measureID = data.optString("measure_id");
        customerID = data.optInt("customer_id");
        customerAppID = data.optString("customer_app_id");
        createdAt = data.optString("created_at");
        updatedAt = data.optString("updated_at");
        mTime = data.optString("m_time");
        type = data.optInt("type");

        unit = "mg/dl";
        healthImg = data.optInt("healthImg");
        healthIcon = data.optInt("healthIcon");
        statusArr = new String[]{"偏低", "標準", "略高"};
        rangeArr = new double[]{69, 125, 150};
        rangeArr2 = new double[]{69, 199, 230};
        colorArr = new int[]{R.color.status_non_enough, R.color.status_normal, R.color.status_danger};
        max = rangeArr[rangeArr.length - 1];
        computeValue();


    }

    //判斷血糖
    private void computeValue() {
        if (ac > 0) {
            acRule();
            value2 = "飯前";
            value = ac + "";
        } else {
            pcRule();
            value2 = "飯後";
            value = pc + "";
        }


        //unit2 = value2+" "+unit;
    }

    //飯前血糖規則
    private void acRule() {
        if (ac >= 50 && ac <= 69) {
            status = 0;
        } else if (ac >= 70 && ac <= 125) {
            status = 1;
        } else if (ac >= 126) {
            status = 2;
        }

        statusTitle = "●  " + statusArr[status];
        color = mContext.getResources().getColor(colorArr[status]);
        colorID = colorArr[status];
    }

    //飯後血糖規則
    private void pcRule() {
        if (pc >= 50 && pc <= 69) {
            status = 0;
        } else if (pc >= 71 && pc <= 199) {
            status = 1;
        } else if (pc >= 200) {
            status = 2;
        }
        statusTitle = "●  " + statusArr[status];
        color = mContext.getResources().getColor(colorArr[status]);
        colorID = colorArr[status];
    }

    public int getAcPcImg (){
        if (ac > 0) {
            return R.drawable.ic_meal_after;
        } else {
            return R.drawable.ic_meal_before;
        }
    }

    public int getAc() {
        return ac;
    }

    public int getPc() {
        return pc;
    }
}
