package com.mgear.mednetapp.entity.health;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;

import org.json.JSONObject;

/**
 * 飲水
 */
public class HealthDrinkingSet extends HealthBaseSet {
    private int today; //飲水量
    private int suggest; //目標
    private int undo;  //尚未完成量
    private static int capacity = 250;  //一杯容量
    private Context mContext;

    public HealthDrinkingSet(Context context, JSONObject data) {
        mContext = context;
        today = data.optInt("drinking_water_today");
        suggest = data.optInt("drinking_water_suggest");
        undo = data.optInt("drinking_water_undo");


        name = data.optString("healthTypeName");
        healthType = data.optString("healthType");
        id = data.optInt("id");
        measureID = data.optString("measure_id");
        customerID = data.optInt("customer_id");
        customerAppID = data.optString("customer_app_id");
        createdAt = data.optString("created_at");
        updatedAt = data.optString("updated_at");
        mTime = data.optString("m_time");
        type = data.optInt("type");

        unit = "ml";
        healthImg = data.optInt("healthImg");
        healthIcon = data.optInt("healthIcon");
        statusArr = new String[]{"未達成", "標準"};
        rangeArr = new double[]{today, MegaApplication.WaterCapacity};
        colorArr = new int[]{R.color.status_non_enough, R.color.status_normal};
        max = rangeArr[rangeArr.length - 1];
        computeValue();
    }

    //判斷血糖
    private void computeValue() {
        if (undo > 0) {
            status = 0;

        } else if (undo <= 0) {
            status = 1;
        }
        statusTitle = "●  " + statusArr[status];
        color = mContext.getResources().getColor(colorArr[status]);
        colorID = colorArr[status];
        value = today + "";
        value2 = "";
        unit2 = "約 (杯)";
    }

    public SpannableString todayValue() {
        String ret = today + " /" + MegaApplication.WaterCapacity;
        int i1 = ret.indexOf("/");
        SpannableString styledText = new SpannableString(ret);
        styledText.setSpan(new RelativeSizeSpan(0.5f), i1, ret.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return styledText;
    }

    public int getCupCount(){
        return today/capacity;
    }

    public int getToday() {
        return today;
    }

    public int getSuggest() {
        return suggest;
    }

    public int getUndo() {
        return undo;
    }

    public static int getCapacity() {
        return capacity;
    }
}
