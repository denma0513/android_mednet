package com.mgear.mednetapp.entity;

import android.content.Intent;

public class OauthSet {
    private String id;
    private String accessToken;
    private String refreshToken;
    private String name;
    private String lastName;
    private String pic;
    private String email;
    private String type;
    private String gender;
    private String birthday;

    public OauthSet setId(String id) {
        this.id = id;
        return this;
    }

    public OauthSet setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public OauthSet setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public OauthSet setName(String name) {
        this.name = name;
        return this;
    }

    public OauthSet setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public OauthSet setEmail(String email) {
        this.email = email;
        return this;
    }

    public OauthSet setPic(String pic) {
        this.pic = pic;
        return this;
    }

    public OauthSet setType(int type) {
        this.type = type + "";
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getId() {
        return id;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getPic() {
        return pic;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }

    public String getGender() {
        return gender;
    }

    public OauthSet setGender(String gender) {
        if (gender == null || "UNSPECIFIED".equals(gender)) {
            gender = "";
        }
        this.gender = gender;
        return this;
    }

    public String getBirthday() {
        return birthday;
    }

    public OauthSet setBirthday(String birthday) {
        if (birthday == null) {
            birthday = "";
        }
        this.birthday = birthday;
        return this;
    }

}
