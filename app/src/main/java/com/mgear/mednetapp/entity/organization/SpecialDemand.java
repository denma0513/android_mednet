package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Jye on 2017/8/18.
 * class: SpecialDemand
 */
public class SpecialDemand {

    //編號
    private UUID id;
    //代碼
    private String code;
    //欄位名稱
    private String fieldName;
    //名稱
    private String name;
    //顯示類型
    private String displayCategory;
    //備註
    private String remark;

    /**
     * 轉換函數
     * @param json Json
     * @return Selected
     */
    public static ArrayList<String> parse(String json) {
        ArrayList<String> result = new ArrayList<>();
        if (json.equals("")) return result;
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray ary = obj.getJSONArray("SpecList");
            int len = ary.length();
            if (len == 0) return result;
            for (int i = 0; i < len; i++) {
                JSONObject objItem = ary.getJSONObject(i);
                result.add(objItem.getString("SpecID"));
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    /**
     * 轉換函數
     * @param json Json
     * @param status Status
     * @param culture Culture
     * @return List
     */
    public static SpecialDemandSet parse(String json, int status, CultureEnum culture) {
        SpecialDemandSet result = new SpecialDemandSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String nameField;
            switch (culture) {
                case English:
                    nameField = "NameEn";
                    break;
                case TraditionalChinese:
                    nameField = "Name";
                    break;
                default:
                    nameField = "Name";
                    break;
            }

            for (int i = 0; i < len; i++) {
                SpecialDemand sd = new SpecialDemand();
                JSONObject obj = jDataAry.getJSONObject(i);
                //取得是否啟用
                if (obj.getBoolean("IsEnable")) {
                    sd.setId(UUID.fromString(obj.getString("ID")));
                    sd.setCode(obj.getString("Code"));
                    sd.setName(obj.getString(nameField));
                    sd.setFieldName(obj.getString("FieldName"));
                    sd.setDisplayCategory(obj.getString("DisplayCategory"));
                    sd.setRemark(obj.getString("Remark"));
                    result.add(sd);
                }
            }

            //加入無的項目
            SpecialDemand none = new SpecialDemand();
            none.setId(UUID.fromString("00000000-0000-0000-0000-000000000000"));
            none.setCode("None");
            none.setFieldName("icon_demand_none");
            none.setDisplayCategory("01");
            none.setRemark("None");
            //判斷語系
            switch (culture) {
                case English:
                    none.setName("None");
                    break;
                case TraditionalChinese:
                    none.setName("無");
                    break;
            }
            result.add(none);
        }
        catch (Exception ignored) { }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getName() {
        return name;
    }

    public String getDisplayCategory() {
        return displayCategory;
    }

    public String getRemark() {
        return remark;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setCode(String code) {
        this.code = code;
    }

    private void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setDisplayCategory(String displayCategory) { this.displayCategory = displayCategory; }

    private void setRemark(String remark) {
        this.remark = remark;
    }

}