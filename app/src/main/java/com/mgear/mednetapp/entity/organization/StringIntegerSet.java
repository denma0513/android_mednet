package com.mgear.mednetapp.entity.organization;

/**
 * Created by Jye on 2017/8/9.
 * class: StringIntegerSet
 */
public class StringIntegerSet {

    //主要
    private String major;
    //次要
    private int minor;

    public StringIntegerSet(String major, int minor) {
        this.major = major;
        this.minor = minor;
    }

    public String getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

}