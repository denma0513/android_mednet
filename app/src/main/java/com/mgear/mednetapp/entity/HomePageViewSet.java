package com.mgear.mednetapp.entity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class HomePageViewSet {

    private String title;
    private String content;
    private String viewUrl;
    private String ansNum;
    private String date;
    private String ansDate;
    private String docName;
    private String actionType;
    private String actionTarget;

    public HomePageViewSet(JSONObject data) {
        title = data.optString("title");
        content = data.optString("content");
        viewUrl = data.optString("viewUrl");
        ansNum = data.optString("ansNum");
        date = data.optString("date");
        ansDate = data.optString("ansDate");
        docName = data.optString("docName");
        actionType = data.optJSONObject("actionData").optString("actionType");
        actionTarget = data.optJSONObject("actionData").optString("actionTarget");
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getViewUrl() {
        return viewUrl;
    }

    public String getAnsNum() {
        return ansNum;
    }

    public String getDate() {
        return date;
    }

    public String getAnsDate() {
        return ansDate;
    }

    public String getDocName() {
        return docName;
    }

    public String getActionType() {
        return actionType;
    }

    public String getActionTarget() {
        return actionTarget;
    }
}
