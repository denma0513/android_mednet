package com.mgear.mednetapp.entity.organization;

/**
 * Created by Jye on 2017/7/5.
 * class: BeaconReceiveValue
 */
public class BeaconReceiveValue {

    //索引
    private int key;
    //加總值
    //private int sum;
    //次數
    //private int count;
    //結果
    private int result;

    public BeaconReceiveValue(int key, int value) {
        this.key = key;
        //this.sum = value;
        //this.count = 1;
        this.result = value;
    }

    public void add(int value) {
        //this.sum += value;
        //this.count++;
        this.result = value;//(this.sum / this.count + value) / 2;
    }

    public int getKey() {
        return key;
    }

    public int getResult() {
        return result;
    }

}