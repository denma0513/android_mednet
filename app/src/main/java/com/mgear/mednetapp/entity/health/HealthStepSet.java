package com.mgear.mednetapp.entity.health;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;

import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * 血壓
 */
public class HealthStepSet extends HealthBaseSet {
    private int stepCount; //步數
    private Double distance; //距離
    private Double calorie;  //卡路里
    private int measureSource;  //資料來源
    private Context mContext;

    public HealthStepSet(Context context, JSONObject data) {
        mContext = context;
        name = data.optString("healthTypeName");
        healthType = data.optString("healthType");
        DecimalFormat df = new DecimalFormat("##.0");//格式化小數
        try {
            stepCount = data.optInt("step_count", 0);
            distance = Double.parseDouble(df.format(data.optDouble("distance", 0.0)));
            calorie = Double.parseDouble(df.format(data.optDouble("calorie", 0.0)));
        } catch (Exception e) {
            stepCount = data.optInt("step_count", 0);
            distance = 0.0;
            calorie = 0.0;
        }

        measureSource = data.optInt("measure_source");
        id = data.optInt("id");
        measureID = data.optString("measure_id");
        customerID = data.optInt("customer_id");
        customerAppID = data.optString("customer_app_id");
        createdAt = data.optString("created_at");
        updatedAt = data.optString("updated_at");
        mTime = data.optString("m_time");
        type = data.optInt("type");

        unit = "步";
        healthImg = data.optInt("healthImg");
        healthIcon = data.optInt("healthIcon");
        statusArr = new String[]{"偏低", "標準"};
        rangeArr = new double[]{stepCount, MegaApplication.TargetStep};
        colorArr = new int[]{R.color.status_non_enough, R.color.gray};
        max = rangeArr[rangeArr.length - 1];

        computeValue();
    }

    //判斷步數
    private void computeValue() {
        value = stepCount + "";
        value2 = "" + MegaApplication.TargetStep;
        value3 = distance + "";
        statusTitle = "";
        color = mContext.getResources().getColor(R.color.home_title_black2);
        colorID = R.color.home_title_black2;
        unit2 = "km";
    }


    public SpannableString stepCount() {
        String ret = stepCount + " /" + MegaApplication.TargetStep;
        int i1 = ret.indexOf("/");
        SpannableString styledText = new SpannableString(ret);
        styledText.setSpan(new RelativeSizeSpan(0.3f), i1, ret.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return styledText;
    }

    public int getStepCount() {
        return stepCount;
    }

    public Double getDistance() {
        return distance;
    }

    public Double getCalorie() {
        return calorie;
    }

    public int getMeasureSource() {
        return measureSource;
    }
}
