package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jye on 2017/7/31.
 * class: SurveyItem
 */
public class SurveyItem {

    //編號
    private UUID id;
    //名稱
    private String name;
    //評分
    private float rating;

    /**
     * 轉換函數
     * @param json Json
     * @param status Status
     * @param culture Culture
     * @return List
     */
    public static SurveyItemSet parse(String json, int status, CultureEnum culture) {
        SurveyItemSet result = new SurveyItemSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String nameField;
            switch (culture) {
                case English:
                    nameField = "NameEn";
                    break;
                case TraditionalChinese:
                    nameField = "Name";
                    break;
                default:
                    nameField = "Name";
                    break;
            }

            for (int i = 0; i < len; i++) {
                SurveyItem si = new SurveyItem();
                JSONObject obj = jDataAry.getJSONObject(i);
                si.setId(UUID.fromString(obj.getString("ID")));
                si.setName(obj.getString(nameField));
                result.add(si);
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getRating() { return rating; }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setName(String name) {
        this.name = name;
    }

    public void setRating(float rating) { this.rating = rating; }

}