package com.mgear.mednetapp.entity.health;

import android.content.Context;

import com.mgear.mednetapp.R;

import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * 體位
 */
public class HealthBodySet extends HealthBaseSet {
    private Double bh; //身高
    private Double bw; //體重
    private Double bmi;  //bmi
    private String note;
    private Context mContext;

    public HealthBodySet(Context context, JSONObject data) {
        mContext = context;
        DecimalFormat fnum = new DecimalFormat("##0.0");
        name = data.optString("healthTypeName");
        healthType = data.optString("healthType");
        try {
            bh = Double.parseDouble(fnum.format(data.optDouble("bh")));
            bw = Double.parseDouble(fnum.format(data.optDouble("bw")));
            bmi = Double.parseDouble(fnum.format(data.optDouble("bmi")));
        } catch (Exception e) {
            bh = 0.0;
            bw = 0.0;
            bmi = 0.0;
        }
        note = data.optString("note");
        id = data.optInt("id");
        measureID = data.optString("measure_id");
        customerID = data.optInt("customer_id");
        customerAppID = data.optString("customer_app_id");
        createdAt = data.optString("created_at");
        updatedAt = data.optString("updated_at");
        mTime = data.optString("m_time");
        type = data.optInt("type");
        healthImg = data.optInt("healthImg");
        healthIcon = data.optInt("healthIcon");

        unit = "公斤";
        unit2 = "BMI";

        statusArr = new String[]{"偏輕", "標準", "偏重", "過重"};
        rangeArr = new double[]{18.4, 24, 27, 40};
        colorArr = new int[]{
                R.color.status_non_enough,
                R.color.status_normal,
                R.color.status_warning,
                R.color.status_danger
        };
        max = rangeArr[rangeArr.length - 1];

        computeValue();
    }

    //判斷體位
    private void computeValue() {
        if (bmi >= 12 && bmi <= 18.4) {
            status = 0;
        } else if (bmi >= 18.5 && bmi <= 24) {
            status = 1;
        } else if (bmi >= 24.1 && bmi <= 27) {
            status = 2;
        } else if (bmi >= 27.1) {
            status = 3;
        }


        statusTitle = "●  " + statusArr[status];
        color = mContext.getResources().getColor(colorArr[status]);
        colorID = colorArr[status];
        value = bw + "";
        value2 = bmi + "";
    }

    public Double getBh() {
        return bh;
    }

    public Double getBw() {
        return bw;
    }

    public Double getBmi() {
        return bmi;
    }

    public String getNote() {
        return note;
    }
}
