package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;
import com.mgear.mednetapp.enums.StatusEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/5.
 * class: CustomerReportExamine
 */
public class CustomerReportExamine {

    private UUID id;
    private UUID reportId;
    private UUID examineId;
    private String examineCode;
    private String examineName;
    private UUID roomId;
    private int status;
    private Date checkin;
    private Date checkout;

    /**
     * 轉換函數
     * @param json Json
     * @param status Status
     * @param culture Culture
     * @return List
     */
    public static CustomerReportExamineSet parse(String json, int status, CultureEnum culture) {
        CustomerReportExamineSet result = new CustomerReportExamineSet(status);
        if (json.equals("")) return result;
        //取得日期格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        //判斷語系
        String examineField;
        switch (culture) {
            case English:
                examineField = "ExamNameEn";
                break;
            case TraditionalChinese:
                examineField = "ExamName";
                break;
            default:
                examineField = "ExamName";
                break;
        }

        int statusCode;
        boolean showSupplementIcon = false;
        try {
            JSONArray ary = new JSONArray(json);
            for (int i = 0; i < ary.length(); i++) {
                JSONObject objExamine = ary.getJSONObject(i);
                CustomerReportExamine examine = new CustomerReportExamine();
                examine.setId(UUID.fromString(objExamine.getString("ID")));
                examine.setReportId(UUID.fromString(objExamine.getString("ReportID")));
                examine.setExamineId(UUID.fromString(objExamine.getString("ExamID")));
                examine.setExamineCode(objExamine.getString("ExamCode"));
                examine.setRoomId(UUID.fromString(objExamine.getString("RoomID")));
                examine.setExamineName(objExamine.getString(examineField));
                statusCode = objExamine.getInt("Status");
                examine.setStatus(statusCode);

                //判斷是否顯示補做的圖示
                if (!showSupplementIcon && statusCode == StatusEnum.Supplement.getCode())
                    showSupplementIcon = true;

                //取得時間格式
                String dateStr = objExamine.getString("CheckInTime");
                if (!dateStr.equals("null"))
                    examine.setCheckin(sdf.parse(dateStr));
                dateStr = objExamine.getString("CheckOutTime");
                if (!dateStr.equals("null"))
                    examine.setCheckout(sdf.parse(dateStr));
                result.add(examine);
            }
        }
        catch (Exception ignored) { }
        result.setSupplementIcon(showSupplementIcon);
        return result;
    }

    public UUID getId() {
        return id;
    }

    public UUID getReportId() {
        return reportId;
    }

    public UUID getExamineId() {
        return examineId;
    }

    public String getExamineCode() { return examineCode; }

    public String getExamineName() {
        return examineName;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public int getStatus() {
        return status;
    }

    public Date getCheckin() {
        return checkin;
    }

    public Date getCheckout() {
        return checkout;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    void setReportId(UUID reportId) {
        this.reportId = reportId;
    }

    void setExamineId(UUID examineId) {
        this.examineId = examineId;
    }

    void setExamineCode(String examineCode) {
        this.examineCode = examineCode;
    }

    void setExamineName(String examineName) {
        this.examineName = examineName;
    }

    void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    void setStatus(int status) {
        this.status = status;
    }

    void setCheckin(Date checkin) {
        this.checkin = checkin;
    }

    void setCheckout(Date checkout) { this.checkout = checkout; }

}