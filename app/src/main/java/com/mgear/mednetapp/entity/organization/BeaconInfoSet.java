package com.mgear.mednetapp.entity.organization;

import android.util.SparseArray;

import com.mgear.mednetapp.algorithms.dijkstra.Edge;
import com.mgear.mednetapp.algorithms.dijkstra.Vertex;
import com.mgear.mednetapp.algorithms.dijkstra.Graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/9.
 * class: BeaconInfoSet
 */
public class BeaconInfoSet extends SparseArray<BeaconInfo> {

    //簡報室
    public static UUID BriefingBeacon = UUID.fromString("4A473488-3D9D-461F-AB88-C32F3D12E2F9");
    //男更衣間
    public static UUID MaleClothesBeacon = UUID.fromString("D9D40BC9-A3D4-4561-AC2B-40AEAA421A3E");
    //女更衣間
    public static UUID FemaleClothesBeacon = UUID.fromString("AB9E8E97-89C7-4948-B1CC-E9865F58ADA2");
    //櫃台
    public static UUID CounterBeacon = UUID.fromString("B3C705AA-3F79-408F-896A-0D7309269F30");
    //男廁
    public static  UUID MaleToiletBeacon = UUID.fromString("470F09D3-EFA1-4486-8973-D1DB1EC8768E");
    //女廁
    public static UUID FemaleToiletBeacon = UUID.fromString("FE7A9633-053C-4B44-902B-04AE9BB75FCC");

    private SparseArray<Vertex> mapNode;
    private List<Vertex> nodeList;
    private List<Edge> edgeList;
    private HashMap<UUID, Integer> mapList;

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public BeaconInfoSet(int status) {
        this.status = status;
        mapNode = new SparseArray<>();
        nodeList = new ArrayList<>();
        edgeList = new ArrayList<>();
    }

    public BeaconInfo get(UUID uuid) {
        if (mapList != null && uuid != null) {
            Integer key = mapList.get(uuid);
            if (key != null)
                return this.get(key);
        }
        return null;
    }

    void addVertex(Vertex node) {
        mapNode.put(node.getId(), node);
        nodeList.add(node);
    }

    public Vertex getVertex(int key) { return mapNode.get(key); }

    void addEdge(Edge edge) { edgeList.add(edge); }

    public Graph getGraph() { return new Graph(nodeList, edgeList); }

    void setKeyMap(HashMap<UUID, Integer> map) { this.mapList = map; }

}