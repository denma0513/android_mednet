package com.mgear.mednetapp.entity.organization;

import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jye on 2017/6/19.
 * class: Organization
 */
public class Organization {

    //編號
    private UUID id;
    //名稱
    private String name;
    //類型
    private int category;
    //是否啟用
    private boolean isEnable;
    //是否為主機構
    private boolean isMajor;
    //圖示路徑
    private String logoPath;
    //描述
    private String description;

    /**
     * 轉換函數
     * @param json JsonString
     * @return obj
     */
    public static Organization parse(String json) {
        Organization result = new Organization();
        if (json.equals("")) return result;
        try {
            JSONObject obj = new JSONObject(json);
            result.setId(UUID.fromString(obj.getString("ID")));
            result.setName(obj.getString("Name"));
            result.setCategory(obj.getInt("Category"));
            result.setEnable(obj.getBoolean("IsEnable"));
            result.setMajor(obj.getBoolean("IsMajor"));
            result.setLogoPath(obj.getString("LogoPath"));
            result.setDescription(obj.getString("Description"));
        }
        catch (Exception ignored) { }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCategory() {
        return category;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public boolean isMajor() {
        return isMajor;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public String getDescription() {
        return description;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setCategory(int category) {
        this.category = category;
    }

    private void setEnable(boolean enable) {
        isEnable = enable;
    }

    private void setMajor(boolean major) {
        isMajor = major;
    }

    private void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    private void setDescription(String description) {
        this.description = description;
    }

}