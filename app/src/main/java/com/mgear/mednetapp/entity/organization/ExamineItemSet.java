package com.mgear.mednetapp.entity.organization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jye on 2017/7/23.
 * class: ExamineItemSet
 */
public class ExamineItemSet extends HashMap<String, ExamineItem> {

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public ExamineItemSet(int status) { this.status = status; }

    /**
     * 取得檢查項目名稱清單
     * @param keys Keys
     * @return List
     */
    public List<String> getExamineName(String[] keys) {
        List<String> result = new ArrayList<>();
        if (keys != null && keys.length > 0) {
            for (String key : keys) {
                ExamineItem ei = this.get(key);
                if (ei != null)
                    result.add(ei.getName());
            }
        }
        return result;
    }

    /**
     * 取得檢查項目的描述
     * @param code Code
     * @return String
     */
    public String getExamineDescription(String code) {
        ExamineItem ei = this.get(code);
        if (ei != null)
            return ei.getDescription();
        return "";
    }

}