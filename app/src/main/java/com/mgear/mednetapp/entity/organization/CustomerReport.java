package com.mgear.mednetapp.entity.organization;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/5.
 * class: CustomerReport
 */
public class CustomerReport {

    private UUID id;
    private UUID customerId;
    private int contactCode;
    private String reportCode;
    private long documentCode;
    private String productName;
    private Date eventDate;

    public UUID getId() {
        return id;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public int getContactCode() {
        return contactCode;
    }

    public String getReportCode() {
        return reportCode;
    }

    public long getDocumentCode() {
        return documentCode;
    }

    public String getProductName() {
        return productName;
    }

    public Date getEventDate() {
        return eventDate;
    }

    void setId(UUID id) {
        this.id = id;
    }

    void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    void setContactCode(int contactCode) {
        this.contactCode = contactCode;
    }

    void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    void setDocumentCode(long documentCode) {
        this.documentCode = documentCode;
    }

    void setProductName(String productName) {
        this.productName = productName;
    }

    void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

}