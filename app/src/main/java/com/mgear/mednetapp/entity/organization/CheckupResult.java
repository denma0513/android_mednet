package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Jye on 2017/8/21.
 * class: CheckupResult
 */
public class CheckupResult {

    //檢查項目代碼
    private String examineCode;
    //檢查項目名稱
    private String examineName;
    //細項名稱
    private String productName;
    //參考值
    private String referenceValue;
    //診斷值
    private String diagnosisValue;
    //結果
    private boolean result;

    /**
     * 轉換函數
     * @param json JSON
     * @param status Status
     * @param systemSet  System
     * @return List
     */
    public static CheckupResultSet parse(String json, int status, HealthySystemSet systemSet) {
        CheckupResultSet result = new CheckupResultSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            for (int i = 0; i < len; i++) {
                CheckupResult cr = new CheckupResult();
                JSONObject obj = jDataAry.getJSONObject(i);
                String code = obj.getString("FunItemID");
                //取得檢查項目所屬的系統編號
                UUID typeId = systemSet.getExamineOfType(code);
                if (typeId == null)
                    continue;
                //取得參考值與診斷值
                String reference = obj.getString("RefValue").trim();
                String diagnosis = obj.getString("DiagnosisValue").trim();
                //判斷結果是否正常
                boolean normal = true;
                //判斷診斷值與參考值是否一致
                if (!(reference.equals(diagnosis) || diagnosis.equals("-") || diagnosis.equals("-NORM") || diagnosis.charAt(0) == '無')) {
                    int refValue, diaValue;
                    switch (reference.charAt(0)) {
                        case '＜':
                            refValue = CommonFunc.tryParseInteger(reference.substring(1, reference.length()));
                            diaValue = CommonFunc.tryParseInteger(diagnosis);
                            if (!(refValue == 0 || diaValue == 0))
                                normal = diaValue <= refValue;
                            break;
                        case '＞':
                            refValue = CommonFunc.tryParseInteger(reference.substring(1, reference.length()));
                            diaValue = CommonFunc.tryParseInteger(diagnosis);
                            if (!(refValue == 0 || diaValue == 0))
                                normal = diaValue >= refValue;
                            break;
                        default:
                            //判斷是否有區間符號
                            if (reference.contains("～")) {
                                int[] ary = CommonFunc.convertToIntArray(reference, "～");
                                if (ary != null && ary.length == 2) {
                                    diaValue = CommonFunc.tryParseInteger(diagnosis);
                                    if (diaValue != 0)
                                        normal = diaValue >= ary[0] && diaValue <= ary[1];
                                }
                            }
                            else normal = true;
                            break;
                    }
                }

                //判斷結果是否正常
                if (!normal)
                    result.setCheckupResult(typeId, false);

                cr.setExamineCode(code);
                cr.setExamineName(obj.getString("FunItemName"));
                cr.setProductName(obj.getString("ProductName"));
                cr.setReferenceValue(reference);
                cr.setDiagnosisValue(diagnosis);
                cr.setResult(normal);
                ArrayList<CheckupResult> lstCheckup = result.get(typeId);
                if (lstCheckup == null) {
                    lstCheckup = new ArrayList<>();
                    lstCheckup.add(cr);
                    result.put(typeId, lstCheckup);
                }
                else lstCheckup.add(cr);
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    private CheckupResult() { this.result = true; }

    public String getExamineCode() {
        return examineCode;
    }

    public String getExamineName() {
        return examineName;
    }

    public String getProductName() {
        return productName;
    }

    public String getReferenceValue() {
        return referenceValue;
    }

    public String getDiagnosisValue() {
        return diagnosisValue;
    }

    public boolean getResult() { return result; }

    private void setExamineCode(String examineCode) {
        this.examineCode = examineCode;
    }

    private void setExamineName(String examineName) {
        this.examineName = examineName;
    }

    private void setProductName(String productName) {
        this.productName = productName;
    }

    private void setReferenceValue(String referenceValue) {
        this.referenceValue = referenceValue;
    }

    private void setDiagnosisValue(String diagnosisValue) {
        this.diagnosisValue = diagnosisValue;
    }

    private void setResult(boolean result) { this.result = result; }

}