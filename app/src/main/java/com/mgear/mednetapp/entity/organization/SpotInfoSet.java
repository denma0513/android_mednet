package com.mgear.mednetapp.entity.organization;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/16.
 * class: SpotInfoSet
 */
public class SpotInfoSet extends HashMap<UUID, SpotInfo> {

    //簡報室
    public static UUID BriefingMarker = UUID.fromString("DA87D023-B775-466B-B88C-21E112F5AE90");
    //男更衣間
    public static UUID MaleClothesMarker = UUID.fromString("43A53BB3-63F2-4298-A224-180136F28865");
    //女更衣間
    public static UUID FemaleClothesMarker = UUID.fromString("53ECA9FD-2F01-4ADE-BDA3-DF3E301CE901");
    //櫃台
    public static UUID CounterMarker = UUID.fromString("4C5937BE-F9E0-44D2-97B5-4D5D3C4984AF");
    //男廁
    public static  UUID MaleToiletMarker = UUID.fromString("C9896249-F844-412A-8903-0E19DA3E2FF7");
    //女廁
    public static UUID FemaleToiletMarker = UUID.fromString("74F5841A-C51C-4AB8-84A9-B01D6A3F7C1C");
    //抽血台
    public static UUID BloodMarker = UUID.fromString("D804E5A9-494A-452B-8B11-E2599A1F3AF5");
    //正門口
    public static UUID EntranceMarker = UUID.fromString("18ACF542-5E7C-414E-A769-5D831E29AFB7");

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public SpotInfoSet(int status) { this.status = status; }

}