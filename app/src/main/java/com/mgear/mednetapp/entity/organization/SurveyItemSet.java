package com.mgear.mednetapp.entity.organization;

import java.util.ArrayList;

/**
 * Created by Jye on 2017/7/31.
 * class: SurveyItemSet
 */
public class SurveyItemSet extends ArrayList<SurveyItem> {

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public SurveyItemSet(int status) { this.status = status; }

}