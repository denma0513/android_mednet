package com.mgear.mednetapp.entity.organization;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/21.
 * class: CheckableItemSet
 */
public class CheckableItemSet extends HashMap<UUID, CheckableItem> {
}