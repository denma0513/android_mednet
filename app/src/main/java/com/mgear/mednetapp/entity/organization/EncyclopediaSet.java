package com.mgear.mednetapp.entity.organization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jye on 2017/8/9.
 * class: EncyclopediaSet
 */
public class EncyclopediaSet extends HashMap<UUID, ArrayList<Encyclopedia>> {

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public EncyclopediaSet(int status) { this.status = status; }

}