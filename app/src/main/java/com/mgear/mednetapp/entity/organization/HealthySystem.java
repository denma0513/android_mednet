package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jye on 2017/8/19.
 * class: HealthySystem
 */
public class HealthySystem {

    //編號
    private UUID id;
    //系統名稱
    private String name;
    //X軸座標
    private int posLeft;
    //Y軸座標
    private int posTop;

    /**
     * 轉換函數
     * @param json Json
     * @param status Status
     * @param culture Culture
     * @return List
     */
    public static HealthySystemSet parse(String json, int status, CultureEnum culture) {
        HealthySystemSet result = new HealthySystemSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String nameField;
            switch (culture) {
                case English:
                    nameField = "NameEn";
                    break;
                case TraditionalChinese:
                    nameField = "Name";
                    break;
                default:
                    nameField = "Name";
                    break;
            }

            for (int i = 0; i < len; i++) {
                HealthySystem hs = new HealthySystem();
                JSONObject obj = jDataAry.getJSONObject(i);
                UUID id = UUID.fromString(obj.getString("ID"));
                hs.setId(id);
                hs.setName(obj.getString(nameField));
                hs.setPosLeft(obj.getInt("PosLeft"));
                hs.setPosTop(obj.getInt("PosTop"));

                //取得檢查項目清單
                JSONArray jsonArray = obj.getJSONArray("ExamItemsListWithName");
                int examLen = jsonArray.length();
                if (examLen > 0) {
                    for (int j = 0; j < examLen; j++) {
                        JSONObject objExam = jsonArray.getJSONObject(j);
                        result.addExamineMap(objExam.getString("Code"), id);
                    }
                }
                result.put(id, hs);
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPosLeft() {
        return posLeft;
    }

    public int getPosTop() {
        return posTop;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setPosLeft(int posLeft) {
        this.posLeft = posLeft;
    }

    private void setPosTop(int posTop) {
        this.posTop = posTop;
    }

}