package com.mgear.mednetapp.entity.organization;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jye on 2017/8/19.
 * class: HealthySystemSet
 */
public class HealthySystemSet extends HashMap<UUID, HealthySystem> {

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public HealthySystemSet(int status) { this.status = status; }

    private HashMap<String, UUID> examineMap = new HashMap<>();

    void addExamineMap(String key, UUID value) {
        if (!examineMap.containsKey(key))
            examineMap.put(key, value);
    }

    UUID getExamineOfType(String examine) {
        return examineMap.get(examine);
    }

}