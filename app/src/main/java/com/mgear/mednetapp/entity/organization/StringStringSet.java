package com.mgear.mednetapp.entity.organization;

/**
 * Created by Jye on 2017/7/21.
 * class: StringStringSet
 */
public class StringStringSet {

    //主要
    private String major;
    //次要
    private String minor;
    //備註
    private String memo;
    //結果
    private boolean result;

    public StringStringSet(String major, String minor) {
        this.major = major;
        this.minor = minor;
        this.result = true;
    }

    public StringStringSet(String major, String minor, String memo) {
        this.major = major;
        this.minor = minor;
        this.memo = memo;
        this.result = true;
    }

    public StringStringSet(String major, String minor, String memo, boolean result) {
        this.major = major;
        this.minor = minor;
        this.memo = memo;
        this.result = result;
    }

    public String getMajor() {
        return major;
    }

    public String getMinor() {
        return minor;
    }

    public String getMemo() { return memo; }

    public boolean getResult() { return result; }

}