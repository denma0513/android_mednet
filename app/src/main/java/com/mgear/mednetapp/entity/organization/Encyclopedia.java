package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Jye on 2017/8/9.
 * class: Encyclopedia
 */
public class Encyclopedia {

    //編號
    private UUID id;
    //系統編號
    private UUID healthySystemId;
    //性別
    private char sex;
    //問題
    private String question;
    //解答
    private String answerText;
    //解答影片
    private String answerVideo;
    //影片縮圖
    private String answerPicture;

    /**
     * 轉換函數
     * @param json Json
     * @param status Status
     * @param culture Culture
     * @return EncyclopediaSet
     */
    public static EncyclopediaSet parse(String json, int status, CultureEnum culture) {
        EncyclopediaSet result = new EncyclopediaSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String questionField, answerField;
            switch (culture) {
                case English:
                    questionField = "QuestionEn";
                    answerField = "AnswerTextEn";
                    break;
                case TraditionalChinese:
                    questionField = "Question";
                    answerField = "AnswerText";
                    break;
                default:
                    questionField = "Question";
                    answerField = "AnswerText";
                    break;
            }

            for (int i = 0; i < len; i++) {
                Encyclopedia model = new Encyclopedia();
                JSONObject obj = jDataAry.getJSONObject(i);
                UUID healthySystemId = UUID.fromString(obj.getString("HealthySystemTypeID"));
                model.setId(UUID.fromString(obj.getString("ID")));
                model.setHealthySystemId(healthySystemId);
                model.setSex(obj.getString("Sex").charAt(0));
                model.setQuestion(obj.getString(questionField));
                model.setAnswerText(obj.getString(answerField));
                model.setAnswerVideo(obj.getString("AnswerVideo"));
                model.setAnswerPicture(obj.getString("AnswerPicture"));

                //判斷索引是否存在
                if (result.containsKey(healthySystemId))
                    result.get(healthySystemId).add(model);
                else {
                    ArrayList<Encyclopedia> list = new ArrayList<>();
                    list.add(model);
                    result.put(healthySystemId, list);
                }
            }
        }
        catch (Exception ignored) { ignored.printStackTrace(); }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public UUID getHealthySystemId() {
        return healthySystemId;
    }

    public char getSex() {
        return sex;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswerText() {
        return answerText;
    }

    public String getAnswerVideo() {
        return answerVideo;
    }

    public String getAnswerPicture() {
        return answerPicture;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    private void setHealthySystemId(UUID healthySystemId) { this.healthySystemId = healthySystemId; }

    private void setSex(char sex) {
        this.sex = sex;
    }

    private void setQuestion(String question) {
        this.question = question;
    }

    private void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    private void setAnswerVideo(String answerVideo) {
        this.answerVideo = answerVideo;
    }

    private void setAnswerPicture(String answerPicture) {
        this.answerPicture = answerPicture;
    }

}