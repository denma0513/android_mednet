package com.mgear.mednetapp.entity.health;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;

import com.mgear.mednetapp.R;

import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * 睡眠時間
 */
public class HealthSleepingSet extends HealthBaseSet {
    private String sleepTime; //入眠時間
    private String getupTime; //起床時間
    private Double sleepingHours; //總時數
    private int measureSource;  //
    private Context mContext;

    public HealthSleepingSet(Context context, JSONObject data) {
        DecimalFormat df = new DecimalFormat("##.0");//格式化小數
        mContext = context;
        name = data.optString("healthTypeName");
        healthType = data.optString("healthType");
        sleepTime = data.optString("sleep_time");
        getupTime = data.optString("getup_time");
        sleepingHours = Double.parseDouble(df.format(data.optDouble("sleeping_hours",0.0)));
        measureSource = data.optInt("measure_source");
        id = data.optInt("id");
        measureID = data.optString("measure_id");
        customerID = data.optInt("customer_id");
        customerAppID = data.optString("customer_app_id");
        createdAt = data.optString("created_at");
        updatedAt = data.optString("updated_at");
        mTime = data.optString("m_time");
        type = data.optInt("type");

        unit = "小時";
        healthImg = data.optInt("healthImg");
        healthIcon = data.optInt("healthIcon");
        statusArr = new String[]{"不足", "標準", "過多"};
        rangeArr = new double[]{6, 9, 15};
        colorArr = new int[]{R.color.status_non_enough, R.color.status_normal, R.color.status_danger};
        max = rangeArr[rangeArr.length - 1];

        computeValue();
    }

    //判斷睡眠時間
    private void computeValue() {
        if (sleepingHours < 6) {
            status = 0;
        } else if (sleepingHours < 9 && sleepingHours >= 6) {
            status = 1;
        } else if (sleepingHours >= 9) {
            status = 2;
        }

        statusTitle = "●  " + statusArr[status];
        color = mContext.getResources().getColor(colorArr[status]);
        colorID = colorArr[status];
        value = sleepingHours + "";
        value2 = "";
        sleepingHours();
    }

    public SpannableString sleepingHours() {
        int hour = (int) (sleepingHours / 1);
        DecimalFormat df = new DecimalFormat("##.0");//格式化小數
        Log.i("debug", "sleepingHours = " + sleepingHours);
        int min = Double.isNaN(sleepingHours) ? 0 : (int) (Double.parseDouble(df.format(sleepingHours % 1)) * 60);
        String ret = hour + "小時" + min + "分";
        int i1 = ret.indexOf("小時");
        int i2 = ret.indexOf("分");
        SpannableString styledText = new SpannableString(ret);
        styledText.setSpan(new RelativeSizeSpan(0.5f), i1, i1 + 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        styledText.setSpan(new RelativeSizeSpan(0.5f), i2, i2 + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return styledText;
    }

    public String getSleepTime() {
        return sleepTime;
    }

    public String getGetupTime() {
        return getupTime;
    }

    public Double getSleepingHours() {
        return sleepingHours;
    }

    public int getMeasureSource() {
        return measureSource;
    }
}
