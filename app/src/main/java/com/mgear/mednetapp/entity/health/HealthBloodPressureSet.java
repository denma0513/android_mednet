package com.mgear.mednetapp.entity.health;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;

import com.mgear.mednetapp.R;

import org.json.JSONObject;

/**
 * 血壓
 */
public class HealthBloodPressureSet extends HealthBaseSet {
    private int sbp; //收縮壓
    private int dbp; //舒張壓
    private int hr;  //心跳
    protected int medication;//是否用藥
    private Context mContext;


    public HealthBloodPressureSet(Context context, JSONObject data) {
        mContext = context;
        name = data.optString("healthTypeName");
        healthType = data.optString("healthType");
        medication = data.optInt("medication");
        sbp = data.optInt("sbp");
        dbp = data.optInt("dbp");
        hr = data.optInt("hr");
        id = data.optInt("id");
        measureID = data.optString("measure_id");
        customerID = data.optInt("customer_id");
        customerAppID = data.optString("customer_app_id");
        createdAt = data.optString("created_at");
        updatedAt = data.optString("updated_at");
        mTime = data.optString("m_time");
        type = data.optInt("type");

        healthImg = data.optInt("healthImg");
        healthIcon = data.optInt("healthIcon");
        unit = "mmHg";
        //unit2 = "收縮壓/舒張壓 ("+unit+")";

        statusArr = new String[]{"偏低", "標準", "略高", "偏高"};
        rangeArr = new double[]{89, 134, 140, 250};
        rangeArr2 = new double[]{59, 84, 90, 130};
        colorArr = new int[]{
                R.color.status_non_enough,
                R.color.status_normal,
                R.color.status_warning,
                R.color.status_danger
        };
        max = rangeArr[rangeArr.length - 1];
        computeValue();


    }

    //判斷血壓
    private void computeValue() {
        if (sbp >= 141 && sbp <= 220 || dbp >= 91 && dbp <= 130) {
            status = 3;
        } else if (sbp >= 135 && sbp <= 140 || dbp >= 85 && dbp <= 90) {
            status = 2;
        } else if (sbp >= 40 && sbp <= 89 || dbp >= 40 && dbp <= 59) {
            status = 0;
        } else if (sbp >= 90 && sbp <= 134 || dbp >= 60 && dbp <= 84) {
            status = 1;
        }

        statusTitle = "●  " + statusArr[status];
        color = mContext.getResources().getColor(colorArr[status]);
        colorID = colorArr[status];
        value = sbp + "/" + dbp;
        value2 = hr + " 次/分";
        unit2 = "心跳(次/分)";
    }

    public SpannableString getBloodPressure() {
        String ret = sbp + " / " + dbp;
        int i1 = ret.indexOf("/");
        SpannableString styledText = new SpannableString(ret);
        styledText.setSpan(new RelativeSizeSpan(0.5f), i1, i1 + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return styledText;
    }

    public int getHr() {
        return hr;
    }

    public int getSbp() {
        return sbp;
    }

    public int getDbp() {
        return dbp;
    }

    public int getMedication() {
        return medication;
    }
}
