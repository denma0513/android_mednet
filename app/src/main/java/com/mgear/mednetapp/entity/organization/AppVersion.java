package com.mgear.mednetapp.entity.organization;

import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jye on 2017/6/19.
 * class: AppVersion
 */
public class AppVersion {

    //編號
    private UUID id;
    //應用程式名稱
    private String name;
    //主要編號
    private int major;
    //次要編號
    private int minor;
    //修補編號
    private int patch;
    //參數1
    private long parameter1;
    //參數2
    private long parameter2;
    //參數3
    private long parameter3;
    //應用程式路徑
    private String appPath;

    /**
     * 轉換函數
     * @param json JsonString
     * @return obj
     */
    public static AppVersion parse(String json) {
        AppVersion result = new AppVersion();
        if (json.equals("")) return result;

        try {
            JSONObject obj = new JSONObject(json);
            result.setId(UUID.fromString(obj.getString("ID")));
            result.setName(obj.getString("Name"));
            result.setMajor(obj.getInt("Major"));
            result.setMinor(obj.getInt("Minor"));
            result.setPatch(obj.getInt("Patch"));
            result.setParameter1(obj.getLong("Parameter1"));
            result.setParameter2(obj.getLong("Parameter2"));
            result.setParameter3(obj.getLong("Parameter3"));
            result.setAppPath(obj.getString("AppPath"));
        }
        catch (Exception ignored) { ignored.printStackTrace(); }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

    public int getPatch() {
        return patch;
    }

    public long getParameter1() { return parameter1; }

    public long getParameter2() { return parameter2; }

    public long getParameter3() { return parameter3; }

    public String getAppPath() {
        return appPath;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setMajor(int major) {
        this.major = major;
    }

    private void setMinor(int minor) {
        this.minor = minor;
    }

    private void setPatch(int patch) {
        this.patch = patch;
    }

    private void setParameter1(long parameter1) { this.parameter1 = parameter1; }

    private void setParameter2(long parameter2) { this.parameter2 = parameter2; }

    private void setParameter3(long parameter3) { this.parameter3 = parameter3; }

    private void setAppPath(String appPath) {
        this.appPath = appPath;
    }

}