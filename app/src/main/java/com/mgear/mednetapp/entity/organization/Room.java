package com.mgear.mednetapp.entity.organization;

import com.mgear.mednetapp.enums.CultureEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jye on 2017/7/12.
 * class: Room
 */
public class Room {

    //診間編號
    private UUID id;
    //地點編號
    private UUID locateId;
    //地點名稱
    private String locateName;
    //群組編號
    private int groupId;
    //診間代碼
    private String code;
    //診間名稱
    private String name;
    //類型
    private int type;
    //是否可用餐
    private boolean isMeal;
    //是否繳交物品
    private boolean isHandIn;
    //是否啟用
    private boolean enabled;
    //信標編號
    private UUID beaconId;
    //圖標編號
    private UUID markerId;

    /**
     * 轉換函數
     * @param json Json
     * @param status Status
     * @param culture Culture
     * @return List
     */
    public static RoomSet parse(String json, int status, CultureEnum culture) {
        RoomSet result = new RoomSet(status);
        if (json.equals("")) return result;
        try {
            JSONArray jDataAry = new JSONArray(json);
            int len = jDataAry.length();
            if (len == 0) return result;
            //判斷語系
            String nameField, locateField;
            switch (culture) {
                case English:
                    nameField = "NameEn";
                    locateField = "LocateNameEn";
                    break;
                case TraditionalChinese:
                    nameField = "Name";
                    locateField = "LocateName";
                    break;
                default:
                    nameField = "Name";
                    locateField = "LocateName";
                    break;
            }

            for (int i = 0; i < len; i++) {
                Room room = new Room();
                JSONObject obj = jDataAry.getJSONObject(i);
                UUID key = UUID.fromString(obj.getString("ID"));
                room.setId(key);
                room.setLocateId(UUID.fromString(obj.getString("LocateID")));
                room.setLocateName(obj.getString(locateField));
                room.setGroupId(obj.getInt("GroupID"));
                room.setCode(obj.getString("Code"));
                room.setName(obj.getString(nameField));
                room.setType(obj.getInt("Type"));
                room.setMeal(obj.getBoolean("HasMeal"));
                room.setHandIn(obj.getBoolean("IsHandIn"));
                room.setEnabled(obj.getBoolean("Enabled"));
                //判斷是否有設定信標
                String bid = obj.getString("BeaconID");
                if (!bid.equals("null")) {
                    UUID uuid = UUID.fromString(bid);
                    room.setBeaconId(uuid);
                    result.addBeacon(uuid);
                }
                //判斷是否有設定圖標
                String sid = obj.getString("SpotInfoID");
                if (!sid.equals("null"))
                    room.setMarkerId(UUID.fromString(sid));
                result.put(key, room);
            }
        }
        catch (Exception ignored) { }
        return result;
    }

    public UUID getId() {
        return id;
    }

    public UUID getLocateId() {
        return locateId;
    }

    public String getLocateName() { return locateName; }

    public int getGroupId() {
        return groupId;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public boolean isMeal() {
        return isMeal;
    }

    public boolean isHandIn() {
        return isHandIn;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public UUID getBeaconId() { return beaconId; }

    public UUID getMarkerId() { return markerId; }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setLocateId(UUID locateId) {
        this.locateId = locateId;
    }

    private void setLocateName(String locateName) { this.locateName = locateName; }

    private void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    private void setCode(String code) {
        this.code = code;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setType(int type) {
        this.type = type;
    }

    private void setMeal(boolean meal) {
        isMeal = meal;
    }

    private void setHandIn(boolean handIn) {
        isHandIn = handIn;
    }

    private void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    private void setBeaconId(UUID beaconId) { this.beaconId = beaconId; }

    private void setMarkerId(UUID markerId) { this.markerId = markerId; }

}