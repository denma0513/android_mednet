package com.mgear.mednetapp.entity.health;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class HealthBaseSet {
    protected String name;
    protected String healthType;
    protected String value;
    protected String value2;
    protected String value3;
    protected String unit;
    protected String unit2;
    protected String unit3;
    protected int id;
    protected String measureID;
    protected int customerID;
    protected String customerAppID;
    protected String createdAt;
    protected String updatedAt;
    protected String mTime;
    protected int type;
    protected int status;
    protected String statusTitle;
    protected int color;
    protected int colorID;
    protected String[] statusArr;
    protected double[] rangeArr;
    protected double[] rangeArr2; //目前只有血壓用
    protected int[] colorArr;
    protected Double max;
    protected int healthImg;
    protected int healthIcon;

    public String getRecordTime() {
        String retDate = "";
        try {
            Date now = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.TAIWAN);
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat sdf3 = new SimpleDateFormat("HH:mm");
            //sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            //Log.i("debug","mTime = "+mTime);
            Date after = sdf.parse(mTime);

            Calendar cal = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal.setTime(new Date());
            cal2.setTime(after);

            if (isSameDay(cal, cal2)) {
                retDate = "今日" + sdf3.format(after);
            } else {
                retDate = sdf2.format(after);
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        return retDate;
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 != null && cal2 != null) {
            return cal1.get(0) == cal2.get(0) && cal1.get(1) == cal2.get(1) && cal1.get(6) == cal2.get(6);
        } else {
            throw new IllegalArgumentException("The date must not be null");
        }
    }

    public SpannableString getStatusUseSpannableString() {
        if (statusTitle != null && statusTitle.contains("●")) {
            SpannableString styledText = new SpannableString(statusTitle);
            styledText.setSpan(new RelativeSizeSpan(0.5f), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            return styledText;
        }

        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHealthType() {
        return healthType;
    }

    public void setHealthType(String healthType) {
        this.healthType = healthType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMeasureID() {
        return measureID;
    }

    public void setMeasureID(String measureID) {
        this.measureID = measureID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getCustomerAppID() {
        return customerAppID;
    }

    public void setCustomerAppID(String customerAppID) {
        this.customerAppID = customerAppID;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusTitle() {

        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    public String[] getStatusArr() {
        return statusArr;
    }

    public void setStatusArr(String[] statusArr) {
        this.statusArr = statusArr;
    }

    public double[] getRangeArr() {
        return rangeArr;
    }

    public void setRangeArr(double[] rangeArr) {
        this.rangeArr = rangeArr;
    }

    public int[] getColorArr() {
        return colorArr;
    }

    public void setColorArr(int[] colorArr) {
        this.colorArr = colorArr;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public int getHealthImg() {
        return healthImg;
    }

    public void setHealthImg(int healthImg) {
        this.healthImg = healthImg;
    }

    public String getUnit2() {
        return unit2;
    }

    public void setUnit2(String unit2) {
        this.unit2 = unit2;
    }

    public int getHealthIcon() {
        return healthIcon;
    }

    public void setHealthIcon(int healthIcon) {
        this.healthIcon = healthIcon;
    }

    public int getColorID() {
        return colorID;
    }

    public void setColorID(int colorID) {
        this.colorID = colorID;
    }

    public double[] getRangeArr2() {
        return rangeArr2;
    }
}
