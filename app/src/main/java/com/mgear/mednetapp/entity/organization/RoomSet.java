package com.mgear.mednetapp.entity.organization;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/23.
 * class: RoomSet
 */
public class RoomSet extends HashMap<UUID, Room> {

    //抽血台
    public static UUID BloodRoom = UUID.fromString("C8625465-2185-4723-B964-641D8B68598F");

    //在診間內的信標
    private HashMap<UUID, Integer> mapBeacon;

    //狀態
    private int status;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status; }

    public RoomSet(int status) {
        this.status = status;
        mapBeacon = new HashMap<>();
    }

    void addBeacon(UUID uuid) {
        //noinspection Java8CollectionsApi
        if (mapBeacon.get(uuid) == null)
            mapBeacon.put(uuid, 0);
    }

    boolean beaconInRoom(UUID uuid) { return mapBeacon.get(uuid) != null; }

}