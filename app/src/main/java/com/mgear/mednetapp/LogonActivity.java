package com.mgear.mednetapp;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;
import com.mgear.mednetapp.entity.organization.Customer;
import com.mgear.mednetapp.enums.StatusEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.organization.MailboxFragment;
import com.mgear.mednetapp.fragments.organization.SurveyFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.CustomerContactCheckTask;
import com.mgear.mednetapp.task.organization.CustomerLogoutTask;
import com.mgear.mednetapp.utils.CommonFunc;

import java.util.List;

/**
 * Created by Jye on 2017/6/8.
 * class: LogonActivity
 */
public class LogonActivity extends AppCompatActivity implements
        View.OnClickListener,
        View.OnKeyListener,
        DialogInterface.OnClickListener,
        BarcodeCallback,
        TaskPostExecuteImpl {

    private int remindSize;
    private boolean mLogout = false;
    private boolean runLogonTask;
    private TextView txtIdentity;
    private CompoundBarcodeView compoundBarcodeView;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.org_logon);
        //判斷目前是否為登出
        Intent intent = getIntent();
        String result = intent.getStringExtra("result");
        if (result != null && result.equals("logout")) {
            mLogout = true;
            //取得提醒的數目
            remindSize = intent.getIntExtra("remind", 0);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.tbLogon);
        toolbar.setTitle(R.string.title_scan);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(this);

        LinearLayout root = (LinearLayout) findViewById(R.id.rootLogon);
        root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(this, R.drawable.bg_empty)));
        Button btnQuery = (Button) findViewById(R.id.btnLogonQuery);
        btnQuery.setOnClickListener(this);
        txtIdentity = (TextView) findViewById(R.id.txtLogonCustomerId);
        txtIdentity.setOnKeyListener(this);

        //設定功能訊息
        if (mLogout) {
            btnQuery.setText(getString(R.string.button_confirm));
            txtIdentity.setHint(getString(R.string.msg_input_logout_code));
        } else {
            btnQuery.setText(getString(R.string.button_query));
            txtIdentity.setHint(getString(R.string.msg_search_customer_id));
        }

        compoundBarcodeView = (CompoundBarcodeView) findViewById(R.id.cbvLogonScanner);
        compoundBarcodeView.decodeContinuous(this);


        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                //關閉小鍵盤
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        compoundBarcodeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        compoundBarcodeView.pause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogonQuery:
                //判斷功能選項
                if (mLogout) {
                    //判斷登出識別碼是否正確
                    if (txtIdentity.getText().toString().equals("16310023")) {
                        //登出系統
//                        new CustomerLogoutTask(this, this, true, getString(R.string.msg_logout))
//                                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                    } else {
                        //顯示失敗訊息
                        new AlertDialog.Builder(this).setCancelable(false)
                                .setTitle(R.string.title_warning)
                                .setMessage(R.string.msg_logout_check_fail)
                                .setPositiveButton(R.string.button_confirm, this).show();
                    }
                } else {
                    //驗證客戶識別碼是否正確
                    new CustomerContactCheckTask(this, this, true, getString(R.string.msg_get_customer))
                            .execute(CultureSelectActivity.BluetoothAddress, txtIdentity.getText().toString().toUpperCase());
                }
                break;
            default:
                //判斷功能選項
                if (mLogout) {
                    //判斷是否有提醒項目
                    if (remindSize == 0)
                        startActivity(new Intent(this, OrgMainActivity.class));
                    else {
                        startActivity(new Intent(this, RemindActivity.class));
                    }

                } else {
                }
                this.finish();
                break;
        }
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        if (!runLogonTask && result.getText() != null) {
            runLogonTask = true;
            txtIdentity.setText(result.getText());
            //判斷功能選項
            if (mLogout) {
                //登出系統
                if (result.getText().equals("16310023")) {
                    new CustomerLogoutTask(this, this, true, getString(R.string.msg_logout))
                            .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                } else {
                    runLogonTask = false;
                }
            } else {
                //登入系統
                new CustomerContactCheckTask(this, this, true, getString(R.string.msg_get_customer))
                        .execute(CultureSelectActivity.BluetoothAddress, txtIdentity.getText().toString());
            }
        }
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        switch (type) {
            case CustomerContactCheckTask:
                Customer customer = (Customer) result;
                //客戶的狀態為結束無法登入
                if (customer.getId() != null && customer.getStatus() != StatusEnum.End.getCode()) {
                    CustomerActivity.setCustomer(customer);
                    startActivity(new Intent(this, CustomerActivity.class));
                    this.finish();
                } else {
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_customer_check_fail)
                            .setPositiveButton(R.string.button_confirm, this).show();
                }
                break;
            case CustomerLogoutTask:
                //清除靜態暫存資源
                CustomerActivity.init();
                SurveyFragment.init();
                MailboxFragment.init();
                //清除登入資訊
                getSharedPreferences("mgear", 0).edit().clear().apply();
                //返回選擇語言的頁面
                this.finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        //登入失敗
        runLogonTask = false;
    }

}