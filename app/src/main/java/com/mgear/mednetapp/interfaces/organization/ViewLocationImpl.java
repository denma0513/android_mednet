package com.mgear.mednetapp.interfaces.organization;

import java.util.UUID;

/**
 * Created by Jye on 2017/8/14.
 * interface: ViewLocationImpl
 */
public interface ViewLocationImpl {

    /**
     * 按下查看地點的事件函數
     * @param position Position
     * @param name Name
     */
    void onViewLocationClick(UUID[] position, String name);

}