package com.mgear.mednetapp.interfaces.organization;

import com.mgear.mednetapp.enums.RemainEnum;

/**
 * Created by Jye on 2017/7/27.
 * interface: RemainFinishImpl
 */
public interface RemainFinishImpl {

    /**
     * 倒數完成的事件處理函數
     */
    void onRemainFinish(RemainEnum index);

}