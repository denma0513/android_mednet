package com.mgear.mednetapp.interfaces.organization;

import com.mgear.mednetapp.entity.organization.CheckableItemSet;
import com.mgear.mednetapp.entity.organization.StringStringSet;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Jye on 2017/7/21.
 * class: AdditionCheckableImpl
 */
public interface AdditionCheckableImpl {

    /**
     * 設定客戶加選清單
     * @param selected Selected
     */
    void setCustomerAddition(CheckableItemSet selected);

    /**
     * 取得客戶加選清單
     * @return HashMap
     */
    HashMap<String, List<StringStringSet>> getCustomerAddition();

}