package com.mgear.mednetapp.interfaces.organization;

/**
 * Created by Jye on 2017/7/29.
 * class: WebPreviousPageImpl
 */
public interface WebPreviousPageImpl {

    /**
     * 返回上一頁
     */
    void previousPage();

    /**
     * 是否可以返回上一頁
     * @return T/F
     */
    boolean hasPreviousPage();

}