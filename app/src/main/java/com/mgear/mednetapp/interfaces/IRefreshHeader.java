package com.mgear.mednetapp.interfaces;

import android.view.View;

public interface IRefreshHeader {
    int STATE_NORMAL = 0;//正常
    int STATE_RELEASE_TO_REFRESH = 1;//下拉
    int STATE_REFRESHING = 2;//正在刷新
    int STATE_DONE = 3;//刷新完成

    void onReset();

    /**
     * 處於可以刷新的狀態，已經過了指定的距離
     */
    void onPrepare();

    /**
     * 正在刷新
     */
    void onRefreshing();

    /**
     * 下拉滑動
     */
    void onMove(float offSet, float sumOffSet);

    /**
     * 下拉放開
     */
    boolean onRelease();

    /**
     * 下拉刷新完成
     */
    void refreshComplete();

    /**
     * 獲取HeaderView
     */
    View getHeaderView();

    /**
     * 獲取Header的顯示高度
     */
    int getVisibleHeight();
}
