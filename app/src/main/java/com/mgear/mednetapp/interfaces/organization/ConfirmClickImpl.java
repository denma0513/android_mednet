package com.mgear.mednetapp.interfaces.organization;

import com.mgear.mednetapp.enums.ConfirmEnum;

/**
 * Created by Jye on 2017/8/26.
 * interface: ConfirmClickImpl
 */
public interface ConfirmClickImpl {

    /**
     * 按下同意按鈕的事件
     * @param type Type
     */
    void onConfirmClick(ConfirmEnum type);

    /**
     * 按下取消按鈕的事件
     * @param type Type
     */
    void onCancelClick(ConfirmEnum type);

}