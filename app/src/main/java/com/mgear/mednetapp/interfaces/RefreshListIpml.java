package com.mgear.mednetapp.interfaces;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public interface RefreshListIpml {
    public void onDrop(Map<String, ArrayList<JSONObject>> list);
}
