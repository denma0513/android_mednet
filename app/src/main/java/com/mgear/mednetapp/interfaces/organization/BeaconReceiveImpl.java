package com.mgear.mednetapp.interfaces.organization;

/**
 * Created by Jye on 2017/9/11.
 * interface: BeaconReceiveImpl
 */
public interface BeaconReceiveImpl {

    /**
     * 當信標訊號已接收
     * @param size Size
     */
    void onBeaconReceiver(int size);

}