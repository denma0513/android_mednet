package com.mgear.mednetapp.interfaces.organization;

/**
 * Created by Dennis on 2019/3/22.
 * class: HomePageImpl
 */
public interface HomePageImpl {

    /**
     * 點擊特定選項
     */
    void onClickItem(int position, int homePosition);

}