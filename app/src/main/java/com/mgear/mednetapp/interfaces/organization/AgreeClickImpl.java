package com.mgear.mednetapp.interfaces.organization;

import com.mgear.mednetapp.enums.AgreeEnum;

/**
 * Created by Jye on 2017/8/26.
 * interface: AgreeClickImpl
 */
public interface AgreeClickImpl {

    /**
     * 按下同意按鈕的事件
     * @param type Type
     */
    void onAgreeClick(AgreeEnum type);

}