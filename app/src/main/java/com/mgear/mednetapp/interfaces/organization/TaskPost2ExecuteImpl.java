package com.mgear.mednetapp.interfaces.organization;

import com.mgear.mednetapp.enums.TaskEnum;

import org.json.JSONObject;


public interface TaskPost2ExecuteImpl {

    /**
     * 工作執行完成的回呼事件處理函數
     *
     * @param type   類型
     * @param result 結果
     */
    void onPostExecute(TaskEnum type, JSONObject result);

}