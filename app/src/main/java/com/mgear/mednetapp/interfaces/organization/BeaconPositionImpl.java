package com.mgear.mednetapp.interfaces.organization;

import com.mgear.mednetapp.entity.organization.BeaconInfo;

/**
 * Created by Jye on 2017/8/15.
 * interface: BeaconPositionImpl
 */
public interface BeaconPositionImpl {

    /**
     * 取得目前的位置
     * @return Position
     */
    BeaconInfo getMyPosition();

    /**
     * 重新載入信標清單
     */
    void reloadBeaconList();

}