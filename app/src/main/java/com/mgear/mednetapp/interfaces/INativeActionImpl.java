package com.mgear.mednetapp.interfaces;

public interface INativeActionImpl {
    public static String WebViewAction = "1"; //開啟webView

    /**
     * 觸發原生點擊事件
     */
    void triggerAction(String action, String target);
}
