package com.mgear.mednetapp.interfaces.organization;

import com.mgear.mednetapp.enums.RemainEnum;

import java.util.UUID;

/**
 * Created by Jye on 2017/7/11.
 * interface: DialogContentImpl
 */
public interface DialogContentImpl {

    /**
     * 顯示中
     */
    void show();

    /**
     * 隱藏中
     */
    void hidden();

    /**
     * 取得標題
     * @return Title
     */
    String getTitle();

    /**
     * 取得訊息
     * @return Message
     */
    String getMessage();

    /**
     * 是否倒數時間
     * @return T/F
     */
    boolean isRemainTime();

    /**
     * 倒數的剩餘時間是否顯示在標題列
     * @return T/F
     */
    boolean showRemainInTitle();

    /**
     * 倒數的剩餘時間是否顯示在訊息列
     * @return T/F
     */
    boolean showRemainInMessage();

    /**
     * 取得倒數的剩餘時間
     * @return Times
     */
    String getRemainTime();

    /**
     * 取得倒數的索引
     * @return Index
     */
    RemainEnum getRemainIndex();

    /**
     * 設定引導的位置
     * @param array Position
     */
    void setGuidePosition(UUID[] array);

    /**
     * 取得引導位置
     * @return PositionArray
     */
    UUID[] getGuidePosition();

    /**
     * 設定引導名稱
     * @param name Name
     */
    void setGuideName(String name);

    /**'
     * 取得引導名稱
     * @return Name
     */
    String getGuideName();

}