package com.mgear.mednetapp.interfaces.organization;

/**
 * Created by Jye on 2017/6/15.
 * interface: PreviousPageImpl
 */
public interface PreviousPageImpl {

    /**
     * 返回上一頁
     */
    void previousPage();

    /**
     * 是否可以返回上一頁
     * @return T/F
     */
    boolean hasPreviousPage();

    /**
     * 取得目前導覽頁面的標題
     * @return Title
     */
    String getNavigationPageTitle();

}