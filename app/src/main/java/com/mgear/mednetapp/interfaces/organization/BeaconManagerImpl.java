package com.mgear.mednetapp.interfaces.organization;

import android.content.Context;

import com.mgear.mednetapp.entity.organization.BeaconInfo;
import com.mgear.mednetapp.entity.organization.BeaconInfoSet;

import java.util.UUID;

/**
 * Created by Jye on 2017/7/18.
 * class: BeaconManagerImpl
 */
public interface BeaconManagerImpl {

    /**
     * 開始接收資料
     * @param context Context
     * @param receiver Receiver
     * @param beaconList Beacons
     * @param scanMillisecond Scan
     * @param betweenMillisecond Between
     * @param parameter Parameter
     * @return T/F
     */
    boolean startScanBeacon(Context context, BeaconReceiveImpl receiver, final BeaconInfoSet beaconList, long scanMillisecond, long betweenMillisecond, long parameter);

    /**
     * 結束接收資料
     */
    void stopScanBeacon();

    /**
     * 重新載入信標清單
     * @param beaconList BeaconList
     */
    void resetBeaconList(final BeaconInfoSet beaconList);

    /**
     * 取得目前所在的信標
     * @param guideTarget Target
     * @return obj
     */
    BeaconInfo getPositionInBeacon(UUID guideTarget);

}