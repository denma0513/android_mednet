package com.mgear.mednetapp.interfaces;


public interface OnRefreshListener {
    void onRefresh();
}
