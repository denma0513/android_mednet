package com.mgear.mednetapp.interfaces.organization;

import com.mgear.mednetapp.enums.TaskEnum;

/**
 * Created by Jye on 2017/6/21.
 * interface: TaskPostExecuteImpl
 */
public interface TaskPostExecuteImpl {

    /**
     * 工作執行完成的回呼事件處理函數
     * @param type 類型
     * @param result 結果
     */
    void onPostExecute(TaskEnum type, Object result);

}