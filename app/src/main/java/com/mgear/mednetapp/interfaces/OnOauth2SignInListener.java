package com.mgear.mednetapp.interfaces;


import com.mgear.mednetapp.entity.OauthSet;

public interface OnOauth2SignInListener {
    void OnSignIn(OauthSet oauth);
}
