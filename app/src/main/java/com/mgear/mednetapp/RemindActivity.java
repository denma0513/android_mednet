package com.mgear.mednetapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mgear.mednetapp.adapter.HtmlListAdapter;
import com.mgear.mednetapp.entity.organization.CheckableItem;
import com.mgear.mednetapp.entity.organization.CustomerReportExamineSet;
import com.mgear.mednetapp.enums.StatusEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.organization.MailboxFragment;
import com.mgear.mednetapp.fragments.organization.SurveyFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.CustomerExamineUpdateTask;
import com.mgear.mednetapp.task.organization.CustomerGiftsDataTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by Jye on 2017/8/14.
 * class: RemindActivity
 */
public class RemindActivity extends AppCompatActivity implements View.OnClickListener, DialogInterface.OnClickListener, TaskPostExecuteImpl {

    private int remindSize;
    private TaskEnum executeTask;
    private ProgressDialog mDialog;

    private TextView txtHelper;
    private ListView lstRemind;
    private Button btnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remind);
        txtHelper = (TextView) findViewById(R.id.txtRemindHelper);
        lstRemind = (ListView) findViewById(R.id.lstRemindContent);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbRemind);
        toolbar.setTitle(R.string.title_reminder);
        setSupportActionBar(toolbar);

        btnConfirm = (Button) findViewById(R.id.btnRemindConfirm);
        btnConfirm.setOnClickListener(this);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(this);

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //顯示進度對話方塊
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.msg_get_remind));
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
        //取得今日健檢人員的檢查項目清單的更新
        new CustomerExamineUpdateTask(this, this, false, null)
                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRemindConfirm:

//                Intent intent = new Intent(this, CultureSelectActivity.class);
//                intent.putExtra("remind", remindSize);
//                intent.putExtra("result", "logout");
//                startActivity(intent);
//                this.finish();
//
//                moveTaskToBack(true);
//                android.os.Process.killProcess(android.os.Process.myPid());

                CustomerActivity.init();
                SurveyFragment.init();
                MailboxFragment.init();
                //清除登入資訊
                getSharedPreferences("mgear", 0).edit().clear().apply();
                //返回選擇語言的頁面
                startActivity(new Intent(this, CultureSelectActivity.class));
                this.finish();

                break;
            default:
                //返回上一頁
                startActivity(new Intent(this, MainActivity.class));
                this.finish();
                break;
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        executeTask = type;
        switch (type) {
            case CustomerExamineUpdateTask:
                CustomerReportExamineSet examineSet = (CustomerReportExamineSet) result;
                if (examineSet.getStatus() == HTTP_OK && examineSet.size() > 0) {
                    CustomerActivity.getCustomer().setExamine(examineSet);
                    //取得今日健檢的贈品
                    new CustomerGiftsDataTask(this, this, false, null)
                            .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_update_examine_status_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case CustomerGiftsDataTask:
                //初始化提醒結果清單
                ArrayList<String> reminds = new ArrayList<>();
                //取得檢查項目與狀態的對應清單
                HashMap<String, Integer> examines = CustomerActivity.getCustomer().getExamineCodeStatusList();

                //取得檢查項目的提醒
                ArrayList<Integer> resRemind = new ArrayList<>();
                for (Map.Entry<String, Integer> entry : examines.entrySet()) {
                    String code = entry.getKey();
                    //判斷是否須歸還健保卡
                    if (code.equals("PA004") && !resRemind.contains(R.string.content_remind_return_card))
                        resRemind.add(R.string.content_remind_return_card);

                    //判斷是否有補做
                    if (entry.getValue() == StatusEnum.Supplement.getCode()) {
                        //判斷是否為糞便或重金屬
                        if (code.equals("SR00") || code.equals("SR003") || code.equals("BR09-1") || code.equals("BR0904") || code.equals("BR0001")) {
                            if (!resRemind.contains(R.string.content_remind_back_mail))
                                resRemind.add(R.string.content_remind_back_mail);
                        }
                        else {
                            if (!resRemind.contains(R.string.content_remind_redo))
                                resRemind.add(R.string.content_remind_redo);
                        }
                    }
                }

                //轉換多語系字串
                for (int i = 0 ; i < resRemind.size(); i++)
                    reminds.add(getString(resRemind.get(i)));

                //計算今日加選的金額
                int money = 0;
                for (CheckableItem entry : CustomerActivity.getCustomerAdditionList().values()) {
                    if (entry.getStatus() == 1) {
                        //判斷加選的項目是否有完成
                        Integer status = examines.get(entry.getCode());
                        if (status!=null && (status == StatusEnum.Released.getCode() || status == StatusEnum.ToBeJudged.getCode() || status == StatusEnum.Supplement.getCode()))
                            money += entry.getNumber();
                    }
                }
                //加入加選金額的提醒
                if (money > 0)
                    reminds.add(String.format(Locale.getDefault(), getString(R.string.content_remind_addition), money));

                //判斷今日是否有贈品
                String gifts = (String) result;
                if (!gifts.equals(""))
                    reminds.add(String.format(Locale.getDefault(), getString(R.string.content_remind_gift), gifts));

                //判斷是否有提醒
                mDialog.dismiss();
                remindSize = reminds.size();
                if (remindSize == 0) {
                    //顯示無提醒的內容
                    txtHelper.setVisibility(View.VISIBLE);
                    lstRemind.setVisibility(View.INVISIBLE);
                    //直接切換到登出的頁面
                    btnConfirm.callOnClick();
                }
                else {
                    //顯示提醒內容
                    String[] content = new String[reminds.size()];
                    HtmlListAdapter adapter = new HtmlListAdapter(this, reminds.toArray(content));
                    lstRemind.setAdapter(adapter);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (executeTask) {
            case CustomerExamineUpdateTask:
                mDialog.show();
                //取得今日健檢人員的檢查項目清單的更新
                new CustomerExamineUpdateTask(this, this, false, null)
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                break;
            default:
                break;
        }
    }

}