package com.mgear.mednetapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.enums.AgreeEnum;
import com.mgear.mednetapp.enums.RemainEnum;
import com.mgear.mednetapp.enums.StatusEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.fragments.organization.AboutFragment;
import com.mgear.mednetapp.fragments.organization.AdditionFragment;
import com.mgear.mednetapp.fragments.organization.CustomerFragment;
import com.mgear.mednetapp.fragments.organization.HomeFragment;
import com.mgear.mednetapp.fragments.organization.MailboxFragment;
import com.mgear.mednetapp.fragments.organization.Map2Fragment;
import com.mgear.mednetapp.fragments.organization.SurveyFragment;
import com.mgear.mednetapp.fragments.organization.TestsFragment;
import com.mgear.mednetapp.fragments.WebViewFragment;
import com.mgear.mednetapp.helpers.BottomNavigationViewHelper;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.organization.AdditionCheckableImpl;
import com.mgear.mednetapp.interfaces.organization.AgreeClickImpl;
import com.mgear.mednetapp.interfaces.organization.BeaconPositionImpl;
import com.mgear.mednetapp.interfaces.organization.BeaconReceiveImpl;
import com.mgear.mednetapp.interfaces.organization.PreviousPageImpl;
import com.mgear.mednetapp.interfaces.organization.RemainFinishImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.interfaces.organization.WebPreviousPageImpl;
import com.mgear.mednetapp.managers.AltBeaconManager;
import com.mgear.mednetapp.entity.organization.AppVersion;
import com.mgear.mednetapp.entity.organization.BeaconInfo;
import com.mgear.mednetapp.entity.organization.BeaconInfoSet;
import com.mgear.mednetapp.entity.organization.BeaconPushSet;
import com.mgear.mednetapp.entity.organization.CheckableItem;
import com.mgear.mednetapp.entity.organization.CheckableItemSet;
import com.mgear.mednetapp.entity.organization.CustomerReportExamineSet;
import com.mgear.mednetapp.entity.organization.Room;
import com.mgear.mednetapp.entity.organization.RoomSet;
import com.mgear.mednetapp.entity.organization.SpotInfo;
import com.mgear.mednetapp.entity.organization.SpotInfoSet;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.services.SignalService;
import com.mgear.mednetapp.task.organization.BeaconDataTask;
import com.mgear.mednetapp.task.organization.CustomerExamineUpdateTask;
import com.mgear.mednetapp.task.organization.CustomerHelpProviderTask;
import com.mgear.mednetapp.task.organization.CustomerInPositionTask;
import com.mgear.mednetapp.task.organization.CustomerTrackRecordTask;
import com.mgear.mednetapp.task.organization.StartScheduleTask;
import com.mgear.mednetapp.task.organization.WaitRemainTimeFinishTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.HttpConnectFunc;
import com.mgear.mednetapp.views.AgreeDialog;
import com.mgear.mednetapp.views.ArticleContent;
import com.mgear.mednetapp.views.BarcodeDialog;
import com.mgear.mednetapp.views.MovementContent;
import com.mgear.mednetapp.views.RemainAlertDialog;
import com.mgear.mednetapp.views.WebViewDialog;
import com.mgear.mednetapp.views.WizardDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static java.net.HttpURLConnection.HTTP_OK;

public class OrgMainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener,
        View.OnKeyListener,
        DialogInterface.OnClickListener,
        TaskPostExecuteImpl,
        AdditionCheckableImpl,
        RemainFinishImpl,
        BeaconPositionImpl,
        BeaconReceiveImpl,
        AgreeClickImpl, IBackHandleIpml {

    public static final int ON_MOVE_ROOM_NOTICE = 1;
    public static final int ON_WAIT_CONFIRM_NOTICE = 2;
    public static final int ON_WAIT_TIMES_FINISH_NOTICE = 3;
    public static final int ON_CALL_ENTER_ROOM_NOTICE = 4;
    public static final int ON_ADDITION_SUCCESS_NOTICE = 5;
    public static final int ON_ROOM_CHECKIN_NOTICE = 6;
    public static final int ON_PROCESS_END_NOTICE = 7;
    public static final int ON_PROCESS_START_NOTICE = 8;
    public static final int ON_PROCESS_PAUSE_NOTICE = 9;
    public static final int ON_SIMULATION_PATH_NOTICE = 100;

    private static int screenWidth;
    private static int screenHeight;

    public static int getScreenWidth() {
        return screenWidth;
    }

    public static int getScreenHeight() {
        return screenHeight;
    }

    public static void setScreenSize(int width, int height) {
        screenWidth = width;
        screenHeight = height;
    }

    public static StringBuilder customerTrack = new StringBuilder(2048);

    public static void addCustomerTrack(String title, int seconds) {
        customerTrack.append(title).append(",").append(String.valueOf(seconds)).append(",");
    }

    private static String clearCustomerTrack() {
        if (customerTrack.length() > 0)
            customerTrack.deleteCharAt(customerTrack.length() - 1);
        String result = customerTrack.toString();
        customerTrack = new StringBuilder();
        return result;
    }

    private TaskEnum executeTask;

    private boolean isStartSchedule;
    private boolean showAdditionIcon;
    private boolean showSupplementIcon;
    private boolean isDoubleRule;
    private String showRoomWait;
    private Room mRoom;
    private UUID targetBeaconID;
    private UUID targetMarkerID;
    private String targetImageUrl = "";
    private List<String> mExamines;

    private Fragment fragmentMain;
    private BeaconInfo myPosition;
    private AltBeaconManager beaconManager;

    private String title;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private TextView txtMessage;
    private BottomNavigationView mBottomNavigation;
    private NavigationView mNavigationView;
    private int nowBottomViewId = 0;
    //診間外的等候秒數
    private int roomWaitSeconds = 0;

    private WizardDialog dialogRange = null;
    private WizardDialog dialogWizard = null;
    private AgreeDialog dialogAgree = null;
    private PreviousPageImpl returnPrevious = null;
    private WebPreviousPageImpl returnWebPrevious = null;

    private Vibrator mVibrator;
    private HavoHandler mHandler = null;
    private HavoBroadcastReceiver mReceiver = null;

    //Services
    private boolean signalBound = false;
    private SignalService signalService = null;
    private ServiceConnection signalConnect = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to SignalRService, cast the IBinder and get SignalRService instance
            SignalService.SignalLocalBinder binder = (SignalService.SignalLocalBinder) service;
            signalService = binder.getService();
            signalService.login(CustomerActivity.getCustomer().getContactCode(), CultureSelectActivity.BluetoothAddress);
            signalBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            signalBound = false;
        }
    };

    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {

    }

    //處理廣播接收
    private class HavoHandler extends Handler {
        private HavoHandler(Looper mainLooper) {
            super(mainLooper);
        }

        @Override
        public void handleMessage(android.os.Message msg) {
            Intent intent = (Intent) msg.obj;
            switch (msg.what) {
                case ON_MOVE_ROOM_NOTICE:
                    //取得診間需等候時間
                    roomWaitSeconds = intent.getIntExtra("seconds", 0);
                    //取得移動的診間
                    mRoom = CustomerActivity.getRoomList().get(UUID.fromString(intent.getStringExtra("room")));
                    //將檢查項目代碼轉換成文字
                    mExamines = CustomerActivity.getExamineList().getExamineName(intent.getStringExtra("examines").split(","));
                    //取得是否為返回診間
                    isDoubleRule = intent.getBooleanExtra("doubles", false);
                    showRoomWait = intent.getStringExtra("showRoomWait");
                    onRoomMoveNotice(intent.getIntExtra("waits", 0));
                    break;
                case ON_WAIT_CONFIRM_NOTICE:
                    onWaitConfirmNotice();
                    break;
                case ON_WAIT_TIMES_FINISH_NOTICE:
                    onWaitTimesFinishNotice();
                    break;
                case ON_CALL_ENTER_ROOM_NOTICE:
                    //取得呼叫的診間
                    mRoom = CustomerActivity.getRoomList().get(UUID.fromString(intent.getStringExtra("room")));
                    //將檢查項目代碼轉換成文字
                    mExamines = CustomerActivity.getExamineList().getExamineName(intent.getStringExtra("examines").split(","));
                    onRoomCallinNotice();
                    break;
                case ON_ADDITION_SUCCESS_NOTICE:
                    onUpdateAdditionItem(UUID.fromString(intent.getStringExtra("addition")), intent.getIntExtra("result", 0));
                    break;
                case ON_ROOM_CHECKIN_NOTICE:
                    //取得檢查中的診間
                    mRoom = CustomerActivity.getRoomList().get(UUID.fromString(intent.getStringExtra("room")));
                    //將檢查項目代碼轉換成文字
                    mExamines = CustomerActivity.getExamineList().getExamineName(intent.getStringExtra("examines").split(","));
                    onRoomCheckinNotice();
                    break;
                case ON_PROCESS_END_NOTICE:
                    onProcessEnd();
                    break;
                case ON_PROCESS_START_NOTICE:
                    onProcessStart();
                    break;
                case ON_PROCESS_PAUSE_NOTICE:
                    onProcessPause();
                    break;
                case ON_SIMULATION_PATH_NOTICE:
                    onSimulationPath(intent.getIntExtra("source", 0), intent.getIntExtra("target", 0));
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }

    //廣播接收
    private class HavoBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == null)
                return;
            switch (intent.getAction()) {
                case SignalService.MOVE_ROOM_NOTICE:
                    mHandler.obtainMessage(ON_MOVE_ROOM_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.WAIT_CONFIRM_NOTICE:
                    mHandler.obtainMessage(ON_WAIT_CONFIRM_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.WAIT_TIMES_FINISH_NOTICE:
                    mHandler.obtainMessage(ON_WAIT_TIMES_FINISH_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.CALL_ENTER_ROOM_NOTICE:
                    mHandler.obtainMessage(ON_CALL_ENTER_ROOM_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.ADDITION_SUCCESS_NOTICE:
                    mHandler.obtainMessage(ON_ADDITION_SUCCESS_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.ROOM_CHECKIN_NOTICE:
                    mHandler.obtainMessage(ON_ROOM_CHECKIN_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.PROCESS_END_NOTICE:
                    mHandler.obtainMessage(ON_PROCESS_END_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.PROCESS_START_NOTICE:
                    mHandler.obtainMessage(ON_PROCESS_START_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.PROCESS_PAUSE_NOTICE:
                    mHandler.obtainMessage(ON_PROCESS_PAUSE_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.SIMULATION_PATH_NOTICE:
                    mHandler.obtainMessage(ON_SIMULATION_PATH_NOTICE, intent).sendToTarget();
                    break;
                case SignalService.PUSH_NOTIFICATION_NOTICE:
                    //有需要轉導畫面
                    String url = intent.getExtras().getString("url");
                    String notificationTitle = intent.getExtras().getString("title");
                    String notificationContent = intent.getExtras().getString("content");

                    if (url != null && !"".equals(url)) {
                        try {
                            new AlertDialog.Builder(OrgMainActivity.this).setCancelable(true)
                                    .setTitle(notificationTitle)
                                    .setMessage(notificationContent)
                                    .setPositiveButton(R.string.button_goto, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            nowBottomViewId = 2;
                                            title = "";
                                            toolbar.setTitle(title);

                                            FragmentManager fragmentManager = getFragmentManager();
                                            WebViewFragment fragment = new WebViewFragment();
                                            fragment.setUrl("promotion", url);
                                            fragmentMain = fragment;
                                            FragmentTransaction ft = fragmentManager.beginTransaction();
                                            ft.replace(R.id.flMainContent, fragmentMain);
                                            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                            ft.commit();
                                        }
                                    }).setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    break;
            }
        }
    }

    /**
     * 放行
     */
    private void onRoomMoveNotice(int waits) {
        isStartSchedule = true;
        //判斷目前是否有診間
        if (mRoom == null) return;
        //移除可進入診間的信標
        targetBeaconID = null;
        //判斷目前是否有顯示精靈對話盒
        if (dialogWizard != null) {
            if (dialogWizard.isShowing())
                dialogWizard.dismiss();
            dialogWizard.dispose();
            dialogWizard = null;
        }
        //震動提醒
        mVibrator.vibrate(800);
        //設定狀態為等候報到
        CustomerActivity.getCustomer().setStatus(StatusEnum.WaitCheckin.getCode());

        //設定地點下拉選單
        if (nowBottomViewId == R.id.action_map) {
            Map2Fragment map = (Map2Fragment) fragmentMain;
            //map.setLocateOption(mRoom.getLocateName());
        }

        //初始化對話方塊內容
        MovementContent mcWait;
        //判斷目前是否有等待時間
        if (waits > 0) {
            //顯示等候時間的對話方塊
            //設定對話方塊內容
            mcWait = new MovementContent(this, getString(R.string.title_room_wait_times));
            mcWait.setReciprocal(RemainEnum.WaitingScheduledTime, waits, false);
            mcWait.setExamine(mExamines);
            mcWait.setLocation(mRoom.getLocateName());
            mcWait.setShowRoomWait(showRoomWait);
            //判斷是否為回診
            if (isDoubleRule)
                mcWait.setExamineFieldText(getString(R.string.field_examine_item_second));
        } else {
            //判斷是否為用餐
            if (mRoom.isMeal()) {
                //設定對話方塊內容
                mcWait = new MovementContent(this, getString(R.string.title_start_dining));
                mcWait.setLocation(mRoom.getLocateName());
                mcWait.setMemo(getString(R.string.msg_meal_hint));
                mcWait.setShowRoomWait(showRoomWait);
            }
            //判斷是否為繳交物品
            else if (mRoom.isHandIn()) {
                //設定對話方塊內容
                mcWait = new MovementContent(this, getString(R.string.title_back_hand_in));
                mcWait.setLocation(mRoom.getLocateName());
                mcWait.setExamine(mExamines);
                mcWait.setShowRoomWait(showRoomWait);
            }
            //一般診間
            else {
                //設定對話方塊內容
                mcWait = new MovementContent(this, getString(R.string.title_room_outside_wait));
                mcWait.setReciprocal(RemainEnum.None, roomWaitSeconds, true, getString(R.string.title_wait_call));
                mcWait.setExamine(mExamines);
                mcWait.setLocation(mRoom.getLocateName());
                mcWait.setShowRoomWait(showRoomWait);
                //判斷是否為回診
                if (isDoubleRule)
                    mcWait.setExamineFieldText(getString(R.string.field_examine_item_second));
            }
        }

        //取得診間的圖標
        if (mRoom.getMarkerId() != null) {
            try {
                SpotInfo si = CustomerActivity.getMarkerList().get(mRoom.getMarkerId());
                if (si != null && !si.getImagePath().equals("")) {
                    //設定圖標的說明圖片
                    targetMarkerID = si.getId();
                    targetImageUrl = si.getImagePath();
                    mcWait.setImage(targetImageUrl);
                }
            } catch (Exception e) {
                Log.e("debug", e.getMessage());
            }

        }

        //設定診間的信標
        mcWait.setGuidePosition(new UUID[]{mRoom.getBeaconId(), targetMarkerID});
        mcWait.setGuideName(mRoom.getLocateName());
        //判斷目前是否顯示地圖
        if (nowBottomViewId == R.id.action_map) {
            Map2Fragment map = (Map2Fragment) fragmentMain;
            //map.clearLocate();
            //map.removeDrawPath();
        }

        //顯示對話方塊
        dialogWizard = new WizardDialog(this, mcWait, false);
        dialogWizard.setMessageControl(this.txtMessage);
        dialogWizard.setNeutralOnClickListener(this::viewLocation);
        dialogWizard.setPositiveOnClickListener(this::viewLocation);
        if (waits > 0) dialogWizard.setOnRemainFinishListener(this);
        dialogWizard.show();
    }

    /**
     * 通知進入診間
     */
    private void onRoomCallinNotice() {
        //判斷目前是否有診間呼叫
        if (mRoom == null || mRoom.isMeal() || mRoom.isHandIn())
            return;
        //判斷目前是否有顯示精靈對話盒
        if (dialogWizard != null) {
            if (dialogWizard.isShowing())
                dialogWizard.dismiss();
            dialogWizard.dispose();
            dialogWizard = null;
        }
        //震動提醒
        mVibrator.vibrate(800);
        //設定狀態為呼叫中
        CustomerActivity.getCustomer().setStatus(StatusEnum.CallEnterRoom.getCode());
        //播放呼叫中的音樂
        CommonFunc.playSound(this, R.raw.event_notice);

        //初始化對話方塊內容
        MovementContent mcCall = new MovementContent(this, getString(R.string.title_enter_room));
        mcCall.setExamine(mExamines);
        mcCall.setLocation(mRoom.getLocateName());
        //判斷是否為回診
        if (isDoubleRule)
            mcCall.setExamineFieldText(getString(R.string.field_examine_item_second));

        //設定診間的圖示
        if (targetImageUrl.equals("")) {
            //取得診間的圖標
            if (mRoom.getMarkerId() != null) {
                try {
                    SpotInfo si = CustomerActivity.getMarkerList().get(mRoom.getMarkerId());
                    if (si != null && !si.getImagePath().equals("")) {
                        //設定圖標的說明圖片
                        targetMarkerID = si.getId();
                        targetImageUrl = si.getImagePath();
                        mcCall.setImage(targetImageUrl);
                    }
                } catch (Exception e) {
                    Log.e("debug", e.getMessage());
                }
            }
        } else mcCall.setImage(targetImageUrl);

        //設定診間的信標
        mcCall.setGuidePosition(new UUID[]{mRoom.getBeaconId(), targetMarkerID});
        mcCall.setGuideName(mRoom.getLocateName());
        //顯示對話方塊
        dialogWizard = new WizardDialog(this, mcCall, false);
        dialogWizard.setMessageControl(this.txtMessage);
        dialogWizard.setNeutralOnClickListener(this::viewLocation);
        dialogWizard.setPositiveOnClickListener(this::viewLocation);
        dialogWizard.show();
    }

    /**
     * 檢查中
     */
    private void onRoomCheckinNotice() {
        //判斷目前是否有診間呼叫
        if (mRoom == null || mRoom.isHandIn())
            return;
        //設定可進入診間的信標
        targetBeaconID = mRoom.getBeaconId();
        //判斷目前是否有顯示精靈對話盒
        if (dialogWizard != null) {
            if (dialogWizard.isShowing())
                dialogWizard.dismiss();
            dialogWizard.dispose();
            dialogWizard = null;
        }
        //震動提醒
        mVibrator.vibrate(800);
        //設定狀態為檢查中
        CustomerActivity.getCustomer().setStatus(StatusEnum.Checkin.getCode());

        //判斷目前是否在餐廳
        MovementContent mcCheck;
        if (mRoom.isMeal()) {
            //設定對話方塊內容
            mcCheck = new MovementContent(this, getString(R.string.title_in_meal));
            mcCheck.setLocation(mRoom.getLocateName());
            //計算用餐結束時間
            Date dtStop = new Date(new Date().getTime() + (1000 * 60 * 20));
            //取得日期格式
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
            mcCheck.setExpansion(getString(R.string.field_meal_end_time), sdf.format(dtStop));
            mcCheck.setMemo(getString(R.string.msg_meal_hint));
        } else {
            //初始化對話方塊內容
            mcCheck = new MovementContent(this, getString(R.string.title_checkin));
            mcCheck.setExamine(mExamines);
            mcCheck.setLocation(mRoom.getLocateName());
            //判斷是否為回診
            if (isDoubleRule)
                mcCheck.setExamineFieldText(getString(R.string.field_examine_item_second));
        }

        //設定診間的圖示
        if (targetImageUrl.equals("")) {
            //取得診間的圖標
            if (mRoom.getMarkerId() != null) {
                SpotInfo si = CustomerActivity.getMarkerList().get(mRoom.getMarkerId());
                if (si != null && !si.getImagePath().equals("")) {
                    //設定圖標的說明圖片
                    targetMarkerID = si.getId();
                    targetImageUrl = si.getImagePath();
                    mcCheck.setImage(targetImageUrl);
                }
            }
        } else mcCheck.setImage(targetImageUrl);

        //設定診間的信標
        mcCheck.setGuidePosition(new UUID[]{mRoom.getBeaconId(), targetMarkerID});
        mcCheck.setGuideName(mRoom.getLocateName());

        //顯示對話方塊
        dialogWizard = new WizardDialog(this, mcCheck, false);
        dialogWizard.setMessageControl(this.txtMessage);
        dialogWizard.setNeutralOnClickListener(this::viewLocation);
        dialogWizard.setPositiveOnClickListener(this::viewLocation);
        dialogWizard.show();
    }

    /**
     * 待總確認
     */
    private void onWaitConfirmNotice() {
        //移除可進入診間的信標
        targetBeaconID = null;
        //判斷目前是否有顯示精靈對話盒
        if (dialogWizard != null) {
            if (dialogWizard.isShowing())
                dialogWizard.dismiss();
            dialogWizard.dispose();
            dialogWizard = null;
        }
        //震動提醒
        mVibrator.vibrate(800);
        //設定狀態為待總確認
        CustomerActivity.getCustomer().setStatus(StatusEnum.WaitConfirm.getCode());

        //初始化待總確認的對話方塊內容
        ArticleContent acConfirm = new ArticleContent(this, getString(R.string.title_wait_confirm));
        acConfirm.setText(getString(R.string.content_total_check));
        //取得抽血台的圖標

        //TODO
        //沒有圖標資料會crash
        mRoom = CustomerActivity.getRoomList().get(RoomSet.BloodRoom);
        try {
            if (mRoom.getMarkerId() != null) {
                SpotInfo si = CustomerActivity.getMarkerList().get(mRoom.getMarkerId());
                if (si != null && !si.getImagePath().equals("")) {
                    //設定圖標的說明圖片
                    targetMarkerID = si.getId();
                    targetImageUrl = si.getImagePath();
                    acConfirm.setImage(targetImageUrl);
                }
            }
            //設定診間的信標
            acConfirm.setGuidePosition(new UUID[]{mRoom.getBeaconId(), targetMarkerID});
            acConfirm.setGuideName(getString(R.string.option_blood_room));
        } catch (Exception e) {
            Log.e("debug", e.getMessage());
        }
        dialogWizard = new WizardDialog(this, acConfirm, false);
        dialogWizard.setMessageControl(this.txtMessage);
        dialogWizard.setPositiveOnClickListener(this::viewLocation);
        dialogWizard.setNeutralOnClickListener(this::showSurvey);
        dialogWizard.show();

        //送出客戶軌跡歷程
        new CustomerTrackRecordTask(this, this, true, null)
                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()), clearCustomerTrack());
    }

    private void onWaitTimesFinishNotice() {
        onRoomMoveNotice(0);
    }

    private void onUpdateAdditionItem(UUID id, int result) {
        CheckableItem ci = CustomerActivity.getCustomerAdditionList().get(id);
        if (ci != null) {
            ci.setStatus((byte) result);
            //判斷是否需更換加選的圖示
            if (result == 1 && !showAdditionIcon) {
                showAdditionIcon = true;
                mBottomNavigation.getMenu().getItem(1).setIcon(R.drawable.icon_tests_add_notice);
            }
        }
    }

    private void showSupplementIcon() {
        CustomerReportExamineSet examines = CustomerActivity.getCustomer().getExamineList();
        if (examines.showSupplementIcon() && !showSupplementIcon) {
            //變更檢查項目的圖示
            showSupplementIcon = true;
            mBottomNavigation.getMenu().getItem(0).setIcon(R.drawable.icon_tests_notice);
        } else if (!examines.showSupplementIcon() && showSupplementIcon) {
            //取消檢查項目的圖示
            showSupplementIcon = false;
            mBottomNavigation.getMenu().getItem(0).setIcon(R.drawable.icon_tests);
        }
    }

    private void onProcessStart() {
        isStartSchedule = true;
        //關閉同意對話盒
        if (dialogAgree != null) {
            if (dialogAgree.isShowing())
                dialogAgree.dismiss();
            dialogAgree = null;
        }
        //關閉精靈對話盒
        if (dialogWizard != null) {
            if (dialogWizard.isShowing())
                dialogWizard.dismiss();
            dialogWizard.dispose();
            dialogWizard = null;
        }
    }


    /**
     * 暫停排程
     * 目前寫死抽血台
     * 需要再依照不同機構做調整
     */
    private void onProcessPause() {
        //判斷目前是否有顯示精靈對話盒
        if (dialogWizard != null) {
            if (dialogWizard.isShowing())
                dialogWizard.dismiss();
            dialogWizard.dispose();
            dialogWizard = null;
        }
        //震動提醒
        mVibrator.vibrate(800);
        //設定狀態為排程暫停
        CustomerActivity.getCustomer().setStatus(StatusEnum.Pause.getCode());

        //初始化排程暫停的對話方塊內容
        ArticleContent acPause = new ArticleContent(this, getString(R.string.title_schedule_pause));

        /*目前寫死抽血台*/
        acPause.setText(getString(R.string.content_process_pause));
        //取得抽血台的圖標
        mRoom = CustomerActivity.getRoomList().get(RoomSet.BloodRoom);
        try {
            //處理圖標例外問宜
            if (mRoom.getMarkerId() != null) {
                SpotInfo si = CustomerActivity.getMarkerList().get(mRoom.getMarkerId());
                if (si != null && !si.getImagePath().equals("")) {
                    //設定圖標的說明圖片
                    targetMarkerID = si.getId();
                    targetImageUrl = si.getImagePath();
                    acPause.setImage(targetImageUrl);
                }
            }
            //設定診間的信標
            acPause.setGuidePosition(new UUID[]{mRoom.getBeaconId(), targetMarkerID});
            acPause.setGuideName(getString(R.string.option_blood_room));
        } catch (Exception e) {
            Log.e("debug", e.getMessage());
        }

        dialogWizard = new WizardDialog(this, acPause, false);
        dialogWizard.setMessageControl(this.txtMessage);
        dialogWizard.setNeutralOnClickListener(this::viewLocation);
        dialogWizard.setPositiveOnClickListener(this::viewLocation);
        dialogWizard.show();
    }

    private void onProcessEnd() {
        //關閉精靈對話盒
        if (dialogWizard != null) {
            if (dialogWizard.isShowing())
                dialogWizard.dismiss();
            dialogWizard.dispose();
            dialogWizard = null;
        }
        //震動提醒
        mVibrator.vibrate(800);
        //設定狀態為排程結束
        CustomerActivity.getCustomer().setStatus(StatusEnum.End.getCode());

        //取得圖標資訊清單
        SpotInfoSet markers = CustomerActivity.getMarkerList();
        boolean bMale = CustomerActivity.getCustomer().getGender() == 'M';
        //顯示更衣內容
        List<View> views = new ArrayList<>();
        ArticleContent ac0 = new ArticleContent(this, getString(R.string.title_locker_off));
        ac0.setText(getString(R.string.content_locker_off));
        ac0.setImage(R.drawable.return_items);
        ac0.setGuidePosition(bMale ?
                new UUID[]{BeaconInfoSet.MaleClothesBeacon, SpotInfoSet.MaleClothesMarker} :
                new UUID[]{BeaconInfoSet.FemaleClothesBeacon, SpotInfoSet.FemaleClothesMarker});
        ac0.setGuideName(bMale ? getString(R.string.option_male_dressing) : getString(R.string.option_female_dressing));
        views.add(ac0);

        //顯示歸還行動裝置內容
        ArticleContent ac1 = new ArticleContent(this, getString(R.string.title_back_phone));
        ac1.setText(getString(R.string.content_back_phone));
        //設定引導位置及說明圖示
        SpotInfo si;
        ac1.setGuidePosition(new UUID[]{BeaconInfoSet.CounterBeacon, SpotInfoSet.CounterMarker});
        ac1.setGuideName(getString(R.string.option_counter));
        si = markers.get(SpotInfoSet.CounterMarker);
        if (si != null) ac1.setImage(si.getImagePath());
        views.add(ac1);

        dialogWizard = new WizardDialog(this, views, false);
        dialogWizard.setMessageControl(this.txtMessage);
        dialogWizard.setPositiveOnClickListener(this::viewLocation);
        dialogWizard.setNeutralOnClickListener(this::showSurvey);
        dialogWizard.show();
    }

    private void onSimulationPath(int source, int target) {
        if (source > 0 && target > 0) {
            BeaconInfoSet beacons = CustomerActivity.getBeaconList();
            //判斷目前是否顯示地圖
            if (nowBottomViewId == R.id.action_map) {
                Map2Fragment map = (Map2Fragment) fragmentMain;
                //map.setLocate(beacons.get(source), beacons.get(target));
            }
        }
    }

    private void setupActionBarDrawer() {
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(false);
            bar.setDisplayShowHomeEnabled(false);
        }

        // 重新設定功能選單
        toolbar.setTitle(title);
        toolbar.setNavigationOnClickListener(null);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.msg_drawer_open, R.string.msg_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //判斷滿意度調查是否已填寫
                if (SurveyFragment.isFinish())
                    mNavigationView.getMenu().getItem(2).setCheckable(true).setChecked(true);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    public void setPreviousPage(PreviousPageImpl previousPage) {
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(this);
            toolbar.setTitle(previousPage.getNavigationPageTitle());
            returnPrevious = previousPage;
        }
    }

    public void setPreviousPage(WebPreviousPageImpl previousPage) {
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(this);
            returnWebPrevious = previousPage;
        }
    }

    public void viewMap() {
        //判斷目前是否顯示地圖
        if (nowBottomViewId != R.id.action_map) {
            mBottomNavigation.setSelectedItemId(R.id.action_map);
        }
    }

    public void viewLocation(UUID[] position, String name) {
        if (position != null && position.length == 2) {
            targetBeaconID = position[0];
            //判斷目前是否顯示地圖
            if (nowBottomViewId != R.id.action_map) {
                mBottomNavigation.setSelectedItemId(R.id.action_map);
            }
            Map2Fragment map = (Map2Fragment) fragmentMain;
            //map.setLocate(position, name);
        }
    }

    public void viewLocation(View v) {
        //判斷目前是否顯示地圖
//        if (nowBottomViewId == R.id.action_map) {
//            DialogContentImpl showContent = (DialogContentImpl) v.getTag();
//            if (showContent != null) {
//                MapFragment map = (MapFragment) fragmentMain;
//                map.setLocate(showContent.getGuidePosition(), showContent.getGuideName());
//            }
//        }
    }

    public void showSurvey(View v) {
        //判斷滿意度是否已填寫完成
        if (!SurveyFragment.isFinish()) {
            //震動提醒
            mVibrator.vibrate(800);
            //顯示滿意度調查表
            onNavigationItemSelected(mNavigationView.getMenu().getItem(2));
            //溫馨提示
            new AlertDialog.Builder(this).setCancelable(false)
                    .setTitle(R.string.title_helpful_tips)
                    .setMessage(R.string.msg_score_hint)
                    .setPositiveButton(R.string.button_confirm, null).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.org_activity_main);
        txtMessage = (TextView) findViewById(R.id.txtMainMessage);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.menu_map);
        setSupportActionBar(toolbar);
        RelativeLayout mainMessage = (RelativeLayout) findViewById(R.id.rlMainMessage);
        mainMessage.setOnClickListener(this);

        //判斷推播
        if (getIntent().getExtras() != null) {
            String notification = getIntent().getExtras().getString("notification");
            String url = getIntent().getExtras().getString("url");
            if ("1".equals(notification)) {
                if (CustomerActivity.getCustomer() == null) {
                    //還沒正確登入
                    Intent intent = new Intent(this, CultureSelectActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("notification", "1");
                    intent.putExtras(bundle);
                    startActivity(intent);
                    this.finish();
                    return;
                }
            }
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.msg_drawer_open, R.string.msg_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //判斷滿意度調查是否已填寫
                if (SurveyFragment.isFinish())
                    mNavigationView.getMenu().getItem(2).setCheckable(true).setChecked(true);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        View headerView = mNavigationView.getHeaderView(0);

        //顯示客戶名稱
        TextView txtCustomerName = (TextView) headerView.findViewById(R.id.txtHeaderCustomerName);
        txtCustomerName.setText(CustomerActivity.getCustomer().getName());

        mBottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(mBottomNavigation);
        mBottomNavigation.setOnNavigationItemSelectedListener(this);
        mBottomNavigation.setSelectedItemId(R.id.action_map);

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //取得震動服務
        mVibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);

        //初始化廣播訊息接收
        this.mHandler = new HavoHandler(getMainLooper());
        this.mReceiver = new HavoBroadcastReceiver();

        //送出客戶軌跡歷程
        new CustomerTrackRecordTask(this, this, true, null)
                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()), clearCustomerTrack());

        //開始播放圖檔動畫
        ImageView imgMainCareAnimate = (ImageView) findViewById(R.id.imgMainCareAnimate);
        imgMainCareAnimate.setImageResource(R.drawable.animation_list_care);
        ((AnimationDrawable) imgMainCareAnimate.getDrawable()).start();

        //判斷是否已加選項目
        CheckableItemSet cis = CustomerActivity.getCustomerAdditionList();
        if (cis.size() > 0) {
            for (CheckableItem ci : cis.values()) {
                if (ci.getStatus() == 1) {
                    //變更加選的圖示
                    showAdditionIcon = true;
                    mBottomNavigation.getMenu().getItem(1).setIcon(R.drawable.icon_tests_add_notice);
                    break;
                }
            }
        }
        //處理補做的檢查項目顯示的圖示
        showSupplementIcon();

//        //啟動SignalR服務接收訊息
//        Intent signalIntent = new Intent();
//        signalIntent.setClass(this, SignalService.class);
//        bindService(signalIntent, signalConnect, Context.BIND_AUTO_CREATE);

        //判斷是否顯示初次登入對話盒
        if (CustomerActivity.getCustomer().getStatus() == StatusEnum.Logon.getCode()) {
            //取得今日會前往的診間
            ArrayList<String> gotoRooms = CustomerActivity.getGotoRoomCodeList(this);
            //取得今日健檢的項目
            HashMap<String, Integer> doExamines = CustomerActivity.getCustomer().getExamineCodeStatusList();
            //取得客戶的性別
            boolean bMale = CustomerActivity.getCustomer().getGender() == 'M';

            //初始化簡報室內容
            List<View> views = new ArrayList<>();

            //觀看手機影片
            ArticleContent ac0 = new ArticleContent(this, getString(R.string.title_present_room));
            ac0.setText(getString(R.string.content_present_room));
            //設定引導位置及說明圖示
            ac0.setGuidePosition(new UUID[]{BeaconInfoSet.BriefingBeacon, SpotInfoSet.BriefingMarker});
            ac0.setGuideName(getString(R.string.option_briefing_room));
            ac0.setImage(R.drawable.video_introduction, "shyhWt4AGpM");
            views.add(ac0);


            //初始化尿液內容
            if (gotoRooms.contains("TP137")) {
                ArticleContent ac1 = new ArticleContent(this, getString(R.string.title_urine_help));
                //判斷性別是否為男
                if (bMale) {
                    //判斷是否有攝護腺超音波
                    ac1.setText(doExamines.get("CS002") == null ? getString(R.string.content_urine) : getString(R.string.content_urine_cs));
                } else ac1.setText(getString(R.string.content_urine_mc));
                ac1.setImage(R.drawable.video_introduction, "XMNqKR6nzd8");
                ac1.setGuidePosition(bMale ?
                        new UUID[]{BeaconInfoSet.MaleToiletBeacon, SpotInfoSet.MaleToiletMarker} :
                        new UUID[]{BeaconInfoSet.FemaleToiletBeacon, SpotInfoSet.FemaleToiletMarker});
                ac1.setGuideName(bMale ? getString(R.string.option_male_toilet) : getString(R.string.option_female_toilet));
                views.add(ac1);
            }


            //初始化更衣室內容
            ArticleContent ac2 = new ArticleContent(this, getString(R.string.title_locker_on));
            String strXRay = "", strColonoscopy = "";
            //判斷是否有X光
            if (gotoRooms.contains("TP104") || gotoRooms.contains("TP127")) {
                strXRay = getString(R.string.content_locker_x_ray);
                ac2.setImage(R.drawable.chest_x_ray);
            }
            //判斷是否有腸鏡
            if (gotoRooms.contains("TP116")) {
                strColonoscopy = getString(R.string.content_locker_colonoscopy);
            }
            ac2.setText(String.format(Locale.getDefault(), getString(R.string.content_locker_on), strXRay, strColonoscopy));
            ac2.setGuidePosition(bMale ?
                    new UUID[]{BeaconInfoSet.MaleClothesBeacon, SpotInfoSet.MaleClothesMarker} :
                    new UUID[]{BeaconInfoSet.FemaleClothesBeacon, SpotInfoSet.FemaleClothesMarker});
            ac2.setGuideName(bMale ? getString(R.string.option_male_dressing) : getString(R.string.option_female_dressing));
            //設定倒數計時 暫時是設定成一秒
            ac2.setReciprocal(RemainEnum.WaitingChangeClothes, 360, true);
            views.add(ac2);


            /**/
            //貼心小叮嚀
            //判斷是否有攝護腺
            boolean bProstate = doExamines.containsKey("CS002") || doExamines.containsKey("BR220");
            //判斷是否有尿液
            boolean bUrine = doExamines.containsKey("SR003") || doExamines.containsKey("SR001") || doExamines.containsKey("SR00") ||
                    doExamines.containsKey("UR2018001") || doExamines.containsKey("FUB-PE003") || doExamines.containsKey("UR901") ||
                    doExamines.containsKey("UR00") || doExamines.containsKey("FUB-PE005") || doExamines.containsKey("FUB-BR601") ||
                    doExamines.containsKey("##_A0008") || doExamines.containsKey("UR014");
            //初始化貼心頁面
            ArticleContent ac3 = new ArticleContent(this, getString(R.string.title_intimate));
            String intimateText = getString(R.string.content_intimate);
            //處理客製化訊息的顯示
            if (bMale) {
                //男性
                if (bProstate)
                    intimateText += getString(R.string.content_intimate_male);
                if (!bProstate && bUrine)
                    intimateText += getString(R.string.content_intimate_urine);
            } else {
                //女性
                if (bUrine)
                    intimateText += getString(R.string.content_intimate_female);
            }
            ac3.setText(intimateText);
            ac3.setGuidePosition(bMale ?
                    new UUID[]{BeaconInfoSet.MaleClothesBeacon, SpotInfoSet.MaleClothesMarker} :
                    new UUID[]{BeaconInfoSet.FemaleClothesBeacon, SpotInfoSet.FemaleClothesMarker});
            ac3.setGuideName(bMale ? getString(R.string.option_male_dressing) : getString(R.string.option_female_dressing));
            //設定倒數計時
            //ac3.setReciprocal(RemainEnum.WaitingChangeClothes, 360, true);
            views.add(ac3);


            dialogWizard = new WizardDialog(this, views, false);
            dialogWizard.setMessageControl(this.txtMessage);
            //按下查看地點
            dialogWizard.setPositiveOnClickListener(this::viewLocation);
            dialogWizard.setOnRemainFinishListener(this);
            dialogWizard.setNeutralButtonStatus(getString(R.string.button_clothes_finish), false);
            dialogWizard.show();
        }

        //取消功能表圖示的色調
        mBottomNavigation.setItemIconTintList(null);

        //判斷推播
        if (getIntent().getExtras() != null) {
            String notification = getIntent().getExtras().getString("notification");
            String url = getIntent().getExtras().getString("url");
            if ("1".equals(notification)) {
                //有需要轉導畫面
                if (url != null && !"".equals(url)) {
                    try {
                        nowBottomViewId = 2;
                        title = "";
                        toolbar.setTitle(title);
                        FragmentManager fragmentManager = getFragmentManager();
                        WebViewFragment fragment = new WebViewFragment();
                        fragment.setUrl("promotion", url);
                        fragmentMain = fragment;
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        ft.replace(R.id.flMainContent, fragmentMain);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                    } catch (Exception e) {
                        Log.i("debug", e.getStackTrace().toString());
                        e.printStackTrace();
                    }
                }

            }
        }

        MegaApplication.ORG_ISOPEN = true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        //判斷使用者的狀態
        StatusEnum status = StatusEnum.valueOf(CustomerActivity.getCustomer().getStatus());
        if (status == null) return;
        switch (status) {
            case WaitCheckin:
                if (txtMessage.getText().length() == 0)
                    txtMessage.setText(String.format(Locale.getDefault(), "【%s】", getString(R.string.title_room_outside_wait)));
                break;
            case Checkin:
            case Released:
            case Temporarily:
            case ToBeJudged:
            case Supplement:
            case Reject:
                if (txtMessage.getText().length() == 0)
                    txtMessage.setText(String.format(Locale.getDefault(), "【%s】", getString(R.string.title_checkin)));
                break;
            case CallEnterRoom:
                if (txtMessage.getText().length() == 0)
                    txtMessage.setText(String.format(Locale.getDefault(), "【%s】", getString(R.string.title_enter_room)));
                break;
            case WaitDoubleInRoom:
            case WaitingTime:
                if (txtMessage.getText().length() == 0)
                    txtMessage.setText(String.format(Locale.getDefault(), "【%s】", getString(R.string.title_room_wait_times)));
                break;
            case Alarm:
            case Pause:
                if (txtMessage.getText().length() == 0)
                    txtMessage.setText(String.format(Locale.getDefault(), "【%s】", getString(R.string.title_process_pause)));
                break;
            case WaitConfirm:
                if (txtMessage.getText().length() == 0)
                    txtMessage.setText(String.format(Locale.getDefault(), "【%s】", getString(R.string.title_wait_confirm)));
                break;
            case End:
                if (txtMessage.getText().length() == 0)
                    txtMessage.setText(String.format(Locale.getDefault(), "【%s】", getString(R.string.title_process_end)));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //啟動SignalR服務接收訊息
        Intent signalIntent = new Intent();
        signalIntent.setClass(this, SignalService.class);
        bindService(signalIntent, signalConnect, Context.BIND_AUTO_CREATE);
        this.signalBound = true;

        //註冊廣播接收的訊息
        IntentFilter filter = new IntentFilter();
        filter.addAction(SignalService.MOVE_ROOM_NOTICE);
        filter.addAction(SignalService.WAIT_CONFIRM_NOTICE);
        filter.addAction(SignalService.WAIT_TIMES_FINISH_NOTICE);
        filter.addAction(SignalService.CALL_ENTER_ROOM_NOTICE);
        filter.addAction(SignalService.ADDITION_SUCCESS_NOTICE);
        filter.addAction(SignalService.ROOM_CHECKIN_NOTICE);
        filter.addAction(SignalService.PROCESS_END_NOTICE);
        filter.addAction(SignalService.PROCESS_START_NOTICE);
        filter.addAction(SignalService.PROCESS_PAUSE_NOTICE);
        filter.addAction(SignalService.SIMULATION_PATH_NOTICE);
        filter.addAction(SignalService.PUSH_NOTIFICATION_NOTICE);
        registerReceiver(mReceiver, filter);
        //開始接收信標訊號
        AppVersion av = CultureSelectActivity.ApplicationVersion;
        beaconManager = AltBeaconManager.getInstance();
        beaconManager.startScanBeacon(this, this, CustomerActivity.getBeaconList(), 1, 1, 1);
    }

    @Override
    public void onBeaconReceiver(int size) {
        //取得信標位置
        BeaconInfo beaconInfo = beaconManager.getPositionInBeacon(targetBeaconID);
        if (beaconInfo != null) {
            runOnUiThread(() -> {
                //每次超出範圍都提醒震動及音樂
                if (beaconInfo.getMinor() == 1) {
                    //震動提醒
                    mVibrator.vibrate(2200);
                    //播放音樂
                    CommonFunc.playSound(this, R.raw.piano_urgent);
                }

                //判斷目前位置是否變更
                if (myPosition == null || myPosition.getKey() != beaconInfo.getKey()) {
                    //判斷是否超出範圍
                    if (beaconInfo.getMinor() == 1) {
                        //提示超出範圍警告訊息
                        ArticleContent range = new ArticleContent(this, getString(R.string.title_warning));
                        range.setText(getString(R.string.content_range_warning));
                        //設定說明圖示
                        SpotInfo entrance = CustomerActivity.getMarkerList().get(SpotInfoSet.EntranceMarker);
                        if (entrance != null && !entrance.getImagePath().equals(""))
                            range.setImage(entrance.getImagePath());
                        dialogRange = new WizardDialog(this, range, false);
                        dialogRange.setNeutralButtonVisible(false);
                        dialogRange.show();
                    } else {
                        //判斷是否關閉超出範圍訊息
                        if (dialogRange != null) {
                            dialogRange.dismiss();
                            dialogRange = null;
                            //停止音樂撥放
                            CommonFunc.playSound(this, 0);
                        }
                    }
                    //判斷是否推撥訊息
                    BeaconPushSet pushSet = CustomerActivity.getBeaconPushList();
                    List<String> contents = pushSet.get(beaconInfo.getId());
                    if (contents != null && contents.size() > 0) {
                        //移除推撥訊息
                        pushSet.remove(beaconInfo.getId());
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < contents.size(); i++)
                            sb.append(contents.get(i));

                        //震動提醒
                        mVibrator.vibrate(800);
                        WebViewDialog dialogWebView = new WebViewDialog(this, sb.toString());
                        dialogWebView.show();
                    }

                    //通知伺服器客戶最新的位置
                    new CustomerInPositionTask(this, this, false, null).execute(
                            CultureSelectActivity.BluetoothAddress,
                            String.valueOf(CustomerActivity.getCustomer().getContactCode()),
                            beaconInfo.getId().toString(), String.valueOf(beaconInfo.getPosLeft()), String.valueOf(beaconInfo.getPosTop()));
                }

                //紀錄目前的位置
                myPosition = beaconInfo;
                //判斷目前是否顯示地圖
                if (nowBottomViewId == R.id.action_map) {
                    Map2Fragment map = (Map2Fragment) fragmentMain;
                    //map.setMyPosition(beaconInfo, size);
                }
            });
        }
    }

    @Override
    protected void onPause() {

        super.onPause();
        unregisterReceiver(mReceiver);
        beaconManager.stopScanBeacon();
        if (this.signalBound) {
            signalService.logout();
            unbindService(signalConnect);
            this.signalBound = false;
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        // Unbind from the service
        if (this.signalBound) {
            signalService.logout();
            unbindService(signalConnect);
            this.signalBound = false;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_help:
                RemainAlertDialog remainDialog = new RemainAlertDialog(this, getString(R.string.title_warning), getString(R.string.msg_call_help), 10);
                remainDialog.setOnRemainFinishListener(this);
                remainDialog.show();
                return true;
            case R.id.action_barcode:
                new BarcodeDialog(this).show();
                return true;
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        // is loaded fragment
        if (id == nowBottomViewId) return true;

        // is sub fragment not return
        if (returnPrevious != null || returnWebPrevious != null) {
            returnPrevious = null;
            returnWebPrevious = null;
            setupActionBarDrawer();
        }

        FragmentManager fragmentManager = getFragmentManager();
        switch (id) {
            case R.id.nav_person:
                nowBottomViewId = id;
                title = getString(R.string.menu_personal);
                toolbar.setTitle(title);
                try {
                    fragmentMain = new CustomerFragment();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.flMainContent, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


            case R.id.nav_health:
                nowBottomViewId = id;
                title = getString(R.string.menu_healthy);
                toolbar.setTitle(title);
                try {
                    WebViewFragment webFragment = new WebViewFragment();
                    webFragment.setTextZoom(1.2f);
                    webFragment.setUrl("question_link", CustomerActivity.getCustomer().getQuestionLink());
                    fragmentMain = webFragment;
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.flMainContent, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.nav_satisfies:
                nowBottomViewId = id;
                title = getString(R.string.menu_satisfies);
                toolbar.setTitle(title);
                try {
                    fragmentMain = new SurveyFragment();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.flMainContent, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.nav_mail:
                nowBottomViewId = id;
                title = getString(R.string.menu_mail);
                toolbar.setTitle(title);
                try {
                    fragmentMain = new MailboxFragment();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.flMainContent, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.nav_about:
                nowBottomViewId = id;
                title = getString(R.string.menu_about);
                toolbar.setTitle(title);
                try {
                    fragmentMain = new AboutFragment();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.flMainContent, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.nav_mednet:
                Intent mainIntent = new Intent(this, MainActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(mainIntent);
                break;
            case R.id.nav_help:
                Intent intent = new Intent(this, TutorialActivity.class);
                intent.putExtra("result", "main");
                startActivityForResult(intent, 1);
                break;
            case R.id.nav_logout:
                //判斷目前的狀態是否可登出

//                if (CustomerActivity.getCustomer().getStatus() == StatusEnum.End.getCode()) {
//                    //顯示溫馨提示頁
                startActivity(new Intent(this, RemindActivity.class));
                this.finish();
//                } else {
//                    //排程未結束，無法登出
//                    View view = fragmentMain.getView();
//                    if (view != null)
//                        Snackbar.make(view, R.string.msg_not_logout, Snackbar.LENGTH_LONG).show();
//                }
                break;
            case R.id.action_map:
                nowBottomViewId = id;
                title = getString(R.string.menu_map);
                toolbar.setTitle(title);
                try {
                    fragmentMain = new Map2Fragment();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.flMainContent, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.action_promotional:
                nowBottomViewId = id;
                title = getString(R.string.menu_promotional);
                toolbar.setTitle(title);
                try {
                    WebViewFragment fragment = new WebViewFragment();
                    fragment.setUrl("promotion", HttpConnectFunc.CONNECT_URL + "Announce/AnnounceList?type=0DC703C7-C704-4C16-B81A-C652FF7A6212");
                    fragmentMain = fragment;
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.flMainContent, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.action_home:
                nowBottomViewId = id;
                title = getString(R.string.menu_home);
                toolbar.setTitle(title);
                try {
                    fragmentMain = new HomeFragment();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.flMainContent, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.action_addition:
                nowBottomViewId = id;
                //取得今日健檢人員的檢查項目清單的更新
                new CustomerExamineUpdateTask(this, this, true, getString(R.string.msg_customer_examine_update))
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                return true;
            case R.id.action_tests:
                nowBottomViewId = id;
                //取得今日健檢人員的檢查項目清單的更新
                new CustomerExamineUpdateTask(this, this, true, getString(R.string.msg_customer_examine_update))
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlMainMessage:
                if (dialogWizard != null)
                    dialogWizard.show();
                else if (dialogAgree != null)
                    dialogAgree.show();
                break;
            default:
                //判斷網頁是否按下返回上一頁
                if (returnWebPrevious != null) {
                    returnWebPrevious.previousPage();
                    if (!returnWebPrevious.hasPreviousPage()) {
                        returnWebPrevious = null;
                        //判斷一般頁面是否可回上一頁
                        if (returnPrevious == null)
                            setupActionBarDrawer();
                    }
                }
                //判斷一般頁面是否按下返回上一頁
                else if (returnPrevious != null) {
                    returnPrevious.previousPage();
                    if (returnPrevious.hasPreviousPage()) {
                        toolbar.setTitle(returnPrevious.getNavigationPageTitle());
                    } else {
                        returnPrevious = null;
                        setupActionBarDrawer();
                    }
                }
                break;
        }
    }

    @Override
    public void onRemainFinish(RemainEnum index) {
        switch (index) {
            //更衣倒數完成
            case WaitingChangeClothes:
                //判斷目前的客戶狀態，只有為登入狀態才能顯示同意確認對話盒
                if (CustomerActivity.getCustomer().getStatus() == StatusEnum.Logon.getCode()) {
                    //將客戶的狀態變更為等候報到
                    CustomerActivity.getCustomer().setStatus(StatusEnum.WaitCheckin.getCode());
                    //判斷目前是否有顯示精靈對話盒
                    if (dialogWizard != null) {
                        if (dialogWizard.isShowing())
                            dialogWizard.dismiss();
                        dialogWizard.dispose();
                        dialogWizard = null;
                    }
                    //顯示開始排程前的同意事項
                    CheckableItem[] checkableItems = new CheckableItem[2];
                    checkableItems[0] = new CheckableItem(getString(R.string.msg_prepare_confirm));
                    checkableItems[1] = new CheckableItem(getString(R.string.msg_prepare_change));
                    //移除 第三項 健診包項目
                    //checkableItems[2] = new CheckableItem(getString(R.string.msg_prepare_come_out));
                    String title = getString(R.string.title_schedule_confirm);
                    txtMessage.setText(title);
                    dialogAgree = new AgreeDialog(this, AgreeEnum.ScheduleConfirm, title, getString(R.string.msg_prepare_help), checkableItems, false);
                    dialogAgree.setPositiveOnClickListener(this);
                    dialogAgree.setNegativeButtonVisible(false);
                    dialogAgree.show();
                }
                break;
            //排程等待時間完成
            case WaitingScheduledTime:
                //重新開始排程
                new WaitRemainTimeFinishTask(this, this, true, getString(R.string.msg_restart_schedule))
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                break;
            //要求協助等候時間完成
            case HelpProvider:
                //傳送客戶請求協助
                new CustomerHelpProviderTask(this, this, true, getString(R.string.msg_send_help))
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                break;
            default:
                break;
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        executeTask = type;
        switch (type) {
            case StartScheduleTask:
                if (!((boolean) result || isStartSchedule)) {
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_start_schedule_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case BeaconDataTask:
                BeaconInfoSet beaconList = (BeaconInfoSet) result;
                if (beaconList.getStatus() == HTTP_OK) {
                    CustomerActivity.setBeaconList(beaconList);
                    beaconManager.resetBeaconList(beaconList);
                }
                break;
            case WaitRemainTimeFinishTask:
                if (!(boolean) result) {
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_restart_schedule_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case CustomerHelpProviderTask:
                if (!(boolean) result) {
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_send_help_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case CustomerExamineUpdateTask:
                CustomerReportExamineSet examineSet = (CustomerReportExamineSet) result;
                if (examineSet.getStatus() == HTTP_OK && examineSet.size() > 0) {
                    CustomerActivity.getCustomer().setExamine(examineSet);
                    //處理補做的檢查項目顯示的圖示
                    showSupplementIcon();
                }
                //判斷選取的功能
                switch (nowBottomViewId) {
                    case R.id.action_addition:
                        title = getString(R.string.menu_addition);
                        toolbar.setTitle(title);
                        try {
                            fragmentMain = new AdditionFragment();
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.replace(R.id.flMainContent, fragmentMain);
                            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                            ft.commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.action_tests:
                        title = getString(R.string.menu_tests);
                        toolbar.setTitle(title);
                        try {
                            fragmentMain = new TestsFragment();
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.replace(R.id.flMainContent, fragmentMain);
                            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                            ft.commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                //關閉小鍵盤
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    if (imm != null)
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (executeTask) {
            case StartScheduleTask:
                //通知開始進行排程
                new StartScheduleTask(this, this, true, getString(R.string.msg_start_schedule))
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                break;
            case WaitRemainTimeFinishTask:
                //通知等候時間完成，重新開始排程
                new WaitRemainTimeFinishTask(this, this, true, getString(R.string.msg_restart_schedule))
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                break;
            case CustomerHelpProviderTask:
                //傳送客戶請求協助
                new CustomerHelpProviderTask(this, this, true, getString(R.string.msg_send_help))
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
                break;
            default:
                break;
        }
    }

    @Override
    public void onAgreeClick(AgreeEnum type) {
        if (type == AgreeEnum.ScheduleConfirm) {
            dialogAgree = null;
            //通知開始進行排程
            new StartScheduleTask(this, this, true, getString(R.string.msg_start_schedule))
                    .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
        }
    }

    @Override
    public void setCustomerAddition(CheckableItemSet selected) {
        if (selected != null && selected.size() > 0)
            CustomerActivity.setCustomerAddition(selected);
    }

    @Override
    public HashMap<String, List<StringStringSet>> getCustomerAddition() {
        //計算加選金額
        int money = 0;
        List<StringStringSet> finishSet = new ArrayList<>();
        List<StringStringSet> waitSet = new ArrayList<>();
        List<StringStringSet> failSet = new ArrayList<>();
        for (CheckableItem entry : CustomerActivity.getCustomerAdditionList().values()) {
            switch (entry.getStatus()) {
                case 0:
                    waitSet.add(new StringStringSet(entry.getName(), entry.getSubName(), entry.getMemo()));
                    break;
                case 1:
                    money += entry.getNumber();
                    finishSet.add(new StringStringSet(entry.getName(), entry.getSubName(), entry.getMemo()));
                    break;
                default:
                    failSet.add(new StringStringSet(entry.getName(), entry.getSubName(), entry.getMemo()));
                    break;
            }
        }

        //初始化回傳結果
        HashMap<String, List<StringStringSet>> result = new HashMap<>();
        if (finishSet.size() > 0)
            result.put(String.format(Locale.getDefault(), getString(R.string.title_addition_finish), money), finishSet);
        if (waitSet.size() > 0)
            result.put(getString(R.string.title_addition_wait), waitSet);
        if (failSet.size() > 0)
            result.put(getString(R.string.title_addition_fail), failSet);
        return result;
    }

    @Override
    public BeaconInfo getMyPosition() {
        return myPosition;
    }

    @Override
    public void reloadBeaconList() {
        //重新載入信標資料
        new BeaconDataTask(this, this, false, getString(R.string.msg_reload_beacon_data)).execute(CultureSelectActivity.BluetoothAddress);
    }

}