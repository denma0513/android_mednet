package com.mgear.mednetapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mgear.mednetapp.entity.organization.Organization;
import com.mgear.mednetapp.enums.CultureEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.AppCheckUpgrade2Task;
import com.mgear.mednetapp.task.organization.OrganizationCheckTask;
import com.mgear.mednetapp.task.organization.OrganizationListTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.HttpConnectFunc;
import com.mgear.mednetapp.entity.organization.AppVersion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

//import static com.mgear.mednetapp.services.DownloadAppService.APK_DOWNLOAD_URL;

/**
 * Created by Jye on 2017/6/8.
 * class: CultureSelectActivity
 */
public class CultureSelectActivity extends AppCompatActivity implements
        View.OnClickListener, TaskPostExecuteImpl, DialogInterface.OnClickListener {

    public static AppVersion ApplicationVersion;
    public static String BluetoothAddress;
    public static String AppVersion;
    public static CultureEnum SelectCulture;
    public static String TAG = "debug";

    public CultureSelectActivity contex;
    private String appPath;
    private int appVersion = 100;
    private TaskEnum executeTask;

    private Button btnChinese;
    private Button btnEnglish;
    private AlertDialog.Builder organizationSel;
    private JSONArray organizationList = new JSONArray();
    private ArrayList<JSONObject> orgChList = new ArrayList<JSONObject>();
    private ArrayList<JSONObject> orgEnList = new ArrayList<JSONObject>();
    private AlertDialog mutiItemDialog;

    private ExpandableListView orgList;
    private RelativeLayout dialogView;
    private TextView closeView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_culture_select);
        FrameLayout root = (FrameLayout) findViewById(R.id.rootCultureSelect);
        root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(this, R.drawable.bg_cover)));
        contex = this;
        btnChinese = (Button) findViewById(R.id.btnCultureChinese);
        btnChinese.setOnClickListener(this);
        btnChinese.setVisibility(View.INVISIBLE);
        btnEnglish = (Button) findViewById(R.id.btnCultureEnglish);
        btnEnglish.setOnClickListener(this);
        btnEnglish.setVisibility(View.INVISIBLE);

        orgList = findViewById(R.id.org_list);
        dialogView = findViewById(R.id.dialog_view);
        closeView = findViewById(R.id.close_view);
        dialogView.setVisibility(View.GONE);

        closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogView.setVisibility(View.GONE);
            }
        });

//        try {
//            Intent intent = new Intent(CultureSelectActivity.this, TutorialStartAppActivity.class);
//            CultureSelectActivity.this.startActivity(intent);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        //取得應用程式的版號
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            AppVersion = packageInfo.versionName;
//            int[] ary = CommonFunc.convertToIntArray(AppVersion, "\\.");
//            if (ary != null && ary.length == 3) {
//                appVersion = ary[0] * 100 + ary[1] * 10 + ary[2];
//            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //取得螢幕視窗的大小
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);
        OrgMainActivity.setScreenSize(screenSize.x, screenSize.y);

        //取得藍芽的MAC地址
        /*
        BluetoothAddress = android.provider.Settings.Secure.getString(getContentResolver(), "bluetooth_address");
        if (BluetoothAddress == null) {
            BluetoothAddress = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
        }
        if (BluetoothAddress == null || BluetoothAddress.isEmpty()) {
            BluetoothAddress = this.randomString();
        }
        */

        //取得push token
        registerPushToken();

        /**
         * 授權相機
         * */
        String permission = Manifest.permission.CAMERA;
        //檢查項是否已經授權
        //int hasPermission = checkSelfPermission(permission);
        int hasPermission = ContextCompat.checkSelfPermission(this, permission);
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            //請求授權
            requestPermissions(new String[]{permission}, 100);
        }

        //取得機構列表
        new OrganizationListTask(this, this, true, getString(R.string.msg_organization_check)).execute(AppVersion);
    }

    public void registerPushToken() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.cnannel_ID);
            String channelName = getString(R.string.cnannel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_HIGH));
        }

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful() || task.getResult() == null) {
                    Log.i(TAG, "get token fail ");
                    return;
                }
                BluetoothAddress = task.getResult().getToken();
                Log.i(TAG, "token = " + BluetoothAddress);
            }
        });
    }

    public String randomString() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(16);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    @Override
    public void onClick(View v) {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();

        //切換語系
        ArrayList<JSONObject> list = null;
        switch (v.getId()) {
            case R.id.btnCultureEnglish:
                SelectCulture = CultureEnum.English;
                conf.setLocale(Locale.ENGLISH);
                //noinspection deprecation
                res.updateConfiguration(conf, dm);
                //list =  orgEnList.toArray();
                list = orgEnList;
                break;
            case R.id.btnCultureChinese:
                SelectCulture = CultureEnum.TraditionalChinese;
                conf.setLocale(Locale.TAIWAN);
                //noinspection deprecation
                res.updateConfiguration(conf, dm);
                list = orgChList;
                break;
        }


        //mutiItemDialog = getMutiItemDialog(list);
        OrgListAdapter orgListAdapter = new OrgListAdapter(list);
        orgList.setAdapter(orgListAdapter);
        dialogView.setVisibility(View.VISIBLE);
        checkLogin();

    }

    private void checkLogin() {
        try {
            //判斷系統是否已經登入
            SharedPreferences settings = getSharedPreferences("mgear", 0);
            String contact = settings.getString("contact", "");
            String orgUrl = settings.getString("orgUrl", "");
            String orgID = settings.getString("orgID", "");
            String orgMap = settings.getString("orgMap", "");
            String organizationName = settings.getString("organization", "");
            String organizationNameEn = settings.getString("organizationNameEn", "");
            String pushToken = BluetoothAddress;
            String ratingUrl = settings.getString("ratingUrl", "");
            int today = settings.getInt("today", 0);
            if (!contact.equals("") && (Calendar.getInstance().get(Calendar.DAY_OF_YEAR)) == today && !"".equals(orgUrl)) {
                HttpConnectFunc.CONNECT_URL = orgUrl + "/";
                HttpConnectFunc.organizationID = UUID.fromString(orgID);
                CommonFunc.orgMap = orgMap;
                CommonFunc.ratingUrl = ratingUrl;
                CommonFunc.pushToken = pushToken;
                CommonFunc.organization = organizationName;
                CommonFunc.organizationEn = organizationNameEn;
                //直接登入系統
                Intent intent = new Intent(CultureSelectActivity.this, CustomerActivity.class);
                intent.putExtra("contact", contact);
                CultureSelectActivity.this.startActivity(intent);
                //CultureSelectActivity.this.finish();
            } else {
                //AlertDialog mutiItemDialog = getMutiItemDialog(list);
                if (mutiItemDialog != null) {
                    mutiItemDialog.setTitle(R.string.title_select_server);
                    mutiItemDialog.show();
                }
            }
        } catch (Exception e) {
            Log.i(TAG, "e = " + e.getStackTrace());
            e.printStackTrace();
        }
    }


    public void chooseOrg(int i, int j) {
        //當使用者點選對話框時，顯示使用者所點選的項目
        JSONObject area = organizationList.optJSONObject(i);
        JSONArray list = area.optJSONArray("data");
        JSONObject organization = list.optJSONObject(j);
        SharedPreferences settings = getSharedPreferences("mgear", 0);
        settings.edit().putString("orgUrl", organization.optString("urlPrifix"))
                .putString("orgID", organization.optString("orgID"))
                .putString("orgMap", organization.optString("orgMap"))
                .putString("ratingUrl", organization.optString("ratingUrl"))
                .putString("organization", organization.optString("Name"))
                .putString("organizationEn", organization.optString("NameEn"))
                .apply();
        HttpConnectFunc.CONNECT_URL = organization.optString("urlPrifix") + "/";
        HttpConnectFunc.organizationID = UUID.fromString(organization.optString("orgID"));
        CommonFunc.orgMap = organization.optString("orgMap");
        CommonFunc.ratingUrl = organization.optString("ratingUrl");
        CommonFunc.pushToken = BluetoothAddress;
        CommonFunc.organization = organization.optString("Name");
        CommonFunc.organizationEn = organization.optString("NameEn");
//
//        //驗證機構是否存在
        new OrganizationCheckTask(contex, contex, true, getString(R.string.msg_organization_check)).execute(BluetoothAddress);
    }

    public AlertDialog getMutiItemDialog(final String[] items) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //設定對話框內的項目
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //當使用者點選對話框時，顯示使用者所點選的項目
                JSONObject organization = organizationList.optJSONObject(which);
                SharedPreferences settings = getSharedPreferences("mgear", 0);
                settings.edit().putString("orgUrl", organization.optString("urlPrifix"))
                        .putString("orgID", organization.optString("orgID"))
                        .putString("orgMap", organization.optString("orgMap"))
                        .putString("ratingUrl", organization.optString("ratingUrl"))
                        .putString("organization", organization.optString("Name"))
                        .putString("organizationEn", organization.optString("NameEn"))
                        .apply();
                HttpConnectFunc.CONNECT_URL = organization.optString("urlPrifix") + "/";
                HttpConnectFunc.organizationID = UUID.fromString(organization.optString("orgID"));
                CommonFunc.orgMap = organization.optString("orgMap");
                CommonFunc.ratingUrl = organization.optString("ratingUrl");
                CommonFunc.pushToken = BluetoothAddress;
                CommonFunc.organization = organization.optString("Name");
                CommonFunc.organizationEn = organization.optString("NameEn");

                //驗證機構是否存在
                new OrganizationCheckTask(contex, contex, true, getString(R.string.msg_organization_check)).execute(BluetoothAddress);

            }
        });
        return builder.create();
    }


    /**
     * 進到下一步
     * 進入隱私權條款說明
     */
    private void doNext() {
        startActivity(new Intent(this, AppClauseActivity.class));
        //this.finish();
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        executeTask = type;
        switch (type) {
            case OrganizationCheckTask:
                Organization org = (Organization) result;
                //判斷機構是否存在
                if (org.getId() == null) {
                    //機構驗證失敗-顯示訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.msg_organization_check_failed)
                            .setMessage(R.string.msg_cannot_connect)
                            .setPositiveButton(R.string.button_confirm, this).show();
                } else {
                    doNext();
                }
                break;
            case AppCheckUpgradeTask:
                JSONObject av = (JSONObject) result;
                //判斷應用程式的狀態
                //Log.i("debug", "av + " + av);

                if (!av.isNull("data") && av.optJSONObject("data").optBoolean("result")) {
                    //顯示按鈕
                    btnChinese.setVisibility(View.VISIBLE);
                    btnEnglish.setVisibility(View.VISIBLE);

                } else {
                    String newVersion = av.optJSONObject("data").optString("newVersion");
                    appPath = av.optJSONObject("data").optString("url");
                    if (!"".equals(newVersion)) {
                        new AlertDialog.Builder(this)
                                .setTitle(R.string.msg_new_version)
                                .setMessage(String.format(Locale.getDefault(), getString(R.string.msg_app_version), newVersion))
                                .setPositiveButton(R.string.button_setup, this).show();
                    }
                }

                break;
            case OrganizationListTask:
                JSONObject orgData = (JSONObject) result;
                //判斷機構列表是否存在
                if (orgData == null || orgData.isNull("data") || orgData.optJSONArray("data").length() <= 0) {
                    //機構列表取得失敗-顯示訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.msg_organization_check_failed)
                            .setMessage(R.string.msg_cannot_connect)
                            .setPositiveButton(R.string.button_retry, this).show();
                } else {
                    organizationList = orgData.optJSONArray("data");

                    try {

                        for (int i = 0; i < organizationList.length(); i++) {

                            JSONObject organizationData = organizationList.optJSONObject(i);
                            String locateName = organizationData.optString("locateName");
                            String locateNameEn = organizationData.optString("locateNameEn");
                            JSONArray data = organizationData.optJSONArray("data");
                            ArrayList<String> nameList = new ArrayList<String>();
                            ArrayList<String> nameEnList = new ArrayList<String>();
                            for (int j = 0; j < data.length(); j++) {
                                JSONObject dataObj = data.optJSONObject(j);
                                String name = dataObj.optString("Name");
                                String nameEn = dataObj.optString("NameEn");
                                nameList.add(name);
                                nameEnList.add(nameEn);
                            }
                            JSONObject tempData = new JSONObject();
                            tempData.put("locate", locateName);
                            tempData.put("data", nameList);
                            orgChList.add(tempData);
                            tempData = new JSONObject();
                            tempData.put("locate", locateNameEn);
                            tempData.put("data", nameEnList);
                            orgEnList.add(tempData);
                        }
                        //取得應用程式更新訊息
                        new AppCheckUpgrade2Task(this, this, true, getString(R.string.msg_app_check_update)).execute(AppVersion);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (executeTask) {
            case OrganizationListTask:
                //機構列表是否存在
                new OrganizationListTask(this, this, true, getString(R.string.msg_organization_check)).execute(AppVersion);
                break;
            case OrganizationCheckTask:
                //驗證機構是否存在
                //new OrganizationCheckTask(this, this, true, getString(R.string.msg_organization_check)).execute(BluetoothAddress);
                break;
            case AppCheckUpgradeTask:
                //判斷是否下載應用程式
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.mgear.mednetapp"));
                if (appPath == null) {
                    //取得應用程式更新訊息
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.mgear.mednetapp"));
                } else {
                    intent.setData(Uri.parse(appPath));
                }
                startActivity(intent);
                break;
            default:
                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        //取得應用程式更新訊息
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            new AppCheckUpgrade2Task(this, this, true, getString(R.string.msg_app_check_update)).execute(AppVersion);
        } else {
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    class OrgListAdapter extends BaseExpandableListAdapter {

        private final ArrayList<JSONObject> dataList;

        public OrgListAdapter(ArrayList<JSONObject> list) {
            dataList = list;
        }

        @Override
        public int getGroupCount() {
            return dataList.size();
        }

        @Override
        public int getChildrenCount(int i) {
            JSONObject item = dataList.get(i);
            ArrayList<String> childItem = (ArrayList<String>) item.opt("data");

            return childItem.size();
        }

        @Override
        public Object getGroup(int i) {
            JSONObject item = dataList.get(i);
            return item;
        }

        @Override
        public Object getChild(int i, int j) {
            JSONObject item = dataList.get(i);
            ArrayList<String> childItem = (ArrayList<String>) item.opt("data");
            return childItem.get(j);
        }

        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int i, int j) {
            return j;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
            JSONObject item = dataList.get(i);

            convertView = LayoutInflater.from(CultureSelectActivity.this).inflate(R.layout.cell_group, null);
            TextView groupTitle = convertView.findViewById(R.id.group_title);
            groupTitle.setText(item.optString("locate"));

            return convertView;
        }

        @Override
        public View getChildView(int i, int j, boolean b, View convertView, ViewGroup viewGroup) {
            String item = (String) getChild(i, j);
            convertView = LayoutInflater.from(CultureSelectActivity.this).inflate(R.layout.cell_child, null);
            TextView groupTitle = convertView.findViewById(R.id.child_title);
            groupTitle.setText(item);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chooseOrg(i, j);
                }
            });

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int i, int j) {

            return false;
        }
    }

}