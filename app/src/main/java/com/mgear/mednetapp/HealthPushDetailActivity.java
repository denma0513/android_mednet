package com.mgear.mednetapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataType;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.DrawerMenuAdapter;
import com.mgear.mednetapp.fragments.GoogleFitFragment;
import com.mgear.mednetapp.fragments.HealthDetailFragment;
import com.mgear.mednetapp.fragments.HealthListFragment;
import com.mgear.mednetapp.fragments.base.MainTopBottomFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.helpers.BottomNavigationViewHelper;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.OnItemClickListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONException;
import org.json.JSONObject;

public class HealthPushDetailActivity extends BaseActivity implements View.OnClickListener, TaskPost2ExecuteImpl, INativeActionImpl, IBackHandleIpml, OnItemClickListener {

    private static String TAG = "HealthDetail";
    private static final String actionType = MegaApplication.ActionTypeHealthPush;
    private static String fitType = "";
    private static int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 6;

    //畫面元件
    private DrawerLayout drawerLayout;
    private ExpandableListView mMenuListView;
    private BottomNavigationView mBottomNavigation, mBottomNavigationDr;
    private NavigationView menuLayout;
    private AppBarLayout mTopBar;
    private Toolbar toolBar;

    private ImageView mMemberPic, mTopBack;
    private RelativeLayout mSpecialTop;
    private LinearLayout mSpecialBottomLayout, mSpecialHomeBottom;
    private TextView mTopTitle, mTopRightBtn;
    private Button mBottomButton;


    //private MainTopBottomFragment mTopFagment;

    //邏輯元件
    private MegaBaseFragment selectedFragment;
    private int index;
    private String healthType;
    private JSONObject healthData;
    private HealthDetailFragment healthDetailFragment;

    public JSONObject getHealthData() {
        return healthData;
    }

    public int getIndex() {
        return index;
    }

    public String getHealthType() {
        return healthType;
    }

    private void setHealthDetailFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        healthDetailFragment = new HealthDetailFragment();
        //fragmentMain = healthListFragment;

        //fragmentMain.setHealthData(healthData);
        if (healthDetailFragment.isAdded()) { // 如果 home fragment 已經被 add 過，
            ft.show(healthDetailFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.main_content, healthDetailFragment, "HEALTHDETAIL"); // 使用 add 方法。
            ft.addToBackStack("HEALTHDETAIL");
        }
        ft.commit();
    }

    public void initView() {
        mTopBar.setVisibility(View.GONE);
        mSpecialTop.setVisibility(View.VISIBLE);
        mSpecialBottomLayout.setVisibility(View.VISIBLE);
        mBottomNavigation.setVisibility(View.GONE);
        mBottomNavigationDr.setVisibility(View.GONE);

        //mSpecialBottom.setOnNavigationItemSelectedListener(this);
        //BottomNavigationViewHelper.disableShiftMode(mSpecialBottom);
        //mSpecialBottom.setSelectedItemId(R.id.mainPage);

        switch (healthType) {
            case "1":
                mTopTitle.setText("血糖");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "2":
                mTopTitle.setText("血壓");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "3":
                mTopTitle.setText("血氧");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "4":
                mTopTitle.setText("飲水");
                //mTopRightBtn.setVisibility(View.GONE);
                mTopRightBtn.setText("目標設定");
                mBottomButton.setText("新增紀錄");
                break;
            case "5":
                mTopTitle.setText("睡眠時間");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "6":
                mTopTitle.setText("每日步數");
                mTopRightBtn.setText("目標設定");
                mBottomButton.setText("自動配對");
                if (checkAppInstalled(this, "com.google.android.apps.fitness")) {

                    FitnessOptions fitnessOptions = FitnessOptions.builder()
                            .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                            .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                            .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                            .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                            .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                            .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_READ)
                            .build();
                    //判斷是否有授權
                    if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
                        mBottomButton.setText("允許取用 google Fit");
                        fitType = "permission";
                    } else {
                        mBottomButton.setText("已自動配對");
                        mBottomButton.setTextColor(getResources().getColor(R.color.home_title_black_deep));
                        mBottomButton.setBackgroundColor(getResources().getColor(R.color.home_bg));
                        mBottomButton.setEnabled(false);
                    }

                } else {
                    mBottomButton.setText("請安裝 google Fit");
                    fitType = "install";
                }
                break;
            case "7":
                mTopTitle.setText("體位");
                mTopRightBtn.setText("身高設定");
                mBottomButton.setText("新增紀錄");
                break;
        }
        setHealthDetailFragment();
    }

    private boolean checkAppInstalled(Context context, String pkgName) {
        if (pkgName == null || pkgName.isEmpty()) {
            return false;
        }
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(pkgName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    public void setSleepTimeSub(Double val) {
        healthDetailFragment.setSleepTimeSub(val);
    }

    public void exit() {
        drawerLayout.closeDrawers();
        //Log.i("debug", "getFragmentManager().getBackStackEntryCount()  = " + getFragmentManager().getBackStackEntryCount());
        FragmentManager fragmentManager = getFragmentManager();
        //Log.i("debug", "fragmentManager = " + fragmentManager.getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1).getName());
        if (selectedFragment == null || !selectedFragment.onBackPressed()) {
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                FragmentManager.BackStackEntry fragment = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1);
                if ("HEALTHDETAIL".equals(fragment.getName())) {
                    this.finish();
                }
                getFragmentManager().popBackStack();
            }
        }
    }

    public void onPageSelected(int i) {
        switch (i) {
            case 0:
                if ("6".equals(healthType) || "7".equals(healthType))
                    mTopRightBtn.setVisibility(View.VISIBLE);
                break;
            case 1:
                mTopRightBtn.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        index = extras.getInt("index");

        healthData = CommonFunc.parseJSON(extras.getString("data"));
        healthType = healthData.optString("healthType");

        Log.i("debug", "healthType = " + healthType);
        //Log.i("debug", "healthData = " + healthData);

        setContentView(R.layout.activity_health_detail);

        //一般top
        mTopBar = findViewById(R.id.topBar);
        toolBar = findViewById(R.id.toolbar);
        mMemberPic = findViewById(R.id.main_pic);
        mBottomNavigation = findViewById(R.id.main_bottom);
        mBottomNavigationDr = findViewById(R.id.main_bottom_dr);

        //Special top
        mSpecialTop = findViewById(R.id.special_top);
        mTopBack = findViewById(R.id.top_back);
        mTopTitle = findViewById(R.id.top_title);
        mTopRightBtn = findViewById(R.id.top_right_btn);
        mSpecialBottomLayout = findViewById(R.id.special_bottom_layout);
        mSpecialHomeBottom = findViewById(R.id.special_home_bottom);
        mBottomButton = findViewById(R.id.bottom_button);

        mBottomButton.setOnClickListener(this);
        mTopBack.setOnClickListener(this);
        mTopRightBtn.setOnClickListener(this);
        mSpecialHomeBottom.setOnClickListener(this);

        drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);//關閉左側選單
        mMenuListView = findViewById(R.id.menu_list_view);
        initView();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bottom_button:
                Intent intent = new Intent(this, HealthPushCreateActivity.class);

                if ("6".equals(healthType)) {
                    if ("permission".equals(fitType)) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        GoogleFitFragment googleFitFragment = new GoogleFitFragment();
                        ft.replace(R.id.extra_view, googleFitFragment);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                        googleFitFragment.triggerPermission(this);

                    } else if ("install".equals(fitType)) {
                        triggerAction(this, "2", "https://play.google.com/store/apps/details?id=com.google.android.apps.fitness&hl=zh_TW", R.id.extra_view);
                    }
                    return;
                }

                Bundle bundle = new Bundle();
                bundle.putInt("index", index);
                bundle.putString("healthType", healthData.optString("healthType"));
                intent.putExtras(bundle);
                startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeHealthPushCreate));
                break;
            case R.id.special_home_bottom:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.top_back:
                exit();
            case R.id.top_right_btn:
                healthDetailFragment.openPick();
                break;
        }
    }

    @Override
    public void onItemClick(View view, int position, JSONObject data) {
        Intent intent = new Intent(this, HealthPushCreateActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index", position);
        bundle.putString("healthType", data.optString("healthType"));
        bundle.putString("data", data.toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeHealthPushCreate));

    }

    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {

    }

    @Override
    public void triggerAction(String action, String target) {
        //Log.i("debug", "triggerAction");
        //triggerAction(action, target, R.id.main_content);
        drawerLayout.closeDrawers();
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("debug", "requestCode = " + requestCode);
        if (requestCode == Integer.parseInt(MegaApplication.ActionTypeHealthPushCreate) && data != null) {
            String saveData = data.getStringExtra("save");
            //Log.i("debug", "saveData = " + saveData);
            if ("success".equals(saveData)) {
                //healthDetailFragment.refreshData();
                Intent intent = new Intent();
                intent.putExtra("save", "success");
                this.setResult(RESULT_OK, intent);
                this.finish();
            }

        } else if (GOOGLE_FIT_PERMISSIONS_REQUEST_CODE == requestCode && resultCode == Activity.RESULT_OK) {

            if (checkAppInstalled(this, "com.google.android.apps.fitness")) {

                FitnessOptions fitnessOptions = FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_READ)
                        .build();
                //判斷是否有授權
                if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
                    mBottomButton.setText("允許取用 google Fit");
                    fitType = "permission";
                } else {
                    mBottomButton.setText("已自動配對");
                    mBottomButton.setTextColor(getResources().getColor(R.color.home_title_black_deep));
                    mBottomButton.setBackgroundColor(getResources().getColor(R.color.home_bg));
                    mBottomButton.setEnabled(false);
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        exit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
    }


}
