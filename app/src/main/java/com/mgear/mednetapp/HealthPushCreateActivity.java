package com.mgear.mednetapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.entity.health.HealthBaseSet;
import com.mgear.mednetapp.entity.health.HealthBloodOxygenSet;
import com.mgear.mednetapp.entity.health.HealthBloodPressureSet;
import com.mgear.mednetapp.entity.health.HealthBloodSugarSet;
import com.mgear.mednetapp.entity.health.HealthBodySet;
import com.mgear.mednetapp.entity.health.HealthDrinkingSet;
import com.mgear.mednetapp.entity.health.HealthSleepingSet;
import com.mgear.mednetapp.entity.health.HealthStepSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.WaveView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HealthPushCreateActivity extends BaseActivity implements View.OnClickListener, TaskPost2ExecuteImpl, INativeActionImpl,
        IBackHandleIpml, NumberPicker.OnValueChangeListener {

    private static String TAG = "HealthDetail";

    //畫面元件
    private TextView mTopTitle, mTopRightBtn;
    private ImageView mTopBack;

    private RelativeLayout mCreatePickerLayout;
    private LinearLayout mFirstLayout, mSecondLayout, mMemoLayout, mCreateDateLayout, mCreateTimeLayout;
    private EditText mFirstValue, mSecondValue, mDateValue, mTimeValue, mMemoValue;
    private TextView mFirstTitle, mSecondTitle, mMemoTitle, mCupCount, mCupUnit, mFirstUnit, mSecondUnit;
    private ImageView mCreateIcon, mFirstIcon, mSecondIcon, mMemoIcon;
    private TextView mCreatePickerCheck, mCreatePickerCancel, mCreatePickerTitle;
    private NumberPicker mCreatePicker, mCreatePicker2, mCreatePicker3, mCreatePicker4;


    //健康entity
    HealthBaseSet baseSet;


    //邏輯元件
    private MegaBaseFragment selectedFragment;
    private int index;
    private JSONObject healthData;
    private WaveView mCupWave;
    private String healthType;
    private String[] pickValueArr = null, hourArr = null, minuteArr = null, monthArr = null, dayArr = null;
    private String pickValue, pickValue2, pickValue3, pickValue4, pickType;
    private int cupNum = 0, oneCup = 250, nowCapacity = 0, nowCup, totleCup, cupHight;
    private float plusRange;
    private int sugerFirstValue;//血糖預設值是飯前或飯後

    {
        hourArr = new String[24];
        minuteArr = new String[60];
        monthArr = new String[12];
        dayArr = new String[31];

        for (int i = 0; i < 24; i++) {
            hourArr[i] = i >= 10 ? "" + i : "0" + i;
        }
        for (int i = 0; i < 60; i++) {
            minuteArr[i] = i >= 10 ? "" + i : "0" + i;
        }
        for (int i = 0; i < 12; i++) {
            monthArr[i] = (i + 1) >= 10 ? "" + (i + 1) : "0" + (i + 1);
        }
        for (int i = 0; i < 31; i++) {
            dayArr[i] = (i + 1) >= 10 ? "" + (i + 1) : "0" + (i + 1);
        }

    }


    public int getIndex() {
        return index;
    }

    public void exit() {
        this.finish();
    }

    /**
     * 儲存資料
     */
    private void saveData() {
        JSONObject sendObj = new JSONObject();
        String mTime = CommonFunc.getUTCDateString(mDateValue.getText().toString() + " " + mTimeValue.getText().toString());

        try {
            //判斷未來時間
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date checkMTime = simpleDateFormat.parse(mDateValue.getText().toString() + " " + mTimeValue.getText().toString());
            cal1.setTime(checkMTime);
            cal2.setTime(new Date());
            if (cal1.compareTo(cal2) == 1) {
                CommonFunc.showToast(this, "不可記錄未來時間");
                return;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        boolean isUpdate = false;
        String id = "", measureID = "";
        if (baseSet != null) {
            isUpdate = baseSet.getId() > 0 && !"".equals(baseSet.getMeasureID());
            id = Integer.toString(baseSet.getId());
            measureID = baseSet.getMeasureID();
        }
        Log.d("debug", "existData : id = " + id + " isUpdate = " + isUpdate + " measureID = " + measureID);
        switch (healthType) {
            case "1":
                if ("".equals(mFirstValue.getText().toString()) || "".equals(mSecondValue.getText().toString())) {
                    CommonFunc.showToast(this, "數值不可空白");
                    return;
                }

                if (isUpdate) {
                    new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                            , TaskEnum.MednetUpdateBloodSugar, true)
                            .execute(mFirstValue.getText().toString(), mSecondValue.getText().toString(), mTime, id, measureID);
                } else {
                    new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                            , TaskEnum.MednetCreateBloodSugar, true)
                            .execute(mFirstValue.getText().toString(), mSecondValue.getText().toString(), mTime);
                }

                break;
            case "2":
                if ("".equals(mFirstValue.getText().toString()) || "".equals(mSecondValue.getText().toString())) {
                    CommonFunc.showToast(this, "數值不可空白");
                    return;
                }
                String[] pressure = mFirstValue.getText().toString().split("/");
                String medication = "無".equals(mMemoValue.getText().toString()) ? "0" : "1";
                if (isUpdate) {
                    new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                            , TaskEnum.MednetUpdateBloodPressure, true)
                            .execute(pressure[0], pressure[1], mSecondValue.getText().toString(), medication,
                                    mTime, id, measureID);
                } else {
                    new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                            , TaskEnum.MednetCreateBloodPressure, true)
                            .execute(pressure[0], pressure[1], mSecondValue.getText().toString(), medication,
                                    mTime);
                }
                break;
            case "3":
                if ("".equals(mFirstValue.getText().toString())) {
                    CommonFunc.showToast(this, "數值不可空白");
                    return;
                }

                Log.d("debug", "saveData: isUpdate = " + isUpdate);
                if (isUpdate) {
                    new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                            , TaskEnum.MednetUpdateBloodOxygen, true)
                            .execute(mFirstValue.getText().toString(), mMemoValue.getText().toString(), mTime, id, measureID);
                } else {
                    new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                            , TaskEnum.MednetCreateBloodOxygen, true)
                            .execute(mFirstValue.getText().toString(), mMemoValue.getText().toString(), mTime);
                }
                break;
            case "4":
                new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                        , TaskEnum.MednetCreateDrinkingCreateOrUpdate, true)
                        .execute(mFirstValue.getText().toString(), mTime);
                break;
            case "5":
                if ("".equals(mFirstValue.getText().toString()) || "".equals(mSecondValue.getText().toString())) {
                    CommonFunc.showToast(this, "數值不可空白");
                    return;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                int hour1 = Integer.parseInt(mFirstValue.getText().toString().split(":")[0]);
                int hour2 = Integer.parseInt(mSecondValue.getText().toString().split(":")[0]);
                int min1 = Integer.parseInt(mFirstValue.getText().toString().split(":")[1]);
                int min2 = Integer.parseInt(mSecondValue.getText().toString().split(":")[1]);

                String getupTiem = CommonFunc.getUTCDateString(sdf.format(new Date()) + " " + mSecondValue.getText().toString());
                String sleepTiem = "";
                if (hour1 > hour2) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.add(Calendar.DATE, -1);
                    sleepTiem = CommonFunc.getUTCDateString(sdf.format(cal.getTime()) + " " + mFirstValue.getText().toString());
                } else {
                    sleepTiem = CommonFunc.getUTCDateString(sdf.format(new Date()) + " " + mFirstValue.getText().toString());
                }

                //計算時數
                if (min2 < min1) {
                    min2 += 60;
                    hour2 -= 1;
                }
                if (hour2 < hour1) {
                    hour2 += 24;
                }
                float sleepingHours = (hour2 - hour1) + ((min2 - min1) / 60.0f);
                //Log.i("debug", "hour2= = " + hour2 + " hour2 = " + hour2);
                //Log.i("debug", "min2= = " + min2 + " min1 = " + min1);
                //Log.i("debug", "sleepingHours= = " + sleepingHours);
                new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                        , TaskEnum.MednetSleepingCreateOrUpdate, true)
                        .execute(sleepTiem, getupTiem, sleepingHours + "", mTime);
                break;
            case "6":
                MegaApplication.TargetStep = Integer.parseInt(mFirstValue.getText().toString());
                Intent intent = new Intent();
                intent.putExtra("save", "success");
                this.setResult(RESULT_OK, intent);
                this.finish();
//                new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
//                        , TaskEnum.MednetCreateStepCreateOrUpdate, true)
//                        .execute("0", "0", mFirstValue.getText().toString(), mTime);
                break;
            case "7":
                if ("".equals(mFirstValue.getText().toString()) || "".equals(mSecondValue.getText().toString())) {
                    CommonFunc.showToast(this, "數值不可空白");
                    return;
                }
                Double bh = Double.valueOf(mFirstValue.getText().toString());
                Double bw = Double.valueOf(mSecondValue.getText().toString());
                Double bmi = bw / ((bh / 100) * (bh / 100));

                MegaApplication.BodyHeight = bh + "";
                SharedPreferences settings = getSharedPreferences("HealthSettingNotice", Activity.MODE_PRIVATE);
                settings.edit().putString("BodyHeight", MegaApplication.BodyHeight).apply();

                //Log.i("debug", MegaApplication.BodyHeight);
                if (isUpdate) {
                    new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                            , TaskEnum.MednetUpdateBodyMassWeight, true)
                            .execute(mFirstValue.getText().toString(), mSecondValue.getText().toString(), bmi.toString(), mMemoValue.getText().toString(), mTime, id, measureID);
                    break;
                } else {
                    new CustomerApiTask(this, this, true, getString(R.string.msg_data_saving)
                            , TaskEnum.MednetCreateBodyMassWeight, true)
                            .execute(mFirstValue.getText().toString(), mSecondValue.getText().toString(), bmi.toString(), mMemoValue.getText().toString(), mTime);
                    break;
                }
        }

    }

    private void initDateEdit(String date) {
        Calendar cal = Calendar.getInstance();
        if ("".equals(CommonFunc.getString(date))) {
            mDateValue.setText(cal.get(Calendar.YEAR) + "/" + monthArr[(cal.get(Calendar.MONTH))] + "/" + dayArr[cal.get(Calendar.DAY_OF_MONTH) - 1]);
            mTimeValue.setText(hourArr[cal.get(Calendar.HOUR_OF_DAY)] + ":" + minuteArr[(cal.get(Calendar.MINUTE))]);
        } else {
            Date mdate = CommonFunc.UTC2Date(date);
            mDateValue.setText(mdate.getYear() + "/" + monthArr[mdate.getMonth()] + "/" + dayArr[mdate.getDate() - 1]);
            mTimeValue.setText(hourArr[mdate.getHours() - 1] + ":" + minuteArr[mdate.getMinutes() - 1]);
        }
    }

    private void initData() {
        initDateEdit(null);
        boolean existData = true;
        if (!healthData.has("m_time")) {
            //沒有帶任何資料進來
            Log.i("debug", "沒有帶任何資料進來");
            existData = false;
        }
        //Log.i("debug", "existData = " + existData + "healthType = " + healthType + "\n healthData = " + healthData);
        Calendar cal = Calendar.getInstance();
        switch (healthType) {
            case "1":
                if (existData) {
                    HealthBloodSugarSet bloodSugarSet = new HealthBloodSugarSet(this, healthData);
                    baseSet = bloodSugarSet;
                    mFirstValue.setText(bloodSugarSet.getValue());
                    mSecondValue.setText(bloodSugarSet.getValue2());
                    Date mTime = CommonFunc.UTC2Date(bloodSugarSet.getmTime());
                    cal.setTime(mTime);
                    mDateValue.setText(cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE));
                    mTimeValue.setText(cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE));
                } else {
//                    mFirstValue.setText("80");
//                    mSecondValue.setText("飯前");
                }
                break;
            case "2":
                if (existData) {
                    HealthBloodPressureSet pressureSet = new HealthBloodPressureSet(this, healthData);
                    baseSet = pressureSet;
                    mFirstValue.setText(pressureSet.getValue());
                    mSecondValue.setText(Integer.toString(pressureSet.getHr()));
                    mMemoValue.setText(pressureSet.getMedication() == 1 ? "有" : "無");
                    Date mTime = CommonFunc.UTC2Date(pressureSet.getmTime());
                    cal.setTime(mTime);
                    mDateValue.setText(cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE));
                    mTimeValue.setText(cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE));
                } else {
//                    mFirstValue.setText("110/70");
//                    mSecondValue.setText("70");
                    mMemoValue.setText("無");
                }
                break;
            case "3":
                if (existData) {
                    HealthBloodOxygenSet oxygenSet = new HealthBloodOxygenSet(this, healthData);
                    baseSet = oxygenSet;
                    mFirstValue.setText(oxygenSet.getValue());
                    Date mTime = CommonFunc.UTC2Date(oxygenSet.getmTime());
                    cal.setTime(mTime);
                    mDateValue.setText(cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE));
                    mTimeValue.setText(cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE));
                } else {
//                    mFirstValue.setText("95");
                }
                break;
            case "4":
                //Log.i("debug", "existData298 = " + existData);
                if (existData) {
                    HealthDrinkingSet drinkingSet = new HealthDrinkingSet(this, healthData);
                    baseSet = drinkingSet;
                    mFirstValue.setText(drinkingSet.getValue());
                    nowCapacity = Integer.parseInt(drinkingSet.getValue()); //現在已喝的水量
                    Date mTime = CommonFunc.UTC2Date(drinkingSet.getmTime());
                    cal.setTime(mTime);
                    mDateValue.setText(cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE));
                    mTimeValue.setText(cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE));
                } else {
                    nowCapacity = MegaApplication.TodayWaterCapacity; //現在已喝的水量
                    //Log.i("debug", "MegaApplication.TodayWaterCapacity = " + MegaApplication.TodayWaterCapacity);
                    mFirstValue.setText(Integer.toString(nowCapacity));
                }
                //Log.i("debug", "nowCapacity = " + nowCapacity);
                break;
            case "5":
                if (existData) {
                    HealthSleepingSet sleepingSet = new HealthSleepingSet(this, healthData);
                    baseSet = sleepingSet;
                    cal = Calendar.getInstance();
                    cal.setTime(CommonFunc.UTC2Date(sleepingSet.getSleepTime()));
                    mFirstValue.setText(cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
                    cal.setTime(CommonFunc.UTC2Date(sleepingSet.getGetupTime()));
                    mSecondValue.setText(cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
                    Date mTime = CommonFunc.UTC2Date(sleepingSet.getmTime());
                    cal.setTime(mTime);
                    mDateValue.setText(cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE));
                    mTimeValue.setText(cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE));

                } else {
                    cal = Calendar.getInstance();
//                    mFirstValue.setText(hourArr[cal.get(Calendar.HOUR_OF_DAY)] + ":" + minuteArr[(cal.get(Calendar.MINUTE))]);
//                    mSecondValue.setText(hourArr[cal.get(Calendar.HOUR_OF_DAY)] + ":" + minuteArr[(cal.get(Calendar.MINUTE))]);
                }
                break;
            case "6":
                if (existData) {
                    HealthStepSet stepSet = new HealthStepSet(this, healthData);
                    baseSet = stepSet;
                    mFirstValue.setText(stepSet.getStepCount());
                } else {
//                    mFirstValue.setText("10000");
                }
                break;
            case "7":
                if (existData) {
                    HealthBodySet bodySet = new HealthBodySet(this, healthData);
                    baseSet = bodySet;
                    mFirstValue.setText(bodySet.getBh().toString());
                    mFirstLayout.setVisibility(View.GONE);
                    mSecondValue.setText(bodySet.getBw().toString());
                    mMemoValue.setText(bodySet.getNote());
                    Date mTime = CommonFunc.UTC2Date(bodySet.getmTime());
                    cal.setTime(mTime);
                    mDateValue.setText(cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE));
                    mTimeValue.setText(cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE));
                } else {

                    //有設定身高則隱藏
                    if (!"".equals(MegaApplication.getInstance().BodyHeight)) {
                        mFirstLayout.setVisibility(View.GONE);
                        mFirstValue.setText(MegaApplication.getInstance().BodyHeight);
                    }
//                    mFirstValue.setText("165.5");
//                    mSecondValue.setText("60.5");
                }
                break;
        }

    }

//    //改變水位
//    private void plusCup(boolean plus, int plusValue) {
//        if (plus) {//要增加水位
//            Log.i("wave", "上升 = " + plusValue);
//            mCupWave.setActionLine(plusValue);
//        } else {//要減少水位
//            Log.i("wave", "減少 = " + plusValue);
//            mCupWave.setActionLine(-plusValue);
//        }
//    }

    //改變水位
    private void plusCup(boolean plus, int cupNum) {
        if (plus) {//要增加水位
            //Log.i("wave", "上升 = " + cupNum + "杯");
            mCupWave.plusCup(cupNum);
        } else {//要減少水位
            //Log.i("wave", "減少 = " + cupNum + "杯");
            mCupWave.plusCup(-cupNum);
        }
    }

    //改變水位
    private void setCup(int cupNum) {
        //Log.i("debug", "cupNum = " + cupNum);
        mCupWave.setCup(cupNum);
    }

    private void setPickerNum(int num) {
        if (num > 4) num = 4;
        mCreatePicker.setVisibility(View.VISIBLE);
        mCreatePicker2.setVisibility(View.VISIBLE);
        mCreatePicker3.setVisibility(View.VISIBLE);
        mCreatePicker4.setVisibility(View.VISIBLE);
        mCreatePicker.setDisplayedValues(null);
        mCreatePicker2.setDisplayedValues(null);
        mCreatePicker3.setDisplayedValues(null);
        mCreatePicker4.setDisplayedValues(null);
        pickValue = pickValue2 = pickValue3 = pickValue4 = "";

        switch (num) {
            case 1:
                mCreatePicker2.setVisibility(View.GONE);
                mCreatePicker3.setVisibility(View.GONE);
                mCreatePicker4.setVisibility(View.GONE);
                break;
            case 2:
                mCreatePicker3.setVisibility(View.GONE);
                mCreatePicker4.setVisibility(View.GONE);
                break;
            case 3:
                mCreatePicker4.setVisibility(View.GONE);
                break;
        }

    }

    /**
     * 設定date picker
     */
    private void openDatePick() {
        pickType = "date";
        mCreatePickerTitle.setText("日期");
        Calendar cal = Calendar.getInstance();
        setPickerNum(3);
        mCreatePicker.setMinValue(2019);
        mCreatePicker.setMaxValue(cal.get(Calendar.YEAR) + 1);
        mCreatePicker2.setMinValue(0);
        mCreatePicker2.setMaxValue(monthArr.length - 1);
        mCreatePicker2.setDisplayedValues(monthArr);
        mCreatePicker3.setMinValue(0);
        mCreatePicker3.setMaxValue(dayArr.length - 1);
        mCreatePicker3.setDisplayedValues(dayArr);

        // 設定預設位置
        if ("".equals(mDateValue.getText().toString())) {
            mCreatePicker.setValue(cal.get(Calendar.YEAR));
            mCreatePicker2.setValue(cal.get(Calendar.MONTH));
            mCreatePicker3.setValue(cal.get(Calendar.DAY_OF_MONTH) - 1);
        } else {
            String[] arr = mDateValue.getText().toString().split("/");
            mCreatePicker.setValue(Integer.parseInt(arr[0]));
            mCreatePicker2.setValue(Integer.parseInt(arr[1]) - 1);
            mCreatePicker3.setValue(Integer.parseInt(arr[2]) - 1);
        }

        mCreatePicker.setWrapSelectorWheel(false); // 是否循環顯示
        mCreatePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mCreatePicker2.setWrapSelectorWheel(false); // 是否循環顯示
        mCreatePicker2.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mCreatePicker3.setWrapSelectorWheel(false); // 是否循環顯示
        mCreatePicker3.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯

        mCreatePickerLayout.setVisibility(View.VISIBLE);
    }

    /**
     * 設定time picker
     */
    private void openTimePick() {
        pickType = "time";
        mCreatePickerTitle.setText("時間");
        Calendar cal = Calendar.getInstance();
        setPickerNum(2);

        mCreatePicker.setMinValue(0);
        mCreatePicker.setMaxValue(23);
        mCreatePicker.setDisplayedValues(hourArr);

        mCreatePicker2.setMinValue(00);
        mCreatePicker2.setMaxValue(59);
        mCreatePicker2.setDisplayedValues(minuteArr);

        // 設定預設位置
        if ("".equals(mTimeValue.getText().toString())) {
            mCreatePicker.setValue(cal.get(Calendar.HOUR_OF_DAY));
            mCreatePicker2.setValue(cal.get(Calendar.MINUTE) - 1);
        } else {
            String[] arr = mTimeValue.getText().toString().split(":");
            mCreatePicker.setValue(Integer.parseInt(arr[0]));
            mCreatePicker2.setValue(Integer.parseInt(arr[1]));
        }

        mCreatePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mCreatePicker2.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mCreatePicker.setWrapSelectorWheel(true); // 是否循環顯示
        mCreatePicker2.setWrapSelectorWheel(true); // 是否循環顯示
        mCreatePickerLayout.setVisibility(View.VISIBLE);
    }


    private JSONObject getHealth() {
        for (int i = 0; i < MegaApplication.healthList.length(); i++) {
            JSONObject obj = MegaApplication.healthList.optJSONObject(i);
            if (healthType.equals(obj.optString("healthType"))) {
                return obj;
            }
        }
        return null;
    }


    /**
     * 設定value picker
     *
     * @param index
     */
    private void openPick(int index) {
        pickType = Integer.toString(index);

        mCreatePicker.setWrapSelectorWheel(false); // 是否循環顯示
        mCreatePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mCreatePicker2.setWrapSelectorWheel(false); // 是否循環顯示
        mCreatePicker2.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mCreatePicker3.setWrapSelectorWheel(false); // 是否循環顯示
        mCreatePicker3.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mCreatePicker4.setWrapSelectorWheel(false); // 是否循環顯示
        mCreatePicker4.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯

        JSONObject obj = getHealth();
        //Log.i(TAG, "openPick: obj= = = = = = =  = " + obj);

        switch (healthType) {
            case "1":
                if (index == 1) {
                    setPickerNum(1);
                    mCreatePicker.setMinValue(30);
                    mCreatePicker.setMaxValue(250);

                    // 設定預設位置
                    if ("".equals(mFirstValue.getText().toString())) {
                        if (sugerFirstValue == 0)
                            mCreatePicker.setValue(80);
                        else if (sugerFirstValue == 1)
                            mCreatePicker.setValue(120);

                        if (obj.has("ac") && obj.optInt("ac") > 0) {
                            mCreatePicker.setValue(obj.optInt("ac"));
                        } else if (obj.has("pc") && obj.optInt("pc") > 0) {
                            mCreatePicker.setValue(obj.optInt("pc"));
                        }

                    } else {
                        mCreatePicker.setValue(Integer.parseInt(mFirstValue.getText().toString()));
                    }

                } else if (index == 2) {
                    setPickerNum(1);
                    pickValueArr = getResources().getStringArray(R.array.health_suger);
                    mCreatePicker.setMinValue(0);
                    mCreatePicker.setMaxValue(pickValueArr.length - 1);
                    mCreatePicker.setDisplayedValues(pickValueArr);

                    // 設定預設位置
                    if ("".equals(mSecondValue.getText().toString())) {
                        mCreatePicker.setValue(0);
                    } else {
                        int i = java.util.Arrays.asList(pickValueArr).indexOf(mSecondValue.getText().toString());
                        mCreatePicker.setValue(i);
                    }

                }
                break;
            case "2"://血壓
                if (index == 1) {//收縮壓舒張壓
                    setPickerNum(4);
                    mCreatePicker.setMinValue(70);
                    mCreatePicker.setMaxValue(200);
                    mCreatePicker3.setMinValue(30);
                    mCreatePicker3.setMaxValue(150);

                    pickValueArr = new String[]{"收縮"};
                    mCreatePicker2.setMinValue(0);
                    mCreatePicker2.setMaxValue(0);
                    mCreatePicker2.setValue(0);
                    mCreatePicker2.setDisplayedValues(pickValueArr);

                    pickValueArr = new String[]{"舒張"};
                    mCreatePicker4.setMinValue(0);
                    mCreatePicker4.setMaxValue(0);
                    mCreatePicker4.setValue(0);
                    mCreatePicker4.setDisplayedValues(pickValueArr);

                    // 設定預設位置
                    if ("".equals(mFirstValue.getText().toString())) {
                        mCreatePicker.setValue(110);
                        mCreatePicker3.setValue(70);
                        if (obj.has("sbp") && obj.has("dbp")) {
                            mCreatePicker.setValue(obj.optInt("sbp"));
                            mCreatePicker3.setValue(obj.optInt("dbp"));
                        }
                    } else {
                        String[] arr = mFirstValue.getText().toString().split("/");
                        mCreatePicker.setValue(Integer.parseInt(arr[0]));
                        mCreatePicker3.setValue(Integer.parseInt(arr[1]));
                    }

                } else if (index == 2) {//心跳
                    setPickerNum(1);
                    pickValueArr = getResources().getStringArray(R.array.health_suger);
                    mCreatePicker.setMinValue(30);
                    mCreatePicker.setMaxValue(150);

                    // 設定預設位置
                    if ("".equals(mSecondValue.getText().toString())) {
                        mCreatePicker.setValue(70);
                        if (obj.has("hr")) {
                            mCreatePicker.setValue(obj.optInt("hr"));
                        }
                    } else {
                        mCreatePicker.setValue(Integer.parseInt(mSecondValue.getText().toString()));
                    }

                } else if (index == 3) {//藥物資料
                    setPickerNum(1);
                    pickValueArr = getResources().getStringArray(R.array.true_false);
                    mCreatePicker.setMinValue(0);
                    mCreatePicker.setMaxValue(1);
                    mCreatePicker.setDisplayedValues(pickValueArr);

                    // 設定預設位置
                    if ("".equals(mMemoValue.getText().toString())) {
                        mCreatePicker.setValue(0);
                    } else {
                        int i = java.util.Arrays.asList(pickValueArr).indexOf(mMemoValue.getText().toString());
                        mCreatePicker.setValue(i);
                    }

                }
                break;
            case "3":
                setPickerNum(1);
                mCreatePicker.setMinValue(90);
                mCreatePicker.setMaxValue(100);
                // 設定預設位置
                if ("".equals(mFirstValue.getText().toString())) {
                    mCreatePicker.setValue(95);
                    if (obj.has("bs")) {
                        mCreatePicker.setValue(obj.optInt("bs"));
                    }
                } else {
                    mCreatePicker.setValue(Integer.parseInt(mFirstValue.getText().toString()));
                }
                break;
            case "4":
                return;
            case "5":
                Calendar cal = Calendar.getInstance();
                setPickerNum(2);

                mCreatePicker.setWrapSelectorWheel(true); // 是否循環顯示
                mCreatePicker2.setWrapSelectorWheel(true); // 是否循環顯示
                mCreatePicker.setMinValue(0);
                mCreatePicker.setMaxValue(23);
                mCreatePicker.setDisplayedValues(hourArr);
                mCreatePicker2.setMinValue(00);
                mCreatePicker2.setMaxValue(59);
                mCreatePicker2.setDisplayedValues(minuteArr);
                if (index == 1) {
                    // 設定預設位置
                    if ("".equals(mFirstValue.getText().toString())) {
                        mCreatePicker.setValue(cal.get(Calendar.HOUR_OF_DAY));
                        mCreatePicker2.setValue(cal.get(Calendar.MINUTE) - 1);


                        if (obj.has("sleep_time")) {
                            String sleepTime = obj.optString("sleep_time");
                            String[] sleep = sleepTime.substring(sleepTime.indexOf("T") + 1, sleepTime.indexOf("Z")).split(":");
                            //Log.i(TAG, "openPick: " + sleepTime.substring(sleepTime.indexOf("T") + 1, sleepTime.indexOf("Z")));
                            mCreatePicker.setValue(Integer.parseInt(sleep[0]));
                            mCreatePicker2.setValue(Integer.parseInt(sleep[1]));
                        }

                    } else {
                        String[] arr = mFirstValue.getText().toString().split(":");
                        mCreatePicker.setValue(Integer.parseInt(arr[0]));
                        mCreatePicker2.setValue(Integer.parseInt(arr[1]));
                    }
                } else if (index == 2) {

                    // 設定預設位置
                    if ("".equals(mSecondValue.getText().toString())) {
                        mCreatePicker.setValue(cal.get(Calendar.HOUR_OF_DAY));
                        mCreatePicker2.setValue(cal.get(Calendar.MINUTE) - 1);
                        if (obj.has("getup_time")) {
                            String getUpTime = obj.optString("getup_time");
                            String[] getUp = getUpTime.substring(getUpTime.indexOf("T") + 1, getUpTime.indexOf("Z")).split(":");
                            //Log.i(TAG, "openPick: " + getUpTime.substring(getUpTime.indexOf("T") + 1, getUpTime.indexOf("Z")));
                            //Log.i(TAG, "openPick: " + Integer.parseInt(getUp[0]) + " : " + Integer.parseInt(getUp[1]));
                            mCreatePicker.setValue(Integer.parseInt(getUp[0]));
                            mCreatePicker2.setValue(Integer.parseInt(getUp[1]));
                        }
                    } else {
                        String[] arr = mSecondValue.getText().toString().split(":");
                        mCreatePicker.setValue(Integer.parseInt(arr[0]));
                        mCreatePicker2.setValue(Integer.parseInt(arr[1]));
                    }
                }
                break;
            case "6":
                setPickerNum(1);
                pickValueArr = new String[30];
                for (int i = 0; i < 30; i++) {
                    pickValueArr[i] = Integer.toString(1000 * (i + 1));
                }
                mCreatePicker.setMinValue(0);
                mCreatePicker.setMaxValue(pickValueArr.length - 1);
                mCreatePicker.setDisplayedValues(pickValueArr);

                // 設定預設位置
                if ("".equals(mFirstValue.getText().toString())) {
                    mCreatePicker.setValue(0);
                } else {
                    int i = java.util.Arrays.asList(pickValueArr).indexOf(mFirstValue.getText().toString());
                    mCreatePicker.setValue(i);
                }
                break;
            case "7":
                pickValueArr = new String[]{"."};
                if (index == 1) {
                    setPickerNum(3);
                    mCreatePicker.setMinValue(90);
                    mCreatePicker.setMaxValue(200);
                    mCreatePicker2.setMinValue(0);
                    mCreatePicker2.setMaxValue(0);
                    mCreatePicker3.setMinValue(0);
                    mCreatePicker3.setMaxValue(9);

                    // 設定預設位置
                    if ("".equals(mFirstValue.getText().toString())) {
                        mCreatePicker.setValue(165);
                        mCreatePicker3.setValue(5);
                    } else {
                        String[] arr = mFirstValue.getText().toString().split("\\.");
                        mCreatePicker.setValue(Integer.parseInt(arr[0]));
                        mCreatePicker3.setValue(Integer.parseInt(arr[1]));
                    }
                } else if (index == 2) {
                    setPickerNum(3);
                    mCreatePicker.setMinValue(30);
                    mCreatePicker.setMaxValue(200);
                    mCreatePicker2.setMinValue(0);
                    mCreatePicker2.setMaxValue(0);
                    mCreatePicker3.setMinValue(0);
                    mCreatePicker3.setMaxValue(9);

                    // 設定預設位置
                    if ("".equals(mSecondValue.getText().toString())) {
                        mCreatePicker.setValue(60);
                        mCreatePicker3.setValue(5);
                        if (obj.has("bw")) {
                            mCreatePicker.setValue(Integer.parseInt(obj.optString("bw").split("\\.")[0]));
                            mCreatePicker3.setValue(Integer.parseInt(obj.optString("bw").split("\\.")[1]));
                        }
                    } else {
                        String[] arr = mSecondValue.getText().toString().split("\\.");
                        mCreatePicker.setValue(Integer.parseInt(arr[0]));
                        mCreatePicker3.setValue(Integer.parseInt(arr[1]));
                    }
                }
                mCreatePicker2.setValue(0);
                mCreatePicker2.setDisplayedValues(pickValueArr);
                break;
        }


        mCreatePickerLayout.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        index = extras.getInt("index");  //順序
        healthType = extras.getString("healthType"); //健康類別
        //Log.i("debug", "healthType = " + healthType);
        JSONObject healthMap = MegaApplication.healthMap.get(healthType);
        healthData = CommonFunc.parseJSON(extras.getString("data")); //健康資料

        setContentView(R.layout.health_create_view);

        mTopBack = findViewById(R.id.top_back);
        mTopTitle = findViewById(R.id.top_title);

        mFirstLayout = findViewById(R.id.first_value_layout);
        mSecondLayout = findViewById(R.id.second_value_layout);
        mMemoLayout = findViewById(R.id.create_memo_layout);
        mCreateDateLayout = findViewById(R.id.create_date_layout);
        mCreateTimeLayout = findViewById(R.id.create_time_layout);

        mFirstValue = findViewById(R.id.first_value_edit);
        mSecondValue = findViewById(R.id.second_value_edit);
        mDateValue = findViewById(R.id.create_date);
        mTimeValue = findViewById(R.id.create_time);
        mMemoValue = findViewById(R.id.create_memo);

        mFirstTitle = findViewById(R.id.first_value_title);
        mSecondTitle = findViewById(R.id.second_value_title);
        mMemoTitle = findViewById(R.id.create_memo_title);
        mCupCount = findViewById(R.id.cup_count);
        mCupUnit = findViewById(R.id.cup_unit);
        mFirstUnit = findViewById(R.id.first_unit);
        mSecondUnit = findViewById(R.id.second_unit);

        mFirstIcon = findViewById(R.id.first_value_icon);
        mSecondIcon = findViewById(R.id.second_value_icon);
        mMemoIcon = findViewById(R.id.create_memo_icon);
        mCreateIcon = findViewById(R.id.create_icon);

        mCreatePicker = findViewById(R.id.create_picker);
        mCreatePicker2 = findViewById(R.id.create_picker2);
        mCreatePicker3 = findViewById(R.id.create_picker3);
        mCreatePicker4 = findViewById(R.id.create_picker4);

        mCreatePickerLayout = findViewById(R.id.create_picker_layout);
        mCreatePickerTitle = findViewById(R.id.create_picker_title);
        mCreatePickerCheck = findViewById(R.id.create_picker_check);
        mCreatePickerCancel = findViewById(R.id.create_picker_cancel);

        initData();
        //依照healthType來設定畫面
        String title = "";
        switch (healthType) {
            case "1":
                title = "紀錄血糖";
                mFirstTitle.setText("血糖值");
                mSecondTitle.setText("飯前/飯後");
                mFirstUnit.setText("mg/dl");
                mSecondUnit.setText("");
                mFirstIcon.setImageResource(R.drawable.ic_blood_sugar);
                mSecondIcon.setImageResource(R.drawable.ic_meal_before);
                mMemoValue.setVisibility(View.GONE);
                mMemoLayout.setVisibility(View.GONE);
                break;
            case "2":
                title = "紀錄血壓";
                mFirstTitle.setText("收縮壓/舒張壓");
                mSecondTitle.setText("脈搏率");
                mMemoTitle.setText("藥物資料");
                mFirstUnit.setText("mmHg");
                mSecondUnit.setText("次/分");
                mFirstIcon.setImageResource(R.drawable.ic_blood_pleasure);
                mSecondIcon.setImageResource(R.drawable.ic_heart_beat);
                mMemoIcon.setImageResource(R.drawable.ic_take_pill_non);
                break;
            case "3":
                title = "紀錄血氧";
                mFirstTitle.setText("血氧量");
                mFirstUnit.setText("%");
                mSecondUnit.setText("");
                mFirstIcon.setImageResource(R.drawable.ic_o2);
                mSecondLayout.setVisibility(View.GONE);
                mMemoLayout.setVisibility(View.GONE);
                //mMemoValue.setVisibility(View.GONE);
                break;
            case "4":
                title = "紀錄飲水量";
                mFirstTitle.setText("當日飲水量");
                mFirstUnit.setText("ml");
                mSecondUnit.setText("");
                mFirstIcon.setImageResource(R.drawable.ic_drink_water);
                mSecondLayout.setVisibility(View.GONE);
                mMemoLayout.setVisibility(View.GONE);
                mCupWave = findViewById(R.id.cup_wave);
                TextView drinkPlus = findViewById(R.id.drink_plus);
                TextView drinkSub = findViewById(R.id.drink_sub);
                TextView cupCount = findViewById(R.id.cup_count);


                //cupHight = (int) mCupWave.getCupHeight();
                //Log.i("debug", "cupHight = " + mCupWave.getCupHeight());
                //Log.i("debug", "cupHight = " + cupHight);
                totleCup = MegaApplication.WaterCapacity / oneCup; //裝到滿需要多少杯
                nowCup = nowCapacity / 250;//已經喝杯數
                plusRange = cupHight / totleCup;  //每一杯上升多少高度
                //Log.i("debug", nowCup + "  " + totleCup);
                if (nowCup >= totleCup) {
                    setCup(totleCup);
                    //plusCup(true, cupHight);
                } else {
                    setCup(nowCup);
                    //plusCup(true, (int) (nowCup * plusRange));
                }
                //int remainingWater = MegaApplication.WaterCapacity - nowCapacity; //距離目標的ml數
                cupCount.setText(nowCup + " 杯");
                mFirstValue.setText(nowCapacity + "");


                mCupWave.setVisibility(View.VISIBLE);
                drinkPlus.setVisibility(View.VISIBLE);
                drinkSub.setVisibility(View.VISIBLE);
                mCupCount.setVisibility(View.VISIBLE);
                mCupUnit.setVisibility(View.VISIBLE);

                drinkPlus.setOnClickListener(this::onClick);
                drinkSub.setOnClickListener(this::onClick);
                break;
            case "5":
                title = "睡眠時間";
                mFirstTitle.setText("就寢時間");
                mSecondTitle.setText("起床時間");
                mFirstIcon.setImageResource(R.drawable.ic_sleep_time);
                mSecondIcon.setImageResource(R.drawable.ic_wakeup);
                mMemoLayout.setVisibility(View.GONE);
                mCreateTimeLayout.setVisibility(View.GONE);
                break;
            case "6":
                title = "每日步數";
                mFirstTitle.setText("每日目標步數");
                mFirstUnit.setText("步");
                mSecondUnit.setText("");
                mFirstIcon.setImageResource(R.drawable.ic_walk);
                mSecondLayout.setVisibility(View.GONE);
                mMemoLayout.setVisibility(View.GONE);
                mCreateDateLayout.setVisibility(View.GONE);
                break;
            case "7":
                title = "紀錄體重";
                mFirstTitle.setText("身高");
                mSecondTitle.setText("體重");
                mFirstUnit.setText("cm");
                mSecondUnit.setText("kg");
                mFirstIcon.setImageResource(R.drawable.ic_account);
                mSecondIcon.setImageResource(R.drawable.ic_weight);

                break;
        }

        //將所有picker 設定成不能 Focus 全選 貼上
        mFirstValue.setFocusable(false);
        mFirstValue.setFocusableInTouchMode(false);
        mFirstValue.setLongClickable(false);
        mSecondValue.setFocusable(false);
        mSecondValue.setFocusableInTouchMode(false);
        mSecondValue.setLongClickable(false);
        mDateValue.setFocusable(false);
        mDateValue.setFocusableInTouchMode(false);
        mDateValue.setLongClickable(false);
        mTimeValue.setFocusable(false);
        mTimeValue.setFocusableInTouchMode(false);
        mTimeValue.setLongClickable(false);
        if ("2".equals(healthType)) {
            mMemoValue.setFocusable(false);
            mMemoValue.setFocusableInTouchMode(false);
            mMemoValue.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
            mMemoValue.setLongClickable(false);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // call that method
            mFirstValue.setCustomSelectionActionModeCallback(new ActionModeCallbackInterceptor());
            mFirstValue.setCustomInsertionActionModeCallback(new ActionModeCallbackInterceptor());
            mSecondValue.setCustomSelectionActionModeCallback(new ActionModeCallbackInterceptor());
            mSecondValue.setCustomInsertionActionModeCallback(new ActionModeCallbackInterceptor());
            mDateValue.setCustomSelectionActionModeCallback(new ActionModeCallbackInterceptor());
            mDateValue.setCustomInsertionActionModeCallback(new ActionModeCallbackInterceptor());
            mTimeValue.setCustomSelectionActionModeCallback(new ActionModeCallbackInterceptor());
            mTimeValue.setCustomInsertionActionModeCallback(new ActionModeCallbackInterceptor());
            if ("2".equals(healthType)) {
                mMemoValue.setCustomSelectionActionModeCallback(new ActionModeCallbackInterceptor());
                mMemoValue.setCustomInsertionActionModeCallback(new ActionModeCallbackInterceptor());
            }
        }

        mCreateIcon.setImageResource(healthMap.optInt("healthRecordIcon"));
        if ("4".equals(healthType)) {//喝水量要有動態水量圖
            mCreateIcon.setImageResource(R.drawable.ic_cup);
        }

        mTopRightBtn = findViewById(R.id.top_right_btn);
        mTopTitle.setText(title);
        mTopRightBtn.setText("儲存");

        mCreatePickerLayout.setOnClickListener(this::onClick);
        mFirstValue.setOnClickListener(this::onClick);
        mSecondValue.setOnClickListener(this::onClick);
        mCreatePickerCheck.setOnClickListener(this::onClick);
        mCreatePickerCancel.setOnClickListener(this::onClick);
        mDateValue.setOnClickListener(this::onClick);
        mTimeValue.setOnClickListener(this::onClick);
        mMemoValue.setOnClickListener(this::onClick);

        //
        mCreatePicker.setOnValueChangedListener(this);
        mCreatePicker2.setOnValueChangedListener(this);
        mCreatePicker3.setOnValueChangedListener(this);
        mCreatePicker4.setOnValueChangedListener(this);

        //top
        mTopBack.setOnClickListener(this::onClick);
        mTopRightBtn.setOnClickListener(this::onClick);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        switch (view.getId()) {
            case R.id.create_picker_layout:
                mCreatePickerLayout.setVisibility(View.GONE);
                break;
            case R.id.top_back:
                exit();
                break;
            case R.id.top_right_btn:
                saveData();
                break;
            case R.id.first_value_edit:
                imm.hideSoftInputFromWindow(mFirstValue.getWindowToken(), 0);
                //Log.i("debug", "first_value_edit");
                mCreatePickerTitle.setText(mFirstTitle.getText());
                openPick(1);
                break;
            case R.id.second_value_edit:
                imm.hideSoftInputFromWindow(mSecondValue.getWindowToken(), 0);
                //Log.i("debug", "second_value_edit");
                mCreatePickerTitle.setText(mSecondTitle.getText());
                openPick(2);
                break;
            case R.id.create_memo:
                if ("2".equals(healthType)) {
                    //血壓 藥物資料
                    imm.hideSoftInputFromWindow(mSecondValue.getWindowToken(), 0);
                    mCreatePickerTitle.setText(mMemoTitle.getText());
                    openPick(3);
                }
                break;
            case R.id.create_date:
                imm.hideSoftInputFromWindow(mSecondValue.getWindowToken(), 0);
                //Log.i("debug", "create_date");
                openDatePick();
                break;
            case R.id.create_time:
                imm.hideSoftInputFromWindow(mSecondValue.getWindowToken(), 0);
                //Log.i("debug", "create_time");
                openTimePick();
                break;
            case R.id.create_picker_check:
                mCreatePickerLayout.setVisibility(View.GONE);
                if ("date".equals(pickType)) {
                    //日期
                    mDateValue.setText(mCreatePicker.getValue() + "/" + monthArr[mCreatePicker2.getValue()] + "/" + dayArr[mCreatePicker3.getValue()]);
                } else if ("time".equals(pickType)) {
                    //時間
                    //Log.i("debug", "pickValue = " + pickValue + ":" + pickValue2);
                    mTimeValue.setText(hourArr[mCreatePicker.getValue()] + ":" + minuteArr[mCreatePicker2.getValue()]);
                } else if ("2".equals(pickType) && "1".equals(healthType)) {
                    //飯前飯後
                    mSecondValue.setText(pickValueArr[mCreatePicker.getValue()]);
//                    if (mCreatePicker.getValue() == 0)
//                        mFirstValue.setText("80");
//                    else if (mCreatePicker.getValue() == 1)
//                        mFirstValue.setText("120");

                } else if ("3".equals(pickType) && "2".equals(healthType)) {
                    //用藥資訊
                    mMemoValue.setText(pickValueArr[mCreatePicker.getValue()]);
                } else if ("1".equals(pickType) && "6".equals(healthType)) {
                    //每日目標步數
                    mFirstValue.setText(pickValueArr[mCreatePicker.getValue()]);
                } else if ("5".equals(healthType)) {
                    //睡眠時間
                    if ("1".equals(pickType)) {
                        mFirstValue.setText(hourArr[mCreatePicker.getValue()] + ":" + minuteArr[mCreatePicker2.getValue()]);
                    } else if ("2".equals(pickType)) {
                        mSecondValue.setText(hourArr[mCreatePicker.getValue()] + ":" + minuteArr[mCreatePicker2.getValue()]);
                    }
                } else if ("1".equals(pickType) && "2".equals(healthType)) {
                    //收縮壓舒張壓 會有兩個值
                    mFirstValue.setText(mCreatePicker.getValue() + "/" + mCreatePicker3.getValue());
                } else if ("7".equals(healthType)) {
                    //身高體重
                    if ("1".equals(pickType)) {
                        mFirstValue.setText(mCreatePicker.getValue() + "." + mCreatePicker3.getValue());
                    } else if ("2".equals(pickType)) {
                        mSecondValue.setText(mCreatePicker.getValue() + "." + mCreatePicker3.getValue());
                    }
                } else if ("1".equals(pickType)) {
                    mFirstValue.setText(Integer.toString(mCreatePicker.getValue()));
                } else if ("2".equals(pickType)) {
                    mSecondValue.setText(Integer.toString(mCreatePicker.getValue()));
                }
                break;
            case R.id.create_picker_cancel:
                mCreatePickerLayout.setVisibility(View.GONE);
                break;
            case R.id.drink_plus:
                nowCup++;
                nowCapacity = nowCup * oneCup;
                mCupCount.setText(nowCup + " 杯");
                mFirstValue.setText(nowCapacity + "");
                Log.i("wave", nowCup + " " + totleCup);
                if (nowCup <= totleCup) {
                    plusCup(true, 1);
                }
                break;
            case R.id.drink_sub:
                nowCup--;
                if (nowCup <= 0)
                    nowCup = 0;
                nowCapacity = nowCup * oneCup;
                mCupCount.setText(nowCup + " 杯");
                mFirstValue.setText(nowCapacity + "");
                if (nowCup >= 0 && nowCup < totleCup) {
                    plusCup(false, 1);
                }
//                if (mCupWave.getLevelLine() < cupHight && nowCup < totleCup) { //沒有低於水位 且 杯數以低於最大杯數
//                    plusCup(false, (int) plusRange);
//                }
                break;
        }
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int oldVal, int newVal) {
        Vibrator myVibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
        myVibrator.vibrate(100);
        if ("1".equals(healthType) && "2".equals(pickType)) {
            sugerFirstValue = newVal;
        }

    }

    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {

    }

    @Override
    public void triggerAction(String action, String target) {
        //Log.i("debug", "triggerAction");
        //triggerAction(action, target, R.id.main_content);
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        try {
            switch (type) {
                case MednetCreateBloodSugar:
                    //Log.i("debug", "result = " + result);
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces(result);
                    }
                    break;
                case MednetCreateBloodPressure:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces(result);
                    }
                    break;
                case MednetCreateBloodOxygen:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces(result);
                    }
                    break;
                case MednetUpdateBloodSugar:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces();
                    }
                    break;
                case MednetUpdateBloodPressure:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces();
                    }
                    break;
                case MednetUpdateBloodOxygen:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces();
                    }
                    break;
                case MednetCreateDrinkingCreateOrUpdate:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces(result);
                    }
                    break;
                case MednetSleepingCreateOrUpdate:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces();
                    }
                    break;
                case MednetCreateStepCreateOrUpdate:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces();
                    }
                    break;
                case MednetCreateBodyMassWeight:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces(result);
                    }
                    break;
                case MednetUpdateBodyMassWeight:
                    if (result != null && "200".equals(result.optString("state_code"))) {
                        saveSucces();
                    }
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveSucces(JSONObject result) {
        CommonFunc.showToast(this, "儲存成功");
        //先更新本地資料
        for (int i = 0; i < MegaApplication.healthList.length(); i++) {
            JSONObject obj = MegaApplication.healthList.optJSONObject(i);
            JSONObject healthMap = MegaApplication.healthMap.get(healthType);
            String typeName = healthMap.optString("healthTypeName");
            if (obj.optString("healthType").equals(healthType)) {
                obj = result.optJSONObject("source");
                try {
                    obj.put("healthTypeName", healthMap.optString("healthName"));
                    obj.put("healthType", healthType);
                    obj.put("healthTypeKey", typeName);
                    obj.put("healthImg", healthMap.optString("healthImg"));
                    obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                    obj.put("index", i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        Intent intent = new Intent();
        intent.putExtra("save", "success");
        this.setResult(RESULT_OK, intent);
        this.finish();
    }

    private void saveSucces() {
        CommonFunc.showToast(this, "儲存成功");
        Intent intent = new Intent();
        intent.putExtra("save", "success");
        this.setResult(RESULT_OK, intent);
        this.finish();
    }


    @Override
    public void onBackPressed() {
        exit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
    }


    private class ActionModeCallbackInterceptor implements ActionMode.Callback {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {
        }
    }
}
