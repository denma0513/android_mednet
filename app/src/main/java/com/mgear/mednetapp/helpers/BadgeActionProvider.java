package com.mgear.mednetapp.helpers;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;

public class BadgeActionProvider extends ActionProvider {
    private ImageView mIvIcon;
    private TextView mTvBadge;

    private int clickWhat;
    private OnClickListener onClickListener;

    public BadgeActionProvider(Context context) {
        super(context);
    }

    @Override
    public View onCreateActionView() {
        int size = getContext().getResources().getDimensionPixelSize(
                android.support.design.R.dimen.abc_action_bar_default_height_material);

        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(size, size);
        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.badge_view, null, false);

        view.setLayoutParams(layoutParams);
        mIvIcon = view.findViewById(R.id.iv_icon);
        mTvBadge = view.findViewById(R.id.tv_badge);


        Log.i("debug", "onCreateActionView: mIvIcon= " + mIvIcon);
//        mTvBadge.setVisibility(View.GONE);
        view.setOnClickListener(onViewClickListener);
        return view;
    }

    // 點選處理。
    private View.OnClickListener onViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onClickListener != null)
                onClickListener.onClick(clickWhat);
        }
    };

    // 外部設定監聽。
    public void setOnClickListener(int what, OnClickListener onClickListener) {
        this.clickWhat = what;
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onClick(int what);
    }

    // 設定圖示。
    public void setIcon(int icon) {
        Log.i("debug", "setIcon: mIvIcon= " + mIvIcon);
        mIvIcon.setImageResource(icon);
        switch (icon) {
            case R.drawable.ic_main_notice: {
                int i = 0;
                try {
                    i = Integer.parseInt(MegaApplication.bell);
                } catch (Exception e) {
                    i = 0;
                }

                if (i > 0) {
                    setBadge(i);
                }
                break;
            }
            case R.drawable.ic_main_shopping: {
                int i = 0;
                try {
                    i = Integer.parseInt(MegaApplication.shoppingCart);
                } catch (Exception e) {
                    i = 0;
                }
                if (i > 0) {
                    setBadge(i);
                }
                break;
            }
        }
    }

    // 設定顯示的數字。
    public void setBadge(int i) {

        if (i > 0)
            mTvBadge.setVisibility(View.VISIBLE);
        else
            mTvBadge.setVisibility(View.GONE);
        if (i > 100) {
            mTvBadge.setText("99+");
            return;
        }
        mTvBadge.setText(Integer.toString(i));
    }

    // 設定文字。
    public void setTextInt(int i) {
        mTvBadge.setText(i);
    }

    // 設定顯示的文字。
    public void setText(String i) {
        mTvBadge.setText(i);
    }
}
