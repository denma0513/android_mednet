package com.mgear.mednetapp.helpers;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

import static android.os.Environment.MEDIA_MOUNTED;

/**
 * Created by Jye on 2017/6/21.
 * class: StorageHelper
 */
final public class StorageHelper {

    //存取外部儲存空間的權限
    private static final String EXTERNAL_STORAGE_PERMISSION = "android.permission.WRITE_EXTERNAL_STORAGE";

    /**
     * 建構函數
     */
    private StorageHelper() {
    }

    /**
     * 取得快取的檔案目錄物件
     * @param context Context
     * @return File
     */
    public static File getCacheDirectory(Context context) {
        File appCacheDir = null;
        if (MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) && hasExternalStoragePermission(context))
            appCacheDir = getExternalCacheDirectory(context);
        if (appCacheDir == null)
            appCacheDir = context.getCacheDir();
        return appCacheDir;
    }

    /**
     * 取得外部儲存空間的快取檔案目錄物件
     * @param context Context
     * @return File
     */
    private static File getExternalCacheDirectory(Context context) {
        File dataDir = new File(new File(Environment.getExternalStorageDirectory(), "Android"), "data");
        File appCacheDir = new File(new File(dataDir, context.getPackageName()), "cache");
        if (!appCacheDir.exists()) {
            if (!appCacheDir.mkdirs())
                return null;
            try {
                //noinspection ResultOfMethodCallIgnored
                new File(appCacheDir, ".nomedia").createNewFile();
            }
            catch (IOException ignored) { }
        }
        return appCacheDir;
    }

    /**
     * 是否有存取外部儲存空間的權限
     * @param context Context
     * @return T/F
     */
    private static boolean hasExternalStoragePermission(Context context) {
        int perm = context.checkCallingOrSelfPermission(EXTERNAL_STORAGE_PERMISSION);
        return perm == PackageManager.PERMISSION_GRANTED;
    }

}
