package com.mgear.mednetapp.helpers;

import android.graphics.Path;
import android.graphics.Point;

import java.util.List;

/**
 * Created by Jye on 2017/7/9.
 * class: PathHelper
 */
public class PathHelper {

    /**
     * 從點清單產生路徑
     * @param points Points
     * @return Path
     */
    public static Path pathFromPoints(List<Point> points) {
        Path path = new Path();
        Point start = points.get(0);
        path.moveTo((float) start.x, (float)start.y);
        int l = points.size();
        for (int i = 1; i < l; i++) {
            Point point = points.get(i);
            path.lineTo((float) point.x, (float) point.y);
        }
        return path;
    }

}