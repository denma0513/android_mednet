package com.mgear.mednetapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.HealthListFragment;
import com.mgear.mednetapp.fragments.base.MainTopBottomFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.OnItemClickListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.task.common.NewDownloadImageTask;

import org.json.JSONArray;
import org.json.JSONObject;

public class HealthPushActivity extends BaseActivity implements View.OnClickListener, TaskPost2ExecuteImpl, INativeActionImpl, IBackHandleIpml,
        OnItemClickListener {

    private static final String TAG = HealthPushActivity.class.getName();
    private static final String actionType = MegaApplication.ActionTypeHealthPush;
    private static int DETAIL = 99;
    //畫面元件
    private MainTopBottomFragment mTopFagment;
    private DrawerLayout drawerLayout;
    private ExpandableListView mMenuListView;
    private HealthListFragment healthListFragment;

    private ImageView mDrowerHeaderUserPic;
    private TextView mDrowerHeaderUserName, mDrowerHeaderMail;


    //邏輯元件
    private MegaBaseFragment selectedFragment;


//    private void setMainFragment() {
//        try {
//            FragmentManager fragmentManager = getFragmentManager();
//            MainTopBottomFragment mainTopBottomFragment = new MainTopBottomFragment();
//            //設定這個activity的資訊
//            mainTopBottomFragment.setDrawer(drawerLayout).setMenu(mMenuListView).setAction(actionType);
//            mTopFagment = mainTopBottomFragment;
//            FragmentTransaction ft = fragmentManager.beginTransaction();
//            ft.replace(R.id.health_top, mTopFagment);
//            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//            ft.commit();
//
//            Log.i("debug", "accesstoken = " + MegaApplication.getInstance().getMember().getAccessToken() +
//                    " refreshToken = " + MegaApplication.getInstance().getMember().getRefreshToken());
//            if ((MegaApplication.getInstance().getMember().isNullAccessToken() || MegaApplication.getInstance().getMember().isNullRefreshToken())) {
//                Intent intent = new Intent(this, UserLoginActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //如果這Activity是開啟的就不再重複開啟
//                startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));
//                return;
//            }
//
//            setHealthListFragment();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void setHealthListFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        healthListFragment = new HealthListFragment();
        //fragmentMain = healthListFragment;
        if (healthListFragment.isAdded()) { // 如果 home fragment 已經被 add 過，
            ft.show(healthListFragment); // 顯示它。
        } else { // 反之，
            ft.add(R.id.main_content, healthListFragment, "HEALTH"); // 使用 add 方法。
            ft.addToBackStack("HEALTH");
        }
        ft.commit();
    }


    //開啟健康記錄設定
//    public void goHealthSetting() {
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction ft = fragmentManager.beginTransaction();
//        HealthListFragment fragmentMain = new HealthListFragment();
//        //fragmentMain = healthListFragment;
//        if (fragmentMain.isAdded()) { // 如果 home fragment 已經被 add 過，
//            ft.show(fragmentMain); // 顯示它。
//        } else { // 反之，
//            ft.add(R.id.main_content, fragmentMain, "HEALTH"); // 使用 add 方法。
//            ft.addToBackStack("HEALTH");
//        }
//        ft.commit();
//    }

    //開啟健康記錄設定
    public void goHealthDetail(int index, JSONObject data) {
        Intent intent = new Intent(this, HealthPushDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index", index);
        bundle.putString("data", data.toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, DETAIL);
    }

    /**
     * 設定左側選單資料
     */
    public void setDrowerData() {
        if ("".equals(MegaApplication.getInstance().getMember().getProfileImage())) {
            new NewDownloadImageTask(mDrowerHeaderUserPic).execute(MegaApplication.getInstance().getMember().getProfileImage());
        }
        mDrowerHeaderUserName.setText(MegaApplication.getInstance().getMember().getName());
        mDrowerHeaderMail.setText(MegaApplication.getInstance().getMember().getEmail());
    }

    public void exit() {
        //Log.i("debug","exit");
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_main);
        drawerLayout = findViewById(R.id.drawer_layout);
        mMenuListView = findViewById(R.id.menu_list_view);

        mDrowerHeaderUserPic = findViewById(R.id.drower_header_user_pic);
        mDrowerHeaderUserName = findViewById(R.id.drower_header_user_name);
        mDrowerHeaderMail = findViewById(R.id.drower_header_mail);

        //setMainFragment();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {

    }

    @Override
    public void triggerAction(String action, String target) {
        //Log.i("debug", "triggerAction");
        //triggerAction(action, target, R.id.main_content);
        drawerLayout.closeDrawers();
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {

    }

    //回接activity的結果
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        Log.d("debug", "requestCode = " + requestCode + " resultCode = " + resultCode + "data = " + data);
        if (requestCode == Integer.parseInt(MegaApplication.ActionTypeLogin)) {
            try {
                Bundle bundle = data.getExtras();
                //完成登入
                if (resultCode == Activity.RESULT_OK) {
                    setHealthListFragment();
                } else {
                    exit();
                }
            } catch (Exception e) {
                Log.w("debug", "signInResult:failed code=" + e.getMessage());
                exit();
                e.printStackTrace();
            }

        } else if (requestCode == DETAIL ) {
            healthListFragment.refreshData();
//            String saveData = data.getStringExtra("save");
//            if ("success".equals(saveData )) {
//                healthListFragment.refreshData();
//            }
        }
    }

    @Override
    public void onBackPressed() {
        drawerLayout.closeDrawers();
        //Log.i("debug", "getFragmentManager().getBackStackEntryCount()  = " + getFragmentManager().getBackStackEntryCount());
        FragmentManager fragmentManager = getFragmentManager();
        //Log.i("debug", "fragmentManager = " + fragmentManager.getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1).getName());
        if (selectedFragment == null || !selectedFragment.onBackPressed()) {
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                FragmentManager.BackStackEntry fragment = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1);
                if ("HEALTH".equals(fragment.getName())) {
                    exit();
                }
                getFragmentManager().popBackStack();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
    }

    @Override
    public void onItemClick(View view, int position, JSONObject data) {
        goHealthDetail(position, data);
    }


}
