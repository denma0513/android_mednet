package com.mgear.mednetapp.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.YouTubePlayerActivity;
import com.mgear.mednetapp.enums.RemainEnum;
import com.mgear.mednetapp.interfaces.organization.DialogContentImpl;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.HttpConnectFunc;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/11.
 * class: ArticleContent
 */
public class ArticleContent extends LinearLayout implements DialogContentImpl, View.OnClickListener {

    private Activity mContext;
    private String title;
    private String message;
    private String guideName;
    private String lastRemain;
    private TextView txtContent;
    private ImageView imgContent;

    private boolean firstShow;
    private boolean showRemainTimeTitle;
    private boolean showRemainTimeMessage;
    private Date dtStandard;
    private RemainEnum index;
    private int waitSeconds;
    private UUID[] posInfo;
    private String imageAction;

    public ArticleContent(Context context) {
        super(context);
        initView(context);
    }

    public ArticleContent(Context context, String title) {
        super(context);
        this.title = title;
        initView(context);
    }

    public ArticleContent(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ArticleContent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        firstShow = true;
        mContext = (Activity) context;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_content_article, this);
        txtContent = (TextView) findViewById(R.id.txtContentArticleText);
        imgContent = (ImageView) findViewById(R.id.imgContentArticleImage);
    }

    public void setText(String text) {
        txtContent.setText(CommonFunc.fromHtml(text));
        txtContent.setVisibility(View.VISIBLE);
    }

    public void setImage(String url) {
        if (url.equals("")) return;
        this.imgContent.setVisibility(VISIBLE);
        //載入影像
        Picasso.with(mContext).load(HttpConnectFunc.CONNECT_URL + url).into(this.imgContent);
    }

    public void setImage(int id) {
        this.imgContent.setVisibility(VISIBLE);
        this.imgContent.setImageResource(id);
    }

    public void setImage(int id, String action) {
        this.imgContent.setVisibility(VISIBLE);
        this.imgContent.setImageResource(id);
        this.imageAction = action;
        this.imgContent.setOnClickListener(this);
    }

    public void setReciprocal(RemainEnum index, int seconds, boolean showTitle) {
        this.index = index;
        this.showRemainTimeTitle = showTitle;
        this.waitSeconds = seconds;
    }

    public void setReciprocal(RemainEnum index, int seconds, boolean showMessage, String message) {
        this.index = index;
        this.showRemainTimeMessage = showMessage;
        this.dtStandard = new Date();
        this.waitSeconds = seconds;
        this.message = message;
    }

    @Override
    public void onClick(View v) {
        //VideoDialog dialog = new VideoDialog(mContext, imageAction);
        //dialog.show();
        Intent intent = new Intent();
        intent.putExtra("result", imageAction);
        intent.setClass(mContext, YouTubePlayerActivity.class);
        mContext.startActivityForResult(intent, 1);
    }

    @Override
    public void show() {
        //判斷是否第一次顯示
        if (firstShow) {
            //判斷是否啟用倒數
            if (this.waitSeconds > 0) {
                this.dtStandard = new Date();
            }
            firstShow = false;
        }
    }

    @Override
    public void hidden() { }

    @Override
    public boolean isRemainTime() { return this.waitSeconds > 0; }

    @Override
    public boolean showRemainInTitle() { return this.showRemainTimeTitle; }

    @Override
    public boolean showRemainInMessage() { return this.showRemainTimeMessage; }

    @Override
    public String getRemainTime() {
        if (firstShow || this.waitSeconds == 0) return "";
        //return String.format(Locale.getDefault(), mContext.getString(R.string.title_about_minute), String.valueOf(this.waitSeconds / 60));

        //計算剩餘的倒數時間
        long interval = (new Date().getTime() - this.dtStandard.getTime()) / 1000;
        if (interval >= this.waitSeconds) {
            lastRemain = "00:00";
        }
        else {
            long diff = this.waitSeconds - interval;
            lastRemain = String.format(Locale.getDefault(), "%02d:%02d", diff / 60, diff % 60);
        }
        return lastRemain;

    }

    @Override
    public RemainEnum getRemainIndex() { return index; }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getMessage() {
        if (showRemainTimeMessage)
            return String.format(Locale.getDefault(), "%s %s", message == null ? title : message, lastRemain);
        else
            return message == null ? title : message;
    }

    @Override
    public void setGuidePosition(UUID[] array) { this.posInfo = array; }

    @Override
    public UUID[] getGuidePosition() { return posInfo; }

    @Override
    public void setGuideName(String name) { this.guideName = name; }

    @Override
    public String getGuideName() { return this.guideName; }

}