package com.mgear.mednetapp.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.interfaces.organization.DialogContentImpl;
import com.mgear.mednetapp.interfaces.organization.RemainFinishImpl;
import com.mgear.mednetapp.interfaces.organization.ViewLocationImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Jye on 2017/7/10.
 * class: WizardDialog
 */
public class WizardDialog implements View.OnClickListener, Runnable {

    private Dialog mDialog;
    private Handler mHandler;

    private List<View> mContents;
    private int mSize;
    private int mIndex;
    private boolean mFirstShow;
    private boolean isDispose;

    private String mStrConfirm;
    private String mStrNext;

    private TextView txtTitleTime;
    private TextView txtTitle;
    private TextView txtMessage;
    private ScrollView svContent;
    private Button btnNegative;
    private Button btnNeutral;
    private Button btnPositive;

    private boolean neutralButtonVisible;
    private boolean neutralButtonEnabled;
    private String neutralButtonText;

    private DialogContentImpl showContent;
    private RemainFinishImpl remainFinish;
    private List<DialogContentImpl> remainContentList;

    private ViewLocationImpl positiveClick;
    private View.OnClickListener neutralClick;

    public WizardDialog(Context context, View view, boolean fullScreen) {
        neutralButtonEnabled = neutralButtonVisible = true;
        remainContentList = new ArrayList<>();
        //加入倒數內容清單
        if (view instanceof DialogContentImpl) {
            DialogContentImpl content = (DialogContentImpl) view;
            if (content.isRemainTime())
                remainContentList.add(content);
        }
        this.mContents = new ArrayList<>();
        this.mContents.add(view);
        this.mSize = 1;
        init(context, fullScreen);
    }

    public WizardDialog(Context context, List<View> contents, boolean fullScreen) {
        Log.i("WizardDialog","jasbvlkndklcnsndjcnjsdcndjcncjyo67t5re4");
        neutralButtonEnabled = neutralButtonVisible = true;
        remainContentList = new ArrayList<>();
        if (contents != null && (mSize = contents.size()) > 0) {
            //加入倒數內容清單
            //noinspection Convert2streamapi
            for (View view : contents) {
                if (view instanceof DialogContentImpl) {
                    DialogContentImpl content = (DialogContentImpl) view;
                    if (content.isRemainTime())
                        remainContentList.add(content);
                }
            }
            this.mContents = contents;
        }
        init(context, fullScreen);
    }

    private void init(Context context, boolean fullScreen) {
        this.isDispose = false;
        this.mFirstShow = true;
        this.mStrNext = context.getString(R.string.button_next);
        this.mStrConfirm = context.getString(R.string.button_confirm);

        mHandler = new Handler();
        mHandler.post(this);
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_wizard);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            if (fullScreen) {
                lp.width = OrgMainActivity.getScreenWidth() - 30;
                lp.height = OrgMainActivity.getScreenHeight() - 30;
            }
            else {
                lp.width = OrgMainActivity.getScreenWidth() - 50;
                lp.height = OrgMainActivity.getScreenHeight() - 200;
            }
            window.setAttributes(lp);
        }

        txtTitle = (TextView) mDialog.findViewById(R.id.txtDialogTitle);
        txtTitleTime = (TextView) mDialog.findViewById(R.id.txtDialogTitleTime);
        svContent = (ScrollView) mDialog.findViewById(R.id.svDialogContent);
        btnNegative = (Button) mDialog.findViewById(R.id.btnDialogNegative);
        btnPositive = (Button) mDialog.findViewById(R.id.btnDialogPositive);
        btnNeutral = (Button) mDialog.findViewById(R.id.btnDialogNeutral);
        btnNegative.setOnClickListener(this);
        btnPositive.setOnClickListener(this);
        btnNeutral.setOnClickListener(this);

        //隱藏起地圖選項
        //btnPositive.setVisibility(View.INVISIBLE);
    }

    public void dispose() {
        isDispose = true;
        mHandler.removeCallbacksAndMessages(null);
    }

    private void updateButtonStatus() {
        //判斷是否有上一步
        btnNegative.setVisibility(mIndex == 0 ? View.INVISIBLE : View.VISIBLE);
        //判斷是否顯示下一步按鈕
        if (neutralButtonVisible) {
            btnNeutral.setVisibility(View.VISIBLE);
            //判斷下一步的顯示文字
            if (mIndex + 1 == mSize) {
                //判斷是否顯示自訂文字
                if (neutralButtonText == null) {
                    btnNeutral.setText(mStrConfirm);
                    btnNeutral.setEnabled(true);
                }
                else {
                    btnNeutral.setText(neutralButtonText);
                    btnNeutral.setEnabled(neutralButtonEnabled);
                }
            }
            else {
                btnNeutral.setText(mStrNext);
                btnNeutral.setEnabled(true);
            }
        }
        else btnNeutral.setVisibility(View.INVISIBLE);
        //判斷是否顯示查看地點
        //btnPositive.setVisibility(View.INVISIBLE);
        btnPositive.setVisibility(showContent.getGuidePosition() == null ? View.INVISIBLE : View.VISIBLE);
    }

    public void setNeutralOnClickListener(View.OnClickListener listener) { neutralClick = listener; }

    public void setPositiveOnClickListener(ViewLocationImpl listener) { positiveClick = listener; }

    public void setOnRemainFinishListener(RemainFinishImpl listener) { remainFinish = listener; }

    public void setNeutralButtonStatus(String text, boolean enabled) {
        neutralButtonText = text;
        neutralButtonEnabled = enabled;
    }

    public void setNeutralButtonVisible(boolean visible) {
        neutralButtonVisible = visible;
    }

    public void setMessageControl(TextView tv) { this.txtMessage = tv; }

    public void dismiss() {
        mDialog.dismiss();
    }

    public boolean isShowing() { return mDialog.isShowing(); }

    public WizardDialog show() {
        if (mFirstShow) {
            mIndex = 0;
            //清空標題列
            txtTitle.setText("");
            txtTitleTime.setText("");
            svContent.removeAllViews();
            if (mSize > 0) {
                View view = mContents.get(0);
                if (view instanceof DialogContentImpl) {
                    showContent = (DialogContentImpl) view;
                    showContent.show();
                    txtTitle.setText(showContent.getTitle());
                    if (txtMessage != null)
                        txtMessage.setText(showContent.getMessage());
                }
                svContent.addView(view);
            }
            updateButtonStatus();
            mFirstShow = false;
        }
        mDialog.show();
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //上一步
            case R.id.btnDialogNegative:
                mIndex--;
                if (showContent != null)
                    showContent.hidden();
                View vp = mContents.get(mIndex);
                if (vp instanceof DialogContentImpl) {
                    showContent = (DialogContentImpl) vp;
                    showContent.show();
                    txtTitle.setText(showContent.getTitle());
                    txtTitleTime.setText("");
                    if (txtMessage != null)
                        txtMessage.setText(showContent.getMessage());
                }
                svContent.removeAllViews();
                svContent.addView(vp);
                updateButtonStatus();
                break;
            //查看地點
            case R.id.btnDialogPositive:
                dismiss();
                if (showContent != null && positiveClick != null) {
                    positiveClick.onViewLocationClick(showContent.getGuidePosition(), showContent.getGuideName());
                }
                break;
            //下一步
            case R.id.btnDialogNeutral:
                if (mIndex + 1 == mSize) {
                    dismiss();
                    if (neutralClick != null) {
                        v.setTag(showContent);
                        neutralClick.onClick(v);
                    }
                }
                else {
                    mIndex++;
                    if (showContent != null)
                        showContent.hidden();
                    View vn = mContents.get(mIndex);
                    if (vn instanceof DialogContentImpl) {
                        showContent = (DialogContentImpl) vn;
                        showContent.show();
                        txtTitle.setText(showContent.getTitle());
                        txtTitleTime.setText("");
                        if (txtMessage != null)
                            txtMessage.setText(showContent.getMessage());
                    }
                    svContent.removeAllViews();
                    svContent.addView(vn);
                    updateButtonStatus();
                }
                break;
        }
    }

    @Override
    public void run() {
        //判斷是否需顯示倒數時間
        if (!isDispose && remainContentList.size() > 0) {
            Iterator<DialogContentImpl> i = remainContentList.iterator();
            while (i.hasNext()) {
                //must be called before you can call i.remove()
                DialogContentImpl item = i.next();
                //取得倒數的值
                String times = item.getRemainTime();
                //判斷是否更新標題列及訊息列
                if (item == showContent) {
                    if (item.showRemainInTitle())
                        this.txtTitleTime.setText(String.format(Locale.getDefault(), "(%s)", times));
                    if (item.showRemainInMessage() && txtMessage != null)
                        this.txtMessage.setText(item.getMessage());
                }
                //判斷是否倒數完成
                if (times.equals("00:00")) {
                    if (remainFinish != null)
                        remainFinish.onRemainFinish(item.getRemainIndex());
                    i.remove();
                }
            }
            mHandler.postDelayed(this, 1000);
        }
    }

}