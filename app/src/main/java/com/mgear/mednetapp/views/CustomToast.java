package com.mgear.mednetapp.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Jye on 2017/8/15.
 * class: CustomToast
 */
public class CustomToast {

    /**
     * 短時間訊息
     */
    public static final int SHORT = android.widget.Toast.LENGTH_SHORT;

    /**
     * 長時間訊息
     */
    public static final int LONG = android.widget.Toast.LENGTH_LONG;

    /**
     * 類別位置
     * @author magiclen
     */
    public enum Position {
        CENTER, CENTER_BOTTOM, CENTER_TOP, CENTER_LEFT, CENTER_RIGHT, LEFT_TOP, RIGHT_TOP, LEFT_BOTTOM, RIGHT_BOTTOM
    }

    private static final int offsetFactor = 12;
    private Context context;
    private android.widget.Toast toast;
    private Timer timer;
    private Position toastPosition = Position.CENTER_BOTTOM;
    private int offsetX;
    private int offsetY;
    private ToastView toastView;

    private void useDefaultOffset() {
        final DisplayMetrics dm = context.getResources().getDisplayMetrics();
        setOffset(dm.widthPixels / offsetFactor, dm.heightPixels / offsetFactor);
    }

    private void setOffset(final int offsetX, final int offsetY) {
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public CustomToast(final Context context) {
        this.toast = new android.widget.Toast(context);
        this.context = context;
        this.toastView = new ToastView(context);
        this.toastView.setRadius(10);
        this.toastView.setTextSize(20);
        this.toastView.setBackgroundColor(Color.BLACK);
        this.toast.setView(this.toastView);
    }

    public CustomToast(final Context context, final ToastView view) {
        this.toast = new android.widget.Toast(context);
        this.context = context;
        this.toastView = view;
        this.toast.setView(view);
        useDefaultOffset();
    }

    public void setInterval(int interval) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                ((Activity)context).runOnUiThread(() -> showToast());
            }
        }, 0, interval);
    }

    public void cancelInterval() {
        if (timer == null)
            return;
        timer.cancel();
        timer = null;
        toast.cancel();
    }

    public void setPosition(final Position position) {
        toastPosition = position == null ? Position.CENTER_BOTTOM : position;
        useDefaultOffset();
    }

    public void setPosition(final Position position, final int offsetX, final int offsetY) {
        toastPosition = position == null ? Position.CENTER_BOTTOM : position;
        setOffset(offsetX, offsetY);
    }

    private void showToast() {
        // 設定氣泡訊息的位置
        int gravity = 0, offsetX = 0, offsetY = 0;
        switch (toastPosition) {
            case CENTER:
                gravity = Gravity.CENTER;
                break;
            case CENTER_BOTTOM:
                gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
                offsetY = this.offsetY;
                break;
            case CENTER_TOP:
                gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
                offsetY = this.offsetY;
                break;
            case CENTER_LEFT:
                gravity = Gravity.START | Gravity.CENTER_VERTICAL;
                offsetX = this.offsetX;
                break;
            case CENTER_RIGHT:
                gravity = Gravity.END | Gravity.CENTER_VERTICAL;
                offsetX = this.offsetX;
                break;
            case LEFT_TOP:
                gravity = Gravity.START | Gravity.TOP;
                offsetX = this.offsetX;
                offsetY = this.offsetY;
                break;
            case LEFT_BOTTOM:
                gravity = Gravity.START | Gravity.BOTTOM;
                offsetX = this.offsetX;
                offsetY = this.offsetY;
                break;
            case RIGHT_TOP:
                gravity = Gravity.END | Gravity.TOP;
                offsetX = this.offsetX;
                offsetY = this.offsetY;
                break;
            case RIGHT_BOTTOM:
                gravity = Gravity.END | Gravity.BOTTOM;
                offsetX = this.offsetX;
                offsetY = this.offsetY;
                break;
        }
        toast.setGravity(gravity, offsetX, offsetY);
        toast.show();
    }

    public void showToast(final String text, final int duration) {
        toast.setDuration(duration);
        //判斷是否重新設定訊息顯示內容
        if (text != null) {
            if (toastView == null)
                toast.setText(text);
            else
                toastView.setText(text);
        }
        showToast();
    }

}