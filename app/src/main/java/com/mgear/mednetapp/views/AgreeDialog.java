package com.mgear.mednetapp.views;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.CheckableListAdapter;
import com.mgear.mednetapp.enums.AgreeEnum;
import com.mgear.mednetapp.interfaces.organization.AgreeClickImpl;
import com.mgear.mednetapp.entity.organization.CheckableItem;

/**
 * Created by Jye on 2017/8/26.
 * class: AgreeDialog
 */
public class AgreeDialog implements View.OnClickListener, AdapterView.OnItemClickListener {

    private Dialog mDialog;
    private Button btnPositive;
    private Button btnNegative;
    private AgreeEnum agreeType;

    private CheckableItem[] contents;
    private CheckableListAdapter adapter;

    private AgreeClickImpl positiveClick;

    public AgreeDialog(Context context, AgreeEnum type, String title, String help, CheckableItem[] content, boolean fullScreen) {
        agreeType = type;
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_agree);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            if (fullScreen) {
                lp.width = OrgMainActivity.getScreenWidth() - 30;
                lp.height = OrgMainActivity.getScreenHeight() - 30;
            }
            else {
                lp.width = OrgMainActivity.getScreenWidth() - 50;
                lp.height = OrgMainActivity.getScreenHeight() - 200;
            }
            window.setAttributes(lp);
        }

        TextView txtTitle = (TextView) mDialog.findViewById(R.id.txtAgreeDialogTitle);
        txtTitle.setText(title);
        TextView txtHelp = (TextView) mDialog.findViewById(R.id.txtAgreeDialogHelp);
        txtHelp.setText(help);
        ListView lstContent = (ListView) mDialog.findViewById(R.id.lstAgreeDialogContent);
        adapter = new CheckableListAdapter(context, content);
        lstContent.setAdapter(adapter);
        lstContent.setOnItemClickListener(this);
        contents = content;

        btnPositive = (Button) mDialog.findViewById(R.id.btnAgreeDialogPositive);
        btnPositive.setOnClickListener(this);
        btnNegative = (Button) mDialog.findViewById(R.id.btnAgreeDialogNegative);
        btnNegative.setOnClickListener(this);
    }

    public AgreeDialog show() {
        mDialog.show();
        return this;
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public boolean isShowing() { return mDialog.isShowing(); }

    public void setPositiveOnClickListener(AgreeClickImpl listener) { positiveClick = listener; }

    public void setNegativeButtonVisible(boolean visible) { btnNegative.setVisibility(visible ? View.VISIBLE : View.INVISIBLE); }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAgreeDialogPositive:
                if (positiveClick != null)
                    positiveClick.onAgreeClick(agreeType);
                break;
            case R.id.btnAgreeDialogNegative:
                break;
        }
        mDialog.dismiss();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CheckableItem checkableItem = contents[position];
        boolean check = checkableItem.isCheck();
        checkableItem.setCheck(!check);
        boolean agree = true;
        for (CheckableItem item : contents)
            agree = agree && item.isCheck();
        btnPositive.setEnabled(agree);
        adapter.notifyDataSetChanged();
    }

}