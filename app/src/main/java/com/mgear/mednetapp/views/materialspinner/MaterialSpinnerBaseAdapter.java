package com.mgear.mednetapp.views.materialspinner;

import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.mgear.mednetapp.R;

import java.util.List;

abstract class MaterialSpinnerBaseAdapter<T> extends BaseAdapter {

    private final Context context;
    private int selectedIndex;
    private int textColor;

    MaterialSpinnerBaseAdapter(Context context) {
        this.context = context;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        final TextView textView;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.ms_list_item, parent, false);
            textView = (TextView) convertView.findViewById(R.id.tv_tinted_spinner);
            textView.setTextColor(textColor);

            Configuration config = context.getResources().getConfiguration();
            if (config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
                textView.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            convertView.setTag(new ViewHolder(textView));
        } else {
            textView = ((ViewHolder) convertView.getTag()).textView;
        }
        textView.setText(getItem(position).toString());
        return convertView;
    }

    int getSelectedIndex() {
        return selectedIndex;
    }

    void notifyItemSelected(int index) {
        selectedIndex = index;
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public abstract T getItem(int position);

    @Override public abstract int getCount();

    public abstract T get(int position);

    public abstract List<T> getItems();

    public MaterialSpinnerBaseAdapter<T> setTextColor(int textColor) {
        this.textColor = textColor;
        return this;
    }

    private static class ViewHolder {

        private TextView textView;

        private ViewHolder(TextView textView) {
            this.textView = textView;
        }

    }

}