package com.mgear.mednetapp.views;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.net.http.SslError;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mgear.mednetapp.R;

/**
 * Created by Jye on 2017/8/14.
 * class: VideoDialog
 */
class VideoDialog implements View.OnClickListener {

    private Dialog mDialog;

    private WebView mWebView;
    private TextView txtMessage;
    private ProgressBar progressBar;

    private class HavoWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            txtMessage.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }

//        @Override
//        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            //全部擋掉
//            handler.cancel();
//        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    VideoDialog(Context context, String url) {
        mDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_video);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        progressBar = (ProgressBar) mDialog.findViewById(R.id.pbBrowserVideoWait);
        txtMessage = (TextView) mDialog.findViewById(R.id.txtBrowserVideoMessage);
        mWebView = (WebView) mDialog.findViewById(R.id.wvDialogVideo);
        View imgClose = mDialog.findViewById(R.id.imgDialogVideoClose);
        imgClose.setOnClickListener(this);

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new HavoWebViewClient());

        //載入頁面內容
        mWebView.loadUrl(url);
    }

    public VideoDialog show() {
        mDialog.show();
        return this;
    }

    @Override
    public void onClick(View v) {
        mWebView.stopLoading();
        mWebView.loadUrl("about:blank");
        mDialog.dismiss();
    }

}