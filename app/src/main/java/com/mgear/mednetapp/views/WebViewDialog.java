package com.mgear.mednetapp.views;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.mgear.mednetapp.R;

/**
 * Created by Jye on 2017/8/22.
 * class: WebViewDialog
 */
public class WebViewDialog implements View.OnClickListener {

    private Dialog mDialog;

    @SuppressLint("SetJavaScriptEnabled")
    public WebViewDialog(Context context, String content) {
        mDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_web_view);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        View imgClose = mDialog.findViewById(R.id.btnDialogWebViewReturn);
        imgClose.setOnClickListener(this);
        //設定內容
        WebView videoView = (WebView) mDialog.findViewById(R.id.webViewDialog);
        WebSettings webSettings = videoView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        videoView.loadData(content, "text/html; charset=UTF-8", null);
    }

    public WebViewDialog show() {
        mDialog.show();
        return this;
    }

    @Override
    public void onClick(View v) {
        mDialog.dismiss();
    }

}