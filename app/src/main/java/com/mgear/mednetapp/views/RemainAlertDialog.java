package com.mgear.mednetapp.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.enums.RemainEnum;
import com.mgear.mednetapp.interfaces.organization.RemainFinishImpl;

/**
 * Created by Jye on 2017/9/6.
 * class: RemainAlertDialog
 */
public class RemainAlertDialog implements Runnable, View.OnClickListener {

    private Dialog mDialog;
    private Handler mHandler;

    private int remainSeconds;
    private String remainText;
    private RemainFinishImpl remainFinish;

    private TextView txtMessage;

    public RemainAlertDialog(Context context, String title, String message, int seconds) {
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_remain_alert);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        Window window = mDialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = OrgMainActivity.getScreenWidth() - 50;
            window.setAttributes(lp);
        }

        remainSeconds = seconds + 1;
        remainText = message;

        TextView txtTitle = (TextView) mDialog.findViewById(R.id.txtRemainAlertTitle);
        txtTitle.setText(title);
        txtMessage = (TextView) mDialog.findViewById(R.id.txtRemainAlertMessage);
        Button btnPositive = (Button) mDialog.findViewById(R.id.btnRemainAlertPositive);
        Button btnNegative = (Button) mDialog.findViewById(R.id.btnRemainAlertNegative);
        btnPositive.setOnClickListener(this);
        btnNegative.setOnClickListener(this);
    }

    public void setOnRemainFinishListener(RemainFinishImpl listener) { remainFinish = listener; }

    public void show() {
        mDialog.show();
        //判斷是否有設定倒數
        if (remainSeconds > 0) {
            mHandler = new Handler();
            mHandler.post(this);
            txtMessage.setText(String.format(remainText, remainSeconds));
        }
    }

    public void dismiss() {
        mDialog.dismiss();
        mHandler.removeCallbacks(this);
    }

    @Override
    public void run() {
        if (mDialog.isShowing() && remainSeconds > 0) {
            //顯示倒數的訊息
            remainSeconds--;
            txtMessage.setText(String.format(remainText, remainSeconds));
            //判斷是否倒數完成
            if (remainSeconds == 0) {
                dismiss();
                if (remainFinish != null)
                    remainFinish.onRemainFinish(RemainEnum.HelpProvider);
            }
            else mHandler.postDelayed(this, 1000);
        }
    }

    @Override
    public void onClick(View v) {
        mDialog.dismiss();
        if (v.getId() == R.id.btnRemainAlertPositive && remainFinish != null)
            remainFinish.onRemainFinish(RemainEnum.HelpProvider);
    }

}