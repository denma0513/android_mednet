package com.mgear.mednetapp.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.mgear.mednetapp.adapter.HealthListViewAdapter;
import com.mgear.mednetapp.adapter.HealthRecordListAdapter;
import com.mgear.mednetapp.adapter.HomePageViewAdapter;
import com.mgear.mednetapp.adapter.RefreshHeaderAdapter;
import com.mgear.mednetapp.enums.RefreshViewEnum;
import com.mgear.mednetapp.interfaces.IRefreshHeader;
import com.mgear.mednetapp.interfaces.OnRefreshListener;

public class RefreshHeaderRecyclerView extends RecyclerView {

    private float mLastY = -1;
    private float sumOffSet;
    private IRefreshHeader mRefreshHeader;
    private boolean mRefreshing = false;

    private OnRefreshListener mRefreshListener;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }


    //建構子
    public RefreshHeaderRecyclerView(Context context) {
        this(context, null);
    }

    public RefreshHeaderRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RefreshHeaderRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setAdapter(Adapter adapter, RefreshViewEnum viewEnum) {
        if (adapter != null) {
            switch (viewEnum) {
                case homePageRefreshView:
                    HomePageViewAdapter mHomePageAdapter;
                    mHomePageAdapter = (HomePageViewAdapter) adapter;
                    mRefreshHeader = new ArrowRefreshHeader(getContext().getApplicationContext());
                    mHomePageAdapter.setRefreshHeader(mRefreshHeader);
                    break;
                case healthListRefreshView:
                    HealthListViewAdapter mHealthListViewAdapter;
                    mHealthListViewAdapter = (HealthListViewAdapter) adapter;
                    mRefreshHeader = new ArrowRefreshHeader(getContext().getApplicationContext());
                    mHealthListViewAdapter.setRefreshHeader(mRefreshHeader);
                    break;
                case healthRecordListRefreshView:
                    HealthRecordListAdapter mHealthRecordListAdapter;
                    mHealthRecordListAdapter = (HealthRecordListAdapter) adapter;
                    mRefreshHeader = new ArrowRefreshHeader(getContext().getApplicationContext());
                    mHealthRecordListAdapter.setRefreshHeader(mRefreshHeader);
                    break;
                default:
                    RefreshHeaderAdapter mHeaderAdapter;
                    mHeaderAdapter = (RefreshHeaderAdapter) adapter;
                    mRefreshHeader = new ArrowRefreshHeader(getContext().getApplicationContext());
                    mHeaderAdapter.setRefreshHeader(mRefreshHeader);
                    break;
            }

        }
        super.setAdapter(adapter);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (mLastY == -1) {
            mLastY = e.getRawY();
        }
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastY = e.getRawY();
                sumOffSet = 0;
                break;
            case MotionEvent.ACTION_MOVE:
                float deltaY = (e.getRawY() - mLastY) / 2;//為了防止滑動幅度過大，将將實際滑動距離除以2
                mLastY = e.getRawY();
                sumOffSet += deltaY;//計算總滑動距離
                if (isOnTop() && !mRefreshing) {
                    mRefreshHeader.onMove(deltaY, sumOffSet);//移動已更新的header
                    if (mRefreshHeader.getVisibleHeight() > 0) {
                        return false;
                    }
                }
                break;
            default:
                mLastY = -1; // reset
                if (isOnTop() && !mRefreshing) {
                    if (mRefreshHeader.onRelease()) {
                        //鬆開手指
                        if (mRefreshListener != null) {
                            mRefreshing = true;
                            mRefreshListener.onRefresh();
                        }
                    }
                }
                break;
        }
        return super.onTouchEvent(e);
    }

    private boolean isOnTop() {
        return mRefreshHeader.getHeaderView().getParent() != null;
    }

    public void setOnRefreshListener(OnRefreshListener listener) {
        mRefreshListener = listener;
    }

    public void refreshComplete() {
        if (mRefreshing) {
            mRefreshing = false;
            mRefreshHeader.refreshComplete();

        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

}
