package com.mgear.mednetapp.views;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/7/29.
 * class: BarcodeDialog
 */
public class BarcodeDialog implements View.OnClickListener {

    private Dialog mDialog;

    public BarcodeDialog(Context context) {
        mDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_barcode);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        View imgClose = mDialog.findViewById(R.id.imgDialogBarcodeClose);
        imgClose.setOnClickListener(this);
        //設定條碼圖示
        ImageView imgBarcode = (ImageView) mDialog.findViewById(R.id.imgDialogBarcode);
        try {
            imgBarcode.setImageBitmap(CommonFunc.encodeAsBitmap(CustomerActivity.getCustomer().getContactEmsCode(), BarcodeFormat.CODE_39, 320, 80));
        }
        catch (Exception ignored) { }
    }

    public BarcodeDialog show() {
        mDialog.show();
        return this;
    }

    @Override
    public void onClick(View v) {
        mDialog.dismiss();
    }

}