package com.mgear.mednetapp.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.facebook.common.Common;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.health.HealthBaseSet;
import com.mgear.mednetapp.entity.health.HealthBloodPressureSet;
import com.mgear.mednetapp.utils.CommonFunc;

import java.util.ArrayList;
import java.util.List;

public class ProgressbarView extends View {


    private final int DEFAULT_WIDTH = 450;
    private final int DEFAULT_HEIGTH = 40;
    private final int DEFAULT_SHAPE = 0;
    private final int DEFAULT_MAX = 100;
    private final int DEFAULT_PROGRESS_TARGET = 100;
    private final int DEFAULT_PROGRESS_STAGE = 0;
    private final int DEFAULT_PROGRESS = 0;
    private final int DEFAULT_PROGRESS_COLOR = Color.parseColor("#76B034");
    private final int DEFAULT_PROGRESS_BACK_COLOR = Color.parseColor("#EFEFEF");
    private final int DEFAULT_CIRCLE_R = 50;
    private final int DEFAULT_SQUARE_R = 20;
    private final int DEFAULT_TEXT_SIZE = 30;
    private Context mContext;
    private HealthBaseSet healthSet;
    private int margin = 0;

    private enum ShapeType {
        RECTANGLE,
        CIRCLE,
        SQUARE
    }

    private int target = DEFAULT_PROGRESS_TARGET;
    private int progress = DEFAULT_PROGRESS;
    private Double progress2;
    private int max = DEFAULT_MAX;
    private Double max2;
    private float mwidth = DEFAULT_WIDTH;
    private float mhight = DEFAULT_HEIGTH;
    private int mShapeType = DEFAULT_SHAPE;
    private int proColor = DEFAULT_PROGRESS_COLOR;
    private int proBackColor = DEFAULT_PROGRESS_BACK_COLOR;
    private int textLowColor = Color.BLACK;
    private int textMiddleColor = Color.BLUE;
    private int textHighColor = Color.RED;
    private int progressStage = DEFAULT_PROGRESS_STAGE;
    private int progressSegmentColor = proColor;
    private int progressDoubleSegColor = Color.GRAY;

    private float squareRadius = DEFAULT_SQUARE_R;
    private float textSize = DEFAULT_TEXT_SIZE;

    private boolean showText;
    private float circleX = DEFAULT_CIRCLE_R + 20;
    private float circleR = DEFAULT_CIRCLE_R;

    private float startX;
    private float startY;
    private float startYCenter;
    private float startR = -90;
    private Paint paint;
    private Paint paint2;
    RectF circleRectf, squareRectf;
    private List<Integer> progressList;
    private List<Double> progressList2 = new ArrayList<Double>();
    private List<Double> progressListBloodPressure;

    public ProgressbarView(Context context) {
        this(context, null);
        mContext = context;
    }

    public ProgressbarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        mContext = context;
    }

    public ProgressbarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
        // Load the styled attributes and set their properties
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ProgressbarView, defStyleAttr, 0);
        mwidth = attributes.getDimension(R.styleable.ProgressbarView_p_width, DEFAULT_WIDTH);
        mhight = attributes.getDimension(R.styleable.ProgressbarView_p_height, DEFAULT_HEIGTH);
        mShapeType = attributes.getInteger(R.styleable.ProgressbarView_p_shapeType, DEFAULT_SHAPE);
        showText = attributes.getBoolean(R.styleable.ProgressbarView_p_showText, false);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        Log.i("slack", "lp.width = " + lp.width);

        if (mShapeType == 1) {
            circleR = attributes.getDimension(R.styleable.ProgressbarView_p_circle_radius, DEFAULT_CIRCLE_R);
//            showText = true;
            circleX = attributes.getDimension(R.styleable.ProgressbarView_p_circle_X_Y, circleR + 20);
        }
        if (mShapeType == 2) {
            squareRadius = attributes.getDimension(R.styleable.ProgressbarView_p_square_radius, DEFAULT_SQUARE_R);
        }

        if (showText) {
            textSize = attributes.getDimensionPixelSize(R.styleable.ProgressbarView_p_textSize, DEFAULT_TEXT_SIZE);
        }
        max = attributes.getInteger(R.styleable.ProgressbarView_p_maxValue, DEFAULT_MAX);
        progress = attributes.getInteger(R.styleable.ProgressbarView_p_progressValue, DEFAULT_PROGRESS);
        progressStage = attributes.getInteger(R.styleable.ProgressbarView_p_progressStage, DEFAULT_PROGRESS_STAGE);
        proColor = attributes.getColor(R.styleable.ProgressbarView_p_progressColor, DEFAULT_PROGRESS_COLOR);
        proBackColor = attributes.getColor(R.styleable.ProgressbarView_p_progressBackColor, DEFAULT_PROGRESS_BACK_COLOR);
        textLowColor = attributes.getColor(R.styleable.ProgressbarView_p_textLowColor, Color.BLACK);
        textMiddleColor = attributes.getColor(R.styleable.ProgressbarView_p_textMiddleColor, Color.BLUE);
        textHighColor = attributes.getColor(R.styleable.ProgressbarView_p_textHighColor, Color.RED);
        progressSegmentColor = attributes.getColor(R.styleable.ProgressbarView_p_progressSegmentColor, DEFAULT_PROGRESS_COLOR);
        progressDoubleSegColor = attributes.getColor(R.styleable.ProgressbarView_p_progressDoubleSegColor, Color.GRAY);

        mhight = 40;
        margin = CommonFunc.dip2px(mContext, 15);

        Log.i("slack", mwidth + "," + mhight + "," + mShapeType + "," + max + "," +
                progress + "," + showText + "," + squareRadius + "," + textSize);
        paint = new Paint();
        paint2 = new Paint();
        circleRectf = new RectF();
        squareRectf = new RectF();
        progressList = new ArrayList<>();

        if (progressStage > 0) {
            for (int i = 0; i < progressStage; i++) {
                int value = (int) (target * ((float) (i + 1) / progressStage));
                progressList.add(value);
            }
        }

        paint.setAntiAlias(true);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor("#EFEFEF"));
        paint.setStrokeWidth(10);
//        paint.setStyle(Paint.Style.STROKE);
        paint.setStyle(Paint.Style.FILL);

        paint2.setAntiAlias(true);
        paint2.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint2.setStyle(Paint.Style.FILL);

    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        Log.i("slack", "widthMeasureSpec  "+widthMeasureSpec+" heightMeasureSpec + "+heightMeasureSpec);
//        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
//                getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
//    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.i("slack", "onDraw  ");
        super.onDraw(canvas);

        paint.setColor(proBackColor);
        paint.setStrokeWidth(10);
        paint.setTextSize(textSize);
        startX = margin;
        startY = CommonFunc.dip2px(mContext, 20);
        startYCenter = (float) (startY + (mhight / 2.0));
        mhight = 40;//初始化高度

        switch (mShapeType) {
            case 2:
                //畫顏色區塊
                Log.i("slack", "mwidth = " + mwidth + " margin = " + margin);
                mhight = CommonFunc.dip2px(mContext, mhight);
                if (progressList2.size() > 0) {
                    int i = 0;
                    float statusX = margin;
                    for (Double pro : progressList2) {
                        progressSegmentColor = getResources().getColor(healthSet.getColorArr()[i]);

                        paint.setColor(progressSegmentColor);
                        if (healthSet.getHealthType().equals("4") && i == 1)
                            paint.setColor(getResources().getColor(R.color.gray));

                        if (healthSet.getHealthType().equals("4") || healthSet.getHealthType().equals("6")) {
                            float actionWidth = 0;
                            if (i == 0) {
                                actionWidth = ((float) (pro / progressList2.get(i + 1))) * mwidth + margin;
                            } else {
                                actionWidth = mwidth - actionWidth;
                            }

                            if (actionWidth >= mwidth) {
                                actionWidth = mwidth;
                            }

                            Log.i("slack", "actionWidth = " + actionWidth);
                            squareRectf.set(startX, startY, actionWidth, startY + mhight);
                            canvas.drawRoundRect(squareRectf, squareRadius, squareRadius, paint);
                            startX = actionWidth;
                            Log.i("slack", "pro = " + pro + " healthSet.getStatusTitle() = " + healthSet.getStatusArr()[i] + textSize);
                            i++;

                        } else {
                            float actionWidth = ((float) (i + 1) / progressList2.size() * mwidth);
                            if (statusX == margin) {
                                statusX = actionWidth / 2;
                            } else {
                                statusX = startX + ((actionWidth - startX) / 2);
                            }
                            squareRectf.set(startX, startY, actionWidth, startY + mhight);
                            canvas.drawRoundRect(squareRectf, squareRadius, squareRadius, paint);
                            startX = actionWidth;
                            //Log.i("slack", "pro = " + pro + " healthSet.getStatusTitle() = " + healthSet.getStatusArr()[i] + textSize);
                            i++;
                        }

                    }

                    /***** 畫外框 *****/

                    Log.i("slack", "畫外框 = " + startY + " " + ((startY + mhight) + 1));

                    Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_progress);
                    Rect dst = new Rect(margin, (int) startY, (int) mwidth, (int) (startY + mhight) + 1); //不知道為什麼總是會突出一點點，所以加一
                    canvas.drawBitmap(mBitmap, null, dst, null);

                    /***** 畫數值 *****/
                    i = 0;
                    startX = margin;
                    //startY = 50;
                    statusX = margin;

                    int subY = CommonFunc.dip2px(mContext, 5);
                    int plusY = (int) (CommonFunc.dip2px(mContext, +10) + mhight);

                    for (Double pro : progressList2) {
                        //float actionWidth = (float) ((pro / max2) * mwidth);
                        float actionWidth = ((float) (i + 1) / progressList2.size() * mwidth);
                        if (statusX == margin) {
                            statusX = actionWidth / 2;
                        } else {
                            statusX = startX + ((actionWidth - startX) / 2);
                        }
                        Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
                        Paint textPaint = new Paint();
                        textPaint.setAntiAlias(true);
                        textPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
                        textPaint.setStrokeWidth(10);
                        textPaint.setTextSize(CommonFunc.dip2px(mContext, 14));
                        textPaint.setTypeface(font);
                        textPaint.setColor(getResources().getColor(R.color.home_title_black_deep));


                        if (!healthSet.getHealthType().equals("2")) {
                            //血壓不顯示
                            if (healthSet.getHealthType().equals("4")) {
                                //飲水
                                String val = pro.toString().split("\\.")[0];
                                if (i == progressList2.size() - 1)
                                    canvas.drawText(val, actionWidth - 100, startY - subY, textPaint);
                            } else if (healthSet.getHealthType().equals("6")) {
                                //步數
                                String val = pro.toString().split("\\.")[0] + "步";
                                if (i == progressList2.size() - 1) {
                                    canvas.drawText(val, actionWidth - 150, startY - subY, textPaint);
                                } else {
                                    canvas.drawText(val, actionWidth - 60, startY - subY, textPaint);
                                }
                            } else {
                                String val = "";
                                val = pro.toString();
                                if (!healthSet.getHealthType().equals("7") && !healthSet.getHealthType().equals("5")) {

                                    val = Integer.parseInt(pro.toString().split("\\.")[0]) + "";
                                }

                                if (i == progressList2.size() - 1) {
                                    canvas.drawText(val, actionWidth - 100, startY - subY, textPaint);
                                } else {
                                    canvas.drawText(val, actionWidth - 60, startY - subY, textPaint);
                                }
                            }

                            if (i != progressList2.size() - 1) {

                            }
                        }

                        if (!healthSet.getHealthType().equals("4") && !healthSet.getHealthType().equals("6")) {
                            //飲水跟步數不顯示

                            if (healthSet.getHealthType().equals("1")) {
                                if (i != progressList2.size() - 1) {
                                    canvas.drawText(healthSet.getStatusArr()[i], statusX - 20, startY + plusY, textPaint);
                                }
                            } else {
                                canvas.drawText(healthSet.getStatusArr()[i], statusX - 20, startY + plusY, textPaint);
                            }
                        }


                        startX = actionWidth;
                        i++;
                    }

                    /***** 畫指標圓形圖 start *****/
                    if (progress2 != null) {
                        if (healthSet.getHealthType().equals("2")) {
                            HealthBloodPressureSet set = (HealthBloodPressureSet) healthSet;
                            int dbp = set.getDbp();
                            int sbp = set.getSbp();

                            float valuePointStartW = margin;
                            float valuePointStartWTemp = margin;

                            //收縮壓
                            for (i = 0; i < progressList2.size(); i++) {
                                //區塊的寬度
                                float rangeWidth = mwidth * (((float) 1) / progressList2.size());

                                if (sbp <= progressList2.get(i)) {
                                    //在這個區域內
                                    //數值在區塊中的比例
                                    double proportion = 1.0;
                                    if (i == 0) {
                                        proportion = ((float) (sbp - 0) / (progressList2.get(i) - 0));
                                    } else {
                                        proportion = ((float) (sbp - progressList2.get(i - 1)) / (progressList2.get(i) - progressList2.get(i - 1)));
                                    }
                                    valuePointStartWTemp = (float) ((proportion * rangeWidth) + (rangeWidth * i)) + margin;
                                    break;
                                }

                                if (i == progressList2.size() - 1 && progress2 > progressList2.get(i)) {
                                    //爆表 把它隸屬在最後一個區塊
                                    valuePointStartWTemp = mwidth;
                                }
                            }

                            valuePointStartW = valuePointStartWTemp - 20;
                            mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_bp_pulse);
                            dst = new Rect((int) valuePointStartW, (int) startY - CommonFunc.dip2px(mContext, 20), (int) valuePointStartW +
                                    CommonFunc.dip2px(mContext, 30), (int) (startY + (mhight / 2)));
                            canvas.drawBitmap(mBitmap, null, dst, null);

                            valuePointStartW = margin;
                            valuePointStartWTemp = margin;
                            //舒張壓
                            for (i = 0; i < progressListBloodPressure.size(); i++) {
                                //區塊的寬度
                                float rangeWidth = mwidth * (((float) 1) / progressListBloodPressure.size());


                                if (dbp <= progressListBloodPressure.get(i)) {
                                    //在這個區域內
                                    //數值在區塊中的比例
                                    double proportion = 1.0;
                                    if (i == 0) {
                                        proportion = ((float) (dbp - 0) / (progressListBloodPressure.get(i) - 0));
                                    } else {
                                        proportion = ((float) (dbp - progressListBloodPressure.get(i - 1)) / (progressListBloodPressure.get(i) - progressListBloodPressure.get(i - 1)));
                                    }
                                    valuePointStartWTemp = (float) ((proportion * rangeWidth) + (rangeWidth * i)) + margin;
                                    break;
                                }


                                if (i == progressListBloodPressure.size() - 1 && progress2 > progressListBloodPressure.get(i)) {
                                    //爆表 把它隸屬在最後一個區塊
                                    valuePointStartWTemp = mwidth;
                                }

                            }

                            valuePointStartW = valuePointStartWTemp - 20;
                            Log.i("slack", "valuePointStartW = " + valuePointStartW);
                            mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_bp_dia);
                            dst = new Rect((int) valuePointStartW, (int) (startY + (mhight / 2)), (int) valuePointStartW +
                                    CommonFunc.dip2px(mContext, 30), (int) (startY + mhight) + CommonFunc.dip2px(mContext, 20));
                            canvas.drawBitmap(mBitmap, null, dst, null);

                        } else if (healthSet.getHealthType().equals("4") || healthSet.getHealthType().equals("6")) {
                            startYCenter = startY + (mhight / 2);//Y中心點

                            int p1 = CommonFunc.dip2px(mContext, 15);
                            int p2 = CommonFunc.dip2px(mContext, 14);
                            int p3 = CommonFunc.dip2px(mContext, 5);


                            Log.i("slack", "p1 = " + p1 + " p2 = " + p2 + " p3 = " + p3 + " margin = " + margin);
                            float actionWidth = ((float) (progressList2.get(0) / progressList2.get(1))) * mwidth + margin;
                            if ((progressList2.get(0) > progressList2.get(1))) {
                                actionWidth = mwidth;
                            }

                            float valuePointStartW = margin;
                            float valuePointStartWTemp = margin;

                            int valuePointStartY = (int) (startYCenter - p1);
                            int radius = p1 * 2;


                            valuePointStartWTemp = actionWidth;
                            valuePointStartW = valuePointStartWTemp - p1;
                            paint2.setColor(getResources().getColor(R.color.home_title_black_deep));
                            squareRectf.set(valuePointStartW, valuePointStartY, valuePointStartW + radius, valuePointStartY + radius);
                            canvas.drawRoundRect(squareRectf, radius, radius, paint2);

                            //valuePointStartW = (float) ((progress2 / max2) * mwidth) - 20;
                            valuePointStartW = valuePointStartWTemp - p2;
                            valuePointStartY = (int) (startYCenter - p2);
                            radius = p2 * 2;
                            //Log.i("slack", "valuePointStartW = " + valuePointStartW + " valuePointStartY = " + valuePointStartY + " radius = " + radius);
                            paint2.setColor(getResources().getColor(R.color.write_bg));
                            squareRectf.set(valuePointStartW, valuePointStartY, valuePointStartW + radius, valuePointStartY + radius);
                            canvas.drawRoundRect(squareRectf, radius, radius, paint2);

                            //valuePointStartW = (float) (((progress2 / max2) * mwidth) - 10);
                            valuePointStartW = valuePointStartWTemp - p3;
                            valuePointStartY = (int) (startYCenter - p3);
                            radius = p3 * 2;
                            //Log.i("slack", "valuePointStartW = " + valuePointStartW + " valuePointStartY = " + valuePointStartY + " radius = " + radius);
                            paint2.setColor(getResources().getColor(R.color.home_title_black_deep));
                            squareRectf.set(valuePointStartW, valuePointStartY, valuePointStartW + radius, valuePointStartY + radius);
                            canvas.drawRoundRect(squareRectf, radius, radius, paint2);

                        } else {
                            Log.i("slack", "progress2 = " + progress2);
                            int range = 0;
                            float valuePointStartW = margin;
                            float valuePointStartWTemp = margin;
                            for (i = 0; i < progressList2.size(); i++) {
                                //區塊的寬度
                                float rangeWidth = mwidth * (((float) 1) / progressList2.size());
                                Log.i("slack", "rangeWidth = " + rangeWidth);
                                //Log.i("slack", "rangeWidth = " + rangeWidth);
                                if (progress2 <= progressList2.get(i)) {
                                    //在這個區域內
                                    range = i;
                                    //數值在區塊中的比例
                                    double proportion = 1.0;
                                    if (i == 0) {
                                        proportion = ((float) (progress2 - 0) / (progressList2.get(i) - 0));
                                    } else {
                                        proportion = ((float) (progress2 - progressList2.get(i - 1)) / (progressList2.get(i) - progressList2.get(i - 1)));
                                    }
                                    valuePointStartWTemp = (float) ((proportion * rangeWidth) + (rangeWidth * i)) + margin;
                                    Log.i("slack", " proportion = " + proportion + " valuePointStartWTemp = " + valuePointStartWTemp);
                                    break;
                                }

                                if (i == progressList2.size() - 1 && progress2 > progressList2.get(i)) {
                                    //爆表 把它隸屬在最後一個區塊
                                    range = i;
                                    valuePointStartWTemp = mwidth;
                                }

                            }

                            int p1 = CommonFunc.dip2px(mContext, 15);
                            int p2 = CommonFunc.dip2px(mContext, 14);
                            int p3 = CommonFunc.dip2px(mContext, 5);

                            valuePointStartW = valuePointStartWTemp - p1;

                            //float valuePointStartW = (float) (((progress2 / max2) * mwidth) - 21);

                            //Log.i("slack", "valuePointStartW = " + valuePointStartW);

                            startYCenter = startY + (mhight / 2);

                            int valuePointStartY = (int) (startYCenter - p1);
                            int radius = p1 * 2;

                            paint2.setColor(getResources().getColor(R.color.home_title_black_deep));
                            squareRectf.set(valuePointStartW, valuePointStartY, valuePointStartW + radius, valuePointStartY + radius);
                            canvas.drawRoundRect(squareRectf, radius, radius, paint2);

                            //valuePointStartW = (float) ((progress2 / max2) * mwidth) - 20;
                            valuePointStartW = valuePointStartWTemp - p2;
                            valuePointStartY = (int) (startYCenter - p2);
                            radius = p2 * 2;
                            //Log.i("slack", "valuePointStartW = " + valuePointStartW + " valuePointStartY = " + valuePointStartY + " radius = " + radius);
                            paint2.setColor(getResources().getColor(R.color.write_bg));
                            squareRectf.set(valuePointStartW, valuePointStartY, valuePointStartW + radius, valuePointStartY + radius);
                            canvas.drawRoundRect(squareRectf, radius, radius, paint2);

                            //valuePointStartW = (float) (((progress2 / max2) * mwidth) - 10);
                            valuePointStartW = valuePointStartWTemp - p3;
                            valuePointStartY = (int) (startYCenter - p3);
                            radius = p3 * 2;
                            //Log.i("slack", "valuePointStartW = " + valuePointStartW + " valuePointStartY = " + valuePointStartY + " radius = " + radius);
                            paint2.setColor(getResources().getColor(R.color.home_title_black_deep));
                            squareRectf.set(valuePointStartW, valuePointStartY, valuePointStartW + radius, valuePointStartY + radius);
                            canvas.drawRoundRect(squareRectf, radius, radius, paint2);

                        }

                    }
                    //畫外圈 end

                } else if (progressList.size() > 0) {

                } else {
                    //Log.i("slack", startX + "   ,   " + startY + "    ,   " + mwidth + "  ,  " + mhight + "  ,  " + ((float) progress / max) * mwidth);
                    //public void drawRect(float left, float top, float right, float bottom, @RecentlyNonNull Paint paint) {
                    squareRectf.set(startX, startY, ((float) progress / max) * mwidth, mhight);
                    canvas.drawRoundRect(squareRectf, squareRadius, squareRadius, paint);
                }

                paint.setColor(progressDoubleSegColor);
                paint.setStrokeWidth(1);

                break;
            default:
                break;
        }


    }


    // - - - - - - - - - - - - -  public - - - - - - - - - - - - - - - - -


    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        Log.i("slack", "setProgress1");
        setProgress(progress, false);
    }

    public void setProgress(int progress, boolean needDel) {
        Log.i("slack", "setProgress2");
        if (progress <= max) {
            this.progress = progress;
            if (needDel) {
                progressList.add(progress);
            }
            invalidate();
        }
    }

    public void removeProgress(int progres) {
        if (progressList.size() > 0) {
            progressList.remove(progressList.size() - 1);
            if (progressList.size() == 0) {
                progress = DEFAULT_PROGRESS;
            } else {
                progress = progressList.get(progressList.size() - 1);
            }
        } else {
            progress = progres;
        }
        invalidate();
    }

    public void setProgressData(HealthBaseSet baseSet) {
        progressList2 = new ArrayList<Double>();
        progressListBloodPressure = new ArrayList<Double>();
        healthSet = baseSet;

        progressStage = baseSet.getRangeArr().length;
        if (progressStage > 0) {
            for (int i = 0; i < progressStage; i++) {
                //int value = (int) (target * ((float) (i + 1) / progressStage));
                if (healthSet.getHealthType().equals("1")) {
                    if (healthSet.getValue2().equals("飯前")) {
                        Log.i("debug", healthSet.getValue2() + " " + baseSet.getRangeArr()[i]);
                        progressList2.add(baseSet.getRangeArr()[i]);
                    } else if (healthSet.getValue2().equals("飯後")) {
                        Log.i("debug", healthSet.getValue2() + " " + baseSet.getRangeArr2()[i]);
                        progressList2.add(baseSet.getRangeArr2()[i]);
                    }
                } else if (healthSet.getHealthType().equals("2")) {
                    progressList2.add(baseSet.getRangeArr()[i]);
                    progressListBloodPressure.add(baseSet.getRangeArr2()[i]);
                } else {
                    progressList2.add(baseSet.getRangeArr()[i]);

                }
                max2 = baseSet.getMax();
            }
        }

        if (healthSet.getHealthType().equals("7")) {
            progress2 = Double.valueOf(baseSet.getValue2());
        } else if (healthSet.getHealthType().equals("2")) {
            progress2 = 50.0;
        } else {
            progress2 = (baseSet.getValue() == null || "".equals(baseSet.getValue())) ? 0.0 : Double.valueOf(baseSet.getValue());
        }
    }

    public void setWidth(int w) {
        mwidth = w;
        mwidth -= (margin);
        invalidate();
    }
}
