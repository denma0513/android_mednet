package com.mgear.mednetapp.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Jye on 2017/6/9.
 * class: CustomViewPager
 */
public class CustomViewPager extends ViewPager {

    private boolean isPagingEnabled = true;

    /**
     * 建構函數
     * @param context Context
     */
    public CustomViewPager(Context context) {
        super(context);
    }

    /**
     * 建構函數
     * @param context Context
     * @param attrs Attrs
     */
    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event);
    }

    /**
     * 設定是否可以滑動切換
     * @param enable Enable
     */
    public void setPagingEnabled(boolean enable) {
        this.isPagingEnabled = enable;
    }

}