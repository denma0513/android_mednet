package com.mgear.mednetapp.views;

import android.app.Dialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.SpotInfo;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.HttpConnectFunc;
import com.squareup.picasso.Picasso;

/**
 * Created by Jye on 2017/7/22.
 * class: MarkerDialog
 */
public class MarkerDialog implements View.OnClickListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {

    private Dialog mDialog;
    private SpotInfo markerInfo;
    private MediaPlayer mediaPlayer;
    private View btnVoice;
    private ProgressBar pbVoice;

    public MarkerDialog(Context context, SpotInfo spotInfo) {
        mDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_marker);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        //取得影像的連結
        markerInfo = spotInfo;
        String url = spotInfo.getImagePath();
        ImageView imgLocate = (ImageView) mDialog.findViewById(R.id.imgDialogMarkerImage);
        if (!url.equals("")) {
            //載入影像
            Picasso.with(context).load(HttpConnectFunc.CONNECT_URL + url).into(imgLocate);
        }

        TextView txtTitle = (TextView) mDialog.findViewById(R.id.txtDialogMarkerTitle);
        TextView txtContent = (TextView) mDialog.findViewById(R.id.txtDialogMarkerText);
        txtTitle.setText(spotInfo.getName());
        txtContent.setText(CommonFunc.fromHtml(spotInfo.getSummary()));

        View btnReturn = mDialog.findViewById(R.id.btnDialogMarkerReturn);
        pbVoice = (ProgressBar) mDialog.findViewById(R.id.pbDialogMarkerVoice);
        btnVoice = mDialog.findViewById(R.id.btnDialogMarkerVoice);
        btnReturn.setOnClickListener(this);
        btnVoice.setOnClickListener(this);
    }

    private void playAudio(String url) throws Exception {
        if (mediaPlayer == null) {
            btnVoice.setVisibility(View.INVISIBLE);
            pbVoice.setVisibility(View.VISIBLE);
            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(url);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.setOnErrorListener(this);
                mediaPlayer.prepareAsync();
            }
            catch (Exception e) {
                killMediaPlayer();
                mediaPlayer = null;
            }
        }
        else {
            killMediaPlayer();
        }
    }

    private void killMediaPlayer() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.reset();
                mediaPlayer.release();
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            mediaPlayer = null;
        }
    }

    public MarkerDialog show() {
        mDialog.show();
        return this;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        if (mediaPlayer != null)
            mediaPlayer.start();
        btnVoice.setVisibility(View.VISIBLE);
        pbVoice.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        killMediaPlayer();
        btnVoice.setVisibility(View.VISIBLE);
        pbVoice.setVisibility(View.INVISIBLE);
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDialogMarkerReturn:
                killMediaPlayer();
                mDialog.dismiss();
                break;
            case R.id.btnDialogMarkerVoice:
                if (!markerInfo.getVoicePath().equals("")) {
                    if (mediaPlayer == null) {
                        try {
                            playAudio(HttpConnectFunc.CONNECT_URL + markerInfo.getVoicePath());
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else killMediaPlayer();
                }
                break;
        }
    }

}