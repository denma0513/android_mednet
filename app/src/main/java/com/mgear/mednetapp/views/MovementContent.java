package com.mgear.mednetapp.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.enums.RemainEnum;
import com.mgear.mednetapp.interfaces.organization.DialogContentImpl;
import com.mgear.mednetapp.utils.HttpConnectFunc;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Jye on 2017/7/12.
 * class: MovementContent
 */
public class MovementContent extends LinearLayout implements DialogContentImpl {

    private Context mContext;
    private String title;
    private String message;
    private String lastRemain;
    private String guideName;
    private TextView txtLocationField;
    private TextView txtLocation;
    private TextView txtWaitTimeField;
    private TextView txtWaitTime;
    private TextView txtExamineField;
    private TextView txtExpansion;
    private TextView txtExpansionField;
    private LinearLayout llExamine;
    private TextView txtMemo;
    private ImageView imgRoom;

    private boolean firstShow;
    private boolean showRemainTimeTitle;
    private boolean showRemainTimeMessage;
    private Date dtStandard;
    private RemainEnum index;
    private int waitSeconds;
    private UUID[] posInfo;

    public MovementContent(Context context) {
        super(context);
        initView(context);
    }

    public MovementContent(Context context, String title) {
        super(context);
        this.title = title;
        initView(context);
    }

    public MovementContent(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public MovementContent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        lastRemain = "";
        firstShow = true;
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_content_movement, this);
        txtLocationField = (TextView) findViewById(R.id.txtContentMovementRoomField);
        txtLocation = (TextView) findViewById(R.id.txtContentMovementRoom);
        txtWaitTimeField = (TextView) findViewById(R.id.txtContentMovementTimeField);
        txtWaitTime = (TextView) findViewById(R.id.txtContentMovementTime);
        txtExpansionField = (TextView) findViewById(R.id.txtContentMovementExpansionField);
        txtExpansion = (TextView) findViewById(R.id.txtContentMovementExpansion);
        txtExamineField = (TextView) findViewById(R.id.txtContentMovementExamineField);
        llExamine = (LinearLayout) findViewById(R.id.llContentMovementExamine);
        txtMemo = (TextView) findViewById(R.id.txtContentMovementMemo);
        imgRoom = (ImageView) findViewById(R.id.imgContentMovementRoom);
    }

    public void setLocation(String location) {
        this.txtLocationField.setVisibility(VISIBLE);
        this.txtLocation.setVisibility(VISIBLE);
        this.txtLocation.setText(location);
    }

    public void setExpansion(String field, String value) {
        this.txtExpansionField.setVisibility(VISIBLE);
        this.txtExpansion.setVisibility(VISIBLE);
        this.txtExpansionField.setText(field);
        this.txtExpansion.setText(value);
    }

    public void setExamineFieldText(String text) {
        this.txtExamineField.setText(text);
        this.txtExamineField.setTextColor(Color.RED);
    }

    public void setExamine(List<String> examines) {
        if (examines == null || examines.size() == 0)
            return;
        this.txtExamineField.setVisibility(VISIBLE);
        this.llExamine.setVisibility(VISIBLE);
        for (String str : examines) {
            TextView txt = new TextView(getContext());
            txt.setMaxLines(1);
            txt.setTextSize(24);
            txt.setText(str);
            llExamine.addView(txt);
        }
    }

    public void setMemo(String memo) {
        this.txtMemo.setVisibility(VISIBLE);
        this.txtMemo.setText(memo);
    }

    public void setReciprocal(RemainEnum index, int seconds, boolean showTitle) {
        this.txtWaitTimeField.setVisibility(VISIBLE);
        this.txtWaitTime.setVisibility(VISIBLE);
        this.index = index;
        this.showRemainTimeTitle = showTitle;
        this.dtStandard = new Date();
        this.waitSeconds = seconds;
    }

    public void setReciprocal(RemainEnum index, int seconds, boolean showMessage, String message) {
        this.txtWaitTimeField.setVisibility(VISIBLE);
        this.txtWaitTime.setVisibility(VISIBLE);
        this.index = index;
        this.showRemainTimeMessage = showMessage;
        this.dtStandard = new Date();
        this.waitSeconds = seconds;
        this.message = message;
    }

    public void setImage(String url) {
        if (url.equals("")) return;
        this.imgRoom.setVisibility(VISIBLE);
        //載入影像
        Picasso.with(mContext).load(HttpConnectFunc.CONNECT_URL + url).into(this.imgRoom);
    }

    public void setShowRoomWait(String showRoomWait) {
        if ("active".equals(showRoomWait)) {
            txtWaitTimeField.setVisibility(VISIBLE);
            txtWaitTime.setVisibility(VISIBLE);
        } else {
            this.waitSeconds = 0;
            txtWaitTimeField.setHeight(0);
            txtWaitTime.setHeight(0);
            txtWaitTimeField.setVisibility(INVISIBLE);
            txtWaitTime.setVisibility(INVISIBLE);
        }
    }

    @Override
    public void show() {
        //判斷是否第一次顯示
        if (firstShow) {
            //判斷是否啟用倒數
            if (this.waitSeconds > 0) {
                this.dtStandard = new Date();
            }
            firstShow = false;
        }
    }

    @Override
    public void hidden() { }

    @Override
    public RemainEnum getRemainIndex() { return index; }

    @Override
    public boolean isRemainTime() { return this.waitSeconds > 0; }

    @Override
    public boolean showRemainInTitle() { return this.showRemainTimeTitle; }

    @Override
    public boolean showRemainInMessage() { return this.showRemainTimeMessage; }

    @Override
    public String getRemainTime() {
        if (firstShow || this.waitSeconds == 0) return "";
        //計算剩餘的倒數時間
        long interval = (new Date().getTime() - this.dtStandard.getTime()) / 1000;
        if (interval >= this.waitSeconds) {
            lastRemain = "00:00";
            //更新剩餘時間
            this.txtWaitTime.setText(String.format(Locale.getDefault(), "%s (%s)", lastRemain, mContext.getString(R.string.field_room_preparing)));
        }
        else {
            long diff = this.waitSeconds - interval;
            lastRemain = String.format(Locale.getDefault(), "%02d:%02d", diff / 60, diff % 60);
            //更新剩餘時間
            this.txtWaitTime.setText(lastRemain);
        }
        return lastRemain;
    }

    @Override
    public String getTitle() { return title; }

    @Override
    public String getMessage() {
        if (showRemainTimeMessage) {
            //判斷是否倒數完成
            if (lastRemain.equals("00:00"))
                return String.format(Locale.getDefault(), "【%s】%s", getResources().getString(R.string.field_room_preparing), txtLocation.getText());
            else
                return String.format(Locale.getDefault(), "【%s %s】%s", message == null ? title : message, lastRemain, txtLocation.getText());
        }
        else
            return String.format(Locale.getDefault(), "【%s】%s", message == null ? title : message, txtLocation.getText());
    }

    @Override
    public void setGuidePosition(UUID[] array) { this.posInfo = array; }

    @Override
    public UUID[] getGuidePosition() { return posInfo; }

    @Override
    public void setGuideName(String name) { this.guideName = name; }

    @Override
    public String getGuideName() { return this.guideName; }

}