package com.mgear.mednetapp.views;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.ListAdapter;

/**
 * Created by Jye on 2017/8/25.
 * class: ListViewDialog
 */
public class ListViewDialog implements View.OnClickListener {

    private Dialog mDialog;

    public ListViewDialog(Context context, String title, String[] content, boolean fullScreen) {
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_list_view);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            if (fullScreen) {
                lp.width = OrgMainActivity.getScreenWidth() - 30;
                lp.height = OrgMainActivity.getScreenHeight() - 30;
            }
            else {
                lp.width = OrgMainActivity.getScreenWidth() - 50;
                lp.height = OrgMainActivity.getScreenHeight() - 200;
            }
            window.setAttributes(lp);
        }

        TextView txtTitle = (TextView) mDialog.findViewById(R.id.txtListDialogTitle);
        txtTitle.setText(title);
        ListView lstContent = (ListView) mDialog.findViewById(R.id.lstListDialogContent);
        lstContent.setAdapter(new ListAdapter(context, content));

        Button btnPositive = (Button) mDialog.findViewById(R.id.btnListDialogPositive);
        btnPositive.setOnClickListener(this);
    }

    public ListViewDialog show() {
        mDialog.show();
        return this;
    }

    @Override
    public void onClick(View v) {
        mDialog.dismiss();
    }

}