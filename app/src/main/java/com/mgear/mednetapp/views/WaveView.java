package com.mgear.mednetapp.views;


import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Region.Op;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

public class WaveView extends View {


    private int mViewWidth;
    private int mViewHeight;

    /**
     * 水位线
     */
    private float mLevelLine;
    private float mActionLine;

    /**
     * 波浪起伏幅度
     */
    private float mWaveHeight = 1;
    /**
     * 波長
     */
    private float mWaveWidth = 400;
    /**
     * 被隐藏的最左边的波形
     */
    private float mLeftSide;

    private float mMoveLen;
    /**
     * 水波平移速度
     */
    public static final float SPEED = 0.5f;

    private List<Point> mPointsList;
    private Paint mPaint;
    private Paint mTextPaint;
    private Path mWavePath;
    private boolean isMeasured = false;

    private Timer timer;
    private MyTimerTask mTask;
    private int heightPXBottom; //高度px 最低位置
    private int heightPXTop; //高度px 最高位置
    private float plusRange;  //每一杯上升多少高度
    private int totleCup;
    private int oneCup = 250;
    private int nowCup = 0;


//    public void setActionLine(int cup) {
//        oneCup * cup;
//
//
//        mActionLine = actionLine;
//    }

    public void setCup(int cup) {
        if (cup <= 0)
            cup = 0;
        nowCup = cup;
        //mActionLine = cup * plusRange;
        //Log.i("debug", cup + " X " + plusRange + " = " + mActionLine);
    }

    public void setActionLine() {
        //nowCup = cup;
        mActionLine = nowCup * plusRange;
        //Log.i("debug",cup +" X "+ plusRange+" = "+mActionLine );
    }

    public void plusCup(int cup) {
        mActionLine = (cup) * plusRange;
    }

    public float getLevelLine() {
        return mLevelLine;
    }

    public float getCupHeight() {
        //Log.i("wave", "heightPXBottom = " + heightPXBottom + " heightPXTop = " + heightPXTop);
        return heightPXBottom - heightPXTop;
    }

    @SuppressLint("HandlerLeak")
    Handler updateHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // 紀錄平移總位移
            mMoveLen += SPEED;
            // 水位上升

            mLevelLine -= mActionLine;

            //Log.i("wave", "mLevelLine = " + mLevelLine);

//            //到底了 不能再水位下去
            if (mLevelLine >= heightPXBottom) {
                mLevelLine = heightPXBottom;
            }
            //到頂了 不能再上去
            if (mLevelLine <= heightPXTop) {
                mLevelLine = heightPXTop;
            }

            mActionLine = 0;
            if (mLevelLine < 0)
                mLevelLine = 0;
            mLeftSide += SPEED;
            // 波形平移
            for (int i = 0; i < mPointsList.size(); i++) {
                mPointsList.get(i).setX(mPointsList.get(i).getX() + SPEED);
                //Log.i("debug","mLevelLine = "+mLevelLine);
                switch (i % 4) {
                    case 0:
                    case 2:
                        mPointsList.get(i).setY(mLevelLine - (mWaveHeight / 4));
                        break;
                    case 1:
                        mPointsList.get(i).setY(mLevelLine);
                        break;
                    case 3:
                        mPointsList.get(i).setY(mLevelLine - (mWaveHeight / 2));
                        break;
                }
            }
            if (mMoveLen >= mWaveWidth) {
                // 波形平移超过一个完整波形后复位
                mMoveLen = 0;
                resetPoints();
            }
            invalidate();
        }

    };

    /**
     * 所有点的x坐标都还原到初始状态，也就是一个周期前的状态
     */
    private void resetPoints() {
        mLeftSide = -mWaveWidth;
        for (int i = 0; i < mPointsList.size(); i++) {
            mPointsList.get(i).setX(i * mWaveWidth / 4 - mWaveWidth);
        }
    }

    public WaveView(Context context) {
        super(context);
        init();
    }

    public WaveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WaveView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mPointsList = new ArrayList<Point>();
        timer = new Timer();

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Style.FILL);
        mPaint.setColor(getResources().getColor(R.color.health_wave));

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextAlign(Align.CENTER);
        mTextPaint.setTextSize(30);

        mWavePath = new Path();
        totleCup = MegaApplication.WaterCapacity / oneCup; //裝到滿需要多少杯

    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        // 開始波動
        start();
    }

    private void start() {
        if (mTask != null) {
            mTask.cancel();
            mTask = null;
        }
        mTask = new MyTimerTask(updateHandler);
        timer.schedule(mTask, 0, 10);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //Log.i("debug", "isMeasured = " + isMeasured);
        if (!isMeasured) {
            isMeasured = true;

            heightPXBottom = CommonFunc.dip2px(getContext(), 120 / 4 * 3);
            mViewHeight = heightPXBottom;
            mViewWidth = getMeasuredWidth();
            //Log.i("wave", "mViewHeight = " + mViewHeight + " mViewHeight = " + CommonFunc.px2dip(getContext(), mViewHeight));
            //Log.i("wave", "mViewWidth = " + mViewWidth);
            // 水位线从最底下开始上升
            mLevelLine = mViewHeight;
            // 根据View宽度计算波形峰值
            mWaveHeight = mViewWidth / 2.5f;//水波的幅度
            //Log.i("wave", "mWaveHeight = " + mWaveHeight);
            heightPXTop = (int) (CommonFunc.dip2px(getContext(), (120 / 4)) - (mWaveHeight / 2));
            //Log.i("wave", "heightPXTop  = " + heightPXTop);
            plusRange = (heightPXBottom - heightPXTop) / totleCup;  //每一杯上升多少高度
            //Log.i("wave", "plusRange = " + plusRange);
            setActionLine();
            // 波长等于四倍View宽度也就是View中只能看到四分之一个波形，这样可以使起伏更明显
            mWaveWidth = mViewWidth * 2;
            // 左边隐藏的距离预留一个波形
            mLeftSide = -mWaveWidth;
            // 这里计算在可见的View宽度中能容纳几个波形，注意n上取整
            int n = (int) Math.round(mViewWidth / mWaveWidth + 0.5);
            // n个波形需要4n+1个点，但是我们要预留一个波形在左边隐藏区域，所以需要4n+5个点
            for (int i = 0; i < (4 * n + 5); i++) {
                // 从P0开始初始化到P4n+4，总共4n+5个点
                float x = i * mWaveWidth / 4 - mWaveWidth;
                float y = 0;
                switch (i % 4) {
                    case 0:
                    case 2:
                        // 零点位于水位线上
                        y = mLevelLine;
                        break;
                    case 1:
                        // 往下波动的控制点
                        y = mLevelLine + mWaveHeight;
                        break;
                    case 3:
                        // 往上波动的控制点
                        y = mLevelLine - mWaveHeight;
                        break;
                }
                mPointsList.add(new Point(x, y));
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        mWavePath.reset();
        int i = 0;
        mWavePath.moveTo(mPointsList.get(0).getX(), mPointsList.get(0).getY());

        for (; i < mPointsList.size() - 2; i = i + 2) {
            mWavePath.quadTo(mPointsList.get(i + 1).getX(), mPointsList.get(i + 1).getY(), mPointsList.get(i + 2).getX(),
                    mPointsList.get(i + 2).getY());
        }
        //Log.i("wave", "onDraw "+mPointsList.get(i).getX()+" = " + mViewHeight);
        mWavePath.lineTo(mPointsList.get(i).getX(), mViewHeight);
        mWavePath.lineTo(mLeftSide, mViewHeight);
        mWavePath.close();

        // mPaint的Style是FILL，会填充整个Path区域
        canvas.drawPath(mWavePath, mPaint);
        // 绘制百分比
//        canvas.drawText("" + ((int) ((1 - mLevelLine / mViewHeight) * 100))
//                + "%", mViewWidth / 2, mLevelLine + mWaveHeight
//                + (mViewHeight - mLevelLine - mWaveHeight) / 2, mTextPaint);
    }

    class MyTimerTask extends TimerTask {
        Handler handler;

        public MyTimerTask(Handler handler) {
            this.handler = handler;
        }

        @Override
        public void run() {
            handler.sendMessage(handler.obtainMessage());
        }

    }

    class Point {
        private float x;
        private float y;

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }

        public Point(float x, float y) {
            this.x = x;
            this.y = y;
        }

    }

}
