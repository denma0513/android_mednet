package com.mgear.mednetapp.views;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.SubListAdapter;
import com.mgear.mednetapp.enums.ConfirmEnum;
import com.mgear.mednetapp.interfaces.organization.ConfirmClickImpl;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/8/26.
 * class: ConfirmListDialog
 */
public class ConfirmTotalDialog implements View.OnClickListener {

    private Dialog mDialog;
    private ConfirmEnum confirmEnum;

    private ConfirmClickImpl buttonClick;

    public ConfirmTotalDialog(Context context, ConfirmEnum type, String title, String total, String help, StringStringSet[] content, boolean fullScreen) {
        confirmEnum = type;
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_confirm_total);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            if (fullScreen) {
                lp.width = OrgMainActivity.getScreenWidth() - 30;
                lp.height = OrgMainActivity.getScreenHeight() - 30;
            }
            else {
                lp.width = OrgMainActivity.getScreenWidth() - 50;
                lp.height = OrgMainActivity.getScreenHeight() - 200;
            }
            window.setAttributes(lp);
        }

        TextView txtTitle = (TextView) mDialog.findViewById(R.id.txtConfirmDialogTitle);
        txtTitle.setText(title);
        TextView txtTotal = (TextView) mDialog.findViewById(R.id.txtConfirmDialogTotal);
        txtTotal.setText(total);
        TextView txtHelp = (TextView) mDialog.findViewById(R.id.txtConfirmDialogHelp);
        txtHelp.setText(help);

        Button btnPositive = (Button) mDialog.findViewById(R.id.btnConfirmDialogPositive);
        btnPositive.setOnClickListener(this);
        Button btnNegative = (Button) mDialog.findViewById(R.id.btnConfirmDialogNegative);
        btnNegative.setOnClickListener(this);

        ListView lstContent = (ListView) mDialog.findViewById(R.id.lstConfirmDialogContent);
        lstContent.setAdapter(new SubListAdapter(context, content));
        CommonFunc.setListViewHeightBasedOnChildren(lstContent);
    }

    public ConfirmTotalDialog show() {
        mDialog.show();
        return this;
    }

    public void setButtonOnClickListener(ConfirmClickImpl listener) { buttonClick = listener; }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConfirmDialogPositive:
                if (buttonClick != null)
                    buttonClick.onConfirmClick(confirmEnum);
                break;
            case R.id.btnConfirmDialogNegative:
                if (buttonClick != null)
                    buttonClick.onCancelClick(confirmEnum);
                break;
        }
        mDialog.dismiss();
    }

}