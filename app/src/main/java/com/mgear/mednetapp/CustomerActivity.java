package com.mgear.mednetapp;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mgear.mednetapp.adapter.IconCheckableListAdapter;
import com.mgear.mednetapp.enums.CultureEnum;
import com.mgear.mednetapp.enums.StatusEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.entity.organization.AdditionItemSet;
import com.mgear.mednetapp.entity.organization.BeaconInfoSet;
import com.mgear.mednetapp.entity.organization.BeaconPushSet;
import com.mgear.mednetapp.entity.organization.CheckableItem;
import com.mgear.mednetapp.entity.organization.CheckableItemSet;
import com.mgear.mednetapp.entity.organization.Customer;
import com.mgear.mednetapp.entity.organization.CustomerReportExamine;
import com.mgear.mednetapp.entity.organization.EncyclopediaSet;
import com.mgear.mednetapp.entity.organization.ExamineItemSet;
import com.mgear.mednetapp.entity.organization.HealthySystemSet;
import com.mgear.mednetapp.entity.organization.Room;
import com.mgear.mednetapp.entity.organization.RoomSet;
import com.mgear.mednetapp.entity.organization.SpecialDemandSet;
import com.mgear.mednetapp.entity.organization.SpotInfoSet;
import com.mgear.mednetapp.entity.organization.SurveyItemSet;
import com.mgear.mednetapp.task.organization.AdditionItemTask;
import com.mgear.mednetapp.task.organization.BeaconDataTask;
import com.mgear.mednetapp.task.organization.BeaconPushDataTask;
import com.mgear.mednetapp.task.organization.CustomerAdditionDataTask;
import com.mgear.mednetapp.task.organization.CustomerContactCheckTask;
import com.mgear.mednetapp.task.organization.CustomerContactUpdateTask;
import com.mgear.mednetapp.task.organization.CustomerLogonTask;
import com.mgear.mednetapp.task.organization.EncyclopediaDataTask;
import com.mgear.mednetapp.task.organization.ExamineItemDataTask;
import com.mgear.mednetapp.task.organization.HealthySystemDataTask;
import com.mgear.mednetapp.task.organization.MessageNoticeTask;
import com.mgear.mednetapp.task.organization.RegisterDeviceTask;
import com.mgear.mednetapp.task.organization.RegisterPushNotificationTokenTask;
import com.mgear.mednetapp.task.organization.RoomDataTask;
import com.mgear.mednetapp.task.organization.SpecialDemandDataTask;
import com.mgear.mednetapp.task.organization.SpecialDemandSelectedTask;
import com.mgear.mednetapp.task.organization.SpecialDemandUpdateTask;
import com.mgear.mednetapp.task.organization.SpotDataTask;
import com.mgear.mednetapp.task.organization.SurveyItemTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.ListViewDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by Jye on 2017/6/8.
 * class: CustomerActivity
 */
public class CustomerActivity extends AppCompatActivity implements
        View.OnClickListener,
        View.OnKeyListener,
        DatePickerDialog.OnDateSetListener,
        AdapterView.OnItemClickListener,
        DialogInterface.OnClickListener,
        TaskPostExecuteImpl {

    //init
    public static void init() {
        gotoRoomList = null;
        gotoRoomCodeList = null;
        //清除加選資料
        customerAdditionList = new CheckableItemSet();
    }

    //marker
    private static SpotInfoSet markerList;
    public static SpotInfoSet getMarkerList() { return markerList; }

    //beacon
    private static BeaconInfoSet beaconList;
    public static void setBeaconList(BeaconInfoSet beacons) { beaconList = beacons; }
    public static BeaconInfoSet getBeaconList() { return beaconList; }

    //beacon push
    private static BeaconPushSet beaconPushList;
    public static BeaconPushSet getBeaconPushList() { return beaconPushList; }

    //room
    private static RoomSet roomList;
    public static RoomSet getRoomList() { return roomList; }

    //examine item
    private static ExamineItemSet examineList;
    public static ExamineItemSet getExamineList() { return examineList; }

    //customer
    private static Customer logonCustomer;
    public static void setCustomer(Customer customer) {
        if (customer != null && customer.getName() != null && customer.getName().indexOf("_") >= 0) {
            customer.setName(customer.getName().split("_")[0]);
        }
        logonCustomer = customer;
    }
    public static Customer getCustomer() {
        return logonCustomer;
    }

    //customer addition
    private static CheckableItemSet customerAdditionList = new CheckableItemSet();
    public static CheckableItemSet getCustomerAdditionList() { return customerAdditionList; }
    public static void setCustomerAddition(CheckableItemSet selected) {
        //noinspection Convert2streamapi
        for (Map.Entry<UUID, CheckableItem> entry : selected.entrySet()) {
            //noinspection Java8CollectionsApi
            if (!customerAdditionList.containsKey(entry.getKey()))
                customerAdditionList.put(entry.getKey(), entry.getValue());
        }
    }

    //addition
    private static AdditionItemSet additionItemList;
    public static AdditionItemSet getAdditionItems() { return additionItemList; }

    //survey
    private static SurveyItemSet surveyItemList;
    public static SurveyItemSet getSurveyItems() { return surveyItemList; }

    //healthy system
    private static HealthySystemSet healthySystemList;
    public static HealthySystemSet getHealthySystemList() { return healthySystemList; }

    //encyclopedia
    private static EncyclopediaSet encyclopediaList;
    public static EncyclopediaSet getEncyclopediaList() { return encyclopediaList; }

    //goto room list
    private static ArrayList<String> gotoRoomCodeList;
    private static HashMap<String, UUID> gotoRoomList;
    private static void loadGotoRoom(Context context) {
        gotoRoomList = new HashMap<>();
        gotoRoomCodeList = new ArrayList<>();
        //加入簡報室
        gotoRoomList.put(context.getString(R.string.option_briefing_room), SpotInfoSet.BriefingMarker);
        //加入抽血台
        gotoRoomList.put(context.getString(R.string.option_blood_room), SpotInfoSet.BloodMarker);
        //加入櫃台
        gotoRoomList.put(context.getString(R.string.option_counter), SpotInfoSet.CounterMarker);
        //加入更衣室及廁所
        if (logonCustomer.getGender() == 'M') {
            gotoRoomList.put(context.getString(R.string.option_male_dressing), SpotInfoSet.MaleClothesMarker);
            gotoRoomList.put(context.getString(R.string.option_male_toilet), SpotInfoSet.MaleToiletMarker);
        }
        else {
            gotoRoomList.put(context.getString(R.string.option_female_dressing), SpotInfoSet.FemaleClothesMarker);
            gotoRoomList.put(context.getString(R.string.option_female_toilet), SpotInfoSet.FemaleToiletMarker);
        }

        //取得今日的檢查項目清單
        ArrayList<CustomerReportExamine> examines = logonCustomer.getExamineList();
        for (CustomerReportExamine examine : examines) {
            Room room = roomList.get(examine.getRoomId());
            //判斷地點是否已加入
            if (room != null) {
                gotoRoomCodeList.add(room.getCode());
                if (room.getMarkerId() != null && gotoRoomList.get(room.getLocateName()) == null)
                    gotoRoomList.put(room.getLocateName(), room.getMarkerId());
            }
        }
    }
    public static HashMap<String, UUID> getGotoRoomList(Context context) {
        if (gotoRoomList == null)
            loadGotoRoom(context);
        return gotoRoomList;
    }
    public static ArrayList<String> getGotoRoomCodeList(Context context) {
        if (gotoRoomCodeList == null)
            loadGotoRoom(context);
        return gotoRoomCodeList;
    }

    private TaskEnum executeTask;

    private boolean autoLogon;
    private String title;
    private String[] examines;

    private ProgressDialog mDialog;
    private Button btnLogon;
    private EditText txtName;
    private EditText txtBirthday;
    private EditText txtIdentity;
    private EditText txtMobile;
    private EditText txtEMail;
    private EditText txtAddress;
    private EditText txtLockerID;
    private TextInputLayout tilAddress;
    private EditText txtProject;
    private ListView lstDemand;
    private LinearLayout llDemand;
    private Toolbar toolbar;

    private ArrayList<String> selectedItems;
    private CheckableItem[] items;
    private IconCheckableListAdapter adapter;

    private Map<String, Integer> iconMap;

    private String[] getChangeDemand() {
        StringBuilder sbName = new StringBuilder();
        StringBuilder sbConfirm = new StringBuilder();
        StringBuilder sbCancel = new StringBuilder();
        for (CheckableItem item : items) {
            if (item.getSubName().equals("None"))
                continue;
            if (item.isCheck()) {
                sbName.append(item.getName()).append(';');
                sbConfirm.append(item.getSubName()).append(';');
            }
            else sbCancel.append(item.getSubName()).append(';');
        }

        //判斷是否加入寄送實體報告
        if (PersonalClauseActivity.Agree)
            sbCancel.append("9999").append(';');
        else
            sbConfirm.append("9999").append(';');

        //移除最後一個分號字元
        int len;
        if ((len = sbName.length()) > 0)
            sbName.deleteCharAt(len - 1);
        if ((len = sbConfirm.length()) > 0)
            sbConfirm.deleteCharAt(len - 1);
        if ((len = sbCancel.length()) > 0)
            sbCancel.deleteCharAt(len - 1);

        //初始化回傳值
        String[] result = new String[3];
        result[0] = sbName.toString();
        result[1] = sbConfirm.toString();
        result[2] = sbCancel.toString();
        return result;
    }

    private void loadCustomer() {
        //載入客戶基本資料
        txtName.setText(logonCustomer.getName());
        txtBirthday.setText(logonCustomer.getBirthdayString());
        txtIdentity.setText(String.format(Locale.getDefault(), "%s (%s)", logonCustomer.getIdentityID(), logonCustomer.getGender() == 'M' ? getString(R.string.option_male) : getString(R.string.option_female)));
        txtMobile.setText(logonCustomer.getMobile());
        txtEMail.setText(logonCustomer.getEmail());
        txtAddress.setText(logonCustomer.getAddress());
        txtLockerID.setText(logonCustomer.getLockerID());
        //載入套組名稱及檢查項目
        title = logonCustomer.getProductName();
        txtProject.setText(title);
        ArrayList<String> lstExamine = logonCustomer.getExamineNameList();
        examines = new String[lstExamine.size()];
        examines = lstExamine.toArray(examines);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        toolbar = (Toolbar) findViewById(R.id.tbCustomer);
        toolbar.setTitle(R.string.title_personal_confirm);
        setSupportActionBar(toolbar);

        btnLogon = (Button) findViewById(R.id.btnCustomerLogon);
        btnLogon.setOnClickListener(this);
        Button btnCancel = (Button) findViewById(R.id.btnCustomerCancel);
        btnCancel.setOnClickListener(this);
        txtName = (EditText) findViewById(R.id.txtCustomerName);
        txtBirthday = (EditText) findViewById(R.id.txtCustomerBirthday);
        txtIdentity = (EditText) findViewById(R.id.txtCustomerIdentity);
        txtMobile = (EditText) findViewById(R.id.txtCustomerMobile);
        txtEMail = (EditText) findViewById(R.id.txtCustomerEMail);
        txtAddress = (EditText) findViewById(R.id.txtCustomerAddress);
        txtLockerID = (EditText) findViewById(R.id.txtCustomerLockerID);
        tilAddress = (TextInputLayout) findViewById(R.id.tilCustomerAddress);
        txtProject = (EditText) findViewById(R.id.txtProjectItem);
        txtProject.setOnClickListener(this);
        llDemand = (LinearLayout) findViewById(R.id.llCustomerDemand);
        lstDemand = (ListView) findViewById(R.id.lstSpecialDemand);
        lstDemand.setOnItemClickListener(this);

        //自行輸入的事件
        txtName.setOnKeyListener(this);
        txtMobile.setOnKeyListener(this);
        txtEMail.setOnKeyListener(this);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(this);

        //設定無法編輯資料
        txtIdentity.setFocusable(false);
        txtAddress.setFocusable(false);
        txtLockerID.setFocusable(false);
        //顯示日期對話方塊
        txtBirthday.setFocusable(false);
        txtBirthday.setOnClickListener(this);

        //初始化圖示對應清單
        iconMap = new HashMap<>();
        iconMap.put("icon_demand_blood", R.drawable.icon_demand_blood);
        iconMap.put("icon_demand_diabetes", R.drawable.icon_demand_diabetes);
        iconMap.put("icon_demand_drug", R.drawable.icon_demand_drug);
        iconMap.put("icon_demand_heart", R.drawable.icon_demand_heart);
        iconMap.put("icon_demand_hypertension", R.drawable.icon_demand_hypertension);
        iconMap.put("icon_demand_mc", R.drawable.icon_demand_mc);
        iconMap.put("icon_demand_none", R.drawable.icon_demand_none);
        iconMap.put("icon_demand_pregnancy", R.drawable.icon_demand_pregnancy);
        iconMap.put("icon_demand_pregnancy_suspect", R.drawable.icon_demand_pregnancy_suspect);

        //判斷是否同意電子報告
        if (PersonalClauseActivity.Agree)
            tilAddress.setVisibility(View.INVISIBLE);

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //判斷是否直接登入
        Intent intent = getIntent();
        String contact = intent.getStringExtra("contact");
        if (!(contact == null || contact.equals(""))) {
            autoLogon = true;
            //登入系統
            mDialog = new ProgressDialog(this);
            mDialog.setMessage(getString(R.string.msg_logon));
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
            //取得客戶基本資料
            new CustomerContactCheckTask(this, this, true, null).execute(CultureSelectActivity.BluetoothAddress, contact);
        }
        else {
            //顯示客戶基本資料
            loadCustomer();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                //關閉小鍵盤
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    if (imm != null)
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.customer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id)
        {
            case R.id.action_refresh:
                new CustomerContactCheckTask(this, this, true, getString(R.string.msg_get_customer))
                        .execute(CultureSelectActivity.BluetoothAddress, logonCustomer.getIdentityID());
                return true;
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProjectItem:
                ListViewDialog dialog = new ListViewDialog(this, title, examines, false);
                dialog.show();
                break;
            case R.id.txtCustomerBirthday:
                Calendar cal = Calendar.getInstance();
                cal.setTime(logonCustomer.getBirthday());
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.CustomDatePickerDialogTheme, this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.setCancelable(false);
                datePickerDialog.setCanceledOnTouchOutside(false);
                datePickerDialog.show();
                break;
            case R.id.btnCustomerLogon:
                //判斷特殊需求是否顯示
                if (llDemand.getVisibility() == View.VISIBLE) {
                    //登入系統
                    mDialog = new ProgressDialog(this);
                    mDialog.setMessage(getString(R.string.msg_logon));
                    mDialog.setCancelable(false);
                    mDialog.setCanceledOnTouchOutside(false);
                    mDialog.show();
                    //判斷是否更新客戶基本資料
                    if (logonCustomer.getName().equals(txtName.getText().toString()) &&
                        logonCustomer.getBirthdayString().equals(txtBirthday.getText().toString()) &&
                        logonCustomer.getEmail().equals(txtEMail.getText().toString()) &&
                        logonCustomer.getMobile().equals(txtMobile.getText().toString())) {
                        //更新特殊需求選項
                        String[] ary = getChangeDemand();
                        new SpecialDemandUpdateTask(this, this, false, null)
                                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getContactCode()), ary[0], ary[1], ary[2]);
                    }
                    else {
                        //更新客戶基本資料
                        new CustomerContactUpdateTask(this, this, false, null)
                                .execute(CultureSelectActivity.BluetoothAddress,
                                        String.valueOf(logonCustomer.getContactCode()),
                                        txtName.getText().toString(),
                                        txtEMail.getText().toString(),
                                        txtBirthday.getText().toString(),
                                        logonCustomer.getPhoneNumber(),
                                        txtMobile.getText().toString());
                    }
                }
                else {
                    //驗證電子信箱的格式
                    String email = txtEMail.getText().toString().trim();
//                    if (!email.equals("") && !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//                        Snackbar.make(txtEMail, R.string.msg_email_format_fail, Snackbar.LENGTH_LONG).show();
//                    }
//                    else {
                        mDialog = new ProgressDialog(this);
                        mDialog.setMessage(getString(R.string.msg_get_special_demand));
                        mDialog.setCancelable(false);
                        mDialog.setCanceledOnTouchOutside(false);
                        mDialog.show();
                        //取得已選取的特殊需求的選項
                        new SpecialDemandSelectedTask(this, this, false, null)
                                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getContactCode()));
//                    }
                }
                break;
            default:
                startActivity(new Intent(this, LogonActivity.class));
                this.finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CheckableItem checkableItem = this.items[position];
        boolean check = !checkableItem.isCheck();
        checkableItem.setCheck(check);
        String code = checkableItem.getSubName();
        //判斷是否為選取
        if (check) {
            //判斷是否選取無
            if (code.equals("None")) {
                //清除其他的所有項目
                for (int i = 0; i < this.items.length - 1; i++)
                    this.items[i].setCheck(false);
            }
            else {
                //清除無的項目
                this.items[this.items.length - 1].setCheck(false);
                //判斷是否為女性
                if (logonCustomer.getGender() == 'F') {
                    //生理期
                    switch (code) {
                        case "0011":
                            this.items[position + 1].setCheck(false);
                            this.items[position + 2].setCheck(false);
                            break;
                        //懷孕
                        case "0012":
                            this.items[position - 1].setCheck(false);
                            this.items[position + 1].setCheck(false);
                            break;
                        //疑似懷孕
                        case "0013":
                            this.items[position - 1].setCheck(false);
                            this.items[position - 2].setCheck(false);
                            break;
                    }
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        executeTask = type;
        switch (type) {
            case CustomerContactCheckTask:
                //更新客戶基本資料
                Customer customer = (Customer) result;
                //判斷是否自動登入
                if (autoLogon) {
                    //判斷客戶的狀態是否允許登入
                    if (customer.getId() != null && customer.getStatus() != StatusEnum.End.getCode()) {
                        logonCustomer = customer;
                        loadCustomer();
                        //客戶登入
                        new CustomerLogonTask(this, this, false, null)
                                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getContactCode()));
                    }
                    else {
                        //清除登入資訊
                        getSharedPreferences("mgear", 0).edit().clear().apply();
                        //切換回選擇語言的頁面
                        startActivity(new Intent(this, CultureSelectActivity.class));
                    }
                }
                else {
                    if (customer.getId() != null) {
                        logonCustomer = customer;
                        loadCustomer();
                    }
                }
                break;
            case SpecialDemandSelectedTask:
                //noinspection unchecked
                selectedItems = (ArrayList<String>) result;
                //取得特殊需求清單
                new SpecialDemandDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case SpecialDemandDataTask:
                SpecialDemandSet demand = (SpecialDemandSet) result;
                //判斷取得特殊需求清單是否成功
                if (demand.getStatus() == HTTP_OK) {
                    items = demand.getByGender(logonCustomer.getGender(), selectedItems, iconMap);
                    adapter = new IconCheckableListAdapter(this, items);
                    lstDemand.setAdapter(adapter);
                    llDemand.setVisibility(View.VISIBLE);
                    btnLogon.setText(getString(R.string.button_logon));
                    toolbar.getMenu().getItem(0).setVisible(false);
                    toolbar.setTitle(getString(R.string.title_special_demand));
                    mDialog.dismiss();
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_demand_data_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case CustomerContactUpdateTask:
                //判斷更新客戶資料是否成功
                if ((boolean) result) {
                    //更新行動裝置目前的資料
                    logonCustomer.setName(txtName.getText().toString());
                    logonCustomer.setEmail(txtEMail.getText().toString());
                    logonCustomer.setMobile(txtMobile.getText().toString());
                    //取得日期格式
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    try {
                        //更新生日
                        logonCustomer.setBirthday(sdf.parse(txtBirthday.getText().toString()));
                    }
                    catch (Exception ignored) { }
                    //更新特殊需求選項
                    String[] ary = getChangeDemand();
                    new SpecialDemandUpdateTask(this, this, false, null)
                            .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getContactCode()), ary[0], ary[1], ary[2]);
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_update_customer_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case SpecialDemandUpdateTask:
                //判斷特殊需求更新是否成功
                if ((boolean) result) {
                    //客戶登入
                    new CustomerLogonTask(this, this, false, null)
                            .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getContactCode()));
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_update_demand_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case CustomerLogonTask:
                //判斷客戶今日登入的狀態
                StatusEnum status = StatusEnum.valueOf(logonCustomer.getStatus());
                if (status != null) {
                    switch (status) {
                        case Initial:
                        case Logon:
                            //變更客戶狀態為已登入
                            logonCustomer.setStatus(StatusEnum.Logon.getCode());
                            //取得可加選項目清單
                            new AdditionItemTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                            break;
                        default:
                            //重推訊息
                            new MessageNoticeTask(this, this, false, null)
                                    .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getContactCode()), "POST");
                            break;
                    }
                }
                break;
            case MessageNoticeTask:
                //取得可加選項目清單
                new AdditionItemTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case AdditionItemDataTask:
                additionItemList = (AdditionItemSet) result;
                if (additionItemList.getStatus() == HTTP_OK) {
                    //取得客戶已加選的項目
                    new CustomerAdditionDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getContactCode()));
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_addition_data_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case CustomerAdditionDataTask:
                CheckableItemSet cis = (CheckableItemSet) result;
                //判斷是否已有加選項目
                if (cis.size() > 0)
                    setCustomerAddition(cis);
                //取得診間資訊清單
                new RoomDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case RoomDataTask:
                roomList = (RoomSet) result;
                if (roomList.getStatus() == HTTP_OK) {
                    //取得檢查項目清單
                    new ExamineItemDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_room_data_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case ExamineItemDataTask:
                examineList = (ExamineItemSet) result;
                if (examineList.getStatus() == HTTP_OK) {
                    //取得滿意度調查項目清單
                    new SurveyItemTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_examine_data_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case SurveyItemDataTask:
                surveyItemList = (SurveyItemSet) result;
                if (surveyItemList.getStatus() == HTTP_OK) {
                    //取得圖標資訊清單
                    new SpotDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);


                    //取得健康小百科清單
                    //new EncyclopediaDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);


                    /**
                     * 跳過接下來所有判斷直接進到下一階段
                     * */
//                    mDialog.dismiss();
//                    //紀錄客戶綁定的行動裝置藍芽裝置編號
//                    OrgMainActivity.addCustomerTrack("login_" + CultureSelectActivity.BluetoothAddress, CommonFunc.getNowSeconds());
//                    //判斷是否自動登入
//                    if (autoLogon) {
//                        //切換到地圖頁面
//                        startActivity(new Intent(this, MainActivity.class));
//                    }
//                    else {
//                        //紀錄目前的登入訊息
//                        SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
//                        settings.edit().putString("contact", logonCustomer.getIdentityID()).putInt("today", Calendar.getInstance().get(Calendar.DAY_OF_YEAR)).apply();
//                        //切換到教學頁面
//                        startActivity(new Intent(this, TutorialActivity.class));
//                    }
//                    this.finish();

                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_survey_item_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case SpotDataTask:
                markerList = (SpotInfoSet) result;
                if (markerList.getStatus() == HTTP_OK) {
                    //取得信標資訊清單
                    //new BeaconDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);

                    //取得健檢系統清單
                    //new HealthySystemDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);



                    //跳過系統清單流程直接進到健診流程
                    //紀錄客戶綁定的行動裝置藍芽裝置編號
                    OrgMainActivity.addCustomerTrack("login_" + CultureSelectActivity.BluetoothAddress, CommonFunc.getNowSeconds());

                    String englishVersion = CultureSelectActivity.SelectCulture.equals(CultureEnum.English)?"1":"0";

                    //註冊push token
                    new RegisterDeviceTask(this,this,false,null).execute(logonCustomer.getContactCode(),englishVersion);
                    //new RegisterDeviceTask(this,this,false,null).execute(logonCustomer.getContactEmsCode());

                    String gender = logonCustomer.getGender()+"";
                    new RegisterPushNotificationTokenTask(this,this,false,null).execute(logonCustomer.getName(), Integer.toString(logonCustomer.getAge()), gender, logonCustomer.getMobile(),englishVersion);

                    //判斷是否自動登入
                    if (autoLogon) {
                        //切換到地圖頁面
                        startActivity(new Intent(this, OrgMainActivity.class));
                    }
                    else {
                        //紀錄目前的登入訊息
                        SharedPreferences settings = getSharedPreferences("mgear", 0);
                        settings.edit().putString("contact", logonCustomer.getIdentityID()).putInt("today", Calendar.getInstance().get(Calendar.DAY_OF_YEAR)).apply();
                        //切換到教學頁面
                        startActivity(new Intent(this, TutorialActivity.class));
                    }
                    this.finish();

                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_spot_data_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case BeaconDataTask:
                beaconList = (BeaconInfoSet) result;
                if (beaconList.getStatus() == HTTP_OK) {
                    //取得信標推撥清單
                    new BeaconPushDataTask(this, this, false, null)
                            .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getGender()),
                                    String.valueOf(logonCustomer.getAge()), logonCustomer.getExamineCodes());
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_beacon_data_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case BeaconPushDataTask:
                beaconPushList = (BeaconPushSet) result;
                if (beaconPushList.getStatus() == HTTP_OK || beaconPushList.getStatus() == HTTP_NOT_FOUND) {

                    new EncyclopediaDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                    //取得健檢系統清單
                    //new HealthySystemDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_beacon_push_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case HealthySystemDataTask:
                healthySystemList = (HealthySystemSet) result;
                if (healthySystemList.getStatus() == HTTP_OK) {
                    //取得健康小百科清單
                    new EncyclopediaDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_healthy_system_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            case EncyclopediaDataTask:
                encyclopediaList = (EncyclopediaSet) result;
                if (encyclopediaList.getStatus() == HTTP_OK) {
                    mDialog.dismiss();
                    //紀錄客戶綁定的行動裝置藍芽裝置編號
                    OrgMainActivity.addCustomerTrack("login_" + CultureSelectActivity.BluetoothAddress, CommonFunc.getNowSeconds());

                    String englishVersion = CultureSelectActivity.SelectCulture.equals(CultureEnum.English)?"1":"0";

                    //註冊push token
                    new RegisterDeviceTask(this,this,false,null).execute(logonCustomer.getContactCode(),englishVersion);
                    //new RegisterDeviceTask(this,this,false,null).execute(logonCustomer.getContactEmsCode());

                    String gender = logonCustomer.getGender()+"";
                    new RegisterPushNotificationTokenTask(this,this,false,null).execute(logonCustomer.getName(), Integer.toString(logonCustomer.getAge()), gender, logonCustomer.getMobile(),englishVersion);

                    //判斷是否自動登入
                    if (autoLogon) {
                        //切換到地圖頁面
                        startActivity(new Intent(this, OrgMainActivity.class));
                    }
                    else {
                        //紀錄目前的登入訊息
                        SharedPreferences settings = getSharedPreferences("mgear", 0);
                        settings.edit().putString("contact", logonCustomer.getIdentityID()).putInt("today", Calendar.getInstance().get(Calendar.DAY_OF_YEAR)).apply();
                        //切換到教學頁面
                        startActivity(new Intent(this, TutorialActivity.class));
                    }
                    this.finish();
                }
                else {
                    mDialog.dismiss();
                    //顯示失敗訊息
                    new AlertDialog.Builder(this).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_get_encyclopedia_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (executeTask) {
            case SpecialDemandDataTask:
                mDialog.show();
                //取得特殊需求清單
                new SpecialDemandDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case CustomerContactUpdateTask:
                mDialog.show();
                //更新客戶基本資料
                new CustomerContactUpdateTask(this, this, false, null)
                        .execute(CultureSelectActivity.BluetoothAddress,
                                String.valueOf(logonCustomer.getContactCode()),
                                txtName.getText().toString(),
                                txtEMail.getText().toString(),
                                txtBirthday.getText().toString(),
                                logonCustomer.getPhoneNumber(),
                                txtMobile.getText().toString());
                break;
            case SpecialDemandUpdateTask:
                mDialog.show();
                //更新特殊需求選項
                String[] ary = getChangeDemand();
                new SpecialDemandUpdateTask(this, this, false, null)
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getContactCode()), ary[0], ary[1], ary[2]);
                break;
            case AdditionItemDataTask:
                mDialog.show();
                //取得可加選項目清單
                new AdditionItemTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case RoomDataTask:
                mDialog.show();
                //取得診間資訊清單
                new RoomDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case ExamineItemDataTask:
                mDialog.show();
                //取得檢查項目清單
                new ExamineItemDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case SurveyItemDataTask:
                mDialog.show();
                //取得滿意度調查項目清單
                new SurveyItemTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case SpotDataTask:
                mDialog.show();
                //取得圖標資訊清單
                new SpotDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case BeaconDataTask:
                mDialog.show();
                //取得信標資訊清單
                new BeaconDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case BeaconPushDataTask:
                mDialog.show();
                //取得信標推撥清單
                new BeaconPushDataTask(this, this, false, null)
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(logonCustomer.getGender()),
                                String.valueOf(logonCustomer.getAge()), logonCustomer.getExamineCodes());
                break;
            case HealthySystemDataTask:
                mDialog.show();
                //取得健檢系統清單
                new HealthySystemDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            case EncyclopediaDataTask:
                mDialog.show();
                //取得健康小百科清單
                new EncyclopediaDataTask(this, this, false, null).execute(CultureSelectActivity.BluetoothAddress);
                break;
            default:
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        txtBirthday.setText(String.format(Locale.getDefault(), "%04d-%02d-%02d", view.getYear(), view.getMonth() + 1, view.getDayOfMonth()));
    }

}