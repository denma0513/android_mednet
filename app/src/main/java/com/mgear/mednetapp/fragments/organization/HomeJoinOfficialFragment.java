package com.mgear.mednetapp.fragments.organization;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/6/15.
 * class: HomeJoinOfficialFragment
 */
public class HomeJoinOfficialFragment extends BaseFragment {

    private boolean isPrepared;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_home_join_official, container, false);
            ImageView img = (ImageView) rootView.findViewById(R.id.imgHomeJoinOfficial);
            img.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.qr_code_havo)));
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("join_official", CommonFunc.getNowSeconds());
    }

}