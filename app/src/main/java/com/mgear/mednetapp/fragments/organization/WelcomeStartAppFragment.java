package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.WelcomeActivity;
import com.mgear.mednetapp.adapter.HomePageViewAdapter;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Dennis.
 * class: TutorialStartAppFragment
 */
public class WelcomeStartAppFragment extends Fragment implements View.OnClickListener {

    private View rootView;
    private ImageView img;
    private TextView skip, title, content;
    private Button signButton;
    private int page;
    private BitmapDrawable bgImg;
    private boolean isCreate;
    private int width, height;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_welcome_startapp, container, false);
            img = rootView.findViewById(R.id.welcome_img);
            signButton = rootView.findViewById(R.id.sign_button);
            skip = rootView.findViewById(R.id.skip);
            title = rootView.findViewById(R.id.title);
            content = rootView.findViewById(R.id.content);

            height = (int) (width * 1.03);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    width, height);
            img.setLayoutParams(layoutParams);


            bgImg = new BitmapDrawable(getResources(), CommonFunc.readBitmapHalf(getActivity(), getBgImg()));

            //root.setBackground(bgImg);
            title.setText(getBgTitle());
            content.setText(getBgContent());
            img.setImageDrawable(bgImg);
            skip.setOnClickListener(this);
            signButton.setOnClickListener(this);
        }

        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isCreate) {
            //可见时加载数据相当于Fragment的onResume
        } else {
            //不可见是不加载数据
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getBgImg() {
        BitmapDrawable img;
        switch (this.page) {
            case 0:
                return R.drawable.welcome1;
            case 1:
                return R.drawable.welcome2;
            case 2:
                return R.drawable.welcome3;
            case 3:
                return R.drawable.welcome4;
            case 4:
                return R.drawable.welcome5;
            case 5:
                return R.drawable.welcome6;
            case 6:
                return R.drawable.welcome7;
            default:
                break;
        }


        return 0;
    }

    public String getBgTitle() {
        switch (this.page) {
            case 0:
                return "健康醫療資源共享平台\n";
            case 1:
                return "每日健康紀錄— 輕鬆做好健康管理";
            case 2:
                return "醫師諮詢— 隨時隨地問醫師";
            case 3:
                return "健康檢查— 線上輕鬆選健檢";
            case 4:
                return "AI醫師— 看對科別找到醫師";
            case 5:
                return "智慧健檢流程— 資訊透明好順暢";
            case 6:
                return "健康風險評估— 快速了解健康風險";
            default:
                break;
        }

        return "";
    }

    public String getBgContent() {
        switch (this.page) {
            case 0:
                return "這是一個健康醫療資源共享平台APP，您可以使用本APP進行每日健康紀錄、各項簡單健康測驗、儲存歷年健康檢查報告、尋找最適合您的健康檢查機構及內容，您也可以在這裡諮詢各科專科醫師。\n"
                        + "我們希望提供一個全新的平台，協助您做好個人自主健康管理工作，擁有美好人生。現在就開始使用吧!";
            case 1:
                return "每日紀錄喝水量、睡眠時數、步數、血壓及血糖等數據，並可預設通知、異常數值提醒等功能，輕鬆做好健康管理。";
            case 2:
                return "25類專科醫師提供線上諮詢服務，您可以隨時隨地提出健康諮詢，或就醫前的疑惑，醫師期限內線上回答您的問題，解除您的疑惑。";
            case 3:
                return "匯集超過200家主流健檢機構，您可以依據個人基本資料及需求，快速尋找屬於您的健檢方案。您也可以自組｢個人化｣健檢方案，或勾選並進行兩個以上的健檢「方案比較」，選出您理想中的健檢內容。";
            case 4:
                return "由醫師團隊依據主流醫學原則所開發，透過簡單三個詢問入口，｢智能醫師｣24hr幫助您找就醫科別與醫師。";
            case 5:
                return "Smart健診流程APP隨時告知您下一個檢查項目與等候時間，並透過手機上地圖導引您至檢查診間。資訊透明，省時又快速，讓健檢不再是’’辛苦’’的年度大事。";
            case 6:
                return "提供腦中風、心肌梗塞、心腦血管疾病、高血壓、腸胃、壓力與內分泌等系統健康測驗，每項測驗僅需3分鐘，即可知道您的健康風險，及早防範疾病發生。";
            default:
                break;
        }

        return "";
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.skip:
                ((WelcomeActivity) getActivity()).gotoMain();
                break;
            case R.id.sign_button:
                ((WelcomeActivity) getActivity()).signIn();
                break;
        }
    }
}