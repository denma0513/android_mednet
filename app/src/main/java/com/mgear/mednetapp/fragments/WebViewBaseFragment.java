package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.interfaces.organization.WebPreviousPageImpl;


/**
 * Created by Jye on 2017/7/29.
 * class: WebViewFragment
 */
public class WebViewBaseFragment extends BaseFragment implements WebPreviousPageImpl {

    private boolean isPrepared;
    private boolean hasLoadedOnce;

    private String strTitle;
    private View rootView;
    private String strUrl;
    private WebView mWebView;
    private TextView txtMessage;
    private ProgressBar progressBar;

    private class MednetWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            txtMessage.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
//            if (view.canGoBack())
//                ((MainActivity) getActivity()).setPreviousPage(WebViewBaseFragment.this);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }

//        @Override
//        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            //全部擋掉
//            handler.cancel();
//        }

    }

    public void setUrl(String title, String url) {
        strTitle = title;
        strUrl = url;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_web_view, container, false);
            progressBar = (ProgressBar)  rootView.findViewById(R.id.pbBrowserWait);
            txtMessage = (TextView) rootView.findViewById(R.id.txtBrowserMessage);
            mWebView = (WebView) rootView.findViewById(R.id.webViewBrowser);

            // Enable Javascript
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setUseWideViewPort(true);
            webSettings.setLoadWithOverviewMode(true);
            // Force links and redirects to open in the WebView instead of in a browser
            mWebView.setWebViewClient(new MednetWebViewClient());
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    public void previousPage() {
        if (mWebView.canGoBack())
            mWebView.goBack();
    }

    @Override
    public boolean hasPreviousPage() {
        return mWebView.canGoBack();
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        //MainActivity.addCustomerTrack(strTitle, CommonFunc.getNowSeconds());
        //判斷是否加載過資料
        if (!hasLoadedOnce) {
            hasLoadedOnce = true;
            //載入頁面內容
            mWebView.loadUrl(strUrl);
        }
    }

}