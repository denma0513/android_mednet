package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.Customer;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.CustomerContactCheckTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.ListViewDialog;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Jye on 2017/8/3.
 * class: CustomerFragment
 */
public class CustomerFragment extends Fragment implements View.OnClickListener, TaskPostExecuteImpl {

    private View rootView;
    private String title;
    private String[] examines;

    private EditText txtName;
    private EditText txtBirthday;
    private EditText txtIdentity;
    private EditText txtMobile;
    private EditText txtEMail;
    private EditText txtAddress;
    private EditText txtLockerID;
    private EditText txtProject;

    private void loadCustomer() {
        Customer logonCustomer = CustomerActivity.getCustomer();
        //載入客戶基本資料
        txtName.setText(logonCustomer.getName());
        txtBirthday.setText(logonCustomer.getBirthdayString());
        txtIdentity.setText(String.format(Locale.getDefault(), "%s (%s)", logonCustomer.getIdentityID(), logonCustomer.getGender() == 'M' ? getString(R.string.option_male) : getString(R.string.option_female)));
        txtMobile.setText(logonCustomer.getMobile());
        txtEMail.setText(logonCustomer.getEmail());
        txtAddress.setText(logonCustomer.getAddress());
        txtLockerID.setText(logonCustomer.getLockerID());
        //載入套組名稱及檢查項目
        title = logonCustomer.getProductName();
        txtProject.setText(title);
        ArrayList<String> lstExamine = logonCustomer.getExamineNameList();
        examines = new String[lstExamine.size()];
        examines = lstExamine.toArray(examines);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("customer", CommonFunc.getNowSeconds());
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_customer, container, false);
            txtName = (EditText) rootView.findViewById(R.id.txtCustomerName);
            txtBirthday = (EditText) rootView.findViewById(R.id.txtCustomerBirthday);
            txtIdentity = (EditText) rootView.findViewById(R.id.txtCustomerIdentity);
            txtMobile = (EditText) rootView.findViewById(R.id.txtCustomerMobile);
            txtEMail = (EditText) rootView.findViewById(R.id.txtCustomerEMail);
            txtAddress = (EditText) rootView.findViewById(R.id.txtCustomerAddress);
            txtLockerID = (EditText) rootView.findViewById(R.id.txtCustomerLockerID);
            txtProject = (EditText) rootView.findViewById(R.id.txtProjectItem);
            txtProject.setOnClickListener(this);

            View btnRefresh = rootView.findViewById(R.id.btnCustomerRefresh);
            btnRefresh.setOnClickListener(this);

            //設定無法編輯資料
            txtName.setEnabled(false);
            txtBirthday.setEnabled(false);
            txtIdentity.setEnabled(false);
            txtMobile.setEnabled(false);
            txtEMail.setEnabled(false);
            txtAddress.setEnabled(false);
            txtLockerID.setEnabled(false);
            //設定無焦點
            txtName.setFocusable(false);
            txtBirthday.setFocusable(false);
            txtIdentity.setFocusable(false);
            txtMobile.setFocusable(false);
            txtEMail.setFocusable(false);
            txtAddress.setFocusable(false);
            txtLockerID.setFocusable(false);
            loadCustomer();
        }
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProjectItem:
                ListViewDialog dialog = new ListViewDialog(getActivity(), title, examines, false);
                dialog.show();
                break;
            case R.id.btnCustomerRefresh:
                new CustomerContactCheckTask(getActivity(), this, true, getString(R.string.msg_get_customer))
                        .execute(CultureSelectActivity.BluetoothAddress, CustomerActivity.getCustomer().getIdentityID());
                break;
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        switch (type) {
            case CustomerContactCheckTask:
                //更新客戶基本資料
                Customer customer = (Customer) result;
                if (customer.getId() != null) {
                    CustomerActivity.setCustomer(customer);
                    loadCustomer();
                }
                break;
            default:
                break;
        }
    }

}