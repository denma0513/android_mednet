package com.mgear.mednetapp.fragments.organization;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.HomeGridAdapter;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.entity.organization.StringIntegerSet;
import com.mgear.mednetapp.interfaces.organization.HomePageImpl;
import com.mgear.mednetapp.interfaces.organization.PreviousPageImpl;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.CustomViewPager;

import java.util.Stack;

/**
 * Created by Jye on 2017/6/15.
 * class: HomeItemFragment
 */
public class HomeItemFragment extends BaseFragment implements HomePageImpl, PreviousPageImpl {

    private boolean isPrepared;
    private View rootView;

    private String strTitle;
    private int pagePosition;
    private String[] aryItem = null;
    private Stack<StringIntegerSet> previousPage;
    private CustomViewPager mPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_home_item, container, false);
            // get parent information

            HomeFragment home = (HomeFragment)getParentFragment();

            strTitle = home.getStrTitle();
            pagePosition = home.getPagePosition();
            previousPage = home.getPreviousPage();
            mPager= home.getMPager();

            aryItem = new String[] {
                    getString(R.string.menu_facebook),
                    getString(R.string.menu_line),
                    getString(R.string.menu_ilink),
                    getString(R.string.menu_org_rating)
            };

                    GridView grid = (GridView) rootView.findViewById(R.id.gvHomeItem);
            grid.setAdapter(new HomeGridAdapter(getActivity(), home.getHomeTitleList(), home.getHomeImageList(),
                    home.getHomeColorList(), OrgMainActivity.getScreenWidth() / 2,
                    (HomePageImpl) this
            ));
            grid.setOnItemClickListener((HomeFragment)getParentFragment());
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("home", CommonFunc.getNowSeconds());
    }

    @Override
    public void onClickItem(int position, int homePosition) {
        //紀錄目前的頁面

        if (homePosition <= 13) {
            previousPage.push(new StringIntegerSet(strTitle, pagePosition));
            //顯示到新頁面
            strTitle = aryItem[position];
            mPager.setCurrentItem(homePosition, false);
            ((OrgMainActivity) getActivity()).setPreviousPage(this);
        } else {
            //開啟外部ＡＰＰ
            if (homePosition == 14) {
                //facebook
                try {
                    getActivity().getPackageManager().getPackageInfo("com.facebook.katana", 0);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("fb://page/885458688469771"));
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://www.facebook.com/885458688469771"));
                    startActivity(intent);
                }
            }else if (homePosition ==15){
                //line
                //TODO 測試中
                try {
                    getActivity().getPackageManager().getPackageInfo("jp.naver.line.android", 0);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://line.me/R/ti/p/%40med-net"));
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://line.me/R/ti/p/%40med-net"));
                    startActivity(intent);
                    }
            }
        }

    }

    @Override
    public void previousPage() {
        if (!previousPage.empty()) {
            StringIntegerSet set = previousPage.pop();
            strTitle = set.getMajor();
            pagePosition = set.getMinor();
            mPager.setCurrentItem(pagePosition, false);
        }
    }

    @Override
    public boolean hasPreviousPage() {
        return !previousPage.empty();
    }

    @Override
    public String getNavigationPageTitle() { return strTitle; }

}