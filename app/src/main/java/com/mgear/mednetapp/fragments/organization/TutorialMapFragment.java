package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/6/9.
 * class: TutorialMapFragment
 */
public class TutorialMapFragment extends Fragment {

    private View rootView;
    private ImageView imgHavo;
    private ImageView imgMarker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tutorial_map, container, false);
            RelativeLayout root = (RelativeLayout) rootView.findViewById(R.id.rootTutorialMap);
            root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_map_bg)));
            imgHavo = (ImageView) rootView.findViewById(R.id.imgTutorialMapHavo);
            imgMarker = (ImageView) rootView.findViewById(R.id.imgTutorialMapMarker);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        imgHavo.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_bottom));
        ScaleAnimation animScaleUp = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, 1, 0.5f, 1, 1.0f);
        animScaleUp.setDuration(750);
        animScaleUp.setInterpolator(new LinearInterpolator());
        animScaleUp.setRepeatCount(-1);
        animScaleUp.setRepeatMode(2);
        imgMarker.startAnimation(animScaleUp);
    }

    @Override
    public void onPause() {
        super.onPause();
        imgHavo.clearAnimation();
        imgMarker.clearAnimation();
    }

}