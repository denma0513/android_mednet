package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.ForgetPasswordActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.LoadingFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class SignUpChkSMSFragment extends MegaBaseFragment implements View.OnClickListener, TaskPost2ExecuteImpl {
    //常數
    private static String TAG = "SIGNIN";
    private static int SIGNUP = 0;
    private static int FORGET = 1;

    //畫面變數
    private View rootView;
    private Button mChksmsNextButton;
    private EditText mChksmsNum1, mChksmsNum2, mChksmsNum3, mChksmsNum4;
    private TextView mChksmsPhone, mChksmsRetry;
    private RelativeLayout chksmsLoading;
    private ImageView chksmsResult;
    private int mType = 0; //預設為註冊流程
    private String mEmail;


    public SignUpChkSMSFragment setType(int type) {
        mType = type;
        return this;
    }

    public SignUpChkSMSFragment setEmail(String email) {
        mEmail = email;
        return this;
    }

    private void sendSMS() {
        Log.i(TAG, "sendSMS " + mType);
        if (mType == FORGET) {
            new CommonApiTask(getActivity(), this, true, getString(R.string.msg_data_loading),
                    TaskEnum.MednetForgetPassword).execute();
        } else {
            new CustomerApiTask(getActivity(), this, true, getString(R.string.msg_data_loading),
                    TaskEnum.MednetSendSMS).execute();
        }
    }


    private void doChkSMS() {
        try {
            String optCode = mChksmsNum1.getText().toString() + mChksmsNum2.getText().toString() + mChksmsNum3.getText().toString() + mChksmsNum4.getText().toString();
            if (mType == FORGET) {
                new CommonApiTask(getActivity(), this, false, "", TaskEnum.MednetCheckForgetPassword)
                        .execute(mEmail, optCode);
            } else {
                new CustomerApiTask(getActivity(), this, true, getString(R.string.msg_data_loading),
                        TaskEnum.MednetCheckSMS).execute(optCode);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.user_login_signup_chksms, container, false);
            mChksmsNum1 = rootView.findViewById(R.id.chksms_num1);
            mChksmsNum2 = rootView.findViewById(R.id.chksms_num2);
            mChksmsNum3 = rootView.findViewById(R.id.chksms_num3);
            mChksmsNum4 = rootView.findViewById(R.id.chksms_num4);
            mChksmsNextButton = rootView.findViewById(R.id.chksms_next_button);
            chksmsLoading = rootView.findViewById(R.id.chksms_loading);
            chksmsResult = rootView.findViewById(R.id.chksms_result);

            mChksmsRetry = rootView.findViewById(R.id.chksms_retry);
            mChksmsPhone = rootView.findViewById(R.id.chksms_phone);

//            mChksmsNum1.setOnKeyListener(this);
//            mChksmsNum2.setOnKeyListener(this);
//            mChksmsNum3.setOnKeyListener(this);
//            mChksmsNum4.setOnKeyListener(this);

            mChksmsNum1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.toString().length() ==1){
                        mChksmsNum2.requestFocus();
                    }else{
                    }
                }
            });
            mChksmsNum2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.toString().length() ==1){
                        mChksmsNum3.requestFocus();
                    }else{
                        mChksmsNum1.requestFocus();
                    }
                }
            });
            mChksmsNum3.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.toString().length() ==1){
                        mChksmsNum4.requestFocus();
                    }else{
                        mChksmsNum2.requestFocus();
                    }
                }
            });
            mChksmsNum4.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.toString().length() ==1){
                        mChksmsNextButton.requestFocus();
                        mChksmsNextButton.performClick();
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    }else{
                        mChksmsNum3.requestFocus();
                    }
                }
            });


            mChksmsRetry.setOnClickListener(this);
            //mChksmsNum4.setOnEditorActionListener(this);
            mChksmsNextButton.setOnClickListener(this);

            mChksmsPhone.setText("驗證碼將發送到您的手機 : " + MegaApplication.getInstance().getMember().getMobile());

            try {
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                LoadingFragment loadingFragment = new LoadingFragment();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                Fragment fragmentMain = loadingFragment;
                ft.replace(R.id.chksms_loading, fragmentMain);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                chksmsLoading.setVisibility(View.GONE);
                chksmsResult.setVisibility(View.GONE);
            } catch (Exception e) {
                Log.w(TAG, "LoadingFragment  " + e.getStackTrace());
                e.printStackTrace();
            }

            if (mType == SIGNUP) {
                //註冊流程
                sendSMS();
            }


        }
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chksms_next_button:
                chksmsLoading.setVisibility(View.VISIBLE);
                doChkSMS();
                break;
            case R.id.chksms_retry:
                sendSMS();
                break;
        }
    }


    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetSendSMS:
                break;
            case MednetCheckSMS:
                try {
                    chksmsLoading.setVisibility(View.GONE);

                    if (result != null && "success".equals(result.optString("message"))) {
                        chksmsResult.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.ic_result_success)));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((UserLoginActivity) getActivity()).goUpProfile();
                            }
                        }, 2000);
                    } else {
                        chksmsResult.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.ic_result_fail)));
                    }
                    chksmsResult.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    Log.w(TAG, "MednetCheckSMS e = " + e.getStackTrace());
                    e.printStackTrace();
                }
                break;
            case MednetCheckForgetPassword:
                try {
                    chksmsLoading.setVisibility(View.GONE);
                    if (result != null && "success".equals(result.optString("message"))) {
                        chksmsResult.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.ic_result_success)));
                        String accessToken = result.optJSONObject("source").optString("access_token");
                        String refreshToken = result.optJSONObject("source").optString("refreshToken");
                        MegaApplication.getInstance().getMember().setAccessToken(accessToken);
                        MegaApplication.getInstance().getMember().setRefreshToken(refreshToken);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((ForgetPasswordActivity) getActivity()).goUpProfile();
                            }
                        }, 2000);
                    } else {
                        chksmsResult.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.ic_result_fail)));
                    }
                    chksmsResult.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    Log.w(TAG, "MednetCheckForgetPassword e = " + e.getStackTrace());
                    e.printStackTrace();
                }
                break;
        }
    }

//    @Override
//    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//        Log.i(TAG, "onEditorAction: view.getId()1" + textView.getId());
//        switch (textView.getId()) {
//            case R.id.chksms_num4:
//                mChksmsNextButton.requestFocus();
//                mChksmsNextButton.performClick();
//                break;
//        }
//        return false;
//    }
//
//    @Override
//    public boolean onKey(View view, int i, KeyEvent keyEvent) {
//        //This is the filter
//        Log.i(TAG, "onKey: view.getId()1" + view.getId());
//        Log.i(TAG, "onKey: keyEvent.getAction()()1" + keyEvent.getAction());
//        if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
//            return true;
//        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
//            getActivity().finish();
//            return true;
//        }
//        Log.i(TAG, "onKey: view.getId()2" + view.getId());
//        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DEL) {
//            switch (view.getId()) {
//                case R.id.chksms_num2:
//                    mChksmsNum1.requestFocus();
//                    break;
//                case R.id.chksms_num3:
//                    mChksmsNum2.requestFocus();
//                    break;
//                case R.id.chksms_num4:
//                    mChksmsNum3.requestFocus();
//                    break;
//            }
//        } else {
//
//            Log.i(TAG, "onKey: view.getId()3" + view.getId());
//            switch (view.getId()) {
//                case R.id.chksms_num1:
//                    Log.i(TAG, "onKey: chksms_num1");
//                    mChksmsNum2.requestFocus();
//                    break;
//                case R.id.chksms_num2:
//                    Log.i(TAG, "onKey: chksms_num2");
//                    mChksmsNum3.requestFocus();
//                    break;
//                case R.id.chksms_num3:
//                    Log.i(TAG, "onKey: chksms_num3");
//                    mChksmsNum4.requestFocus();
//                    break;
//                case R.id.chksms_num4:
//                    Log.i(TAG, "onKey: chksms_num4");
//                    mChksmsNextButton.requestFocus();
//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
//                    break;
//            }
//        }
//
//        return false;
//    }
}
