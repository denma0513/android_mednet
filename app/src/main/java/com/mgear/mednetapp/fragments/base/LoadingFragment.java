package com.mgear.mednetapp.fragments.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mgear.mednetapp.R;

import java.util.Timer;
import java.util.TimerTask;

@SuppressLint("ValidFragment")
public class LoadingFragment extends MegaBaseFragment {
    //常數
    private static String TAG = "LOADING";

    //畫面變數
    private View rootView;
    private TextView[] loadingPoint;

    //邏輯參數
    private boolean[] visible = {false, false, false};


    private Button mChksmsNextButton;
    private EditText mChksmsNum1, mChksmsNum2, mChksmsNum3, mChksmsNum4;
    private TextView mChksmsPhone, mChksmsRetry;


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_loading, container, false);

            AlphaAnimation appearAnimation = new AlphaAnimation((float) 0.3, (float) 1.0);
            appearAnimation.setDuration(300);
            AlphaAnimation disappearAnimation = new AlphaAnimation((float) 1, (float) 0.3);
            disappearAnimation.setDuration(300);
            loadingPoint = new TextView[]{rootView.findViewById(R.id.load_point1), rootView.findViewById(R.id.load_point2), rootView.findViewById(R.id.load_point3)};

            @SuppressLint("HandlerLeak")
            Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    int index = msg.what;
                    if (visible[index]) {
                        loadingPoint[index].startAnimation(appearAnimation);
                        visible[index] = false;
                    } else {
                        loadingPoint[index].startAnimation(disappearAnimation);
                        visible[index] = true;
                    }

                }
            };


            new Timer().scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    handler.sendEmptyMessage(0);
                }
            }, 0, 300);
            new Timer().scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    handler.sendEmptyMessage(1);
                }
            }, 150, 300);
            new Timer().scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    handler.sendEmptyMessage(2);
                }
            }, 300, 300);


        }
        return rootView;
    }

}
