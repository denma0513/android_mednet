package com.mgear.mednetapp.fragments.organization;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.algorithms.dijkstra.DijkstraAlgorithm;
import com.mgear.mednetapp.algorithms.dijkstra.Vertex;
import com.mgear.mednetapp.helpers.PathHelper;
import com.mgear.mednetapp.entity.organization.BeaconInfo;
import com.mgear.mednetapp.entity.organization.BeaconInfoSet;
import com.mgear.mednetapp.entity.organization.BeaconNeighbor;
import com.mgear.mednetapp.entity.organization.SpotInfo;
import com.mgear.mednetapp.entity.organization.SpotInfoSet;
import com.mgear.mednetapp.interfaces.organization.BeaconPositionImpl;
import com.mgear.mednetapp.utils.AnimationTools;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.CustomToast;
import com.mgear.mednetapp.views.MarkerDialog;
import com.mgear.mednetapp.views.ToastView;
import com.mgear.mednetapp.views.materialspinner.MaterialSpinner;
import com.qozix.tileview.TileView;
import com.qozix.tileview.markers.MarkerLayout;
import com.qozix.tileview.paths.CompositePathView.DrawablePath;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by Jye on 2017/6/12.
 * class: MapFragment
 */
public class MapFragment extends Fragment implements
        SensorEventListener,
        MaterialSpinner.OnItemSelectedListener<String>,
        View.OnClickListener,
        View.OnLongClickListener {

    private SensorManager sensorManager  = null;
    private Sensor accSensor = null;
    private Sensor magSensor = null;

    private float[] magneticFieldValues;
    private float[] accelerometerValues;

    private int myPositionIndex;
    private UUID moveMarkerID;
    private boolean hasDrawPath = false;
    private boolean hasLoadedOnce = false;
    private MaterialSpinner spinnerLocateOption;
    private TileView tvMap;
    private DrawablePath directionPath;

    private View rootView;
    private View btnMyPosition;
    private FrameLayout flMyPosition;
    private ImageView imgMyPositionBg;
    private HashMap<UUID, ImageView> markerMap;
    private List<String> locateList;

    private static String selectedLocateName;
    private UUID[] mLocatePosition;
    private BeaconPositionImpl beaconPosition;
    private CustomToast toastPosition;
    private CustomToast toastWeak;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("map", CommonFunc.getNowSeconds());
        if (rootView == null) {
            Activity context = getActivity();
            if (context instanceof BeaconPositionImpl)
                beaconPosition = (BeaconPositionImpl) context;

            //感應器服務
            sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
            //加速度感應器
            accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            //磁場感應器
            magSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

            this.accelerometerValues = new float[3];
            this.magneticFieldValues = new float[3];

            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_map, container, false);
            btnMyPosition = rootView.findViewById(R.id.btnMapMyPosition);
            btnMyPosition.setOnClickListener(this);
            tvMap = (TileView) rootView.findViewById(R.id.tvMap);
            tvMap.setSize(8000, 3005);
            //判斷語系
            switch (CultureSelectActivity.SelectCulture) {
                case English:
                    tvMap.addDetailLevel(1.000f, "map/tile/en/1000/%d_%d.png", 256, 256);
                    tvMap.addDetailLevel(0.500f, "map/tile/en/500/%d_%d.png", 256, 256);
                    tvMap.addDetailLevel(0.250f, "map/tile/en/250/%d_%d.png", 256, 256);
                    tvMap.addDetailLevel(0.125f, "map/tile/en/125/%d_%d.png", 256, 256);
                    break;
                case TraditionalChinese:
                    tvMap.addDetailLevel(1.000f, "map/tile/tw/1000/%d_%d.png", 256, 256);
                    tvMap.addDetailLevel(0.500f, "map/tile/tw/500/%d_%d.png", 256, 256);
                    tvMap.addDetailLevel(0.250f, "map/tile/tw/250/%d_%d.png", 256, 256);
                    tvMap.addDetailLevel(0.125f, "map/tile/tw/125/%d_%d.png", 256, 256);
                    break;
            }

            tvMap.setScale(0.125f);
            tvMap.setDrawingCacheEnabled(true);
            tvMap.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));

            // we're running from assets, should be fairly fast decodes, go ahead and render asap
            tvMap.setShouldRenderWhilePanning(true);
            // for quickly drawn tiles without a down sample, transitions aren't particularly useful
            tvMap.setTransitionsEnabled(false);

            //加入說明圖標
            markerMap = new HashMap<>();
            SpotInfoSet markerList = CustomerActivity.getMarkerList();
            if (markerList != null) {
                Drawable drawable = new BitmapDrawable(getResources(), CommonFunc.readBitmap(context, R.drawable.map_marker));
                //noinspection Convert2streamapi
                for (SpotInfo marker: markerList.values()) {
                    if (marker.isEnable()) {
                        ImageView iv = new ImageView(context);
                        iv.setLayoutParams(new ViewGroup.LayoutParams(30, 50));
                        iv.setTag(marker);
                        iv.setOnClickListener(this);
                        iv.setImageDrawable(drawable);
                        tvMap.addMarker(iv, marker.getMarkerLeft(), marker.getMarkerTop(), -0.8f, -0.5f);
                        markerMap.put(marker.getId(), iv);
                    }
                }
            }

            //加入目前位置的圖標
            flMyPosition = new FrameLayout(context);
            flMyPosition.setLayoutParams(new FrameLayout.LayoutParams(100, 100));
            imgMyPositionBg = new ImageView(context);
            imgMyPositionBg.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(context, R.drawable.map_position_bg)));
            ImageView imgMyPositionHavo = new ImageView(context);
            imgMyPositionHavo.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(context, R.drawable.map_position_havo)));
            flMyPosition.addView(imgMyPositionBg);
            flMyPosition.addView(imgMyPositionHavo);
            tvMap.addMarker(flMyPosition, -100, -100, -0.4f, -0.5f);

            //設定路徑的參數
            directionPath = new DrawablePath();
            directionPath.paint = tvMap.getDefaultPathPaint();
            directionPath.paint.setStrokeWidth(10.0f);
            directionPath.paint.setColor(ContextCompat.getColor(context, R.color.light_red));
            //設定虛線效果
            directionPath.paint.setPathEffect(new ComposePathEffect(new CornerPathEffect(10.0f), new DashPathEffect(new float[] { 21.0f, 7.0f }, 3.0f)));

            //設定今日健檢會前往的診間地點
            HashMap<String, UUID> gotoRoomList = CustomerActivity.getGotoRoomList(context);
            spinnerLocateOption = (MaterialSpinner) rootView.findViewById(R.id.lstMapMarker);
            if (gotoRoomList.size() > 0) {
                locateList = new ArrayList<>(gotoRoomList.keySet());
                spinnerLocateOption.setItems(locateList);
                spinnerLocateOption.setOnItemSelectedListener(this);
            }

            //接收藍芽訊號微弱訊息
            ToastView tv = new ToastView(context);
            tv.setText(getString(R.string.msg_bluetooth_weak));
            tv.setRadius(10);
            tv.setTextSize(18);
            tv.setBackgroundColor(Color.argb(0xff, 0xed, 0x99, 0x99));
            toastWeak = new CustomToast(context, tv);
            toastWeak.setPosition(CustomToast.Position.CENTER_BOTTOM, 0, 250);
        }

        //繪製路徑
        setLocate();
        if (selectedLocateName != null)
            setLocateOption(selectedLocateName);

        //設定載入完成
        hasLoadedOnce = true;
        return rootView;
    }

    public void setLocateOption(String name) {
        selectedLocateName = name;
        //判斷地圖是否已載入完成
        if (hasLoadedOnce && locateList != null && locateList.size() > 0) {
            int index = locateList.indexOf(name);
            if (index >= 0)
                spinnerLocateOption.setSelectedIndex(index);
            else {
                //將地點加入位置清單
                List<String> items = spinnerLocateOption.getItems();
                items.add(name);
                spinnerLocateOption.setSelectedIndex(items.size() - 1);
            }
        }
    }

    public void setLocate(UUID[] array, String name) {
        mLocatePosition = array;
        selectedLocateName = name;
        //判斷地圖是否已載入完成
        if (hasLoadedOnce) {
            setLocate();
            setLocateOption(name);
        }
    }

    public void setLocate(BeaconInfo source, BeaconInfo target) {
        if (source == null || target == null) return;
        if (hasLoadedOnce)
            drawPath(source.getKey(), target.getKey());
    }

    public void clearLocate() { mLocatePosition = null; }

    public void setMyPosition(BeaconInfo source, int size) {
        if (toastPosition != null) {
            flMyPosition.setVisibility(View.VISIBLE);
            btnMyPosition.setVisibility(View.VISIBLE);
            toastPosition.cancelInterval();
            toastPosition = null;
        }
        //判斷訊號是否微弱
        if (size == 1) {
            toastWeak.showToast(null, 2000);
        }
        //判斷位置是否變更
        if (hasLoadedOnce && source.getKey() != myPositionIndex) {
            myPositionIndex = source.getKey();
            int posLeft = source.getPosLeft();
            int posTop = source.getPosTop();
            //判斷是否有設定移動停留點
            UUID moveStayId = source.getMoveStayId();
            if (moveStayId != null) {
                BeaconInfo bi = CustomerActivity.getBeaconList().get(moveStayId);
                if (bi != null) {
                    posLeft = bi.getPosLeft();
                    posTop = bi.getPosTop();
                }
            }

            //移動目前的位置
            tvMap.moveMarker(flMyPosition, posLeft, posTop);
            tvMap.moveToMarker(flMyPosition, true);
            //重新繪製路徑
            setLocate();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, magSensor, SensorManager.SENSOR_DELAY_FASTEST);
        //判斷是否已定位完成
        /*
        if (beaconPosition.getMyPosition() == null) {
            Activity context = getActivity();
            flMyPosition.setVisibility(View.INVISIBLE);
            btnMyPosition.setVisibility(View.GONE);
            ToastView tv = new ToastView(context);
            tv.setText(getString(R.string.msg_location));
            tv.setRadius(10);
            tv.setTextSize(18);
            tv.setBackgroundColor(Color.argb(0xff, 0xed, 0x99, 0x99));
            toastPosition = new CustomToast(context, tv);
            toastPosition.setPosition(CustomToast.Position.CENTER_BOTTOM, 0, 250);
            toastPosition.setInterval(5000);
        }
        */
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this, accSensor);
        sensorManager.unregisterListener(this, magSensor);
        if (toastPosition != null)
            toastPosition.cancelInterval();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                this.accelerometerValues = event.values.clone();
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                this.magneticFieldValues = event.values.clone();
                break;
        }
        calculateOrientation();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void removeDrawPath() {
        //移除已繪製的路徑
        if (hasDrawPath) {
            tvMap.removePath(directionPath);
            hasDrawPath = false;
        }
    }

    private boolean drawPath(int source, int target) {
        //移除已繪製的路徑
        removeDrawPath();
        //判斷起訖點是否相同
        if (source == target)
            return false;

        //取得信標清單
        BeaconInfoSet beaconList = CustomerActivity.getBeaconList();
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(beaconList.getGraph());
        //設定路徑的起點
        dijkstra.execute(beaconList.getVertex(source));
        //設定路徑的終點
        LinkedList<Vertex> path = dijkstra.getPath(beaconList.getVertex(target));
        int size;
        if (path != null && (size = path.size()) > 0) {
            ArrayList<Point> points = new ArrayList<>();
            BeaconInfo biPrev = null;
            for (int i = 0; i < size; i++) {
                Vertex vertex = path.get(i);
                BeaconInfo bi = beaconList.get(vertex.getId());
                if (bi != null) {
                    if (biPrev != null) {
                        //判斷前一個點移動到目前的點是否經過中繼點
                        SparseArray<BeaconNeighbor> neighbors = biPrev.getNeighborList();
                        BeaconNeighbor neighbor = neighbors.get(bi.getKey());
                        if (neighbor != null) {
                            ArrayList<Point> relays = neighbor.getRelayList();
                            if (relays != null && relays.size() > 0) {
                                //加入中繼清單
                                points.addAll(relays);
                            }
                        }
                    }

                    //加入目前的位置點
                    points.add(new Point(bi.getPosLeft(), bi.getPosTop()));
                    //紀錄前一個點的資料
                    biPrev = bi;
                }
            }

            directionPath.path = PathHelper.pathFromPoints(points);
            tvMap.drawPath(directionPath);
            hasDrawPath = true;
            return true;
        }
        return false;
    }

    private void setLocate() {
        if (mLocatePosition == null || mLocatePosition.length != 2) return;
        BeaconInfo mTargetLocate = CustomerActivity.getBeaconList().get(mLocatePosition[0]);

        //更換前位置的圖標
        if (moveMarkerID != null) {
            ImageView iv = markerMap.get(moveMarkerID);
            if (iv != null) {
                MarkerLayout.LayoutParams lp = (MarkerLayout.LayoutParams) iv.getLayoutParams();
                lp.width = 30;
                lp.height = 50;
                iv.setLayoutParams(lp);
                iv.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.map_marker)));
            }
        }
        //更新新位置的圖標
        moveMarkerID = mLocatePosition[1];
        if (moveMarkerID != null) {
            ImageView iv = markerMap.get(moveMarkerID);
            if (iv != null) {
                MarkerLayout.LayoutParams lp = (MarkerLayout.LayoutParams) iv.getLayoutParams();
                lp.width = 50;
                lp.height = 80;
                iv.setLayoutParams(lp);
                iv.setImageDrawable(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.map_marker_target)));
                //播放縮放動畫
                AnimationTools.startScaleUpAnimation(iv, 1.5f, 0.8f, 0.5f, 1.0f);
            }
        }

        //判斷是否有設來源位置
        BeaconInfo source;
        if ((source = beaconPosition.getMyPosition()) != null) {
            if (source.getMoveStayId() != null) {
                BeaconInfo bi = CustomerActivity.getBeaconList().get(source.getMoveStayId());
                if (bi != null)
                    drawPath(bi.getKey(), mTargetLocate.getKey());
            }
            else drawPath(source.getKey(), mTargetLocate.getKey());
        }
    }

    private void calculateOrientation() {
        float[] values = new float[3];
        float[] R = new float[9];
        SensorManager.getRotationMatrix(R, null, this.accelerometerValues, this.magneticFieldValues);
        SensorManager.getOrientation(R, values);
        values[0] = (float) Math.toDegrees((double) values[0]);
        if (values[0] != 0.0f) {
            imgMyPositionBg.setRotation(values[0] - 61);
        }
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
        //切換到選取的診間位置
        UUID uuid = CustomerActivity.getGotoRoomList(getActivity()).get(item);
        if (uuid != null) {
            ImageView iv = markerMap.get(uuid);
            if (iv != null)
                tvMap.moveToMarker(iv, true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMapMyPosition:
                tvMap.moveToMarker(flMyPosition, true);
                break;
            default:
                new MarkerDialog(getActivity(), (SpotInfo) v.getTag()).show();
        }
    }

    @Override
    public boolean onLongClick(View v) {
        beaconPosition.reloadBeaconList();
        return true;
    }

}