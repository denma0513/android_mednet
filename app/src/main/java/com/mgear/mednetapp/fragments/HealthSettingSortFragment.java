package com.mgear.mednetapp.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.HealthPushSettingActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.DragNDropAdapter;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.RefreshListIpml;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.NewDownloadImageTask;
import com.mgear.mednetapp.views.DragListView;
import com.mgear.mednetapp.views.DragNDropListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Dennis
 * class: HomePageFragment
 */
public class HealthSettingSortFragment extends MegaBaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener, View.OnTouchListener,
        RefreshListIpml, TaskPost2ExecuteImpl {
    //靜態
    private static String TAG = "HealthSetting";
    private static final int FINISH = 1;

    //畫面參數
    private View rootView;
    //private DragListView mHomeListView, mMoreListView;
    private ArrayList<JSONObject> mHomeList, mMoreList;
    private DragNDropListView mListView;
    private Map<String, ArrayList<JSONObject>> mList;
    private String healthType = "";


    public HealthSettingSortFragment setHealthType(String type) {
        healthType = type;
        return this;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            try {
                rootView = inflater.inflate(R.layout.health_setting_sort_list, container, false);
//                mHomeListView = rootView.findViewById(R.id.setting_list);
//                mMoreListView = rootView.findViewById(R.id.setting_list2);
                mListView = rootView.findViewById(R.id.setting_list);
                mHomeList = new ArrayList<>();
                mMoreList = new ArrayList<>();
                mList = new HashMap<String, ArrayList<JSONObject>>();
                for (int i = 0; i < MegaApplication.healthSortList.length; i++) {

                    if (i < 4)
                        mHomeList.add(MegaApplication.healthMap.get(MegaApplication.healthSortList[i]));
                    else
                        mMoreList.add(MegaApplication.healthMap.get(MegaApplication.healthSortList[i]));
                }
                mList.put("首頁顯示", mHomeList);
                mList.put("更多項目", mMoreList);

                DragNDropAdapter dragNDropAdapter = new DragNDropAdapter(getActivity(), this, new int[]{R.layout.row_item},
                        new int[]{R.id.txt__customizer_item}, new int[]{R.id.icon_customizer_item}, mList);

                mListView.setDragOnLongPress(true);
                mListView.setAdapter(dragNDropAdapter);

                for (int i = 0; i < dragNDropAdapter.getGroupCount(); i++) {
                    mListView.expandGroup(i);
                }
                mListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                        return true;
                    }
                });


//                mHomeList = new ArrayList<JSONObject>();
//                mMoreList = new ArrayList<JSONObject>();
//                for (int i = 0; i < MegaApplication.healthSortList.length; i++) {
//
//                    if (i < 4)
//                        mHomeList.add(MegaApplication.healthMap.get(MegaApplication.healthSortList[i]));
//                    else
//                        mMoreList.add(MegaApplication.healthMap.get(MegaApplication.healthSortList[i]));
//                }


//                ListAdapter adapter = new ListAdapter(mHomeList);
//                mHomeListView.setAdapter(adapter);
//                mHomeListView.setMyDragListener(new DragListView.MyDragListener() {
//                    @Override
//                    public void onDragFinish(int srcPositon, int finalPosition) {
//                        //Toast.makeText(MainActivity.this, "beginPoisiton : " + srcPositon + "...endPosition : " + finalPosition, Toast.LENGTH_LONG).show();
//                    }
//                });
//                adapter = new ListAdapter(mMoreList);
//                mMoreListView.setAdapter(adapter);


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return rootView;
    }

    @Override
    protected void lazyLoad() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //((HealthPushSettingActivity) getActivity()).goDetail(settingList.get(i).optString("healthName"), settingList.get(i).optString("healthType"));
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onDrop(Map<String, ArrayList<JSONObject>> list) {
        Log.i("debug", "e04");
        mList = list;
        mHomeList = mList.get("首頁顯示");
        mMoreList = mList.get("更多項目");
        ArrayList<String> typeSort = new ArrayList<String>();
        //MegaApplication.healthSortList = new String[];
        for (int i = 0; i < mHomeList.size(); i++) {
            JSONObject obj = mHomeList.get(i);
            typeSort.add(obj.optString("healthType"));
        }
        for (int i = 0; i < mMoreList.size(); i++) {
            JSONObject obj = mMoreList.get(i);
            typeSort.add(obj.optString("healthType"));
        }
        MegaApplication.healthSortList = typeSort.toArray(new String[typeSort.size()]);

        String arrStr = "";

        for (int i = 0; i < MegaApplication.healthSortList.length; i++) {
            //Log.i("debug", "MegaApplication.healthSortList = " + MegaApplication.healthSortList[i]);
            arrStr += "," + MegaApplication.healthSortList[i];
            new CommonApiTask(getActivity(), this, false, "", TaskEnum.MednetAppLogHealthOrder)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MegaApplication.healthSortList[i], (i + 1) + "");
        }
        arrStr = arrStr.substring(1);

        MegaApplication.setHealth();

        SharedPreferences settings = getActivity().getSharedPreferences("HealthSettingNotice", Activity.MODE_PRIVATE);
        settings.edit().putString("healthTypeList", arrStr).apply();


    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {

    }
}