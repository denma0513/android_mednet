package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mgear.mednetapp.ForgetPasswordActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.entity.OauthSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.LoadingFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

@SuppressLint("ValidFragment")
public class SignUpProfileFragment extends MegaBaseFragment implements View.OnClickListener, TextView.OnEditorActionListener,
        View.OnFocusChangeListener,
        TaskPost2ExecuteImpl {
    //常數
    private static String TAG = "SIGNIN";
    private SignUpProfileFragment context;
    private final String[] gender = {"性別", "男", "女"};
    private final Calendar myCalendar = Calendar.getInstance();
    private static int SIGNUP = 0;
    private static int FORGET = 1;

    //畫面變數
    private View rootView;
    private Button mProfileNextButton;
    private EditText mProfileLastname, mProfileFirstname, mProfileBirthday;
    private Spinner mProfileGender;
    private TextView mProfileSkip;

    private EditText mResetPassword, mResetPasswordRetry;
    private Button mResetSendButton;
    private TextView mResetPasswordRetryWarning, mResetPasswordWarning;

    private int mType = SIGNUP;
    private OauthSet mOauthSet;


    //date picker Listener
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "yyyy/MM/dd"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.TAIWAN);
            mProfileBirthday.setText(sdf.format(myCalendar.getTime()));
        }
    };

    DatePickerDialog dataPicker;


    public SignUpProfileFragment setType(int type) {
        mType = type;
        return this;
    }

    public SignUpProfileFragment setEmail(String email) {
        return this;
    }

    private void doUpdateProfile() {

        if (mType == FORGET) {
            String password = mResetPassword.getText().toString();
            String passwordRetry = mResetPasswordRetry.getText().toString();
            boolean done = true;

            mResetPasswordWarning.setVisibility(View.GONE);
            mResetPasswordRetryWarning.setVisibility(View.GONE);

            if ("".equals(password)) {
                mResetPasswordWarning.setText(getString(R.string.msg_invalid_password));
                mResetPasswordWarning.setVisibility(View.VISIBLE);
                done = false;
            }

            if (!password.equals(passwordRetry)) {
                mResetPasswordRetryWarning.setText(getString(R.string.msg_invalid_password_retry));
                mResetPasswordRetryWarning.setVisibility(View.VISIBLE);
                done = false;
            }

            if (done)
                new CustomerApiTask(getActivity(), context, true, getString(R.string.msg_data_loading), TaskEnum.MednetSetPassword)
                        .execute(password);
        } else {
            String firstName = mProfileFirstname.getText().toString();
            String lastName = mProfileLastname.getText().toString();
            String gender = "男".equals(mProfileGender.getSelectedItem().toString()) ? "1" : "女".equals(mProfileGender.getSelectedItem().toString()) ? "0" : null;
            String birthday = mProfileBirthday.getText().toString();

            if (CommonFunc.isBlank(firstName)) {
                CommonFunc.showToast(getActivity(), "請填寫名字！");
                return;
            }

            if (CommonFunc.isBlank(lastName)) {
                CommonFunc.showToast(getActivity(), "請填寫姓氏！");
                return;
            }

            if (CommonFunc.isBlank(gender)) {
                CommonFunc.showToast(getActivity(), "請填寫性別！");
                return;
            }

            if (CommonFunc.isBlank(birthday)) {
                CommonFunc.showToast(getActivity(), "請填寫生日！");
                return;
            }

            new CustomerApiTask(getActivity(), context, true, getString(R.string.msg_data_loading), TaskEnum.MednetUpdateProfile)
                    .execute(firstName, lastName, gender, birthday);
        }
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {

            if (mType == FORGET) {
                rootView = inflater.inflate(R.layout.fragment_forget_resetpassword, container, false);
                mResetPassword = rootView.findViewById(R.id.reset_password);
                mResetPasswordRetry = rootView.findViewById(R.id.reset_password_retry);
                mResetSendButton = rootView.findViewById(R.id.reset_send_button);
                mResetPasswordWarning = rootView.findViewById(R.id.reset_password_warning);
                mResetPasswordRetryWarning = rootView.findViewById(R.id.reset_password_retry_warning);

                //OnEditorAction
                mResetPasswordRetry.setOnEditorActionListener(context);
                //onClick
                mResetSendButton.setOnClickListener(context);

            } else {
                rootView = inflater.inflate(R.layout.user_login_profile, container, false);
                mProfileLastname = rootView.findViewById(R.id.profile_lastname);
                mProfileFirstname = rootView.findViewById(R.id.profile_firstname);
                mProfileBirthday = rootView.findViewById(R.id.profile_birthday);
                mProfileNextButton = rootView.findViewById(R.id.profile_next_button);
                mProfileSkip = rootView.findViewById(R.id.profile_skip);
                mProfileBirthday.setKeyListener(null);
                mProfileBirthday.setInputType(InputType.TYPE_NULL);

                //OnEditorAction
                mProfileFirstname.setOnEditorActionListener(context);
                //mProfileBirthday.setOnEditorActionListener(context);
                mProfileBirthday.setOnFocusChangeListener(context);
                //onClick
                mProfileNextButton.setOnClickListener(context);
                mProfileBirthday.setOnClickListener(context);
                mProfileSkip.setOnClickListener(context);
                mProfileGender = rootView.findViewById(R.id.profile_gender);

                //Spinner spinner = (Spinner)findViewById(R.id.spinner);
                ArrayAdapter<String> lunchList = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_dropdown_item,
                        gender);
                mProfileGender.setAdapter(lunchList);

                dataPicker = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));


                mOauthSet = MegaApplication.getInstance().getOauth();
                if (!CommonFunc.isBlank(mOauthSet.getGender())) {
                    switch (mOauthSet.getGender()) {
                        case "M":
                            mProfileGender.setSelection(1);
                            break;
                        case "F":
                            mProfileGender.setSelection(2);
                            break;
                    }
                }
                if (!CommonFunc.isBlank(mOauthSet.getName())) {
                    mProfileFirstname.setText(mOauthSet.getName());
                }
                if (!CommonFunc.isBlank(mOauthSet.getLastName())) {
                    mProfileLastname.setText(mOauthSet.getLastName());
                }
                if (!CommonFunc.isBlank(mOauthSet.getBirthday())) {
                    mProfileBirthday.setText(mOauthSet.getBirthday());
                }


            }
        }
        return rootView;
    }

    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        switch (v.getId()) {
            case R.id.profile_next_button:
                doUpdateProfile();
                break;
            case R.id.profile_birthday:
                imm.hideSoftInputFromWindow(mProfileBirthday.getWindowToken(), 0);

                if (!dataPicker.isShowing()) {
                    dataPicker.show();
                }

                break;
            case R.id.profile_skip:
                //跳過更新會員資料直接登入
                ((UserLoginActivity) getActivity()).loginForever();
                break;
            case R.id.reset_send_button:
                //更新密碼
                doUpdateProfile();
                break;
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetUpdateProfile:
                //更新完使用者資料 接著登入
                ((UserLoginActivity) getActivity()).loginForever();
                break;
            case MednetSetPassword:
                //修改完密碼
                if (result != null && "success".equals(result.optString("message"))) {
                    //修改完成 登入
                    ((ForgetPasswordActivity) getActivity()).loginForever();
                }

                break;
        }

    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        switch (textView.getId()) {
            case R.id.profile_firstname:
                mProfileGender.findFocus();
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                mProfileGender.performClick();
                break;
            case R.id.profile_birthday:
                //xmProfileNextButton.requestFocus();
                mProfileNextButton.performClick();
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                break;
            case R.id.reset_password_retry:
                //xmProfileNextButton.requestFocus();
                mResetSendButton.performClick();
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                break;
        }
        return false;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        switch (view.getId()) {
            case R.id.profile_birthday:
                if (b) {
                    imm.hideSoftInputFromWindow(mProfileBirthday.getWindowToken(), 0);
                    if (!dataPicker.isShowing()) {
                        dataPicker.show();
                    }
                }
                break;
        }
    }
}
