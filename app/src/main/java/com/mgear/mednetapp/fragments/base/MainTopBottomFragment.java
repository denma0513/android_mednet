package com.mgear.mednetapp.fragments.base;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.DrawerMenuAdapter;
import com.mgear.mednetapp.fragments.WebViewFragment;
import com.mgear.mednetapp.helpers.BottomNavigationViewHelper;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.NewDownloadImageTask;

import org.json.JSONObject;

public class MainTopBottomFragment extends MegaBaseFragment implements View.OnClickListener, TaskPost2ExecuteImpl, INativeActionImpl, IBackHandleIpml,
        NavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getName();

    //畫面元件
    private BottomNavigationView mBottomNavigation, mSpecialBottom;
    private NavigationView menuLayout;
    private AppBarLayout mTopBar;
    private Toolbar toolBar;
    //private Fragment fragmentMain;
    private FrameLayout mainContent;
    private ImageView mMemberPic, mTopBack;
    private RelativeLayout mSpecialTop;
    private LinearLayout mSpecialBottomLayout;
    private TextView mTopTitle, mTopRightBtn;
    private Button mBottomButton;


    //從activity來的元件
    private DrawerLayout drawerLayout;
    private ExpandableListView mMenuListView;
    private String actionType = "3";
    private String topType = "default";
    private Context mContext;


    //邏輯元件
    private MegaBaseFragment selectedFragment;
    private View rootView;


    /**
     * 接收從activity 來的畫面元件 drawerLayout
     *
     * @param context
     */
    public MainTopBottomFragment setContext(Context context) {
        this.mContext = context;
        return this;
    }

    /**
     * 接收從activity 來的畫面元件 drawerLayout
     *
     * @param drawer
     */
    public MainTopBottomFragment setDrawer(DrawerLayout drawer) {
        this.drawerLayout = drawer;
        return this;
    }

    /**
     * 接收從activity 來的畫面元件 menu
     *
     * @param menu
     */
    public MainTopBottomFragment setMenu(ExpandableListView menu) {
        this.mMenuListView = menu;
        return this;
    }

    /**
     * 接收從activity 來的action type
     *
     * @param action
     */
    public MainTopBottomFragment setAction(String action) {
        actionType = action;
        return this;
    }

    /**
     * 接收從activity 來的action type
     *
     * @param type
     */
    public MainTopBottomFragment setTopType(String type) {
        topType = type;
        return this;
    }


    private void initView() {

        Log.i("debug", topType);
        if ("default".equals(topType)) {
            mTopBar.setVisibility(View.VISIBLE);
            mSpecialTop.setVisibility(View.GONE);
            mSpecialBottomLayout.setVisibility(View.GONE);
            mBottomNavigation.setVisibility(View.VISIBLE);
        } else {
            mSpecialTop.setVisibility(View.VISIBLE);
            mTopBar.setVisibility(View.GONE);
            mSpecialBottomLayout.setVisibility(View.VISIBLE);
            mBottomNavigation.setVisibility(View.GONE);

            mSpecialBottom.setOnNavigationItemSelectedListener(this);
            BottomNavigationViewHelper.disableShiftMode(mSpecialBottom);
            mSpecialBottom.setSelectedItemId(R.id.mainPage);
        }

        switch (topType) {
            case "blood_sugar":
                mTopTitle.setText("血糖");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "blood_pressure":
                mTopTitle.setText("血壓");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "blood_oxygen":
                mTopTitle.setText("血氧");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "drinking":
                mTopTitle.setText("飲水");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "sleeping":
                mTopTitle.setText("睡眠時間");
                mTopRightBtn.setVisibility(View.GONE);
                mBottomButton.setText("新增紀錄");
                break;
            case "step":
                mTopTitle.setText("每日步數");
                mTopRightBtn.setText("目標設定");
                mBottomButton.setText("新增紀錄");
                mBottomButton.setEnabled(false);
                break;
            case "body_mass_weight":
                mTopTitle.setText("體位");
                mTopRightBtn.setText("身高設定");
                mBottomButton.setText("新增紀錄");
                break;

            case "default":

                //讓bottom的icon固定
                BottomNavigationViewHelper.disableShiftMode(mBottomNavigation);
                mBottomNavigation.setOnNavigationItemSelectedListener(this);
                mMemberPic.setOnClickListener(this);

                //預設bottom選項
                mBottomNavigation.setSelectedItemId(R.id.mainPage);

                if (MegaApplication.ActionTypeMainPage.equals(actionType) && MegaApplication.ActionTypeHealthPush.equals(actionType)) {
                    //mBottomNavigation.setSelectedItemId(R.id.mainPage);
                }

                //設定左側選單的adapter 並展開所有選項 且設定group無法點擊
                DrawerMenuAdapter drawerMenuAdapter = new DrawerMenuAdapter(getActivity());
                if (mMenuListView != null) {
                    mMenuListView.setAdapter(drawerMenuAdapter);
                }
                for (int i = 0; i < drawerMenuAdapter.getGroupCount(); i++) {
                    mMenuListView.expandGroup(i);
                }
                mMenuListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                        return true;
                    }
                });


                //將toolbar 設定到頂端的action bar
                ((AppCompatActivity) getActivity()).setSupportActionBar(toolBar);

                //隱藏title
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

                //左側選單漢堡圖示設定
                ActionBarDrawerToggle acrionActionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolBar, R.string.msg_drawer_open, R.string.msg_drawer_close) {
                    @Override
                    public void onDrawerClosed(View draweView) {
                        super.onDrawerClosed(draweView);
                        Log.d(TAG, "onDrawerClosed");
                    }

                    @Override
                    public void onDrawerOpened(View draweView) {
                        super.onDrawerOpened(draweView);
                        Log.d(TAG, "onDrawerOpened");
                    }
                };
                drawerLayout.addDrawerListener(acrionActionBarDrawerToggle);
                acrionActionBarDrawerToggle.syncState();

                //取消功能表圖示的色調
                mBottomNavigation.setItemIconTintList(null);
                mSpecialBottom.setItemIconTintList(null);
                break;
        }


    }


//    //依照action type來繪製主畫面
//    public void gotoActionDragment(Fragment fragmentMain) {
//        try {
//            FragmentManager fragmentManager = getFragmentManager();
//            switch (actionType) {
//                case MegaApplication.ActionTypeMainPage:
//                    if (fragmentMain == null) {
//                        fragmentMain = new HomePageFragment();
//                    }
//                    FragmentTransaction ft = fragmentManager.beginTransaction();
//                    if (fragmentMain.isAdded()) { // 如果 home fragment 已經被 add 過，
//                        ft.show(fragmentMain); // 顯示它。
//                    } else { // 反之，
//                        ft.add(R.id.main_content, fragmentMain, "HOME"); // 使用 add 方法。
//                        ft.addToBackStack("HOME");
//                    }
//                    ft.commit();
//                    break;
//                case MegaApplication.ActionTypeHealthPush:
//
//                    if (fragmentMain == null) {
//                        fragmentMain = new HealthListFragment();
//                    }
//                    //fragmentMain = healthListFragment;
//                    ft = fragmentManager.beginTransaction();
//                    if (fragmentMain.isAdded()) { // 如果 home fragment 已經被 add 過，
//                        ft.show(fragmentMain); // 顯示它。
//                    } else { // 反之，
//                        ft.add(R.id.main_content, fragmentMain, "HEALTH"); // 使用 add 方法。
//                        ft.addToBackStack("HEALTH");
//                    }
//                    ft.commit();
//                    break;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.activity_main, container, false);
            mTopBar = rootView.findViewById(R.id.topBar);
            toolBar = rootView.findViewById(R.id.toolbar);
            mainContent = rootView.findViewById(R.id.main_content);
            mMemberPic = rootView.findViewById(R.id.main_pic);
            mBottomNavigation = rootView.findViewById(R.id.main_bottom);

            //Special
            mSpecialTop = rootView.findViewById(R.id.special_top);
            mTopBack = rootView.findViewById(R.id.top_back);
            mTopTitle = rootView.findViewById(R.id.top_title);
            mTopRightBtn = rootView.findViewById(R.id.top_right_btn);
            mSpecialBottomLayout = rootView.findViewById(R.id.special_bottom_layout);
            //mSpecialBottom = rootView.findViewById(R.id.special_bottom);
            mBottomButton = rootView.findViewById(R.id.bottom_button);

            mBottomButton.setOnClickListener((View.OnClickListener) mContext);
            mTopBack.setOnClickListener((View.OnClickListener) mContext);
            mTopRightBtn.setOnClickListener((View.OnClickListener) mContext);

            initView();

        }
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_pic:
                Log.i(TAG, "memberPic");
                Bundle bundle = new Bundle();
                Intent integer = new Intent(getActivity(), UserLoginActivity.class);
                integer.putExtras(bundle);
                startActivity(integer);
                break;
        }
    }

    /**
     * Api reponse handle
     */
    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case AppCheckUpgradeTask:
                Log.i(TAG, result + getActivity().getPackageName());
                break;
            case MednetGetProfile:
                Log.i("debug", result + "result = " + result);
                if (result != null && !result.isNull("source")) {
                    JSONObject source;

                    source = result.optJSONObject("source");
                    String customerAppId = source.optString("customer_app_id");
                    String customerId = source.optString("customer_id");
                    String email = source.optString("email");
                    String name = source.optString("name");
                    String firstName = source.optString("first_name");
                    String lastName = source.optString("last_name");
                    String webToken = source.optString("web_token");
                    String cellphone = source.optString("cellphone");
                    String gender = source.optString("gender");
                    String birthday = source.optString("birthday");
                    String profileImage = source.optString("profile_image");
                    String coverImage = source.optString("cover_image");
                    MegaApplication.getInstance().getMember().setName(name);
                    MegaApplication.getInstance().getMember().setCustomerAppId(customerAppId);
                    MegaApplication.getInstance().getMember().setCustomerId(customerId);
                    MegaApplication.getInstance().getMember().setEmail(email);
                    MegaApplication.getInstance().getMember().setFirstName(firstName);
                    MegaApplication.getInstance().getMember().setLastName(lastName);
                    MegaApplication.getInstance().getMember().setWebToken(webToken);
                    MegaApplication.getInstance().getMember().setCellphone(cellphone);
                    MegaApplication.getInstance().getMember().setGender(gender);
                    MegaApplication.getInstance().getMember().setBirthday(birthday);
                    MegaApplication.getInstance().getMember().setProfileImage(profileImage);
                    MegaApplication.getInstance().getMember().setCoverImage(coverImage);
                    updateMember();
                }
                break;
        }

    }

    private void updateMember() {
        if ("".equals(MegaApplication.getInstance().getMember().getProfileImage())) {
            new NewDownloadImageTask(mMemberPic).execute(MegaApplication.getInstance().getMember().getProfileImage());
        }
        //menu.getItem(0).setTitle(MegaApplication.getInstance().getMember().getName());
    }

    /**
     * 建置top bar 畫面
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_top_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * 建置top bar 點擊事件
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notice:
                Log.d("debug", "notice");
                return true;
            case R.id.shopping_car:
                Log.d("debug", "shoppingCar");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * 監控地的選項點擊觸發
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        Log.d(TAG, "item.getItemId() = " + id);
        FragmentManager fragmentManager = getFragmentManager();
        Intent intent;
        switch (id) {
            case R.id.mainPage:
                Log.i("debug", "mainPage");
//                Intent intent = new Intent(getActivity(), MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //如果這Activity是開啟的就不再重複開啟
//                getActivity().startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));

//                String name = getActivity().getPackageName();
//                if (!name.contains("MainActivity")) {
//                    Intent intent = new Intent(getActivity(), MainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //如果這Activity是開啟的就不再重複開啟
//                    getActivity().startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));
//                }
                //                this.startActivity(intent);
                //gotoActionDragment();
                break;
            case R.id.item2:
                try {
                    WebViewFragment webFragment = new WebViewFragment();
                    webFragment.setTextZoom(1.2f);
                    //webFragment.setUrl("question_link", "https://med-net.com/Support");
                    webFragment.setUrl("question_link", "https://med-net.com/PhysicalExamination");
                    //fragmentMain = webFragment;
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.main_content, webFragment);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.item3:
                try {
                    WebViewFragment webFragment = new WebViewFragment();
                    webFragment.setTextZoom(1.2f);
                    webFragment.setUrl("question_link", "https://med-net.com/Member");
                    //webFragment.setUrl("question_link", "https://med-net.com/Member");
                    //webFragment = webFragment;
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.main_content, webFragment);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.item4:
                try {
                    WebViewFragment webFragment = new WebViewFragment();
                    webFragment.setTextZoom(1.2f);
                    webFragment.setUrl("question_link", "https://med-net.com/MemberFavorite");
                    //fragmentMain = webFragment;
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.main_content, webFragment);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.orgApp:
                //開啟機構端檢中功能
                intent = new Intent(getActivity(), CultureSelectActivity.class);
                this.startActivity(intent);

                break;
        }
        return true;
    }

//    @Override
//    public void onPointerCaptureChanged(boolean hasCapture) {
//
//    }

    /**
     * 首頁點擊事件監控
     */
    @Override
    public void triggerAction(String action, String target) {
        Log.i("debug", "action: " + action + " target: " + target);
        //triggerAction(action, target, R.id.main_content);
        drawerLayout.closeDrawers();
    }


    /**
     * 管理首頁中開啟的fragmaent
     * selectedFragment 為目前顯示的視窗層
     * 當觸發onBackPressed時會依照個畫面有不同動作
     * ＥＸ 當name = HOME 時，表示當時在首頁畫面
     * 則直接關閉ＡＰＰ
     */
    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {
        this.selectedFragment = selectedFrangment;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
//        drawerLayout.closeDrawers();
//        Log.i("debug", "mainActivity, on Start");
//        String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
//        String email = MegaApplication.getInstance().getMember().getEmail();
//        Log.i("debug", "accessToken = " + accessToken + ", \nMegaApplication.getInstance().getMember().isNullEmail() " + MegaApplication.getInstance().getMember().isNullEmail());
//
//        //有token 但是email是空的 表示有登入 但是資料尚未同步回ＡＰＰ
//        if (accessToken != null && MegaApplication.getInstance().getMember().isNullEmail()) {
//            Log.i("debug", "ＡＰＩ");
//            new CustomerApiTask(getActivity(), this, true, "", TaskEnum.MednetGetProfile).execute();
//        }
    }


    @Override
    protected void lazyLoad() {

    }

}
