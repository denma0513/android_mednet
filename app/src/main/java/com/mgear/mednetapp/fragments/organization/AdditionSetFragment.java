package com.mgear.mednetapp.fragments.organization;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.ExpandableCheckableListAdapter;
import com.mgear.mednetapp.entity.organization.Organization;
import com.mgear.mednetapp.enums.ConfirmEnum;
import com.mgear.mednetapp.enums.StatusEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.interfaces.organization.AdditionCheckableImpl;
import com.mgear.mednetapp.interfaces.organization.ConfirmClickImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.entity.organization.CheckableItem;
import com.mgear.mednetapp.entity.organization.CheckableItemSet;
import com.mgear.mednetapp.entity.organization.CustomerReportExamine;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.task.organization.CustomerAdditionSelectedTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.ConfirmTotalDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Jye on 2017/6/14.
 * class: AdditionSetFragment
 */
public class AdditionSetFragment extends BaseFragment implements
        ExpandableListView.OnChildClickListener,
        ExpandableListView.OnItemLongClickListener,
        View.OnClickListener,
        DialogInterface.OnClickListener,
        ConfirmClickImpl,
        TaskPostExecuteImpl {

    private boolean isPrepared;
    private TaskEnum executeTask;

    private View rootView;
    private ExpandableListView expandableListView;
    private ExpandableCheckableListAdapter expandableListAdapter;
    private HashMap<String, List<CheckableItem>> expandableListDetail;
    private List<String> expandableListTitle;
    private CheckableItemSet selectedItem;
    private View btnSend;

    private AdditionCheckableImpl additionListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            selectedItem = new CheckableItemSet();
            Activity activity = getActivity();
            if (activity instanceof AdditionCheckableImpl)
                additionListener = (AdditionCheckableImpl) activity;

            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_addition_set, container, false);
            TextView txtHelper = (TextView) rootView.findViewById(R.id.txtAdditionSetHelper);
            expandableListView = (ExpandableListView) rootView.findViewById(R.id.elvAdditionItem);
            btnSend = rootView.findViewById(R.id.btnAdditionSend);
            btnSend.setOnClickListener(this);

            //判斷目前狀態
            int status = CustomerActivity.getCustomer().getStatus();
            if (status == StatusEnum.WaitConfirm.getCode() || status == StatusEnum.End.getCode()) {
                //狀態已結束，無法加選
                expandableListView.setVisibility(View.GONE);
                txtHelper.setVisibility(View.VISIBLE);
            }
            else {
                //取得客戶的所有檢查項目
                HashMap<String, Integer> map = new HashMap<>();
                ArrayList<CustomerReportExamine> examines = CustomerActivity.getCustomer().getExamineList();
                for (CustomerReportExamine exam : examines)
                    map.put(exam.getExamineCode(), 0);

                //將已加選的項目加入
                ArrayList<UUID> adds;
                if (additionListener != null) {
                    //取得已加選的清單
                    adds = new ArrayList<>(CustomerActivity.getCustomerAdditionList().keySet());
                }
                else adds = new ArrayList<>();

                //載入加選清單
                expandableListDetail = CustomerActivity.getAdditionItems().getExpandableList(map, adds, CustomerActivity.getCustomer().getGender());
                expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
                expandableListAdapter = new ExpandableCheckableListAdapter(activity, expandableListTitle, expandableListDetail);
                expandableListView.setAdapter(expandableListAdapter);
                expandableListView.setOnChildClickListener(this);
                expandableListView.setOnItemLongClickListener(this);

                txtHelper.setVisibility(View.GONE);
                expandableListView.setVisibility(View.VISIBLE);
            }
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        CheckableItem checkableItem = this.expandableListDetail.get(this.expandableListTitle.get(groupPosition)).get(childPosition);
        boolean check = checkableItem.isCheck();
        if (check) {
            //移除選取清單
            selectedItem.remove(checkableItem.getId());
        }
        else {
            //加入選取清單
            selectedItem.put(checkableItem.getId(), checkableItem);
        }
        checkableItem.setCheck(!check);
        //判斷是否顯示送出按鈕
        btnSend.setVisibility(selectedItem.size() > 0 ? View.VISIBLE : View.GONE);
        expandableListAdapter.notifyDataSetChanged();
        return false;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        long packedPosition = expandableListView.getExpandableListPosition(position);
        int itemType = ExpandableListView.getPackedPositionType(packedPosition);
        int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
        int childPosition = ExpandableListView.getPackedPositionChild(packedPosition);
        if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            CheckableItem checkableItem = this.expandableListDetail.get(this.expandableListTitle.get(groupPosition)).get(childPosition);
            new AlertDialog.Builder(getActivity()).setCancelable(false)
                    .setTitle(checkableItem.getName())
                    .setMessage(checkableItem.getMemo())
                    .setPositiveButton(R.string.button_confirm, null).show();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (selectedItem.size() > 0) {
            int i = 0, money = 0;
            //初始對話視窗的內容
            StringStringSet[] content = new StringStringSet[selectedItem.size()];
            for (CheckableItem checkableItem : selectedItem.values()) {
                content[i++] = new StringStringSet(checkableItem.getName(), checkableItem.getSubName());
                money += checkableItem.getNumber();
            }
            //顯示確認視窗
            ConfirmTotalDialog dialog = new ConfirmTotalDialog(getActivity(), ConfirmEnum.TotalConfirm,
                    getString(R.string.title_confirm_addition),
                    String.format(Locale.getDefault(), getString(R.string.field_total_money), money),
                    getString(R.string.msg_addition_confirm), content, false);
            dialog.setButtonOnClickListener(this);
            dialog.show();
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (executeTask) {
            case CustomerAdditionSelectedTask:
                StringBuilder sb = new StringBuilder();
                for (UUID uuid : selectedItem.keySet())
                    sb.append(uuid.toString()).append(',');
                //刪除最後一個字元
                sb.deleteCharAt(sb.length() - 1);
                new CustomerAdditionSelectedTask(getActivity(), this, true, getString(R.string.msg_customer_addition))
                        .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()), sb.toString());
                break;
            default:
                break;
        }
    }

    @Override
    public void onConfirmClick(ConfirmEnum type) {
        if (type == ConfirmEnum.TotalConfirm && selectedItem.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (UUID uuid : selectedItem.keySet())
                sb.append(uuid.toString()).append(',');
            //刪除最後一個字元
            sb.deleteCharAt(sb.length() - 1);
            new CustomerAdditionSelectedTask(getActivity(), this, true, getString(R.string.msg_customer_addition))
                    .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()), sb.toString());
        }
    }

    @Override
    public void onCancelClick(ConfirmEnum type) {
        if (type == ConfirmEnum.TotalConfirm && selectedItem.size() > 0) {
            for (CheckableItem checkableItem : selectedItem.values()) {
                checkableItem.setCheck(false);
            }
            selectedItem.clear();
            expandableListAdapter.notifyDataSetChanged();
            btnSend.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        executeTask = type;
        switch (type) {
            case CustomerAdditionSelectedTask:
                if ((boolean) result) {
                    //過濾加選成功的清單
                    for (Map.Entry<UUID, CheckableItem> entry : selectedItem.entrySet()) {
                        List<CheckableItem> lst = expandableListDetail.get(entry.getValue().getType());
                        if (lst != null)
                            lst.remove(entry.getValue());
                    }
                    expandableListAdapter.notifyDataSetChanged();
                    if (additionListener != null)
                        additionListener.setCustomerAddition(selectedItem);
                    selectedItem.clear();
                    btnSend.setVisibility(View.GONE);
                }
                else {
                    //顯示失敗訊息
                    new AlertDialog.Builder(getActivity()).setCancelable(false)
                            .setTitle(R.string.title_warning)
                            .setMessage(R.string.msg_send_addition_fail)
                            .setPositiveButton(R.string.button_retry, this).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("addition_set", CommonFunc.getNowSeconds());
    }

}