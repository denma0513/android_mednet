package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.ExpandableStringSetAdapter;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.entity.organization.CustomerReportExamineSet;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.CustomerExamineUpdateTask;
import com.mgear.mednetapp.utils.CommonFunc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by Jye on 2017/6/13.
 * class: TestsFragment
 */
public class TestsFragment extends Fragment
        implements ExpandableListView.OnItemLongClickListener, View.OnClickListener, TaskPostExecuteImpl {

    private View rootView;
    private ExpandableListView expandableListView;
    private List<String> expandableListTitle;
    private HashMap<String, List<StringStringSet>> expandableListDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("tests", CommonFunc.getNowSeconds());
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tests, container, false);
            View btnRefresh = rootView.findViewById(R.id.btnTestRefresh);
            btnRefresh.setOnClickListener(this);

            //設定檢查項目類別
            String strWaiting = getString(R.string.title_wait_examine);
            String strComplete = getString(R.string.title_complete_examine);
            String strSupplement = getString(R.string.title_supplement_examine);
            //加入檢查項目類別分類對應清單
            SparseArray<String> map = new SparseArray<>();
            map.put(2, strWaiting);
            map.put(3, strWaiting);
            map.put(5, strWaiting);
            map.put(4, strComplete);
            map.put(7, strComplete);
            map.put(9, strComplete);
            map.put(8, strSupplement);

            expandableListView = (ExpandableListView) rootView.findViewById(R.id.elvTestsItem);
            expandableListDetail = CustomerActivity.getCustomer().getExamineListByStatus(map);
            //取得標題列
            expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
            ExpandableStringSetAdapter expandableListAdapter = new ExpandableStringSetAdapter(getActivity(), expandableListTitle, expandableListDetail);
            expandableListView.setAdapter(expandableListAdapter);
            expandableListView.setOnItemLongClickListener(this);
        }
        return rootView;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        long packedPosition = expandableListView.getExpandableListPosition(position);
        int itemType = ExpandableListView.getPackedPositionType(packedPosition);
        int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
        int childPosition = ExpandableListView.getPackedPositionChild(packedPosition);
        if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            StringStringSet item = this.expandableListDetail.get(this.expandableListTitle.get(groupPosition)).get(childPosition);
            new AlertDialog.Builder(getActivity()).setCancelable(false)
                    .setTitle(item.getMajor())
                    .setMessage(CustomerActivity.getExamineList().getExamineDescription(item.getMinor()))
                    .setPositiveButton(R.string.button_confirm, null).show();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        //取得今日健檢人員的檢查項目清單的更新
        new CustomerExamineUpdateTask(getActivity(), this, true, getString(R.string.msg_customer_examine_update))
                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        switch (type) {
            case CustomerExamineUpdateTask:
                //更新客戶檢查項目清單
                CustomerReportExamineSet examineSet = (CustomerReportExamineSet) result;
                if (examineSet.getStatus() == HTTP_OK && examineSet.size() > 0) {
                    CustomerActivity.getCustomer().setExamine(examineSet);

                    //設定檢查項目類別
                    String strWaiting = getString(R.string.title_wait_examine);
                    String strComplete = getString(R.string.title_complete_examine);
                    String strSupplement = getString(R.string.title_supplement_examine);
                    //加入檢查項目類別分類對應清單
                    SparseArray<String> map = new SparseArray<>();
                    map.put(2, strWaiting);
                    map.put(3, strWaiting);
                    map.put(5, strWaiting);
                    map.put(4, strComplete);
                    map.put(7, strComplete);
                    map.put(9, strComplete);
                    map.put(8, strSupplement);

                    expandableListDetail = CustomerActivity.getCustomer().getExamineListByStatus(map);
                    //取得標題列
                    expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
                    ExpandableStringSetAdapter expandableListAdapter = new ExpandableStringSetAdapter(getActivity(), expandableListTitle, expandableListDetail);
                    expandableListView.setAdapter(expandableListAdapter);
                }
                break;
            default:
                break;
        }
    }
}