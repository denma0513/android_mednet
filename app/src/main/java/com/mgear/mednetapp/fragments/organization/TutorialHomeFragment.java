package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/6/9.
 * class: TutorialHomeFragment
 */
public class TutorialHomeFragment extends Fragment {

    private View rootView;
    private ImageView imgItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tutorial_home, container, false);
            RelativeLayout root = (RelativeLayout) rootView.findViewById(R.id.rootTutorialHome);
            root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_home_bg)));
            ImageView imgCover = (ImageView) rootView.findViewById(R.id.imgTutorialHomeCover);
            imgItem = (ImageView) rootView.findViewById(R.id.imgTutorialHomeItem);
            imgItem.setImageBitmap(CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_home_item));
            imgCover.setImageBitmap(CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_home_cover));
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        imgItem.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.vertical_movement));
    }

    @Override
    public void onPause() {
        super.onPause();
        imgItem.clearAnimation();
    }

}