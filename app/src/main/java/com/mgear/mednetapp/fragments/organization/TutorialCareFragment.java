package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.Customer;
import com.mgear.mednetapp.utils.CommonFunc;

import java.util.Locale;

/**
 * Created by Jye on 2017/6/9.
 * class: TutorialCareFragment
 */
public class TutorialCareFragment extends Fragment {

    private View rootView;
    private ImageView imgCare;
    private ImageView imgHeart1;
    private ImageView imgHeart2;
    private ImageView imgHeart3;
    private ImageView imgHeart4;
    private ImageView imgHeart5;
    private ImageView imgHeart6;
    private ImageView imgHeart7;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tutorial_care, container, false);
            RelativeLayout root = (RelativeLayout) rootView.findViewById(R.id.rootTutorialCare);
            root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_care_bg)));
            imgCare = (ImageView) rootView.findViewById(R.id.imgTutorialCare);
            imgHeart1 = (ImageView) rootView.findViewById(R.id.imgTutorialCareHeart1);
            imgHeart2 = (ImageView) rootView.findViewById(R.id.imgTutorialCareHeart2);
            imgHeart3 = (ImageView) rootView.findViewById(R.id.imgTutorialCareHeart3);
            imgHeart4 = (ImageView) rootView.findViewById(R.id.imgTutorialCareHeart4);
            imgHeart5 = (ImageView) rootView.findViewById(R.id.imgTutorialCareHeart5);
            imgHeart6 = (ImageView) rootView.findViewById(R.id.imgTutorialCareHeart6);
            imgHeart7 = (ImageView) rootView.findViewById(R.id.imgTutorialCareHeart7);
            TextView txtName = (TextView) rootView.findViewById(R.id.txtTutorialCareName);
            TextView txtGender = (TextView) rootView.findViewById(R.id.txtTutorialCareGender);
            Customer customer = CustomerActivity.getCustomer();
            txtName.setText(customer.getName());
            txtGender.setText(String.format(Locale.getDefault(), getString(R.string.field_gender_hello), customer.getGender() == 'M' ? getString(R.string.field_mr) : getString(R.string.field_ms)));
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        imgCare.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.bounce));
        imgHeart1.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_light));
        imgHeart2.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_light));
        imgHeart3.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_light));
        imgHeart4.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_light));
        imgHeart5.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_light));
        imgHeart6.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_light));
        imgHeart7.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_light));
    }

    @Override
    public void onPause() {
        super.onPause();
        imgCare.clearAnimation();
        imgHeart1.clearAnimation();
        imgHeart2.clearAnimation();
        imgHeart3.clearAnimation();
        imgHeart4.clearAnimation();
        imgHeart5.clearAnimation();
        imgHeart6.clearAnimation();
        imgHeart7.clearAnimation();
    }

}