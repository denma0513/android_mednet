package com.mgear.mednetapp.fragments.organization;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.SubListAdapter;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/8/9.
 * class: HealthyQuestionFragment
 */
public class HealthyQuestionFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private boolean isPrepared;

    private View rootView;
    private ListView listView;

    public void setContent(StringStringSet[] content) {
        if (isPrepared) {
            SubListAdapter adapter = new SubListAdapter(getActivity(), R.layout.item_question_sub_list, content);
            listView.setAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_healthy_question, container, false);
            listView = (ListView) rootView.findViewById(R.id.lstHealthyQuestion);
            listView.setOnItemClickListener(this);
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView textView = (TextView) view.findViewById(R.id.txtSubListMinorName);
        if (textView.getVisibility() == View.GONE)
            textView.setVisibility(View.VISIBLE);
        else
            textView.setVisibility(View.GONE);
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("healthy_question", CommonFunc.getNowSeconds());
    }
}