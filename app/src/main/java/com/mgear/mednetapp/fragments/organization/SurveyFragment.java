package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.RatingRecyclerAdapter;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.entity.organization.SurveyItemSet;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.CustomerSurveyScoreTask;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/8/1.
 * class: SurveyFragment
 */
public class SurveyFragment extends Fragment implements View.OnClickListener, TaskPostExecuteImpl {

    private static boolean finish;
    private static String people = "";
    private static String suggest = "";
    public static boolean isFinish() { return finish; }
    public static void init() {
        finish = false;
        people = "";
        suggest = "";
    }

    private View rootView;
    private RecyclerView recyclerView;
    private EditText txtPeople;
    private EditText txtSuggest;
    private Button btnSubmit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("survey", CommonFunc.getNowSeconds());
        if (rootView == null) {
            OrgMainActivity mainActivity = (OrgMainActivity) getActivity();
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_survey, container, false);
            btnSubmit = (Button) rootView.findViewById(R.id.btnSurveySubmit);
            btnSubmit.setOnClickListener(this);
            txtPeople = (EditText) rootView.findViewById(R.id.txtSurveyPeople);
            txtPeople.setOnKeyListener(mainActivity);
            txtSuggest = (EditText) rootView.findViewById(R.id.txtSurveySuggest);
            txtSuggest.setOnKeyListener(mainActivity);

            recyclerView = (RecyclerView) rootView.findViewById(R.id.rvSurveyRating);
            recyclerView.setNestedScrollingEnabled(false);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);

            //設定滿意度調查項目
            RatingRecyclerAdapter adapter = new RatingRecyclerAdapter(CustomerActivity.getSurveyItems(), finish);
            recyclerView.setAdapter(adapter);
        }
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        txtPeople.setText(people);
        txtSuggest.setText(suggest);
        if (finish) {
            txtPeople.setEnabled(false);
            txtPeople.setFocusable(false);
            txtSuggest.setEnabled(false);
            txtSuggest.setFocusable(false);
            btnSubmit.setVisibility(View.INVISIBLE);
            Snackbar.make(rootView, R.string.msg_survey_finish, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!finish) {
            //取得滿意度項目
            SurveyItemSet surveyItemSet = CustomerActivity.getSurveyItems();
            int itemSize = recyclerView.getChildCount();
            //紀錄當前的評分狀態
            for (int i = 0; i < itemSize; i++) {
                View view = recyclerView.getChildAt(i);
                RatingBar rb = (RatingBar) view.findViewById(R.id.rbSurveyItem);
                //紀錄滿意度的評分
                surveyItemSet.get(i).setRating(rb.getRating());
                rb.setEnabled(false);
            }
            people = txtPeople.getText().toString();
            suggest = txtSuggest.getText().toString();
        }
    }

    @Override
    public void onClick(View v) {
        //取得所有檢查項目的評分
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < recyclerView.getChildCount(); i++) {
            View view = recyclerView.getChildAt(i);
            RatingBar rb = (RatingBar) view.findViewById(R.id.rbSurveyItem);
            //取得評分
            float score = rb.getRating();
            //餐點滿意度和醫師專業滿意度改成非必填
            if (i != 3 && i != 10 && score == 0f) {
                Snackbar.make(rootView, R.string.msg_survey_verify, Snackbar.LENGTH_LONG).show();
                return;
            }
            sb.append(rb.getTag()).append(',').append(score).append(',');
        }
        //移除最後一個分隔逗號
        if (sb.length() > 0)
            sb.deleteCharAt(sb.length() - 1);

        //送出評分
        new CustomerSurveyScoreTask(getActivity(), this, true, getString(R.string.msg_send_survey_score))
                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()),
                        sb.toString(), txtPeople.getText().toString(), txtSuggest.getText().toString());
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        if (type == TaskEnum.CustomerSurveyScoreTask) {
            btnSubmit.setVisibility(View.INVISIBLE);
            //取得滿意度項目
            SurveyItemSet surveyItemSet = CustomerActivity.getSurveyItems();
            int itemSize = recyclerView.getChildCount();
            for (int i = 0; i < itemSize; i++) {
                View view = recyclerView.getChildAt(i);
                RatingBar rb = (RatingBar) view.findViewById(R.id.rbSurveyItem);
                //紀錄滿意度的評分
                surveyItemSet.get(i).setRating(rb.getRating());
                rb.setEnabled(false);
            }
            people = txtPeople.getText().toString();
            suggest = txtSuggest.getText().toString();
            finish = true;
            Snackbar.make(rootView, R.string.msg_survey_finish, Snackbar.LENGTH_LONG).show();
            //切換回地圖頁面
            ((OrgMainActivity) getActivity()).viewMap();
        }
    }

}