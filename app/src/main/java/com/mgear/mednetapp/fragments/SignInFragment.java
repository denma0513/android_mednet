package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.SyncStateContract;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mgear.mednetapp.ForgetPasswordActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.entity.OauthSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.OnOauth2SignInListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import pl.droidsonroids.gif.GifImageView;


@SuppressLint("ValidFragment")
public class SignInFragment extends MegaBaseFragment implements View.OnClickListener, OnOauth2SignInListener, TextView.OnEditorActionListener, TaskPost2ExecuteImpl {
    //常數
    private static String TAG = "SIGNIN";
    private static int SIGNUP = 0;
    private static int FORGET = 1;
    private static boolean IS_OAUTHLOGIN = false;

    //畫面變數
    private View rootView;
    private Button mSigninNextButton, mSigninCancelButton;
    private EditText mSigninAccount, mSigninPassword;
    private TextView mSigninAccountWarning, mSigninPasswordWarning, mSigninForgetPassword;
    private RelativeLayout mSigninConfirm;
    private GifImageView mSigninLoading;
    private ImageView mSigninLoadingOK;
    private OauthSet mOauthSet;

    /**
     * 呼叫登入
     */
    private void doLogin() {

        boolean done = true;
        mSigninAccountWarning.setVisibility(View.GONE);
        mSigninPasswordWarning.setVisibility(View.GONE);

        if ("".equals(mSigninAccount.getText().toString())) {
            mSigninAccountWarning.setText(getString(R.string.msg_invalid_email_mobile));
            mSigninAccountWarning.setVisibility(View.VISIBLE);
            done = false;
        }

        if (!CommonFunc.isEmailValid(mSigninAccount.getText().toString().replaceAll(" ", "")) && !CommonFunc.isMobileValid(mSigninAccount.getText().toString().replaceAll(" ", ""))) {
            mSigninAccountWarning.setText(getString(R.string.msg_invalid_email_mobile));
            mSigninAccountWarning.setVisibility(View.VISIBLE);
            done = false;
        }

        if ("".equals(mSigninPassword.getText().toString())) {
            mSigninPasswordWarning.setText(getString(R.string.msg_invalid_password));
            mSigninPasswordWarning.setVisibility(View.VISIBLE);
            done = false;
        }

        if (done)
            doLogin(null);
    }

    private void showLoading() {
        mSigninCancelButton.setVisibility(View.GONE);
        mSigninNextButton.setVisibility(View.GONE);
        mSigninLoading.setVisibility(View.VISIBLE);
        mSigninLoadingOK.setVisibility(View.GONE);
        mSigninConfirm.setVisibility(View.VISIBLE);
    }

    Handler mHandler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mSigninCancelButton.setVisibility(View.GONE);
            mSigninNextButton.setVisibility(View.GONE);
            mSigninLoading.setVisibility(View.VISIBLE);
            mSigninConfirm.setVisibility(View.VISIBLE);
            mSigninLoadingOK.setVisibility(View.GONE);
        }
    };

    private void doLogin(OauthSet oauthSet) {
        CommonApiTask task = new CommonApiTask(getActivity(), this, false, "登入中，請稍候...",
                TaskEnum.MednetLoginTask);
        SharedPreferences settings = getActivity().getSharedPreferences("sign", 0);

        if (oauthSet == null) {
            IS_OAUTHLOGIN = false;
            showLoading();
            Log.d(TAG, "帳號密碼登入");
            String account = mSigninAccount.getText().toString().replaceAll(" ", "");
            String password = mSigninPassword.getText().toString();
            task.execute("0", account, password);
            settings.edit().putString("type", "0").putString("account", account).putString("password", password).apply();
            new CommonApiTask(getActivity(), this, false, "", TaskEnum.MednetAppLogEven)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "signin", "0");
        } else {
            IS_OAUTHLOGIN = true;
            mOauthSet = oauthSet;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSigninCancelButton.setVisibility(View.GONE);
                    mSigninNextButton.setVisibility(View.GONE);
                    mSigninLoading.setVisibility(View.VISIBLE);
                    mSigninLoadingOK.setVisibility(View.GONE);
                    mSigninConfirm.setVisibility(View.VISIBLE);
                    // Stuff that updates the UI
                }
            });
            Log.d(TAG, "第三方登入");
            task.execute(oauthSet.getType(), "", "", oauthSet.getAccessToken());
            settings.edit().putString("type", oauthSet.getType()).putString("accessToken", oauthSet.getAccessToken()).apply();
            new CommonApiTask(getActivity(), this, false, "", TaskEnum.MednetAppLogEven)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "signin", oauthSet.getType());
        }
    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo("com.mgear.mednetapp", PackageManager.GET_SIGNATURES);
            String sha1 = "", keyHash = "";
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                keyHash = Base64.encodeToString(md.digest(), Base64.DEFAULT).replaceAll("\n", "");
                Log.d("KeyHash:", keyHash);
            }

            for (Signature signature : info.signatures) {

                MessageDigest digest = MessageDigest.getInstance("SHA-1");
                byte[] buffer = new byte[1024 * 1024 * 10];
                int len = 0;
                digest.update(signature.toByteArray());
                sha1 = new BigInteger(1, digest.digest()).toString(16);
                if (sha1 != null)
                    sha1 = sha1.toUpperCase();
                Log.d("KeyHash:", sha1);
            }
            new CommonApiTask(getActivity(), this, false, "", TaskEnum.AndroidKeyHashApi).executeOnExecutor(MegaApplication.threadPoolExecutor, sha1, keyHash);
        } catch (Exception e) {
            new CommonApiTask(getActivity(), this, false, "", TaskEnum.AndroidKeyHashApi)
                    .executeOnExecutor(MegaApplication.threadPoolExecutor, e.getMessage(), "");
            e.printStackTrace();
        }

    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {

            if (MegaApplication.IsMegaAppVersion) {
                rootView = inflater.inflate(R.layout.user_login_signin, container, false);
            } else {
                rootView = inflater.inflate(R.layout.user_login_signin_org, container, false);
            }

            mSigninConfirm = rootView.findViewById(R.id.signin_confirm);
            mSigninAccount = rootView.findViewById(R.id.signin_account);
            mSigninPassword = rootView.findViewById(R.id.signin_password);
            mSigninNextButton = rootView.findViewById(R.id.signin_next_button);
            mSigninCancelButton = rootView.findViewById(R.id.signin_cancel_button);
            mSigninAccountWarning = rootView.findViewById(R.id.signin_account_warning);
            mSigninPasswordWarning = rootView.findViewById(R.id.signin_password_warning);
            mSigninForgetPassword = rootView.findViewById(R.id.signin_forget_password);

            mSigninLoading = rootView.findViewById(R.id.signin_loading);
            mSigninLoadingOK = rootView.findViewById(R.id.signin_loading_ok);

            mSigninAccountWarning.setVisibility(View.GONE);
            mSigninPasswordWarning.setVisibility(View.GONE);

            mSigninNextButton.setOnClickListener(this);
            mSigninCancelButton.setOnClickListener(this);
            mSigninForgetPassword.setOnClickListener(this);

            mSigninPassword.setOnEditorActionListener(this);

            mSigninConfirm.setVisibility(View.GONE);

            try {
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                Oauth2LoginFragment oauth2LoginFragment = new Oauth2LoginFragment();
                oauth2LoginFragment.setOnOauth2SignInListener(this);
                FragmentTransaction ft = fragmentManager.beginTransaction();
                Fragment fragmentMain = oauth2LoginFragment;
                ft.replace(R.id.signin_auth_layout, fragmentMain);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signin_next_button:
                doLogin();
                break;

            case R.id.signin_cancel_button:
                ((UserLoginActivity) getActivity()).cancel();
                break;

            case R.id.signin_forget_password:
                Intent intent = new Intent(getActivity(), ForgetPasswordActivity.class);
                startActivityForResult(intent, FORGET);
                break;
        }
    }

    @Override
    public void OnSignIn(OauthSet oauth) {
        if (oauth == null) {
            CommonFunc.showToast(getActivity(), "登入失敗！");
            return;
        }
        doLogin(oauth);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetLoginTask: {
                mSigninLoading.setVisibility(View.GONE);
                if (result == null || result.isNull("source") || result.optJSONObject("source").isNull("access_token")) {
                    String message = result.optString("message");
                    //Log.i(TAG, "onPostExecute: result = "+result);
                    //用第三方登入時，若為新帳號則導註冊
                    if ("沒有找到 OauthId".equals(message) && IS_OAUTHLOGIN) {

                        if (CommonFunc.isBlank(mOauthSet.getEmail())) {
                            //假設email是空的
                            CommonFunc.showToast(getActivity(), "請同意取得Email權限！");
                            return;
                        }

                        CommonApiTask task = new CommonApiTask(getActivity(), this, true, getString(R.string.msg_data_loading),
                                TaskEnum.MednetCreateAccount);
                        task.execute(mOauthSet.getType(), mOauthSet.getEmail(), "", mOauthSet.getAccessToken());
                        return;
                    }

                    CommonFunc.showToast(getActivity(), getString(R.string.msg_wrong_account_login));
                    mSigninCancelButton.setVisibility(View.VISIBLE);
                    mSigninNextButton.setVisibility(View.VISIBLE);
                    mSigninConfirm.setVisibility(View.GONE);
                    mSigninLoadingOK.setVisibility(View.VISIBLE);
                    return;
                }

                JSONObject source = result.optJSONObject("source");
                String accessToken = source.optString("access_token");
                String refreshToken = source.optString("refresh_token");
                //登入成功
                if (accessToken != null && accessToken.length() > 0) {
                    mSigninConfirm.setVisibility(View.VISIBLE);
                    mSigninLoading.setVisibility(View.GONE);
                    mSigninNextButton.setVisibility(View.GONE);
                    mSigninCancelButton.setVisibility(View.GONE);
                    mSigninLoadingOK.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            MegaApplication.getInstance().getMember().setAccessToken(accessToken);
                            MegaApplication.getInstance().getMember().setRefreshToken(refreshToken);
                            ((UserLoginActivity) getActivity()).loginForever();
                        }
                    }, 100);
                }
            }
            break;
            case MednetCreateAccount: {
                //建立新會員帳號
                String message = "";
                if (result == null) {
                    message = getString(R.string.msg_cannot_connect);
                }

                if (result.optInt("state_code") != 200) {
                    message = result.optString("message");
                }

                if (message.length() > 0) {
                    CommonFunc.showToast(getActivity(), message);
                    return;
                }

                JSONObject source = result.optJSONObject("source");
                String accessToken = source.optString("access_token");
                String refreshToken = source.optString("refresh_token");
                //Log.i("debug", "accessToken= " + accessToken);
                if (accessToken != null && accessToken.length() > 0) {
                    //寫入accesstoken
                    MegaApplication.getInstance().getMember().setAccessToken(accessToken);
                    MegaApplication.getInstance().getMember().setRefreshToken(refreshToken);
                    ((UserLoginActivity) getActivity()).loginForever();
                }
            }
            break;

        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FORGET) {
            try {
                Bundle bundle = data.getExtras();
                String starus = bundle.getString("status");
                if ("success".equals(starus)) {
                    //登入成功
                    ((UserLoginActivity) getActivity()).loginForever();
                }
            } catch (Exception e) {
                Log.w(TAG, "signInResult:failed code=" + e.getMessage());
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        switch (textView.getId()) {

            case R.id.signin_password:
                mSigninNextButton.requestFocus();
                mSigninNextButton.performClick();
                break;
        }
        return false;
    }
}
