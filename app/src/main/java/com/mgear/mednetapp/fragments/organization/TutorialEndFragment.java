package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/6/9.
 * class: TutorialEndFragment
 */
public class TutorialEndFragment extends Fragment {

    private View rootView;
    private TextView txtLocker;
    private ImageView imgHelp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tutorial_end, container, false);
            RelativeLayout root = (RelativeLayout) rootView.findViewById(R.id.rootTutorialEnd);
            root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_end_bg)));
            txtLocker = (TextView) rootView.findViewById(R.id.txtTutorialLockerHelp);
            imgHelp = (ImageView) rootView.findViewById(R.id.imgTutorialEndHelp);
/*
            //判斷是否顯示置物櫃編號
            String lockerID = CustomerActivity.getCustomer().getLockerID();
            if (lockerID != null && lockerID.length() > 0 && !lockerID.equals("null")) {
                //設定專屬置物櫃編號
                txtLocker.setText(String.format(Locale.getDefault(), getString(R.string.msg_locker_number), CustomerActivity.getCustomer().getLockerID()));
            }*/
        }
        return rootView;
    }
/*
    @Override
    public void onResume() {
        super.onResume();
        RotateAnimation rotate = new RotateAnimation(-15.0f, 15.0f, 1, 0.5f, 1, 0.5f);
        rotate.setDuration(250);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setRepeatCount(-1);
        rotate.setRepeatMode(2);
        imgHelp.startAnimation(rotate);
    }
*/
    @Override
    public void onPause() {
        super.onPause();
        imgHelp.clearAnimation();
    }

}