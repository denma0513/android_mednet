package com.mgear.mednetapp.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mgear.mednetapp.BaseActivity;
import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.entity.OauthSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.interfaces.organization.WebPreviousPageImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.WebViewUtil;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WebViewUploadFragment extends MegaBaseFragment implements WebPreviousPageImpl, TaskPost2ExecuteImpl {

    private static final String TAG = WebViewFragment.class.getSimpleName();

    public static final int REQUEST_CODE_LOLIPOP = 1;
    private final static int RESULT_CODE_ICE_CREAM = 2;

    private WebView mWebView;
    private Context mContext;
    private View rootView;
    private ProgressBar progressBar;

    private int mRootView;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
    private ValueCallback<Uri> mUploadMessage;
    private String strUrl;
    private String nowUrl = "";
    private String title = "";

    //private boolean neesRelogin = false;


    public void setUrl(String url) {
        strUrl = url;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void setRootView(int rootView) {
        mRootView = rootView;
    }

    public boolean canGoBack() {
        return mWebView.canGoBack();
    }

    public void goBack() {
        mWebView.goBack();
    }


    @Override
    protected void lazyLoad() {

    }

    @Override
    public void previousPage() {
        if (mWebView.canGoBack())
            mWebView.goBack();
    }

    @Override
    public boolean hasPreviousPage() {
        return mWebView.canGoBack();
    }


    @Override
    public boolean onBackPressed() {
        Log.i("debug", "mWebView.canGoBack() = " + mWebView.canGoBack());
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return false;
    }


    private void doLogin() {
        CommonApiTask task = new CommonApiTask(mContext, this, false, "", TaskEnum.MednetLoginTask);
        SharedPreferences settings = getActivity().getSharedPreferences("sign", 0);
        String type = settings.getString("type", "");

        if ("0".equals(type)) {
            Log.d(TAG, "帳號密碼登入");
            String account = settings.getString("account", "");
            String password = settings.getString("password", "");
            task.execute("0", account, password);
        } else {
            Log.d(TAG, "第三方登入");
            String accessToken = settings.getString("accessToken", "");
            task.execute(type, "", "", accessToken);
        }
    }


    public class MednetWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            if ("https://med-net.com/Member".equals(nowUrl) && nowUrl.equals(url)) {
                //判斷當開啟前開啟後都是會員中心代表有更改會員資料
                doLogin();
            }

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    // TODO: http request.
                    try {
                        Document doc = Jsoup.connect(url).get();
//                        //Log.i("debug", "doc = " + doc);
                        Elements elements = doc.select("meta[property=og:title]");
//                        //Log.i("debug", "elements = " + elements);
//                        //Log.i("debug", "elements = " + elements.attr("content"));
                        String content = elements.attr("content");
                        Log.i("debug", "content = " + content);
                        //title = content;
                        if (content != null) {
                            //setTitle(content);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            new Thread(runnable).start();

        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i("debug", "shouldOverrideUrlLoading = " + url);
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Log.i("debug", "shouldOverrideUrlLoading = " + request.getUrl().toString());
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.i("debug", "onPageFinished = " + url +" "+url.contains("aidoctor.med-net.com/AIDoctor"));

            //增加AIDoctor專用判斷
            try {
                if (url.contains("aidoctor.med-net.com/AIDoctor")) {
                    ((MainActivity) getActivity()).hideTopBar();
                } else {
                    ((MainActivity) getActivity()).showTopBar();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (String needLoginUrl : MegaApplication.needLoginUrlArray) {
                if (strUrl.contains(needLoginUrl)) {//需要登入的url
                    if (url.equals("https://med-net.com/") || url.contains("med-net.com/Home/Index_m")) {
                        Log.i("debug", "醫聯網 該回去登入囉");
                        JSONObject loginTarget = new JSONObject();
                        try {
                            clearCache();
                            loginTarget.put("action", MegaApplication.ActionTypeWebView);
                            loginTarget.put("target", strUrl);
                            ((BaseActivity) getActivity()).triggerAction(getActivity(), MegaApplication.ActionTypeLogin, loginTarget.toString(), mRootView);
                            //triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (url.contains("sso2.med-net.com")) {
                        Log.i("debug", "醫師諮詢 該回去登入囉");
                        try {
                            clearCache();
                            JSONObject loginTarget = new JSONObject();
                            loginTarget.put("action", MegaApplication.ActionTypeWebView);
                            loginTarget.put("target", strUrl);
                            ((BaseActivity) getActivity()).triggerAction(getActivity(), MegaApplication.ActionTypeLogin, loginTarget.toString(), mRootView);
                            //triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
            nowUrl = url;
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            Log.e("debug", error.getErrorCode() + "" + error.getDescription());
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
        }


        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("SSL 憑證錯誤");
            builder.setMessage("無法驗證伺服器SSL憑證。\n仍要繼續嗎?");
            builder.setPositiveButton("繼續", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });

            final AlertDialog dialog = builder.create();
            dialog.show();
        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_web_view, container, false);
        initFacebook();
        mWebView = (WebView) rootView.findViewById(R.id.webViewBrowser);
        progressBar = (ProgressBar) rootView.findViewById(R.id.pbBrowserWait);
        TextView txtMessage = (TextView) rootView.findViewById(R.id.txtBrowserMessage);
        //progressBar.setVisibility(View.GONE);
        txtMessage.setVisibility(View.GONE);
        mWebView.setVisibility(View.VISIBLE);

        //WebSettings settings = mWebView.getSettings();
        //settings.setJavaScriptEnabled(true);

        WebViewUtil.configWebView(mWebView);

        if (savedInstanceState != null) {
            mWebView.restoreState(savedInstanceState);
        }

        mWebView.setWebViewClient(new WebViewUploadFragment.MednetWebViewClient());

        mWebView.setWebChromeClient(new WebChromeClient() {
            private void openFileChooser(String type) {
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType(type);
                startActivityForResult(Intent.createChooser(i, "請選擇檔案"),
                        RESULT_CODE_ICE_CREAM);
            }

            private void onShowFileChooser(Intent cameraIntent) {
                Intent selectionIntent = new Intent(Intent.ACTION_PICK, null);
                selectionIntent.setType("image/*");
                //------------------------------------


                Intent[] intentArray;
                if (cameraIntent != null) {
                    intentArray = new Intent[]{cameraIntent};
                } else {
                    intentArray = new Intent[0];
                }

                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "請選擇檔案");
                chooserIntent.putExtra(Intent.EXTRA_INTENT, selectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

                startActivityForResult(chooserIntent, REQUEST_CODE_LOLIPOP);

            }


            //The undocumented magic method override
            //Eclipse will swear at you if you try to put @Override here
            // For Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                openFileChooser("image/*");
            }

            // For Android 3.0+
            public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                openFileChooser("*/*");
            }

            //For Android 4.1
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                openFileChooser("image/*");
            }

            //For Android5.0+
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (mFilePathCallback != null) {
                    mFilePathCallback.onReceiveValue(null);
                }
                mFilePathCallback = filePathCallback;

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    } else {
                        takePictureIntent = null;
                    }
                }
                onShowFileChooser(takePictureIntent);
                return true;
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        // Load the local index.html file
        if (mWebView.getUrl() == null) {
            mWebView.loadUrl(strUrl);
        }
        mWebView.addJavascriptInterface(new WebViewJavaScriptInterface(mContext), "MedicrowdAppWebView");
        return rootView;
    }

    /*
     * JavaScript Interface. Web code can access methods in here
     * (as long as they have the @JavascriptInterface annotation)
     */
    public class WebViewJavaScriptInterface {

        private Context context;

        /*
         * Need a reference to the context in order to sent a post message
         */
        public WebViewJavaScriptInterface(Context context) {
            this.context = context;
        }

        /*
         * This method can be called from Android. @JavascriptInterface
         * required after SDK version 17.
         */
        @JavascriptInterface
        public void notifyApp(String message) {
            //Log.i("debug", "notifyApp: " + message);
            try {
                //Log.i("debug", "notifyApp message = " + message);
                JSONObject noyifyObj = new JSONObject(message);
                String value = title + "\n" + noyifyObj.optString("value");
                switch (noyifyObj.optString("key")) {
                    case "line share": {
                        StringBuilder urlStr = new StringBuilder("line://msg/");
                        urlStr.append("text/");
                        urlStr.append(URLEncoder.encode(value));
                        Uri uri = Uri.parse(urlStr.toString());
                        mContext.startActivity(new Intent(Intent.ACTION_VIEW, uri));
                        break;
                    }
                    case "facebook share": {
                        shareToFacebook(value);
                        break;
                    }
                    case "mainpage": {
                        Log.i(TAG, "notifyApp: 回首頁");
                        ((MainActivity) getActivity()).triggerAction(MegaApplication.ActionTypeMainPage, "");
                        break;
                    }
                    case "webback": {
                        Log.i(TAG, "notifyApp: 回上一頁");
                        goBack();
                        break;
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            //CommonFunc.showToast(this.context, message);
        }

    }

    /**
     * More info this method can be found at
     * http://developer.android.com/training/camera/photobasics.html
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        return imageFile;
    }

    private void clearCache() {
        Log.d("debug", "clearCache");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            //Log.d("debug", "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            Log.d("debug", "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(getActivity());
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetLoginTask:
                if (result == null || result.isNull("source") || result.optJSONObject("source").isNull("access_token")) {
                    return;
                }
                JSONObject source = result.optJSONObject("source");
                String accessToken = source.optString("access_token");
                String refreshToken = source.optString("refresh_token");
                //登入成功
                if (accessToken != null && accessToken.length() > 0) {
                    try {
                        ((MainActivity) getActivity()).getProfile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_CODE_ICE_CREAM:
                Uri uri = null;
                if (data != null) {
                    uri = data.getData();
                }
                mUploadMessage.onReceiveValue(uri);
                mUploadMessage = null;
                break;
            case REQUEST_CODE_LOLIPOP:
                Uri[] results = null;
                // Check that the response is a good one
                if (resultCode == Activity.RESULT_OK) {
                    if (data == null) {
                        // If there is not data, then we may have taken a photo
                        if (mCameraPhotoPath != null) {
                            Log.d("AppChooserFragment", mCameraPhotoPath);

                            results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                        }
                    } else {
                        String dataString = data.getDataString();
                        if (dataString != null) {
                            results = new Uri[]{Uri.parse(dataString)};
                        }
                    }
                }

                mFilePathCallback.onReceiveValue(results);
                mFilePathCallback = null;
                break;
        }
    }
}
