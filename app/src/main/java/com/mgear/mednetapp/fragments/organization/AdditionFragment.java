package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.AdditionPagerAdapter;
import com.mgear.mednetapp.views.CustomViewPager;

/**
 * Created by Jye on 2017/6/13.
 * class: AdditionFragment
 */
public class AdditionFragment extends Fragment {

    private View rootView;
    private CustomViewPager mPager;
    private AdditionPagerAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_addition, container, false);
            TabLayout mTab = (TabLayout) rootView.findViewById(R.id.tabAddition);
            mPager = (CustomViewPager) rootView.findViewById(R.id.vpAdditionPager);

            mTab.addTab(mTab.newTab().setText(getString(R.string.title_addition)));
            mTab.addTab(mTab.newTab().setText(getString(R.string.title_status)));
            mTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    int pos = tab.getPosition();
                    if (pos == 1)
                        ((AdditionStatusFragment) mAdapter.getItem(1)).refresh();
                    mPager.setCurrentItem(pos);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            // Create the adapter that will return a fragment for each of the
            // primary sections of the activity.
            mAdapter = new AdditionPagerAdapter(getChildFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mPager.setAdapter(mAdapter);
            mPager.setPagingEnabled(false);
            mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTab));
        }
        return rootView;
    }

}