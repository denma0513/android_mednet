package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/6/9.
 * class: TutorialTestsFragment
 */
public class TutorialTestsFragment extends Fragment implements Runnable {

    private int nIndex;
    private View rootView;
    private ImageView imgAdd;
    private ImageView imgCheck1;
    private ImageView imgCheck2;
    private ImageView imgSend;
    private Handler mHandler;

    public void startPagerAnimation() {
        nIndex = 0;
        imgAdd.setVisibility(View.INVISIBLE);
        imgCheck1.setVisibility(View.INVISIBLE);
        imgCheck2.setVisibility(View.INVISIBLE);
        imgSend.setVisibility(View.INVISIBLE);
        mHandler.postDelayed(this, 1000);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tutorial_tests, container, false);
            mHandler = new Handler(Looper.getMainLooper());
            RelativeLayout root = (RelativeLayout) rootView.findViewById(R.id.rootTutorialTests);
            root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_tests_bg)));
            imgAdd = (ImageView) rootView.findViewById(R.id.imgTutorialTestsAddBg);
            imgCheck1 = (ImageView) rootView.findViewById(R.id.imgTutorialTestsCheck1);
            imgCheck2 = (ImageView) rootView.findViewById(R.id.imgTutorialTestsCheck2);
            imgSend = (ImageView) rootView.findViewById(R.id.imgTutorialTestsSend);
            imgAdd.setImageBitmap(CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_add_bg));
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacks(this);
        imgSend.clearAnimation();
    }

    @Override
    public void run() {
        switch (nIndex++) {
            case 0:
                imgAdd.setVisibility(View.VISIBLE);
                mHandler.postDelayed(this, 500);
                break;
            case 1:
                imgCheck1.setVisibility(View.VISIBLE);
                mHandler.postDelayed(this, 500);
                break;
            case 2:
                imgCheck2.setVisibility(View.VISIBLE);
                mHandler.postDelayed(this, 500);
                break;
            case 3:
                imgSend.setVisibility(View.VISIBLE);
                ScaleAnimation animScaleUp = new ScaleAnimation(1.0f, 1.25f, 1.0f, 1.25f, 1, 0.5f, 1, 0.5f);
                animScaleUp.setDuration(250);
                animScaleUp.setInterpolator(new LinearInterpolator());
                animScaleUp.setRepeatCount(1);
                animScaleUp.setRepeatMode(2);
                imgSend.startAnimation(animScaleUp);
                break;
        }
    }

}