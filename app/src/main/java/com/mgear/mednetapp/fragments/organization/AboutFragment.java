package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.SubListAdapter;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/8/8.
 * class: AboutFragment
 */
public class AboutFragment extends Fragment {

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("about", CommonFunc.getNowSeconds());
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_about, container, false);
            StringStringSet[] content = new StringStringSet[2];
            content[0] = new StringStringSet(getString(R.string.field_app_design), getString(R.string.app_designer));
            content[1] = new StringStringSet(getString(R.string.field_app_version), CultureSelectActivity.AppVersion);
            SubListAdapter adapter = new SubListAdapter(getActivity(), content);
            ListView lst = (ListView) rootView.findViewById(R.id.lstAbout);
            lst.setAdapter(adapter);
        }
        return rootView;
    }

}