package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.SendCeoMailboxTask;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/8/3.
 * class: MailboxFragment
 */
public class MailboxFragment extends Fragment implements View.OnClickListener, TaskPostExecuteImpl {

    private static boolean finish;
    private static String subject = "";
    private static String content = "";
    public static void init() {
        finish = false;
        subject = "";
        content = "";
    }

    private View rootView;
    private EditText txtSubject;
    private EditText txtContent;
    private Button btnSubmit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("ceo_mailbox", CommonFunc.getNowSeconds());
        if (rootView == null) {
            OrgMainActivity mainActivity = (OrgMainActivity) getActivity();
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_mailbox, container, false);
            btnSubmit = (Button) rootView.findViewById(R.id.btnMailboxSubmit);
            btnSubmit.setOnClickListener(this);
            txtSubject = (EditText) rootView.findViewById(R.id.txtMailboxTitle);
            txtSubject.setOnKeyListener(mainActivity);
            txtContent = (EditText) rootView.findViewById(R.id.txtMailboxContent);
            txtContent.setOnKeyListener(mainActivity);
        }
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        txtSubject.setText(subject);
        txtContent.setText(content);
        if (finish) {
            txtSubject.setEnabled(false);
            txtSubject.setFocusable(false);
            txtContent.setEnabled(false);
            txtContent.setFocusable(false);
            btnSubmit.setVisibility(View.INVISIBLE);
            Snackbar.make(rootView, R.string.msg_mailbox_finish, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!finish) {
            subject = txtSubject.getText().toString();
            content = txtContent.getText().toString();
        }
    }

    @Override
    public void onClick(View v) {
        //驗證資料
        String subject = txtSubject.getText().toString();
        String content = txtContent.getText().toString();
        if (subject.equals("") || content.equals("")) {
            Snackbar.make(rootView, R.string.msg_mailbox_verify, Snackbar.LENGTH_LONG).show();
            return;
        }
        //送出信件
        new SendCeoMailboxTask(getActivity(), this, true, getString(R.string.msg_send_mail))
                .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()),
                        subject, content);
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        if (type == TaskEnum.SendCeoMailboxTask) {
            btnSubmit.setVisibility(View.INVISIBLE);
            subject = txtSubject.getText().toString();
            content = txtContent.getText().toString();
            finish = true;
            Snackbar.make(rootView, R.string.msg_mailbox_finish, Snackbar.LENGTH_LONG).show();
            //切換回地圖頁面
            ((OrgMainActivity) getActivity()).viewMap();
        }
    }

}