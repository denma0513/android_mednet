package com.mgear.mednetapp.fragments.organization;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.ExpandableSubListAdapter;
import com.mgear.mednetapp.entity.organization.CheckableItemSet;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.interfaces.organization.AdditionCheckableImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.CustomerAdditionDataTask;
import com.mgear.mednetapp.utils.CommonFunc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jye on 2017/6/14.
 * class: AdditionStatusFragment
 */
public class AdditionStatusFragment extends BaseFragment implements View.OnClickListener, ExpandableListView.OnItemLongClickListener, TaskPostExecuteImpl {

    private boolean isPrepared;

    private View rootView;
    private View btnRefresh;
    private TextView txtHelper;
    private ExpandableListView expandableListView;
    private AdditionCheckableImpl additionListener;

    private List<String> expandableListTitle;
    private HashMap<String, List<StringStringSet>> expandableListDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            Activity activity = getActivity();
            if (activity instanceof AdditionCheckableImpl)
                additionListener = (AdditionCheckableImpl) activity;
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_addition_status, container, false);
            txtHelper = (TextView) rootView.findViewById(R.id.txtAdditionStatusHelper);
            expandableListView = (ExpandableListView) rootView.findViewById(R.id.elvAdditionStatusItem);
            expandableListView.setOnItemLongClickListener(this);
            btnRefresh = rootView.findViewById(R.id.btnAdditionStatusRefresh);
            btnRefresh.setOnClickListener(this);
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    public void refresh() {
        new CustomerAdditionDataTask(getActivity(), this, false, null).execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
    }

    public void doRefresh() {
        if (additionListener != null) {
            expandableListDetail = additionListener.getCustomerAddition();
            if (expandableListDetail.size() > 0) {
                expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
                ExpandableSubListAdapter expandableListAdapter = new ExpandableSubListAdapter(getActivity(), expandableListTitle, expandableListDetail);
                expandableListView.setAdapter(expandableListAdapter);
                txtHelper.setVisibility(View.GONE);
                expandableListView.setVisibility(View.VISIBLE);
                btnRefresh.setVisibility(View.VISIBLE);
            }
            else {
                txtHelper.setVisibility(View.VISIBLE);
                expandableListView.setVisibility(View.GONE);
                btnRefresh.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) { refresh(); }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        long packedPosition = expandableListView.getExpandableListPosition(position);
        int itemType = ExpandableListView.getPackedPositionType(packedPosition);
        int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
        int childPosition = ExpandableListView.getPackedPositionChild(packedPosition);
        if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            StringStringSet set = this.expandableListDetail.get(this.expandableListTitle.get(groupPosition)).get(childPosition);
            new AlertDialog.Builder(getActivity()).setCancelable(false)
                    .setTitle(set.getMajor())
                    .setMessage(set.getMemo())
                    .setPositiveButton(R.string.button_confirm, null).show();
            return true;
        }
        return false;
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("addition_status", CommonFunc.getNowSeconds());
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        CheckableItemSet cis = (CheckableItemSet) result;
        //判斷是否已有加選項目
        if (cis.size() > 0)
            CustomerActivity.setCustomerAddition(cis);
        doRefresh();
    }
}