package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgear.mednetapp.GoogleFitActivity;
import com.mgear.mednetapp.HealthPushSettingActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.HealthListViewAdapter;
import com.mgear.mednetapp.enums.RefreshViewEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.OnRefreshListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.RefreshHeaderRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;


/**
 * Created by Dennis
 * class: HomePageFragment
 */
public class HealthListFragment extends MegaBaseFragment implements TaskPost2ExecuteImpl, View.OnClickListener {
    //靜態
    private static String TAG = "HealthList";
    private static final int FINISH = 1;

    //畫面參數
    private View rootView;
    private ImageView mSetting, mBanner;
    private RefreshHeaderRecyclerView mRecyclerView;
    private HealthListViewAdapter mAdapter;
    private JSONArray healthList = new JSONArray();
    private TextView mCustomerName, mGreet;

    public JSONArray getHealthList() {
        return healthList;
    }

    private HashMap<String, String> typeMap = new HashMap<String, String>();

    //邏輯參數
    private String[] healthTypeList = {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"
    };

    private int[] healthRecordIconList = {
            R.drawable.ic_record_blood_suger,
            R.drawable.ic_record_blood_pressure,
            R.drawable.ic_record_blood_o2,
            R.drawable.ic_record_drink,
            R.drawable.ic_record_sleep,
            R.drawable.ic_record_step,
            R.drawable.ic_record_weight
    };

    private int[] healthImgList = {
            R.drawable.ic_blood_sugar,
            R.drawable.ic_blood_pleasure,
            R.drawable.ic_o2,
            R.drawable.ic_drink_water,
            R.drawable.ic_sleep,
            R.drawable.ic_walk,
            R.drawable.ic_weight
    };
    private String[] healthNameList = {
            "血糖",
            "血壓",
            "血氧",
            "飲水量",
            "睡眠時間",
            "每日步數",
            "體位"
    };

    String helloTitle = "早安";
    String helloContent = "早安，你有睡足8小時嗎?";
    String[] morningHello = new String[]{
            "早安，你有睡足8小時嗎?",
            "先喝一杯溫開水，喚醒頭腦！",
            "早餐記得要吃得好，充分攝取足夠蛋白質喔！"
    };
    String[] morningHello2 = new String[]{
            "小口小口地喝足量的溫水，增強消化力、增加代謝！",
            "坐太久囉，該起來走動，舒展一下筋骨吧！",
            "盯著電腦太久嗎? 記得30分鐘休息至少5分鐘喔！"
    };

    String[] afternoonHello = new String[]{
            "午餐時間到了，糙米飯取代白米飯，並搭配低油脂、高蛋白儲備工作能量喔！",
            "午休時間趁機休息15-20分鐘，讓工作思緒更清晰！",
            "坐太久囉，該起來伸伸懶腰，動一動囉",
            "工作太忙了，忘記喝水嗎? 要養成喝水的好習慣喔！",
            "盯著電腦太久嗎? 記得30分鐘休息至少5分鐘喔！",
            "下午茶時光，可補充堅果、無糖豆漿、烤地瓜、茶葉蛋喔！"
    };

    String[] nightHello = new String[]{
            "準備要吃晚餐囉，避免高油、高糖、高鹽與加工食品！",
            "晚餐建議選擇蔬菜、有飽足感的根莖類 (如地瓜、芋頭)、低GI食物，會更健康喔！",
    };
    String[] nightHello2 = new String[]{
            "睡前四小時不要進食，避免消化不良喔！",
            "晚安，準備要上床睡覺囉！不要再滑手機囉！"
    };


    private int[] viewIdList;
    private boolean hadIntercept;
    private boolean isPrepared;
    private String startTime;
    private String endTime;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        typeMap.put("1", "blood_sugar");
        typeMap.put("2", "blood_pressure");
        typeMap.put("3", "blood_oxygen");
        typeMap.put("4", "drinking");
        typeMap.put("5", "sleeping");
        typeMap.put("6", "step");
        typeMap.put("7", "body_mass_weight");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        cal.setTime(new Date());
        endTime = sdf.format(cal.getTime());
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        //cal.add(Calendar.DAY_OF_YEAR, -1);
        startTime = sdf.format(cal.getTime());
    }

//    private void checkTime() {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(new Date());
//        int hour = cal.get(Calendar.HOUR_OF_DAY);
//        Random random = new Random();
//        int index = 0;
//        if (hour >= 6 && hour < 10) {
//            index = random.nextInt(morningHello.length);
//            helloContent = morningHello[index];
//            helloTitle = "早安";
//        } else if (hour >= 10 && hour < 12) {
//            index = random.nextInt(morningHello2.length);
//            helloContent = morningHello2[index];
//            helloTitle = "早安";
//        } else if (hour >= 12 && hour < 18) {
//            index = random.nextInt(afternoonHello.length);
//            helloContent = afternoonHello[index];
//            helloTitle = "午安";
//        } else if (hour >= 18 && hour < 22) {
//            index = random.nextInt(nightHello.length);
//            helloContent = nightHello[index];
//            helloTitle = "晚安";
//        } else {
//            index = random.nextInt(nightHello2.length);
//            helloContent = nightHello2[index];
//            helloTitle = "晚安";
//        }
//    }

    private void setView() {
        //checkTime();
        if (!CommonFunc.isBlank(MegaApplication.getInstance().getMember().getFirstName())) {
            mCustomerName.setText(MegaApplication.getInstance().getMember().getFirstName() + " " + MegaApplication.helloTitle);
        } else if (!CommonFunc.isBlank(MegaApplication.getInstance().getMember().getName())) {
            mCustomerName.setText(MegaApplication.getInstance().getMember().getName() + " " + MegaApplication.helloTitle);
        } else {
            mCustomerName.setText("健康新手 " + MegaApplication.helloTitle);
        }
        //mCustomerName.setText(helloTitle + " " + MegaApplication.getInstance().getMember().getName());
        mGreet.setText(MegaApplication.helloContent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            try {
                rootView = inflater.inflate(R.layout.fragment_healthlist_view, container, false);
                mSetting = rootView.findViewById(R.id.health_setting);
                mBanner = rootView.findViewById(R.id.health_banner);
                mRecyclerView = rootView.findViewById(R.id.health_list_view);
                mCustomerName = rootView.findViewById(R.id.health_customer_name);
                mGreet = rootView.findViewById(R.id.health_greet);
                int layoutSWidth = getStageWidth();

                //16:9
                double height = Math.round(layoutSWidth * .5625f);
                mBanner.getLayoutParams().height = (int) height;

                double greatWidth = Math.round(layoutSWidth * .5f);
                mGreet.getLayoutParams().width = (int) greatWidth;

                mSetting.setOnClickListener(this);

                //searchBar.requestFocus();
                mAdapter = new HealthListViewAdapter(getActivity());
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                mRecyclerView.setAdapter(mAdapter, RefreshViewEnum.healthListRefreshView);
                mRecyclerView.setOnRefreshListener(new OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshData();
                    }
                });
//                DividerItemDecoration divider = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
//                divider.setDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.custom_divider));
//                mRecyclerView.addItemDecoration(divider);

                CommonFunc.getHelloWord();
                setView();

                new CustomerApiTask(getActivity(), this, true, getString(R.string.msg_data_loading)
                        , TaskEnum.MednetQueryLastMeasureValue, false).executeOnExecutor(MegaApplication.threadPoolExecutor);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        //lazyLoad();
        return rootView;
    }

    /**
     * 初始化畫面資訊
     */
    private void initViewdate() {
        healthList = new JSONArray();
        for (int i = 0; i < 7; i++)
            healthList.put(new JSONObject());
    }

    /**
     * 重整畫面資訊
     */
    private void reloadViewdate() {
        Log.i(TAG, "healthList = " + healthList + "");
        mAdapter.setAdapterList(healthList);
        mAdapter.notifyDataSetChanged();
    }


    /**
     * 更新資料
     */
    public void refreshData() {
        Log.i("debug", "更新資料requestData ");
        new CustomerApiTask(getActivity(), this, true, getString(R.string.msg_data_loading)
                , TaskEnum.MednetQueryLastMeasureValue, true).executeOnExecutor(MegaApplication.threadPoolExecutor);
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler sHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == FINISH) {
                //Toast.makeText(getActivity(), "更新完成！", Toast.LENGTH_SHORT).show();
                mRecyclerView.refreshComplete();
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.health_setting:
                Intent intent = new Intent(getActivity(), HealthPushSettingActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        Message message = Message.obtain();
        message.what = FINISH;
        sHandler.sendMessage(message);
        try {
            switch (type) {
                case MednetQueryLastMeasureValue:
                    healthList = new JSONArray();
                    int i = 0;
                    boolean checkNew = false;
                    for (String healthType : MegaApplication.healthSortList) {
                        JSONObject item = new JSONObject();
                        JSONObject healthMap = MegaApplication.healthMap.get(healthType);
                        String typeName = healthMap.optString("healthTypeName");
                        if (result != null) {

                            if (result != null && !result.isNull("source") && !result.optJSONObject("source").isNull(typeMap.get(healthType))) {
                                item = result.optJSONObject("source").optJSONObject(typeMap.get(healthType));

                            } else {
                                item = new JSONObject();
                            }

                            /**######################################################################*/

                            //特別需要給初始資料，或是透過google fit新增資料的
                            String mTime = item.optString("m_time");
                            if (!"".equals(mTime)) {
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(CommonFunc.UTC2Date(mTime));
                                Calendar now = Calendar.getInstance();

                                if ("1".equals(healthType)) {
                                    if (item.optDouble("ac") > 0 && item.optDouble("pc") > 0) {
                                        //飯前飯後都有職 要去抓最後一筆
                                        checkNew = true;

                                        if (MegaApplication.healthList != null && MegaApplication.healthList.length() == 7)
                                            item = MegaApplication.healthList.optJSONObject(i);
                                        else
                                            item = new JSONObject();

                                        Calendar cal2 = Calendar.getInstance();
                                        cal2.setTime(new Date());
                                        int maxDay = cal2.getActualMaximum(Calendar.DAY_OF_MONTH);
                                        String startTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "T00:00:00Z";
                                        String endTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "T23:59:59Z";
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                                                , TaskEnum.MednetQueryMeasureValuesType1, true).executeOnExecutor(MegaApplication.threadPoolExecutor, startTime, endTime);
                                    }
                                } else if ("4".equals(healthType)) {
                                    if (now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
//                                        //取今天是今天的飲水量
                                        MegaApplication.TodayWaterCapacity = item.optInt("drinking_water_today");
                                    } else {
                                        MegaApplication.TodayWaterCapacity = 0;
                                    }


                                } else if ("5".equals(healthType)) {
                                    if (item.optDouble("sleeping_hours") > 0.0 && now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
                                    } else {
                                        if (MegaApplication.TodaySleepHour > 0) {
                                            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                                    , TaskEnum.MednetSleepingCreateOrUpdate, true).executeOnExecutor(MegaApplication.threadPoolExecutor, MegaApplication.TodaySleepTime,
                                                    MegaApplication.TodayWakeUpTime, Double.toString(MegaApplication.TodaySleepHour), CommonFunc.getNowUTCDateString());
                                        }
                                    }
                                } else if ("6".equals(healthType)) {
                                    Calendar cal2 = Calendar.getInstance();
                                    int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

                                    cal2.setTime(new Date());
                                    if (item.optInt("step_count") > 0 && now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
                                        //今天的資料
                                        if (item.optInt("step_count") != MegaApplication.TodayStepCount && MegaApplication.TodayStepCount != 0) {

                                            //先撈今天資料
                                            String startTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "T00:00:00Z";
                                            String endTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "T23:59:59Z";
                                            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                                                    , TaskEnum.MednetQueryMeasureValuesType6, true).executeOnExecutor(MegaApplication.threadPoolExecutor, startTime, endTime);

                                        }
                                    } else {
                                        //Log.i("debug", "直接新增");
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                                , TaskEnum.MednetCreateStepCreateOrUpdate, true)
                                                .executeOnExecutor(MegaApplication.threadPoolExecutor, Integer.toString(MegaApplication.TodayStepCount), Double.toString(MegaApplication.TodayDistance),
                                                        Double.toString(MegaApplication.TodayCalories),
                                                        CommonFunc.getNowUTCDateStringOnlyDate());
                                    }
                                }
                            } else {
                                //沒有資料
                                if ("5".equals(healthType)) {
                                    if (!"".equals(MegaApplication.TodaySleepTime)) {
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                                , TaskEnum.MednetSleepingCreateOrUpdate, true).executeOnExecutor(MegaApplication.threadPoolExecutor, MegaApplication.TodaySleepTime,
                                                MegaApplication.TodayWakeUpTime, Double.toString(MegaApplication.TodaySleepHour), CommonFunc.getNowUTCDateString());
                                    }

                                } else if ("6".equals(healthType)) {
                                    Log.i("debug", "healthType 步行");
                                    if (MegaApplication.TodayStepCount > 0) {
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                                , TaskEnum.MednetCreateStepCreateOrUpdate, true)
                                                .executeOnExecutor(MegaApplication.threadPoolExecutor, Integer.toString(MegaApplication.TodayStepCount), Double.toString(MegaApplication.TodayDistance),
                                                        Double.toString(MegaApplication.TodayCalories),
                                                        CommonFunc.getNowUTCDateStringOnlyDate());
                                    }
                                }
                            }
                            /**######################################################################*/

                            if (!"".equals(item.optString("bh"))) {
                                MegaApplication.getInstance().BodyHeight = item.optString("bh");
                            }

                            item.put("healthTypeName", healthMap.optString("healthName"));
                            item.put("healthType", healthType);
                            item.put("healthTypeKey", typeName);
                            item.put("healthImg", healthMap.optString("healthImg"));
                            item.put("healthIcon", healthMap.optString("healthRecordIcon"));
                            item.put("index", i);
                            //Log.i("debug", "item  = " + item);
                        }
                        healthList.put(item);
                        i++;
                    }
//                    if (!checkNew)
                    MegaApplication.healthList = healthList;
                    reloadViewdate();
                    break;
                case MednetQueryMeasureValuesType1:
                    Log.i("debug", "MednetQueryMeasureValuesType1 = " + result);
                    if (result != null && !result.isNull("source")) {
                        JSONArray source = result.optJSONArray("source");
                        JSONObject finObj = new JSONObject();
                        if (source.length() >= 2) {
                            JSONObject obj = source.optJSONObject(0);
                            JSONObject obj2 = source.optJSONObject(1);
                            Calendar cal = Calendar.getInstance();
                            Calendar cal2 = Calendar.getInstance();
                            cal.setTime(CommonFunc.UTC2Date(obj.optString("m_time")));
                            cal2.setTime(CommonFunc.UTC2Date(obj2.optString("m_time")));
                            if (cal.compareTo(cal2) == 1) {
                                finObj = obj;
                            } else {
                                finObj = obj2;
                            }
                        } else if (source.length() == 1) {
                            finObj = source.optJSONObject(0);
                        }

                        for (i = 0; i < healthList.length(); i++) {
                            JSONObject obj = healthList.optJSONObject(i);
                            if ("1".equals(obj.optString("healthType"))) {
                                JSONObject healthMap = MegaApplication.healthMap.get("1");
                                String typeName = healthMap.optString("healthTypeName");
                                obj = finObj;
                                obj.put("healthTypeName", healthMap.optString("healthName"));
                                obj.put("healthType", "1");
                                obj.put("healthTypeKey", typeName);
                                obj.put("healthImg", healthMap.optString("healthImg"));
                                obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                                obj.put("index", i);
                                healthList.put(i, obj);
                            }
                        }
                        MegaApplication.healthList = healthList;
                        reloadViewdate();
                    }
                    break;
                case MednetQueryMeasureValuesType4:
                    if (result != null && !result.isNull("source")) {
                        //Log.i("debug", "result  = " + result);
                        JSONArray source = result.optJSONArray("source");

                        Calendar calTemp = Calendar.getInstance();
                        JSONObject finObj = null;
                        for (i = 0; i < source.length(); i++) {
                            try {
                                String updateDate = source.optJSONObject(i).optString("updated_at");
                                if (i == 0) {
                                    calTemp.setTime(CommonFunc.UTC2DateE04(updateDate));
                                    finObj = source.optJSONObject(i);
                                    continue;
                                }
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(CommonFunc.UTC2DateE04(updateDate));
                                if (cal.compareTo(calTemp) == 1) {
                                    calTemp.setTime(CommonFunc.UTC2Date(updateDate));
                                    finObj = source.optJSONObject(i);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        if (finObj == null)
                            break;


                        MegaApplication.TodayWaterCapacity = finObj.optInt("drinking_water_today");
                        Log.i("debug", "MegaApplication.TodayWaterCapacity = " + MegaApplication.TodayWaterCapacity);
                        for (i = 0; i < healthList.length(); i++) {
                            JSONObject obj = healthList.optJSONObject(i);
                            if ("4".equals(obj.optString("healthType"))) {
                                JSONObject healthMap = MegaApplication.healthMap.get("4");
                                String typeName = healthMap.optString("healthTypeName");
                                obj = finObj;
                                obj.put("healthTypeName", healthMap.optString("healthName"));
                                obj.put("healthType", "4");
                                obj.put("healthTypeKey", typeName);
                                obj.put("healthImg", healthMap.optString("healthImg"));
                                obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                                obj.put("index", i);
                                healthList.put(i, obj);
                                Log.i("debug", "healthList = '" + healthList);
                            }
                        }
                        MegaApplication.healthList = healthList;
                        reloadViewdate();
                    }
                    break;
                case MednetQueryMeasureValuesType6:
                    if (result != null && !result.isNull("source")) {
                        JSONArray source = result.optJSONArray("source");

                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                , TaskEnum.MednetCreateStepCreateOrUpdate, true)
                                .executeOnExecutor(MegaApplication.threadPoolExecutor, Integer.toString(MegaApplication.TodayStepCount), Double.toString(MegaApplication.TodayDistance),
                                        Double.toString(MegaApplication.TodayCalories),
                                        CommonFunc.getNowUTCDateStringOnlyDate(), source.optJSONObject(0).optInt("id") + "",
                                        source.optJSONObject(0).optString("measure_id"));
                    }
                    break;
                case MednetCreateStepCreateOrUpdate:
                    if (result != null && !result.isNull("source")) {
                        JSONObject source = result.optJSONObject("source");
                        for (i = 0; i < healthList.length(); i++) {
                            JSONObject obj = healthList.optJSONObject(i);
                            if ("6".equals(obj.optString("healthType"))) {
                                JSONObject item = new JSONObject();
                                JSONObject healthMap = MegaApplication.healthMap.get("6");
                                String typeName = healthMap.optString("healthTypeName");
                                obj = source;
                                obj.put("healthTypeName", healthMap.optString("healthName"));
                                obj.put("healthType", "6");
                                obj.put("healthTypeKey", typeName);
                                obj.put("healthImg", healthMap.optString("healthImg"));
                                obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                                obj.put("index", i);
                                healthList.put(i, obj);
                            }

                        }
                        MegaApplication.healthList = healthList;
                        reloadViewdate();
                    }
                    break;
                case MednetSleepingCreateOrUpdate:
                    if (result != null && !result.isNull("source")) {
                        JSONObject source = result.optJSONObject("source");
                        for (i = 0; i < healthList.length(); i++) {
                            JSONObject obj = healthList.optJSONObject(i);
                            if ("5".equals(obj.optString("healthType"))) {
                                JSONObject item = new JSONObject();
                                JSONObject healthMap = MegaApplication.healthMap.get("5");
                                String typeName = healthMap.optString("healthTypeName");
                                obj = source;
                                obj.put("healthTypeName", healthMap.optString("healthName"));
                                obj.put("healthType", "5");
                                obj.put("healthTypeKey", typeName);
                                obj.put("healthImg", healthMap.optString("healthImg"));
                                obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                                obj.put("index", i);
                                healthList.put(i, obj);
                            }

                        }
                        MegaApplication.healthList = healthList;
                        reloadViewdate();
                    }
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onBackPressed() {
        return false;
//        if (hadIntercept) {
//            return false;
//        } else {
//            hadIntercept = true;
//            return true;
//        }
    }

    public int[] getHealthImgList() {
        return healthImgList;
    }
}