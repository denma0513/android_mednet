package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.HomePagerAdapter;
import com.mgear.mednetapp.interfaces.organization.PreviousPageImpl;
import com.mgear.mednetapp.entity.organization.StringIntegerSet;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.views.CustomViewPager;

import java.util.Stack;

/**
 * Created by Jye on 2017/6/13.
 * class: HomeFragment
 */
public class HomeFragment extends Fragment implements AdapterView.OnItemClickListener, PreviousPageImpl {

    private String strTitle;
    private View rootView;
    private CustomViewPager mPager;
    private HomePagerAdapter mAdapter;

    private int pagePosition;
    private String[] aryItem = null;
    private int[] aryColor = null;
    private int[] aryImage = null;

    private Stack<StringIntegerSet> previousPage;

    public String[] getHomeTitleList() { return aryItem; }

    public int[] getHomeColorList() { return aryColor; }

    public int[] getHomeImageList() { return aryImage; }

    public String getStrTitle() {
        return strTitle;
    }

    public CustomViewPager getMPager() {
        return mPager;
    }

    public int getPagePosition() {
        return pagePosition;
    }

    public Stack<StringIntegerSet> getPreviousPage() {
        return previousPage;
    }

    public void navigationPage(String title, int position) {
        //紀錄目前的頁面
        previousPage.push(new StringIntegerSet(strTitle, pagePosition));
        //顯示到新頁面
        strTitle = title;
        pagePosition = position;
        mPager.setCurrentItem(position, false);
        ((OrgMainActivity) getActivity()).setPreviousPage(this);
    }

    public void setReportCheckupContent(StringStringSet[] content) {
        ReportCheckupFragment fragment = (ReportCheckupFragment) mAdapter.getItem(12);
        fragment.setContent(content);
    }

    public void setReportHistoryContent(String content) {
        ReportHistoryFragment fragment = (ReportHistoryFragment) mAdapter.getItem(11);
        fragment.setContent(content, Environment.getExternalStorageDirectory() + "/report.pdf");
    }

    public void setHealthyQuestionContent(StringStringSet[] content) {
        HealthyQuestionFragment fragment = (HealthyQuestionFragment) mAdapter.getItem(9);
        fragment.setContent(content);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            previousPage = new Stack<>();
            // get item information
            aryItem = new String[] {
                    getString(R.string.menu_new),
                    getString(R.string.menu_promotional),
                    getString(R.string.menu_result),
                    getString(R.string.menu_article),
                    getString(R.string.menu_tip),
                    getString(R.string.menu_game),
                    getString(R.string.menu_join),
                    getString(R.string.menu_ilink)
            };
            aryColor = new int[] {
                    Color.parseColor("#39CAE8"),
                    Color.parseColor("#39CAE8"),
                    Color.parseColor("#39CAE8"),
                    Color.parseColor("#39CAE8"),
                    Color.parseColor("#39CAE8"),
                    Color.parseColor("#39CAE8"),
                    Color.parseColor("#39CAE8"),
                    Color.parseColor("#39CAE8")
            };
            aryImage = new int[] {
                    R.drawable.icon_new,
                    R.drawable.icon_promotion_home,
                    R.drawable.icon_result,
                    R.drawable.icon_article,
                    R.drawable.icon_tip,
                    R.drawable.icon_game,
                    R.drawable.icon_join,
                    R.drawable.icon_ilink
            };

            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_home, container, false);

            // Create the adapter that will return a fragment for each of the
            // primary sections of the activity.
            mAdapter = new HomePagerAdapter(getChildFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mPager = (CustomViewPager) rootView.findViewById(R.id.vpHomePager);
            mPager.setOffscreenPageLimit(0);
            mPager.setAdapter(mAdapter);
            mPager.setPagingEnabled(false);
        }

        return rootView;
    }

    public void gotoNewFragment(int position){
        //紀錄目前的頁面
        previousPage.push(new StringIntegerSet(strTitle, pagePosition));
        //顯示到新頁面
        strTitle = aryItem[position];
        pagePosition = position + 1;
        mPager.setCurrentItem(pagePosition, false);
        ((OrgMainActivity) getActivity()).setPreviousPage(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i("HomeFragment", "點擊了item => "+view + position+ id);
        if (aryItem[position].contains(getString(R.string.menu_join)) || aryItem[position].contains(getString(R.string.menu_ilink))) {
            return;
        }
        //紀錄目前的頁面
        previousPage.push(new StringIntegerSet(strTitle, pagePosition));
        //顯示到新頁面
        strTitle = aryItem[position];
        pagePosition = position + 1;
        mPager.setCurrentItem(pagePosition, false);
        ((OrgMainActivity) getActivity()).setPreviousPage(this);
    }

    @Override
    public void previousPage() {
        if (!previousPage.empty()) {
            StringIntegerSet set = previousPage.pop();
            strTitle = set.getMajor();
            pagePosition = set.getMinor();
            mPager.setCurrentItem(pagePosition, false);
        }
    }

    @Override
    public boolean hasPreviousPage() {
        return !previousPage.empty();
    }

    @Override
    public String getNavigationPageTitle() { return strTitle; }

}