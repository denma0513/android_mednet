package com.mgear.mednetapp.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jye on 2017/6/15.
 * class: HomePagerAdapter
 */
public class DrawerMenuAdapter extends BaseExpandableListAdapter implements TaskPost2ExecuteImpl {
    public static int MENUCOUNT = 6;
    private final Context mContext;
    private final INativeActionImpl mINativeAction;

    //private ArrayList<WebViewBaseFragment> menuFragmantList;
    private ArrayList<ArrayList> menuList = new ArrayList<ArrayList>();


    public DrawerMenuAdapter(Context context) {
        mContext = context;
        mINativeAction = (INativeActionImpl) context;
        //載入網頁內容
        for (int i = 0; i < MENUCOUNT; i++) {
            ArrayList<MenuHandle> childList = new ArrayList<MenuHandle>();
            switch (i) {
                case 0:

                    if (!"".equals(MegaApplication.getInstance().getMember().getAccessToken())) {
//                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的健康報告").setTarget("https://med-net.com/PhysicalExamination"));
//                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("每日健康紀錄").setTarget("https://med-net.com/DailyAnalysis"));
//                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的運動紀錄").setTarget("https://med-net.com/CustomerExercise"));
//                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的飲食紀錄").setTarget("https://med-net.com/CustomerDiet"));
                    }

                    break;
                case 1:
                    if ("1".equals(MegaApplication.getInstance().getMember().getAuthType())) {//醫師
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("醫師諮詢首頁").setTarget("https://expert.med-net.com"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的諮詢紀錄").setTarget(String.format("https://expert.med-net.com/doctorsdetail/%s/1", MegaApplication.getInstance().getMember().getDrID())));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的心意紀錄").setTarget("https://med-net.com/MindRecord"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("機構評價紀錄").setTarget("https://med-net.com/ChannelCommentRecord"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的預約紀錄").setTarget("https://med-net.com/Cart?PageId=2"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的收藏文章").setTarget("https://med-net.com/MemberFavorite"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我關注的醫師").setTarget("https://med-net.com/MemberFavorite"));
                    } else {//一般用戶
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("醫師諮詢紀錄").setTarget("https://med-net.com/ConsultationRecord"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("機構評價紀錄").setTarget("https://med-net.com/ChannelCommentRecord"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的心意紀錄").setTarget("https://med-net.com/MindRecord"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的預約紀錄").setTarget("https://med-net.com/Cart?PageId=2"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的收藏紀錄").setTarget("https://med-net.com/MemberFavorite"));
//                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我關注的醫師").setTarget("https://med-net.com/MemberFavorite"));

                    }

                    break;
                case 2:
                    if (!"".equals(MegaApplication.getInstance().getMember().getAccessToken())) {
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("會員資料").setTarget("https://med-net.com/Member"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("密碼修改").setTarget("https://med-net.com/Member/PasswordEdit"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的好友").setTarget("https://med-net.com/Member/Friendsettings"));
                        childList.add(MenuHandle.getInstance().setAction("1").setTitle("健康問卷").setTarget("https://med-net.com/Member/questionnaire"));
                        //childList.add(MenuHandle.getInstance().setAction("1").setTitle("我的點數歷程").setTarget("我的點數歷程"));
                    }

                    break;
                case 3:
                    childList.add(MenuHandle.getInstance().setAction("1").setTitle("健康文章").setTarget("https://med-net.com/CMSContent"));
                    childList.add(MenuHandle.getInstance().setAction("1").setTitle("關於醫聯網").setTarget("https://med-net.com/Themes/Aboutus"));
                    break;
                case 4:
                    childList.add(MenuHandle.getInstance().setAction("1").setTitle("找健診機構").setTarget("https://med-net.com/SelfPayChannel"));
                    childList.add(MenuHandle.getInstance().setAction("1").setTitle("快速找健診").setTarget("https://med-net.com/Product"));

                    break;
                case 5:
                    if (!"".equals(MegaApplication.getInstance().getMember().getAccessToken())) {
                        childList.add(MenuHandle.getInstance().setAction("1002").setTitle("登出").setTarget("https://med-net.com/Member/Logout"));
                    }
                    break;
            }

            menuList.add(childList);
        }
    }

    @Override
    public int getGroupCount() {
        return menuList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return menuList.get(i).size();
    }

    @Override
    public Object getGroup(int i) {
        return menuList.get(i);
    }

    @Override
    public Object getChild(int i, int j) {
        return menuList.get(i).get(j);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int j) {
        return j;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
        TextView textView = new TextView(mContext);
        textView.setBackgroundColor(mContext.getResources().getColor(R.color.home_title_black3));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams
                ((int) RelativeLayout.LayoutParams.MATCH_PARENT, 1);
        textView.setLayoutParams(params);
        if (menuList.get(i).size() <= 0 || i == 0)
            textView.setVisibility(View.GONE);
        return textView;
    }

    @Override
    public View getChildView(int i, int j, boolean b, View convertView, ViewGroup viewGroup) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.cell_child, null);
        TextView childTitle = convertView.findViewById(R.id.child_title);
        MenuHandle item = (MenuHandle) menuList.get(i).get(j);
        childTitle.setText(item.getTitle());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.i("DrawerMenuAdapter", "i = " + i + ", j = " + j + " item = " + item.getTitle());
                mINativeAction.triggerAction(item.action, item.target);

                new CommonApiTask(mContext, DrawerMenuAdapter.this, false, "", TaskEnum.MednetAppLogEven)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "menu_button", item.target);

            }
        });
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {

    }

    static class MenuHandle {
        private String action;
        private String target;
        private String title;

        public static MenuHandle getInstance() {
            return new MenuHandle();
        }


        public String getTarget() {
            return target;
        }

        public MenuHandle setTarget(String target) {
            this.target = target;
            return this;
        }

        public String getAction() {
            return action;
        }

        public MenuHandle setAction(String action) {
            this.action = action;
            return this;
        }

        public String getTitle() {
            return title;
        }

        public MenuHandle setTitle(String title) {
            this.title = title;
            return this;
        }
    }
}