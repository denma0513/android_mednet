package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.mgear.mednetapp.HealthPushDetailActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleFitFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        OnDataPointListener, ResultCallback<DataSourcesResult> {

    private INativeActionImpl mINativeActionImpl;
    //常數
    private static String TAG = "GOOGLE_FIT";

    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 24 * HOUR;
    private View rootView;

    //邏輯元件
    private MegaBaseFragment selectedFragment;
    private static int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 6;
    private GoogleApiClient mClient;

    private static int stepCount = 0;
    private static Double calories = 0.0;
    private static Double distance = 0.0;
    private static Double sleepHour = 0.0;
    private static long sleepTime = 0;
    private static long getUpTime = 0;
    private FitnessOptions fitnessOptions;
    //private static JSONObject stepObj = new JSONObject();
    private Context mContext;
    private boolean isTrigger = false;

    private void accessGoogleFit() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        long endTime = cal.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        //for sleep time
        cal.add(Calendar.DATE, -1);
        cal.set(Calendar.HOUR_OF_DAY, 18);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        //cal.add(Calendar.DAY_OF_YEAR, -1);

        long startTime = cal.getTimeInMillis();

        DateFormat dateFormat = DateFormat.getDateInstance();
        //Log.d("GOOGLE_FIT", "Range Start: " + sdf.format(startTime));
        //Log.d("GOOGLE_FIT", "Range End: " + sdf.format(endTime));


        mClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addScope(new Scope(Scopes.FITNESS_NUTRITION_READ))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        DataSourcesRequest dataSourcesRequest = new DataSourcesRequest.Builder()
//                .setDataTypes(DataType.TYPE_HEART_RATE_BPM),
                .setDataTypes(DataType.TYPE_STEP_COUNT_DELTA)
                .setDataSourceTypes(DataSource.TYPE_DERIVED) // data type, raw or derived?
                .build();

        Fitness.SensorsApi.findDataSources(mClient, dataSourcesRequest).setResultCallback(this);

        DataReadRequest readRequest =
                new DataReadRequest.Builder()
                        .aggregate(DataType.TYPE_ACTIVITY_SEGMENT, DataType.AGGREGATE_ACTIVITY_SUMMARY)
                        .bucketByTime(1, TimeUnit.DAYS)
                        .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                        .build();


        Fitness.getHistoryClient(getActivity(), GoogleSignIn.getLastSignedInAccount(getActivity())).readData(readRequest)
                .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                    @Override
                    public void onSuccess(DataReadResponse dataReadResult) {
                        Log.d("GOOGLE_FIT", "onSuccess()");
                        if (dataReadResult.getBuckets().size() > 0) {
                            //Log.d("GOOGLE_FIT", "Number of returned buckets of DataSets is: " + dataReadResult.getBuckets().size());
                            for (Bucket bucket : dataReadResult.getBuckets()) {
                                List<DataSet> dataSets = bucket.getDataSets();
                                for (DataSet dataSet : dataSets) {
                                    dumpDataSet(dataSet);
                                }
                            }
                        } else if (dataReadResult.getDataSets().size() > 0) {
                            //Log.d("GOOGLE_FIT", "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                            //Log.d("GOOGLE_FIT", "getName = " + dataReadResult.getDataSets().get(0).getDataType().getName());
                            for (DataSet dataSet : dataReadResult.getDataSets()) {
                                dumpDataSet(dataSet);
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("GOOGLE_FIT", "onFailure()", e);
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<DataReadResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<DataReadResponse> task) {
                        Log.d("GOOGLE_FIT", "onComplete()");
                        SimpleDateFormat sim = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                        MegaApplication.TodayStepCount = stepCount;//今天步數
                        MegaApplication.TodayCalories = calories;//今天卡路里
                        MegaApplication.TodayDistance = distance;//今天距離
                        MegaApplication.TodaySleepTime = CommonFunc.getUTCDateString(sim.format(new Date(sleepTime)));//今天就寢時間
                        MegaApplication.TodayWakeUpTime = CommonFunc.getUTCDateString(sim.format(new Date(getUpTime)));//今天起床時間
                        MegaApplication.TodaySleepHour = (Double.valueOf(getUpTime - sleepTime) / Double.valueOf(HOUR));
                        //MegaApplication.TodaySleepHour = sleepHour / HOUR;//今天睡眠時間
                        //Log.d("GOOGLE_FIT", "MegaApplication.TodaySleepHour = " + MegaApplication.TodaySleepHour);

                    }
                });


        //fot step
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        startTime = cal.getTimeInMillis();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);

        DataReadRequest readRequest2 = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .aggregate(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA)
                .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();
        fitnessReadData(readRequest2);

    }


    private void fitnessReadData(DataReadRequest readRequest) {
        Fitness.getHistoryClient(getActivity(), GoogleSignIn.getLastSignedInAccount(getActivity()))
                .readData(readRequest)
                .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                    @Override
                    public void onSuccess(DataReadResponse dataReadResult) {
                        if (dataReadResult.getBuckets().size() > 0) {
                            for (Bucket bucket : dataReadResult.getBuckets()) {
                                List<DataSet> dataSets = bucket.getDataSets();
                                for (DataSet dataSet : dataSets) {
                                    dumpDataSet(dataSet);
                                }
                            }
                        } else if (dataReadResult.getDataSets().size() > 0) {
                            for (DataSet dataSet : dataReadResult.getDataSets()) {
                                dumpDataSet(dataSet);
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("GOOGLE_FIT", "onFailure()", e);
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<DataReadResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<DataReadResponse> task) {
                        Log.d("GOOGLE_FIT", "onComplete()");
                        Log.d("GOOGLE_FIT", "stepObj = " + MegaApplication.FitStepObj);

                        SimpleDateFormat sim = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                        MegaApplication.TodayStepCount = stepCount;//今天步數
                        MegaApplication.TodayCalories = calories;//今天卡路里
                        MegaApplication.TodayDistance = distance;//今天距離
                    }
                });
    }

    // [START parse_dataset]
    private static void dumpDataSet(DataSet dataSet) {
        DateFormat dateFormat = DateFormat.getTimeInstance();
        for (DataPoint dp : dataSet.getDataPoints()) {
            //Log.d("GOOGLE_FIT", "dp = " + dp);
            //Log.d("GOOGLE_FIT", "Data point:");
            //Log.d("GOOGLE_FIT", "\tgetDataType Type: " + dp.getDataType().getName() + " " + dp.getDataType().getName());
            //Log.d("GOOGLE_FIT", "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
            //Log.d("GOOGLE_FIT", "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
            long startDateLong = dp.getTimestamp(TimeUnit.MILLISECONDS);
            String startDate = sdf.format(startDateLong);
            Calendar cal = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            try {
                cal2.setTime(new Date());
                cal.setTime(sdf.parse(startDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String date = sdf2.format(cal.getTimeInMillis());

            for (Field field : dp.getDataType().getFields()) {
                //Log.d("GOOGLE_FIT", "\tfield.getName() Type: " + field.getName());
                if (field.getName().contains("duration")) {
                    SimpleDateFormat sdf111 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    Log.d("GOOGLE_FIT", "\tgetAppPackageName(): " + dp.getOriginalDataSource().getAppPackageName() + " " + field.getName());
//                    Log.d("GOOGLE_FIT", "\tdp = " + dp);
//                    Log.d("GOOGLE_FIT", "\t dp.getValue(field).asInt() = " + dp.getValue(field).asInt());
//                    Log.d("GOOGLE_FIT", "\t dp.getStartTime(TimeUnit.MILLISECONDS) = " + sdf111.format(new Date(dp.getStartTime(TimeUnit.MILLISECONDS))));
//                    Log.d("GOOGLE_FIT", "\t dp.getEndTime(TimeUnit.MILLISECONDS) = " + sdf111.format(new Date(dp.getEndTime(TimeUnit.MILLISECONDS))));
                    if (dp.getOriginalDataSource().getAppPackageName() != null) {
                        if (dp.getOriginalDataSource().getAppPackageName().contains("sleep")
                                || dp.getOriginalDataSource().getAppPackageName().contains("fitness")
                                || dp.getOriginalDataSource().getAppPackageName().contains("healthsync")) {

                            Log.d("GOOGLE_FIT", "\tgetAppPackageName(): " + dp.getOriginalDataSource().getAppPackageName());
                            Log.d("GOOGLE_FIT", "\tgetAppPackageName(): " + dp.getOriginalDataSource().getName());
                            Log.d("GOOGLE_FIT", "\tgetAppPackageName(): " + dp.getOriginalDataSource().getStreamName());
                            Log.d("GOOGLE_FIT", "\tgetAppPackageName(): " + dp.getOriginalDataSource().getDataType());
                            double ms = dp.getValue(field).asInt();
                            StringBuffer text = new StringBuffer("");
                            int hour = 0;
                            int min = 0;
                            if (ms > HOUR) {
                                hour = (int) (ms / HOUR);
                            }
                            if (ms > MINUTE) {
                                min = (int) (ms / MINUTE) % 60;
                            }
                            if (sleepTime == 0) {
                                sleepTime = dp.getStartTime(TimeUnit.MILLISECONDS);
                            } else {
                                if (dp.getStartTime(TimeUnit.MILLISECONDS) < sleepTime)
                                    sleepTime = dp.getStartTime(TimeUnit.MILLISECONDS);
                            }
                            if (dp.getEndTime(TimeUnit.MILLISECONDS) > getUpTime)
                                getUpTime = dp.getEndTime(TimeUnit.MILLISECONDS);

                            Log.d("GOOGLE_FIT", "\tdate: " + date);
                            Log.d(TAG, "就寢時間 ＝" + dp.getStartTime(TimeUnit.MILLISECONDS));
                            Log.d(TAG, "起床時間 ＝" + dp.getEndTime(TimeUnit.MILLISECONDS));
                            Log.d(TAG, "睡眠時間 ＝" + hour + "：" + min);
                            sleepHour += ms;
                        }
                    }
                } else if (dp.getDataType().getName().contains("step")) {
                    if (CommonFunc.isSameDay(cal, cal2)) {
                        //Log.d(TAG, "startDate ＝" + startDate);

                        Log.d(TAG, "步數 ＝" + dp.getValue(field));
                        stepCount = dp.getValue(field).asInt();
                    }
                    JSONObject item = MegaApplication.FitStepObj.optJSONObject(date);
                    if (!MegaApplication.FitStepObj.has(date)) {
                        item = new JSONObject();
                    }
                    try {
                        item.put("stepCount", dp.getValue(field));
                        MegaApplication.FitStepObj.put(date, item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("GOOGLE_FIT", "\tdate: " + date);
                    //Log.d("GOOGLE_FIT", "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                    //Log.d("GOOGLE_FIT", "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                    Log.d(TAG, "步數 ＝" + dp.getValue(field));

                } else if (dp.getDataType().getName().contains("distance")) {
                    Log.d(TAG, "公里數 ＝" + dp.getValue(field));
                    if (CommonFunc.isSameDay(cal, cal2)) {
                        distance = Double.valueOf(dp.getValue(field).asFloat()) / 1000.0;
                    }
                    JSONObject item = MegaApplication.FitStepObj.optJSONObject(date);
                    if (!MegaApplication.FitStepObj.has(date)) {
                        item = new JSONObject();
                    }
                    try {
                        item.put("distance", Double.valueOf(dp.getValue(field).asFloat()) / 1000.0);
                        MegaApplication.FitStepObj.put(date, item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (dp.getDataType().getName().contains("calories")) {
                    Log.d(TAG, "卡路里 ＝" + dp.getValue(field));
                    if (CommonFunc.isSameDay(cal, cal2)) {
                        calories = Double.valueOf(dp.getValue(field).asFloat());
                    }
                    JSONObject item = MegaApplication.FitStepObj.optJSONObject(date);
                    if (!MegaApplication.FitStepObj.has(date)) {
                        item = new JSONObject();
                    }
                    Log.d(TAG, "卡路里 ＝" + calories);
                    try {
                        item.put("calories", dp.getValue(field));
                        MegaApplication.FitStepObj.put(date, item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MegaApplication.TodaySleepHour = 0.0;
        sleepTime = 0;

        mClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Fitness.SENSORS_API)
                .addScope(new com.google.android.gms.common.api.Scope(Scopes.FITNESS_BODY_READ))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_READ)
                .build();

        Log.i(TAG, " fit hasPermissions " + GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(getActivity()), fitnessOptions));

        if (checkAppInstalled(getActivity(), "com.google.android.apps.fitness")) {
            Log.i(TAG, "onCreate: MegaApplication.IsFitFirstTime = " + MegaApplication.IsFitFirstTime);
            if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(getActivity()), fitnessOptions)) {
                Log.i(TAG, "onCreate: MegaApplication.getInstance().getMember().isNullAccessToken(). = " + MegaApplication.getInstance().getMember().isNullAccessToken());
                if (MegaApplication.getInstance().getMember().isNullAccessToken())
                    return;
                if (!MegaApplication.IsFitFirstTime) {
                    return;
                }
                MegaApplication.IsFitFirstTime = false;
                new AlertDialog.Builder(getActivity()).setCancelable(true)
                        .setTitle("Google Fit授權")
                        .setMessage("是否同意醫聯網取得Google Fit權限，以提供服務。")
                        .setPositiveButton("同意", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                GoogleSignIn.requestPermissions(
                                        getActivity(), // your activity
                                        GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                                        GoogleSignIn.getLastSignedInAccount(getActivity()),
                                        fitnessOptions);
                            }
                        }).setNegativeButton("不同意", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
            } else {
                accessGoogleFit();

            }
        }

    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.google_fit, container, false);
        }
        return rootView;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull DataSourcesResult dataSourcesResult) {

    }

    @Override
    public void onDataPoint(DataPoint dataPoint) {

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private boolean checkAppInstalled(Context context, String pkgName) {
        if (pkgName == null || pkgName.isEmpty()) {
            return false;
        }
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(pkgName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //mClient.stopAutoManage(getActivity());
        mClient.disconnect();
    }

    public void triggerPermission(Context context) {

        isTrigger = true;
        fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_READ)
                .build();

        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(context), fitnessOptions)) {
            Log.i(TAG, "onCreate: MegaApplication.getInstance().getMember().isNullAccessToken(). = " + MegaApplication.getInstance().getMember().isNullAccessToken());
            if (MegaApplication.getInstance().getMember().isNullAccessToken())
                return;
            new AlertDialog.Builder(context).setCancelable(true)
                    .setTitle("Google Fit授權")
                    .setMessage("是否同意醫聯網取得Google Fit權限，以提供服務。")
                    .setPositiveButton("同意", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            GoogleSignIn.requestPermissions(
                                    getActivity(), // your activity
                                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                                    GoogleSignIn.getLastSignedInAccount(getActivity()),
                                    fitnessOptions);
                        }
                    }).setNegativeButton("不同意", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult: requestCode = " + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (GOOGLE_FIT_PERMISSIONS_REQUEST_CODE == requestCode) {

                if (isTrigger)
                    ((HealthPushDetailActivity) getActivity()).initView();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i(TAG, "onRequestPermissionsResult: requestCode = " + requestCode);
        if (isTrigger) {

            if (requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ((HealthPushDetailActivity) getActivity()).initView();
                } else {
                    // Permission Denied
                }
            }
            //((HealthPushDetailActivity)getActivity()).initView();
        }
    }
}
