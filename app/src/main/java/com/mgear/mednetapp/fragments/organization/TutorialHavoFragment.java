package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.entity.organization.Customer;
import com.mgear.mednetapp.utils.CommonFunc;

import java.util.Locale;

/**
 * Created by Jye on 2017/6/9.
 * class: TutorialHavoFragment
 */
public class TutorialHavoFragment extends Fragment {

    private View rootView;
    private ImageView imgHavo;
    private ImageView imgStar1;
    private ImageView imgStar2;
    private ImageView imgStar3;
    private ImageView imgStar4;
    private ImageView imgStar5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tutorial_havo, container, false);
            RelativeLayout root = (RelativeLayout) rootView.findViewById(R.id.rootTutorialHavo);
            root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_havo_bg)));
            imgHavo = (ImageView) rootView.findViewById(R.id.imgTutorialHavo);
            imgStar1 = (ImageView) rootView.findViewById(R.id.imgTutorialHavoStar1);
            imgStar2 = (ImageView) rootView.findViewById(R.id.imgTutorialHavoStar2);
            imgStar3 = (ImageView) rootView.findViewById(R.id.imgTutorialHavoStar3);
            imgStar4 = (ImageView) rootView.findViewById(R.id.imgTutorialHavoStar4);
            imgStar5 = (ImageView) rootView.findViewById(R.id.imgTutorialHavoStar5);
            TextView txtHello = (TextView) rootView.findViewById(R.id.txtTutorialHavoHelp);
            Customer customer = CustomerActivity.getCustomer();
            txtHello.setText(String.format(Locale.getDefault(), getString(R.string.field_hello_content), customer.getName()));
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        imgHavo.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.wobble_bottom));
        ScaleAnimation animScaleUp = new ScaleAnimation(0.8f, 1.2f, 0.8f, 1.2f, 1, 0.5f, 1, 0.5f);
        animScaleUp.setDuration(1000);
        animScaleUp.setInterpolator(new LinearInterpolator());
        animScaleUp.setRepeatCount(-1);
        animScaleUp.setRepeatMode(2);
        imgStar1.startAnimation(animScaleUp);
        imgStar2.startAnimation(animScaleUp);
        imgStar3.startAnimation(animScaleUp);
        imgStar4.startAnimation(animScaleUp);
        imgStar5.startAnimation(animScaleUp);
    }

    @Override
    public void onPause() {
        super.onPause();
        imgHavo.clearAnimation();
        imgStar1.clearAnimation();
        imgStar2.clearAnimation();
        imgStar3.clearAnimation();
        imgStar4.clearAnimation();
        imgStar5.clearAnimation();
    }

}