package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/6/9.
 * class: TutorialStatusFragment
 */
public class TutorialStatusFragment extends Fragment implements Runnable {

    private int nIndex;
    private View rootView;
    private ImageView imgTarget;
    private ImageView imgCare;
    private Handler mHandler;

    public void startPagerAnimation() {
        nIndex = 0;
        imgTarget.setVisibility(View.INVISIBLE);
        mHandler.postDelayed(this, 500);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tutorial_status, container, false);
            mHandler = new Handler(Looper.getMainLooper());
            RelativeLayout root = (RelativeLayout) rootView.findViewById(R.id.rootTutorialStatus);
            root.setBackground(new BitmapDrawable(getResources(), CommonFunc.readBitmap(getActivity(), R.drawable.tutorial_status_bg)));
            imgTarget = (ImageView) rootView.findViewById(R.id.imgTutorialStatusTarget);
            imgCare = (ImageView) rootView.findViewById(R.id.imgTutorialStatusCare);
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacks(this);
        imgCare.clearAnimation();
        imgTarget.clearAnimation();
    }

    @Override
    public void run() {
        switch (nIndex++) {
            case 0:
                Animation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) ((-imgCare.getHeight()) / 5));
                translateAnimation.setDuration(200);
                translateAnimation.setInterpolator(new LinearInterpolator());
                translateAnimation.setRepeatCount(1);
                translateAnimation.setRepeatMode(2);
                imgCare.startAnimation(translateAnimation);
                mHandler.postDelayed(this, 700);
                break;
            case 1:
                ScaleAnimation animScaleUp = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 1.0f);
                animScaleUp.setDuration(250);
                animScaleUp.setInterpolator(new LinearInterpolator());
                imgTarget.setVisibility(View.VISIBLE);
                imgTarget.startAnimation(animScaleUp);
                break;
        }
    }

}