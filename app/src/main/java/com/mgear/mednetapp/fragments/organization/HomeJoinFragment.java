package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.HomeJoinPagerAdapter;
import com.mgear.mednetapp.views.CustomViewPager;

/**
 * Created by Jye on 2017/6/15.
 * class: HomeJoinFragment
 */
public class HomeJoinFragment extends Fragment {

    private View rootView;
    private CustomViewPager mPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_home_join, container, false);
            TabLayout tab = (TabLayout) rootView.findViewById(R.id.tabHomeJoin);
            mPager = (CustomViewPager) rootView.findViewById(R.id.vpHomeJoinPager);

            tab.addTab(tab.newTab().setText(getString(R.string.title_official)));
            tab.addTab(tab.newTab().setText(getString(R.string.title_facebook)));
            tab.addTab(tab.newTab().setText(getString(R.string.title_line)));
            tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    mPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            // Create the adapter that will return a fragment for each of the
            // primary sections of the activity.
            HomeJoinPagerAdapter adapter = new HomeJoinPagerAdapter(getChildFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mPager.setAdapter(adapter);
            mPager.setPagingEnabled(false);
            mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));
        }
        return rootView;
    }

}