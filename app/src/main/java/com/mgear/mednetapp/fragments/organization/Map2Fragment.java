package com.mgear.mednetapp.fragments.organization;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.interfaces.organization.BeaconPositionImpl;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.CustomToast;
import com.mgear.mednetapp.views.TouchImageView;
import com.mgear.mednetapp.views.materialspinner.MaterialSpinner;
import com.qozix.tileview.paths.CompositePathView.DrawablePath;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Dennis on 2019/3/26.
 * class: Map2Fragment
 */
public class Map2Fragment extends Fragment  {

    private SensorManager sensorManager  = null;
    private Sensor accSensor = null;
    private Sensor magSensor = null;

    private float[] magneticFieldValues;
    private float[] accelerometerValues;

    private int myPositionIndex;
    private UUID moveMarkerID;
    private boolean hasDrawPath = false;
    private boolean hasLoadedOnce = false;
    private MaterialSpinner spinnerLocateOption;
    private TouchImageView tvMap;
    private DrawablePath directionPath;

    private View rootView;
    private View btnMyPosition;
    private FrameLayout flMyPosition;
    private ImageView imgMyPositionBg;
    private HashMap<UUID, ImageView> markerMap;
    private List<String> locateList;

    private static String selectedLocateName;
    private UUID[] mLocatePosition;
    private BeaconPositionImpl beaconPosition;
    private CustomToast toastPosition;
    private CustomToast toastWeak;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("map", CommonFunc.getNowSeconds());
        if (rootView == null) {
            Activity mContext = getActivity();

            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_map2, container, false);
            tvMap = (TouchImageView) rootView.findViewById(R.id.tvMap2);
            //tvMap.getLayoutParams().width = 8000;
            //tvMap.getLayoutParams().height = 3005;
            //tvMap.setMinZoom(2);
            //Bitmap orgMap = getBitmapFromURL(HttpConnectFunc.orgMap);
            //tvMap.setImageBitmap(orgMap);
            //tvMap.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));


            //建立一個AsyncTask執行緒進行圖片讀取動作，並帶入圖片連結網址路徑
            new AsyncTask<String, Void, Bitmap>()
            {
                @Override
                protected Bitmap doInBackground(String... params)
                {
                    String url = CommonFunc.orgMap;
                    return getBitmapFromURL(url);
                }

                @Override
                protected void onPostExecute(Bitmap result)
                {
                    tvMap.setImageBitmap (result);
                    super.onPostExecute(result);
                }
            }.execute("圖片連結網址路徑");

        }

        return rootView;
    }

    //讀取網路圖片，型態為Bitmap
    private static Bitmap getBitmapFromURL(String imageUrl)
    {
        try
        {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

}