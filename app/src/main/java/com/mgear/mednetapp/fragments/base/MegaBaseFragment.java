package com.mgear.mednetapp.fragments.base;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;

/**
 * Created by Dennis.
 * class: MegaBaseFragment
 */
public abstract class MegaBaseFragment extends Fragment {
    protected boolean isVisible;
    protected IBackHandleIpml mIBackHandleIpml;
    private int stageWidth;
    protected View toastView;
    protected CallbackManager callbackManager;
    protected ShareDialog shareDialog;
    protected String title;

    abstract public boolean onBackPressed();

    /**
     * facebook配置, 在oncreate()方法中调用
     */
    protected void initFacebook() {
        //抽取成员变量
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                //分享成功的回调，在这里做一些自己的逻辑处理
                Log.i("debug", "onSuccess: result = " + result);
            }

            @Override
            public void onCancel() {
                Log.i("debug", "onCancel: ");

            }

            @Override
            public void onError(FacebookException e) {
                Log.e("debug", "onError: " + e);
                e.printStackTrace();
            }
        });
    }

    /**
     * 这是一个按钮的点击事件，分享到facebook
     * 若未安装facebook客户端，则会跳转到浏览器
     */
    public void shareToFacebook(String message) {
        //这里分享一个链接，更多分享配置参考官方介绍：https://developers.facebook.com/docs/sharing/android
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            Log.i("debug", "shareToFacebook: " + Uri.parse(message));
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    //.setQuote(title)
                    .setContentUrl(Uri.parse(message))
                    .build();
            shareDialog.show(linkContent);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = "";
        if (!(getActivity() instanceof IBackHandleIpml)) {
            throw new ClassCastException("Hosting Activity must implement BackHandledInterface");
        } else {
            this.mIBackHandleIpml = (IBackHandleIpml) getActivity();
        }


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        stageWidth = display.getWidth();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        toastView = inflater.inflate(R.layout.custom_toast_background, null);
        return null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        if (this.mIBackHandleIpml != null)
            this.mIBackHandleIpml.setSelectedFragment(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mIBackHandleIpml = (IBackHandleIpml) getActivity();
        } catch (Exception e) {
            e.printStackTrace();
            //throw new ClassCastException("Hosting activity must implement BackHandlerInterface");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //facebook的界面回调
        Log.i("debug", "onActivityResult: " + requestCode + "  " + resultCode + "   " + data);
        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    protected void onVisible() {
        lazyLoad();
    }

    protected void onInvisible() {
    }

    protected abstract void lazyLoad();

    public int getStageWidth() {
        return stageWidth;
    }

    public void setStageWidth(int stageWidth) {
        this.stageWidth = stageWidth;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}