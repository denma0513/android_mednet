package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.common.Common;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.entity.OauthSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.OnOauth2SignInListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONObject;

import java.util.Locale;

@SuppressLint("ValidFragment")
public class SignUpFragment extends MegaBaseFragment implements View.OnClickListener, OnOauth2SignInListener, TextView.OnEditorActionListener, TaskPost2ExecuteImpl {
    //常數
    private static String TAG = "SIGNIN";

    //畫面變數
    private View rootView;
    private Button mSignupNextButton, mSignupCancelButton;
    private EditText mSignupEmail, mSignupMobile, mSignupPassword, mSignupPasswordRetry;
    private TextView mSignupEmailWarning, mSignupMobileWarning, mSignupPasswordWarning, mSignupPasswordRetryWarning;
    private TextView mSignupMednetRule, mSignupPrivacy;
    //private OauthSet mOauthSet;


    /**
     * 呼叫註冊
     */
    private void doSignup() {
        boolean done = true;
        if ("".equals(mSignupEmail.getText().toString())) {
            mSignupEmailWarning.setText(getString(R.string.msg_invalid_email));
            mSignupEmailWarning.setVisibility(View.VISIBLE);
            done = false;
        } else {
            mSignupEmailWarning.setVisibility(View.GONE);
        }

        if (!CommonFunc.isEmailValid(mSignupEmail.getText().toString())) {
            mSignupEmailWarning.setText(getString(R.string.msg_invalid_email));
            mSignupEmailWarning.setVisibility(View.VISIBLE);
            done = false;
        } else {
            mSignupEmailWarning.setVisibility(View.GONE);
        }

        if ("".equals(mSignupMobile.getText().toString())) {
            mSignupMobileWarning.setText(getString(R.string.msg_invalid_mobile));
            mSignupMobileWarning.setVisibility(View.VISIBLE);
            done = false;
        } else {
            mSignupMobileWarning.setVisibility(View.GONE);
        }

        if (!CommonFunc.isMobileValid(mSignupMobile.getText().toString())) {
            mSignupMobileWarning.setText(getString(R.string.msg_invalid_mobile));
            mSignupMobileWarning.setVisibility(View.VISIBLE);
            done = false;
        } else {
            mSignupMobileWarning.setVisibility(View.GONE);
        }

        if ("".equals(mSignupPassword.getText().toString())) {
            mSignupPasswordWarning.setText(getString(R.string.msg_invalid_password));
            mSignupPasswordWarning.setVisibility(View.VISIBLE);
            done = false;
        } else {
            mSignupPasswordWarning.setVisibility(View.GONE);
        }
        if ("".equals(mSignupPasswordRetry.getText().toString()) || !mSignupPasswordRetry.getText().toString()
                .equals(mSignupPassword.getText().toString())) {
            mSignupPasswordRetryWarning.setText(getString(R.string.msg_invalid_password_retry));
            mSignupPasswordRetryWarning.setVisibility(View.VISIBLE);
            done = false;
        } else {
            mSignupPasswordRetryWarning.setVisibility(View.GONE);
        }

        if (done)
            doSignup(null);
    }

    private void doSignup(OauthSet oauthSet) {
        CommonApiTask task = new CommonApiTask(getActivity(), this, true, getString(R.string.msg_data_loading),
                TaskEnum.MednetCreateAccount);
        if (oauthSet == null) {
            Log.d(TAG, "新增帳密");
            String email = mSignupEmail.getText().toString();
            String password = mSignupPasswordRetry.getText().toString();
            task.execute("0", email, password);
            new CommonApiTask(getActivity(), this, false, "", TaskEnum.MednetAppLogEven)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "signup", "0");
        } else {
            Log.d(TAG, "第三方登入");
            MegaApplication.getInstance().setOauth(oauthSet);
            if (CommonFunc.isBlank(oauthSet.getEmail())) {
                //假設email是空的
                CommonFunc.showToast(getActivity(), "請同意取得Email權限！");
                return;
            }
            task.execute(oauthSet.getType(), oauthSet.getEmail(), "", oauthSet.getAccessToken());
            new CommonApiTask(getActivity(), this, false, "", TaskEnum.MednetAppLogEven)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "signup", oauthSet.getType());
        }
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {

            if (MegaApplication.IsMegaAppVersion) {
                rootView = inflater.inflate(R.layout.user_login_signup, container, false);
            } else {
                rootView = inflater.inflate(R.layout.user_login_signup_org, container, false);
            }
            mSignupEmail = rootView.findViewById(R.id.signup_email);
            mSignupMobile = rootView.findViewById(R.id.signup_mobile);
            mSignupPassword = rootView.findViewById(R.id.signup_password);
            mSignupPasswordRetry = rootView.findViewById(R.id.signup_password_retry);
            mSignupNextButton = rootView.findViewById(R.id.signup_next_button);
            mSignupCancelButton = rootView.findViewById(R.id.signup_cancel_button);

            mSignupEmailWarning = rootView.findViewById(R.id.signup_email_warning);
            mSignupMobileWarning = rootView.findViewById(R.id.signup_mobile_warning);
            mSignupPasswordWarning = rootView.findViewById(R.id.signup_password_warning);
            mSignupPasswordRetryWarning = rootView.findViewById(R.id.signup_password_retry_warning);

            mSignupMednetRule = rootView.findViewById(R.id.signup_mednet_rule);
            mSignupPrivacy = rootView.findViewById(R.id.signup_privacy);

            mSignupEmailWarning.setVisibility(View.GONE);
            mSignupMobileWarning.setVisibility(View.GONE);
            mSignupPasswordWarning.setVisibility(View.GONE);
            mSignupPasswordRetryWarning.setVisibility(View.GONE);
            mSignupNextButton.setOnClickListener(this);
            mSignupCancelButton.setOnClickListener(this);
            mSignupPasswordRetry.setOnEditorActionListener(this);
            mSignupMednetRule.setOnClickListener(this);
            mSignupPrivacy.setOnClickListener(this);

            try {
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                Oauth2LoginFragment oauth2LoginFragment = new Oauth2LoginFragment();
                oauth2LoginFragment.setOnOauth2SignInListener(this);
                FragmentTransaction ft = fragmentManager.beginTransaction();
                Fragment fragmentMain = oauth2LoginFragment;
                ft.replace(R.id.signup_auth_layout, fragmentMain);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            } catch (Exception e) {
                Log.w(TAG, "Oauth2LoginFragment : " + e.getStackTrace());
                //e.printStackTrace();
            }
        }
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signup_next_button:
                //test
                //((UserLoginActivity) getActivity()).goChkSMS();

                doSignup();
                break;
            case R.id.signup_cancel_button:
                ((UserLoginActivity) getActivity()).cancel();
                break;
            case R.id.signup_mednet_rule:
                ((UserLoginActivity) getActivity()).openWebView("https://med-net.com/Themes/Terms");
                break;
            case R.id.signup_privacy:
                ((UserLoginActivity) getActivity()).openWebView("https://med-net.com/Themes/Disclaimer");
                break;
        }
    }


    @Override
    public void OnSignIn(OauthSet oauth) {
        if (oauth == null) {
            CommonFunc.showToast(getActivity(), "登入失敗！");
            return;
        }
        //第三方註冊
        doSignup(oauth);
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetCreateAccount:
                //建立新會員帳號
                String message = "";
                if (result == null) {
                    message = getString(R.string.msg_cannot_connect);
                }

                if (result.optInt("state_code") != 200) {
                    message = result.optString("message");
                }

                if (message.length() > 0) {
                    CommonFunc.showToast(getActivity(), message);
                    return;
                }

                JSONObject source = result.optJSONObject("source");
                String accessToken = source.optString("access_token");
                String refreshToken = source.optString("refresh_token");
                //Log.i("debug", "accessToken= " + accessToken);
                if (accessToken != null && accessToken.length() > 0) {
                    //寫入accesstoken
                    MegaApplication.getInstance().getMember().setAccessToken(accessToken);
                    MegaApplication.getInstance().getMember().setRefreshToken(refreshToken);
                    MegaApplication.getInstance().getMember().setMobile(mSignupMobile.getText().toString());
                    if (MegaApplication.getInstance().getOauth() != null && !"".equals(MegaApplication.getInstance().getOauth().getType())) {
                        //原本第三方註冊完直接登入，現在需要跳到個人資料頁面補資料
                        //((UserLoginActivity) getActivity()).loginForever();
                        ((UserLoginActivity) getActivity()).goUpProfile();
                        return;
                    }

                    //更新使用者手機資料準備驗證簡訊
                    new CustomerApiTask(getActivity(), this, true, getString(R.string.msg_data_loading),
                            TaskEnum.MednetUpdateProfile).execute(mSignupMobile.getText().toString());
                }

                break;
            case MednetUpdateProfile:
                //更新完使用者手機
                //進入驗證簡訊流程
                ((UserLoginActivity) getActivity()).goChkSMS();
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

        switch (textView.getId()) {
            case R.id.signup_password_retry:
                mSignupNextButton.requestFocus();
                mSignupNextButton.performClick();
                break;
        }
        return false;
    }
}
