package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.internal.service.Common;
import com.mgear.mednetapp.BarChartActivity;
import com.mgear.mednetapp.HealthPushDetailActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.StepCompetitionActivity;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.entity.health.HealthBaseSet;
import com.mgear.mednetapp.entity.health.HealthBloodOxygenSet;
import com.mgear.mednetapp.entity.health.HealthBloodPressureSet;
import com.mgear.mednetapp.entity.health.HealthBloodSugarSet;
import com.mgear.mednetapp.entity.health.HealthBodySet;
import com.mgear.mednetapp.entity.health.HealthDrinkingSet;
import com.mgear.mednetapp.entity.health.HealthSleepingSet;
import com.mgear.mednetapp.entity.health.HealthStepSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.OnOauth2SignInListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.ProgressbarView;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;


@SuppressLint("ValidFragment")
public class HealthRecordFragment extends MegaBaseFragment implements View.OnClickListener, TaskPost2ExecuteImpl, NumberPicker.OnValueChangeListener {
    //常數
    private static String TAG = "HealthDetailRecord";
    private static int SIGNUP = 0;
    private static int FORGET = 1;

    //畫面變數
    private View rootView;
    private ProgressbarView mProgressBar;
    private JSONObject mHealthData;

    private HealthBaseSet baseSet;
    private TextView mRecordValue, mRecordStatus, mRecordTime, mRecordUnit2, mRecordValue2, mRecordType, mRecordPressureText, mRecordHealthContent;
    private ImageView mRecordImg, mRecordPressureImg;
    private ImageView mStepCompetition;

    private RelativeLayout mPickerLayout;
    private TextView mPickerCheck, mPickerCancel, mPickerTitle;
    private NumberPicker mPicker, mPicker2, mPicker3, mPicker4;
    private String pickValue, pickValue2, pickValue3, pickValue4, pickType;

    private String[] pickValueArr;

    private void setPickerNum(int num) {
        if (num > 4) num = 4;
        mPicker.setVisibility(View.VISIBLE);
        mPicker2.setVisibility(View.VISIBLE);
        mPicker3.setVisibility(View.VISIBLE);
        mPicker4.setVisibility(View.VISIBLE);
        mPicker.setDisplayedValues(null);
        mPicker2.setDisplayedValues(null);
        mPicker3.setDisplayedValues(null);
        mPicker4.setDisplayedValues(null);
        pickValue = pickValue2 = pickValue3 = pickValue4 = "";

        switch (num) {
            case 1:
                mPicker2.setVisibility(View.GONE);
                mPicker3.setVisibility(View.GONE);
                mPicker4.setVisibility(View.GONE);
                break;
            case 2:
                mPicker3.setVisibility(View.GONE);
                mPicker4.setVisibility(View.GONE);
                break;
            case 3:
                mPicker4.setVisibility(View.GONE);
                break;
        }

    }

    public void setSleepTimeSub(Double val) {
        int value = (int) (val * 60);
        //mRecordValue2.setText(CommonFunc.getHour(val));
        if (value > 0) {
            mRecordValue2.setText("+" + value + "分");
            mRecordValue2.setTextColor(getResources().getColor(R.color.status_normal));
        } else if (value < 0) {
            mRecordValue2.setText("-" + Math.abs(value) + "分");
            mRecordValue2.setTextColor(getResources().getColor(R.color.home_title_red));
        } else {
            mRecordValue2.setText("+" + value + "分");
            mRecordValue2.setTextColor(getResources().getColor(R.color.gray));
        }
    }

    public void openPick() {
        Log.i("debug", "openPick");

        JSONObject viewData = mHealthData;
        switch (viewData.optString("healthType")) {
            case "4":
                mPickerTitle.setText("設定目標");

                setPickerNum(2);
                pickValueArr = new String[16];
                for (int i = 0; i < 16; i++) {
                    pickValueArr[i] = Integer.toString(1500 + (i * 100));
                }
                mPicker.setMinValue(0);
                mPicker.setMaxValue(pickValueArr.length - 1);
                mPicker.setDisplayedValues(pickValueArr);
                int i = java.util.Arrays.asList(pickValueArr).indexOf(Integer.toString(MegaApplication.WaterCapacity));
                mPicker.setValue(i);

                String[] pickValueArr2 = new String[]{"ml"};
                mPicker2.setMinValue(0);
                mPicker2.setMaxValue(0);
                mPicker2.setValue(0);
                mPicker2.setDisplayedValues(pickValueArr2);

                break;
            case "6":
                mPickerTitle.setText("設定目標");

                setPickerNum(1);
                pickValueArr = new String[30];
                for (i = 0; i < 30; i++) {
                    pickValueArr[i] = Integer.toString(1000 * (i + 1));
                }
                mPicker.setMinValue(0);
                mPicker.setMaxValue(pickValueArr.length - 1);
                mPicker.setDisplayedValues(pickValueArr);

                if (!"".equals(MegaApplication.TargetStep)) {
                    i = java.util.Arrays.asList(pickValueArr).indexOf(Integer.toString(MegaApplication.TargetStep));
                    mPicker.setValue(i);
                }
                break;
            case "7":
                mPickerTitle.setText("設定身高");
                pickValueArr = new String[]{"."};
                setPickerNum(3);
                mPicker.setMinValue(90);
                mPicker.setMaxValue(200);
                mPicker2.setMinValue(0);
                mPicker2.setMaxValue(0);
                mPicker3.setMinValue(0);
                mPicker3.setMaxValue(9);

                mPicker.setValue(165);
                mPicker3.setValue(5);

                mPicker2.setValue(0);
                mPicker2.setDisplayedValues(pickValueArr);
                if (!"".equals(MegaApplication.BodyHeight)) {
                    mPicker.setValue(Integer.parseInt(MegaApplication.BodyHeight.split("\\.")[0]));
                    mPicker3.setValue(Integer.parseInt(MegaApplication.BodyHeight.split("\\.")[1]));
                }
                break;
        }
        mPicker.setWrapSelectorWheel(false); // 是否循環顯示
        mPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mPicker2.setWrapSelectorWheel(false); // 是否循環顯示
        mPicker2.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mPicker3.setWrapSelectorWheel(false); // 是否循環顯示
        mPicker3.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mPicker4.setWrapSelectorWheel(false); // 是否循環顯示
        mPicker4.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mPickerLayout.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    private void setHealthData() {
        JSONObject viewData = mHealthData;
        String value = "";
        SpannableString styledText = null;
        mRecordPressureImg.setVisibility(View.INVISIBLE);
        mRecordPressureText.setVisibility(View.INVISIBLE);
        switch (viewData.optString("healthType")) {
            case "1":
                HealthBloodSugarSet bloodSugarSet = new HealthBloodSugarSet(getActivity(), viewData);
                baseSet = bloodSugarSet;
                mRecordType.setText(bloodSugarSet.getValue2() + " " + bloodSugarSet.getUnit());
                mRecordValue.setText(bloodSugarSet.getValue());

                //至中
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRecordValue.getLayoutParams();
                lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                mRecordValue.setLayoutParams(lp);

                lp = (RelativeLayout.LayoutParams) mRecordImg.getLayoutParams();
                lp.leftMargin = -20;
                mRecordImg.setLayoutParams(lp);

                mRecordStatus.setText(bloodSugarSet.getStatusUseSpannableString());
                mRecordStatus.setTextColor(bloodSugarSet.getColor());
                mRecordTime.setText(bloodSugarSet.getRecordTime());
                mRecordImg.setImageResource(bloodSugarSet.getAcPcImg());
                mRecordUnit2.setText(bloodSugarSet.getUnit2());
                mRecordValue2.setText("");
                mRecordHealthContent.setText("當血糖升高到某一程度，超過腎臟所能回收的極限時，葡萄糖便會從尿液中「漏」出，所以稱之為糖尿病。其實，若血糖超過訂定的標準便算是糖尿病，不一定要有尿糖存在才算數。由於血糖要高到某程度才會有尿糖出現，所以測定血糖比尿糖較為準確，所以診斷糖尿病是以測定血糖為主。");
                break;
            case "2":
                HealthBloodPressureSet bloodPressureSet = new HealthBloodPressureSet(getActivity(), viewData);
                baseSet = bloodPressureSet;
                mRecordType.setText("收縮壓/舒張壓 (" + baseSet.getUnit() + ")");
                mRecordValue.setText(bloodPressureSet.getBloodPressure());
                mRecordValue2.setText(bloodPressureSet.getHr() + "");
                mRecordStatus.setText(bloodPressureSet.getStatusUseSpannableString());
                mRecordStatus.setTextColor(bloodPressureSet.getColor());
                mRecordTime.setText(bloodPressureSet.getRecordTime());
                mRecordImg.setImageResource(bloodPressureSet.getHealthImg());
                mRecordUnit2.setText(bloodPressureSet.getUnit2());
                mRecordPressureImg.setVisibility(View.VISIBLE);
                mRecordPressureText.setVisibility(View.VISIBLE);

                if (bloodPressureSet.getMedication() == 1) {
                    mRecordPressureImg.setImageResource(R.drawable.ic_take_pill);
                    mRecordPressureText.setText("有用藥");
                } else {
                    mRecordPressureImg.setImageResource(R.drawable.ic_take_pill_non);
                    mRecordPressureText.setText("無用藥");
                }
                mRecordHealthContent.setText("高血壓典型徵兆為頭頸之間緊繃、頭部有沉重感以及易疲倦，但大部分患者罹患高血壓時沒有特別明顯的徵兆，往往等到併發嚴重疾病如:腦中風、心肌梗塞、心臟衰竭及腎臟病等的疾病後才發現。");
                break;
            case "3":
                HealthBloodOxygenSet bloodOxygenSet = new HealthBloodOxygenSet(getActivity(), viewData);
                baseSet = bloodOxygenSet;
                mRecordType.setText("血氧量 (" + bloodOxygenSet.getUnit() + ")");

                //至中
                lp = (RelativeLayout.LayoutParams) mRecordValue.getLayoutParams();
                lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                mRecordValue.setLayoutParams(lp);

                lp = (RelativeLayout.LayoutParams) mRecordImg.getLayoutParams();
                lp.leftMargin = -20;
                mRecordImg.setLayoutParams(lp);
                mRecordValue.setText(bloodOxygenSet.getValue());
                mRecordValue2.setText(bloodOxygenSet.getValue2());
                mRecordStatus.setText(bloodOxygenSet.getStatusUseSpannableString());
                mRecordStatus.setTextColor(bloodOxygenSet.getColor());
                mRecordTime.setText(bloodOxygenSet.getRecordTime());
                mRecordImg.setImageResource(bloodOxygenSet.getHealthImg());
                mRecordUnit2.setText(bloodOxygenSet.getUnit2());
                mRecordHealthContent.setText("血氧濃度適合應用於家居照護，或是可在運動時監測身體的氧氣是否不足。尤其是有疾病的中老年患者，若是長期氧氣不足，將導致身體機能急劇退化，故測量血氧濃度對於監視呼吸品質、心跳、心率等有很大的幫助。");
                break;
            case "4":
                HealthDrinkingSet drinkingSet = new HealthDrinkingSet(getActivity(), viewData);
                baseSet = drinkingSet;
                mRecordType.setText("飲水量 (" + baseSet.getUnit() + ")");
                mRecordValue.setText(drinkingSet.todayValue());
                mRecordValue2.setText(drinkingSet.getCupCount() + "");
                mRecordStatus.setText(drinkingSet.getStatusUseSpannableString());
                mRecordStatus.setTextColor(drinkingSet.getColor());
                mRecordTime.setText(drinkingSet.getRecordTime());
                mRecordImg.setImageResource(drinkingSet.getHealthImg());
                mRecordUnit2.setText(drinkingSet.getUnit2());
                mRecordHealthContent.setText("正確的水分攝取，可維持人體基礎新陳代謝功能、保持心血管穩定正常運作、促進體內排除廢物。水能載舟亦能覆舟，過猶不及均不利於健康。有慢性病如心臟病、肝硬化、腎臟病則應該注意喝水量，請諮詢專科醫生。");
                break;
            case "5":
                HealthSleepingSet sleepingSet = new HealthSleepingSet(getActivity(), viewData);
                baseSet = sleepingSet;
                mRecordType.setText("睡眠時間");
                mRecordValue.setText(sleepingSet.sleepingHours());
                mRecordStatus.setText(sleepingSet.getStatusUseSpannableString());
                mRecordStatus.setTextColor(sleepingSet.getColor());
                mRecordTime.setText(sleepingSet.getRecordTime());
                mRecordImg.setImageResource(sleepingSet.getHealthImg());
                //mRecordValue2.setText("+01:00");
                mRecordValue2.setTextColor(getResources().getColor(R.color.status_normal));
                mRecordUnit2.setText("最近變化");
                mRecordHealthContent.setText("1.入睡時間：進入睡眠狀態的時間要小於30分鐘，也就是在半小時內睡著。\n" +
                        "2.夜醒次數：半夜醒來超過5分鐘的次數不超過一次。\n" +
                        "3.醒後入睡：睡到一半醒來，在20分鐘內可以再度入睡。\n" +
                        "4.睡著時間：躺在床上的時間，有超過85%處於睡著狀態。\n");
                break;
            case "6":
                mStepCompetition.setVisibility(View.VISIBLE);
                HealthStepSet stepSet = new HealthStepSet(getActivity(), viewData);
                baseSet = stepSet;
                mRecordType.setText("步數");

                mRecordValue.setText(stepSet.stepCount(), TextView.BufferType.SPANNABLE);

                if (Integer.parseInt(stepSet.getValue()) < Integer.parseInt(stepSet.getValue2())) {
                    value = "●  再加油喔";
                    mRecordStatus.setTextColor(getResources().getColor(R.color.status_non_enough));
                } else {
                    value = "●  已達目標";
                    mRecordStatus.setTextColor(getResources().getColor(R.color.status_normal));
                }
                styledText = new SpannableString(value);
                styledText.setSpan(new RelativeSizeSpan(0.5f), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                mRecordStatus.setText(styledText, TextView.BufferType.SPANNABLE);

                mRecordTime.setText(stepSet.getRecordTime());
                mRecordImg.setImageResource(stepSet.getHealthImg());
                mRecordUnit2.setText(stepSet.getUnit2());
                mRecordValue2.setText(stepSet.getValue3());
                mRecordHealthContent.setText("每天走個20分鐘，就能降低第二型糖尿病風險達35~50%。每天多走1千步，也有助降低血壓。如果一天能花1~2小時走路，還可減少三分之一的中風機率。整體而言，你的心血管疾病風險約可降低20~35%。");
                break;
            case "7":
                HealthBodySet bodySet = new HealthBodySet(getActivity(), viewData);
                baseSet = bodySet;
                mRecordType.setText("體重");
                mRecordValue.setText(baseSet.getValue());
                mRecordValue2.setText(baseSet.getValue2());
                mRecordStatus.setText(baseSet.getStatusUseSpannableString());
                mRecordStatus.setTextColor(baseSet.getColor());
                mRecordTime.setText(baseSet.getRecordTime());
                mRecordImg.setImageResource(baseSet.getHealthImg());
                mRecordUnit2.setText(baseSet.getUnit2());
                mRecordHealthContent.setText("1.多爬樓梯少搭電梯：如能爬樓梯而不搭乘電梯，以5層樓而言，1趟就可減少排碳量達1.09公斤。\n" +
                        "2.多走路、少開車：短距離通勤若由開車改為騎自行車或步行，以6公里計算(約步行1萬步)，單趟可減少1.42公斤排碳量。\n" +
                        "3.關掉電視來運動：每天少看1小時電視，改為出外散步或運動，也可以減少0.1公斤的排碳量。\n");
                break;
        }


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHealthData = ((HealthPushDetailActivity) getActivity()).getHealthData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_health_record, container, false);
            mProgressBar = rootView.findViewById(R.id.progress_bar);
            mRecordValue = rootView.findViewById(R.id.record_value);
            mRecordStatus = rootView.findViewById(R.id.record_status);
            mRecordTime = rootView.findViewById(R.id.record_time);
            mRecordImg = rootView.findViewById(R.id.record_type_img);
            mRecordUnit2 = rootView.findViewById(R.id.record_unit2);
            mRecordValue2 = rootView.findViewById(R.id.record_value2);
            mRecordType = rootView.findViewById(R.id.record_type);
            mRecordPressureImg = rootView.findViewById(R.id.record_pressure_img);
            mRecordPressureText = rootView.findViewById(R.id.record_pressure_text);
            mRecordHealthContent = rootView.findViewById(R.id.record_health_content);

            mStepCompetition = rootView.findViewById(R.id.step_competition);

            //picker
            mPickerLayout = rootView.findViewById(R.id.picker_layout);
            mPickerCheck = rootView.findViewById(R.id.picker_check);
            mPickerCancel = rootView.findViewById(R.id.picker_cancel);
            mPickerTitle = rootView.findViewById(R.id.picker_title);
            mPicker = rootView.findViewById(R.id.picker);
            mPicker2 = rootView.findViewById(R.id.picker2);
            mPicker3 = rootView.findViewById(R.id.picker3);
            mPicker4 = rootView.findViewById(R.id.picker4);

            mPickerLayout.setOnClickListener(this::onClick);
            mPickerCheck.setOnClickListener(this::onClick);
            mPickerCancel.setOnClickListener(this::onClick);
            mStepCompetition.setOnClickListener(this::onClick);

            mPickerLayout.setVisibility(View.GONE);

            mPicker.setOnValueChangedListener(this);
            mPicker2.setOnValueChangedListener(this);
            mPicker2.setOnValueChangedListener(this);
            mPicker2.setOnValueChangedListener(this);

            updateView();

            //測試中
            //getHealthDetail();
        }
        return rootView;
    }

    private void updateView() {
        setHealthData();
        if (baseSet != null) {
            mProgressBar.setProgressData(baseSet);
        }
        mProgressBar.setWidth(getStageWidth());
    }

    public void getHealthDetail() {
        Log.i("debug", "getHealthDetail ");
        new CustomerApiTask(getActivity(), this, true, getString(R.string.msg_data_saving)
                , TaskEnum.MednetQueryMeasureValueByID, true).execute(mHealthData.optString("healthType"), mHealthData.optString("measureId"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.picker_check: {
                JSONObject viewData = mHealthData;
                SharedPreferences settings = getActivity().getSharedPreferences("HealthSettingNotice", Activity.MODE_PRIVATE);
                switch (viewData.optString("healthType")) {
                    case "4":
                        MegaApplication.WaterCapacity = Integer.parseInt(pickValueArr[mPicker.getValue()]);
                        settings.edit().putInt("WaterCapacity", MegaApplication.WaterCapacity).apply();
                        break;
                    case "6":
                        MegaApplication.TargetStep = Integer.parseInt(pickValueArr[mPicker.getValue()]);
                        settings.edit().putInt("TargetStep", MegaApplication.TargetStep).apply();
                        break;
                    case "7":
                        MegaApplication.BodyHeight = mPicker.getValue() + "." + mPicker3.getValue();
                        settings.edit().putString("BodyHeight", MegaApplication.BodyHeight).apply();
                        break;
                }
                updateView();
                CommonFunc.showToast(getActivity(), "設定成功");
                mPickerLayout.setVisibility(View.GONE);
            }
            break;
            case R.id.picker_cancel: {
                mPickerLayout.setVisibility(View.GONE);
            }
            break;
            case R.id.picker_layout: {
                mPickerLayout.setVisibility(View.GONE);
            }
            break;
            case R.id.step_competition: {
                Intent intent = new Intent(getActivity(), StepCompetitionActivity.class);
                startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeStepCompetition));
            }
            break;
        }
    }


    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetQueryMeasureValueByID:
                Log.i("debug", "MednetQueryMeasureValueByID ");
                if (result != null && "200".equals(result.optString("state_code"))) {
                    JSONObject source = result.optJSONObject("source");
                    try {
                        source.put("healthTypeName", mHealthData.optString("healthName"));
                        source.put("healthType", mHealthData.optString("healthType"));
                        source.put("healthTypeKey", mHealthData.optString("healthTypeKey"));
                        source.put("healthImg", mHealthData.optString("healthImg"));
                        source.put("healthIcon", mHealthData.optString("healthRecordIcon"));
                        mHealthData = source;

                        //Log.i("debug", "mHealthData = " + mHealthData);

                        setHealthData();
//                        if (baseSet != null) {
//                            mProgressBar.setProgressData(baseSet);
//                        }
//                        mProgressBar.setWidth(getStageWidth());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
        Vibrator myVibrator = (Vibrator) getActivity().getSystemService(Service.VIBRATOR_SERVICE);
        myVibrator.vibrate(100);
    }
}
