package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;


import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.enums.CultureEnum;
import com.mgear.mednetapp.fragments.WebViewBaseFragment;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.HttpConnectFunc;

import java.util.Locale;

/**
 * Created by Jye on 2017/6/15.
 * class: HomePagerAdapter
 */
public class HomePagerAdapter extends FragmentPagerAdapter {

    private HomeItemFragment itemFragment;
    private HomeJoinFragment joinFragment;
    private TestsFragment testsFragment;
    private ReportFragment reportFragment;
    private ReportTodayFragment reportTodayFragment;
    private ReportHistoryFragment reportHistoryFragment;
    private ReportCheckupFragment reportCheckupFragment;
    private WebViewBaseFragment newsFragment;
    private WebViewBaseFragment promotionFragment;
    private WebViewBaseFragment articleFragment;
    private WebViewBaseFragment ilinkareFragment;
    private WebViewBaseFragment gameFragment;
    private HealthySystemFragment healthySystemFragment;
    private HealthyQuestionFragment healthyQuestionFragment;
    private WebViewBaseFragment orgRatingFragment;


    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
        itemFragment = new HomeItemFragment();
        joinFragment = new HomeJoinFragment();
        testsFragment = new TestsFragment();
        reportFragment = new ReportFragment();
        reportTodayFragment = new ReportTodayFragment();
        reportHistoryFragment = new ReportHistoryFragment();
        reportCheckupFragment = new ReportCheckupFragment();
        newsFragment = new WebViewBaseFragment();
        promotionFragment = new WebViewBaseFragment();
        articleFragment = new WebViewBaseFragment();
        ilinkareFragment = new WebViewBaseFragment();
        gameFragment = new WebViewBaseFragment();
        healthySystemFragment = new HealthySystemFragment();
        healthyQuestionFragment = new HealthyQuestionFragment();
        orgRatingFragment = new WebViewBaseFragment();

        //載入網頁內容
        newsFragment.setUrl("news", HttpConnectFunc.CONNECT_URL + "Announce/AnnounceList?type=39374B9E-BD74-46D7-B795-E32926971B00");
        promotionFragment.setUrl("promotion", HttpConnectFunc.CONNECT_URL + "Announce/AnnounceList?type=0DC703C7-C704-4C16-B81A-C652FF7A6212");
        articleFragment.setUrl("article", HttpConnectFunc.CONNECT_URL + "Announce/AnnounceList?type=B33EA3CC-0656-4FCF-B688-5010659EF2A2");
        gameFragment.setUrl("game", String.format(Locale.getDefault(), "%sQuestion/QuestionList?contact=%s&En=%s",
                HttpConnectFunc.CONNECT_URL,
                String.valueOf(CustomerActivity.getCustomer().getContactCode()),
                CultureSelectActivity.SelectCulture == CultureEnum.English ? "true" : "false"));
        ilinkareFragment.setUrl("med-net", "https://med-net.com");
//        facebookFragment.setUrl("facebook", " fb://page/885458688469771");
//        lineFragment.setUrl("lineFragment", "https://line.me/R/ti/p/%40hke2271c#~");
        orgRatingFragment.setUrl("organizationFragment", CommonFunc.ratingUrl);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Log.i("Homeitem","e04  position = "+position);

        switch (position) {
            case 0:
                return itemFragment;
            case 1:
                return newsFragment;
            case 2:
                return promotionFragment;
            case 3:
                return testsFragment;
            case 4:
                return articleFragment;
            case 5:
                return healthySystemFragment;
            case 6:
                return gameFragment;
            case 7:
                return joinFragment;
            case 8:
                return ilinkareFragment;
            case 9:
                return healthyQuestionFragment;
            case 10:
                return reportTodayFragment;
            case 11:
                return reportHistoryFragment;
            case 12:
                return reportCheckupFragment;
            case 13:
                return orgRatingFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 14;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}