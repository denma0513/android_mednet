package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.common.internal.service.Common;
import com.google.android.youtube.player.internal.h;
import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.HomePageViewAdapter;
import com.mgear.mednetapp.enums.RefreshViewEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.OnRefreshListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.RefreshHeaderRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by Dennis
 * class: HomePageFragment
 */
public class HomePageFragment extends MegaBaseFragment implements TaskPost2ExecuteImpl, View.OnClickListener {
    //靜態
    private static String TAG = "HomePageFragment";
    private static final int FINISH = 1;

    //畫面參數
    private View rootView;
    private RefreshHeaderRecyclerView mRecyclerView;
    private HomePageViewAdapter mAdapter;
    private JSONArray homeList = new JSONArray();
    private RelativeLayout mAidoctorLayout;
    private ImageView mAidoctorCancel;
    private LinearLayout mAidoctorNew, mAidoctorOld;


    //邏輯參數
    private int[] layoutList;
    private int[][] viewIdList;
    private boolean hadIntercept;
    private boolean isPrepared;
    private JSONArray healthList;


    private INativeActionImpl mINativeActionIpml;

    public int[] getLayoutList() {
        return layoutList;
    }

    public int[][] getViewIdList() {
        return viewIdList;
    }

    public void triggerAidoctor() {
        mAidoctorLayout.setVisibility(View.VISIBLE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            this.layoutList = new int[]{
                    R.layout.home_viewtype1,
                    R.layout.home_viewtype2,
                    R.layout.home_viewtype3,
                    R.layout.home_viewtype4,
                    R.layout.home_viewtype5,
                    R.layout.home_viewtype6,
            };
            this.viewIdList = new int[][]{{
                    R.id.viewType1Item1,
                    R.id.viewType1Item2,
                    R.id.viewType1Item3
            }, {
                    R.id.viewType2Item1,
                    R.id.viewType2Item2
            }, {
                    R.id.viewType3Item1,
                    R.id.viewType3Item2,
                    R.id.viewType3Item3,
                    R.id.viewType3Item4,
            }, {}, {}, {}
            };

            try {
                rootView = inflater.inflate(R.layout.fragment_main_view, container, false);
                //searchBar = rootView.findViewById(R.id.search);
                mRecyclerView = rootView.findViewById(R.id.recyclerView);

                mAidoctorLayout = rootView.findViewById(R.id.aidoctor_layout);
                mAidoctorCancel = rootView.findViewById(R.id.aidoctor_cancel);
                mAidoctorNew = rootView.findViewById(R.id.aidoctor_new);
                mAidoctorOld = rootView.findViewById(R.id.aidoctor_old);


                mAidoctorLayout.setVisibility(View.GONE);
                mAidoctorCancel.setOnClickListener(this);
                mAidoctorNew.setOnClickListener(this);
                mAidoctorOld.setOnClickListener(this);

                this.mINativeActionIpml = (INativeActionImpl) getActivity();

                //searchBar.requestFocus();
                mAdapter = new HomePageViewAdapter(getActivity(), this.getLayoutList(), this.getViewIdList());
                mAdapter.setHomePageFragmentContext(this);

                //TODO 修正中
                mAdapter.setAdapterList(MegaApplication.homeList, MegaApplication.healthList);

                mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                mRecyclerView.setAdapter(mAdapter, RefreshViewEnum.homePageRefreshView);
                mRecyclerView.setOnRefreshListener(new OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshData();
                    }
                });

                mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int visibleItemCount = layoutManager.getChildCount();
                        int totalItemCount = layoutManager.getItemCount();

                        if (firstVisibleItem >= 4 && !MegaApplication.scrollLog) {
                            Log.i(TAG, "firstVisibleItem = " + firstVisibleItem + ": visibleItemCount = " + visibleItemCount + " totalItemCount = " + totalItemCount);
                            Log.i(TAG, "紀錄ＬＯＧ scrollLog ");
                            MegaApplication.scrollLog = true;
                            new CommonApiTask(getActivity(), HomePageFragment.this, false, "", TaskEnum.MednetAppLogEven)
                                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "scorll", "home_page");
                        }

                        boolean mShouldReload = firstVisibleItem + visibleItemCount == totalItemCount && dy > 0;


                    }
                });

                new CommonApiTask(getActivity(), this, false, getString(R.string.msg_data_loading), TaskEnum.HomePageTask).executeOnExecutor(MegaApplication.threadPoolExecutor);
                //撈出健康資料
                new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                        , TaskEnum.MednetQueryLastMeasureValue, false).executeOnExecutor(MegaApplication.threadPoolExecutor);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        //lazyLoad();
        return rootView;
    }


    /**
     * 重整畫面資訊
     */
    private void reloadViewdate() {
        Log.i(TAG, "healthList = " + healthList);
        //mAdapter.setAdapterList(homeList, healthList);
        mAdapter.notifyDataSetChanged();
    }


    /**
     * 更新資料
     */
    public void refreshData() {
        try {
            new CommonApiTask(getActivity(), this, false, getString(R.string.msg_data_loading), TaskEnum.HomePageTask).executeOnExecutor(MegaApplication.threadPoolExecutor);
        } catch (Exception e) {
            Log.e(TAG, "refreshData: " + e.getMessage());
            //e.printStackTrace();
        }
    }

    /**
     * 更新健康資料
     */
    public void refreshHealth() {
        mAdapter.notifyDataSetChanged();
        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                , TaskEnum.MednetQueryLastMeasureValue, false).executeOnExecutor(MegaApplication.threadPoolExecutor);
    }

    private void getHealthRecord() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String startTime = "";
        String endTime = "";

        try {
            int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            endTime = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + maxDay + "T23:59:59Z";
            cal.setTime(sdf.parse(endTime));
            startTime = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/01T00:00:00Z";
        } catch (Exception e) {
            e.printStackTrace();
        }

        //判斷是否已經撈過資料，假設撈過只撈步數
        if (MegaApplication.healthRecord.length() > 0 || MegaApplication.getInstance().getMember().isNullAccessToken()) {
            String finalStartTime = startTime;
            String finalEndTime = endTime;
            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                    , TaskEnum.MednetQueryMeasureValues, false).executeOnExecutor(MegaApplication.threadPoolExecutor, "6", finalStartTime, finalEndTime, "false");
            return;
        }

        for (int i = 0; i < MegaApplication.healthTypeList.length; i++) {
            String type = MegaApplication.healthTypeList[i];
            String finalStartTime = startTime;
            String finalEndTime = endTime;
            //Log.i("debug", "開始撈歷史紀錄 " + type + " " + startTime + "    " + endTime);
            if ("4".equals(type) || "6".equals(type)) {
                new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                        , TaskEnum.MednetQueryMeasureValues, false).executeOnExecutor(MegaApplication.threadPoolExecutor, type, finalStartTime, finalEndTime, "true");
            } else {
                new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                        , TaskEnum.MednetQueryMeasureValues, false).executeOnExecutor(MegaApplication.threadPoolExecutor, type, finalStartTime, finalEndTime, "false");
            }

        }
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler sHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == FINISH) {
                //Toast.makeText(getActivity(), "更新完成！", Toast.LENGTH_SHORT).show();
                mRecyclerView.refreshComplete();
            }
        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.aidoctor_cancel: {
                mAidoctorLayout.setVisibility(View.GONE);
                break;
            }
            case R.id.aidoctor_new: {
                mAidoctorLayout.setVisibility(View.GONE);
                String url = String.format(MegaApplication.AIDOCTOR_URL, MegaApplication.getInstance().getMember().getCustomerId());
                mINativeActionIpml.triggerAction("1", url);
                mAidoctorLayout.setVisibility(View.GONE);
                break;
            }
            case R.id.aidoctor_old: {
                mINativeActionIpml.triggerAction("1", "https://med-net.com/AIDoctor");
                mAidoctorLayout.setVisibility(View.GONE);
                break;
            }
        }

    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        Message message = Message.obtain();
        message.what = FINISH;
        sHandler.sendMessage(message);
        try {
            switch (type) {
                case HomePageTask:
                    if (result != null && "200".equals(result.optString("code"))) {
                        homeList = result.optJSONArray("data");
                        if (homeList != null) {
                            MegaApplication.homeList = homeList;
                            //Log.i("debug", "MegaApplication.homeList = " + MegaApplication.homeList);
                            mAdapter.setAdapterList(MegaApplication.homeList, MegaApplication.healthList);
                            mAdapter.notifyDataSetChanged();
                        }


                    }
                    break;
                case MednetQueryLastMeasureValue:
                    healthList = new JSONArray();
                    int i = 0;
                    boolean checkNew = false;
                    for (String healthType : MegaApplication.healthSortList) {
                        JSONObject item = new JSONObject();
                        JSONObject healthMap = MegaApplication.healthMap.get(healthType);
                        String typeName = healthMap.optString("healthTypeName");
                        if (result != null) {

                            if (result != null && !result.isNull("source") && !result.optJSONObject("source").isNull(typeName)) {
                                item = result.optJSONObject("source").optJSONObject(typeName);
                            } else {
                                item = new JSONObject();
                            }


                            //特別需要給初始資料，或是透過google fit新增資料的
                            String mTime = item.optString("m_time");
                            //Log.i("debug", "mTime = " + mTime);
                            if (!"".equals(mTime)) {
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(CommonFunc.UTC2Date(mTime));
                                Calendar now = Calendar.getInstance();

                                if ("1".equals(healthType)) {
                                    if (item.optDouble("ac") > 0 && item.optDouble("pc") > 0) {
                                        //飯前飯後都有職 要去抓最後一筆
                                        checkNew = true;

                                        if (MegaApplication.healthList != null && MegaApplication.healthList.length() == 7)
                                            item = MegaApplication.healthList.optJSONObject(i);
                                        else
                                            item = new JSONObject();

                                        Calendar cal2 = Calendar.getInstance();
                                        cal2.setTime(new Date());
                                        String startTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "T00:00:00Z";
                                        String endTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + +cal.get(Calendar.DATE) + "T23:59:59Z";
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                                                , TaskEnum.MednetQueryMeasureValuesType1, true).executeOnExecutor(MegaApplication.threadPoolExecutor, startTime, endTime);
                                    }
                                } else if ("4".equals(healthType)) {

                                    if (now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
                                        //取今天是今天的飲水量
                                        MegaApplication.TodayWaterCapacity = item.optInt("drinking_water_today");
                                    } else {
                                        MegaApplication.TodayWaterCapacity = 0;
                                    }


                                } else if ("5".equals(healthType)) {
                                    if (item.optDouble("sleeping_hours") > 0.0 && now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
                                        //Log.i("debug", "不更新");
                                    } else {
                                        if (MegaApplication.TodaySleepHour > 0) {
                                            //Log.i("debug", "直接新增");
                                            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                                    , TaskEnum.MednetSleepingCreateOrUpdate, true).executeOnExecutor(MegaApplication.threadPoolExecutor, MegaApplication.TodaySleepTime,
                                                    MegaApplication.TodayWakeUpTime, Double.toString(MegaApplication.TodaySleepHour), CommonFunc.getNowUTCDateString());
                                        }
                                    }
                                } else if ("6".equals(healthType)) {
                                    //Log.i("debug", "healthType 步行");
                                    Calendar cal2 = Calendar.getInstance();
                                    int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

                                    cal2.setTime(new Date());
                                    if (item.optInt("step_count") > 0 && now.get(Calendar.DATE) == cal.get(Calendar.DATE)) {
                                        //Log.i("debug", "今天的資料");
                                        //今天的資料
                                        if (item.optInt("step_count") != MegaApplication.TodayStepCount && MegaApplication.TodayStepCount != 0) {
                                            //Log.i("debug", "更新");

                                            //先撈今天資料
                                            String startTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "T00:00:00Z";
                                            String endTime = cal2.get(Calendar.YEAR) + "/" + (cal2.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "T23:59:59Z";
//                                            new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
//                                                    , TaskEnum.MednetQueryMeasureValuesType6, true).executeOnExecutor(MegaApplication.threadPoolExecutor, startTime, endTime);

                                        }
                                    } else {
                                        Activity activity = getActivity();
                                        if (activity != null) {
//                                            new CustomerApiTask(activity, this, false, getString(R.string.msg_data_saving)
//                                                    , TaskEnum.MednetCreateStepCreateOrUpdate, true)
//                                                    .executeOnExecutor(MegaApplication.threadPoolExecutor, Integer.toString(MegaApplication.TodayStepCount), Double.toString(MegaApplication.TodayDistance),
//                                                            Double.toString(MegaApplication.TodayCalories),
//                                                            CommonFunc.getNowUTCDateString());
                                        }

                                    }
                                }
                            } else {
                                //沒有資料
                                if ("5".equals(healthType)) {
                                    if (!"".equals(MegaApplication.TodaySleepTime)) {
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                                , TaskEnum.MednetSleepingCreateOrUpdate, false).executeOnExecutor(MegaApplication.threadPoolExecutor, MegaApplication.TodaySleepTime,
                                                MegaApplication.TodayWakeUpTime, Double.toString(MegaApplication.TodaySleepHour), CommonFunc.getNowUTCDateString());
                                    }

                                } else if ("6".equals(healthType)) {
                                    //Log.i("debug", "healthType 步行");
                                    if (MegaApplication.TodayStepCount > 0) {
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                                , TaskEnum.MednetCreateStepCreateOrUpdate, false)
                                                .executeOnExecutor(MegaApplication.threadPoolExecutor, Integer.toString(MegaApplication.TodayStepCount), Double.toString(MegaApplication.TodayDistance),
                                                        Double.toString(MegaApplication.TodayCalories),
                                                        CommonFunc.getNowUTCDateStringOnlyDate());
                                    }
                                }
                            }
                        }

                        if (!"".equals(item.optString("bh"))) {
                            MegaApplication.BodyHeight = item.optString("bh");
                            SharedPreferences settings = getActivity().getSharedPreferences("HealthSettingNotice", Activity.MODE_PRIVATE);
                            settings.edit().putString("BodyHeight", MegaApplication.BodyHeight).apply();
                        }

                        item.put("healthTypeName", healthMap.optString("healthName"));
                        item.put("healthType", healthType);
                        item.put("healthTypeKey", typeName);
                        item.put("healthImg", healthMap.optString("healthImg"));
                        item.put("healthIcon", healthMap.optString("healthRecordIcon"));
                        item.put("index", i);
                        //Log.i("debug", "item  = " + item);
                        healthList.put(item);
                        i++;
                    }

                    MegaApplication.healthList = healthList;
                    reloadViewdate();

                    //偷偷撈紀錄資料
                    getHealthRecord();
                    break;
                case MednetQueryMeasureValuesType1:
                    //Log.i("debug", "MednetQueryMeasureValuesType1 = " + result);
                    if (result != null && !result.isNull("source")) {
                        JSONArray source = result.optJSONArray("source");
                        JSONObject finObj = new JSONObject();
                        if (source.length() >= 2) {
                            JSONObject obj = source.optJSONObject(0);
                            JSONObject obj2 = source.optJSONObject(1);
                            Calendar cal = Calendar.getInstance();
                            Calendar cal2 = Calendar.getInstance();
                            cal.setTime(CommonFunc.UTC2Date(obj.optString("m_time")));
                            cal2.setTime(CommonFunc.UTC2Date(obj2.optString("m_time")));
                            if (cal.compareTo(cal2) == 1) {
                                finObj = obj;
                            } else {
                                finObj = obj2;
                            }
                        } else if (source.length() == 1) {
                            finObj = source.optJSONObject(0);
                        }

                        for (i = 0; i < healthList.length(); i++) {
                            JSONObject obj = healthList.optJSONObject(i);
                            if ("1".equals(obj.optString("healthType"))) {
                                JSONObject healthMap = MegaApplication.healthMap.get("1");
                                String typeName = healthMap.optString("healthTypeName");
                                obj = finObj;
                                obj.put("healthTypeName", healthMap.optString("healthName"));
                                obj.put("healthType", "1");
                                obj.put("healthTypeKey", typeName);
                                obj.put("healthImg", healthMap.optString("healthImg"));
                                obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                                obj.put("index", i);
                                healthList.put(i, obj);
                            }
                        }
                        MegaApplication.healthList = healthList;
                        reloadViewdate();
                    }
                    break;
                case MednetQueryMeasureValuesType4:
                    if (result != null && !result.isNull("source")) {
                        //Log.i("debug", "result  = " + result);
                        JSONArray source = result.optJSONArray("source");

                        Calendar calTemp = Calendar.getInstance();
                        JSONObject finObj = null;
                        for (i = 0; i < source.length(); i++) {
                            try {
                                String updateDate = source.optJSONObject(i).optString("updated_at");
                                if (i == 0) {
                                    calTemp.setTime(CommonFunc.UTC2DateE04(updateDate));
                                    finObj = source.optJSONObject(i);
                                    continue;
                                }
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(CommonFunc.UTC2DateE04(updateDate));
                                if (cal.compareTo(calTemp) == 1) {
                                    calTemp.setTime(CommonFunc.UTC2Date(updateDate));
                                    finObj = source.optJSONObject(i);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        if (finObj == null)
                            break;

                        MegaApplication.TodayWaterCapacity = finObj.optInt("drinking_water_today");
                        //Log.i("debug", "MegaApplication.TodayWaterCapacity = " + MegaApplication.TodayWaterCapacity);
                        for (i = 0; i < healthList.length(); i++) {
                            JSONObject obj = healthList.optJSONObject(i);
                            if ("4".equals(obj.optString("healthType"))) {
                                JSONObject healthMap = MegaApplication.healthMap.get("4");
                                String typeName = healthMap.optString("healthTypeName");
                                obj = finObj;
                                obj.put("healthTypeName", healthMap.optString("healthName"));
                                obj.put("healthType", "4");
                                obj.put("healthTypeKey", typeName);
                                obj.put("healthImg", healthMap.optString("healthImg"));
                                obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                                obj.put("index", i);
                                healthList.put(i, obj);
                                //Log.i("debug", "healthList = '" + healthList);
                            }
                        }
                        MegaApplication.healthList = healthList;
                        reloadViewdate();
                    }
                    break;
                case MednetQueryMeasureValuesType6:
                    if (result != null && !result.isNull("source")) {
                        JSONArray source = result.optJSONArray("source");

                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving)
                                , TaskEnum.MednetCreateStepCreateOrUpdate, true)
                                .executeOnExecutor(MegaApplication.threadPoolExecutor, Integer.toString(MegaApplication.TodayStepCount), Double.toString(MegaApplication.TodayDistance),
                                        Double.toString(MegaApplication.TodayCalories),
                                        CommonFunc.getNowUTCDateStringOnlyDate(), source.optJSONObject(0).optInt("id") + "",
                                        source.optJSONObject(0).optString("measure_id"));
                    }
                    break;
                case MednetCreateStepCreateOrUpdate:
                    if (result != null && !result.isNull("source")) {
                        JSONObject source = result.optJSONObject("source");
                        for (i = 0; i < healthList.length(); i++) {
                            JSONObject obj = healthList.optJSONObject(i);
                            Calendar cal = null;
                            //Calendar calUp = null;
                            //Calendar calSourceUp = null;
                            Calendar calToday = Calendar.getInstance();
                            calToday.setTime(new Date());

                            try {
                                cal = Calendar.getInstance();
                                //calUp = Calendar.getInstance();
                                //calSourceUp = Calendar.getInstance();
                                Date mTime = CommonFunc.UTC2Date(source.optString("m_time"));
                                Date updateTime = CommonFunc.UTC2Date(obj.optString("updated_at"));
                                Date sourceUpdateTime = CommonFunc.UTC2Date(source.optString("updated_at"));
                                cal.setTime(mTime);
                                //calUp.setTime(updateTime);
                                //calSourceUp.setTime(sourceUpdateTime);
                            } catch (Exception e) {
                                //因為撈失敗，不把他當今天的資料
                                e.printStackTrace();
                                continue;
                            }

                            //Log.i(TAG, "e04e04e04 " + cal.get(Calendar.DAY_OF_MONTH) + " ==== " + calToday.get(Calendar.DAY_OF_MONTH));
                            //Log.i(TAG, "onPostExecute: e04e04e04 " + source.optString("m_time") + source.optString("step_count") + "  " + CommonFunc.isSameDay(cal, calToday));

                            //畫面只更新今天
                            if ("6".equals(obj.optString("healthType")) && cal != null && CommonFunc.isSameDay(cal, calToday)) {
                                JSONObject item = new JSONObject();
                                JSONObject healthMap = MegaApplication.healthMap.get("6");
                                String typeName = healthMap.optString("healthTypeName");
                                obj = source;
                                obj.put("healthTypeName", healthMap.optString("healthName"));
                                obj.put("healthType", "6");
                                obj.put("healthTypeKey", typeName);
                                obj.put("healthImg", healthMap.optString("healthImg"));
                                obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                                obj.put("index", i);
                                healthList.put(i, obj);
                            }

                        }
                        MegaApplication.healthList = healthList;
                        reloadViewdate();
                    }
                    break;
                case MednetSleepingCreateOrUpdate:
                    if (result != null && !result.isNull("source")) {
                        JSONObject source = result.optJSONObject("source");
                        for (i = 0; i < healthList.length(); i++) {
                            JSONObject obj = healthList.optJSONObject(i);
                            if ("5".equals(obj.optString("healthType"))) {
                                JSONObject item = new JSONObject();
                                JSONObject healthMap = MegaApplication.healthMap.get("5");
                                String typeName = healthMap.optString("healthTypeName");
                                obj = source;
                                obj.put("healthTypeName", healthMap.optString("healthName"));
                                obj.put("healthType", "5");
                                obj.put("healthTypeKey", typeName);
                                obj.put("healthImg", healthMap.optString("healthImg"));
                                obj.put("healthIcon", healthMap.optString("healthRecordIcon"));
                                obj.put("index", i);
                                healthList.put(i, obj);
                            }

                        }
                        MegaApplication.healthList = healthList;
                        reloadViewdate();
                    }
                    break;
                case MednetQueryMeasureValues:
                    //偷偷暫存資料
                    if (result != null && !result.isNull("source")) {
                        //Log.i("debug", "result  = " + result);
                        JSONArray source = result.optJSONArray("source");
                        Calendar cal = Calendar.getInstance();

                        String hType = "";
                        JSONArray healthAdapterList = new JSONArray();

                        if (source.length() > 0) {
                            if (source.optJSONObject(0).has("ac") || source.optJSONObject(0).has("pc")) {
                                hType = "1";
                            } else if (source.optJSONObject(0).has("sbp")) {
                                hType = "2";
                            } else if (source.optJSONObject(0).has("bs")) {
                                hType = "3";
                            } else if (source.optJSONObject(0).has("drinking_water_today")) {
                                hType = "4";
                            } else if (source.optJSONObject(0).has("sleep_time")) {
                                hType = "5";
                            } else if (source.optJSONObject(0).has("step_count")) {
                                hType = "6";
                            } else if (source.optJSONObject(0).has("bh")) {
                                hType = "7";
                            }
                            //Log.i("debug", "hType = " + hType + " source.length() =   " + source.length());
                            //開始跑撈出來的歷史資料列表
                            Map<String, Date> mDateMap = new HashMap<String, Date>();
                            for (i = source.length() - 1; i >= 0; i--) {
                                JSONObject obj = source.optJSONObject(i);
                                obj = MegaApplication.setHealthBasData(obj, hType);
                                //Log.i("debug", "mDateMap = " + mDateMap);
                                //飲水跟步行資料要判斷最大值只顯示一筆
                                if ("4".equals(hType)) {
                                    Date mTime = CommonFunc.UTC2Date(obj.optString("m_time"));
                                    Date updatedAt = CommonFunc.UTC2Date(obj.optString("updated_at"));
                                    //Log.i("debug", "obj.optString(\"updated_at\") = "+obj.optString("updated_at"));
                                    cal.setTime(mTime);
                                    String yyyymmdd = cal.get(Calendar.YEAR) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DAY_OF_MONTH);
                                    cal.setTime(updatedAt);

                                    Calendar cal2 = Calendar.getInstance();
                                    if (mDateMap.get(yyyymmdd) != null) {
                                        cal2.setTime(mDateMap.get(yyyymmdd));
                                        //Log.i("debug", "yyyymmdd = " + yyyymmdd + "  "
                                        //        + cal2.get(Calendar.HOUR) + " " + cal2.get(Calendar.MINUTE) + "    "
                                        //        + cal.get(Calendar.HOUR) + " " + cal.get(Calendar.MINUTE));
                                        if (cal.compareTo(cal2) == 1) {
                                            //Log.i("debug", "更新");
                                            mDateMap.put(yyyymmdd, cal.getTime());
                                            healthAdapterList = MegaApplication.replaceHealthItem(healthAdapterList, obj);
                                            continue;
                                        } else {
                                            //Log.i("debug", "跳過");
                                            continue;
                                        }
                                    } else {
                                        //Log.i("debug", "直接新增");
                                        mDateMap.put(yyyymmdd, cal.getTime());
                                    }
                                } else if ("6".equals(hType)) {
                                    Date mtime = CommonFunc.UTC2Date(obj.optString("m_time"));
                                    cal.setTime(mtime);
                                    String yyyymmdd = cal.get(Calendar.YEAR) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DAY_OF_MONTH);

                                    if (mDateMap.get(yyyymmdd) != null) {
                                        Calendar cal2 = Calendar.getInstance();
                                        cal2.setTime(mDateMap.get(yyyymmdd));
                                        if (cal.compareTo(cal2) == 1) {
                                            mDateMap.put(yyyymmdd, cal.getTime());
                                            healthAdapterList = MegaApplication.replaceHealthItem(healthAdapterList, obj);
                                        } else {
                                            continue;
                                        }
                                    } else {
                                        mDateMap.put(yyyymmdd, cal.getTime());
                                    }
                                }

                                //置入title
                                if (i == (source.length() - 1)) {
                                    Date mtime = CommonFunc.UTC2Date(obj.optString("m_time"));
                                    cal.setTime(mtime);
                                    JSONObject titleObj = new JSONObject();
                                    titleObj.put("title", cal.get(Calendar.YEAR) + "年" + (cal.get(Calendar.MONTH) + 1) + "月");
                                    healthAdapterList.put(titleObj);
                                }
                                healthAdapterList.put(obj);

                                if ("6".equals(hType)) {
                                    Date mTime = CommonFunc.UTC2Date(obj.optString("m_time"));
                                    cal.setTime(mTime);
                                    int mm = cal.get(Calendar.MONTH) + 1;
                                    int dd = cal.get(Calendar.DAY_OF_MONTH);
                                    String yyyymmdd = cal.get(Calendar.YEAR) + "" + (mm > 9 ? "" + mm : "0" + mm) + "" + (dd > 9 ? "" + dd : "0" + dd);
                                    JSONObject stepObj = MegaApplication.FitStepObj.optJSONObject(yyyymmdd);
                                    if (stepObj == null)
                                        continue;
                                    String step = stepObj.optString("stepCount", "0");
                                    String distance = stepObj.optString("distance", "0.0");
                                    int stepCount = Integer.parseInt(step);
                                    Double distanceNum = Double.parseDouble(distance);
                                    //Log.i(TAG, "onPostExecute: stepCount = " + stepCount + " =====  " + obj.optInt("step_count"));
                                    //Log.i(TAG, "onPostExecute: distance = " + distanceNum + " =====  " + obj.optInt("distance"));
                                    if (stepCount > obj.optInt("step_count") || distanceNum > obj.optInt("distance")) {
                                        distance = stepObj.optString("distance", "0");
                                        String calories = stepObj.optString("calories", "0");
                                        String upMTime = obj.optString("m_time").substring(0, obj.optString("m_time").indexOf("T")) + "T" + "00:00:00.000Z";
                                        //Log.i(TAG, "onPostExecute: upMTime = "+upMTime);
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving), TaskEnum.MednetCreateStepCreateOrUpdate, true)
                                                .executeOnExecutor(MegaApplication.threadPoolExecutor, step, distance, calories, upMTime, obj.optInt("id") + "", obj.optString("measure_id"));
                                    }
                                    MegaApplication.FitStepObj.remove(yyyymmdd);
                                }
                            }

                            if ("6".equals(hType)) {
                                for (Iterator<String> it = MegaApplication.FitStepObj.keys(); it.hasNext(); ) {
                                    String key = it.next();
                                    //Log.i(TAG, "onPostExecute: it key = " + key);
                                    JSONObject stepObj = MegaApplication.FitStepObj.optJSONObject(key);
                                    if (stepObj.has("stepCount")) {
                                        String step = stepObj.optString("stepCount", "0");
                                        String distance = stepObj.optString("distance", "0");
                                        String calories = stepObj.optString("calories", "0");
                                        String mTime = key.substring(0, 4) + "-" + key.substring(4, 6) + "-" + key.substring(6, 8) + "T" + "00:00:00.000Z";
                                        new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_saving), TaskEnum.MednetCreateStepCreateOrUpdate, true)
                                                .executeOnExecutor(MegaApplication.threadPoolExecutor, step, distance, calories, mTime);
                                        //MegaApplication.FitStepObj.remove(yyyymmdd);
                                    }
                                }
                            }
                            //Log.i("debug", "healthAdapterList = " + healthAdapterList);
                            if (healthAdapterList.length() > 0) {
                                MegaApplication.healthRecord.put(hType, healthAdapterList);
                                //Log.i("debug", "MegaApplication.healthRecord = " + MegaApplication.healthRecord);
                            }

                        }
                    }

                    break;

                default:
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onBackPressed() {
        return false;
//        if (hadIntercept) {
//            return false;
//        } else {
//            hadIntercept = true;
//            return true;
//        }
    }

}