package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mgear.mednetapp.BaseActivity;
import com.mgear.mednetapp.MainActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.helpers.DataHelper;
import com.mgear.mednetapp.interfaces.organization.WebPreviousPageImpl;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.FileUtils;
import com.mgear.mednetapp.utils.WebViewUtil;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jye on 2017/8/23.
 * class: WebViewFragment
 */
public class WebViewFragment extends MegaBaseFragment implements WebPreviousPageImpl {

    private float textZoom = 1.0f;

    private String strTitle;
    //private View rootView;
    private String strUrl;
    private WebView mWebView;
    private TextView txtMessage;
    private ProgressBar progressBar;
    private boolean hadIntercept;
    private int mRootView;
    private Date endDate, curDate;
    //private DataHelper mDataHelper;
    private Context mContext;
    private String mCameraPhotoPath;


    private ValueCallback<Uri> uploadMessage;
    private ValueCallback<Uri[]> uploadMessageAboveL;
    private final static int FILE_CHOOSER_RESULT_CODE = 10000;
    public static final int REQUEST_CODE_LOLIPOP = 1;
    private final static int RESULT_CODE_ICE_CREAM = 2;

    public void setTextZoom(float textZoom) {
        this.textZoom = textZoom;
    }

    public boolean canGoBack() {
        return mWebView.canGoBack();
    }

    public void goBack() {
        mWebView.goBack();
    }

    @Override
    protected void lazyLoad() {

    }

    private class MednetWebChromeClient extends WebChromeClient {

        private void openFileChooser(String type) {
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType(type);
            startActivityForResult(Intent.createChooser(i, "請選擇檔案"), RESULT_CODE_ICE_CREAM);
        }

        private void onShowFileChooser(Intent cameraIntent) {
            Intent selectionIntent = new Intent(Intent.ACTION_PICK, null);
            selectionIntent.setType("image/*");
            //------------------------------------
            Intent[] intentArray;
            if (cameraIntent != null) {
                intentArray = new Intent[]{cameraIntent};
            } else {
                intentArray = new Intent[0];
            }

            Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
            chooserIntent.putExtra(Intent.EXTRA_TITLE, "請選擇檔案");
            chooserIntent.putExtra(Intent.EXTRA_INTENT, selectionIntent);
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

            startActivityForResult(chooserIntent, REQUEST_CODE_LOLIPOP);

        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
            if (uploadMessageAboveL != null) {
                uploadMessageAboveL.onReceiveValue(null);
                uploadMessageAboveL = null;
            }
            uploadMessageAboveL = filePathCallback;

            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                    takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                // Continue only if the File was successfully created
                if (photoFile != null) {
                    mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                } else {
                    takePictureIntent = null;
                }
            }
            onShowFileChooser(takePictureIntent);

//            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//            i.addCategory(Intent.CATEGORY_OPENABLE);
//            i.setType("image/*");
//            startActivityForResult(Intent.createChooser(i, "Image Chooser"), FILE_CHOOSER_RESULT_CODE);
            return true;
        }
    }

    /**
     * More info this method can be found at
     * http://developer.android.com/training/camera/photobasics.html
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        Log.i("ebug", "createImageFile: " + imageFileName);
        Log.i("ebug", "storageDir: " + storageDir);
        File imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        return imageFile;
    }

    private class MednetWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.i("debug", "onPageStarted = " + url);
            curDate = new Date(System.currentTimeMillis());
            Log.e("debug", url + "網頁 開始呼叫");
        }

//        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//        @Override
//        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
//
//            try {
//                String url = request.getUrl().toString();
//                WebResourceResponse response = mDataHelper.getReplacedWebResourceResponse(
//                        mContext.getApplicationContext(), url);
//
//                if (response != null) {
//                    return response;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//
//            return super.shouldInterceptRequest(view, request);
//        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i("debug", "shouldOverrideUrlLoading = " + url);

            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Log.i("debug", "shouldOverrideUrlLoading = " + request.getUrl().toString());
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.i("debug", "onPageFinished = " + url);
            txtMessage.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
            for (String needLoginUrl : MegaApplication.needLoginUrlArray) {
                if (strUrl.contains(needLoginUrl)) {//需要登入的url
                    if (url.equals("https://med-net.com/")) {
                        Log.i("debug", "醫聯網 該回去登入囉");
                        JSONObject loginTarget = new JSONObject();

                        try {
                            loginTarget.put("action", MegaApplication.ActionTypeWebView);
                            loginTarget.put("target", strUrl);
                            ((BaseActivity) getActivity()).triggerAction(getActivity(), MegaApplication.ActionTypeLogin, loginTarget.toString(), mRootView);
                            //triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (url.contains("sso2.med-net.com")) {
                        Log.i("debug", "醫師諮詢 該回去登入囉");
                        try {
                            JSONObject loginTarget = new JSONObject();
                            loginTarget.put("action", MegaApplication.ActionTypeWebView);
                            loginTarget.put("target", strUrl);
                            ((BaseActivity) getActivity()).triggerAction(getActivity(), MegaApplication.ActionTypeLogin, loginTarget.toString(), mRootView);
                            //triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            endDate = new Date(System.currentTimeMillis());
            try {
                long diff = endDate.getTime() - curDate.getTime();
                Log.e("debug", "onPageFinished" + "  呼叫時間: " + diff);
            } catch (Exception e) {
                e.printStackTrace();
            }


            //if (view.canGoBack())
            //  ((MainActivity) getActivity()).setPreviousPage(WebViewFragment.this);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }


//        @Override
//        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            //全部擋掉
//            handler.cancel();
//        }


    }

    public void setUrl(String title, String url) {
        strTitle = title;
        strUrl = url;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void setRootView(int rootView) {
        mRootView = rootView;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        //MainActivity.addCustomerTrack(strTitle, CommonFunc.getNowSeconds());
        View rootView = inflater.inflate(R.layout.fragment_web_view, container, false);
        // Inflate the layout for this fragment
        progressBar = (ProgressBar) rootView.findViewById(R.id.pbBrowserWait);
        txtMessage = (TextView) rootView.findViewById(R.id.txtBrowserMessage);
        mWebView = (WebView) rootView.findViewById(R.id.webViewBrowser);
        //mDataHelper = new DataHelper();

        progressBar.setVisibility(View.GONE);
        txtMessage.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            mWebView.restoreState(savedInstanceState);
        }

        WebViewUtil.configWebView(mWebView);
        mWebView.setWebViewClient(new WebViewFragment.MednetWebViewClient());
        mWebView.setWebChromeClient(new WebViewFragment.MednetWebChromeClient());
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //載入頁面內容
        mWebView.loadUrl(strUrl);
    }

    @Override
    public void previousPage() {
        if (mWebView.canGoBack())
            mWebView.goBack();
    }

    @Override
    public boolean hasPreviousPage() {
        return mWebView.canGoBack();
    }

    @Override
    public boolean onBackPressed() {
        Log.i("debug", "mWebView.canGoBack() = " + mWebView.canGoBack());
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return false;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case RESULT_CODE_ICE_CREAM:
                Uri uri = null;
                if (data != null) {
                    uri = data.getData();
                }
                uploadMessage.onReceiveValue(uri);
                uploadMessage = null;
                break;
            case REQUEST_CODE_LOLIPOP:
                Uri[] results = null;
                // Check that the response is a good one
                if (resultCode == Activity.RESULT_OK) {
                    if (data == null) {
                        // If there is not data, then we may have taken a photo
                        if (mCameraPhotoPath != null) {
                            Log.d("AppChooserFragment", mCameraPhotoPath);

                            results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                        }
                    } else {
                        String dataString = data.getDataString();
                        if (dataString != null) {
                            results = new Uri[]{Uri.parse(dataString)};
                        }
                    }
                }

                uploadMessageAboveL.onReceiveValue(results);
                uploadMessageAboveL = null;
                break;
        }
    }


}