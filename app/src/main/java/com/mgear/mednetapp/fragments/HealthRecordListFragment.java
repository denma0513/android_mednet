package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;

import com.mgear.mednetapp.HealthPushDetailActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.adapter.HealthListViewAdapter;
import com.mgear.mednetapp.adapter.HealthRecordListAdapter;
import com.mgear.mednetapp.adapter.HealthRecordListAdapter_bak;
import com.mgear.mednetapp.entity.health.HealthBaseSet;
import com.mgear.mednetapp.enums.RefreshViewEnum;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.OnRefreshListener;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.ProgressbarView;
import com.mgear.mednetapp.views.RefreshHeaderRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@SuppressLint("ValidFragment")
public class HealthRecordListFragment extends MegaBaseFragment implements View.OnClickListener, TaskPost2ExecuteImpl, AbsListView.OnScrollListener {
    //常數
    private static String TAG = "HealthDetailRecord";
    private static int SIGNUP = 0;
    private static int FORGET = 1;
    private int upCount = 0;
    private static final int FINISH = 1;
    //畫面變數
    private View rootView;
    private ProgressbarView mProgressBar;
    private JSONObject mHealthData;

    private HealthBaseSet baseSet;
    private RefreshHeaderRecyclerView mRecordList;
    private HealthRecordListAdapter mAdapter;
    private String mHealthType;
    private JSONArray healthAdapterList;
    private JSONArray subList = new JSONArray();
    private ArrayList<String> monthTitle;
    private Date searchDate;
    private boolean isTrigger = false;


    private void getHealthListData() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String startTime = "";
        String endTime = "";

        try {
            if (searchDate == null) {
                searchDate = new Date();
                cal.setTime(searchDate);
                int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                endTime = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + maxDay + "T23:59:59Z";
            } else {
                cal.setTime(searchDate);
                cal.add(Calendar.MONTH, -1);
                searchDate = cal.getTime();
                int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                endTime = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + maxDay + "T23:59:59Z";

            }

            cal.setTime(sdf.parse(endTime));
            startTime = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/01T00:00:00Z";
            Log.d("debug", startTime + " ~ " + endTime);
            if ("4".equals(mHealthType) || "6".equals(mHealthType)) {
                new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                        , TaskEnum.MednetQueryMeasureValues, true).execute(mHealthType, startTime, endTime, "true");
            } else {
                new CustomerApiTask(getActivity(), this, false, getString(R.string.msg_data_loading)
                        , TaskEnum.MednetQueryMeasureValues, true).execute(mHealthType, startTime, endTime, "false");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新資料
     */
    public void refreshData() {

        Message message = Message.obtain();
        message.what = FINISH;
        sHandler.sendMessage(message);

        //Log.i("debug", "更新資料requestData ");
//        searchDate = null;
//        healthAdapterList = new JSONArray();
//        getHealthListData();
//        new CustomerApiTask(getActivity(), this, true, getString(R.string.msg_data_loading)
//                , TaskEnum.MednetQueryLastMeasureValue, true).execute();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        monthTitle = new ArrayList<String>();
        mHealthData = ((HealthPushDetailActivity) getActivity()).getHealthData();
        mHealthType = mHealthData.optString("healthType");
        //Log.i("debug", "MegaApplication.healthRecord.optJSONArray(mHealthType) = " + MegaApplication.healthRecord.optJSONArray(mHealthType));
        //Log.i("debug", "MegaApplication.healthRecord = " + MegaApplication.healthRecord);
        healthAdapterList = MegaApplication.healthRecord.optJSONArray(mHealthType);
        getHealthListData();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_health_record_list, container, false);
            mRecordList = rootView.findViewById(R.id.record_list);

            mAdapter = new HealthRecordListAdapter(getActivity(), mHealthType);
            mAdapter.setAdapterList(healthAdapterList);
            mRecordList.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecordList.setAdapter(mAdapter, RefreshViewEnum.healthRecordListRefreshView);
            mRecordList.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshData();
                }
            });

            mRecordList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    }
                    //Log.i("debug", "newState = " + newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    //Log.i("debug", "dy = " + dy);

                    //是否滑到底部 2
                    if (!recyclerView.canScrollVertically(1)) {
                        //相應處理操作
                        //Log.i("debug", "是否滑到底部");
                        if (upCount < 12) {
                            //Log.i("debug", "upNullCount = " + upNullCount + " 加載...");
                            getHealthListData();
                        } else {
                        }


                    }
                    //是否滑到頂部
                    if (!recyclerView.canScrollVertically(-1)) {
                        //相應處理操作
                        //Log.i("debug", "是否滑到頂部");
                    }

                }
            });

        }
        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    ArrayList<String> mDateList = new ArrayList<String>();
    Map<String, Date> mDateMap = new HashMap<String, Date>();
    Map<String, Date> mHelthDataMap = new HashMap<String, Date>();

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        Message message = Message.obtain();
        message.what = FINISH;
        sHandler.sendMessage(message);
        switch (type) {
            case MednetQueryMeasureValues:
                if (upCount == 0)
                    healthAdapterList = new JSONArray();

                upCount++;
                //Log.i("debug", "result = result= " + result);
                if (result == null || result.optInt("state_code") != 200) {
                    reloadViewdate();
                    return;
                }
                JSONArray list = result.optJSONArray("source");

                Calendar cal = Calendar.getInstance();
                Log.i("debug", "原有" + healthAdapterList.length() + "筆 加載" + list.length() + "筆");

                for (int i = list.length() - 1; i >= 0; i--) {
                    JSONObject obj = list.optJSONObject(i);
                    try {
                        obj = MegaApplication.setHealthBasData(obj, mHealthType);
                        Log.i("debug", "mHealthType = " + mHealthType);
                        //飲水跟步行資料要判斷最大值只顯示一筆
                        if ("4".equals(mHealthType)) {
                            Date mTime = CommonFunc.UTC2Date(obj.optString("m_time"));
                            Date updatedAt = CommonFunc.UTC2Date(obj.optString("updated_at"));
                            Log.i("debug", "obj.optString(\"updated_at\") = " + obj.optString("updated_at"));
                            cal.setTime(mTime);
                            String yyyymmdd = cal.get(Calendar.YEAR) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DAY_OF_MONTH);
                            cal.setTime(updatedAt);

                            Calendar cal2 = Calendar.getInstance();
                            if (mDateMap.get(yyyymmdd) != null) {
                                cal2.setTime(mDateMap.get(yyyymmdd));
                                if (cal.compareTo(cal2) == 1) {
                                    mDateMap.put(yyyymmdd, cal.getTime());
                                    healthAdapterList = MegaApplication.replaceHealthItem(healthAdapterList, obj);
                                    continue;
                                } else {
                                    continue;
                                }
                            } else {
                                mDateMap.put(yyyymmdd, cal.getTime());
                            }
                        } else if ("6".equals(mHealthType)) {
                            Date mtime = CommonFunc.UTC2Date(obj.optString("m_time"));
                            cal.setTime(mtime);
                            String yyyymmdd = cal.get(Calendar.YEAR) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DAY_OF_MONTH);

                            //Log.i("debug", "yyyymmdd = " + yyyymmdd);
                            //Log.i("debug", "mDateMap.get(yyyymmdd = " + mDateMap.get(yyyymmdd));

                            if (mDateMap.get(yyyymmdd) != null) {
                                Calendar cal2 = Calendar.getInstance();
                                cal2.setTime(mDateMap.get(yyyymmdd));

                                //Log.i("debug", "cal.compareTo(cal2)= " + cal.compareTo(cal2));
                                if (cal.compareTo(cal2) == 1) {
                                    mDateMap.put(yyyymmdd, cal.getTime());
                                    replaceItem(obj);
                                } else {
                                    continue;
                                }
                            } else {
                                mDateMap.put(yyyymmdd, cal.getTime());
                            }
                        } else {
//                            String id = Integer.toString(obj.optInt("id"));
//                            Date mTime = CommonFunc.UTC2Date(obj.optString("m_time"));
//                            Date updatedAt = CommonFunc.UTC2Date(obj.optString("updated_at"));
//                            cal.setTime(mTime);
//                            String yyyymmdd = cal.get(Calendar.YEAR) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DAY_OF_MONTH);
//                            cal.setTime(updatedAt);
//
//                            Calendar cal2 = Calendar.getInstance();
//                            Log.i("debug", "mHelthDataMap.get(id) = " + mHelthDataMap.get(id));
//
//
//
//
//                            if (mHelthDataMap.get(id) != null) {
//                                cal2.setTime(mHelthDataMap.get(id));
//                                if (cal.compareTo(cal2) == 1) {
//                                    mHelthDataMap.put(id, cal.getTime());
//                                    healthAdapterList = MegaApplication.replaceHealthItemById(healthAdapterList, obj);
//                                    continue;
//                                } else {
//                                    continue;
//                                }
//                            } else {
//                                mHelthDataMap.put(id, cal.getTime());
//                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //Log.i("debug", "obj = " + obj);
                    if (i == (list.length() - 1)) {
                        Date mtime = CommonFunc.UTC2Date(obj.optString("m_time"));
                        cal.setTime(mtime);
                        JSONObject titleObj = new JSONObject();
                        try {
                            titleObj.put("title", cal.get(Calendar.YEAR) + "年" + (cal.get(Calendar.MONTH) + 1) + "月");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        healthAdapterList.put(titleObj);
                    }
                    subList.put(obj);
                    healthAdapterList.put(obj);

                }


                if ("5".equals(mHealthType)) {
                    Log.i("deubg", "e04");
                    if (subList.length() <= 1) {
                        //Log.i("deubg","111");
                        ((HealthPushDetailActivity) getActivity()).setSleepTimeSub(0.0);
                    } else {
                        Double time1 = subList.optJSONObject(0).optDouble("sleeping_hours");
                        Double time2 = subList.optJSONObject(1).optDouble("sleeping_hours");
                        //Log.i("deubg", "2222   " + time2 + " - " + time1 + " = " + (time2 - time1));
                        try {
                            ((HealthPushDetailActivity) getActivity()).setSleepTimeSub(time1 - time2);
                        } catch (Exception e) {
                            Log.e("debug", "e = " + e.getStackTrace());
                        }
                    }

                }


                reloadViewdate();
                break;
        }
    }

    private void replaceItem(JSONObject obj) {
        try {
            Date mtime = CommonFunc.UTC2Date(obj.optString("m_time"));
            Calendar cal = Calendar.getInstance();
            cal.setTime(mtime);

            for (int i = 0; i < healthAdapterList.length(); i++) {
                Calendar cal2 = Calendar.getInstance();
                JSONObject item = healthAdapterList.optJSONObject(i);
                mtime = CommonFunc.UTC2Date(item.optString("m_time"));
                cal2.setTime(mtime);
                if (CommonFunc.isSameDay(cal, cal2)) {
                    healthAdapterList.put(i, obj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 重整畫面資訊
     */
    private void reloadViewdate() {
        Log.i(TAG, "healthAdapterList = " + healthAdapterList + "");
        mAdapter.setAdapterList(healthAdapterList);
        mAdapter.notifyDataSetChanged();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    protected void lazyLoad() {

    }

    @SuppressLint("HandlerLeak")
    private Handler sHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == FINISH) {
                //Toast.makeText(getActivity(), "更新完成！", Toast.LENGTH_SHORT).show();
                mRecordList.refreshComplete();
            }
        }
    };

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        Log.i("debug", "absListView = " + absListView + " scrollState = " + scrollState);
        switch (scrollState) {
            case SCROLL_STATE_IDLE:
                Log.i("debug", "SCROLL_STATE_IDLE");
                getHealthListData();
                break;

        }
    }

    @Override
    public void onScroll(AbsListView absListView, int i, int j, int k) {
        Log.i("debug", "i = " + i + " j = " + j + " k = " + k);

    }
}
