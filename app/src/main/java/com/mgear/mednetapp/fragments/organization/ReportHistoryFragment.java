package com.mgear.mednetapp.fragments.organization;

import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.DownloadFileTask;
import com.mgear.mednetapp.utils.CommonFunc;

import java.io.File;
import java.io.IOException;

/**
 * Created by Jye on 2017/8/10.
 * class: ReportHistoryFragment
 */
public class ReportHistoryFragment extends BaseFragment implements View.OnClickListener, TaskPostExecuteImpl {

    private boolean isPrepared;

    private View rootView;
    private ParcelFileDescriptor mFileDescriptor;
    private PdfRenderer mPdfRenderer;
    private PdfRenderer.Page mCurrentPage;
    private ImageView mImageView;
    private Button mButtonPrevious;
    private Button mButtonNext;
    private int mPageIndex;

    private void openRenderer(String path) throws IOException {
        closeRenderer();
        mFileDescriptor = ParcelFileDescriptor.open(new File(path), ParcelFileDescriptor.MODE_READ_ONLY);
        if (mFileDescriptor != null) {
            mPdfRenderer = new PdfRenderer(mFileDescriptor);
        }
    }

    private void closeRenderer() throws IOException {
        if (mCurrentPage != null) {
            mCurrentPage.close();
            mCurrentPage = null;
        }
        if (mPdfRenderer != null) {
            mPdfRenderer.close();
            mPdfRenderer = null;
        }
        if (mFileDescriptor != null) {
            mFileDescriptor.close();
            mFileDescriptor = null;
        }
    }

    private void showPage(int index) {
        if (mPdfRenderer.getPageCount() <= index)
            return;
        if (null != mCurrentPage)
            mCurrentPage.close();

        mCurrentPage = mPdfRenderer.openPage(index);
        Bitmap bitmap = Bitmap.createBitmap(mCurrentPage.getWidth(), mCurrentPage.getHeight(), Bitmap.Config.ARGB_8888);
        mCurrentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        mImageView.setImageBitmap(bitmap);
        updateUi();
    }

    private void updateUi() {
        int index = mCurrentPage.getIndex();
        int pageCount = mPdfRenderer.getPageCount();
        mButtonPrevious.setEnabled(0 != index);
        mButtonNext.setEnabled(index + 1 < pageCount);
    }

    public void setContent(String url, String savePath) {
        if (isPrepared)
            new DownloadFileTask(getActivity(), this, true, getString(R.string.msg_download_report)).execute(url, savePath);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_report_history, container, false);
            // Retain view references.
            mImageView = (ImageView) rootView.findViewById(R.id.imgReportHistoryRender);
            mButtonPrevious = (Button) rootView.findViewById(R.id.btnReportHistoryPrevious);
            mButtonNext = (Button) rootView.findViewById(R.id.btnReportHistoryNext);
            // Bind events.
            mButtonPrevious.setOnClickListener(this);
            mButtonNext.setOnClickListener(this);
            mPageIndex = 0;
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    public void onStop() {
        try {
            closeRenderer();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReportHistoryPrevious: {
                showPage(mCurrentPage.getIndex() - 1);
                break;
            }
            case R.id.btnReportHistoryNext: {
                showPage(mCurrentPage.getIndex() + 1);
                break;
            }
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        if (type == TaskEnum.DownloadFileTask) {
            String savePath = result.toString();
            if (!savePath.equals("")) {
                try {
                    openRenderer(savePath);
                    showPage(mPageIndex);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("report_history", CommonFunc.getNowSeconds());
    }

}