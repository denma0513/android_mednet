package com.mgear.mednetapp.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.linecorp.linesdk.LoginDelegate;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.api.LineApiClientBuilder;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.auth.LineLoginApi;
import com.linecorp.linesdk.auth.LineLoginResult;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.YahooLoginActivity;
import com.mgear.mednetapp.entity.OauthSet;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.INativeActionImpl;
import com.mgear.mednetapp.interfaces.OnOauth2SignInListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Oauth2LoginFragment extends MegaBaseFragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {


    private INativeActionImpl mINativeActionImpl;
    //常數
    private static int FACEBOOK_SIGN_IN = 1;
    private static int GOOGLE_SIGN_IN = 2;
    private static int YAHOO_SIGN_IN = 3;
    private static int LINE_SIGN_IN = 4;
    private static String TAG = "SIGNIN";

    public static final int RESULT_CANCELED = 0;
    public static final int RESULT_FIRST_USER = 1;
    public static final int RESULT_OK = -1;

    //畫面變數
    private View rootView;
    private ImageView mGoogleLogin, mFbLogin, mYahooLogin, mLineLogin;

    //API SDK 變數
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleApiClient mGoogleApiClient;
    private LoginManager loginManager;
    private CallbackManager callbackManager;
    private LoginDelegate loginDelegate = LoginDelegate.Factory.create();
    private static LineApiClient lineApiClient;
    private OnOauth2SignInListener mOnOauth2SignInListener;


    private FirebaseAuth mAuth;
    private GoogleSignInOptions googleSignInOptions;


    //執行google登入的activity
    private void googleSignIn() {

        Intent intent = mGoogleSignInClient.getSignInIntent();
//        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, GOOGLE_SIGN_IN);
    }

    //執行yahoo登入的activity
    private void yahooSignIn() {
        Intent intent = new Intent(getActivity(), YahooLoginActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("test", "test");
        intent.putExtras(bundle);
        startActivityForResult(intent, YAHOO_SIGN_IN);
    }

    //執行line登入的activity
    private void lineSignIn(View view) {
        Intent loginIntent = LineLoginApi.getLoginIntent(
                view.getContext(),
                getString(R.string.line_channel_id),
                new LineAuthenticationParams.Builder()
                        .scopes(Arrays.asList(Scope.PROFILE))
                        .build());
        startActivityForResult(loginIntent, LINE_SIGN_IN);

    }

    //執行facebook登入的activity
    private void facebookSignIn() {
        Log.d(TAG, "facebookSignIn");
        // 設定FB login的顯示方式 ; 預設是：NATIVE_WITH_FALLBACK
        /**
         * 1. NATIVE_WITH_FALLBACK
         * 2. NATIVE_ONLY
         * 3. KATANA_ONLY
         * 4. WEB_ONLY
         * 5. WEB_VIEW_ONLY
         * 6. DEVICE_AUTH
         */
        loginManager.logOut();
        loginManager.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
        // 設定要跟用戶取得的權限，以下3個是基本可以取得，不需要經過FB的審核
        List<String> permissions = new ArrayList<>();
        permissions.add("public_profile");
        permissions.add("email");
        //permissions.add("user_friends");
        permissions.add("user_gender");
        permissions.add("user_birthday");
        permissions.add("user_link");
        permissions.add("user_photos");

        // 設定要讀取的權限
        loginManager.logInWithReadPermissions(this, permissions);

        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                /*
                    登入成功
                    可以取得相關資訊，這裡就請各位自行打印出來
                    透過GraphRequest來取得用戶的Facebook資訊
                 */
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            if (response.getConnection().getResponseCode() == 200) {
                                Log.i(TAG, "onCompleted: object = " + object);
                                String id = object.getString("id");
                                String name = object.getString("name");
                                String email = object.getString("email");

//                                Log.d(TAG, "Facebook object:" + object);
//                                Log.d(TAG, "Facebook id:" + id);
//                                Log.d(TAG, "Facebook name:" + name);
//                                Log.d(TAG, "Facebook email:" + email);
                                // 此時如果登入成功，就可以順便取得用戶大頭照
                                //Profile profile = Profile.getCurrentProfile();
                                // 設定大頭照大小
                                //Uri userPhoto = null;
                                //if (profile != null) {
                                //  userPhoto = profile.getProfilePictureUri(300, 300);
                                //}
                                Log.i(TAG, "onCompleted: " + email);

                                OauthSet oauthSet = new OauthSet();
                                oauthSet.setAccessToken(loginResult.getAccessToken().getToken()).setType(FACEBOOK_SIGN_IN).setEmail(email);
                                if (name != null) {
                                    if (name.indexOf(" ") >= 0) {
                                        String[] nameArr = name.split(" ");
                                        oauthSet.setName(nameArr[0]).setLastName(nameArr[1]);
                                    } else {
                                        oauthSet.setName(name);
                                    }
                                }
                                onSignIn(oauthSet);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                // https://developers.facebook.com/docs/android/graph?locale=zh_TW
                // 如果要取得email，需透過添加參數的方式來獲取(如下)
                // 不添加只能取得id & name
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                // 用戶取消
                Log.d(TAG, "Facebook onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                // 登入失敗
                Log.d(TAG, "Facebook onError:" + error.toString());
            }
        });
    }

//    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
//        Log.d(TAG, "Google User Id :" + acct.getId());
//        mAuth = FirebaseAuth.getInstance();
//
//        // --------------------------------- //
//        // BELOW LINE GIVES YOU JSON WEB TOKEN, (USED TO GET ACCESS TOKEN) :
//        Log.d(TAG, "Google JWT : " + acct.getIdToken());
//        // --------------------------------- //
//
//        // Save this JWT in global String :
//        String idTokenString = acct.getIdToken();
//
//        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
//        mAuth.signInWithCredential(credential)
//                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
//
//                        if(task.isSuccessful()){
//                            // --------------------------------- //
//                            // BELOW LINE GIVES YOU FIREBASE TOKEN ID :
//                            FirebaseUser user = mAuth.getCurrentUser();
//
//                            Log.d(TAG, "Firebase User Access Token : " + task.getResult());
//                            //Log.d(TAG, "Firebase User Access Token : " + task.getResult().getToken());
//                            // --------------------------------- //
//                        }
//                        // If sign in fails, display a message to the user. If sign in succeeds
//                        // the auth state listener will be notified and logic to handle the
//                        // signed in user can be handled in the listener.
//                        else {
//                            Log.w(TAG, "signInWithCredential", task.getException());
//                        }
//                    }
//                });
//    }


    public void setOnOauth2SignInListener(OnOauth2SignInListener onOauth2SignInListener) {
        mOnOauth2SignInListener = onOauth2SignInListener;
    }

    private void onSignIn(OauthSet onOauthSet) {
        mOnOauth2SignInListener.OnSignIn(onOauthSet);
    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // init google
        String server_client_id = getString(R.string.google_client_id);
        Log.i(TAG, server_client_id);


        googleSignInOptions = new GoogleSignInOptions.Builder()
                .requestIdToken(server_client_id)
                .requestServerAuthCode(server_client_id)
                .requestPhatIdToken(server_client_id)
                .requestEmail().build();

//        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
//                //.enableAutoManage((FragmentActivity) getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
//                .build();
//        mGoogleApiClient.maybeSignOut();


        // init facebook
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        // init LoginManager & CallbackManager
        loginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();

        // init line
        LineApiClientBuilder apiClientBuilder = new LineApiClientBuilder(getActivity().getApplicationContext(), getString(R.string.line_channel_id));
        lineApiClient = apiClientBuilder.build();


        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), googleSignInOptions);
//        mGoogleSignInClient.signOut();
    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.oauth2_login_button, container, false);
            mGoogleLogin = rootView.findViewById(R.id.google_login_button);
            mFbLogin = rootView.findViewById(R.id.fb_login_button);
            mYahooLogin = rootView.findViewById(R.id.yahoo_login_button);
            mLineLogin = rootView.findViewById(R.id.line_login_button);
            mGoogleLogin.setOnClickListener(this);
            mFbLogin.setOnClickListener(this);
            mYahooLogin.setOnClickListener(this);
            mLineLogin.setOnClickListener(this);
        }
        return rootView;
    }


    //回接activity的結果
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (resultCode != RESULT_OK) {
            mOnOauth2SignInListener.OnSignIn(null);
            return;
        }
        if (requestCode == GOOGLE_SIGN_IN) {
            try {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                Log.i(TAG, "task " + task.getResult());
                GoogleSignInAccount account = task.getResult(ApiException.class);
                //取得account 需要在乎叫GoogleAuthUtil.getToken 來解析accesstoken
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Looper.prepare();
                            String scope = "oauth2:" + Scopes.EMAIL + " " + Scopes.PROFILE;
                            String accessToken = GoogleAuthUtil.getToken(getActivity(), account.getAccount(), scope, new Bundle());
                            String mail = account.getEmail();
                            String firstName = account.getGivenName();
                            String lastName = account.getFamilyName();
                            Log.d(TAG, "DisplayName:" + account.getDisplayName()); //accessToken:ya29.Gl...
                            Log.d(TAG, "GivenName:" + account.getGivenName());
                            Log.d(TAG, "FamilyName:" + account.getFamilyName());

                            OauthSet oauthSet = new OauthSet();
                            oauthSet.setAccessToken(accessToken).setType(GOOGLE_SIGN_IN).setEmail(mail).setName(firstName).setLastName(lastName);
                            onSignIn(oauthSet);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (GoogleAuthException e) {
                            e.printStackTrace();
                        }
                    }
                };
                AsyncTask.execute(runnable);

            } catch (Exception e) {
                Log.w(TAG, "signInResult:failed code=" + e.getMessage());
                e.printStackTrace();
            }
        } else if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            Log.d(TAG, "facebook sign in  onActivityResult" + requestCode + " resultCode = " + resultCode);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == YAHOO_SIGN_IN) {
            try {
                Bundle bundle = data.getExtras();
                String accessToken = bundle.getString("accessToken");
                String refreshToken = bundle.getString("refreshToken");
                String gender = bundle.getString("gender");
                String birthday = bundle.getString("birthday");
                String email = bundle.getString("mail");
                String lastName = bundle.getString("lastName");
                String firstName = bundle.getString("firstName");

                Log.d(TAG, "firstName:" + firstName);
                Log.d(TAG, "lastName:" + lastName);
                Log.d(TAG, "birthday:" + birthday);
                Log.d(TAG, "gender:" + gender);


                OauthSet oauthSet = new OauthSet();
                oauthSet.setAccessToken(accessToken).setRefreshToken(refreshToken)
                        .setType(YAHOO_SIGN_IN).setEmail(email)
                        .setBirthday(birthday).setGender(gender)
                        .setName(firstName).setLastName(lastName);

                onSignIn(oauthSet);
            } catch (Exception e) {
                Log.w(TAG, "signInResult:failed code=" + e.getMessage());
                e.printStackTrace();
            }

        } else if (requestCode == LINE_SIGN_IN) {
            LineLoginResult result = LineLoginApi.getLoginResultFromIntent(data);
            switch (result.getResponseCode()) {
                case SUCCESS:
                    // Login successful
                    String accessToken = result.getLineCredential().getAccessToken().getTokenString();
                    Log.d("debug", "result.getLineCredential().getAccessToken(): " + result.getLineCredential().getAccessToken());

                    //Log.d("debug", "result.getLineCredential().getAccessToken(): "+result.getLineCredential().getAccessToken().getTokenString());

                    String mail = "";
                    String firstName = "";
                    String lastName = "";
                    String birthday = "";
                    if (result.getLineIdToken() != null) {
                        mail = result.getLineIdToken().getEmail();
                        lastName = result.getLineIdToken().getFamilyName();
                        firstName = result.getLineIdToken().getGivenName();
                        birthday = result.getLineIdToken().getBirthdate();
                        Log.d("debug", "mail : " + mail);
                        Log.d("debug", "lastName : " + lastName);
                        Log.d("debug", "firstName : " + firstName);
                        Log.d("debug", "birthday : " + birthday);
                    }

                    OauthSet oauthSet = new OauthSet();
                    oauthSet.setAccessToken(accessToken).setType(LINE_SIGN_IN).setEmail(mail).setName(firstName).setLastName(lastName).setBirthday(birthday);
                    onSignIn(oauthSet);

                    Log.i(TAG, accessToken);
                    break;
                case CANCEL:
                    // Login canceled by user
                    Log.e("ERROR", "LINE Login Canceled by user.");
                    break;

                default:
                    // Login canceled due to other error
                    //Log.e("ERROR", "Login FAILED!");
                    Log.e("ERROR", result.getErrorData().toString());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.google_login_button:
                googleSignIn();
                break;
            case R.id.fb_login_button:
                facebookSignIn();
                break;
            case R.id.yahoo_login_button:
                yahooSignIn();
                break;
            case R.id.line_login_button:
                lineSignIn(v);
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
