package com.mgear.mednetapp.fragments.organization;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Dennis.
 * class: TutorialStartAppFragment
 */
public class TutorialStartAppFragment extends Fragment {

    private View rootView;
    private RelativeLayout root;
    private View nextButton, backButton;
    private ImageView imgTutorial;
    private Button btnTutorialNext;
    private int page;
    private BitmapDrawable bgImg, img;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_tutorial_startapp, container, false);
            root = (RelativeLayout) rootView.findViewById(R.id.rootTutorialStartApp);
            nextButton = (View) rootView.findViewById(R.id.nextButton);
            backButton = (View) rootView.findViewById(R.id.backButton);
            btnTutorialNext = (Button) rootView.findViewById(R.id.btnTutorialNext);
            imgTutorial = rootView.findViewById(R.id.imgTutorial);

            nextButton.setVisibility(View.VISIBLE);
            backButton.setVisibility(View.VISIBLE);
            btnTutorialNext.setVisibility(View.VISIBLE);

            bgImg = new BitmapDrawable(getResources(), CommonFunc.readBitmapHalf(getActivity(), getBgImg()));
            img = new BitmapDrawable(getResources(), CommonFunc.readBitmapHalf(getActivity(), getImg()));
            imgTutorial.setImageDrawable(img);
            root.setBackground(bgImg);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getImg() {
        BitmapDrawable img;
        switch (this.page) {
            case 0:
                return R.drawable.tutorial_startapp_object1;
            case 1:
                return R.drawable.tutorial_startapp_object2;
            case 2:
                return R.drawable.tutorial_startapp_object3;
            case 3:
                return R.drawable.tutorial_startapp_object4;
            case 4:
                return R.drawable.tutorial_startapp_object5;
            default:
                break;
        }


        return 0;
    }

    public int getBgImg() {
        switch (this.page) {
            case 0:
                backButton.setVisibility(View.INVISIBLE);
                nextButton.setVisibility(View.VISIBLE);
                btnTutorialNext.setVisibility(View.INVISIBLE);
                return R.drawable.tutorial_startapp1;
            case 1:
                backButton.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                btnTutorialNext.setVisibility(View.VISIBLE);
                return R.drawable.tutorial_startapp1;
            case 2:
                btnTutorialNext.setVisibility(View.INVISIBLE);
                return R.drawable.tutorial_startapp3;
            case 3:
                btnTutorialNext.setVisibility(View.INVISIBLE);
                return R.drawable.tutorial_startapp4;
            case 4:
                nextButton.setVisibility(View.INVISIBLE);
                return R.drawable.tutorial_startapp5;
            default:
                break;
        }


        return 0;
    }
}