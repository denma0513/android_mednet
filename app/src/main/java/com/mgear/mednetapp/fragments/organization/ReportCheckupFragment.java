package com.mgear.mednetapp.fragments.organization;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.SubListAdapter;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.utils.CommonFunc;

/**
 * Created by Jye on 2017/8/21.
 * class: ReportCheckupResultFragment
 */
public class ReportCheckupFragment extends BaseFragment {

    private boolean isPrepared;

    private View rootView;
    private ListView lstCheckup;

    public void setContent(StringStringSet[] content) {
        if (isPrepared) {
            lstCheckup.setAdapter(new SubListAdapter(getActivity(), R.layout.item_sub_list, content));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_report_checkup, container, false);
            lstCheckup = (ListView) rootView.findViewById(R.id.lstReportCheckup);
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("report_checkup_result", CommonFunc.getNowSeconds());
    }
}