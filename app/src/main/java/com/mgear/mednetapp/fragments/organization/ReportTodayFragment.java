package com.mgear.mednetapp.fragments.organization;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.entity.organization.CheckupResult;
import com.mgear.mednetapp.entity.organization.CheckupResultSet;
import com.mgear.mednetapp.entity.organization.HealthySystem;
import com.mgear.mednetapp.entity.organization.HealthySystemSet;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.organization.CustomerCheckupResultTask;
import com.mgear.mednetapp.utils.CommonFunc;
import com.qozix.tileview.TileView;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Jye on 2017/8/10.
 * class: ReportTodayFragment
 */
public class ReportTodayFragment extends BaseFragment implements View.OnClickListener, TaskPostExecuteImpl {

    private boolean isPrepared;
    private boolean hasLoadedOnce;

    private View rootView;
    private TileView tvSystem;
    private CheckupResultSet resultSet;

    private void loadCheckupResult() {
        if (resultSet != null && resultSet.size() > 0) {
            Context context = getActivity();
            //健檢系統圖示
            Drawable drawableGreen = new BitmapDrawable(getResources(), CommonFunc.readBitmap(context, R.drawable.button_green_ball));
            Drawable drawableRed = new BitmapDrawable(getResources(), CommonFunc.readBitmap(context, R.drawable.button_red_ball));
            //取得系統清單
            HealthySystemSet systemSet = CustomerActivity.getHealthySystemList();
            for (Map.Entry<UUID, ArrayList<CheckupResult>> entry : resultSet.entrySet()) {
                HealthySystem hs = systemSet.get(entry.getKey());
                ImageView iv = new ImageView(context);
                iv.setTag(hs);
                iv.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
                iv.setOnClickListener(this);
                //取的結果是否正常
                Boolean normal = resultSet.getCheckupResult(hs.getId());
                iv.setImageDrawable(normal == null || normal ? drawableGreen : drawableRed);
                tvSystem.addMarker(iv, hs.getPosLeft(), hs.getPosTop(), -0.5f, -1.0f);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_report_today, container, false);
            tvSystem = (TileView) rootView.findViewById(R.id.tvReportToday);
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        if (type == TaskEnum.CustomerCheckupResultTask) {
            resultSet = (CheckupResultSet) result;
            hasLoadedOnce = true;
            loadCheckupResult();
        }
    }

    @Override
    public void onClick(View v) {
        //取得點選的系統
        HealthySystem hs = (HealthySystem) v.getTag();
        //取得診斷值的顯示字串
        Locale locale = Locale.getDefault();
        String diagnostic = getString(R.string.field_diagnosis_value);
        //取得報告內容
        ArrayList<CheckupResult> results = resultSet.get(hs.getId());
        StringStringSet[] content = new StringStringSet[results.size()];
        for (int i = 0; i < results.size(); i++) {
            CheckupResult cr = results.get(i);
            content[i] = new StringStringSet(cr.getProductName(), String.format(locale, diagnostic, cr.getDiagnosisValue()), null, cr.getResult());
        }
        HomeFragment homeFragment = (HomeFragment)getParentFragment();
        homeFragment.navigationPage(hs.getName(), 12);
        homeFragment.setReportCheckupContent(content);
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("report_today", CommonFunc.getNowSeconds());

        //判斷是否加載過資料
        if (!hasLoadedOnce) {
            //判斷客戶的性別
            tvSystem.setSize(1440, 1600);
            if (CustomerActivity.getCustomer().getGender() == 'M') {
                tvSystem.addDetailLevel(0.500f, "system/tile/man/500/%d_%d.jpg", 256, 256);
                tvSystem.addDetailLevel(1.000f, "system/tile/man/1000/%d_%d.jpg", 256, 256);
            }
            else {
                tvSystem.addDetailLevel(0.500f, "system/tile/woman/500/%d_%d.jpg", 256, 256);
                tvSystem.addDetailLevel(1.000f, "system/tile/woman/1000/%d_%d.jpg", 256, 256);
            }
            tvSystem.setDrawingCacheEnabled(true);
            tvSystem.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.white));

            //取得今日的報告項目內容
            new CustomerCheckupResultTask(getActivity(), this, true, getString(R.string.msg_get_today_result))
                    .execute(CultureSelectActivity.BluetoothAddress, String.valueOf(CustomerActivity.getCustomer().getContactCode()));
        }

        //設定顯示比率
        tvSystem.setScale(0.500f);
    }

}