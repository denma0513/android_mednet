package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.BaseActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.helpers.DataHelper;
import com.mgear.mednetapp.interfaces.organization.WebPreviousPageImpl;
import com.mgear.mednetapp.utils.WebViewUtil;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Jye on 2017/8/23.
 * class: WebViewFragment
 */
public class WebView2Fragment extends MegaBaseFragment implements WebPreviousPageImpl {

    private float textZoom = 1.0f;

    private String strTitle;
    private View rootView;
    private String strUrl;
    private RelativeLayout mWebviewLayout;
    private WebView mWebView;
    private boolean hadIntercept;
    private int mRootView;
    private Date endDate, curDate;
    private DataHelper mDataHelper;
    private Context mContext;
    private ProgressBar progressBar;


    @Override
    protected void lazyLoad() {

    }

    public boolean canGoBack() {
        if (mWebView != null)
            return mWebView.canGoBack();
        else
            return false;
    }

    public void goBack() {
        mWebView.goBack();
    }

    private class MednetWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.i("debug", "onPageStarted 網頁 開始呼叫 = " + url);
            curDate = new Date(System.currentTimeMillis());
        }

        public void onLoadResource(WebView view, String url) {
            //Log.e("debug", "onLoadResource : " + url);
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            //Log.d("debug", "shouldInterceptRequest1: url = " + url);
            if (mDataHelper.hasLocalResource(url)) {
                //Log.d("debug", "shouldInterceptRequest1: 资源命中");
                WebResourceResponse response =
                        mDataHelper.getReplacedWebResourceResponse(mContext,
                                url);
                if (response != null) {
                    return response;
                }
            }
            return super.shouldInterceptRequest(view, url);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            //Log.d("debug", "shouldInterceptRequest2: url = " + url);
            if (mDataHelper.hasLocalResource(url)) {
                //Log.d("debug", "shouldInterceptRequest2: 资源命中");
                WebResourceResponse response = mDataHelper.getReplacedWebResourceResponse(mContext, url);
                if (response != null) {
                    return response;
                }
            }
            return super.shouldInterceptRequest(view, request);
        }


        // 设置不用系统浏览器打开,直接显示在当前Webview
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //Log.d("debug", "shouldOverrideUrlLoading1: url = " + url);
            view.loadUrl(url);
            return true;
        }

        // 该方法在5.0版本上可使用
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            //Log.d("debug", "shouldOverrideUrlLoading1: url = " + request.getUrl());
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //Log.i("debug", "onPageFinished = " + url);
            for (String needLoginUrl : MegaApplication.needLoginUrlArray) {
                if (strUrl.contains(needLoginUrl)) {//需要登入的url
                    if (url.equals("https://med-net.com/")) {
                        Log.e("debug", "醫聯網 該回去登入囉");
                        JSONObject loginTarget = new JSONObject();

                        try {
                            loginTarget.put("action", MegaApplication.ActionTypeWebView);
                            loginTarget.put("target", strUrl);
                            ((BaseActivity) mContext).triggerAction(mContext, MegaApplication.ActionTypeLogin, loginTarget.toString(), mRootView);
                            //triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (url.contains("sso2.med-net.com")) {
                        Log.e("debug", "醫師諮詢 該回去登入囉");
                        try {
                            JSONObject loginTarget = new JSONObject();
                            loginTarget.put("action", MegaApplication.ActionTypeWebView);
                            loginTarget.put("target", strUrl);
                            ((BaseActivity) mContext).triggerAction(mContext, MegaApplication.ActionTypeLogin, loginTarget.toString(), mRootView);
                            //triggerAction(context, MegaApplication.ActionTypeLogin, loginTarget.toString(), view);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
            view.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            endDate = new Date(System.currentTimeMillis());
            try {
                long diff = endDate.getTime() - curDate.getTime();
                Log.e("debug", "onPageFinished" + "  呼叫時間: " + diff);
            } catch (Exception e) {
                e.printStackTrace();
            }


            //if (view.canGoBack())
            //  ((MainActivity) getActivity()).setPreviousPage(WebViewFragment.this);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            Log.i("debug", "error1 = " + errorCode + " " + description);
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.stopLoading();
            Log.i("debug", "error2 = " + error.getErrorCode() + " " + error.getDescription());
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
        }


//        @Override
//        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            //全部擋掉
//            handler.cancel();
//        }


    }

    public void setUrl(String title, String url) {
        strTitle = title;
        strUrl = url;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void setRootView(int rootView) {
        mRootView = rootView;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        //MainActivity.addCustomerTrack(strTitle, CommonFunc.getNowSeconds());
        if (rootView == null) {
            // Inflate the layout for this fragment

            try {
                rootView = inflater.inflate(R.layout.fragment_web_view2, container, false);
                mWebviewLayout = rootView.findViewById(R.id.webview_layout);
                mWebView = new WebView(mContext);
                mDataHelper = new DataHelper();
                mWebviewLayout.addView(mWebView);
                progressBar = (ProgressBar) rootView.findViewById(R.id.pbBrowserWait);
                // Enable Javascript
                WebViewUtil.configWebView(mWebView);
                mWebView.setWebViewClient(new WebView2Fragment.MednetWebViewClient());
                //progressBar.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //載入頁面內容
        if (mWebView != null)
            mWebView.loadUrl(strUrl);
    }

    @Override
    public void previousPage() {
        if (mWebView != null)
            if (mWebView.canGoBack())
                mWebView.goBack();
    }

    @Override
    public boolean hasPreviousPage() {
        return mWebView.canGoBack();
    }

    @Override
    public boolean onBackPressed() {
        if (mWebView != null) {
            Log.i("debug", "mWebView.canGoBack() = " + mWebView.canGoBack());
            if (mWebView.canGoBack()) {
                mWebView.goBack();
                return true;
            }
        }
        return false;
    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d("Fragment 1", "onStop");
    }


    @Override
    public void onDestroy() {

        if (mWebView != null) {
            mWebView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            mWebView.clearHistory();
            ((ViewGroup) mWebView.getParent()).removeView(mWebView);
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
        Log.d("Fragment 1", "onDestroy");
    }


}