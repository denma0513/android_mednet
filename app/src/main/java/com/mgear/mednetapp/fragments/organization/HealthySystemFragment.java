package com.mgear.mednetapp.fragments.organization;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.entity.organization.Encyclopedia;
import com.mgear.mednetapp.entity.organization.EncyclopediaSet;
import com.mgear.mednetapp.entity.organization.HealthySystem;
import com.mgear.mednetapp.entity.organization.HealthySystemSet;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.utils.CommonFunc;
import com.qozix.tileview.TileView;

import java.util.ArrayList;

/**
 * Created by Jye on 2017/8/9.
 * class: HealthySystemFragment
 */
public class HealthySystemFragment extends BaseFragment implements View.OnClickListener {

    private boolean isPrepared;
    private boolean hasLoadedOnce;

    private View rootView;
    private TileView tvSystem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("healthy_system", CommonFunc.getNowSeconds());
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_healthy_system, container, false);
            TextView txtTitle = (TextView) rootView.findViewById(R.id.txtHealthySystemTitle);
            txtTitle.setText(CommonFunc.fromHtml(getString(R.string.content_healthy_system)));
            tvSystem = (TileView) rootView.findViewById(R.id.tvHealthySystem);
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    public void onClick(View v) {
        //取得點選的系統
        HealthySystem hs = (HealthySystem) v.getTag();
        //取得小百科清單
        final EncyclopediaSet set = CustomerActivity.getEncyclopediaList();
        ArrayList<Encyclopedia> values = set.get(hs.getId());
        if (values != null && values.size() > 0) {
            StringStringSet[] content = new StringStringSet[values.size()];
            for (int i = 0; i < values.size(); i++) {
                Encyclopedia encyclopedia = values.get(i);
                content[i] = new StringStringSet(encyclopedia.getQuestion(), encyclopedia.getAnswerText());
            }
            HomeFragment homeFragment = (HomeFragment) getParentFragment();
            homeFragment.navigationPage(hs.getName(), 9);
            homeFragment.setHealthyQuestionContent(content);
        }
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }

        //判斷是否加載過資料
        if (!hasLoadedOnce) {
            Context context = getActivity();
            //判斷客戶的性別
            tvSystem.setSize(1440, 1600);
            if (CustomerActivity.getCustomer().getGender() == 'M') {
                tvSystem.addDetailLevel(0.500f, "system/tile/man/500/%d_%d.jpg", 256, 256);
                tvSystem.addDetailLevel(1.000f, "system/tile/man/1000/%d_%d.jpg", 256, 256);
            } else {
                tvSystem.addDetailLevel(0.500f, "system/tile/woman/500/%d_%d.jpg", 256, 256);
                tvSystem.addDetailLevel(1.000f, "system/tile/woman/1000/%d_%d.jpg", 256, 256);
            }
            tvSystem.setDrawingCacheEnabled(true);
            tvSystem.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));

            //健檢系統圖示
            Drawable drawable = new BitmapDrawable(getResources(), CommonFunc.readBitmap(context, R.drawable.button_green_ball));
            //取得健檢系統清單
            HealthySystemSet healthySystemSet = CustomerActivity.getHealthySystemList();

            //沒資料暫時先拿掉
            if (healthySystemSet != null) {
                for (HealthySystem hs : healthySystemSet.values()) {
                    ImageView iv = new ImageView(context);
                    iv.setTag(hs);
                    iv.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
                    iv.setOnClickListener(this);
                    iv.setImageDrawable(drawable);
                    tvSystem.addMarker(iv, hs.getPosLeft(), hs.getPosTop(), -0.5f, -1.0f);
                }
                hasLoadedOnce = true;
            }
        }

        //設定顯示比率
        tvSystem.setScale(0.500f);
    }

}