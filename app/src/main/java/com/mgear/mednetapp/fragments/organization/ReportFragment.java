package com.mgear.mednetapp.fragments.organization;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.OrgMainActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.SubListAdapter;
import com.mgear.mednetapp.fragments.base.BaseFragment;
import com.mgear.mednetapp.entity.organization.Customer;
import com.mgear.mednetapp.entity.organization.CustomerReport;
import com.mgear.mednetapp.entity.organization.CustomerReportSet;
import com.mgear.mednetapp.entity.organization.StringStringSet;
import com.mgear.mednetapp.utils.CommonFunc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Jye on 2017/8/10.
 * class: ReportFragment
 */
public class ReportFragment extends BaseFragment implements AdapterView.OnItemClickListener, View.OnTouchListener {

    private boolean isPrepared;

    private View rootView;
    private CustomerReportSet mReportList;
    private ListView lstToday;
    private ListView lstHistory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_report, container, false);
            lstToday = (ListView) rootView.findViewById(R.id.lstReportToday);
            lstHistory = (ListView) rootView.findViewById(R.id.lstReportHistory);
        }
        isPrepared = true;
        lazyLoad();
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HomeFragment homeFragment = (HomeFragment)getParentFragment();
        switch (parent.getId()) {
            case R.id.lstReportToday:
                homeFragment.navigationPage(getString(R.string.title_today_report), 10);
            break;
            case R.id.lstReportHistory:
                homeFragment.navigationPage(getString(R.string.title_history_report), 11);
                //設定PDF報告的連結
                CustomerReport report = mReportList.get(position);
                homeFragment.setReportHistoryContent("http://192.168.0.233/GDCRM/Prog/CheckupReportData/ws_DownloadReport_S.asp?ID=" + report.getDocumentCode());
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Disallow the touch request for parent scroll on touch of child view
        v.getParent().requestDisallowInterceptTouchEvent(true);
        return false;
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
        if (!isPrepared || !isVisible) {
            return;
        }
        //紀錄客戶軌跡
        OrgMainActivity.addCustomerTrack("report", CommonFunc.getNowSeconds());

        //取得登入的客戶資料
        final Customer customer = CustomerActivity.getCustomer();
        //取得今日的報告
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        StringStringSet[] todayContent = new StringStringSet[1];
        todayContent[0] = new StringStringSet(customer.getProductName(), sdf.format(new Date()));
        SubListAdapter adapterToday = new SubListAdapter(getActivity(), todayContent);
        lstToday.setOnItemClickListener(this);
        lstToday.setOnTouchListener(this);
        lstToday.setAdapter(adapterToday);
        //取得歷史的報告
        mReportList = customer.getReportList();
        StringStringSet[] historyContent = new StringStringSet[mReportList.size()];
        for (int i = 0; i < mReportList.size(); i++) {
            CustomerReport report = mReportList.get(i);
            historyContent[i] = new StringStringSet(report.getProductName(), sdf.format(report.getEventDate()));
        }
        SubListAdapter adapterHistory = new SubListAdapter(getActivity(), historyContent);
        lstHistory.setOnItemClickListener(this);
        lstHistory.setOnTouchListener(this);
        lstHistory.setAdapter(adapterHistory);
        CommonFunc.setListViewHeightBasedOnChildren(lstHistory);
    }

}