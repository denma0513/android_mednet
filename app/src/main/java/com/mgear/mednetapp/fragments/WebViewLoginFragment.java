package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.organization.WebPreviousPageImpl;

public class WebViewLoginFragment extends MegaBaseFragment implements WebPreviousPageImpl {

    private float textZoom = 1.0f;

    private String strTitle;
    private View rootView;
    private String strUrl;
    private WebView mWebView;
    private TextView txtMessage;
    private ProgressBar progressBar;
    private boolean hadIntercept;

    public void setTextZoom(float textZoom) {
        this.textZoom = textZoom;
    }

    @Override
    protected void lazyLoad() {

    }

    private class MednetWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i("debug", "shouldOverrideUrlLoading = " + url);

            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Log.i("debug", "shouldOverrideUrlLoading = " + request.getUrl().toString());
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.i("debug", "onPageFinished = " + url);
            txtMessage.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
            //if (view.canGoBack())
            //  ((MainActivity) getActivity()).setPreviousPage(WebViewFragment.this);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }

        //避掉SSL Error Handler
//        @Override
//        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            //全部擋掉
//            handler.cancel();
//        }


    }

    public void setUrl(String title, String url) {
        strTitle = title;
        strUrl = url;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //紀錄客戶軌跡
        //MainActivity.addCustomerTrack(strTitle, CommonFunc.getNowSeconds());
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_web_view, container, false);
            progressBar = (ProgressBar) rootView.findViewById(R.id.pbBrowserWait);
            txtMessage = (TextView) rootView.findViewById(R.id.txtBrowserMessage);
            mWebView = (WebView) rootView.findViewById(R.id.webViewBrowser);

            // Enable Javascript
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setUseWideViewPort(true);
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setDomStorageEnabled(true);
            if (textZoom != 1.0f) {
                webSettings.setTextZoom((int) (webSettings.getTextZoom() * textZoom));
            }
            // Force links and redirects to open in the WebView instead of in a browser
            mWebView.setWebViewClient(new WebViewLoginFragment.MednetWebViewClient());
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //載入頁面內容
        mWebView.loadUrl(strUrl);
    }

    @Override
    public void previousPage() {
        if (mWebView.canGoBack())
            mWebView.goBack();
    }

    @Override
    public boolean hasPreviousPage() {
        return mWebView.canGoBack();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}