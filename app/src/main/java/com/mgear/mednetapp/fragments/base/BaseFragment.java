package com.mgear.mednetapp.fragments.base;

import android.app.Fragment;

import com.mgear.mednetapp.interfaces.IBackHandleIpml;

/**
 * Created by Jye on 2017/8/21.
 * class: BaseFragment
 */
public abstract class BaseFragment extends Fragment {
    protected boolean isVisible;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }

    }

    protected void onVisible() {
        lazyLoad();
    }

    protected void onInvisible() {
    }

    protected abstract void lazyLoad();

}