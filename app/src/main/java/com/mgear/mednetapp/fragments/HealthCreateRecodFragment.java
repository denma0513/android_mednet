package com.mgear.mednetapp.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgear.mednetapp.HealthPushActivity;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.adapter.HealthDetailTabAdapter;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;

import org.json.JSONObject;


/**
 * Created by Dennis
 * class: HomePageFragment
 */
public class HealthCreateRecodFragment extends MegaBaseFragment implements TaskPost2ExecuteImpl, View.OnClickListener,
        ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {
    //靜態
    private static String TAG = "HealthDetail";
    private static final int FINISH = 1;

    //畫面參數
    private View rootView;
    private TabLayout mTebView;
    private ViewPager mViewPager;
    private HealthDetailTabAdapter myAdapter;


    //邏輯參數
    private JSONObject mHealthData;


    public void setHealthData(JSONObject healthData) {
        mHealthData = healthData;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            try {
                rootView = inflater.inflate(R.layout.fragment_healthdetail_view, container, false);
                mTebView = rootView.findViewById(R.id.tab_view);
                mViewPager = rootView.findViewById(R.id.view_pager);
                myAdapter = new HealthDetailTabAdapter(getActivity(), getFragmentManager());
                mViewPager.setAdapter(myAdapter);

                //記錄應用程式事件
                mViewPager.addOnPageChangeListener(this);
                mTebView.addOnTabSelectedListener(this);
                mTebView.setupWithViewPager(mViewPager);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //lazyLoad();
        return rootView;
    }

    @Override
    protected void lazyLoad() {
        //判斷頁面是否顯示
    }

    @SuppressLint("HandlerLeak")
    private Handler sHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        Message message = Message.obtain();
        message.what = FINISH;
        sHandler.sendMessage(message);
        try {
            switch (type) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

}