package com.mgear.mednetapp.fragments;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.utils.AlarmTimer;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


/**
 * Created by Dennis
 * class: HomePageFragment
 */
public class HealthSettingDetailFragment extends MegaBaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener, TaskPost2ExecuteImpl {
    //靜態
    private static String TAG = "HealthSetting";
    private static final int FINISH = 1;

    //畫面參數
    private View rootView;
    private ListView mNoticeListView;
    private RelativeLayout mSettingPickerLayout;
    private TextView mSettingPickerCheck;
    private NumberPicker mPicker, mPicker2;


    private String healthType = "";
    private JSONArray noticeList = new JSONArray();
    private SharedPreferences settings;
    private JSONObject noticeObj;
    private String[] hourArr = null, minuteArr = null;
    private ListAdapter adapter;

    {
        hourArr = new String[24];
        minuteArr = new String[60];

        for (int i = 0; i < 24; i++) {
            hourArr[i] = i >= 10 ? "" + i : "0" + i;
        }
        for (int i = 0; i < 60; i++) {
            minuteArr[i] = i >= 10 ? "" + i : "0" + i;
        }
    }


    public HealthSettingDetailFragment setHealthType(String type) {
        healthType = type;
        return this;
    }

    public void newSetting() {
        Calendar cal = Calendar.getInstance();

        mPicker.setMinValue(0);
        mPicker.setMaxValue(23);
        mPicker.setDisplayedValues(hourArr);

        mPicker2.setMinValue(00);
        mPicker2.setMaxValue(59);
        mPicker2.setDisplayedValues(minuteArr);

        mPicker.setValue(cal.get(Calendar.HOUR_OF_DAY));
        mPicker2.setValue(cal.get(Calendar.MINUTE));
        mPicker.setWrapSelectorWheel(false); // 是否循環顯示
        mPicker2.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // 不可編輯
        mSettingPickerLayout.setVisibility(View.VISIBLE);
    }


    private void checkNoticeData() {
        //Log.i("debug", "e04 healthType = " + healthType);
        settings = getActivity().getSharedPreferences("HealthSettingNotice", Activity.MODE_PRIVATE);
        String noticeStr = settings.getString(healthType, "");
        noticeObj = new JSONObject();
        //Log.i("debug", "e04 noticeStr = " + noticeStr);
        if ("".equals(noticeStr)) {//第一次進來沒資料
            JSONArray list = new JSONArray();
            JSONObject obj = new JSONObject();
            //Log.i("debug", "healthType = " + healthType);
            try {
                switch (healthType) {
                    case "1":
                        obj = new JSONObject();
                        obj.put("notice", false);
                        obj.put("time", "06:00");
                        list.put(obj);
                        obj = new JSONObject();
                        obj.put("notice", false);
                        obj.put("time", "12:00");
                        list.put(obj);
                        obj = new JSONObject();
                        obj.put("notice", false);
                        obj.put("time", "18:00");
                        list.put(obj);
                        break;
                    case "2":
                        obj = new JSONObject();
                        obj.put("notice", false);
                        obj.put("time", "12:00");
                        list.put(obj);
                        break;
                    case "3":
                        break;
                    case "4":
                        obj = new JSONObject();
                        obj.put("notice", false);
                        obj.put("time", "14:00");
                        list.put(obj);
                        break;
                    case "5":
                        break;
                    case "6":
                        break;
                    case "7":
                        obj = new JSONObject();
                        obj.put("notice", false);
                        obj.put("time", "09:00");
                        list.put(obj);
                        break;
                }
                noticeObj.put("first", "1");
                noticeObj.put("list", list);
                //Log.i("debug", "noticeObj = " + noticeObj);
                settings.edit().putString(healthType, noticeObj.toString()).apply();
                noticeStr = settings.getString(healthType, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //Log.i("debug", "noticeStr = " + noticeStr);
        noticeObj = CommonFunc.parseJSON(noticeStr);
        noticeList = noticeObj.optJSONArray("list");
        //Log.i("debug", "noticeList = " + noticeList);
    }

    /**
     * 儲存新增提醒
     */
    private void saveNotice() {
        JSONArray sortedJsonArray = new JSONArray();
        List<JSONObject> jsonValues = new ArrayList<JSONObject>();
        JSONObject obj = new JSONObject();

        try {
            obj.put("notice", true);
            obj.put("time", hourArr[mPicker.getValue()] + ":" + minuteArr[mPicker2.getValue()]);
            noticeList.put(obj);

            for (int i = 0; i < noticeList.length(); i++) {
                JSONObject notice = noticeList.getJSONObject(i);
                setNotice(notice);
                jsonValues.add(notice);
            }

            //排序
            Collections.sort(jsonValues, new Comparator<JSONObject>() {
                private static final String KEY_NAME = "time";

                @Override
                public int compare(JSONObject a, JSONObject b) {
                    String valA = new String();
                    String valB = new String();
                    try {
                        valA = (String) a.get(KEY_NAME);
                        valB = (String) b.get(KEY_NAME);
                    } catch (JSONException e) {
                    }
                    return valA.compareTo(valB);
                }
            });

            for (int i = 0; i < noticeList.length(); i++) {
                sortedJsonArray.put(jsonValues.get(i));
            }

            noticeList = sortedJsonArray;
            //Log.i("debug", "sortedJsonArray= " + sortedJsonArray);
            noticeObj.put("first", "1");
            noticeObj.put("list", noticeList);
            //Log.i("debug", "noticeObj = " + noticeObj);
            settings.edit().putString(healthType, noticeObj.toString()).apply();
            adapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }
        mSettingPickerLayout.setVisibility(View.GONE);
    }

    private void setNotice(JSONObject notice) {
        // 要開通知
        if (notice.optBoolean("notice")) {
            int hour = Integer.parseInt(notice.optString("time").split(":")[0]);
            int minute = Integer.parseInt(notice.optString("time").split(":")[1]);
            int notificationID = Integer.parseInt(healthType + "" + hour + "" + minute);
            String time = notice.optString("time").replaceAll(":", "");

            Calendar cal = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, minute);
            cal.set(Calendar.SECOND, 0);
            cal2.setTime(new Date());

            if (cal.compareTo(cal2) == -1) {
                cal.add(Calendar.DATE, 1);
            }
            Log.d(TAG, "cal.compareTo(cal2)  = " + cal.compareTo(cal2));
            Log.d(TAG, "notificationID  = " + notificationID);
            Log.d(TAG, "預計提醒時間  = " + cal.getTime());
            AlarmTimer.setAlarmTimer(getActivity(), cal.getTimeInMillis(), MegaApplication.TIMER_ACTION, AlarmManager.INTERVAL_DAY, cal, notificationID);

            new CommonApiTask(getActivity(), this, false, "", TaskEnum.MednetAppLogNotice)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, healthType, time, "1");

        }
    }

    private void cancelNotice(JSONObject notice) {
        Log.d(TAG, "notice = " + notice);
        // 關閉通知
        if (!notice.optBoolean("notice")) {
            int hour = Integer.parseInt(notice.optString("time").split(":")[0]);
            int minute = Integer.parseInt(notice.optString("time").split(":")[1]);
            int notificationID = Integer.parseInt(healthType + "" + hour + "" + minute);
            String time = notice.optString("time").replaceAll(":", "");

            Log.i(TAG, "cancelNotice: " + notificationID);
            Log.d(TAG, "關閉提醒");
            AlarmTimer.cancelAlarmTimer(getActivity(), MegaApplication.TIMER_ACTION, notificationID);
            new CommonApiTask(getActivity(), this, false, "", TaskEnum.MednetAppLogNotice)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, healthType, time, "0");
        }
    }

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            try {
                rootView = inflater.inflate(R.layout.health_setting_notice, container, false);
                mNoticeListView = rootView.findViewById(R.id.notice_list);

                mSettingPickerLayout = rootView.findViewById(R.id.setting_picker_layout);
                mSettingPickerCheck = rootView.findViewById(R.id.setting_picker_check);
                mPicker = rootView.findViewById(R.id.picker);
                mPicker2 = rootView.findViewById(R.id.picker2);

                checkNoticeData();
                adapter = new ListAdapter();
                mNoticeListView.setAdapter(adapter);
                mNoticeListView.setOnItemClickListener(this::onItemClick);
                mSettingPickerLayout.setOnClickListener(this::onClick);
                mSettingPickerCheck.setOnClickListener(this::onClick);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return rootView;
    }

    @Override
    protected void lazyLoad() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_picker_layout:
                mSettingPickerLayout.setVisibility(View.GONE);
                break;
            case R.id.setting_picker_check:
                saveNotice();
                break;
        }
    }


    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {

    }

    class ListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return noticeList.length();
        }

        @Override
        public Object getItem(int i) {
            return noticeList.optJSONObject(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            convertView = LayoutInflater.from(getActivity()).inflate(R.layout.health_setting_timer_cell, null);
            RelativeLayout cell = convertView.findViewById(R.id.cell);
            TextView title = convertView.findViewById(R.id.cell_title);
            Switch mSwitch = convertView.findViewById(R.id.switch_istrue);
            JSONObject item = noticeList.optJSONObject(i);

            title.setText(item.optString("time"));
            mSwitch.setChecked(item.optBoolean("notice"));


            cell.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    new AlertDialog.Builder(getActivity()).setCancelable(false)
                            .setTitle("確認刪除")
                            .setMessage("確認刪除此提醒？")
                            .setPositiveButton("刪除", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int p) {
                                    //Log.i("debug", "確認刪除");
                                    try {
                                        item.put("notice", false);
                                        cancelNotice(item);
                                        JSONArray newArray = new JSONArray();
                                        for (int j = 0; j < noticeList.length(); j++) {
                                            if (i != j) {
                                                newArray.put(noticeList.optJSONObject(j));
                                            }
                                        }
                                        noticeList = newArray;
                                        noticeObj.put("first", "1");
                                        noticeObj.put("list", noticeList);
                                        settings.edit().putString(healthType, noticeObj.toString()).apply();
                                        adapter.notifyDataSetChanged();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }
                            )
                            .show();


                    return false;
                }
            });

            mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        try {
                            item.put("notice", true);
                            setNotice(item);
                            //Log.i("debug", "noticeObj = " + noticeObj);
                            settings.edit().putString(healthType, noticeObj.toString()).apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            item.put("notice", false);
                            cancelNotice(item);
                            //Log.i("debug", "noticeObj = " + noticeObj);
                            settings.edit().putString(healthType, noticeObj.toString()).apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });

            return convertView;
        }
    }

}