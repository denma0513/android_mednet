package com.mgear.mednetapp.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.HealthPushSettingActivity;
import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.R;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Dennis
 * class: HomePageFragment
 */
public class HealthSettingFragment extends MegaBaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener, View.OnTouchListener {
    //靜態
    private static String TAG = "HealthSetting";
    private static final int FINISH = 1;

    //畫面參數
    private View rootView;
    private ListView mNoticeListView;
    private ImageView mSettingTopBack;
    private TextView mSettingTopTitle, mSettingTopRightBtn;
    private FrameLayout mSettingContent;
    private RelativeLayout mSettingGoogleFit;
    private ListView mSettingList;


    private String healthType = "";
    private ArrayList<JSONObject> settingList = new ArrayList<JSONObject>();


    public HealthSettingFragment setHealthType(String type) {
        healthType = type;
        return this;
    }


    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            try {
                rootView = inflater.inflate(R.layout.health_setting_list, container, false);
                mSettingGoogleFit = rootView.findViewById(R.id.setting_google_fit);
                mSettingList = rootView.findViewById(R.id.setting_list);

                try {
                    for (int i = 0; i < MegaApplication.healthTypeList.length; i++) {
                        JSONObject obj = new JSONObject();
                        if ("6".equals(MegaApplication.healthTypeList[i])) continue;
                        obj.put("healthType", MegaApplication.healthTypeList[i]);
                        obj.put("healthName", MegaApplication.healthNameList[i]);
                        settingList.add(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ListAdapter adapter = new ListAdapter();
                mSettingList.setAdapter(adapter);
                mSettingList.setOnItemClickListener(this);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return rootView;
    }

    @Override
    protected void lazyLoad() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ((HealthPushSettingActivity) getActivity()).goDetail(settingList.get(i).optString("healthName"), settingList.get(i).optString("healthType"));
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }


    class ListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return settingList.size();
        }

        @Override
        public Object getItem(int i) {
            return settingList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            convertView = LayoutInflater.from(getActivity()).inflate(R.layout.health_setting_cell, null);
            TextView title = convertView.findViewById(R.id.cell_title);
            title.setText(settingList.get(i).optString("healthName"));
            return convertView;
        }
    }

}