package com.mgear.mednetapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mgear.mednetapp.adapter.WelcomePagerStartAppAdapter;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.task.common.CustomerApiTask;
import com.mgear.mednetapp.task.organization.AppCheckUpgrade2Task;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.views.CustomViewPager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class WelcomeActivity extends BaseActivity implements TaskPost2ExecuteImpl, View.OnClickListener, TaskPostExecuteImpl,
        DialogInterface.OnClickListener, ViewPager.OnPageChangeListener {

    private TaskEnum executeTask;
    private String appPath;
    private JSONArray healthList;
    private JSONArray homeList;
    private CustomViewPager mViewPager;
    private WelcomePagerStartAppAdapter mTutorialPagerAdapter;
    public static String AppVersion;
    private Intent shareIntent;

    public void registerPushToken() {
        Log.d("debug", "Build.VERSION.SDK_INT >= Build.VERSION_CODES.O = " + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.cnannel_ID);
            String channelName = getString(R.string.cnannel_name);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH));
        }

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful() || task.getResult() == null) {
                    //Log.d("debug", "get token fail ");
                    return;
                }
                CommonFunc.pushToken = task.getResult().getToken();
                MegaApplication.pushToken = task.getResult().getToken();
                Log.d("debug", "token = " + MegaApplication.pushToken);
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        shareIntent = getIntent();
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        MegaApplication.deviceID = androidId;
        setContentView(R.layout.activity_welcome);
        this.mTutorialPagerAdapter = new WelcomePagerStartAppAdapter(getFragmentManager());
        this.mViewPager = findViewById(R.id.view_pager);

        MegaApplication.homeList = new JSONArray();
        MegaApplication.healthList = new JSONArray();

        //取得應用程式的版號
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            AppVersion = packageInfo.versionName;
            Log.i("debug", "onCreate: getIPAddress(true) = " + getIPAddress(true));
            MegaApplication.IPAddress = getIPAddress(true);
            String uuid = UUID.randomUUID().toString().toUpperCase();
            MegaApplication.UUID = uuid;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        registerPushToken();


        if (MegaApplication.IsFirstTime) {
            this.mViewPager.addOnPageChangeListener(this);
            this.mViewPager.setAdapter(mTutorialPagerAdapter);
            this.mViewPager.setPagingEnabled(true);
            SharedPreferences firstSettings = getSharedPreferences("static", 0);
            firstSettings.edit().putBoolean("firstTime", false).apply();

        } else {
            this.mViewPager.setVisibility(View.GONE);
            //Bundle extras = getIntent().getExtras();
            //new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetProfile).execute();
            new CommonApiTask(this, this, false, "", TaskEnum.HomePageTask).execute();
        }

        //判斷app版本是否要強更
        new AppCheckUpgrade2Task(this, this, false, "").execute(AppVersion);


        int i = 0;
        Log.d("debug", "MegaApplication.healthSortList: " + MegaApplication.healthSortList);
        for (String healthType : MegaApplication.healthSortList) {
            JSONObject item = new JSONObject();
            JSONObject healthMap = MegaApplication.healthMap.get(healthType);
            String typeName = healthMap.optString("healthTypeName");

            try {
                item.put("healthTypeName", healthMap.optString("healthName"));
                item.put("healthType", healthType);
                item.put("healthTypeKey", typeName);
                item.put("healthImg", healthMap.optString("healthImg"));
                item.put("healthIcon", healthMap.optString("healthRecordIcon"));
                item.put("index", i);
                //Log.i("debug", "item  = " + item);
                MegaApplication.healthList.put(item);
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        //MegaApplication.healthList = healthList;
        Log.d("debug", "onCreate: " + MegaApplication.healthList);
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }


    @Override
    public void onPostExecute(TaskEnum type, Object result) {
        executeTask = type;
        switch (type) {
            case AppCheckUpgradeTask:
                JSONObject av = (JSONObject) result;
                //判斷應用程式的狀態
                //Log.i("debug", "av + " + av);
                if (av != null && !av.isNull("data")) {
                    if (av.optJSONObject("data").optBoolean("result")) {
                    } else {
                        String newVersion = av.optJSONObject("data").optString("newVersion");
                        appPath = av.optJSONObject("data").optString("url");
                        if (!"".equals(newVersion)) {
                            new AlertDialog.Builder(this)
                                    .setTitle(R.string.msg_new_version)
                                    .setMessage(String.format(Locale.getDefault(), getString(R.string.msg_app_version), newVersion))
                                    .setPositiveButton(R.string.button_setup, this).show();
                        }
                    }
                }

                break;
        }
    }

    /**
     * Api reponse handle
     */
    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {

        switch (type) {
            case MednetGetProfile:
                //Log.i("debug", "MednetGetProfile" + result);
                if (result != null && !result.isNull("source")) {
                    JSONObject source;
                    source = result.optJSONObject("source");
                    MegaApplication.getInstance().getMember().setName(source.optString("name"));
                    MegaApplication.getInstance().getMember().setCustomerAppId(source.optString("customer_app_id"));
                    MegaApplication.getInstance().getMember().setCustomerId(source.optString("customer_id"));
                    MegaApplication.getInstance().getMember().setEmail(source.optString("email"));
                    MegaApplication.getInstance().getMember().setFirstName(source.optString("first_name"));
                    MegaApplication.getInstance().getMember().setLastName(source.optString("last_name"));
                    MegaApplication.getInstance().getMember().setWebToken(source.optString("web_token"));
                    MegaApplication.getInstance().getMember().setCellphone(source.optString("cellphone"));
                    MegaApplication.getInstance().getMember().setGender(source.optString("gender"));
                    MegaApplication.getInstance().getMember().setBirthday(source.optString("birthday"));
                    MegaApplication.getInstance().getMember().setProfileImage(source.optString("profile_image"));
                    MegaApplication.getInstance().getMember().setCoverImage(source.optString("cover_image"));

                    SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
                    settings.edit().putString("name", MegaApplication.getInstance().getMember().getName()).apply();

                    return;
                } else {
                    //尚未登入 直接進到主畫面
                    //gotoMain();
                }
                break;
            case HomePageTask:
                if (result != null && "200".equals(result.optString("code"))) {
                    homeList = result.optJSONArray("data");
                    MegaApplication.homeList = homeList;
                    if (!MegaApplication.getInstance().getMember().isNullRefreshToken())
                        new CommonApiTask(this, this, false, "", TaskEnum.MednetRefreshTokenTask)
                                .execute(MegaApplication.getInstance().getMember().getRefreshToken());
                    else
                        gotoMain();
                } else {
                    gotoMain();
                }
                break;
            case MednetRefreshTokenTask:
                if (result != null && "200".equals(result.optString("state_code"))) {
                    JSONObject source = result.optJSONObject("source");
                    if (source != null) {
                        String accessToken = source.optString("access_token");
                        String refreshToken = source.optString("refresh_token");
                        Log.i("debug", "accessToken = " + accessToken);
                        Log.i("debug", "refreshToken = " + refreshToken);
                        MegaApplication.getInstance().getMember().setAccessToken(accessToken);
                        MegaApplication.getInstance().getMember().setRefreshToken(refreshToken);
                        SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
                        settings.edit().putString("accessToken", accessToken).putString("refreshToken", refreshToken).apply();
                    }
                }
                gotoMain();
                break;
        }
    }

    public void gotoMain() {
        Uri uri = shareIntent.getData();
        Intent intent = new Intent(this, MainActivity.class);
        if (uri != null) {
            String guid = uri.getQueryParameter("guid");
            String AddFriendsId = uri.getQueryParameter("AddFriendsId");
            String actionType = uri.getQueryParameter("actionType");
            String actionTarget = uri.getQueryParameter("actionTarget");

            Bundle bundle = new Bundle();
            bundle.putString("guid", guid);
            bundle.putString("AddFriendsId", AddFriendsId);
            bundle.putString("action", actionType);
            bundle.putString("actionTarget", actionTarget);
            intent.putExtras(bundle);
        }

        startActivity(intent);
    }

    public void signIn() {
        Bundle bundle = new Bundle();
        Intent integer = new Intent(this, UserLoginActivity.class);
        //bundle.putString("action", MegaApplication.ActionTypeWebView);
        //bundle.putString("target", "https://med-net.com/Member");
        integer.putExtras(bundle);
        startActivityForResult(integer, Integer.parseInt(MegaApplication.ActionTypeLogin));
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (executeTask) {
            case AppCheckUpgradeTask:
                //判斷是否下載應用程式
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.mgear.mednetapp"));
                if (appPath == null) {
                    //取得應用程式更新訊息
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.mgear.mednetapp"));
                } else {
                    intent.setData(Uri.parse(appPath));
                }
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    //回接activity的結果
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Integer.parseInt(MegaApplication.ActionTypeLogin)) {
            Log.w("SIGNIN", "onActivityResult");
            try {
                //完成登入
                if (resultCode == Activity.RESULT_OK) {
                    mViewPager.setVisibility(View.GONE);
                    //new CustomerApiTask(this, this, false, "", TaskEnum.MednetGetProfile).execute();
                    new CommonApiTask(this, this, false, "", TaskEnum.HomePageTask).execute();
                    return;
                }
            } catch (Exception e) {
                Log.w("debug", "signInResult:failed code=" + e.getMessage());
                e.printStackTrace();
            }
        }
        gotoMain();
    }
}
