package com.mgear.mednetapp;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.WebViewFragment;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.OAuth2GetPorfileTask;
import com.mgear.mednetapp.task.common.OAuth2GetTokenTask;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONArray;
import org.json.JSONObject;

public class YahooLoginActivity extends FragmentActivity implements TaskPost2ExecuteImpl {
    private static String TAG = "SIGNIN";
    private static String loginUrl = "https://api.login.yahoo.com/oauth2/request_auth?client_id=dj0yJmk9NUhUN3F1THdIeVk1JmQ9WVdrOVpEVnhhbFpWTXpZbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1mNw--&redirect_uri=https%3A%2F%2Fwww.med-net.com&response_type=code&language=zh-tw";
    private FragmentManager fragmentManager;
    private ProgressBar progressBar;
    private TextView txtMessage;
    private WebView mWebView;
    private String loginCode;
    private boolean startLogin = false;
    private String accessToken, refreshToken, xoauthYahooGuid, expiresIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_web_view);
        progressBar = (ProgressBar) findViewById(R.id.pbBrowserWait);
        txtMessage = (TextView) findViewById(R.id.txtBrowserMessage);
        mWebView = (WebView) findViewById(R.id.webViewBrowser);

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);

        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new YahooLoginActivity.MednetWebViewClient());
        mWebView.loadUrl(loginUrl);


    }

    private class MednetWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i(TAG, "shouldOverrideUrlLoading = " + url);

            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Log.i(TAG, "shouldOverrideUrlLoading = " + request.getUrl().toString());
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.i(TAG, "onPageFinished = " + url);
            txtMessage.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
            //https://med-net.com/?code=uz8mykt
            if (url != null && url.contains("code=")) {
                String code = url.substring(url.indexOf("code=") + 5);
                Log.i(TAG, "code   = " + code);
                if (code != null && code.length() > 0 && !startLogin) {
                    startLogin = true;
                    Log.i(TAG, "我取得了code  = " + code);
                    loginCode = code;
                    mWebView.setVisibility(View.GONE);
                    new OAuth2GetTokenTask(YahooLoginActivity.this, YahooLoginActivity.this, true, "登入中...").execute(code);
                }
            }

        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.stopLoading();
            view.loadUrl("about:blank");
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
        }


//        @Override
//        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            //全部擋掉
//            handler.cancel();
//        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        if (type == TaskEnum.Oauth2GetTokenTask && result != null) {
            Log.i(TAG, "result = " + result);
            accessToken = result.optString("access_token");
            refreshToken = result.optString("refresh_token");
            xoauthYahooGuid = result.optString("xoauth_yahoo_guid");
            expiresIn = result.optString("expires_in");
            Log.i(TAG, "onPostExecute: e04");
            new OAuth2GetPorfileTask(YahooLoginActivity.this, YahooLoginActivity.this, true, "登入中...").execute(accessToken);


        } else if (type == TaskEnum.Oauth2GetProfileTask && result != null) {
            Log.i(TAG, "onPostExecute: result = " + result);

            if (result.has("profile") && result.optJSONObject("profile").has("emails")) {
                JSONArray emails = result.optJSONObject("profile").optJSONArray("emails");
                String gender = result.optJSONObject("profile").optString("gender");
                String birthday = result.optJSONObject("profile").optString("birthday");
                String lastName = result.optJSONObject("profile").optString("familyName");
                String firstName = result.optJSONObject("profile").optString("givenName");


                String mail = "";
                if (emails != null && emails.length() > 0) {
                    mail = emails.optJSONObject(0).optString("handle");
                }
                Intent intent = new Intent();
                intent.putExtra("accessToken", accessToken);
                intent.putExtra("refreshToken", refreshToken);
                intent.putExtra("expiresIn", expiresIn);
                intent.putExtra("mail", mail);
                intent.putExtra("gender", gender);
                intent.putExtra("birthday", birthday);
                intent.putExtra("lastName", lastName);
                intent.putExtra("firstName", firstName);
                intent.putExtra("xoauthYahooGuid", xoauthYahooGuid);
                this.setResult(RESULT_OK, intent);
                this.finish();

            }
            Log.i(TAG, "onPostExecute: " + result);
        } else {
            Intent intent = new Intent();
            this.setResult(RESULT_CANCELED, intent);
            this.finish();
        }


    }

    @Override
    public void onBackPressed() {
        Log.i("debug", "onBackPressed");
        YahooLoginActivity.this.finish();
    }
}
