package com.mgear.mednetapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by Jye on 2017/9/5.
 * class: VideoActivity
 */
public class YouTubePlayerActivity extends YouTubeBaseActivity implements
        View.OnClickListener, YouTubePlayer.OnInitializedListener, YouTubePlayer.PlaybackEventListener {

    private static final int RECOVERY_REQUEST = 1;

    private String strUrl;
    private View imgClose;
    private YouTubePlayerView youTubeView;

    protected Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player);
        Intent intent = getIntent();
        strUrl = intent.getStringExtra("result");

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtubePlayer);
        youTubeView.initialize(getString(R.string.youtube_api_key), this);
        imgClose = findViewById(R.id.imgYoutubePlayerClose);
        imgClose.setOnClickListener(this);

        //設定螢幕電源不暗
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onClick(View v) {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(getString(R.string.youtube_api_key), this);
        }
    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            youTubePlayer.setPlaybackEventListener(this);
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
            youTubePlayer.cueVideo(strUrl);
        }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_REQUEST).show();
        }
    }

    @Override
    public void onPlaying() {
        imgClose.setVisibility(View.GONE);
    }

    @Override
    public void onPaused() {
        imgClose.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStopped() {
        imgClose.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBuffering(boolean b) {

    }

    @Override
    public void onSeekTo(int i) {

    }

}