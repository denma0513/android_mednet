package com.mgear.mednetapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.fragments.SignUpChkSMSFragment;
import com.mgear.mednetapp.fragments.SignUpProfileFragment;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;
import com.mgear.mednetapp.interfaces.IBackHandleIpml;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.task.common.CommonApiTask;
import com.mgear.mednetapp.utils.CommonFunc;

import org.json.JSONObject;


public class ForgetPasswordActivity extends BaseActivity implements View.OnClickListener, TaskPost2ExecuteImpl, IBackHandleIpml {
    //常數
    private static String TAG = "SIGNIN";

    private EditText mForgetEmail;
    private Button mSigninCancelButton;
    private RelativeLayout mForgetLayout;
    private TextView mForgetEmailWarning;
    private MegaBaseFragment selectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_forget_password);
        mForgetEmail = findViewById(R.id.forget_email);
        mSigninCancelButton = findViewById(R.id.forget_send_button);
        mForgetLayout = findViewById(R.id.forget_layout);
        mForgetEmailWarning = findViewById(R.id.forget_email_warning);
        mSigninCancelButton.setOnClickListener(this);
        mForgetEmailWarning.setVisibility(View.GONE);
    }


    public void goUpProfile() {
        try {
            //先儲存access token
            String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
            String refreshToken = MegaApplication.getInstance().getMember().getRefreshToken();
            SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
            settings.edit().putString("accessToken", accessToken).putString("refreshToken", refreshToken).apply();

            //進到修改密碼流程
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            FragmentManager fragmentManager = getFragmentManager();
            SignUpProfileFragment signUpProfile = new SignUpProfileFragment().setType(1);
            FragmentTransaction ft = fragmentManager.beginTransaction();
            Fragment fragmentMain = signUpProfile;
            ft.replace(R.id.forget_layout, fragmentMain);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //登入後紀錄token
    public void loginForever() {
        try {
            String accessToken = MegaApplication.getInstance().getMember().getAccessToken();
            String refreshToken = MegaApplication.getInstance().getMember().getRefreshToken();
            SharedPreferences settings = getSharedPreferences(SyncStateContract.Constants.DATA, 0);
            settings.edit().putString("accessToken", accessToken).putString("refreshToken", refreshToken).apply();
            Intent intent = new Intent();
            intent.putExtra("status", "success");
            this.setResult(RESULT_OK, intent);
            this.finish();
        } catch (Exception e) {
            Log.w(TAG, "loginForever : " + e.getStackTrace());
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forget_send_button:
                String email = mForgetEmail.getText().toString();
                mForgetEmailWarning.setVisibility(View.GONE);
                if (!"".equals(email) && CommonFunc.isEmailValid(email)) {
                    new CommonApiTask(this, this, true, getString(R.string.msg_data_loading),
                            TaskEnum.MednetForgetPassword).execute(mForgetEmail.getText().toString());
                } else {
                    mForgetEmailWarning.setVisibility(View.VISIBLE);
                    mForgetEmailWarning.setText("請輸入");
                }
                break;


        }
    }

    @Override
    public void onPostExecute(TaskEnum type, JSONObject result) {
        switch (type) {
            case MednetForgetPassword:
                try {
                    FragmentManager fragmentManager = getFragmentManager();
                    SignUpChkSMSFragment signUpChkSMSFragment = new SignUpChkSMSFragment();
                    signUpChkSMSFragment.setEmail(mForgetEmail.getText().toString()).setType(1);
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    Fragment fragmentMain = signUpChkSMSFragment;
                    ft.replace(R.id.forget_layout, fragmentMain);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                } catch (Exception e) {
                    Log.w(TAG, "MednetForgetPassword : " + e.getStackTrace());
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void setSelectedFragment(MegaBaseFragment selectedFrangment) {
        this.selectedFragment = selectedFrangment;
    }
}
