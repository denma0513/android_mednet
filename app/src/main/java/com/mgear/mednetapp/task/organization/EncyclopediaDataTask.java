package com.mgear.mednetapp.task.organization;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.CustomerActivity;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.entity.organization.EncyclopediaSet;
import com.mgear.mednetapp.utils.HttpConnectFunc;

/**
 * Created by Jye on 2017/8/9.
 * class: EncyclopediaDataTask
 */
public class EncyclopediaDataTask extends AsyncTask<String, Void, EncyclopediaSet> {

    private Context mContext;
    private boolean mShowDialog;
    private String mDialogMessage;
    private TaskPostExecuteImpl mPostListener;
    private ProgressDialog mDialog;

    /**
     * 建構函數
     * @param context Context
     * @param postExecute 執行完成的回呼函數
     * @param showProgressDialog 是否顯示對話方塊
     * @param message 對話方塊訊息文字
     */
    public EncyclopediaDataTask(Context context, TaskPostExecuteImpl postExecute, boolean showProgressDialog, String message) {
        mContext = context;
        mPostListener = postExecute;
        mShowDialog = showProgressDialog;
        mDialogMessage = message;
    }

    @Override
    protected void onPreExecute() {
        if (mShowDialog) {
            mDialog = new ProgressDialog(mContext);
            mDialog.setMessage(mDialogMessage);
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }
    }

    @Override
    protected EncyclopediaSet doInBackground(String... args) {
        return HttpConnectFunc.executeEncyclopedia(args[0], CultureSelectActivity.SelectCulture, CustomerActivity.getCustomer().getGender());
    }

    @Override
    protected void onPostExecute(EncyclopediaSet result) {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        if (mPostListener != null)
            mPostListener.onPostExecute(TaskEnum.EncyclopediaDataTask, result);
    }

}