package com.mgear.mednetapp.task.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mgear.mednetapp.CultureSelectActivity;
import com.mgear.mednetapp.entity.organization.AdditionItemSet;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.utils.HttpConnectFunc;

import org.json.JSONObject;

/**
 * Created by Jye on 2017/7/9.
 * class: AdditionItemDataTask
 */
public class OAuth2GetTokenTask extends AsyncTask<String, Void, JSONObject> {

    private Context mContext;
    private boolean mShowDialog;
    private String mDialogMessage;
    private TaskPost2ExecuteImpl mPostListener;
    private ProgressDialog mDialog;

    /**
     * 建構函數
     * @param context Context
     * @param postExecute 執行完成的回呼函數
     * @param showProgressDialog 是否顯示對話方塊
     * @param message 對話方塊訊息文字
     */
    public OAuth2GetTokenTask(Context context, TaskPost2ExecuteImpl postExecute, boolean showProgressDialog, String message) {
        mContext = context;
        mPostListener = postExecute;
        mShowDialog = showProgressDialog;
        mDialogMessage = message;
    }

    @Override
    protected void onPreExecute() {
        if (mShowDialog) {
            mDialog = new ProgressDialog(mContext);
            mDialog.setMessage(mDialogMessage);
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }
    }

    @Override
    protected JSONObject doInBackground(String... args) {
        return HttpConnectFunc.oauth2GetToken(args[0]);
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        if (mPostListener != null)
            mPostListener.onPostExecute(TaskEnum.Oauth2GetTokenTask, result);
    }

}