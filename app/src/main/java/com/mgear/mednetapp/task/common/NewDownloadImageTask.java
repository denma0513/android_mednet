package com.mgear.mednetapp.task.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.mgear.mednetapp.utils.CommonFunc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class NewDownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView mImage;
    private int picType = 0;//0表示默认png图片；1表示jpg或者jpeg

    public NewDownloadImageTask(ImageView image, int pixels) {
        this.mImage = image;
    }

    public NewDownloadImageTask(ImageView image) {
        this.mImage = image;
    }


    /**
     * 质量压缩
     */
    public Bitmap compressImage(Bitmap image) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap.CompressFormat Type = picType == 0 ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG;
            //image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
            image.compress(Type, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
            int options = 100;
            while (baos.toByteArray().length / 1024 > 100) {  //循环判断如果压缩后图片是否大于100kb,大于继续压缩
                baos.reset();//重置baos即清空baos
                image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
                options -= 10;//每次都减少10
            }
            ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
            Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
            return bitmap;

        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }

    }

    protected Bitmap doInBackground(String... args) {
        String urldisplay = args[0];

        Bitmap mIcon11 = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            InputStream in = new java.net.URL(urldisplay).openStream();

            mIcon11 = BitmapFactory.decodeStream(in, null, options);

            if (mIcon11 == null) {
                // Do the actual decoding
                options.inJustDecodeBounds = false;


                int w = options.outWidth;
                int h = options.outHeight;
                //现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
                float hh = 800f;//这里设置高度为800f
                float ww = 480f;//这里设置宽度为480f
                //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
                int be = 1;//be=1表示不缩放
                if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
                    be = (int) (options.outWidth / ww);
                } else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放
                    be = (int) (options.outHeight / hh);
                }

                if (be <= 0)
                    be = 1;
                options.inSampleSize = be;//设置缩放比例
                //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
                //bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
                in.close();
                in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in, null, options);
                in.close();
            }

        } catch (Exception e) {
            //Log.e("Error", e.getMessage());
            //e.printStackTrace();
            return null;
        }
        return compressImage(mIcon11);
    }

    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            mImage.setImageBitmap(result);
        }
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int bytes = read();
                    if (bytes < 0) {
                        break; // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }
}
