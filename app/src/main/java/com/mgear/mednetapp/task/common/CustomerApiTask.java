package com.mgear.mednetapp.task.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.UserLoginActivity;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.utils.CommonFunc;
import com.mgear.mednetapp.utils.HttpConnectFunc;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Dennis
 * class: CommonApiTask
 */
public class CustomerApiTask extends AsyncTask<String, Void, JSONObject> {

    private Context mContext;
    private boolean mShowDialog;
    private String mDialogMessage;
    private TaskPost2ExecuteImpl mPostListener;
    private ProgressDialog mDialog;
    private TaskEnum mTask;
    private boolean mNeedLogin = false;
    private static int GET = 0;
    private static int POST = 1;
    private Date endDate, curDate;

    /**
     * 建構函數
     *
     * @param context            Context
     * @param postExecute        執行完成的回呼函數
     * @param showProgressDialog 是否顯示對話方塊
     * @param message            對話方塊訊息文字
     */
    public CustomerApiTask(Context context, TaskPost2ExecuteImpl postExecute, boolean showProgressDialog, String message, TaskEnum task) {
        mContext = context;
        mPostListener = postExecute;
        mShowDialog = showProgressDialog;
        mDialogMessage = message;
        mTask = task;
    }

    /**
     * 建構函數
     *
     * @param context            Context
     * @param postExecute        執行完成的回呼函數
     * @param showProgressDialog 是否顯示對話方塊
     * @param message            對話方塊訊息文字
     * @param needLogin          要幫轉登入頁面
     */
    public CustomerApiTask(Context context, TaskPost2ExecuteImpl postExecute, boolean showProgressDialog, String message, TaskEnum task, boolean needLogin) {
        mContext = context;
        mPostListener = postExecute;
        mShowDialog = showProgressDialog;
        mDialogMessage = message;
        mNeedLogin = needLogin;
        mTask = task;
    }

    @Override
    protected void onPreExecute() {
        if (mShowDialog) {
            mDialog = new ProgressDialog(mContext);
            mDialog.setMessage(mDialogMessage);
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }
    }

    @Override
    protected JSONObject doInBackground(String... args) {

        curDate = new Date(System.currentTimeMillis());
        Log.e("debug", mTask + "呼叫時間 開始呼叫");
        String apiUrl = "";
        JSONObject jsonParam = new JSONObject();
        int type = POST;
        try {
            switch (mTask) {
                case MednetGetProfile:
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Customer/GetProfile");
                    break;
                case MednetSetFCMToken:
                    jsonParam.put("platform_type", 1);
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("device_token", MegaApplication.deviceID);
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Account/SetFCMToken");
                    break;
                case MednetGetAuthType:
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/GetAuthTypeApi");
                    jsonParam.put("userID", MegaApplication.getInstance().getMember().getCustomerId());
                    break;
                case MednetGetBadge:
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_MEDNET_API_URL, "api/badge");
                    jsonParam.put("userID", MegaApplication.getInstance().getMember().getCustomerId());
                    break;
                case MednetGetWebToken:
                    type = GET;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Customer/GetWebToken");
                    break;
                case MednetUpdateProfile:
                    //更新用戶資料
                    type = POST;
                    if (args.length == 1) {
                        jsonParam.put("cellphone", args[0]);
                    } else if (args.length == 4) {
                        jsonParam.put("first_name", args[0]);
                        jsonParam.put("last_name", args[1]);
                        jsonParam.put("name", args[1] + args[0]);

                        if (args[2] != null) {
                            jsonParam.put("gender", Integer.parseInt(args[2]));
                        }
                        jsonParam.put("birthday", args[3]);
                    }
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Customer/UpdateProfile");
                    break;
                case MednetSendSMS:
                    //發送簡訊
                    type = GET;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Account/SendSMS");
                    break;
                case MednetCheckSMS:
                    //驗證簡訊
                    type = GET;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Account/CheckSMS");
                    jsonParam.put("optCode", args[0]);
                    break;
                case MednetSetPassword:
                    //修改密碼
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Customer/SetPassword");
                    jsonParam.put("password", args[0]);
                    break;
                case MednetQueryLastMeasureValue:
                    //取得健康資料全部type
                    type = GET;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/QueryLastMeasureValue");
                    break;
                case MednetQueryMeasureValueByID:
                    //取得單筆健康資料
                    type = GET;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/QueryMeasureValue");
                    jsonParam.put("measureType", args[0]);
                    jsonParam.put("measureId", args[1]);
                    break;
                case MednetQueryMeasureValues:
                    //取得健康資料for type
                    type = GET;
                    jsonParam.put("measureType", args[0]);
                    jsonParam.put("startTime", args[1]);
                    jsonParam.put("endTime", args[2]);
                    jsonParam.put("IsByDateLast", args[3]);
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/QueryMeasureValues");
                    break;
                case MednetQueryMeasureValuesType1:
                    //取得健康資料for type
                    type = GET;
                    jsonParam.put("startTime", args[0]);
                    jsonParam.put("endTime", args[1]);
                    jsonParam.put("measureType", "1");
                    jsonParam.put("IsByDateLast", "true");
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/QueryMeasureValues");
                    break;
                case MednetQueryMeasureValuesType4:
                    //取得健康資料for type
                    type = GET;
                    jsonParam.put("startTime", args[0]);
                    jsonParam.put("endTime", args[1]);
                    jsonParam.put("measureType", "4");
                    jsonParam.put("IsByDateLast", "false");
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/QueryMeasureValues");
                    break;
                case MednetQueryMeasureValuesType6:
                    //取得健康資料for type
                    type = GET;
                    jsonParam.put("startTime", args[0]);
                    jsonParam.put("endTime", args[1]);
                    jsonParam.put("measureType", "6");
                    jsonParam.put("IsByDateLast", "true");
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/QueryMeasureValues");
                    break;
                case MednetCreateBloodSugar:
                    //新增血糖
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/BloodSugar/Create");

                    if ("飯前".equals(args[1])) {
                        jsonParam.put("ac", Integer.parseInt(args[0]));
                        jsonParam.put("pc", 0);
                    } else if ("飯後".equals(args[1])) {
                        jsonParam.put("ac", 0);
                        jsonParam.put("pc", Integer.parseInt(args[0]));
                    }

                    jsonParam.put("m_time", args[2]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetUpdateBloodSugar:
                    //更新血糖
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/BloodSugar/Update");

                    if ("飯前".equals(args[1])) {
                        jsonParam.put("ac", Integer.parseInt(args[0]));
                        jsonParam.put("pc", 0);
                    } else if ("飯後".equals(args[1])) {
                        jsonParam.put("ac", 0);
                        jsonParam.put("pc", Integer.parseInt(args[0]));
                    }

                    jsonParam.put("m_time", args[2]);
                    jsonParam.put("id", Integer.parseInt(args[3]));
                    jsonParam.put("measure_id", args[4]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("updated_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetCreateBloodPressure:
                    //新增血壓
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/BloodPressure/Create");

                    jsonParam.put("sbp", Integer.parseInt(args[0]));
                    jsonParam.put("dbp", Integer.parseInt(args[1]));
                    jsonParam.put("hr", Integer.parseInt(args[2]));
                    jsonParam.put("medication", Integer.parseInt(args[3]));

                    jsonParam.put("m_time", args[4]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetUpdateBloodPressure:
                    //更新血壓
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/BloodPressure/Update");

                    jsonParam.put("sbp", Integer.parseInt(args[0]));
                    jsonParam.put("dbp", Integer.parseInt(args[1]));
                    jsonParam.put("hr", Integer.parseInt(args[2]));
                    jsonParam.put("medication", Integer.parseInt(args[3]));

                    jsonParam.put("m_time", args[4]);
                    jsonParam.put("id", Integer.parseInt(args[5]));
                    jsonParam.put("measure_id", args[6]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("updated_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetCreateBloodOxygen:
                    //新增血氧
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/BloodOxygen/Create");
                    jsonParam.put("bs", Integer.parseInt(args[0]));
                    jsonParam.put("note", args[1]);

                    jsonParam.put("m_time", args[2]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetUpdateBloodOxygen:
                    //更新血氧
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/BloodOxygen/Update");
                    jsonParam.put("bs", Integer.parseInt(args[0]));
                    jsonParam.put("note", args[1]);

                    jsonParam.put("m_time", args[2]);
                    jsonParam.put("id", Integer.parseInt(args[3]));
                    jsonParam.put("measure_id", args[4]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("updated_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetCreateDrinkingCreateOrUpdate:
                    //新增或更新飲水
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/Drinking/CreateOrUpdate");
                    jsonParam.put("drinking_water_today", Integer.parseInt(args[0]));
                    jsonParam.put("drinking_water_suggest", MegaApplication.WaterCapacity);
                    jsonParam.put("drinking_water_undo", MegaApplication.WaterCapacity - Integer.parseInt(args[0]));

                    jsonParam.put("m_time", args[1]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("update_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;

                case MednetSleepingCreateOrUpdate:
                    //新增睡眠時間
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/Sleeping/CreateOrUpdate");

                    jsonParam.put("sleep_time", args[0]);
                    jsonParam.put("getup_time", args[1]);
                    jsonParam.put("sleeping_hours", Double.parseDouble(args[2]));
                    jsonParam.put("measure_source", 1);

                    jsonParam.put("m_time", args[3]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetCreateStepCreateOrUpdate:
                    //新增步數
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/Step/CreateOrUpdate");

                    jsonParam.put("step_count", Integer.parseInt(args[0]));
                    jsonParam.put("distance", Double.parseDouble(args[1]));
                    jsonParam.put("calorie", Double.parseDouble(args[2]));
                    jsonParam.put("measure_source", 1);

                    jsonParam.put("m_time", args[3]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("updated_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");
                    if (args.length == 6) {
                        jsonParam.put("id", Integer.parseInt(args[4]));
                        jsonParam.put("measure_id", args[5]);
                    }
                    //Log.i("debug", "MednetCreateStepCreateOrUpdate jsonParam: "+jsonParam);

                    break;
                case MednetCreateBodyMassWeight:
                    //新增身體數值
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/BodyMassWeight/Create");
                    jsonParam.put("bh", Double.parseDouble(args[0]));
                    jsonParam.put("bw", Double.parseDouble(args[1]));
                    jsonParam.put("bmi", Double.parseDouble(args[2]));
                    jsonParam.put("note", args[3]);

                    jsonParam.put("m_time", args[4]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetUpdateBodyMassWeight:
                    //更新身體數值
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/BodyMassWeight/Update");
                    jsonParam.put("bh", Double.parseDouble(args[0]));
                    jsonParam.put("bw", Double.parseDouble(args[1]));
                    jsonParam.put("bmi", Double.parseDouble(args[2]));
                    jsonParam.put("note", args[3]);

                    jsonParam.put("m_time", args[4]);
                    jsonParam.put("id", Integer.parseInt(args[5]));
                    jsonParam.put("measure_id", args[6]);
                    jsonParam.put("customer_id", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customer_app_id", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("created_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("updated_at", CommonFunc.getNowUTCDateString());
                    jsonParam.put("type", "0");

                    break;
                case MednetGetStepCompetition:
                    //取得競賽列表
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/MeasureValue/StepCompetition/GetStepCompetition");

                    Date now = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(now);
                    String month = (cal.get(Calendar.MONTH) + 1) >= 9 ? "" + (cal.get(Calendar.MONTH) + 1) : "0" + (cal.get(Calendar.MONTH) + 1);
                    String startTime = cal.get(Calendar.YEAR) + "-" + month + "-01T00:00:00.000Z";
                    String endTime = cal.get(Calendar.YEAR) + "-" + month + "-" + cal.get(Calendar.DAY_OF_MONTH) + "T23:59:59.000Z";
                    jsonParam.put("start_time", startTime);
                    jsonParam.put("end_time", endTime);
                    //Log.i("debug", "jsonParam:  " + jsonParam);
                    break;
                case MednetGetRelationships:
                    //取得好友列表
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Customer/GetRelationships");
                    break;
                case MednetAddRelationship:
                    //新增好友列表
                    type = POST;
                    jsonParam.put("friend_app_id", args[0]);
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "/api/Customer/AddRelationship");
                    break;
                case MednetIsAcceptRelationship:
                    //同意好友邀請
                    type = POST;
                    HashMap mapParam = new HashMap();
                    mapParam.put("appId", args[0]);
                    mapParam.put("IsAllow", args[1]);

                    //Log.i("debug", "doInBackground: " + mapParam);
                    apiUrl = String.format("%s%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "/api/Customer/IsAcceptRelationship?", HttpConnectFunc.getQuery(mapParam).toString());
                    //Log.i("debug", "doInBackground:apiUrl =  " + apiUrl);
                    break;
            }
            //Log.e("debug", mTask + "  jsonParam: " + jsonParam);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        if (type == GET) {
            return HttpConnectFunc.sendCustomerGet(apiUrl, jsonParam);
        }
        return HttpConnectFunc.sendCustomerPost(apiUrl, jsonParam);
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        //Log.i("debug", "mTask = "+mTask+" mNeedLogin = " + mNeedLogin + " result = " + result);
        if (result != null && "401".equals(result.optString("code")) && mNeedLogin) {
            doLogin();
        }

        endDate = new Date(System.currentTimeMillis());
        long diff = endDate.getTime() - curDate.getTime();
        Log.e("debug", mTask + "  呼叫時間: " + diff);

        if (mPostListener != null)
            mPostListener.onPostExecute(mTask, result);
    }

    private void doLogin() {
        //沒登入且該登入頁需要導登入
        Intent intent = new Intent(mContext, UserLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //如果這Activity是開啟的就不再重複開啟
        ((AppCompatActivity) mContext).startActivityForResult(intent, Integer.parseInt(MegaApplication.ActionTypeLogin));
        return;
    }

}