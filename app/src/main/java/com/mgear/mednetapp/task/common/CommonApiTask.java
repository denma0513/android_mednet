package com.mgear.mednetapp.task.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mgear.mednetapp.MegaApplication;
import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPost2ExecuteImpl;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.utils.HttpConnectFunc;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Dennis
 * class: CommonApiTask
 */
public class CommonApiTask extends AsyncTask<String, Void, JSONObject> {

    private Context mContext;
    private boolean mShowDialog;
    private String mDialogMessage;
    private TaskPost2ExecuteImpl mPostListener;
    private ProgressDialog mDialog;
    private TaskEnum mTask;
    private static int GET = 0;
    private static int POST = 1;
    private Date endDate, curDate;

    /**
     * 建構函數
     *
     * @param context            Context
     * @param postExecute        執行完成的回呼函數
     * @param showProgressDialog 是否顯示對話方塊
     * @param message            對話方塊訊息文字
     */
    public CommonApiTask(Context context, TaskPost2ExecuteImpl postExecute, boolean showProgressDialog, String message, TaskEnum task) {
        mContext = context;
        mPostListener = postExecute;
        mShowDialog = showProgressDialog;
        mDialogMessage = message;
        mTask = task;
    }

    @Override
    protected void onPreExecute() {
        if (mShowDialog) {
            mDialog = new ProgressDialog(mContext);
            mDialog.setMessage(mDialogMessage);
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }
    }

    @Override
    protected JSONObject doInBackground(String... args) {
        String apiUrl = "";
        JSONObject jsonParam = new JSONObject();
        int type = POST;
        curDate = new Date(System.currentTimeMillis());

        try {
            switch (mTask) {
                case AndroidKeyHashApi:
                    apiUrl = String.format("%s%s", "http://survey.med-net.com:8080/", "MedicalApi/AndroidKeyHashApi");
                    jsonParam.put("sha1", args[0]);
                    jsonParam.put("keyHash", args[1]);
                    break;
                case FixEmail:
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/FixEmail");
                    jsonParam.put("customerAppId", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("email", args[0]);
                    break;
                case HomePageTask:
                    //http://mednetmember.shinda.com.tw/api/appapi
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_MEDNET_API_URL, "api/appapi");
                    //apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/TestApi");
                    jsonParam.put("path", "MainPage");
                    break;
                case AppCheckUpgradeTask:
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/AppVersion");
                    jsonParam.put("device", "2");
                    jsonParam.put("version", args[0]);
                    break;
                case MednetLoginTask:
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Account/LogIn");
                    if ("0".equals(args[0])) {
                        //帳號密碼
                        jsonParam.put("account_id", args[1]);
                        jsonParam.put("password", args[2]);
                    } else {
                        //第三方登入
                        jsonParam.put("access_token", args[3]);
                    }
                    jsonParam.put("login_type", Integer.parseInt(args[0]));
                    break;
                case MednetRefreshTokenTask:
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Account/RefreshToekn");
                    jsonParam.put("refresh_token", args[0]);
                    break;
                case MednetCreateAccount:
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Account/CreateAccount");
                    if ("0".equals(args[0])) {
                        //帳號密碼
                        jsonParam.put("account_id", args[1]);
                        jsonParam.put("password", args[2]);
                    } else {
                        //第三方
                        jsonParam.put("account_id", args[1]);
                        jsonParam.put("access_token", args[3]);
                    }
                    jsonParam.put("login_type", Integer.parseInt(args[0]));
                    break;
                case MednetForgetPassword:
                    type = GET;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Account/ForgetPassword");
                    jsonParam.put("email", args[0]);
                    break;
                case MednetCheckForgetPassword:
                    type = GET;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_HQ_API_URL, "api/Account/CheckSMS_ForgetPassword");
                    jsonParam.put("email", args[0]);
                    jsonParam.put("optCode", args[1]);
                    break;
                case MednetRegisterPushTokenSave:
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/RegisterPushTokenSave");
                    //apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/RegisterPushTokenSave");
                    jsonParam.put("pushToken", MegaApplication.pushToken);
                    jsonParam.put("customerId", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("device", "2");
                    break;
                case MednetSendMessageWithCustomerId:
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/sendMessageWithCustomerId");
                    //apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/sendMessageWithCustomerId");
                    jsonParam.put("customerId", args[0]);
                    break;
                case MednetRejectAddFriend:
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/RejectAddFriend");
                    //apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/sendMessageWithCustomerId");
                    jsonParam.put("appId", args[0]);
                    jsonParam.put("customerId", args[0]);
                    break;

                case MednetAppLogEven:
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/CreateMednetAppLogEven");
                    //apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/sendMessageWithCustomerId");
                    jsonParam.put("event", args[0]);
                    jsonParam.put("value", args[1]);
                    jsonParam.put("customerId", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customerAppId", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("insertUser", MegaApplication.UUID);
                    jsonParam.put("insertAddress", MegaApplication.IPAddress);
                    jsonParam.put("device", "2");
                    break;
                case MednetAppLogNotice:
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/CreateMednetAppLogNotice");
                    //apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/sendMessageWithCustomerId");
                    jsonParam.put("healthType", args[0]);
                    jsonParam.put("noticeTime", args[1]);
                    jsonParam.put("action", args[2]);
                    jsonParam.put("customerId", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customerAppId", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("insertUser", MegaApplication.UUID);
                    jsonParam.put("insertAddress", MegaApplication.IPAddress);
                    jsonParam.put("device", "2");
                    break;
                case MednetAppLogHealthOrder:
                    type = POST;
                    apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/RegisterMednetAppLogHealthOrder");
                    //apiUrl = String.format("%s%s", HttpConnectFunc.CONNECT_API_URL, "MedicalApi/sendMessageWithCustomerId");
                    jsonParam.put("healthType", args[0]);
                    jsonParam.put("healthOrder", args[1]);
                    jsonParam.put("customerId", MegaApplication.getInstance().getMember().getCustomerId());
                    jsonParam.put("customerAppId", MegaApplication.getInstance().getMember().getCustomerAppId());
                    jsonParam.put("insertUser", MegaApplication.UUID);
                    jsonParam.put("insertAddress", MegaApplication.IPAddress);
                    jsonParam.put("device", "2");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        //Log.i("debu", "url = " + apiUrl + " jsonParam =  " + jsonParam);
        if (type == GET) {
            return HttpConnectFunc.sendGet(apiUrl, jsonParam);
        }

        return HttpConnectFunc.sendPost(apiUrl, jsonParam);
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        endDate = new Date(System.currentTimeMillis());
        long diff = endDate.getTime() - curDate.getTime();
        Log.e("debug", mTask + "  呼叫時間: " + diff);
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }


        switch (mTask) {
            case HomePageTask:

                if (result != null) {
                    try {
                        result.put("code", result.optInt("code"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }


        if (mPostListener != null)
            mPostListener.onPostExecute(mTask, result);
    }

}