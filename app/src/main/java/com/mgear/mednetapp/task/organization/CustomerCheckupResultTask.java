package com.mgear.mednetapp.task.organization;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mgear.mednetapp.enums.TaskEnum;
import com.mgear.mednetapp.interfaces.organization.TaskPostExecuteImpl;
import com.mgear.mednetapp.entity.organization.CheckupResultSet;
import com.mgear.mednetapp.utils.HttpConnectFunc;

/**
 * Created by Jye on 2017/8/21.
 * class: CustomerCheckupResultTask
 */
public class CustomerCheckupResultTask extends AsyncTask<String, Void, CheckupResultSet> {

    private Context mContext;
    private boolean mShowDialog;
    private String mDialogMessage;
    private TaskPostExecuteImpl mPostListener;
    private ProgressDialog mDialog;

    /**
     * 建構函數
     * @param context Context
     * @param postExecute 執行完成的回呼函數
     * @param showProgressDialog 是否顯示對話方塊
     * @param message 對話方塊訊息文字
     */
    public CustomerCheckupResultTask(Context context, TaskPostExecuteImpl postExecute, boolean showProgressDialog, String message) {
        mContext = context;
        mPostListener = postExecute;
        mShowDialog = showProgressDialog;
        mDialogMessage = message;
    }

    @Override
    protected void onPreExecute() {
        if (mShowDialog) {
            mDialog = new ProgressDialog(mContext);
            mDialog.setMessage(mDialogMessage);
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }
    }

    @Override
    protected CheckupResultSet doInBackground(String... args) {
        return null;
        //return HttpConnectFunc.executeCustomerCheckupResult(args[0], args[1], CustomerActivity.getHealthySystemList());
    }

    @Override
    protected void onPostExecute(CheckupResultSet result) {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        if (mPostListener != null)
            mPostListener.onPostExecute(TaskEnum.CustomerCheckupResultTask, result);
    }

}