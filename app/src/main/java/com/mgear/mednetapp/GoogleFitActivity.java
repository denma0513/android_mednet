package com.mgear.mednetapp;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.mgear.mednetapp.fragments.base.MegaBaseFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleFitActivity extends BaseActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        OnDataPointListener, ResultCallback<DataSourcesResult> {
    private static final String TAG = "GOOGLE_FIT";
    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 24 * HOUR;

    //邏輯元件
    private MegaBaseFragment selectedFragment;
    private static int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 6;
    private GoogleApiClient mClient;


    private void accessGoogleFit() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        long endTime = cal.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        //cal.add(Calendar.DAY_OF_YEAR, -1);

        long startTime = cal.getTimeInMillis();

        DateFormat dateFormat = DateFormat.getDateInstance();
        Log.i("GOOGLE_FIT", "Range Start: " + sdf.format(startTime));
        Log.i("GOOGLE_FIT", "Range End: " + sdf.format(endTime));


        mClient = new GoogleApiClient.Builder(this)
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        DataSourcesRequest dataSourcesRequest = new DataSourcesRequest.Builder()
                .setDataTypes(DataType.TYPE_HEART_RATE_BPM/*,
                    DataType.TYPE_STEP_COUNT_DELTA*/)
                .setDataSourceTypes(DataSource.TYPE_RAW) // data type, raw or derived?
                .build();


        Fitness.SensorsApi.findDataSources(mClient, dataSourcesRequest).setResultCallback(this);


        DataReadRequest readRequest =
                new DataReadRequest.Builder()
                        .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                        //.aggregate(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY)
                        .aggregate(DataType.TYPE_ACTIVITY_SEGMENT, DataType.AGGREGATE_ACTIVITY_SUMMARY)
                        .bucketByTime(1, TimeUnit.DAYS)
                        .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                        .build();


        Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .readData(readRequest)
                .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                    @Override
                    public void onSuccess(DataReadResponse dataReadResult) {
                        Log.d("GOOGLE_FIT", "onSuccess()");
                        if (dataReadResult.getBuckets().size() > 0) {
                            Log.i("GOOGLE_FIT", "Number of returned buckets of DataSets is: " + dataReadResult.getBuckets().size());
                            for (Bucket bucket : dataReadResult.getBuckets()) {
                                List<DataSet> dataSets = bucket.getDataSets();
                                for (DataSet dataSet : dataSets) {
                                    dumpDataSet(dataSet);
                                }
                            }
                        } else if (dataReadResult.getDataSets().size() > 0) {
                            Log.i("GOOGLE_FIT", "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                            Log.i("GOOGLE_FIT", "getName = " + dataReadResult.getDataSets().get(0).getDataType().getName());
                            for (DataSet dataSet : dataReadResult.getDataSets()) {
                                dumpDataSet(dataSet);
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("GOOGLE_FIT", "onFailure()", e);
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<DataReadResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<DataReadResponse> task) {
                        Log.d("GOOGLE_FIT", "onComplete()");
                        Log.d("GOOGLE_FIT", "task = " + task.getResult().getBuckets());
                    }
                });
    }

    // [START parse_dataset]
    private static void dumpDataSet(DataSet dataSet) {
        DateFormat dateFormat = DateFormat.getTimeInstance();
        for (DataPoint dp : dataSet.getDataPoints()) {
//            Log.i("GOOGLE_FIT", "Data point:");
//            Log.i("GOOGLE_FIT", "\tType: " + dp.getDataType().getName() + " " + dp.getDataType().getName());
//            Log.i("GOOGLE_FIT", "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
//            Log.i("GOOGLE_FIT", "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
            for (Field field : dp.getDataType().getFields()) {
//                Log.i("GOOGLE_FIT", "\tField: " + field.getName() + " Value: " + dp.getValue(field));

                if (dp.getOriginalDataSource().getAppPackageName().toString().contains("sleep") && field.getName().contains("duration")) {
                    //if ("duration".equals(field.getName())) {
                    double ms = dp.getValue(field).asInt();
                    StringBuffer text = new StringBuffer("");
                    int hour = 0;
                    int min = 0;
                    if (ms > HOUR) {
                        hour = (int) (ms / HOUR);
                    }
                    if (ms > MINUTE) {
                        min = (int) (ms / MINUTE) % 60 ;
                    }
                    Log.i(TAG,"睡眠時間 ＝"+hour+"："+min);
                }else if (dp.getDataType().getName().toString().contains("step")) {
                    Log.i(TAG,"步數 ＝"+dp.getValue(field));
                }
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_fit);

        mClient = new GoogleApiClient.Builder(this)
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        FitnessOptions fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_WRITE)
                .build();
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this, // your activity
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions);
        } else {
            accessGoogleFit();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull DataSourcesResult dataSourcesResult) {

    }

    @Override
    public void onDataPoint(DataPoint dataPoint) {

    }

    @Override
    public void onPause() {
        super.onPause();
        mClient.stopAutoManage(this);
        mClient.disconnect();
    }
}
